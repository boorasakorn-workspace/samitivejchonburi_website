<?php
//Title
$lang['Home_Text'] = 'หน้าหลัก';
$lang['title_name'] = 'โรงพยาบาลสมิติเวชชลบุรี';
$lang['Self_regis_title'] = 'ระบบลงทะเบียนออนไลน์';
$lang['Self_regis_text'] = 'ลงทะเบียนด้วยตัวเองที่นี่';
$lang['Self_Reg_text'] = 'ลงทะเบียนด้วยตัวเอง';
$lang['Close_Text'] = 'ปิด';
$lang['Login_Modal_Text'] = 'ป้อนชื่อผู้ใช้และรหัสผ่านเพื่อเข้าสู่ระบบ';
$lang['User_Username_Text'] = 'ชื่อผู้ใช้';
$lang['User_Password_Text'] = 'รหัสผ่าน';
$lang['User_Login_Text'] = 'เข้าสู่ระบบ';
$lang['User_Register_Text'] = 'สมัครสมาชิก';
//Top#1
 //$lang['samitivej_Find_a_Doctor']
$lang['samitivej_Find_a_Doctor_text'] = 'ค้นหาแพทย์ของโรงพยาบาลสมิติเวชชลบุรี.';
 //$lang['samitivej_Request_an_Appointment']
$lang['samitivej_Request_an_Appointment_text'] = 'นัดหมายแพทย์ของโรงพยาบาลสมิติเวชชลบุรี.';
$lang['samitivej_Hotline'] = 'สายด่วน สมิติเวชชลบุรี';
$lang['samitivej_Hotline_text'] = 'สำหรับทั้งติดต่อฉุกเฉินและติดต่อเพื่อทำการนัดหมายแพทย์.';
$lang['samitivej_Contact'] = 'ติดต่อสอบถาม';
 //$lang['samitivej_Working_Time_title']
$lang['samitivej_Mon-Fri'] = 'จันทร์ - ศุกร์';
$lang['samitivej_Saturday'] = 'เสาร์';
$lang['samitivej_Sunday'] = 'อาทิตย์';
//Top#2
 //$lang['samitivej_Package_Promotion']
$lang['samitivej_Package_Text'] = 'แพ็กเกจ';
$lang['samitivej_Promotion_Text'] = 'โปรโมชั่น';
$lang['samitivej_Start_Price'] = 'ราคาเริ่มต้น';
$lang['samitivej_Price_Desc'] = 'ราคาแพ็กเกจรวมค่าแพทย์และค่าบริการโรงพยาบาลแล้ว';
$lang['samitivej_Price_Desc_2'] = 'ใช้บริการที่โรงพยาบาลสมิติเวชชลบุรีเท่านั้น';
//Top#3
$lang['samitivej_Healthy_Blog'] = 'บทความสุขภาพ';
//Top#4
$lang['samitivej_Service'] = 'บริการทางการแพทย์';
$lang['samitivej_Service_text'] = 
'โรงพยาบาลสมิติเวช ชลบุรี มีบริการทางการแพทย์ มีแผนกต่าง ๆ ให้บริการทั้งการรักษาโรคทั่วไปและโรคเฉพาะทาง';
//Top#5
$lang['samitivej_Our_Doctor'] = 'แนะนำแพทย์';
$lang['samitivej_Our_Doctor_text'] = 'แพทย์โรงพยาบาลสมิติเวชชลบุรี';
//Top#6
$lang['samitivej_News_And_Activity'] = 'ข่าวสาร & กิจกรรม';
$lang['samitivej_News_Text'] = 'ข่าวสาร';
$lang['samitivej_Activity_Text'] = 'กิจกรรม';
//Top#6-2
$lang['samitivej_Subscribe'] = 'รับข่าวสารจากทางโรงพยาบาล';
$lang['samitivej_Subscribe_text'] = 'อัพเดทข่าวสารเกี่ยวสุขภาพใหม่ ๆ กับเรา';
$lang['samitivej_Firstname'] = 'ชื่อ';
$lang['samitivej_Lastname'] = 'นามสกุล';
$lang['samitivej_Subscribe_Submit'] = 'สมัครรับข่าวสาร';
//Top Section
$lang['samitivej_See_All'] = 'ดูทั้งหมด';
$lang['samitivej_Read_More'] = 'อ่านต่อ';
$lang['samitivej_More_Details'] = 'รายละเอียดเพิ่มเติม';

//Footer#1
$lang['samitivej_locate_title'] = 'สถานที่ตั้งโรงพยาบาลสมิติเวชชลบุรี';
$lang['samitivej_locate_text'] = 
'888/88 หมู่ 3 ถนนสุขุมวิท <br>
ตำบลบ้านสวน อำเภอเมือง <br>
จังหวัดชลบุรี รหัสไปรษณีย์ 20000';
$lang['samitivej_Contact_Us'] = 'ติดต่อเรา';
//Footer#2
$lang['samitivej_Our_Services'] = 'บริการของเรา';
$lang['samitivej_About_Us'] = 'เกี่ยวกับเรา';
$lang['samitivej_Service_Center'] = 'ศูนย์บริการ';
$lang['samitivej_Package_Promotion'] = 'แพ็กเกจและโปรโมชั่น';
$lang['samitivej_Find_a_Doctor'] = 'ค้นหาแพทย์';
$lang['samitivej_Request_an_Appointment'] = 'นัดหมายแพทย์';
$lang['samitivej_Appointment'] = 'นัดหมายแพทย์';
$lang['samitivej_Join_Us'] = 'ร่วมงานกับเรา';
//Footer#3
$lang['samitivej_Hospital_Locations'] = 'เครือข่ายโรงพยาบาล';
$lang['samitivej_Locate_Sukhumvit'] = 'สมิติเวช สุขุมวิท';
$lang['samitivej_Locate_Srinakarin'] = 'สมิติเวช ศรีนครินทร์';
$lang['samitivej_Locate_Thonburi'] = 'สมิติเวช ธนบุรี';
$lang['samitivej_Locate_Chinatown'] = 'สมิติเวช ไชน่าทาวน์';
$lang['samitivej_Locate_Sriracha'] = 'สมิติเวช ศรีราชา';
$lang['samitivej_Locate_Chonburi'] = 'สมิติเวช ชลบุรี';
$lang['samitivej_Locate_Outreach_Clinics'] = 'คลินิกพิเศษสมิติเวช';
$lang['samitivej_Locate_Children'] = 'โรงพยาบาลเด็กสมิติเวช';
//Footer#4
$lang['samitivej_Working_Time_title'] = 'ตลอด 24 ชั่วโมง';
$lang['samitivej_Working_Time_text'] = 
'จันทร์ - อังคาร : 6.00 am - 10.00 pm
พุธ - พฤหัสบดี : 8.00 am - 6.00 pm
ศุกร์ : 3.00 pm - 8.00 pm
อาทิตย์ : Closed';

$lang['samitivej_Working_Time_01'] = 'จันทร์ - อังคาร : <span class="pull-right">6.00 am - 10.00 pm</span>';
$lang['samitivej_Working_Time_02'] = 'พุธ - พฤหัสบดี : <span class="pull-right">8.00 am - 6.00 pm</span>';
$lang['samitivej_Working_Time_03'] = 'ศุกร์ : <span class="pull-right">3.00 pm - 8.00 pm</span>';
$lang['samitivej_Working_Time_04'] = 'อาทิตย์ : <span class="pull-right">ปิด</span>';

//ContactUsPage
$lang['samitivej_Contact_Us_Title'] = 'ติดต่อสอบถาม';
$lang['samitivej_Contact_Information_Title'] = 'ข้อมูลผู้ติดต่อ';
//$lang['samitivej_Firstname'] = 'ชื่อ';
//$lang['samitivej_Lastname'] = 'นามสกุล';
$lang['samitivej_Email'] = 'อีเมลล์';
$lang['samitivej_Tel_no'] = 'เบอร์โทรศัพท์';
$lang['samitivej_Contact_Topic'] = 'กรุณาเลือกประเภทคำถาม';
$lang['samitivej_Select_Contact_Topic'] = 'เลือกคำถาม';
//$lang['samitivej_Request_an_Appointment'] = 'นัดหมายแพทย์';
$lang['samitivej_Billing'] = 'การเรียกเก็บเงิน';
$lang['samitivej_Insurance'] = 'การประกันภัย';
$lang['samitivej_Contact_Details'] = 'กรุณาระบุคำถามของท่าน หรือ ให้ข้อคิดเห็น';
$lang['samitivej_Privacy_Policy'] = 'นโยบายความเป็นส่วนตัว';
$lang['Accept_our_privacy_policy'] = 'ยอมรับนโยบายความเป็นส่วนตัวของเรา';
$lang['Subscribe_News_Letter'] = 'สมัครรับข่าวสารสุขภาพของเรา';
$lang['Confirmed_Subscribe'] = ' ใช่ ฉันต้องการรับข่าวสารสุขภาพจากโรงพยาบาลสมิติเวช';
$lang['samitivej_Save'] = 'บันทึก';

//Doctor_all
$lang['samitivej_All_Doctor_Title'] = 'แพทย์ทั้งหมด';
$lang['samitivej_All_Doctor_Search'] = 'ค้นหาแพทย์';
$lang['samitivej_All_Doctor_Select'] = 'เลือกแพทย์';
$lang['samitivej_Doctor_name'] = 'ชื่อแพทย์';
//$lang['samitivej_Speciality'] = 'ความชำนาญ';
$lang['samitivej_All_Service'] = 'ทุกศูนย์บริการ';
$lang['samitivej_All_Speciality'] = 'ทุกความชำนาญ';
$lang['samitivej_Doctor_Work_Time'] = 'วัน-เวลา ที่เข้าตรวจ';
$lang['samitivej_Doctor_Language'] = 'ภาษา';
$lang['samitivej_All_Language'] = 'ทุกภาษา';
$lang['samitivej_Doctor_Gender'] = 'เพศ';
$lang['samitivej_Doctor_Gender_All'] = 'ทุกเพศ';
$lang['samitivej_Doctor_Gender_Male'] = 'ชาย';
$lang['samitivej_Doctor_Gender_Female'] = 'หญิง';
$lang['samitivej_Doctor_Filter_Result'] = 'กรองผลการค้นหา';
//$lang['samitivej_More_Details'] = 'รายละเอียดเพิ่มเติม';
//$lang['samitivej_Request_an_Appointment'] = 'นัดหมายแพทย์';
$lang['samitivej_Previous_Text'] = 'ก่อนหน้า';
$lang['samitivej_Next_Text'] = 'ถัดไป';

//Doctor_details
$lang['samitivej_Doctor_Info'] = 'รายละเอียดแพทย์';
$lang['samitivej_Fullname'] = 'ชื่อ';
$lang['samitivej_Speciality'] = 'ความชำนาญ';
$lang['samitivej_Sub_Speciality'] = 'ความชำนาญเฉพาะ';
$lang['samitivej_Education'] = 'คุณวุฒิทางการศึกษา';
//$lang['samitivej_Tel_no'] = 'เบอร์โทรศัพท์';
//$lang['samitivej_Email'] = 'อีเมลล์';
$lang['samitivej_Experience'] = 'ประสบการณ์การทำงาน';
$lang['samitivej_Doctor_Description'] = 'ความคิดเห็นเพิ่มเติมจากแพทย์';
$lang['samitivej_Doctor_Appointment_Table'] = 'ตาราง วัน-เวลา นัดหมายแพทย์';
$lang['samitivej_Day_Appointment_Table'] = 'วัน';
$lang['samitivej_Hour_Appointment_Table'] = 'เวลา';
//$lang['samitivej_Request_an_Appointment'] = 'นัดหมายแพทย์';

$lang['samitivej_day_จันทร์'] = 'จันทร์';
$lang['samitivej_day_วันจันทร์'] = 'วันจันทร์';
$lang['samitivej_day_อังคาร'] = 'อังคาร';
$lang['samitivej_day_วันอังคาร'] = 'วันอังคาร';
$lang['samitivej_day_พุธ'] = 'พุธ';
$lang['samitivej_day_วันพุธ'] = 'วันพุธ';
$lang['samitivej_day_พฤหัสบดี'] = 'พฤหัสบดี';
$lang['samitivej_day_วันพฤหัสบดี'] = 'วันพฤหัสบดี';
$lang['samitivej_day_ศุกร์'] = 'ศุกร์';
$lang['samitivej_day_วันศุกร์'] = 'วันศุกร์';
$lang['samitivej_day_เสาร์'] = 'เสาร์';
$lang['samitivej_day_วันเสาร์'] = 'เสาร์';
$lang['samitivej_day_อาทิตย์'] = 'อาทิตย์';
$lang['samitivej_day_วันอาทิตย์'] = 'วันอาทิตย์';


$lang['samitivej_Of'] = 'ของ';
$lang['samitivej_By'] = 'โดย';

//Appointment
$lang['samitivej_Patient_Info_Title'] = 'ข้อมูลผู้ป่วย';
//$lang['samitivej_Firstname'] = 'ชื่อ';
//$lang['samitivej_Lastname'] = 'นามสกุล';
$lang['samitivej_Birthday'] = 'วันเกิด';
//$lang['samitivej_Doctor_Gender'] = 'เพศ';
$lang['samitivej_Doctor_Select_Gender'] = 'เลือกเพศ';
//$lang['samitivej_Doctor_Gender_Male'] = 'ชาย';
//$lang['samitivej_Doctor_Gender_Female'] = 'หญิง';
//$lang['samitivej_Email'] = 'อีเมลล์';
//$lang['samitivej_Tel_no'] = 'เบอร์โทรศัพท์';
$lang['samitivej_Speciality_Doctor'] = 'สาขาเฉพาะทางของแพทย์ผู้เชี่ยวชาญ';
$lang['samitivej_Select_Doctor'] = 'เลือกแพทย์';
$lang['samitivej_Appointment_Info'] = 'ข้อมูลนัดหมาย';
$lang['samitivej_Describe_medical_problem'] = 'อธิบายปัญหาทางด้านสุขภาพของท่าน';
$lang['samitivej_Appointment_Date_Select'] = 'วันที่นัดหมาย';
$lang['samitivej_SecondAppointment_Date_Select'] = 'วันที่นัดหมาย (สำรอง)';
$lang['samitivej_Be_Patient_Before'] = 'คุณเคยขึ้นทะเบียนเป็นผู้ป่วย ที่โรงพยาบาลสมิติเวชแล้วหรือไม่';
$lang['samitivej_Yes'] = 'ใช่';
//$lang['samitivej_Privacy_Policy'] = 'นโยบายความเป็นส่วนตัว';
//$lang['Accept_our_privacy_policy'] = 'ยอมรับนโยบายความเป็นส่วนตัวของเรา';
//$lang['Subscribe_News_Letter'] = 'สมัครรับข่าวสารสุขภาพของเรา';
//$lang['Confirmed_Subscribe'] = ' ใช่ ฉันต้องการรับข่าวสารสุขภาพจากโรงพยาบาลสมิติเวช';
//$lang['samitivej_Request_an_Appointment'] = 'นัดหมายแพทย์';

$lang['samitivej_Related_Promotion'] = 'โปรโมชั่นที่เกี่ยวข้อง';
$lang['samitivej_Related_Medical_Center'] = 'ศูนย์บริการที่เกี่ยวข้อง';
$lang['samitivej_Related_Doctor'] = 'แพทย์ที่เกี่ยวข้อง';
