<?php
//Title
$lang['Home_Text'] = 'Home';
$lang['title_name'] = 'Samitivej Chonburi';
$lang['Self_regis_title'] = 'Online Self-Register';
$lang['Self_regis_text'] = 'Self Register';
$lang['Self_Reg_text'] = 'Register';
$lang['Close_Text'] = 'Close';
$lang['Login_Modal_Text'] = 'Enter Username and Password';
$lang['User_Username_Text'] = 'Username';
$lang['User_Password_Text'] = 'Password';
$lang['User_Login_Text'] = 'Login';
$lang['User_Register_Text'] = 'Register';
//Top#1
 //$lang['samitivej_Find_a_Doctor']
$lang['samitivej_Find_a_Doctor_text'] = 'Which problem you faced on your health ?call or mail us and make an appointment with our expert who can solved your problem.';
 //$lang['samitivej_Request_an_Appointment']
$lang['samitivej_Request_an_Appointment_text'] = 'Take a look on our clinic Doctors timetable and make an appointment with our expert which department or service.';
$lang['samitivej_Hotline'] = 'Samitivej Hotline';
$lang['samitivej_Hotline_text'] = 'You can call us for emergency & make an appointment out of clinic.';
$lang['samitivej_Contact'] = 'Contact';
 //$lang['samitivej_Working_Time_title']
$lang['samitivej_Mon-Fri'] = 'Mon-Fri';
$lang['samitivej_Saturday'] = 'Saturday';
$lang['samitivej_Sunday'] = 'Sunday';
//Top#2
 //$lang['samitivej_Package_Promotion']
$lang['samitivej_Package_Text'] = 'Package';
$lang['samitivej_Promotion_Text'] = 'Promotion';
$lang['samitivej_Start_Price'] = 'Start Price';
$lang['samitivej_Price_Desc'] = 'Package Price include medical and service cost';
$lang['samitivej_Price_Desc_2'] = 'Only At Samitivej Chonburi';
//Top#3
$lang['samitivej_Healthy_Blog'] = 'Healthy Blog';
//Top#4
$lang['samitivej_Service'] = 'Medical Services';
$lang['samitivej_Service_text'] = 'Samitivej have various services';
//Top#5
$lang['samitivej_Our_Doctor'] = 'Our Doctor';
$lang['samitivej_Our_Doctor_text'] = 'Samitivej Chonburi Doctor';
//Top#6
$lang['samitivej_News_And_Activity'] = 'News & Activity';
$lang['samitivej_News_Text'] = 'News';
$lang['samitivej_Activity_Text'] = 'Activity';
//Top#6-2
$lang['samitivej_Subscribe'] = 'Subscribe to Our Newsletter';
$lang['samitivej_Subscribe_text'] = 'Stay tuned with us';
$lang['samitivej_Firstname'] = 'Firstname';
$lang['samitivej_Lastname'] = 'Lastname';
$lang['samitivej_Subscribe_Submit'] = 'Subscribe';
//Top Section
$lang['samitivej_See_All'] = 'See All';
$lang['samitivej_Read_More'] = 'Read More';
$lang['samitivej_More_Details'] = 'More Details';

//Footer#1
$lang['samitivej_locate_title'] = 'Samitivej Location';
$lang['samitivej_locate_text'] = 
'888/88 Village No.3 Sukhumvit Road <br>
Ban Suan Mueang District  <br>
Chonburi 20000';
$lang['samitivej_Contact_Us'] = 'Contact Us';
//Footer#2
$lang['samitivej_Our_Services'] = 'Our Services';
$lang['samitivej_About_Us'] = 'About Us';
$lang['samitivej_Service_Center'] = 'Service Center';
$lang['samitivej_Package_Promotion'] = 'Package & Promotion';
$lang['samitivej_Find_a_Doctor'] = 'Find a Doctor';
$lang['samitivej_Request_an_Appointment'] = 'Request an Appointment';
$lang['samitivej_Appointment'] = 'Appointment';
$lang['samitivej_Join_Us'] = 'Join Us';
//Footer#3
$lang['samitivej_Hospital_Locations'] = 'Hospital Locations';
$lang['samitivej_Locate_Sukhumvit'] = 'Samitivej Sukhumvit';
$lang['samitivej_Locate_Srinakarin'] = 'Samitivej Srinakarin';
$lang['samitivej_Locate_Thonburi'] = 'Samitivej Thonburi';
$lang['samitivej_Locate_Chinatown'] = 'Samitivej Chinatown';
$lang['samitivej_Locate_Sriracha'] = 'Samitivej Sriracha';
$lang['samitivej_Locate_Chonburi'] = 'Samitivej Chonburi';
$lang['samitivej_Locate_Outreach_Clinics'] = 'Samitivej Outreach Clinics';
$lang['samitivej_Locate_Children'] = 'Samitivej Children Hospital';
//Footer#4
$lang['samitivej_Working_Time_title'] = '24 hours';
$lang['samitivej_Working_Time_text'] = 
'Mon - Tues : 6.00 am - 10.00 pm
Wednes - Thurs : 8.00 am - 6.00 pm
Fri : 3.00 pm - 8.00 pm
Sun : Closed';

$lang['samitivej_Working_Time_01'] = 'Mon - Tues : <span class="pull-right">6.00 am - 10.00 pm</span>';
$lang['samitivej_Working_Time_02'] = 'Wednes - Thurs : <span class="pull-right">8.00 am - 6.00 pm</span>';
$lang['samitivej_Working_Time_03'] = 'Fri : <span class="pull-right">3.00 pm - 8.00 pm</span>';
$lang['samitivej_Working_Time_04'] = 'Sun : <span class="pull-right">ปิด</span>';

//ContactUsPage
$lang['samitivej_Contact_Us_Title'] = 'Contact Us';
$lang['samitivej_Contact_Information_Title'] = 'Contact Information';
//$lang['samitivej_Firstname'] = 'Firstname';
//$lang['samitivej_Lastname'] = 'Lastname';
$lang['samitivej_Email'] = 'Email';
$lang['samitivej_Tel_no'] = 'Tel-number';
$lang['samitivej_Contact_Topic'] = 'Contact Topic';
$lang['samitivej_Select_Contact_Topic'] = 'Please select your topic';
//$lang['samitivej_Request_an_Appointment'] = 'Request an Appointment';
$lang['samitivej_Billing'] = 'Billing';
$lang['samitivej_Insurance'] = 'Insurance';
$lang['samitivej_Contact_Details'] = 'Topic Details or giving us a comment';
$lang['samitivej_Privacy_Policy'] = 'Privacy Policy';
$lang['Accept_our_privacy_policy'] = 'Accept our privacy policy';
$lang['Subscribe_News_Letter'] = 'Subscribe to Our news letter';
$lang['Confirmed_Subscribe'] = 'Yes i want to subscribe to samitivej chonburi news letter';
$lang['samitivej_Save'] = 'Save';

//Doctor_all
$lang['samitivej_All_Doctor_Title'] = 'All Doctors';
$lang['samitivej_All_Doctor_Search'] = 'Doctor Search';
$lang['samitivej_All_Doctor_Select'] = 'Select Doctor';
$lang['samitivej_Doctor_name'] = 'Doctor Name';
//$lang['samitivej_Speciality'] = 'ความชำนาญ';
$lang['samitivej_All_Service'] = 'All Medical Center';
$lang['samitivej_All_Speciality'] = 'All Speciality';
$lang['samitivej_Doctor_Work_Time'] = 'Day-Hour Work Time';
$lang['samitivej_Doctor_Language'] = 'Language';
$lang['samitivej_All_Language'] = 'All Language';
$lang['samitivej_Doctor_Gender'] = 'Gender';
$lang['samitivej_Doctor_Gender_All'] = 'All Gender';
$lang['samitivej_Doctor_Gender_Male'] = 'Male';
$lang['samitivej_Doctor_Gender_Female'] = 'Female';
$lang['samitivej_Doctor_Filter_Result'] = 'Filter Result';
//$lang['samitivej_More_Details'] = 'More Details';
//$lang['samitivej_Request_an_Appointment'] = 'Request an Appointment';
$lang['samitivej_Previous_Text'] = 'Previous';
$lang['samitivej_Next_Text'] = 'Next';

//Doctor_details
$lang['samitivej_Doctor_Info'] = 'Doctor Information';
$lang['samitivej_Fullname'] = 'Name';
$lang['samitivej_Speciality'] = 'Speciality';
$lang['samitivej_Sub_Speciality'] = 'Sub Speciality';
$lang['samitivej_Education'] = 'Education';
//$lang['samitivej_Tel_no'] = 'Tel number';
//$lang['samitivej_Email'] = 'Email';
$lang['samitivej_Experience'] = 'Experience';
$lang['samitivej_Doctor_Description'] = 'Comment/Description From Doctor';
$lang['samitivej_Doctor_Appointment_Table'] = 'Day-Hour Appointment Table';
$lang['samitivej_Day_Appointment_Table'] = 'Day';
$lang['samitivej_Hour_Appointment_Table'] = 'Hour';
//$lang['samitivej_Request_an_Appointment'] = 'Request an Appointment';

$lang['samitivej_day_จันทร์'] = 'Monday';
$lang['samitivej_day_วันจันทร์'] = 'Monday';
$lang['samitivej_day_อังคาร'] = 'Tuesday';
$lang['samitivej_day_วันอังคาร'] = 'Tuesday';
$lang['samitivej_day_พุธ'] = 'Wednesday';
$lang['samitivej_day_วันพุธ'] = 'Wednesday';
$lang['samitivej_day_พฤหัสบดี'] = 'Thursday';
$lang['samitivej_day_วันพฤหัสบดี'] = 'Thursday';
$lang['samitivej_day_ศุกร์'] = 'Friday';
$lang['samitivej_day_วันศุกร์'] = 'Friday';
$lang['samitivej_day_เสาร์'] = 'Saturday';
$lang['samitivej_day_วันเสาร์'] = 'Saturday';
$lang['samitivej_day_อาทิตย์'] = 'Sunday';
$lang['samitivej_day_วันอาทิตย์'] = 'Sunday';


$lang['samitivej_Of'] = 'Of';
$lang['samitivej_By'] = 'By';

//Appointment
$lang['samitivej_Patient_Info_Title'] = 'Patient Info';
//$lang['samitivej_Firstname'] = 'ชื่อ';
//$lang['samitivej_Lastname'] = 'นามสกุล';
$lang['samitivej_Birthday'] = 'Birthday';
//$lang['samitivej_Doctor_Gender'] = 'เพศ';
$lang['samitivej_Doctor_Select_Gender'] = 'Select Gender';
//$lang['samitivej_Doctor_Gender_Male'] = 'ชาย';
//$lang['samitivej_Doctor_Gender_Female'] = 'หญิง';
//$lang['samitivej_Email'] = 'อีเมลล์';
//$lang['samitivej_Tel_no'] = 'เบอร์โทรศัพท์';
$lang['samitivej_Appointment_Info'] = 'Appointment Information';
$lang['samitivej_Describe_medical_problem'] = 'Describe your medical problem';
$lang['samitivej_Appointment_Date_Select'] = 'Appointment Date';
$lang['samitivej_SecondAppointment_Date_Select'] = 'Second Appointment Date';
$lang['samitivej_Be_Patient_Before'] = 'Have you ever registered as a patient at Samitivej Hospital?
';
$lang['samitivej_Yes'] = 'Yes';
//$lang['samitivej_Privacy_Policy'] = 'นโยบายความเป็นส่วนตัว';
//$lang['Accept_our_privacy_policy'] = 'ยอมรับนโยบายความเป็นส่วนตัวของเรา';
//$lang['Subscribe_News_Letter'] = 'สมัครรับข่าวสารสุขภาพของเรา';
//$lang['Confirmed_Subscribe'] = ' ใช่ ฉันต้องการรับข่าวสารสุขภาพจากโรงพยาบาลสมิติเวช';
//$lang['samitivej_Request_an_Appointment'] = 'นัดหมายแพทย์';

$lang['samitivej_Related_Promotion'] = 'Related Promotion';
$lang['samitivej_Related_Medical_Center'] = 'Related Service';
$lang['samitivej_Related_Doctor'] = 'Related Doctor';