<?php

defined('BASEPATH') OR exit('No direct script access allowed');
$lang['hospital']='RAJAVITHI病院 バンコク、タイ';
$lang['poll_txt']='外来の前に、新型コロナウルイス感染リスクの自己調査表';
$lang['message'] = '次の問診に真実にお答えください。';

$lang['welcome'] = '発症の14日間前は次の感染リスクに繋がっている行動を取ったことがありますか。';
$lang['choice1']='はい';
$lang['choice2']='いいえ';

$lang['poll_save']='Submit';

$lang['symptom']='どんな症状で外来にお越しですか。';

$lang['symptom1']='発熱';

$lang['symptom2']='咳';
$lang['symptom3']='鼻水が出る';
$lang['symptom4']='のどはれ �痛め';
$lang['symptom5']='痰があり';
$lang['symptom6']='呼吸困難';
$lang['symptom7']='无异常症状';

