<?php

defined('BASEPATH') OR exit('No direct script access allowed');
$lang['hospital'] = 'โรงพยาบาลสมิติเวชชลบุรี';
$lang['poll_txt']='แบบประเมินความเสี่ยงก่อนมาโรงพยาบาลต่อการติดเชื้อ COVID-19';

$lang['area']='พื้นที่ที่มีรายงานการระบาดต่อเนื่องของโรคติดเชื้อไวรัสโคโรนาสายพันธุ์ใหม่2019
สาธารณรัฐประชาชนจีน ฮ่องกง มาเก๊า เกาหลีใต้ อิตาลี อิหร่าน ฝรั่งเศส สเปน สหรัฐอเมริกา สวิตเซอร์แลนด์ นอร์เวย์ ญี่ปุ่น เดนมาร์ก เนเธอร์แลนด์ สวีเดน อังกฤษ เยอรมนี ออสเตรีย เบลเยียม มาเลเซีย แคนาดา โปรตุเกส บราซิล เช็กเกีย อิสราเอล ออสเตรเลีย เกาะไอร์แลนด์ ปากีสถาน ตุรกี ชิลี ลักเซมเบิร์ก เอกวาดอร์ กรีซ ฟินแลนด์';

$lang['select_language']='กรุณาเลือกภาษา';
$lang['welcome'] = 'ในช่วง 14 วันก่อนหน้า ท่านมีประวัติเสี่ยงต่อการติดเชื้อ หรือไม่ดังนี้';
// $lang['message'] = 'Notre mission est de fournir des services de conception Web professionnels et très créatifs, ainsi que d’autres services connexes, à un large éventail de clients potentiels dans le monde entier.';
$lang['form_title']='กรุณาให้ข้อมูลตามความเป็นจริงเพื่อประโยชน์ของท่าน
การมาที่รพ.โดยไม่มีความจำเป็นอาจทำให้ท่านเสี่ยงการติดเชื้อมากขึ้น';
$lang['choice1']='ใช่';
$lang['choice2']='ไม่ใช่';

$lang['poll_save']='ส่งแบบประเมิน';

$lang['symptom']='ท่านมีอาการดังต่อไปนี้หรือไม่ ( หากไม่มีอาการไม่ต้องกดทำเครื่องหมายใดๆ )';

$lang['symptom1']='มีไข้';

$lang['symptom2']='ไอ';
$lang['symptom3']='มีน้ำมูก';
$lang['symptom4']='เจ็บคอ';
$lang['symptom5']='หายใจลำบาก';
$lang['symptom6']='หอบเหนื่อย';