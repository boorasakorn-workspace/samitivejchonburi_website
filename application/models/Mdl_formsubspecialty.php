<?php
	class Mdl_formsubspecialty extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
			$this->selfregisdb = $this->load->database('selfregis', TRUE);
		}

		function get_parent_table(){			
			$parent_table = "ms_service_location";
			return $parent_table;
		}
		function get_sub_parent_table(){	
			$sub_parent_table = "ms_specialty";
			return $sub_parent_table;
		}

		function get_table()
		{
			$table = "ms_specialist";
			return $table;
		}

		function get_parent($order_by)
		{
			$table = $this->get_parent_table();
			$this->selfregisdb->order_by($order_by);
			$query = $this->selfregisdb->get($table);
			return $query;
		}

		function get_name_parent($idparent)
		{
			$this->selfregisdb = $this->load->database('selfregis', TRUE);
			$this->selfregisdb->select('ms_service_location.description as medical_center_name');
			$this->selfregisdb->where('uid', $idparent);
			$query = $this->selfregisdb->get('ms_service_location');
			return $query;
			// $table = $this->get_parent_table();
			// $this->selfregisdb->select('medical_center_name');
			// $this->selfregisdb->where('uid', $idparent);
			// $query = $this->selfregisdb->get($table);
			// return $query;
		}

		function get_id_parent($parentname)
		{
			$this->selfregisdb = $this->load->database('selfregis', TRUE);
			$this->selfregisdb->select('uid as id');
			$this->selfregisdb->where('description', $parentname);
			$query = $this->selfregisdb->get('ms_service_location');
			return $query;
			// $table = $this->get_parent_table();
			// $this->selfregisdb->select('uid');
			// $this->selfregisdb->where('medical_center_name', $parentname);
			// $query = $this->selfregisdb->get($table);
			// return $query;
		}

		function get_name_subparent($idsubparent)
		{
			$this->selfregisdb = $this->load->database('selfregis', TRUE);
			$this->selfregisdb->select('ms_specialty.description as specialty_name_th');
			$this->selfregisdb->where('uid', $idsubparent);
			$query = $this->selfregisdb->get('ms_specialty');
			return $query;
			// $table = $this->get_sub_parent_table();
			// $this->selfregisdb->select('specialty_name_th');
			// $this->selfregisdb->where('uid', $idsubparent);
			// $query = $this->selfregisdb->get($table);
			// return $query;
		}

		function check_name_duplicate($namecheck)
		{
			$this->selfregisdb = $this->load->database('selfregis', TRUE);;
			$this->selfregisdb->where('description', $namecheck);
			$query = $this->selfregisdb->get('ms_specialty');
			return $query->num_rows();
			// $table = $this->get_table();
			// $this->selfregisdb->where('sub_specialty_name_th', $namecheck);
			// $query = $this->selfregisdb->get($table);
			// return $query->num_rows();
		}

		function get_id_subparent($subparentname)
		{
			$this->selfregisdb = $this->load->database('selfregis', TRUE);
			$this->selfregisdb->select('uid as id');
			$this->selfregisdb->where('description', $subparentname);
			$query = $this->selfregisdb->get('ms_specialty');
			return $query;
			// $table = $this->get_sub_parent_table();
			// $this->selfregisdb->select('uid');
			// $this->selfregisdb->where('specialty_name_th', $subparentname);
			// $query = $this->selfregisdb->get($table);
			// return $query;
		}

		function get_id_subsubparent($subname)
		{
			$this->selfregisdb = $this->load->database('selfregis', TRUE);
			$this->selfregisdb->select('uid as id');
			$this->selfregisdb->where('description', $subname);
			$query = $this->selfregisdb->get('ms_specialist');
			return $query;
			// $table = $this->get_table();
			// $this->selfregisdb->select('uid');
			// $this->selfregisdb->where('sub_specialty_name_th', $subname);
			// $query = $this->selfregisdb->get($table);
			// return $query;
		}

		function get_name_subsubparent($idsubsubparent)
		{
			$this->selfregisdb = $this->load->database('selfregis', TRUE);
			$this->selfregisdb->select('ms_specialist.description as sub_specialty_name_th');
			$this->selfregisdb->where('uid', $idsubsubparent);
			$query = $this->selfregisdb->get('ms_specialist');
			return $query;
			// $table = $this->get_table();
			// $this->selfregisdb->select('sub_specialty_name_th');
			// $this->selfregisdb->where('uid', $idsubsubparent);
			// $query = $this->selfregisdb->get($table);
			// return $query;
		}

		function get($order_by)
		{
			$table = $this->get_table();
			$this->selfregisdb->order_by($order_by);
			$query = $this->selfregisdb->get($table);
			return $query;
		}

		function get_with_limit($limit, $offset, $order_by)
		{
			$table = $this->get_table();
			$this->selfregisdb->limit($limit, $offset);
			$this->selfregisdb->order_by($order_by);
			$query = $this->selfregisdb->get($table);
			return $query;
		}

		function get_where($id)
		{
			$table = $this->get_table();
			$this->selfregisdb->where('uid', $id);
			$query = $this->selfregisdb->get($table);
			return $query;
		}

		function get_parent_sub($id)
		{
			$table = $this->get_table();
			$this->selfregisdb->where('parent_specialty_id', $id);
			$query = $this->selfregisdb->get($table);
			return $query;
		}

		function get_where_custom($col, $value)
		{
			$table = $this->get_table();
			$this->selfregisdb->where($col, $value);
			$query = $this->selfregisdb->get($table);
			return $query;
		}

		function _insert($data)
		{
			$table = $this->get_table();
			$this->selfregisdb->insert($table, $data);
		}

		function _update($id, $data)
		{
			$table = $this->get_table();
			$this->selfregisdb->where('uid', $id);
			$this->selfregisdb->update($table, $data);
		}

		function _delete($id)
		{
			$table = $this->get_table();
			$this->selfregisdb->where('uid', $id);
			$this->selfregisdb->delete($table);
		}

		function count_where($column, $value)
		{
			$table = $this->get_table();
			$this->selfregisdb->where($column, $value);
			$query = $this->selfregisdb->get($table);
			$num_rows = $query->num_rows();
			return $num_rows;
		}

		function count_all()
		{
			$table = $this->get_table();
			$query = $this->selfregisdb->get($table);
			$num_rows = $query->num_rows();
			return $num_rows;
		}

		function get_max()
		{
			$table = $this->get_table();
			$this->selfregisdb->select_max('uid');
			$query = $this->selfregisdb->get($table);
			$row = $query->row();
			$id = $row->id;
			return $id;
		}

		function _custom_query($mysql_query)
		{
			$query = $this->selfregisdb->query($mysql_query);
			return $query;
		}		
	}