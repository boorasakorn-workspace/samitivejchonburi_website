<?php 

class Covid_model extends CI_Model{

   public function get_question($lang){
     $this->db2 = $this->load->database('database2',TRUE);
     $this->db2->select('*');
     $this->db2->from('covid_survey');
     $this->db2->where('lang',$lang);
     $this->db2->order_by('uid','asc');
     $covid_survey=$this->db2->get();
     return $covid_survey->result();
  }
  public function get_province(){
     $this->db2 = $this->load->database('database2',TRUE);
     $this->db2->select('*');
     $this->db2->from('ms_provinces');
     $this->db2->order_by('PROVINCE_NAME','asc');
     $query=$this->db2->get();
     return $query->result();
  }

  public function save_data($data){
     $this->db2 = $this->load->database('database2',TRUE);
     $this->db2->insert('covid_ip', $data);
  }

  public function save_poller($data){

     $this->db2 = $this->load->database('database2',TRUE);
     $this->db2->insert('covid_poller', $data);

  }
  public function save_poller_question($data){

     $this->db2 = $this->load->database('database2',TRUE);
     $this->db2->insert('covid_poller_question', $data);

  }

  public function get_province_query($PROVINCE_ID)
  {
     $this->db2 = $this->load->database('database2',TRUE);
     $query = $this->db2->get_where('ms_amphures', array('PROVINCE_ID' => $PROVINCE_ID));
     $this->db2->order_by('PROVINCE_NAME=','asc');
     return $query->result();
  }


}