<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Front_Home_Model extends CI_Model
{
   public function __construct()
   {
      parent::__construct();
   }

   //Homepage
   public function get_slide_carousel()
   {
      $select_query = "SELECT * ";
      $from_query = "FROM tb_maincarousel ";
      $where_query = "WHERE carousel_status = 1 ";
      $after_query = "ORDER BY created_at ASC ";

      $mysql_query = $select_query . $from_query . $where_query . $after_query;
      $query = $this->db->query($mysql_query);

      return $query->result();
   }

   public function pre_package()
   {
      $ql = $this->session->userdata('q_l');

      $select_query =
         "SELECT package_title" . $ql . " AS package_title, 
      package_name" . $ql . " AS package_name, 
      package_description" . $ql . " AS package_description, 
      package_subdesc" . $ql . " AS package_subdesc, package_unitprice, 
      package_url, package_status,  package_big_img, package_small_img, 
      id, c_user, m_user, created_at,	updated_at, 
      meta_author, meta_description, meta_keywords, 
      package_startdate, package_enddate ";
      $from_query = "FROM tb_package ";
      $where_query =
         "WHERE package_status = 1 
      AND package_startdate <= '" . date('Y-m-d') . "' 
      AND package_enddate >= '" . date('Y-m-d') . "' ";
      $after_query = "ORDER BY id DESC LIMIT 4 ";

      $mysql_query = $select_query . $from_query . $where_query . $after_query;
      $query = $this->db->query($mysql_query);

      return $query->result();
   }

   public function pre_medicial_center()
   {
		$this->selfregisdb = $this->load->database('selfregis', TRUE);
		$query = $this->selfregisdb->select('*,uid as id,description as medical_center_name')->where('status',0)->get('ms_service_location');
      return $query->result();
      
   //    $ql = $this->session->userdata('q_l');

   //    $select_query =
   //       "SELECT medical_center_name" . $ql . " as medical_center_name, 
   //  id, medical_master_id, medical_center_icon, medical_center_url, medical_center_details, medical_center_contents, 
   //  medical_center_location, medical_center_office_daystxt, medical_center_office_hours, 
   //  medical_center_phone, medical_center_email, medical_center_meta_keywords, medical_center_meta_description, 
   //  c_user, m_user, order_priority, medical_center_is, created_at,updated_at ";
   //    $from_query = "FROM tb_medical_center ";
   //    $where_query =
   //       "WHERE status = 1 ";
   //    $after_query = " ";
   //    //$after_query = "LIMIT 5 ";

   //    $mysql_query = $select_query . $from_query . $where_query . $after_query;
   //    $query = $this->db->query($mysql_query);

      return $query->result();
   }

   public function pre_bloghealthy()
   {
      $ql = $this->session->userdata('q_l');
      $ql_doc = $ql;
      if ($ql_doc == ' ') {
         $ql_doc = '_th';
      }

      $select_query =
         "SELECT tb_bloghealthy.blog_title" . $ql . " as blog_title, 
   tb_bloghealthy.blog_description" . $ql . " as blog_description, 
   tb_bloghealthy.blog_contents" . $ql . " as blog_contents, 
   tb_bloghealthy.id, tb_bloghealthy.blog_url, tb_bloghealthy.blog_keywords, 
   tb_bloghealthy.blog_status, tb_bloghealthy.date_published, 
   tb_bloghealthy.c_user, tb_bloghealthy.m_user, tb_bloghealthy.created_at, tb_bloghealthy.updated_at, 
   tb_bloghealthy.meta_author, tb_bloghealthy.meta_description, tb_bloghealthy.meta_keywords, 
   tb_bloghealthy.blog_big_img, tb_bloghealthy.blog_small_img, 
   tb_bloghealthy.promotion_link_1, tb_bloghealthy.promotion_link_2, tb_bloghealthy.doc_id, 
   CONCAT(tb_doctor.pname" . $ql_doc . ",tb_doctor.firstname" . $ql_doc . ",' ',tb_doctor.lastname" . $ql_doc . ") as doctor_name ";
      $from_query = "FROM tb_bloghealthy ";
      $join_query = "LEFT JOIN tb_doctor ";
      $join_on_query = "ON tb_doctor.id = tb_bloghealthy.doc_id ";
      $where_query = "WHERE blog_status = 1 AND blog_big_img != '' ";
      $after_query = "ORDER BY id DESC LIMIT 6";

      $mysql_query = $select_query . $from_query . $join_query . $join_on_query . $where_query . $after_query;
      $query = $this->db->query($mysql_query);

      return $query->result();
   }

   public function pre_doctor()
   {
		$ql = $this->session->userdata('q_l');
		$this->selfregisdb = $this->load->database('selfregis', TRUE);
		$this->selfregisdb->select('ms_care.doctor_img as doctor_imgs');
		$this->selfregisdb->select('ms_care.uid');
		$this->selfregisdb->select('ms_care.medical_id as id');				
		$this->selfregisdb->select('ms_title.titlename as pname_th');	
		$this->selfregisdb->select("ms_care.forename$ql as firstname_th");	
		$this->selfregisdb->select("ms_care.surname$ql as lastname_th");
		$this->selfregisdb->select("ms_care.parent_medical_center_id$ql as parent_medical_center_id");
		$this->selfregisdb->join('ms_title','ms_care.ms_title_uid = ms_title.uid','left');
		$this->selfregisdb->where('ms_care.status',0);
		$this->selfregisdb->where('ms_care.doctor_img !=',"''",FALSE);
		$this->selfregisdb->where('medical_id is NOT NULL',NULL,FALSE);
		$this->selfregisdb->limit(4);
		$selfregisdoctor = $this->selfregisdb->get('ms_care');
      return $selfregisdoctor->result();
      
      $ql = $this->session->userdata('q_l');
      if ($ql == ' ') {
         $ql = '_th';
      }

      $select_query =
         "SELECT pname" . $ql . " as pname_th, firstname" . $ql . " as firstname_th, lastname" . $ql . " as lastname_th, 
education" . $ql . " as education_th, experience" . $ql . " as experience_th, type" . $ql . " as type_th, language" . $ql . ", 
id, medical_id, medical_master_id, phone, email, gender, at_hospital, status, doctor_imgs, 
created_at, updated_at, doctor_code, location_code, location_name, 
parent_medical_center_id, parent_specialty_id, parent_sub_specialty_id ";
      $from_query = "FROM tb_doctor ";
      $where_query =
         "WHERE status = 1 AND doctor_imgs != '' ";
      $after_query = "LIMIT 4 ";

      $mysql_query = $select_query . $from_query . $where_query . $after_query;
      $query = $this->db->query($mysql_query);

      return $query->result();
   }

   public function pre_news()
   {
      $ql = $this->session->userdata('q_l');

      $select_query =
         "SELECT new_activity_name" . $ql . " as new_activity_name, 
 description" . $ql . " as description, new_activity_big_img, new_activity_small_img, 
 new_activity_id, cuser, cwhen, mwhen, 
 meta_author, meta_description, meta_keywords, 
 tb_account.* ";
      $from_query = "FROM new_activity ";
      $join_query = "INNER JOIN tb_account ";
      $join_on_query = "ON tb_account.acc_id = new_activity.cuser ";
      $where_query = "WHERE new_activity_status = 1 AND new_activity_big_img != '' ";
      $after_query = "ORDER BY new_activity_id DESC LIMIT 5";

      $mysql_query = $select_query . $from_query . $join_query . $join_on_query . $where_query . $after_query;
      $query = $this->db->query($mysql_query);

      return $query->result();
   }
   //End_Homepage

   public function all_promotion()
   {
      $ql = $this->session->userdata('q_l');

      $select_query =
         "SELECT package_title" . $ql . " AS package_title, 
 package_name" . $ql . " AS package_name, 
 package_description" . $ql . " AS package_description, 
 package_subdesc" . $ql . " AS package_subdesc, package_unitprice, 
 package_url, package_status,  package_big_img, package_small_img, 
 id, c_user, m_user, created_at,	updated_at, 
 meta_author, meta_description, meta_keywords, 
 package_startdate, package_enddate ";
      $from_query = "FROM tb_package ";
      $where_query =
         "WHERE package_status = 1 
 AND package_startdate <= '" . date('Y-m-d') . "' 
 AND package_enddate >= '" . date('Y-m-d') . "' ";
      $after_query = "ORDER BY id DESC ";

      $mysql_query = $select_query . $from_query . $where_query . $after_query;
      $query = $this->db->query($mysql_query);

      return $query;
   }

   public function id_promotion($update_id)
   {
      $ql = $this->session->userdata('q_l');

      $select_query =
         "SELECT package_title" . $ql . " AS package_title, 
 package_name" . $ql . " AS package_name, 
 package_description" . $ql . " AS package_description, 
 package_subdesc" . $ql . " AS package_subdesc, package_unitprice, 
 package_url, package_status,  package_big_img, package_small_img, 
 id, c_user, m_user, created_at,	updated_at, 
 meta_author, meta_description, meta_keywords, 
 package_startdate, package_enddate, relate_medical_center_id  ";
      $from_query = "FROM tb_package ";
      $where_query =
         "WHERE id = '" . $update_id . "' 
 AND package_status = 1 
 AND package_startdate <= '" . date('Y-m-d') . "' 
 AND package_enddate >= '" . date('Y-m-d') . "' ";
      $after_query = "ORDER BY id DESC ";

      $mysql_query = $select_query . $from_query . $where_query . $after_query;
      $query = $this->db->query($mysql_query);

      return $query->row();
   }

   public function get_related_doctor_id($update_id)
   {
      $ql = $this->session->userdata('q_l');
      if ($ql == ' ') {
         $ql = '_th';
      }

      $select_query =
         "SELECT pname" . $ql . " as pname_th, firstname" . $ql . " as firstname_th, lastname" . $ql . " as lastname_th, 
education" . $ql . " as education_th, experience" . $ql . " as experience_th, type" . $ql . " as type_th, language" . $ql . ", 
doctor_description" . $ql . " as doctor_description,  
id, doctor_url, medical_id, medical_master_id, phone, email, gender, at_hospital, status, doctor_imgs, 
created_at, updated_at, 
doctor_code, location_code, location_name, parent_medical_center_id, parent_specialty_id, parent_sub_specialty_id ";
      $from_query = "FROM tb_doctor ";
      $where_query =
         "WHERE status = 1 
AND (parent_medical_center_id = '" . $update_id . "' OR parent_medical_center_id LIKE '%|" . $update_id . "%' )";
      $after_query = " ";

      $mysql_query = $select_query . $from_query . $where_query . $after_query;
      $query = $this->db->query($mysql_query);

      return $query->result();
   }

   public function all_bloghealthy($id = null)
   {
      $ql = $this->session->userdata('q_l');

      if ($id == null || $id == "") {
         $select_query =
            "SELECT blog_title" . $ql . " as blog_title, 
   blog_description" . $ql . " as blog_description, 
   blog_contents" . $ql . " as blog_contents, 
   id, blog_url, blog_keywords, blog_status, date_published, 
   c_user, m_user, created_at, updated_at, 
   meta_author, meta_description, meta_keywords, blog_big_img, blog_small_img, 
   promotion_link_1,promotion_link_2,doc_id ";
         $from_query = "FROM tb_bloghealthy ";
         $where_query = "WHERE blog_status = 1 ";
         $after_query = "ORDER BY id DESC ";

         $mysql_query = $select_query . $from_query . $where_query . $after_query;
         $query = $this->db->query($mysql_query);
         return $query;
      } else {
         $select_query =
            "SELECT blog_title" . $ql . " as blog_title, 
   blog_description" . $ql . " as blog_description, 
   blog_contents" . $ql . " as blog_contents, 
   id, blog_url, blog_keywords, blog_status, date_published, 
   c_user, m_user, created_at, updated_at, 
   meta_author, meta_description, meta_keywords, blog_big_img, blog_small_img, 
   promotion_link_1,promotion_link_2,doc_id ";
         $from_query = "FROM tb_bloghealthy ";
         $where_query = "WHERE blog_status = 1 and doc_id=$id ";
         $after_query = "ORDER BY id DESC ";

         $mysql_query = $select_query . $from_query . $where_query . $after_query;
         $query = $this->db->query($mysql_query);
         return $query;
      }
   }

   public function id_bloghealthy($update_id)
   {
      $ql = $this->session->userdata('q_l');

      $select_query =
         "SELECT blog_title" . $ql . " as blog_title, 
 blog_description" . $ql . " as blog_description, 
 blog_contents" . $ql . " as blog_contents, 
 id, blog_url, blog_keywords, blog_status, date_published, 
 c_user, m_user, created_at, updated_at, 
 blog_big_img, blog_small_img, blog_video, 
 meta_author, meta_description, meta_keywords, 
 promotion_link_1, promotion_link_2, doc_id ";
      $from_query = "FROM tb_bloghealthy ";
      $where_query =
         "WHERE id = '" . $update_id . "' 
 AND blog_status = 1 ";
      $after_query = " ";

      $mysql_query = $select_query . $from_query . $where_query . $after_query;
      $query = $this->db->query($mysql_query);

      return $query->row();
   }

   public function all_doctor()
   {
      $ql = $this->session->userdata('q_l');
      if ($ql == ' ') {
         $ql = '_th';
      }

      $select_query =
         "SELECT pname" . $ql . " as pname_th, firstname" . $ql . " as firstname_th, lastname" . $ql . " as lastname_th, 
education" . $ql . " as education_th, experience" . $ql . " as experience_th, type" . $ql . " as type_th, language" . $ql . ", 
id, medical_id, medical_master_id, phone, email, gender, at_hospital, status, doctor_imgs, 
created_at, updated_at, 
doctor_code, location_code, location_name, parent_medical_center_id, parent_specialty_id, parent_sub_specialty_id ";
      $from_query = "FROM tb_doctor ";
      $where_query = "WHERE status = 1 ";
      $after_query = "ORDER BY doctor_imgs DESC ";

      $mysql_query = $select_query . $from_query . $where_query . $after_query;
      $query = $this->db->query($mysql_query);

      return $query->result();
   }


   // public function get_blog_doctor($id){
   //  $ql = $this->session->userdata('q_l');
   //    $querystr = "blog_title".$ql." as blog_title, 
   //  blog_description".$ql." as blog_description, 
   //  blog_contents".$ql." as blog_contents, 
   //  id, blog_url, blog_keywords, blog_status, date_published, 
   //  c_user, m_user, created_at, updated_at, 
   //  meta_author, meta_description, meta_keywords, blog_big_img, blog_small_img, 
   //  promotion_link_1,promotion_link_2,doc_id ";
   //    $this->db->select($querystr,false);
   //    $this->db->from('tb_bloghealthy');
   //    $this->db->where('doc_id',$id);
   //    $query = $this->db->get();
   //    return $query->result();

   // }



   public function doctor_related_medcenter($id)
   {
      $this->selfregisdb = $this->load->database('selfregis', TRUE);
      $this->selfregisdb->select('ms_care.*');
      $this->selfregisdb->select('ms_title.titlename as pname_th');
      $this->selfregisdb->select('ms_care.forename as firstname_th');
      $this->selfregisdb->select('ms_care.surname as lastname_th');
      $this->selfregisdb->join('ms_title','ms_care.ms_title_uid = ms_title.uid','left');
      $this->selfregisdb->where('ms_care.status',0);
      $this->selfregisdb->where("
      ( parent_medical_center_id = '" . $id . "' 
      OR parent_medical_center_id LIKE '%|" . $id . "' 
      OR parent_medical_center_id LIKE '%|" . $id . "|%' ) ",NULL,FALSE);
      $query = $this->selfregisdb->get('ms_care');
      return $query->result();

      $ql = $this->session->userdata('q_l');
      if ($ql == ' ') {
         $ql = '_th';
      }

      $select_query =
         "SELECT pname" . $ql . " as pname_th, firstname" . $ql . " as firstname_th, lastname" . $ql . " as lastname_th, 
education" . $ql . " as education_th, experience" . $ql . " as experience_th, type" . $ql . " as type_th, language" . $ql . ", 
id, medical_id, medical_master_id, phone, email, gender, at_hospital, status, doctor_imgs, 
created_at, updated_at, 
doctor_code, location_code, location_name, parent_medical_center_id, parent_specialty_id, parent_sub_specialty_id ";
      $from_query = "FROM tb_doctor ";
      $where_query = "WHERE status = 1 
AND ( parent_medical_center_id = '" . $id . "' 
OR parent_medical_center_id LIKE '%|" . $id . "' 
OR parent_medical_center_id LIKE '%|" . $id . "|%' ) ";
      $after_query = "ORDER BY id DESC ";

      $mysql_query = $select_query . $from_query . $where_query . $after_query;
      $query = $this->db->query($mysql_query);

      return $query->result();
   }

   public function id_doctor($update_id)
   {
      $ql = $this->session->userdata('q_l');
      if ($ql == ' ') {
         $ql = '_th';
      }

      $select_query =
         "SELECT pname" . $ql . " as pname_th, firstname" . $ql . " as firstname_th, lastname" . $ql . " as lastname_th, 
education" . $ql . " as education_th, experience" . $ql . " as experience_th, type" . $ql . " as type_th, language" . $ql . ", 
doctor_description" . $ql . " as doctor_description,  
id, medical_id, medical_master_id, phone, email, gender, at_hospital, status, doctor_imgs, doctor_preview_imgs, 
created_at, updated_at, 
doctor_code, location_code, location_name, parent_medical_center_id, parent_specialty_id, parent_sub_specialty_id,doctor_video ";
      $from_query = "FROM tb_doctor ";
      $where_query = "WHERE id = '" . $update_id . "' 
AND status = 1 ";
      $after_query = " ";

      $mysql_query = $select_query . $from_query . $where_query . $after_query;
      $query = $this->db->query($mysql_query);

      return $query->row();
   }

   public function id_doctor_workdayhour($update_id)
   {
      $ql = $this->session->userdata('q_l');
      if ($ql == ' ') {
         $ql = '_th';
      }

      $select_query =
         "SELECT  * ";
      $from_query = "FROM tb_workdayhours ";
      $where_query = "WHERE medical_id = '" . $update_id . "' ";
      $after_query = " ";

      $mysql_query = $select_query . $from_query . $where_query . $after_query;
      $query = $this->db->query($mysql_query);

      return $query->result();
   }

   public function id_doctor_bloghealthy($doc_id)
   {
      $ql = $this->session->userdata('q_l');

      $select_query =
         "SELECT blog_title" . $ql . " as blog_title, blog_description" . $ql . " as blog_description, blog_contents" . $ql . " as blog_contents,  
 id, blog_url, blog_keywords, blog_status, date_published, 
 c_user, m_user, created_at, updated_at, 
 meta_author, meta_description, meta_keywords, 
 blog_big_img, blog_small_img, 
 promotion_link_1, promotion_link_2, doc_id ";
      $from_query = "FROM tb_bloghealthy ";
      $where_query = "WHERE doc_id = '" . $doc_id . "'
 AND blog_status = '1' 
 AND blog_big_img != '' ";
      $after_query = "ORDER BY date_published DESC";

      $mysql_query = $select_query . $from_query . $where_query . $after_query;
      $query = $this->db->query($mysql_query);

      return $query->result();
   }

   public function all_news()
   {
      $ql = $this->session->userdata('q_l');

      $select_query =
         "SELECT new_activity_name" . $ql . " AS new_activity_name, 
 description" . $ql . " AS description, 
 new_activity_big_img, new_activity_small_img, 
 new_activity_id, cuser, cwhen, mwhen, 
 meta_author, meta_description, meta_keywords,
 tb_account.* ";
      $from_query = "FROM new_activity ";
      $join_query = "LEFT JOIN tb_account ";
      $join_on_query = "ON tb_account.acc_id = new_activity.cuser ";
      $where_query = "WHERE new_activity_status = 1 ";
      $after_query = " ";

      $mysql_query = $select_query . $from_query . $join_query . $join_on_query . $where_query . $after_query;
      $query = $this->db->query($mysql_query);

      return $query->result();
   }

   public function id_news($update_id)
   {
      $ql = $this->session->userdata('q_l');

      $select_query =
         "SELECT new_activity_name" . $ql . " AS new_activity_name, 
 description" . $ql . " AS description, news_video, 
 new_activity_big_img, new_activity_small_img, 
 new_activity_id, cuser, cwhen, mwhen, 
 meta_author, meta_description, meta_keywords,
 tb_account.* ";
      $from_query = "FROM new_activity ";
      $join_query = "LEFT JOIN tb_account ";
      $join_on_query = "ON tb_account.acc_id = new_activity.cuser ";
      $where_query = "WHERE new_activity_id = '" . $update_id . "' 
 AND new_activity_status = 1 ";
      $after_query = " ";

      $mysql_query = $select_query . $from_query . $join_query . $join_on_query . $where_query . $after_query;
      $query = $this->db->query($mysql_query);

      return $query->row();
   }

   public function id_medicial_center($update_id)
   {
      $this->selfregisdb = $this->load->database('selfregis', TRUE);
      $this->selfregisdb->select('*');
      $this->selfregisdb->select('description as medical_center_name');
      $this->selfregisdb->where('uid', $update_id);
      $this->selfregisdb->where('status', 0);
      $query = $this->selfregisdb->get('ms_service_location');
      return $query->row();





      $ql = $this->session->userdata('q_l');

      $select_query =
         "SELECT medical_center_name" . $ql . " as medical_center_name, 
 id, medical_master_id, medical_center_imgs, medical_center_thumb_imgs, medical_center_icon, 
 medical_center_url, medical_center_details, medical_center_contents, medical_center_location, 
 medical_center_office_daystxt, medical_center_office_hours, medical_center_phone, medical_center_email, 
 medical_center_meta_keywords, medical_center_meta_description, order_priority, medical_center_is, 
 c_user, m_user, created_at,updated_at  ";
      $from_query = "FROM tb_medical_center ";
      $where_query =
         "WHERE id = '" . $update_id . "' 
 AND status = 1 ";
      $after_query = " ";

      $mysql_query = $select_query . $from_query . $where_query . $after_query;
      $query = $this->db->query($mysql_query);

      return $query->row();
   }


   public function sub_child($id)
   {
      $this->db->select('*');
      $this->db->from('tb_sub_child');
      $this->db->join('tb_medical_center', 'tb_medical_center.id=tb_sub_child.medical_center_id');
      // $this->db->join('tb_medicial_center','tb_sub_child.medical_center_id=tb_medicial_center.id','left');
      $this->db->where('tb_sub_child.child_id', $id);
      $query = $this->db->get();
      return $query->result_array();
   }





   public function id_nav($update_id)
   {

      $select_query =
         "SELECT * ";
      $from_query = "FROM tb_navigation ";
      $where_query = "WHERE id = '" . $update_id . "' 
 AND nav_status = 1 ";
      $after_query = " ";

      $mysql_query = $select_query . $from_query . $where_query . $after_query;
      $query = $this->db->query($mysql_query);

      return $query->row();
   }

   public function id_subnav($update_id)
   {

      $select_query =
         "SELECT * ";
      $from_query = "FROM tb_subnavigation ";
      $where_query = "WHERE id = '" . $update_id . "' 
 AND sub_nav_status = 1 ";
      $after_query = " ";

      $mysql_query = $select_query . $from_query . $where_query . $after_query;
      $query = $this->db->query($mysql_query);

      return $query->row();
   }

   public function id_sidebar($update_id)
   {

      $select_query =
         "SELECT * ";
      $from_query = "FROM tb_sidemenu ";
      $where_query = "WHERE id = '" . $update_id . "' 
 AND sidemenu_status = 1 ";
      $after_query = " ";

      $mysql_query = $select_query . $from_query . $where_query . $after_query;
      $query = $this->db->query($mysql_query);

      return $query->row();
   }

   public function id_subsidebar($update_id)
   {

      $select_query =
         "SELECT * ";
      $from_query = "FROM tb_subofsidemenu ";
      $where_query = "WHERE id = '" . $update_id . "' 
 AND sub_sidemenu_status = 1 ";
      $after_query = " ";

      $mysql_query = $select_query . $from_query . $where_query . $after_query;
      $query = $this->db->query($mysql_query);

      return $query->row();
   }

   public function id_sub_subsidebar($update_id)
   {

      $select_query =
         "SELECT * ";
      $from_query = "FROM tb_subofsubmenu ";
      $where_query = "WHERE id = '" . $update_id . "' 
 AND subofsubmenu_status = 1 ";
      $after_query = " ";

      $mysql_query = $select_query . $from_query . $where_query . $after_query;
      $query = $this->db->query($mysql_query);

      return $query->row();
   }

   public function lang_all_medicial_center()
   {
      $this->selfregisdb = $this->load->database('selfregis', TRUE);
      $this->selfregisdb->select('*');
      $this->selfregisdb->select('uid as id');
      $this->selfregisdb->select('description as medical_center_name');
      $this->selfregisdb->where('status', 0);
      $query = $this->selfregisdb->get('ms_service_location');
      return $query->result();


      $ql = $this->session->userdata('q_l');
      $query = $this->db->query("SELECT * , medical_center_name" . $ql . " as medical_center_name FROM tb_medical_center where status='1' ");
      return $query->result();
   }
   public function lang_all_specialty()
   {
      $ql = $this->session->userdata('q_l');
      if ($ql == ' ') {
         $ql = '_th';
      }
      $query = $this->db->query("SELECT * , specialty_name" . $ql . " as specialty_name FROM tb_specialty WHERE status='1' ");
      return $query->result();
   }
   public function lang_all_sub_specialty()
   {
      $ql = $this->session->userdata('q_l');
      if ($ql == ' ') {
         $ql = '_th';
      }
      $query = $this->db->query("SELECT * , sub_specialty_name" . $ql . " as sub_specialty_name FROM tb_sub_specialty WHERE status='1' ");
      return $query->result();
   }

   public function all_medicial_center()
   {
      $ql = $this->session->userdata('q_l');
      $query = $this->db->query("SELECT * FROM tb_medical_center where status='1' ");
      return $query->result();
   }

   public function all_specialty()
   {
      $ql = $this->session->userdata('q_l');
      if ($ql == ' ') {
         $ql = '_th';
      }
      $query = $this->db->query("SELECT * , specialty_name" . $ql . " as specialty_name FROM tb_specialty WHERE status='1' ");
      return $query->result();
   }

   public function all_sub_specialty()
   {
      $query = $this->db->query("SELECT * FROM tb_sub_specialty WHERE status='1' ");
      return $query->result();
   }

   function _insert_email($data)
   {
      $table = "tb_email";
      $this->db->insert($table, $data);
   }
}
