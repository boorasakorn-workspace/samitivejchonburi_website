<?php

	class Mdl_fetch extends CI_Model
	  {
	  
	    function __construct()
	    {
	        parent::__construct();
	    }

		public function fetch_carousel()
		{
			$mysql_query = "select * from tb_maincarousel where carousel_status=1 order by created_at asc";
			$query = $this->db->query($mysql_query);

			return $query;
		}


		public function fetch_header()
		{
			$mysql_query = "select * from tb_header where header_status=1";
			$query = $this->db->query($mysql_query)->row();

			return $query;
		}

		public function fetch_nav()
		{
			$mysql_query = "select nav_title,nav_url,id
							from tb_navigation
							where nav_status=1
							order by order_priority asc
							";
			$query = $this->db->query($mysql_query);

			return $query;
		}

		public function fetch_subnav()
		{
			$mysql_query = "select tb_subnavigation.sub_nav_title,tb_subnavigation.sub_nav_url,tb_subnavigation.parent_nav_id,
							tb_navigation.id
							from tb_subnavigation
							inner join tb_navigation
							on tb_subnavigation.parent_nav_id=tb_navigation.id
							where tb_subnavigation.sub_nav_status=1
							and tb_subnavigation.parent_nav_id=tb_navigation.id
							order by tb_subnavigation.order_priority asc
							";
			$query = $this->db->query($mysql_query);

			return $query;
		}

		/*public function fetch_package()
		{
			$this->db->select('*');
			$this->db->from('tb_package');
			$this->db->order_by('package_startdate', 'asc');
			$this->db->limit(3);
			$query = $this->db->get();

			return $query;
		}*/

		public function fetch_healthy()
		{
			$this->db->select('*');
			$this->db->from('tb_bloghealthy');
			$this->db->order_by('created_at', 'desc');
			$this->db->limit(6);
			$query = $this->db->get()->result();

			return $query;
		}

		public function fetch_package()
		{
			$this->db->select('*');
			$this->db->from('tb_package');
			$this->db->where('package_status', 1);
			$this->db->order_by('package_startdate', 'asc');
			$this->db->limit(3);
			$query = $this->db->get()->result();

			return $query;
		}
	}