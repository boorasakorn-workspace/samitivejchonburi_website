<?php
	class Mdl_formsidebar extends CI_Model
	{
		private $subnavigation = 'tb_subofsidemenu';
		private $subofsubnavigation = 'tb_subofsubmenu';

		function __construct()
		{
			parent::__construct();
		}


		function get_table()
		{
			$table = "tb_sidemenu";
			return $table;
		}

		function get_subsubnav($order_by)
		{
			$this->db->order_by($order_by);
			$query = $this->db->get($this->subofsubnavigation);
			return $query;
		}

		function get_subnav($order_by)
		{
			$this->db->order_by($order_by);
			$query = $this->db->get($this->subnavigation);
			return $query;
		}

		function get($order_by)
		{
			$table = $this->get_table();
			$this->db->order_by($order_by);
			$query = $this->db->get($table);
			return $query;
		}

		function get_with_limit($limit, $offset, $order_by)
		{
			$table = $this->get_table();
			$this->db->limit($limit, $offset);
			$this->db->order_by($order_by);
			$query = $this->db->get($table);
			return $query;
		}

		function get_where_subsubnav($id)
		{
			$table = $this->db->where('id', $id);
			$query = $this->db->get($this->subofsubnavigation);
			return $query;
		}

		function get_where_subnav($id)
		{
			$table = $this->db->where('id', $id);
			$query = $this->db->get($this->subnavigation);
			return $query;
		}

		function get_where($id)
		{
			$table = $this->get_table();
			$this->db->where('id', $id);
			$query = $this->db->get($table);
			return $query;
		}

		function get_where_custom_subsubnav($col, $value)
		{
			$this->db->where($col, $value);
			$query = $this->db->get($this->subofsubnavigation);
			return $query;
		}

		function get_where_custom_subnav($col, $value)
		{
			$this->db->where($col, $value);
			$query = $this->db->get($this->subnavigation);
			return $query;
		}

		function get_where_custom($col, $value)
		{
			$table = $this->get_table();
			$this->db->where($col, $value);
			$query = $this->db->get($table);
			return $query;
		}

		function _insert_subsubnav($data)
		{
			$table = $this->get_table();
			$this->db->insert($this->subofsubnavigation, $data);
		}

		function _insert_subnav($data)
		{
			$table = $this->get_table();
			$this->db->insert($this->subnavigation, $data);
		}

		function _insert($data)
		{
			$table = $this->get_table();
			$this->db->insert($table, $data);
		}

		function _update_subsubnav($id, $data)
		{
			$this->db->where('id', $id);
			$this->db->update($this->subofsubnavigation, $data);
		}

		function _update_subnav($id, $data)
		{
			$this->db->where('id', $id);
			$this->db->update($this->subnavigation, $data);
		}

		function _update($id, $data)
		{
			$table = $this->get_table();
			$this->db->where('id', $id);
			$this->db->update($table, $data);
		}

		function _delete_subsubnav($id)
		{
			$this->db->where('id', $id);
			$this->db->delete($this->subofsubnavigation);
		}

		function _delete_subnav($id)
		{
			$this->db->where('id', $id);
			$this->db->delete($this->subnavigation);
		}

		function _delete($id)
		{
			$table = $this->get_table();
			$this->db->where('id', $id);
			$this->db->delete($table);
		}

		function count_where($column, $value)
		{
			$table = $this->get_table();
			$this->db->where($column, $value);
			$query = $this->db->get($table);
			$num_rows = $query->num_rows();
			return $num_rows;
		}

		function count_all()
		{
			$table = $this->get_table();
			$query = $this->db->get($table);
			$num_rows = $query->num_rows();
			return $num_rows;
		}

		function get_max_subsubnav()
		{
			$this->db->select_max('id');
			$query = $this->db->get($this->subofsubnavigation);
			$row = $query->row();
			$id = $row->id;
			return $id;
		}

		function get_max_subnav()
		{
			$this->db->select_max('id');
			$query = $this->db->get($this->subnavigation);
			$row = $query->row();
			$id = $row->id;
			return $id;
		}

		function get_max()
		{
			$table = $this->get_table();
			$this->db->select_max('id');
			$query = $this->db->get($table);
			$row = $query->row();
			$id = $row->id;
			return $id;
		}

		function _custom_query($mysql_query)
		{
			$query = $this->db->query($mysql_query);
			return $query;
		}
	}