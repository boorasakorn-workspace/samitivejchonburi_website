<?php
	class Mdl_formspecialty extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
			$this->selfregisdb = $this->load->database('selfregis', TRUE);
		}

		function get_parent_table(){			
			$parent_table = "ms_service_location";
			return $parent_table;
		}

		function get_table()
		{
			$parent_table = "ms_specialty";
			$table = "ms_specialty";
			return $table;
		}

		function check_name_duplicate($namecheck)
		{
			$table = $this->get_table();
			$this->selfregisdb->where('description', $namecheck);
			$query = $this->selfregisdb->get($table);
			return $query->num_rows();
		}

		function get_id_parent($parentname)
		{
			$table = $this->get_parent_table();
			$this->selfregisdb->select('uid');
			$this->selfregisdb->where('description', $parentname);
			$query = $this->selfregisdb->get($table);
			return $query;
		}

		function get($order_by)
		{
			$table = $this->get_table();
			$this->selfregisdb->order_by($order_by);
			$query = $this->selfregisdb->get($table);
			return $query;
		}

		function get_with_limit($limit, $offset, $order_by)
		{
			$table = $this->get_table();
			$this->selfregisdb->limit($limit, $offset);
			$this->selfregisdb->order_by($order_by);
			$query = $this->selfregisdb->get($table);
			return $query;
		}

		function get_where($id)
		{
			$table = $this->get_table();
			$this->selfregisdb->where('uid', $id);
			$query = $this->selfregisdb->get($table);
			return $query;
		}

		function get_parent_sub($id)
		{
			$table = $this->get_table();
			$this->selfregisdb->where('parent_id', $id);
			$query = $this->selfregisdb->get($table);
			return $query;
		}


		function get_where_custom($col, $value)
		{
			$table = $this->get_table();
			$this->selfregisdb->where($col, $value);
			$query = $this->selfregisdb->get($table);
			return $query;
		}

		function _insert($data)
		{
			$table = $this->get_table();
			$this->selfregisdb->insert($table, $data);
		}

		function _update($id, $data)
		{
			$table = $this->get_table();
			$this->selfregisdb->where('uid', $id);
			$this->selfregisdb->update($table, $data);
		}

		function _delete($id)
		{
			$table = $this->get_table();
			$this->selfregisdb->where('uid', $id);
			$this->selfregisdb->delete($table);
		}

		function count_where($column, $value)
		{
			$table = $this->get_table();
			$this->selfregisdb->where($column, $value);
			$query = $this->selfregisdb->get($table);
			$num_rows = $query->num_rows();
			return $num_rows;
		}

		function count_all()
		{
			$table = $this->get_table();
			$query = $this->selfregisdb->get($table);
			$num_rows = $query->num_rows();
			return $num_rows;
		}

		function get_max()
		{
			$table = $this->get_table();
			$this->selfregisdb->select_max('uid');
			$query = $this->selfregisdb->get($table);
			$row = $query->row();
			$id = $row->id;
			return $id;
		}

		function _custom_query($mysql_query)
		{
			$query = $this->selfregisdb->query($mysql_query);
			return $query;
		}		
	}