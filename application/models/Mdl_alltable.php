<?php
	class Mdl_alltable extends CI_Model
	{
		function __construct()
		{
			parent::__construct();
		}

		function get_all_table_name()
		{
			$query = $this->db->query("SELECT * FROM information_schema.tables WHERE table_schema='samitivej'
			")->result();
			return $query;
		}

		function get_all_content_page()
		{			
			/*$query = $this->get_page_nav();
			$i = count($query);*/
			$i=0;
			//Add TOP Nav			
			$query[$i]['id']='';
			$query[$i]['url']='';
			$query[$i]['title']='เมนู';
			$query[$i]['name']='เมนู';
			$query[$i]['path']='';
			$i++;
			foreach ($this->get_page_nav() as $data) {
				$query[$i]['id']=$data->id;
				$query[$i]['url']=$data->url;
				$query[$i]['title']=$data->title;
				$query[$i]['name']=$data->name;
				$query[$i]['path']=$data->path;
				$i++;
			}
			//Add TOP SubNav			
			$query[$i]['id']='';
			$query[$i]['url']='';
			$query[$i]['title']='เมนูย่อย';
			$query[$i]['name']='เมนูย่อย';
			$query[$i]['path']='';
			$i++;
			foreach ($this->get_page_sub_nav() as $data) {
				$query[$i]['id']=$data->id;
				$query[$i]['url']=$data->url;
				$query[$i]['title']=$data->title;
				$query[$i]['name']=$data->name;
				$query[$i]['path']=$data->path;
				$i++;
			}
			//Add TOP BlogHealthy			
			$query[$i]['id']='';
			$query[$i]['url']='';
			$query[$i]['title']='บทความสุขภาพ';
			$query[$i]['name']='บทความสุขภาพ';
			$query[$i]['path']='frontend/Home/HealthyBlog';
			$i++;
			foreach ($this->get_page_bloghealthy() as $data) {
				$query[$i]['id']=$data->id;
				$query[$i]['url']=$data->url;
				$query[$i]['title']=$data->title;
				$query[$i]['name']=$data->name;
				$query[$i]['path']=$data->path;
				$i++;
			}
			//Add TOP Package			
			$query[$i]['id']='';
			$query[$i]['url']='';
			$query[$i]['title']='แพ็กเกจและโปรโมชั่น';
			$query[$i]['name']='แพ็กเกจและโปรโมชั่น';
			$query[$i]['path']='frontend/Home/Promotion/';
			$i++;
			foreach ($this->get_page_package() as $data) {
				$query[$i]['id']=$data->id;
				$query[$i]['url']=$data->url;
				$query[$i]['title']=$data->title;
				$query[$i]['name']=$data->name;
				$query[$i]['path']=$data->path;
				$i++;
			}
			//Add TOP News			
			$query[$i]['id']='';
			$query[$i]['url']='';
			$query[$i]['title']='ข่าวสารและกิจกรรม';
			$query[$i]['name']='ข่าวสารและกิจกรรม';
			$query[$i]['path']='frontend/Home/News/';
			$i++;
			foreach ($this->get_page_news() as $data) {
				$query[$i]['id']=$data->id;
				$query[$i]['url']=$data->url;
				$query[$i]['title']=$data->title;
				$query[$i]['name']=$data->name;
				$query[$i]['path']=$data->path;
				$i++;
			}
			//Add TOP Service			
			$query[$i]['id']='';
			$query[$i]['url']='';
			$query[$i]['title']='บริการทางการแพทย์ / ศูนย์พยาบาล';
			$query[$i]['name']='บริการทางการแพทย์ / ศูนย์พยาบาล';
			$query[$i]['path']='frontend/Home/Service/';
			$i++;
			foreach ($this->get_page_service() as $data) {
				$query[$i]['id']=$data->id;
				$query[$i]['url']=$data->url;
				$query[$i]['title']=$data->title;
				$query[$i]['name']=$data->name;
				$query[$i]['path']=$data->path;
				$i++;
			}
			//Add TOP SideMenu			
			$query[$i]['id']='';
			$query[$i]['url']='';
			$query[$i]['title']='เมนูข้าง';
			$query[$i]['name']='เมนูข้าง';
			$query[$i]['path']='';
			$i++;
			foreach ($this->get_page_side() as $data) {
				$query[$i]['id']=$data->id;
				$query[$i]['url']=$data->url;
				$query[$i]['title']=$data->title;
				$query[$i]['name']=$data->name;
				$query[$i]['path']=$data->path;
				$i++;
			}
			//Add TOP SubSideMenu			
			$query[$i]['id']='';
			$query[$i]['url']='';
			$query[$i]['title']='เมนูข้าง ย่อย';
			$query[$i]['name']='เมนูข้าง ย่อย';
			$query[$i]['path']='';
			$i++;
			foreach ($this->get_page_sub_side() as $data) {
				$query[$i]['id']=$data->id;
				$query[$i]['url']=$data->url;
				$query[$i]['title']=$data->title;
				$query[$i]['name']=$data->name;
				$query[$i]['path']=$data->path;
				$i++;
			}
			//Add TOP SubSubSideMenu			
			$query[$i]['id']='';
			$query[$i]['url']='';
			$query[$i]['title']='เมนูข้าง ย่อยของเมนูย่อย';
			$query[$i]['name']='เมนูข้าง ย่อยของเมนูย่อย';
			$query[$i]['path']='';
			$i++;
			foreach ($this->get_page_sub_sub_side() as $data) {
				$query[$i]['id']=$data->id;
				$query[$i]['url']=$data->url;
				$query[$i]['title']=$data->title;
				$query[$i]['name']=$data->name;
				$query[$i]['path']=$data->path;
				$i++;
			}
			//Add Additional 			
			$query[$i]['id']='';
			$query[$i]['url']='';
			$query[$i]['title']='แพทย์ทั้งหมด';
			$query[$i]['name']='แพทย์ทั้งหมด';
			$query[$i]['path']='frontend/Home/OurDoctors';
			$i++;			
			$query[$i]['id']='';
			$query[$i]['url']='';
			$query[$i]['title']='นัดหมายแพทย์';
			$query[$i]['name']='นัดหมายแพทย์';
			$query[$i]['path']='frontend/Home/doctorAppointment';
			$i++;
			return $query;
		}

		function get_page_nav()
		{
			$query = $this->db->query("SELECT id,nav_url as url,nav_title as title FROM tb_navigation WHERE nav_status != 0 AND nav_content <> '' ")->result();
			$i = 0;
			foreach ($query as $data) {
				$query[$i]->name = 'เมนู';
				$query[$i]->path = 'frontend/Home/NavDetails/';
				$i++;
			}
			return $query;
		}

		function get_page_sub_nav()
		{
			$query = $this->db->query("SELECT id,sub_nav_url as url,sub_nav_title as title FROM tb_subnavigation WHERE sub_nav_status != 0 AND sub_nav_content <> '' ")->result();
			$i = 0;
			foreach ($query as $data) {
				$query[$i]->name = 'เมนูย่อย';
				$query[$i]->path = 'frontend/Home/SubNavDetails/';
				$i++;
			}
			return $query;
		}

		function get_page_bloghealthy()
		{
			$query = $this->db->query("SELECT id,blog_url as url,blog_title as title FROM tb_bloghealthy WHERE blog_status != 0")->result();
			$i = 0;
			foreach ($query as $data) {
				$query[$i]->name = 'บทความสุขภาพ';
				$query[$i]->path = 'frontend/Home/HealthyblogDetails/';
				$i++;
			}
			return $query;
		}

		function get_page_package()
		{
			$query = $this->db->query("SELECT id as id,package_url as url,package_title as title FROM tb_package WHERE package_status != 0")->result();
			$i = 0;
			foreach ($query as $data) {
				$query[$i]->name = 'แพ็กเกจและโปรโมชั่น';
				$query[$i]->path = 'frontend/Home/PromotionDetails/';
				$i++;
			}
			return $query;
		}

		function get_package(){			
			$i=0;
			//Add TOP Nav			
			$query[$i]['id']='';
			$query[$i]['url']='';
			$query[$i]['title']='โปรโมชั่น';
			$query[$i]['name']='โปรโมชั่น';
			$query[$i]['path']='';
			$i++;
			foreach ($this->get_page_package() as $data) {
				$query[$i]['id']=$data->id;
				$query[$i]['url']=$data->url;
				$query[$i]['title']=$data->title;
				$query[$i]['name']=$data->name;
				$query[$i]['path']=$data->path;
				$i++;
			}
			return $query;
		}

		function get_page_news()
		{
			$query = $this->db->query("SELECT new_activity_id as id,new_activity_url as url,new_activity_name as title FROM new_activity WHERE new_activity_status != 0")->result();
			$i = 0;
			foreach ($query as $data) {
				$query[$i]->name = 'ข่าวสารและกิจกรรม';
				$query[$i]->path = 'frontend/Home/NewsDetails/';
				$i++;
			}
			return $query;
		}

		function get_page_service()
		{
			$query = $this->db->query("SELECT id as id,medical_center_url as url,medical_center_name as title FROM tb_medical_center WHERE status != 0")->result();
			$i = 0;
			foreach ($query as $data) {
				$query[$i]->name = 'ศูนย์พยาบาล';
				$query[$i]->path = 'frontend/Home/ServiceDetails/';
				$i++;
			}
			return $query;
		}

		function get_page_side()
		{
			$query = $this->db->query("SELECT id,sidemenu_url as url,sidemenu_title as title FROM tb_sidemenu WHERE sidemenu_status != 0 AND sidemenu_content <> '' ")->result();
			$i = 0;
			foreach ($query as $data) {
				$query[$i]->name = 'เมนูข้าง';
				$query[$i]->path = 'frontend/Home/SidebarDetails/';
				$i++;
			}
			return $query;
		}

		function get_page_sub_side()
		{
			$query = $this->db->query("SELECT id,sub_sidemenu_url as url,sub_sidemenu_title as title FROM tb_subofsidemenu WHERE sub_sidemenu_status != 0 AND sub_sidemenu_content <> '' ")->result();
			$i = 0;
			foreach ($query as $data) {
				$query[$i]->name = 'เมนูข้าง ย่อย';
				$query[$i]->path = 'frontend/Home/SubSidebarDetails/';
				$i++;
			}
			return $query;
		}

		function get_page_sub_sub_side()
		{
			$query = $this->db->query("SELECT id,subofsubmenu_url as url,subofsubmenu_title as title FROM tb_subofsubmenu WHERE subofsubmenu_status != 0 AND subofsubmenu_content <> '' ")->result();
			$i = 0;
			foreach ($query as $data) {
				$query[$i]->name = 'เมนูข้าง ย่อยของเมนูย่อย';
				$query[$i]->path = 'frontend/Home/SubSubSidebarDetails/';
				$i++;
			}
			return $query;
		}

		function get_page_doctor()
		{
			
			$this->selfregisdb = $this->load->database('selfregis', TRUE);
			$this->selfregisdb->select('ms_care.uid as id');
			$this->selfregisdb->select('ms_care.*');			
			$this->selfregisdb->select('ms_title.uid as pname_uid');
			$this->selfregisdb->select('ms_title.titlename as pname');
			$this->selfregisdb->join('ms_title','ms_care.ms_title_uid = ms_title.uid','left');
			$this->selfregisdb->where('ms_care.status',0);
			$query  = $this->selfregisdb->get('ms_care')->result();
			$i = 0;
			foreach ($query as $data) {
				$query[$i]->name = 'แพทย์';
				$query[$i]->path = 'frontend/Home/DoctorDetails/';
				$i++;
			}

			return $query;
			//pname_th . ' ' . firstname_th . ' ' . lastname_th
			$ql = $this->session->userdata('q_l');
			if($ql == ' ' || $ql == '' || !isset($ql) || empty($ql)){
				$ql = '_th';
			}
			$query = $this->db->query("SELECT id as id,doctor_url as url, pname" .$ql. " as pname_th, firstname" .$ql. " as firstname_th, lastname" .$ql. " as lastname_th FROM tb_doctor WHERE status != 0")->result();
			$i = 0;
			foreach ($query as $data) {
				$query[$i]->name = 'แพทย์';
				$query[$i]->path = 'frontend/Home/DoctorDetails/';
				$i++;
			}

			return $query;
		}
		
		function get_doctor(){			
			$i=0;
			//Add TOP Nav			
			$query[$i]['id']='';
			$query[$i]['url']='';
			$query[$i]['title']='แพทย์';
			$query[$i]['name']='แพทย์';
			$query[$i]['path']='';
			$i++;
			foreach ($this->get_page_doctor() as $data) {
				$query[$i]['id']=$data->id;
				// $query[$i]['url']=$data->url;
				$query[$i]['title']=$data->pname . ' ' . $data->forename . ' ' . $data->surname;
				$query[$i]['name']=$data->pname . ' ' . $data->forename . ' ' . $data->surname;
				$query[$i]['path']=$data->path;
				$i++;
			}
			return $query;
		}


	}