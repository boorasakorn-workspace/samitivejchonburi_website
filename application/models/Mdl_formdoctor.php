<?php
	class Mdl_formdoctor extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
			$this->selfregisdb = $this->load->database('selfregis', TRUE);
		}

		function getAllDoctor(){
			$ch = curl_init();
			curl_setopt($ch,CURLOPT_URL,"http://27.254.59.19:5088/Master?master_name=location"); //getAll Location
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
			$output=curl_exec($ch);
			curl_close($ch);
			$location = json_decode($output);
			$alldoctor = Array();
			foreach($location as $locate){
				$ch = curl_init();
				curl_setopt($ch,CURLOPT_URL,"http://27.254.59.19:5088/Master?master_name=careprovider&parent=$locate->id"); //getAll Location
				curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
				$output=curl_exec($ch);
				curl_close($ch);
				$careprovider = json_decode($output);
				foreach($careprovider as $doctor){
					$doctor->location = $locate->id;
					$alldoctor[count($alldoctor)] = $doctor;
				}
			}
			return $alldoctor;
		}

		function getPName(){
			$this->selfregisdb->select('*');
			$selftitle = $this->selfregisdb->get('ms_title');
			return $selftitle;
		}
		function getGender(){
			$this->selfregisdb->select('*');
			$selfgender = $this->selfregisdb->get('ms_gender');
			return $selfgender;
		}

		function getDbSelfRegister()
		{
			$this->selfregisdb->select('ms_care.*');			
			$this->selfregisdb->select('ms_title.uid as pname_uid');
			$this->selfregisdb->select('ms_title.titlename as pname');
			$this->selfregisdb->join('ms_title','ms_care.ms_title_uid = ms_title.uid','left');
			$selfregisdoctor = $this->selfregisdb->get('ms_care');
			return $selfregisdoctor;
		}


		function get_table()
		{
			$table = "ms_care";
			return $table;
		}

		function get($order_by)
		{
			$table = $this->get_table();
			$this->selfregisdb->select('ms_care.*');
			$this->selfregisdb->select('ms_title.titlename as pname');
			$this->selfregisdb->join('ms_title','ms_care.ms_title_uid = ms_title.uid','left');
			$this->selfregisdb->order_by($order_by);
			$query = $this->selfregisdb->get($table);
			return $query;
		}

		function get_with_limit($limit, $offset, $order_by)
		{
			$table = $this->get_table();
			$this->selfregisdb->limit($limit, $offset);
			$this->selfregisdb->order_by($order_by);
			$query = $this->selfregisdb->get($table);
			return $query;
		}

		function get_where($id)
		{
			$table = $this->get_table();
			$this->selfregisdb->select('ms_care.*');
			$this->selfregisdb->select('ms_title.uid as pname_uid');
			$this->selfregisdb->select('ms_title.titlename as pname');
			$this->selfregisdb->join('ms_title','ms_care.ms_title_uid = ms_title.uid','left');
			$this->selfregisdb->where('ms_care.uid', $id);
			$query = $this->selfregisdb->get($table);
			return $query;
		}

		function get_where_custom($col, $value)
		{
			$table = $this->get_table();
			$this->selfregisdb->where($col, $value);
			$query = $this->selfregisdb->get($table);
			return $query;
		}

		function _insert($data)
		{
			$table = $this->get_table();
			$this->selfregisdb->insert($table, $data);
		}

		function _update($id, $data)
		{
			$table = $this->get_table();
			$this->selfregisdb->where('uid', $id);
			$this->selfregisdb->update($table, $data);
		}

		function _delete($id)
		{
			$this->selfregisdb->where('uid', $id);
			$this->selfregisdb->delete('ms_care');
		}

		function count_where($column, $value)
		{
			$table = $this->get_table();
			$this->selfregisdb->where($column, $value);
			$query = $this->selfregisdb->get($table);
			$num_rows = $query->num_rows();
			return $num_rows;
		}

		function count_all()
		{
			$table = $this->get_table();
			$query = $this->selfregisdb->get($table);
			$num_rows = $query->num_rows();
			return $num_rows;
		}

		function get_max()
		{
			$table = $this->get_table();
			$this->selfregisdb->select_max('uid');
			$query = $this->selfregisdb->get($table);
			$row = $query->row();
			$id = $row->id;
			return $id;
		}

		function _custom_query($mysql_query)
		{
			$query = $this->selfregisdb->query($mysql_query);
			return $query;
		}
	}