<?php
defined('BASEPATH') or exit('No direct script access allowed');

class API_MDL extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_doctor($uid = NULL)
    {
        $this->db->select('*');
        $this->db->select("CONCAT('".base_url('gallery/doctor/resize_imgs/')."',( CASE WHEN doctor_imgs IS NOT NULL AND doctor_imgs != '' THEN doctor_imgs ELSE 'doctor_vectopr.png' END )) as imgpath");
        $this->db->from('tb_doctor');
        $this->db->where('status',1);
        if($uid){
            $this->db->where('medical_id',$uid);
        }
        $query = $this->db->get();
        return $uid?$query->row_array():$query->result_array();
    }

    public function get_careprovider($data){        
        $this->db->select('*');
        $this->db->select("CONCAT('".base_url('gallery/doctor/resize_imgs/')."',( CASE WHEN doctor_imgs IS NOT NULL AND doctor_imgs != '' THEN doctor_imgs ELSE 'doctor_vectopr.png' END )) as imgpath");
        $this->db->from('tb_doctor');
        $this->db->where('status',1);
        if(isset($data['doctor_name']) && $data['doctor_name']){
            $this->db->where("( CONCAT(pname_th,' ',firstname_th,' ',lastname_th) LIKE '%" .$data['doctor_name']. "%'
                OR CONCAT(pname_en,' ',firstname_en,' ',lastname_en) LIKE '%" .$data['doctor_name']. "%' ) ",NULL,FALSE);
        }
        if(isset($data['language']) && $data['language'] ){
            $this->db->where("( language_th = '" .$data['language']. "' 
                OR language_th LIKE '%," .$data['language']. "' 
                OR language_th LIKE '%," .$data['language']. ",%' ) 
                OR
                ( language_en = '" .$data['language']. "' 
                OR language_en LIKE '%," .$data['language']. "' 
                OR language_en LIKE '%," .$data['language']. ",%' ) ",NULL,FALSE);
        }
        if(isset($data['medical']) && $data['medical'] ){
            $this->db->where("( parent_medical_center_id = '" .$data['medical']. "' 
                OR parent_medical_center_id LIKE '%|" .$data['medical']. "' 
                OR parent_medical_center_id LIKE '%|" .$data['medical']. "|%' ) ",NULL,FALSE);
        }
        if(isset($data['specialty']) && $data['specialty'] ){
            $this->db->where("( parent_specialty_id = '" .$data['specialty']. "' 
                OR parent_specialty_id LIKE '%|" .$data['specialty']. "' 
                OR parent_specialty_id LIKE '%|" .$data['specialty']. "|%' ) ",NULL,FALSE);
        }
        if(isset($data['subspecialty']) && $data['subspecialty'] ){
            $this->db->where("( parent_sub_specialty_id = '" .$data['subspecialty']. "' 
                OR parent_sub_specialty_id LIKE '%|" .$data['subspecialty']. "' 
                OR parent_sub_specialty_id LIKE '%|" .$data['subspecialty']. "|%' ) ",NULL,FALSE);
        }
        $query = $this->db->get();
        return $query->result_array();
    }
    public function get_careprovider_med($uid){        
        $this->db->select('*');
        $this->db->select("CONCAT('".base_url('gallery/doctor/resize_imgs/')."',( CASE WHEN doctor_imgs IS NOT NULL AND doctor_imgs != '' THEN doctor_imgs ELSE 'doctor_vectopr.png' END )) as imgpath");
        $this->db->from('tb_doctor');
        $this->db->where('status',1);
        $this->db->where("( parent_medical_center_id = '" .$uid. "' 
            OR parent_medical_center_id LIKE '%|" .$uid. "' 
            OR parent_medical_center_id LIKE '%|" .$uid. "|%' ) ",NULL,FALSE);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function get_careprovider_sp($uid){        
        $this->db->select('*');
        $this->db->select("CONCAT('".base_url('gallery/doctor/resize_imgs/')."',( CASE WHEN doctor_imgs IS NOT NULL AND doctor_imgs != '' THEN doctor_imgs ELSE 'doctor_vectopr.png' END )) as imgpath");
        $this->db->from('tb_doctor');
        $this->db->where('status',1);
        $this->db->where("( parent_specialty_id = '" .$uid. "' 
            OR parent_specialty_id LIKE '%|" .$uid. "' 
            OR parent_specialty_id LIKE '%|" .$uid. "|%' ) ",NULL,FALSE);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function get_careprovider_subsp($uid){        
        $this->db->select('*');
        $this->db->select("CONCAT('".base_url('gallery/doctor/resize_imgs/')."',( CASE WHEN doctor_imgs IS NOT NULL AND doctor_imgs != '' THEN doctor_imgs ELSE 'doctor_vectopr.png' END )) as imgpath");
        $this->db->from('tb_doctor');
        $this->db->where('status',1);
        $this->db->where("( parent_sub_specialty_id = '" .$uid. "' 
            OR parent_sub_specialty_id LIKE '%|" .$uid. "' 
            OR parent_sub_specialty_id LIKE '%|" .$uid. "|%' ) ",NULL,FALSE);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_clinic($uid = NULL)
    {
        $this->db->select('*');
        $this->db->from('tb_medical_center');
        if($uid){
            $this->db->where('id',$uid);
        }
        $query = $this->db->get();
        return $uid?$query->row_array():$query->result_array();
    }

    public function get_speciality($uid = NULL)
    {
        $this->db->select('speciality_id,parent_speciality_id,speciality_name as speciality_th,speciality_name_en,speciality_code');
        $this->db->from('speciality');
        if($uid){
            $this->db->where('speciality_id',$uid);
        }
        $query = $this->db->get();
        return $uid?$query->row_array():$query->result_array();
    }

    public function get_specialty($uid = NULL)
    {
        $this->db->select('id,parent_id,specialty_name_th,specialty_name_en');
        $this->db->from('tb_specialty');
        $this->db->where('status',1);
        if($uid){
            $this->db->where('id',$uid);
        }
        $query = $this->db->get();
        return $uid?$query->row_array():$query->result_array();
    }

    public function get_subspecialty($uid = NULL)
    {
        $this->db->select('id,parent_center_id,parent_specialty_id,sub_specialty_name_th,sub_specialty_name_en');
        $this->db->from('tb_sub_specialty');
        $this->db->where('status',1);
        if($uid){
            $this->db->where('id',$uid);
        }
        $query = $this->db->get();
        return $uid?$query->row_array():$query->result_array();
    }
    
    public function get_list_clinic($uid = NULL)
    {
        $this->db->select('tb_medical_center.id as clinic_id, medical_center_name as clinic_name_th, medical_center_name_en as clinic_name_en,medical_center_office_daystxt as clinic_days,medical_center_office_hours as clinic_time,medical_center_phone as clinic_phone,medical_center_email as clinic_email');
        $this->db->select('tb_specialty.id as specialty_id, tb_specialty.specialty_name_th as specialty_name_th, tb_specialty.specialty_name_en as specialty_name_en');
        $this->db->select('tb_sub_specialty.id as sub_speciality_id, tb_sub_specialty.sub_specialty_name_th as sub_specialty_name_th, tb_sub_specialty.sub_specialty_name_th as sub_specialty_name_en');
        $this->db->from('tb_medical_center');
        $this->db->join('tb_specialty',"
        ( 
            tb_specialty.parent_id = tb_medical_center.id
            OR tb_specialty.parent_id LIKE CONCAT('%|', tb_medical_center.id ) 
            OR tb_specialty.parent_id LIKE CONCAT('%|', tb_medical_center.id ,'|%') 
        ) AND tb_specialty.status = 1",'left');
        $this->db->join('tb_sub_specialty',"
        ( 
            tb_sub_specialty.parent_specialty_id = tb_specialty.id 
            OR tb_sub_specialty.parent_specialty_id LIKE CONCAT('%|', tb_specialty.id) 
            OR tb_sub_specialty.parent_specialty_id LIKE CONCAT('%|', tb_specialty.id ,'|%') 
        ) AND tb_sub_specialty.status = 1",'left');
        $this->db->where('tb_medical_center.status',1);
        if($uid){
            $this->db->where('speciality_id',$uid);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_list_specialty($uid = NULL)
    {
        $this->db->select('tb_medical_center.id as clinic_id, medical_center_name as clinic_name_th, medical_center_name_en as clinic_name_en,medical_center_office_daystxt as clinic_days,medical_center_office_hours as clinic_time,medical_center_phone as clinic_phone,medical_center_email as clinic_email');
        $this->db->select('tb_specialty.id as specialty_id, tb_specialty.specialty_name_th as specialty_name_th, tb_specialty.specialty_name_en as specialty_name_en');
        $this->db->select('tb_sub_specialty.id as sub_speciality_id, tb_sub_specialty.sub_specialty_name_th as sub_specialty_name_th, tb_sub_specialty.sub_specialty_name_th as sub_specialty_name_en');
        $this->db->from('tb_specialty');
        $this->db->join('tb_medical_center',"
        ( 
            tb_specialty.parent_id = tb_medical_center.id
            OR tb_specialty.parent_id LIKE CONCAT('%|', tb_medical_center.id ) 
            OR tb_specialty.parent_id LIKE CONCAT('%|', tb_medical_center.id ,'|%') 
        ) AND tb_medical_center.status = 1",'left');
        $this->db->join('tb_sub_specialty',"
        ( 
            tb_specialty.id = tb_sub_specialty.parent_specialty_id 
            OR tb_specialty.id LIKE CONCAT('%|', tb_sub_specialty.parent_specialty_id) 
            OR tb_specialty.id LIKE CONCAT('%|', tb_sub_specialty.parent_specialty_id ,'|%') 
        ) AND tb_sub_specialty.status = 1",'left');
        $this->db->where('tb_specialty.status',1);
        if($uid){
            $this->db->where('id',$uid);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_list_subspecialty($uid = NULL)
    {
        $this->db->select('tb_medical_center.id as clinic_id, medical_center_name as clinic_name_th, medical_center_name_en as clinic_name_en,medical_center_office_daystxt as clinic_days,medical_center_office_hours as clinic_time,medical_center_phone as clinic_phone,medical_center_email as clinic_email');
        $this->db->select('tb_specialty.id as specialty_id, tb_specialty.specialty_name_th as specialty_name_th, tb_specialty.specialty_name_en as specialty_name_en');
        $this->db->select('tb_sub_specialty.id as sub_speciality_id, tb_sub_specialty.sub_specialty_name_th as sub_specialty_name_th, tb_sub_specialty.sub_specialty_name_th as sub_specialty_name_en');
        $this->db->from('tb_sub_specialty');
        $this->db->join('tb_specialty',"
        ( 
            tb_specialty.id = tb_sub_specialty.parent_specialty_id 
            OR tb_specialty.id LIKE CONCAT('%|', tb_sub_specialty.parent_specialty_id) 
            OR tb_specialty.id LIKE CONCAT('%|', tb_sub_specialty.parent_specialty_id ,'|%') 
        ) AND tb_specialty.status = 1",'left');
        $this->db->join('tb_medical_center',"
        ( 
            tb_specialty.parent_id = tb_medical_center.id
            OR tb_specialty.parent_id LIKE CONCAT('%|', tb_medical_center.id ) 
            OR tb_specialty.parent_id LIKE CONCAT('%|', tb_medical_center.id ,'|%') 
        ) AND tb_medical_center.status = 1",'left');
        $this->db->where('tb_sub_specialty.status',1);
        if($uid){
            $this->db->where('id',$uid);
        }
        $query = $this->db->get();
        return $query->result_array();
    }
}
