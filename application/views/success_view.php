
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<link rel="stylesheet" href="<?php echo base_url('public/css/bootstrap.min.css');?>">
<script src="<?php echo base_url('public/js/bootstrap.min.js');?>"></script>
<title><?php echo $this->lang->line('welcome') ?></title>

<link href="https://fonts.googleapis.com/css?family=Mitr:200,300,400,500,600,700&display=swap" rel="stylesheet">
<html lang="en">
<head>
   <meta charset="utf-8">
   <title>แบบประเมินความเสี่ยงก่อนมาโรงพยาบาลต่อการติดเชื้อ COVID-19</title>

   <style type="text/css">
      a{
         text-decoration: none;
      }
      .h-covid{
         font-family: Mitr;
         font-weight: 400;
         font-size: 25px;
      }
      .txt{
         font-family: Mitr;
         font-weight: 300;
         font-size: 17px;
      }
      .txt-form{
         font-family: Mitr;
         font-weight: 300;
         font-size: 17px;
      }
      .txt-b{
         font-family: Mitr;
         font-weight: 400;
         font-size: 19px;
         color:#000;
      }
      .txt-s{
         font-family: Mitr;
         font-weight: 200;
         font-size: 19px;
      }
      .lang{
         font-family: Mitr;
         font-weight: 400;
         font-size: 17px;
         color: #000;
      }
   </style>
</head>




<div class="containter">
   <div class="text-center">
      <img src="<?php echo base_url('public/images/logo.png');?>">
   </div>
   <div class="h-covid text-center mb-3">
      <?php echo $this->lang->line('hospital') ?>

   </div>



 
   <div class="text-center txt-b mb-3">
      <a href="<?php echo base_url("welcome/switchLang/thai"); ?>" class="txt-b">ภาษาไทย</a> |
      <a href="<?php echo base_url("welcome/switchLang/english"); ?>" class="txt-b">English</a> |
      <a href="<?php echo base_url("welcome/switchLang/chinese"); ?>" class="txt-b">中文</a> |
      <a href="<?php echo base_url("welcome/switchLang/japanese"); ?>" class="txt-b">日本</a> |
      <!-- <a href="<?php echo base_url("welcome/switchLang/spanish"); ?>">Spanish</a> -->
   </div>
   <div class="txt-b text-center">
      Thank you.
   </div>
</div>