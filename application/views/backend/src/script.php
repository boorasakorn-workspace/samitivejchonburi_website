
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url(); ?>assets_b/plugins/jquery/jquery-3.3.1.min.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/plugins/jquery/jquery-migrate-1.1.0.min.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/plugins/jquery-ui/jquery-ui.min.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/plugins/bootstrap/4.1.3/js/bootstrap.bundle.min.js?v=1001"></script>
	
	
	<script src="<?php echo base_url(); ?>assets_b/plugins/slimscroll/jquery.slimscroll.min.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/plugins/js-cookie/js.cookie.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/js/theme/default.min.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/js/apps.min.js?v=1001"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="<?php echo base_url(); ?>assets_b/plugins/d3/d3.min.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/plugins/nvd3/build/nv.d3.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/plugins/jquery-jvectormap/jquery-jvectormap.min.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js?v=1001"></script>
	<!-- <script src="<?php echo base_url(); ?>assets_b/plugins/gritter/js/jquery.gritter.js?v=1001"></script> -->
	<script src="<?php echo base_url(); ?>assets_b/js/demo/dashboard-v2.min.js?v=1001"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="<?php echo base_url(); ?>assets_b/plugins/parsley/dist/parsley.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/plugins/highlight/highlight.common.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/js/demo/render.highlight.js?v=1001"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->


	<script src="<?php echo base_url(); ?>assets_b/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/plugins/bootstrap-daterangepicker/moment.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/js/demo/form-plugins.demo.js?v=1001"></script>
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="<?php echo base_url(); ?>assets_b/plugins/DataTables/media/js/jquery.dataTables.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/plugins/DataTables/media/js/dataTables.bootstrap.min.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/plugins/DataTables/extensions/Buttons/js/jszip.min.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/js/demo/table-manage-buttons.demo.min.js?v=1001"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->

	<script src="<?php echo base_url(); ?>assets_b/plugins/jquery-tag-it/js/tag-it.min.js?v=1001"></script>
	
	<script src="<?php echo base_url(); ?>assets_b/plugins/bootstrap-wysihtml5/dist/bootstrap3-wysihtml5.all.min.js?v=1001"></script>
	 
	<script src="<?php echo base_url(); ?>assets_b/js/demo/email-compose.demo.min.js?v=1001"></script>
	<script src="<?php echo base_url(); ?>assets_b/js/demo/email-inbox.demo.js?v=1001"></script>

	<script src="<?php echo base_url(); ?>assets_b/js/bootstrap-select.min.js?v=1001"></script>

	
	<script src="<?=base_url();?>assets_b/js/ckeditor/ckeditor.js"></script>

	<script>
		$(document).ready(function() {
			App.init();
			InboxV2.init();
			EmailCompose.init();
			DashboardV2.init();
			TableManageButtons.init();
			FormTimePicker.init();
			FormPluginsDatePicker.init();

		});
		$(document).ready(async function() {
		 await $('[aria-label="รหัสแพทย์').click();
		 await $('[aria-label="รหัสแพทย์').click();
		//  await
		});
	</script>