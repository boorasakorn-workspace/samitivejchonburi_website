<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
	<meta charset="utf-8" />
	<title>Samitivej Chonburi</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />

	<!-- Favicon -->
	<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" type="image/x-icon" />
	<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" />
	<!-- logo icon -->
	<link rel="icon" type="image/png" href="https://www.samitivejhospitals.com/th/wp-content/themes/svh/images/svh-favicons-standard.png" />
	<link rel="icon" type="image/x-icon" href="https://www.samitivejhospitals.com/th/wp-content/themes/svh/images/svh-favicons-standard-ico.ico" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="https://www.samitivejhospitals.com/th/wp-content/themes/svh/images/svh-homescreen-144px.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="https://www.samitivejhospitals.com/th/wp-content/themes/svh/images/svh-homescreen-72px.png" />
	<link rel="apple-touch-icon-precomposed" href="https://www.samitivejhospitals.com/th/wp-content/themes/svh/images/svh-homescreen-57px.png" />

	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets_b/plugins/jquery-ui/jquery-ui.min.css?v=1001" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets_b/plugins/bootstrap/4.1.3/css/bootstrap.min.css?v=1001" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets_b/plugins/font-awesome/5.3/css/all.min.css?v=1001" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets_b/plugins/animate/animate.min.css?v=1001" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets_b/css/default/style.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets_b/css/default/style-responsive.min.css?v=1001" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets_b/css/default/theme/default.css?v=1001" rel="stylesheet" id="theme" />
	<!-- ================== END BASE CSS STYLE ================== -->

	<!-- ================== BEGIN PAGE LEVEL CSS STYLE ================== -->
	<link href="<?php echo base_url(); ?>assets_b/plugins/jquery-jvectormap/jquery-jvectormap.css?v=1001" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets_b/plugins/bootstrap-calendar/css/bootstrap_calendar.css?v=1001" rel="stylesheet" />
	<!-- <link href="<?php echo base_url(); ?>assets_b/plugins/gritter/css/jquery.gritter.css?v=1001" rel="stylesheet" /> -->
	<link href="<?php echo base_url(); ?>assets_b/plugins/nvd3/build/nv.d3.css?v=1001" rel="stylesheet" />
	<!-- ================== END PAGE LEVEL CSS STYLE ================== -->

	<link href="<?php echo base_url(); ?>assets_b/plugins/bootstrap-eonasdan-datetimepicker/build/css/bootstrap-datetimepicker.min.css?v=1001" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets_b/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css?v=1001" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets_b/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css?v=1001" rel="stylesheet" />

	<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
	<link href="<?php echo base_url(); ?>assets_b/plugins/DataTables/media/css/dataTables.bootstrap.min.css?v=1001" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets_b/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css?v=1001" rel="stylesheet" />
	<!-- ================== END PAGE LEVEL STYLE ================== -->


	<link href="<?php echo base_url(); ?>assets_b//plugins/jquery-tag-it/css/jquery.tagit.css?v=1001" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets_b//plugins/bootstrap-wysihtml5/src/bootstrap3-wysihtml5.css?v=1001" rel="stylesheet" />

	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url(); ?>assets_b/plugins/pace/pace.min.js?v=1001"></script>
	<!-- ================== END BASE JS ================== -->

	<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
	<link href="<?php echo base_url(); ?>assets_b/plugins/parsley/src/parsley.css?v=1001" rel="stylesheet" />
	<!-- ================== END PAGE LEVEL STYLE ================== -->

	<link href="<?php echo base_url(); ?>assets_b/css/bootstrap-select.min.css?v=1001" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>resource/fonts/kanit/stylesheet.css?v=1001" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>resource/customCSS/customCSS.css?v=1001" rel="stylesheet" />
	

	<style>
		.pricing_table {
			text-align: center;
			position: relative;
		}

		.pricing_table .pricing_table_header {
			border-radius: 0 60px 0 0;
			overflow: hidden;
		}

		.pricing_table .pricing_table_header .title {
			color: #ffffff;
			font-size: 20px;
			font-weight: 700;
			line-height: 30px;
			padding: 15px 0 0;
			margin: 0;
			text-transform: uppercase;
			-webkit-transition: all 0.4s ease 0s;
			-o-transition: all 0.4s ease 0s;
			transition: all 0.4s ease 0s;
		}

		.pricing_table .pricing_table_header .price-value {
			color: #ffffff;
			display: block;
			font-size: 35px;
			font-weight: 700;
			margin: 0;
			padding: 5px 0 10px;
			-webkit-transition: all 0.4s ease 0s;
			-o-transition: all 0.4s ease 0s;
			transition: all 0.4s ease 0s;
		}

		.pricing_table .pricing_table_header .price-value .month {
			display: block;
			font-size: 16px;
			font-weight: 500;
			color: #ffffff;
			text-transform: uppercase;
		}

		.pricing_table .pricing-content {
			border-left: 1px solid #f2f2f2;
			position: relative;
		}

		.pricing_table .pricing-content ul {
			padding: 0;
			margin: 0;
			list-style: none;
			background: #ffffff;
		}

		.pricing_table .pricing-content ul li {
			display: block;
			font-size: 15px;
			color: #333333;
			line-height: 23px;
			padding: 11px 0;
			border-bottom: 1px solid #f2f2f2;
		}

		.pricing_table .pricing-content .pricing_table_signup {
			background: #ffffff;
			border-bottom: 1px solid #f2f2f2;
			border-radius: 0 0 0 60px;
			padding: 20px 0;
		}

		.pricing_table .pricing-content .pricing_table_signup a {
			display: inline-block;
			padding: 9px 20px;
			font-weight: 700;
			color: #555555;
			text-transform: uppercase;
			border: 1px solid #555555;
			background: #ffffff;
			transition: all 0.4s ease 0s;
		}

		.pricing_table .pricing-content .pricing_table_signup a:hover {
			background: #006640 !important;
			color: #ffffff;
		}

		.pricing_table .pricing-content .pricing_table_signup a i {
			padding-left: 5px;
		}

		.pricing_table:hover .pricing_table_header,
		.pricing_table:hover .title,
		.pricing_table:hover .price-value {
			background: #006640 !important;
		}

		.pricingTable {
			text-align: center;
			position: relative;
		}

		.pricingTable .pricingTable-header:after {
			content: "";
			height: 25.8%;
			position: absolute;
			right: 0;
			top: -1px;
			transform: skewY(45deg) translateY(18px);
			width: 36px;
			z-index: 2;
			-webkit-transition: all 0.4s ease 0s;
			-o-transition: all 0.4s ease 0s;
			transition: all 0.4s ease 0s;
		}

		.pricingTable .title {
			color: #ffffff;
			font-size: 20px;
			font-weight: 700;
			line-height: 30px;
			padding: 15px 0 0;
			margin: 0 35px 0 0;
			text-transform: uppercase;
			-webkit-transition: all 0.4s ease 0s;
			-o-transition: all 0.4s ease 0s;
			transition: all 0.4s ease 0s;
		}

		.pricingTable .price-value {
			color: #ffffff;
			display: block;
			font-size: 35px;
			font-weight: 700;
			margin: 0 35px 0 0;
			padding: 5px 0 10px;
			-webkit-transition: all 0.4s ease 0s;
			-o-transition: all 0.4s ease 0s;
			transition: all 0.4s ease 0s;
		}

		.pricingTable .month {
			display: block;
			font-size: 13px;
			font-weight: 500;
			color: #ffffff;
			text-transform: uppercase;
		}

		.pricingTable .pricing-content {
			border-left: 1px solid #f2f2f2;
			position: relative;
		}

		.pricingTable .pricing-content:after {
			content: "";
			width: 35px;
			height: 100%;
			background: #f8f8f8;
			box-shadow: 9px 9px 20px #DCDCDC;
			position: absolute;
			top: 0;
			right: 0;
			z-index: 1;
			transform: skewY(-45deg) translateY(-18px);
			-webkit-transition: all 0.4s ease 0s;
			-o-transition: all 0.4s ease 0s;
			transition: all 0.4s ease 0s;
		}

		.pricingTable .pricing-content ul {
			padding: 0;
			margin: 0 35px 0 0;
			list-style: none;
			background: #ffffff;
		}

		.pricingTable .pricing-content ul li {
			display: block;
			font-size: 15px;
			color: #333333;
			line-height: 23px;
			padding: 11px 0;
			border-bottom: 1px solid #f2f2f2;
		}

		.pricingTable .pricingTable-signup {
			background: #ffffff;
			padding: 20px 0;
			margin-right: 35px;
			border-bottom: 1px solid #f2f2f2;
		}

		.pricingTable .pricingTable-signup a {
			display: inline-block;
			padding: 9px 20px;
			font-weight: 700;
			color: #555555;
			text-transform: uppercase;
			border: 1px solid #555555;
			background: #ffffff;
			transition: all 0.4s ease 0s;
		}

		.pricingTable .pricingTable-signup a:hover {
			color: #ffffff;
		}

		.pricingTable .pricingTable-signup a i {
			padding-left: 5px;
		}

		.pricingTable:hover .pricingTable-header:after,
		.pricingTable:hover .title,
		.pricingTable:hover .price-value {
			background: #004274 !important;
		}
		.pricingTable,.pricing_table,.grid-box,.feature-box2,.srvc-box,.img-box,.blog_post {/*margin-bottom: 50px !important;*/}
	</style>

</head>