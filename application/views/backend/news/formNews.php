<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
		<li class="breadcrumb-item active">News</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header"><?=$headline; ?></h1>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">เพิ่มข่าว</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<?=validation_errors("<p style='color:red;'>", "</p>") ?>
					<?php
						error_reporting(0);
						if(isset(explode('_',$update_id)[1]) && explode('_',$update_id)[1] == '2'){
							$language = "EN";
						}
						else{
							$language = "TH";
						}
						$update_id = explode('_',$update_id)[0];

						if(is_numeric($update_id)) {							
					?>
                        <div class="form-group row fileupload-buttonbar">
                            <div class="col-md-12">
                                <span class="fileinput-button m-r-3">
									<?php if($news_big_img==''){ ?>
                                    <i class="fa fa-plus"></i>
                                    <!--
                                    <a href="<?=base_url(); ?>backend/src/News/upload_image/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-primary" id="upload_img" name="upload_img">Upload News Image</button>
                                    </a>
									-->

	                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#uploadImgModal">Upload News Image</button>

									<!-- Modal -->
									<div id="uploadImgModal" class="modal fade" role="dialog">
									  <div class="modal-dialog">
									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">Upload News Image</h4>
									      </div>
									      <div class="modal-body">
						        			<form id="NewsImageForm" name="NewsImageForm" enctype="multipart/form-data" method="post" >
											<label class="control-label">Please Choose a file from your Computer </label>
											<input type="file" name="file" size="20">
									      </div>
									      <div class="modal-footer">
											<button type="submit" id="Imagesubmit" name="submit" value="Submit" class="btn btn-block btn-success">Upload</button>
									        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											</form>
									      </div>
									    </div>

									  </div>
									</div>

                                	<?php }else{ ?>
                                    <i class="fa fa-times"></i>
                                    <!--
									<a href="<?=base_url(); ?>backend/src/News/delete_image/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-danger" id="delete_img" name="delete_img">Delete News Image</button>
                                    </a>
	                                -->

									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteImgModal">Delete News Image</button>

									<!-- Modal -->
									<div id="deleteImgModal" class="modal fade" role="dialog">
									  <div class="modal-dialog">
									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">Delete News Image</h4>
									      </div>
									      <div class="modal-body">
									        <p>Sure to delete news image?</p>
									      </div>
									      <div class="modal-footer">
									      	<a href="<?=base_url(); ?>backend/src/News/delete_image/<?=$update_id; ?>">
		                                    	<button type="button" class="btn btn-danger" id="delete_img" name="delete_img">Delete News Image</button>
		                                    </a>
									        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
									      </div>
									    </div>

									  </div>
									</div>

                                	<?php } ?>
                                	<!--
                                    <a href="<?=base_url(); ?>backend/src/News/delete_news/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-danger" id="delete_news" name="delete_news">Delete News</button>
                                    </a>
									-->
									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">Delete News</button>

									<!-- Modal -->
									<div id="deleteModal" class="modal fade" role="dialog">
									  <div class="modal-dialog">
									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">Delete News</h4>
									      </div>
									      <div class="modal-body">
									        <p>Sure to delete news?</p>
									      </div>
									      <div class="modal-footer">
		                                    <a href="<?=base_url(); ?>backend/src/News/delete_news/<?=$update_id; ?>">
		                                    	<button type="button" class="btn btn-danger" id="delete_news" name="delete_news">Delete News</button>
		                                    </a>
									        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
									      </div>
									    </div>

									  </div>
									</div>

                                    <a href="<?=base_url(); ?>backend/src/News/view_news_details/<?=$update_id; ?>">
                                        <button type="button" class="btn btn-success" id="view_news" name="view_news">View News In Front</button>
                                    </a>
                                    <?php
										if($language == "EN"){
											echo '<a href="'.base_url().'backend/src/News/create/'.explode('_',$update_id)[0].'">';
										}
										else{
											echo '<a href="'.base_url().'backend/src/News/create/'.explode('_',$update_id)[0]."_2".'">';
										}
									/*
                                    <a href="<?=base_url(); ?>backend/src/News/create/<?=$update_id.'_2'; ?>">
                                    */
                                    ?>
                                        <button type="button" class="btn btn-success" id="switch_language" name="switch_language">Switch Language</button>
                                    </a>
                                </span>
                            </div>
                        </div>
					<?php }
						$location_form = base_url()."backend/src/News/create/".$update_id;
					?>
					<form id="formContents" class="form-horizontal" action="<?= $location_form; ?>" method="POST" enctype="multipart/form-data">
						<H4><?=$language;?></H4>
						<div class="row row-space-10">
							<div class="col-md-4">
								<div class="form-group">
		                            <label for="new_activity_name">ชื่อ Title <span class="text-danger">*</span></label>
		                            <?php
		                            	if($language == 'EN'){
		                            		echo '<input type="hidden" class="form-control" id="new_activity_name" name="new_activity_name" value="'.$new_activity_name.'" />
				                            <input type="hidden" id="news_name" name="news_name" value="'.$new_activity_name.'" />';
		                            		echo '<input type="text" class="form-control" id="new_activity_name_en" name="new_activity_name_en" placeholder="ENG Title" value="'.$new_activity_name_en.'" />
				                            <input type="hidden" id="news_name_en" name="news_name_en" value="'.$new_activity_name_en.'" />';
				                        }
				                        else{
		                            		echo '<input type="text" class="form-control" id="new_activity_name" name="new_activity_name" placeholder="TH Title" value="'.$new_activity_name.'" />
				                            <input type="hidden" id="news_name" name="news_name" value="'.$new_activity_name.'" />';
		                            		echo '<input type="hidden" class="form-control" id="new_activity_name_en" name="new_activity_name_en" value="'.$new_activity_name_en.'" />
				                            <input type="hidden" id="news_name_en" name="news_name_en" value="'.$new_activity_name_en.'" />';
				                        }
		                            ?>		                            
		                        </div>		 
		                    </div>
							<div class="col-md-4">
		                        <div class="form-group">
		                            <label for="new_activity_status">Published Status<span class="text-danger">*</span></label>
		                            <?php
		                            $addition_type = "id='selectType' class='form-control'";
									$type_options = array(
													'' => 'Select Type',
													'0' => 'Unpublished',
													'1' => 'Published'
												);
									echo form_dropdown('new_activity_status', $type_options, $new_activity_status, $addition_type);
									?>
		                        </div>
		                    </div>
		                </div>   
		                <div class="row row-space-10">
		                    <div class="col-md-4" id="PreviewSection" style="width: 0px;height: 0px;opacity: 0;">
		                    	<label for="news_preview_img" >Preview Upload Image |  </label>
		                    	<button type="button" class="btn btn-danger" id="deletePreview" name="deletePreview">Delete</button>
		                        <div class="form-group" id="preimage_section"></div>
		                    </div>
		                        
		                </div>
		                <div class="row row-space-10">
		                    <div class="col-md-12">
		                        <div class="form-group">
		                            <label for="description">News Description</label>
		                            <?php
		                            	if($language == 'EN'){
		                            		echo '<textarea style="display:none;" name="description">'.$description.'</textarea>';
		                            		echo '<textarea id="editor3" class="ckeditor" name="description_en" >'.$description_en.'</textarea>';
				                        }
				                        else{
		                            		echo '<textarea id="editor3" class="ckeditor" name="description" >'.$description.'</textarea>';
		                            		echo '<textarea style="display:none;" name="description_en">'.$description_en.'</textarea>';
				                        }
		                            ?>
		                            
		                        </div>
		                    </div>
		                </div>
						<div class="row row-space-10">
		                    <div class="col-md-6">
								<div class="form-group">
		                            <label for="video">Video (Youtube Link)</label>
	                            	<input type="text" class="form-control" id="news_video" name="news_video" placeholder="https://www.youtube.com/watch?v=foobar , https://youtu.be/foobar" value="<?=$news_video; ?>" />
		                        </div>
		                    </div>
						</div>
						<div class="form-group row">
                            <div class="col-md-4">
								<div class="form-group">
		                            <label for="meta_author">Meta Author</label>
		                            <input type="text" class="form-control" id="meta_author" name="meta_author" value="<?=$meta_author; ?>" placeholder="Author" />
	                            </div>  
		                    </div>
                            <div class="col-md-4">
								<div class="form-group">
		                            <label for="meta_keyword">Meta Keyword</label>
		                            <input type="text" class="form-control" id="meta_keywords" name="meta_keywords" value="<?=$meta_keywords; ?>" placeholder="Keywords"/>
	                            </div>  
		                    </div>
                            <div class="col-md-4">
								<div class="form-group">
		                            <label for="meta_desciption">Meta Description</label>
		                            <input type="text" class="form-control" id="meta_description" name="meta_description" value="<?=$meta_description; ?>" placeholder="Description"/>
	                            </div>  
		                    </div>
                        </div>
						<?php if($new_activity_big_img!=''){ //ต้องทำการส่งค่า news_big_img มาจาก controller News->create ก่อน ?>
                        <div class="form-group row">
                            <label class="col-md-1 col-form-label">Image</label>
                            <div class="col-md-4">
                            	<!--
								<img src="<?=base_url(); ?>gallery/news/big_imgs/<?=$news_big_img; ?>">
								-->
								<img class="img-responsive" style="height:200px; width:295px;" src="<?=base_url(); ?>gallery/news/big_imgs/<?=$new_activity_big_img; ?>">
							</div>
                        </div>
                    	<?php }	
							if(is_numeric($update_id)){	
								echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">แก้ไขข้อมูล</button>';
							}else{
								echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">เพิ่มข้อมูล</button>';
                    		}
						?>
						<button type="button" id="previewContent" name="previewContent" value="Preview" class="btn btn-sm btn-primary m-r-5">พรีวิว</button>
                        <button type="submit" id="submit" name="submit" value="Cancel" class="btn btn-sm btn-warning m-r-5">ยกเลิก</button>
                    </form>
				</div>
				<!-- end panel-body -->
			</div>
			<!-- end panel -->
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
</div>
<!-- end #content -->
