<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
		<li class="breadcrumb-item active">Header Navigation</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header"><?=$headline; ?></h1>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">จัดการ Header Menu</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<?=validation_errors("<p style='color:red;'>", "</p>") ?>
					<?php
						if(is_numeric($update_id)) {
					?>
                        <div class="form-group row fileupload-buttonbar">
                            <div class="col-md-12">
                                <span class="fileinput-button m-r-3">
                                	<!--
                                    <a href="<?=base_url(); ?>backend/src/Navbar/delete_menu/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-danger" id="delete_menu" name="delete_menu">Delete Menu</button>
                                    </a>
	                                -->

									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">Delete Menu</button>

									<!-- Modal -->
									<div id="deleteModal" class="modal fade" role="dialog">
									  <div class="modal-dialog">
									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">Delete Menu</h4>
									      </div>
									      <div class="modal-body">
									        <p>Sure to delete menu?</p>
									      </div>
									      <div class="modal-footer">
		                                    <a href="<?=base_url(); ?>backend/src/Navbar/delete_menu/<?=$update_id; ?>">
		                                    	<button type="button" class="btn btn-danger" id="delete_menu" name="delete_menu">Delete Menu</button>
		                                    </a>
									        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
									      </div>
									    </div>

									  </div>
									</div>

                                    <a href="<?=base_url(); ?>backend/src/Navbar/view_menu_details/<?=$update_id; ?>">
                                        <button type="button" class="btn btn-success" id="view_menu" name="view_menu"><i class="fas fa-eye"></i> View Menu In Front</button>
                                    </a>
                                </span>
                            </div>
                        </div>
					<?php }
						$location_form = base_url()."backend/src/Navbar/create/".$update_id;
					?>
					<form class="form-horizontal" action="<?= $location_form; ?>" method="POST" enctype="multipart/form-data">
						<div class="form-group">
                            <label for="nav_title">ชื่อเมนู <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="nav_title" name="nav_title" value="<?=$nav_title; ?>" placeholder="Thai Menu" />
                            <label for="nav_title_en">English Title</label>
                            <input type="text" class="form-control" id="nav_title_en" name="nav_title_en" value="<?=$nav_title_en; ?>"  placeholder="English Menu"/>
                        </div>
						<div class="form-group">							
							<?php 
								if(!isset($nav_url)){
									$nav_url="";
								} 
							?>
                            <label for="nav_url">URL </label>
                            <input type="text" class="form-control" id="nav_url" name="nav_url" value="<?=$nav_url; ?>" placeholder="Menu Link" />
                        </div>
						<div class="form-group row">
                            <label class="col-md-1 col-form-label" for="nav_status">สถานะ</label>
                            <div class="col-md-4">
							<?php
							$additional_dd_code = "id='selectStatusCode' class='form-control'";
								$options = array(
												'' => 'โปรดเลือกการเผยแพร่',
												'1' => 'Published',
												'0' => 'Unpublished'
											);

								echo form_dropdown('nav_status', $options, $nav_status, $additional_dd_code);
							?>
							</div>
                        </div>
						<div class="form-group row">
                            <label class="col-md-1 col-form-label" for="nav_url">Page Content</label>
                            <div class="col-md-4">                      
        	 				<select name="nav_url_content" id="nav_url_content" class="form-control bgc" tabindex="13">
        	 					<option value="">ไม่ใช้คอนเทนต์ที่มีอยู่</option>
        	 					<?php
        	 						foreach($content_page as $key => $selection ) :?>
        	 						<option value="<?=$selection['path'].$selection['id'];?>" <?php if(isset($nav_url_content) && $nav_url_content ==  $selection['path'].$selection['id']){echo 'selected';}?>>
        	 							<?php 
        	 								if($selection['id'] != ''){
        	 									echo " - ".$selection['title'];
        	 								}
        	 								else{
        	 									echo " ".$selection['title'];
        	 								}
        	 							?></option>
        	 					<?php endforeach;?>  

        	 				</select>
							</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-1 col-form-label" for="nav_content">Content</label>
                            <div class="col-md-11">
								<textarea id="editor4" class="ckeditor" name="nav_content"><?=$nav_content;?></textarea>
							</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-1 col-form-label" for="order_priority">ลำดับเมนู</label>
                            <div class="col-md-4">
								<select class="form-control-sm" name="order_priority">
									<option <?php if($order_priority==$order_priority){ echo 'selected="selected"'; } ?> value="<?=$order_priority; ?>">
										<?=$order_priority; ?>
									</option>
									<?php for($i=1; $i<=50; $i++){ ?>
										<option value="<?=$i; ?>"><?=$i; ?></option>
									<?php } ?>
								</select>
							</div>
                        </div>
                    	<?php
							if(is_numeric($update_id)){
								echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">แก้ไขเมนู</button>';
							}else{
								echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">เพิ่มเมนู</button>';
                    		}
						?>
                        <button type="submit" id="submit" name="submit" value="Cancel" class="btn btn-sm btn-warning m-r-5">ยกเลิก</button>
                    </form>
				</div>
				<!-- end panel-body -->
			</div>
			<!-- end panel -->
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
</div>
<!-- end #content -->
