<!-- DOCTOR WORK -->
<!-- begin col-12 -->
<div class="col-lg-12" id="col_add_doctorwork">
	<!-- begin panel -->
	<div class="panel panel-inverse">
		<!-- begin panel-heading -->
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
			</div>
			<h4 class="panel-title">จัดการวันและเวลาปฏิบัติงานของแพทย์</h4>
		</div>
		<!-- end panel-heading -->
		<!-- begin panel-body -->
		<div class="panel-body">
			<form id="form_DoctorWork_ADD" name="form_DoctorWork_ADD" class="form-horizontal" method="POST" enctype="multipart/form-data"> 
				<div class="row row-space-10">
                    <div class="col-md-3">
						<div class="form-group">
                            <label for="medical_id">รหัสแพทย์<span class="text-danger">*</span></label>
							<input type="text" class="form-control" id="medical_id" name="medical_id">		                            
                        </div>
                    </div>
                    <div class="col-md-3">
						<div class="form-group">
                            <label for="work_days">วันทำงานแพทย์<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="work_days" name="work_days" placeholder="Example : วันจันทร์ - ศุกร์" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label for="work_times_in">เวลาเข้างานแพทย์<span class="text-danger">*</span></label>
                    	<div class="input-group date" id="timepicker_in">
							<input type="text" class="form-control" name="work_times_in" />
							<span class="input-group-addon">
							<i class="fa fa-clock"></i>
							</span>
						</div>
                    </div>
                    <div class="col-md-3">
                        <label for="work_times_out">เวลาออกงานแพทย์<span class="text-danger">*</span></label>
                    	<div class="input-group date" id="timepicker_out">
							<input type="text" class="form-control" name="work_times_out" />
							<span class="input-group-addon">
							<i class="fa fa-clock"></i>
							</span>
						</div>
                    </div>
                </div>
                <button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">แก้ไขข้อมูลวันและเวลาปฏิบัติงานแพทย์</button>
                <!-- เพิ่มข้อมูลวันและเวลาปฏิบัติงานแพทย์ -->
                <!-- แก้ไขข้อมูลวันและเวลาปฏิบัติงานแพทย์ -->
                <a href="javascript:;" class="btn btn-sm btn-warning m-r-5" data-click="panel-reload">ยกเลิก</a>
            </form>
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->
</div>
<!-- end col-12 -->	
