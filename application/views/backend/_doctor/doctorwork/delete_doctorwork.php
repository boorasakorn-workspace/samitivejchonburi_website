<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
		<li class="breadcrumb-item active">Delete Doctor</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header text-center"><?=$headline?></h1>
	<!-- end page-header -->

    <!-- begin row -->
    <div class="row">
	    <!-- begin col-12 -->
	    <div class="col-lg-12">
	        <!-- begin panel -->
            <div class="panel panel-inverse">
                <!-- begin panel-heading -->
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">ยืนยันการลบ</h4>
                </div>
                <!-- end panel-heading -->
                <!-- begin panel-body -->
                <div class="panel-body">
				<?php 
					if(isset($flash)){
						echo $flash;
					}
				?>
				 	<?php 
						$attributes = array('class' => 'form_horizontal');
						echo form_open_multipart('backend/src/DoctorWork/delete_doctorwork_process/'.$update_id, $attributes); 
					?>
					<label class="control-label">คุณต้องการลบแพทย์คนนี้จริงหรือไม่ ? </label>
				</div>
				<div class="hljs-wrapper">
					<div class="row">
						<div class="col-md-6">
							<button type="submit" name="submit" value="Yes - Delete Doctor Work" class="btn btn-block btn-danger">ยืนยัน</button>
						</div>
						<div class="col-md-6">
							<button type="submit" name="submit" value="Cancel" class="btn btn-block btn-lime">ยกเลิก</button>
						</div>
					</div>
				</div>
				</form>
                </div>
                <!-- end panel-body -->
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
</div>