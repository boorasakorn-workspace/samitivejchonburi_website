<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
		<li class="breadcrumb-item active">Doctor Form</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header"><?=$headline; ?></h1>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">จัดการ Doctor Details</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<?=validation_errors("<p style='color:red;'>", "</p>") ?>
					<?php
					if(is_numeric($update_id)) {
						?>
						<div class="form-group row fileupload-buttonbar">
							<div class="col-md-12">
								<span class="fileinput-button m-r-3">
									<?php if($doctor_imgs==''){ ?>
										<i class="fa fa-plus"></i>
										<!--
										<a href="<?=base_url(); ?>backend/src/Doctor/upload_image/<?=$update_id; ?>">
											<button type="button" class="btn btn-primary" id="upload_img" name="upload_img">Upload Doctor Image</button>
										</a>
										-->

		                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#uploadImgModal">Upload Doctor Image</button>

										<!-- Modal -->
										<div id="uploadImgModal" class="modal fade" role="dialog">
										  <div class="modal-dialog">
										    <!-- Modal content-->
										    <div class="modal-content">
										      <div class="modal-header">
										        <button type="button" class="close" data-dismiss="modal">&times;</button>
										        <h4 class="modal-title">Upload Doctor Image</h4>
										      </div>
										      <div class="modal-body">
							        			<form id="DoctorImageForm" name="DoctorImageForm" enctype="multipart/form-data" method="post" >
												<label class="control-label">Please Choose a file from your Computer </label>
												<input type="file" name="file" size="20">
										      </div>
										      <div class="modal-footer">
												<button type="submit" id="Imagesubmit" name="submit" value="Submit" class="btn btn-block btn-success">Upload</button>
										        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</form>
										      </div>
										    </div>

										  </div>
										</div>


									<?php }else{ ?>
										<i class="fa fa-times"></i>
										<!--
										<a href="<?=base_url(); ?>backend/src/Doctor/delete_image/<?=$update_id; ?>">
											<button type="button" class="btn btn-danger" id="delete_img" name="delete_img">Delete Doctor Image</button>
										</a>
										-->										                                

										<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteImgModal">Delete Doctor Image</button>

										<!-- Modal -->
										<div id="deleteImgModal" class="modal fade" role="dialog">
										  <div class="modal-dialog">
										    <!-- Modal content-->
										    <div class="modal-content">
										      <div class="modal-header">
										        <button type="button" class="close" data-dismiss="modal">&times;</button>
										        <h4 class="modal-title">Delete Doctor Image</h4>
										      </div>
										      <div class="modal-body">
										        <p>Sure to delete doctor image?</p>
										      </div>
										      <div class="modal-footer">
										      	<a href="<?=base_url(); ?>backend/src/Doctor/delete_image/<?=$update_id; ?>">
													<button type="button" class="btn btn-danger" id="delete_img" name="delete_img">Delete Doctor Image</button>
												</a>
										        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
										      </div>
										    </div>

										  </div>
										</div>

									<?php } ?>


									<?php if($doctor_video==''){ ?>

		                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#uploadVidModal">Upload Doctor Video</button>

										<!-- Modal -->
										<div id="uploadVidModal" class="modal fade" role="dialog">
										  <div class="modal-dialog">
										    <!-- Modal content-->
										    <div class="modal-content">
										      <div class="modal-header">
										        <button type="button" class="close" data-dismiss="modal">&times;</button>
										        <h4 class="modal-title">Upload Doctor Video</h4>
										      </div>
										      <div class="modal-body">
							        			<form id="DoctorVidForm" name="DoctorVidForm" enctype="multipart/form-data" method="post" >
												<label class="control-label">Please Choose a file from your Computer </label>
												<input type="file" name="file_vid">
										      </div>
										      <div class="modal-footer">
												<button type="submit" id="Vidsubmit" name="submit" value="Submit" class="btn btn-block btn-success">Upload</button>
										        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</form>
										      </div>
										    </div>

										  </div>
										</div>

									<?php }else{ ?>

										<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteVidModal">Delete Doctor Video</button>

										<!-- Modal -->
										<div id="deleteVidModal" class="modal fade" role="dialog">
										  <div class="modal-dialog">
										    <!-- Modal content-->
										    <div class="modal-content">
										      <div class="modal-header">
										        <button type="button" class="close" data-dismiss="modal">&times;</button>
										        <h4 class="modal-title">Delete Doctor Video</h4>
										      </div>
										      <div class="modal-body">
										        <p>Sure to delete doctor video?</p>
										      </div>
										      <div class="modal-footer">
										      	<a href="<?=base_url(); ?>backend/src/Doctor/delete_video/<?=$update_id; ?>">
													<button type="button" class="btn btn-danger" id="delete_vid" name="delete_vid">Delete Doctor Video</button>
												</a>
										        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
										      </div>
										    </div>

										  </div>
										</div>

									<?php } ?>


									<!--
									<a href="<?=base_url(); ?>backend/src/Doctor/delete_doctor/<?=$update_id; ?>">
										<button type="button" class="btn btn-danger" id="delete_doctor" name="delete_doctor">Delete Doctor</button>
									</a>
									-->

									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">Delete Doctor</button>

									<!-- Modal -->
									<div id="deleteModal" class="modal fade" role="dialog">
									  <div class="modal-dialog">
									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">Delete Doctor</h4>
									      </div>
									      <div class="modal-body">
									        <p>Sure to delete doctor?</p>
									      </div>
									      <div class="modal-footer">
											<a href="<?=base_url(); ?>backend/src/Doctor/delete_doctor/<?=$update_id; ?>">
												<button type="button" class="btn btn-danger" id="delete_doctor" name="delete_doctor">Delete Doctor</button>
											</a>
									        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
									      </div>
									    </div>

									  </div>
									</div>

									<a href="<?=base_url(); ?>backend/src/Doctor/view_doctor_details/<?=$update_id; ?>">
										<button type="button" class="btn btn-success" id="view_doctor" name="view_doctor"><i class="fas fa-eye"></i> View Doctor In Front</button>
									</a>
								</span>
							</div>
						</div>
					<?php }
					$location_form = base_url()."backend/src/Doctor/create/".$update_id;
					?>					
	                <div class="row row-space-10">
	                    <div class="col-md-4" id="PreviewSection" style="width: 0px;height: 0px;opacity: 0;">
	                    	<label for="package_preview_img" >Preview Upload Image |  </label>
	                    	<button type="button" class="btn btn-danger" id="deletePreview" name="deletePreview">Delete</button>
	                        <div class="form-group" id="preimage_section"></div>
	                    </div>
	                        
	                </div>

	                <div class="row row-space-10">
	                    <div class="col-md-4" id="VidPreviewSection" style="width: 0px;height: 0px;opacity: 0;">
	                    	<label for="package_preview_vid" >Preview Upload Video |  </label>
	                    	<button type="button" class="btn btn-danger" id="deletePreviewVid" name="deletePreviewVid">Delete</button>
	                        <div class="form-group" id="previd_section"></div>
	                    </div>
	                        
	                </div>

					<form id="formContents" class="form-horizontal" action="<?= $location_form; ?>" method="POST" enctype="multipart/form-data">
						<input type="hidden" name="hidden_id" value="<?=$update_id;?>">
						<!-- Thailand -->
						<div class="hljs-wrapper">
							<h4 class="text-lime"><span class="bold">ชื่อ (ภาษาไทย) <i class="fas fa-hand-point-down"></i></span></h4>
						</div>

						<div class="row row-space-10">
							<div class="col-md-3">
								<div class="form-group">
									<label for="pname_th">คำนำหน้า<span class="text-danger">*</span></label>
									<?php
									$additional_dd_code = "id='selectPNameTH' class='selectpicker' data-live-search='true' ";
									$options = array(
										'รศ.นพ.' => 'รองศาสตราจารย์นายแพทย์',
										'นพ.' => 'นายแพทย์',
										'พญ.' => 'แพทย์หญิง',
										'ทพ.' => 'ทันตแพทย์',
										'ทพญ.' => 'ทันตแพทย์หญิง',
										'ภก.' => 'เภสัชกร',
										'ภกญ.' => 'เภสัชกรหญิง',
										'ทนพญ.' => 'เทคนิคการแพทย์',
										'พจ.' => 'แพทย์จีน',
										'พย.' => 'พยาบาล',
										'พยช.' => 'พยาบาลชาย',
									);

									echo form_dropdown('pname_th', $options, $pname_th, $additional_dd_code);
									?>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="firstname_th">ชื่อจริง (ไทย)<span class="text-danger">*</span></label>
									<input type="text" class="form-control" id="firstname_th" name="firstname_th" value="<?=$firstname_th; ?>" />
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="lastname_th">นามสกุล (ไทย)<span class="text-danger">*</span></label>
									<input type="text" class="form-control" id="lastname_th" name="lastname_th" value="<?=$lastname_th; ?>" />
								</div>
							</div>
						</div>
						<div class="row row-space-10">
							<div class="col-md-12">
								<div class="form-group">
									<label for="education_th">ประวัติการศึกษา<span class="text-danger">*</span></label>
									<textarea class="ckeditor" id="editor6" name="education_th" rows="20"><?php echo $education_th; ?></textarea>
								</div>
							</div>
							<!-- 
							<div class="col-md-6">
								<div class="form-group">
									<label for="experience_th">ประสบการณ์ทำงาน<span class="text-danger">*</span></label>
									<textarea class="form-control" rows="3" name="experience_th" rows="20"><?php echo $experience_th; ?></textarea>
								</div>
							</div>
							 -->
						</div>
						<div class="row row-space-10">
							<div class="col-md-12">
								<div class="form-group">
									<label for="doctor_description_th">ความเห็นเพิ่มเติมจากแพทย์</label>
									<textarea class="ckeditor" id="editor7" name="doctor_description_th" rows="20"><?php echo $doctor_description_th; ?></textarea>
								</div>
							</div>
						</div>
						<?php /*
 						<div class="row row-space-10">							
							<!-- <div class="col-md-10"> -->								
								<div class="form-group">
									<textarea hidden class="form-control" rows="3" name="speciality_th" rows="20"><?php echo $speciality_th."speciality_th"; ?></textarea>
									<label for="speciality_th">ความเชี่ยวชาญ<span class="text-danger">*</span></label>
										<?php $i=0; foreach ($data_speciality as $result) : $i++;?>
											<div class="col-4" style="padding: 3px;">
												<input type="checkbox" name="special_id[]" value="<?php echo $result->speciality_id;?>" <?php if( strpos( $speciality_id, ''.$i.'' ) !== false) { echo "checked";}?>><?php echo $result->speciality_name;?>
											</div>
										<?php endforeach;?>
								</div>
							<!-- </div> -->
						</div>	
						</div>				
 						*/ ?>
						<div class="row row-space-10">
							<!--
							<div class="col-10">
								<label for="speciality_th">ความเชี่ยวชาญ<span class="text-danger">*</span></label>
							</div>
							-->
							<?php /*
								<?php $i=0; foreach ($data_speciality as $result) : $i++;?>
									<div class="col-4" style="padding: 3px;">
										<input type="checkbox" name="special_id[]" value="<?php echo $result->speciality_id;?>" <?php if( strpos( $speciality_id, ''.$i.'' ) !== false) { echo "checked";}?>><?php echo $result->speciality_name;?>
									</div>
								<?php if($i%3 == 0){echo '</div><div class="row row-space-10">';}?>
								<?php endforeach;?>
							*/ ?>
						</div>
							<!-- </div> -->				
						<div class="row row-space-10">	
							<?php /*
							<!--
							<div class="col-md-2">
								<div class="form-group">
									<label for="doctor_department">แพทย์ประจำศูนย์<span class="text-danger">*</span></label>
									<select class="form-control" name="doctor_department">
										<option value=""></option>										
										<?php 
										$doctor_department;
										foreach ($medical_center as $medical_center) :?>
											<option value="<?php echo $medical_center->medical_master_id;?>" <?php if($medical_center->medical_master_id == $doctor_department){echo 'selected';}?> >
												<?php echo $medical_center->medical_center_name;?> 
											</option>
										<?php endforeach;?>
									</select>
								</div>
							</div>
							<!--
							*/ ?> 
							<?php /*
							<!--
							<div class="col-md-2">
								<div class="form-group">
									<label for="doctor_department">แพทย์ประจำศูนย์เด็ก</label>
									<select class="form-control" name="doctor_department_child">
										<option value="0"></option>
									</select>
								</div>
							</div>
							 -->
							*/ ?> 
							<div class="col-md-12">
								<div class="form-group">
									<label for="doctor_department">ภาษาที่แพทย์สามารถใช้ได้<span class="text-danger">*</span></label>
									<?php
									/*
									$additional_dd_code = "id='selectLanguageTH' class='form-control'";
									$options = array(
										'0' => 'ไทย',
										'1' => 'อังกฤษ',
										'2' => 'จีน',
										'3' => 'จีนแต้จิ๋ว',
										'4' => 'จีนแมนดาริน',
										'5' => 'ญี่ปุ่น',
										'6' => 'ลาว',
									);

									echo form_dropdown('language_th', $options, $language_th, $additional_dd_code);
									*/?>
									<input class="form-control" type="text" name="language_th" id="language_th" value="<?=$language_th;?>" placeholder="ไทย,อังกฤษ" />
								</div>
							</div>
						</div>
						
						<!-- End Thailand -->
						<div class="hljs-wrapper">
							<h4 class="text-lime"><span class="bold">Name (English) <i class="fas fa-hand-point-down"></i></span></h4>
						</div>
						<!-- ENGLISH -->
						<div class="row row-space-10">
							<div class="col-md-2">
								<div class="form-group">
									<label for="pname_en">Title Name<span class="text-danger">*</span></label>
									<input type="text" class="form-control" id="pname_en" name="pname_en" value="<?=$pname_en; ?>" />
								</div>
							</div>
							<div class="col-md-5">
								<div class="form-group">
									<label for="firstname_en">Firstname<span class="text-danger">*</span></label>
									<input type="text" class="form-control" id="firstname_en" name="firstname_en" value="<?=$firstname_en; ?>" />
								</div>
							</div>
							<div class="col-md-5">
								<div class="form-group">
									<label for="lastname_en">Lastname<span class="text-danger">*</span></label>
									<input type="text" class="form-control" id="lastname_en" name="lastname_en" value="<?=$lastname_en; ?>" />
								</div>
							</div>
						</div>
						<div class="row row-space-10">
							<div class="col-md-12">
								<div class="form-group">
									<label for="education_en">Education<span class="text-danger">*</span></label>
									<textarea class="ckeditor" id="editor8" name="education_en" rows="20"><?php echo $education_en; ?></textarea>
								</div>
							</div>
							<!-- 
							<div class="col-md-6">
								<div class="form-group">
									<label for="experience_en">Experience<span class="text-danger">*</span></label>
									<textarea class="form-control" rows="3" name="experience_en" rows="20"><?php echo $experience_en; ?></textarea>
								</div>
							</div>
							 -->
						</div>		
						<div class="row row-space-10">
							<div class="col-md-12">
								<div class="form-group">
									<label for="doctor_description_en">Doctor Description</label>
									<textarea class="ckeditor" id="editor9" name="doctor_description_en" rows="2"><?php echo $doctor_description_en; ?></textarea>
								</div>
							</div>
						</div>				
						<div class="row row-space-10">
							<!--
							<div class="col-10">
								<label for="speciality_en">Speciality<span class="text-danger">*</span></label>
							</div>
							-->
							<?php /*
							<?php $i=0; foreach ($data_speciality as $result) : $i++;?>
								<div class="col-4" style="padding: 3px;">
									<input type="checkbox" name="special_id[]" value="<?php echo $result->speciality_id;?>" <?php if( strpos( $speciality_id, ''.$i.'' ) !== false) { echo "checked";}?>><?php echo $result->speciality_name_en;?>
								</div>
							<?php if($i%3 == 0){echo '</div><div class="row row-space-10">';}?>
							<?php endforeach;?>
							*/ ?>
						</div>
						<div class="row row-space-10">
							<!-- 
							<div class="col-md-2">
								<div class="form-group">
									<label for="doctor_department">Medical Center<span class="text-danger">*</span></label>
									<?php /*
									<select class="form-control" name="doctor_department_en">
										<option value=""></option>
									<?php $i=0; foreach ($medical_center as $medical_center) : $i++;?>
											<option value="">
												<?=$medical_center->medical_center_name;?>
												<!-- <?php echo $medical_center->medical_center_name;?>  -->
											</option>
										<?php endforeach;?> 
									</select>*/
									?>
								</div>
							</div> 
							<div class="col-md-2">
								<div class="form-group">
									<label for="doctor_department">Medical Center Child</label>
									<select class="form-control" name="doctor_department">
										<option value="Default"></option>
									</select>
								</div>
							</div> 
							-->
							<div class="col-md-12">
								<div class="form-group">
									<label for="doctor_department">Doctor Language<span class="text-danger">*</span></label>
									<?php 
									/*
									$additional_dd_code = "id='selectLanguageEN' class='form-control'";
									$options = array(
										'0' => 'Thai',
										'1' => 'English',
										'2' => 'Chinese',
										'3' => 'Chinese (Mandarin)',
										'4' => 'Chinese (Teo Chew)',
										'5' => 'Japanese',
										'6' => 'Lao',
									);

									echo form_dropdown('language_en', $options, $language_en, $additional_dd_code); 
									*/
									?>
									<input class="form-control" type="text" name="language_en" id="language_en" value="<?=$language_en;?>" placeholder="Thai,English"  />
								</div>
							</div>
						</div>

						<!-- End Thailand -->

						<div class="hljs-wrapper">
							<h4 class="text-lime"><span class="bold"> ความเชี่ยวชาญ / Specialty <i class="fas fa-hand-point-down"></i></span></h4>
						</div>
						<div class="row row-space-10">
							<div class="col-md-4">
								<div class="form-group">
									<label for="parent">Select Medical Center</label><label style="float: right;">  |  <button style="padding:1px;" class="btn btn-primary" type="button" id="addParent">ADD</button></label>
									<select class="selectpicker" data-live-search="true" name="parent" id="parent">
										<option value="">Select Medical Center</option>
										<?php
											foreach ($medical_center as $dropdown_med):
										?>
											<option value="<?=$dropdown_med->medical_center_name;?>">
												<?=$dropdown_med->medical_center_name;?>
											</option>
										<?php
											endforeach
										?>									
									</select>
								</div>
							</div>
							<div class="col-md-8">
								<div class="form-group">
									<label for="parent_medical_center_name">Medical Center</label>
									<input type="text" class="form-control" id="parent_medical_center_name" name="parent_medical_center_name" value="<?php if(isset($parent_medical_center_name)){echo $parent_medical_center_name;} ?>" />
								</div>
							</div>
						</div>
						<div class="row row-space-10">
							<div class="col-md-4">
								<div class="form-group">
									<label for="parent_specialty">Select Specialty</label><label style="float: right;">  |  <button style="padding:1px;" class="btn btn-primary" type="button" id="addSpecialtyParent">ADD</button></label>
									<select class="selectpicker" data-live-search="true" name="parent_specialty" id="parent_specialty">
										<option value="">Select Specialty</option>
										<?php
											foreach ($all_specialty as $dropdown_specialty):
										?>
											<option value="<?=$dropdown_specialty->specialty_name_th;?>">
												<?=$dropdown_specialty->specialty_name_th;?>
											</option>
										<?php
											endforeach
										?>								
									</select>
								</div>
							</div>
							<div class="col-md-8">
								<div class="form-group">
									<label for="parent_specialty_name">Specialty</label>
									<input type="text" class="form-control" id="parent_specialty_name" name="parent_specialty_name" value="<?php if(isset($parent_specialty_name)){echo $parent_specialty_name;} ?>" />
								</div>
							</div>
						</div>

						<div class="row row-space-10">
							<div class="col-md-4">
								<div class="form-group">
									<label for="parent_sub_specialty">Select SubSpecialty</label><label style="float: right;">  |  <button style="padding:1px;" class="btn btn-primary" type="button" id="addSubSpecialtyParent">ADD</button></label>
									<select class="selectpicker" data-live-search="true" name="parent_sub_specialty" id="parent_sub_specialty">
										<option value="">Select Sub Specialty</option>
										<?php
											foreach ($all_sub_specialty as $dropdown_sub_specialty):
										?>
											<option value="<?=$dropdown_sub_specialty->sub_specialty_name_th;?>">
												<?=$dropdown_sub_specialty->sub_specialty_name_th;?>
											</option>
										<?php
											endforeach
										?>								
									</select>
								</div>
							</div>
							<div class="col-md-8">
								<div class="form-group">
									<label for="parent_specialty_name">Sub Specialty</label>
									<input type="text" class="form-control" id="parent_sub_specialty_name" name="parent_sub_specialty_name" value="<?php if(isset($parent_sub_specialty_name)){echo $parent_sub_specialty_name;} ?>" />
								</div>
							</div>
						</div>

						<div class="hljs-wrapper">
							<h4 class="text-lime"><span class="bold">ข้อมูลส่วนตัว <i class="fas fa-hand-point-down"></i></span></h4>
						</div>
						<!-- ENGLISH -->
						<?php /*
						<div class="row row-space-10">
							<div class="col-md-6">
								<div class="form-group">
									<label for="phone">เบอร์โทรศัพท์<span class="text-danger">*</span></label>
									<input type="text" class="form-control" id="phone" name="phone" value="<?=$phone; ?>" />
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="email">อีเมลล์<span class="text-danger">*</span></label>
									<input type="text" class="form-control" id="email" name="email" value="<?=$email; ?>" />
								</div>
							</div>
						</div> */ ?>
						<div class="row row-space-10">
							<?php /*
							<div class="col-md-3">
								<div class="form-group">
									<label for="gender">เพศ<span class="text-danger">*</span></label>
									<?php
									$additional_dd_code = "id='selectGender' class='form-control'";
									$options = array(
										'ไม่ระบุ' => 'ไม่ระบุ',
										'ชาย' => 'ชาย',
										'หญิง' => 'หญิง'
									);

									echo form_dropdown('gender', $options, $gender, $additional_dd_code);
									?>
								</div>
							</div>
							*/ ?>
							<div class="col-md-3">
								<div class="form-group">
									<label for="medical_id">รหัสแพทย์<span class="text-danger">*</span></label>
									<input type="text" class="form-control" id="medical_id" name="medical_id" value="<?=$medical_id; ?>" />
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label for="type_th">สังกัดโรงพยาบาล<span class="text-danger">*</span></label>
									<?php
									$additional_dd_code = "id='selectStatusCode' class='form-control'";
									$options = array(
										'ไม่ระบุ' => 'ไม่ระบุ',
										'แพทย์ประจำ' => 'แพทย์ประจำ',
										'แพทย์นอกเวลา' => 'แพทย์นอกเวลา'
									);

									echo form_dropdown('type_th', $options, $type_th, $additional_dd_code);
									?>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label for="type_th">Doctor Work at Hospital</label>
									<?php
									$additional_dd_code = "id='selectAtHospital' class='form-control'";
									$options = array(
										'Unknown' => 'Unknown',
										'Fulltime' => 'Fulltime',
										'Parttime' => 'Parttime'
									);

									echo form_dropdown('type_en', $options, $type_en, $additional_dd_code);
									?>
								</div>
							</div>
						</div>
						<!--
						<div class="row row-space-10">
							<div class="col-md-10">
								<div class="form-group">
									<label for="status">ที่อยู่แพทย์ / Address<span class="text-danger">*</span></label>
									<textarea name="address" rows="6" cols="150"><?=$address; ?></textarea>
								</div>
							</div>
						</div>
						-->
						<div class="row row-space-10">
							<div class="col-md-6">
								<div class="form-group">
									<label for="status">สถานะ<span class="text-danger">*</span></label>
									<?php
									$additional_dd_code = "id='selectStatusCode' class='form-control'";
									$options = array(
										'' => 'โปรดเลือกสถานะของแพทย์',
										'1' => 'Active',
										'0' => 'Inactive'
									);

									echo form_dropdown('status', $options, isset($status)?$status:'1', $additional_dd_code);
									?>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="at_hospital">โรงพยาบาลที่แพทย์ประจำ<span class="text-danger">*</span></label>
									<?php
									$additional_dd_code = "id='selectStatusCode' class='form-control'";
									$options = array(
										'สมิติเวช ชลบุรี' => 'สมิติเวช ชลบุรี',
										'ทุกโรงพยาบาล' => 'ทุกโรงพยาบาล',
										'สมิติเวช สุขุมวิท' => 'สมิติเวช สุขุมวิท',
										'สมิติเวช ศรีนครินทร์' => 'สมิติเวช ศรีนครินทร์',
										'สมิติเวช ศรีราชา' => 'สมิติเวช ศรีราชา',
										'สมิติเวช ธนบุรี' => 'สมิติเวช ธนบุรี',										
										'โรงพยาบาลเด็กสมิติเวช' => 'โรงพยาบาลเด็กสมิติเวช'
									);

									echo form_dropdown('at_hospital', $options, $at_hospital, $additional_dd_code);
									?>
								</div>
							</div>
						</div>

						<div class="row row-space-10">
		                    <div class="col-md-6">
								<div class="form-group">
		                            <label for="video">Video (Youtube Link)</label>
	                            	<input type="text" class="form-control" id="doctor_video" name="doctor_video" placeholder="https://www.youtube.com/watch?v=foobar , https://youtu.be/foobar" value="<?=$doctor_video; ?>" />
		                        </div>
		                    </div>
						</div>
						
						<?php if($doctor_imgs!=''){ //ต้องทำการส่งค่า item_big_img มาจาก controller store_items->create ก่อน ?>
							<div class="form-group row">
								<label class="col-md-1 col-form-label">Doctor Image</label>
								<div class="col-md-4">
									<!--
									<img src="<?=base_url(); ?>gallery/doctor/big_imgs/<?=$doctor_imgs; ?>">
									-->
									<img class="img-responsive" style="height:320px; width:260px;" src="<?=base_url(); ?>gallery/doctor/big_imgs/<?=$doctor_imgs; ?>">
								</div>
							</div>
						<?php }
						if(is_numeric($update_id)){
							echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">แก้ไขข้อมูลแพทย์</button>';
						}else{
							echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">เพิ่มข้อมูลแพทย์</button>';
						}
						?>
						<button type="button" id="previewContent" name="previewContent" value="Preview" class="btn btn-sm btn-primary m-r-5">พรีวิว</button>
						<button type="submit" id="submit" name="submit" value="Cancel" class="btn btn-sm btn-warning m-r-5">ยกเลิก</button>
					</form>
				</div>
				<!-- end panel-body -->
			</div>
			<!-- end panel -->
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->

<!-- end #content -->
