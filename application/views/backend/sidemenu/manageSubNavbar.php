<!-- begin #content -->
<div id="content" class="content">
	<h1>จัดการส่วน Sub Menu</h1>
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
		<li class="breadcrumb-item active">รายละเอียด Sub Menu</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<?php
		$back_navbar_url = base_url()."backend/src/Sidebar/manage";
		$create_subnavbar_url = base_url()."backend/src/Sidebar/create_subnav";
	?>
	<h1 class="page-header">
		<a href="<?=$back_navbar_url; ?>"><button type="button" class="btn btn-warning">กลับไปหน้าเมนูหลัก</button></a>
		<a href="<?=$create_subnavbar_url; ?>"><button type="button" class="btn btn-lime">Add New Sub Menu</button></a></small>
	</h1>


	<?php
		if(isset($flash)){
			echo $flash;
		}
	?>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-lg-12">
			<div class="panel panel-inverse">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">จัดการ Sub Menu</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<table id="data-table-buttons" class="table table-bordered">
						<thead>
							<tr>
								<th width="1%" class="text-nowrap">#</th>
								<th class="text-nowrap">ชื่อเมนูย่อย</th>
 								<th class="text-nowrap">เมนูย่อยของเมนูย่อย
								<th class="text-nowrap">ลำดับเมนู</th>
								<th class="text-nowrap">สถานะ</th>
								<th class="text-nowrap">แก้ไข</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach($query->result() as $rows){
								$num_sub_navigation = $subofsubnav->_count_sub_of_sub_nav();

								$edit_submenu_url = base_url()."backend/src/Sidebar/create_subnav/".$rows->id;
								$delete_submenu_url = base_url()."backend/src/Sidebar/delete_menu_subnav/".$rows->id;
								$status = $rows->sub_sidemenu_status;
								if($status == 1){
									$status_label = "green";
									$status_desc = "Published";
								}else{
									$status_label = "secondary";
									$status_desc = "Unpublished";
								}
							?>
							<tr class="odd gradeX">
								<td width="1%" class="text-nowrap"><?=$rows->id; ?></td>
								<td><?=$rows->sub_sidemenu_title; ?></td>
								<td>
									<?php
										if($num_sub_navigation < 1){
											echo "-";
										}else{
											if($num_sub_navigation == 1){
												$entity = "คลิกเพื่อดูเมนูย่อยของเมนูย่อย";
											}else{
												$entity = "คลิกเพื่อดูเมนูย่อยของเมนูย่อย";
											}

											$subofsubmenu_url = base_url()."backend/src/Sidebar/manage_subsubnav/".$rows->id;

	echo '<a class="btn btn-block btn-purple" href="'.$subofsubmenu_url.'"><i class="fas fa-eye"></i> '.$entity.'</a>';
										}
									?>
								</td> 
								<td class="text-nowrap"><?=$rows->order_priority; ?></td>
								<td>
									<span class="label label-<?=$status_label; ?>"><?=$status_desc; ?></span>
								</td>
								<td width="30%" class="text-center">
									<a class="btn btn-xs btn-warning" href="<?=$edit_submenu_url; ?>">
										<i class="fas fa-edit"></i>
									</a>
									<a class="btn btn-xs btn-danger" href="<?=$delete_submenu_url; ?>">
										<i class="fas fa-trash"></i>
									</a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
				<!-- end panel-body -->
			</div>
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
</div>