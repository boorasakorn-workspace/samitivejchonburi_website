<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
		<li class="breadcrumb-item active">Package</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header"><?=$headline; ?></h1>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">เพิ่มข้อมูลแพ็กเกจ</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<?=validation_errors("<p style='color:red;'>", "</p>") ?>
					<?php
						if(isset(explode('_',$update_id)[1]) && explode('_',$update_id)[1] == '2'){
							$language = "EN";
						}
						else{
							$language = "TH";
						}
						$update_id = explode('_',$update_id)[0];

						if(is_numeric($update_id)) {							
					?>
                        <div class="form-group row fileupload-buttonbar">
                            <div class="col-md-12">
                                <span class="fileinput-button m-r-3">
									<?php if($package_big_img==''){ ?>
                                    <i class="fa fa-plus"></i>
                                    <!--
                                    <a href="<?=base_url(); ?>backend/src/Package/upload_image/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-primary" id="upload_img" name="upload_img">Upload Package Image</button>
                                    </a>
	                                -->
	                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#uploadImgModal">Upload Package Image</button>

									<!-- Modal -->
									<div id="uploadImgModal" class="modal fade" role="dialog">
									  <div class="modal-dialog">
									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">Upload Package Image</h4>
									      </div>
									      <div class="modal-body">
						        			<form id="PackageImageForm" name="PackageImageForm" enctype="multipart/form-data" method="post" >
											<label class="control-label">Please Choose a file from your Computer </label>
											<input type="file" name="file" size="20">
									      </div>
									      <div class="modal-footer">
											<button type="submit" id="Imagesubmit" name="submit" value="Submit" class="btn btn-block btn-success">Upload</button>
									        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											</form>
									      </div>
									    </div>

									  </div>
									</div>

                                	<?php }else{ ?>
                                    <i class="fa fa-times"></i>
									<!--
									<a href="<?=base_url(); ?>backend/src/Package/delete_image/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-danger" id="delete_img" name="delete_img">Delete Package Image</button>
                                    </a>
	                                -->

									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteImgModal">Delete Package Image</button>

									<!-- Modal -->
									<div id="deleteImgModal" class="modal fade" role="dialog">
									  <div class="modal-dialog">
									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">Delete Package Image</h4>
									      </div>
									      <div class="modal-body">
									        <p>Sure to delete package image?</p>
									      </div>
									      <div class="modal-footer">
									      	<a href="<?=base_url(); ?>backend/src/Package/delete_image/<?=$update_id; ?>">
									      		<button type="button" class="btn btn-danger" id="delete_img" name="delete_img">Delete Package Image</button>
									      	</a>
									        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
									      </div>
									    </div>

									  </div>
									</div>

                                	<?php } ?>
                                	<!--
                                    <a href="<?=base_url(); ?>backend/src/Package/delete_package/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-danger" id="delete_package" name="delete_package">Delete Package</button>
                                    </a>
	                                -->

									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">Delete Package</button>

									<!-- Modal -->
									<div id="deleteModal" class="modal fade" role="dialog">
									  <div class="modal-dialog">
									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">Delete Package</h4>
									      </div>
									      <div class="modal-body">
									        <p>Sure to delete package?</p>
									      </div>
									      <div class="modal-footer">
		                                    <a href="<?=base_url(); ?>backend/src/Package/delete_package/<?=$update_id; ?>">
		                                    	<button type="button" class="btn btn-danger" id="delete_package" name="delete_package">Delete Package</button>
		                                    </a>
									        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
									      </div>
									    </div>

									  </div>
									</div>

                                    <a href="<?=base_url(); ?>backend/src/Package/view_package_details/<?=$update_id; ?>">
                                        <button type="button" class="btn btn-success" id="view_package" name="view_package"><i class="fas fa-eye"></i> View Package In Front</button>
                                    </a>
                                    <?php
										if($language == "EN"){
											echo '<a href="'.base_url().'backend/src/Package/create/'.explode('_',$update_id)[0].'">';
										}
										else{
											echo '<a href="'.base_url().'backend/src/Package/create/'.explode('_',$update_id)[0]."_2".'">';
										}
                                    ?>
                                        <button type="button" class="btn btn-success" id="switch_language" name="switch_language">Switch Language</button>
                                    </a>
                                </span>
                            </div>
                        </div>
					<?php }
						$location_form = base_url()."backend/src/Package/create/".$update_id;
					?>
					<form id="formContents" class="form-horizontal" action="<?= $location_form; ?>" method="POST" enctype="multipart/form-data">
						<H4><?=$language;?></H4>
						<div class="row">
							<div class="col-md-8">
								<div class="container-fluid">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label for="package_title">ชื่อ Title <span class="text-danger">*</span></label>
												<?php
													if($language == 'EN'){
														echo '<input type="hidden" class="form-control" id="package_title" name="package_title" value="'.$package_title.'" />
														<!-- <input type="hidden" id="package_name" name="package_name" value="'.$package_title.'" /> -->';
														echo '<input type="text" class="form-control" id="package_title_en" name="package_title_en" placeholder="ENG Title" value="'.$package_title_en.'" />
														<!-- <input type="hidden" id="package_name_en" name="package_name_en" value="'.$package_title_en.'" /> -->';
													}
													else{
														echo '<input type="text" class="form-control" id="package_title" name="package_title" placeholder="TH Title" value="'.$package_title.'" />
														<!-- <input type="hidden" id="package_name" name="package_name" value="'.$package_title.'" /> -->';
														echo '<input type="hidden" class="form-control" id="package_title_en" name="package_title_en" value="'.$package_title_en.'" />
														<!-- <input type="hidden" id="package_name_en" name="package_name_en" value="'.$package_title_en.'" /> -->';
													}
												?>		                            
											</div>
											<div class="form-group">
												<label for="package_subdesc">Package Sub Description</label>
												<?php
													if($language == 'EN'){
														echo '<textarea class="form-control" style="display:none;" name="package_subdesc">'.$package_subdesc.'</textarea>';
														echo '<textarea class="form-control" name="package_subdesc_en" placeholder="ENG Subdescription">'.$package_subdesc_en.'</textarea>';
													}
													else{
														echo '<textarea class="form-control" name="package_subdesc" placeholder="TH Subdescription" >'.$package_subdesc.'</textarea>';
														echo '<textarea class="form-control" style="display:none;" name="package_subdesc_en">'.$package_subdesc_en.'</textarea>';
													}
												?>
											</div>
										</div>
										<?php /*
										<div class="col-md-4">
											<div class="form-group">
												<label for="package_type">Package Type<span class="text-danger">*</span></label>
												<?php
												$addition_type = "id='selectType' class='form-control'";
												$type_options = array(
																'' => 'Select Type',
																'0' => 'Undefined',
																'1' => 'General'
															);
												echo form_dropdown('package_type', $type_options, $package_type, $addition_type);
												?>
											</div>         
											<div class="form-group">
												<label for="package_unitprice">Package Price</label>
												<input type="text" class="form-control" id="package_unitprice" name="package_unitprice" value="<?=$package_unitprice; ?>"  placeholder="Price" />
											</div>          
										</div> */ ?>

										<div class="col-md-6">
											<div class="form-group">
												<label for="package_name">Title แสดงผล <span class="text-danger">*</span></label>
												<?php
													if($language == 'EN'){
														$package_name_lang = $package_name_en;
														echo '<input type="hidden" class="form-control" package_name_input id="package_name" name="package_name" value="'.$package_name.'" maxlength="30" />';
														echo '<input type="text" class="form-control" package_name_input id="package_name_en" name="package_name_en" value="'.$package_name_en.'" maxlength="30" />';
													}
													else{
														$package_name_lang = $package_name;
														echo '<input type="text" class="form-control" package_name_input id="package_name" name="package_name" value="'.$package_name.'" maxlength="30" />';
														echo '<input type="hidden" class="form-control" package_name_input id="package_name_en" name="package_name_en" value="'.$package_name_en.'" maxlength="30" />';
													}
												?>		                            
											</div>
											<div class="form-group">
												<label for="relate_medical_center_id">Related Medical Center</label>
												<select class="selectpicker" data-live-search="true" name="relate_medical_center_id" id="relate_medical_center_id">
													<option value="">Select Medical Center</option>
													<?php
														foreach ($medical_center as $dropdown_med):
													?>
														<option value="<?=$dropdown_med->id;?>" <?php if(isset($relate_medical_center_id) && $dropdown_med->id == $relate_medical_center_id){echo "selected";}?>>
															<?=$dropdown_med->medical_center_name;?>
														</option>
													<?php
														endforeach
													?>									
												</select>
											</div>         
											<div class="form-group">
												<label for="package_unitprice">Package Price</label>
												<input type="text" class="form-control" id="package_unitprice" name="package_unitprice" value="<?=$package_unitprice; ?>"  placeholder="Price" />
											</div>  
										</div>
									</div>
									<div class="row">
										<div class="col-md-2">
											<div class="form-group">
												<label for="package_status">Package Status</label>
												<select class="form-control" name="package_status" id="package_status">
													<option value="2" >Please Select</option>
													<option value="0" <?=($package_status == 0 ? "selected" : "");?> >Unpublished</option>
													<option value="1" <?=($package_status == 1 ? "selected" : "");?> >Published</option>
												</select>
											</div>
										</div>
										<div class="col-md-4">	
											<div class="form-group">
												<label for="package_startdate">Start Date<span class="text-danger">*</span></label>

												<?php 
												if($package_startdate == '1970-01-01'){
													$package_startdate = date("m/d/Y"); 
												}
												$package_startdate = strtotime($package_startdate);
												$package_startdate = date('m/d/Y',$package_startdate);
												?>
												<input type="text" class="form-control" id="datepicker-default" package_date="start" name="package_startdate" placeholder="Select Date" value="<?=$package_startdate; ?>" />
											</div>     
										</div>          	
										<div class="col-md-4">       
											<div class="form-group">
												<label for="package_enddate">End Date<span class="text-danger">*</span></label>
												<?php 
												if($package_enddate == '1970-01-01'){
													$package_enddate = date("m/d/Y"); 
												}
												$package_enddate = strtotime($package_enddate);
												$package_enddate = date('m/d/Y',$package_enddate);
												?>
												<input type="text" class="form-control" id="datepicker-inline" package_date="end" name="package_enddate" placeholder="Select Date" value="<?=$package_enddate; ?>" />
											</div>  
										</div>
									</div>
									<div class="row">
										<div class="col-md-4" id="PreviewSection" style="width: 0px;height: 0px;opacity: 0;">
											<label for="package_preview_img" >Preview Upload Image |  </label>
											<button type="button" class="btn btn-danger" id="deletePreview" name="deletePreview">Delete</button>
											<div class="form-group" id="preimage_section"></div>
										</div>
											
									</div>
									
								</div>
							</div>
							<div>                    
								<div class="pricing_table" style="float:left;width:255px">
									<div class="pricing_table_header ulockd-bghthm bgc-package" style="background-image: linear-gradient(#d0b176,#e3d5ae,#bda372);border-radius: 0 60px 0 0;overflow: hidden;    font-family: Kanit;">
										<h1 style="color:#fff;font-size:17px" preview_package="name" class="title font_PackageTitle"><?=isset($package_name_lang)?mb_substr($package_name_lang,0,30,'UTF-8'):'Title';?></h1>
										<img class="img-responsive hidden-xs" style="height:160px; width:100%;" <?=isset($package_small_img)&&$package_small_img?'src="'.base_url('gallery/package/small_imgs/').$package_small_img.'"':'';?> alt="">
										<span style="color:#fff;font-size:16px;;" class="price-value">฿ <?=$package_unitprice; ?>                              
										<span style="color:#fff;" preview_package="price" class="month hidden-xs">ราคาเริ่มต้น</span>
										</span>
										<span style="color: #000;;" preview_date><?=$package_startdate;?> - <?=$package_enddate;?></span>
									</div>									
								</div>
							</div>
						</div>
		                <div class="row row-space-10">
		                    <div class="col-md-12">
		                        <div class="form-group">
		                            <label for="package_description">Package Description</label>
		                            <?php
		                            	if($language == 'EN'){
		                            		echo '<textarea style="display:none;" name="package_description">'.$package_description.'</textarea>';
		                            		echo '<textarea id="editor2" class="ckeditor" name="package_description_en" >'.$package_description_en.'</textarea>';
				                        }
				                        else{
		                            		echo '<textarea id="editor2" class="ckeditor" name="package_description" >'.$package_description.'</textarea>';
		                            		echo '<textarea style="display:none;" name="package_description_en">'.$package_description_en.'</textarea>';
				                        }
		                            ?>
		                            
		                        </div>
		                    </div>
		                </div>
						<div class="form-group row">
                            <div class="col-md-4">
								<div class="form-group">
		                            <label for="meta_author">Meta Author</label>
		                            <input type="text" class="form-control" id="meta_author" name="meta_author" value="<?=$meta_author; ?>" placeholder="Author" />
	                            </div>  
		                    </div>
                            <div class="col-md-4">
								<div class="form-group">
		                            <label for="meta_keyword">Meta Keyword</label>
		                            <input type="text" class="form-control" id="meta_keywords" name="meta_keywords" value="<?=$meta_keywords; ?>" placeholder="Keywords"/>
	                            </div>  
		                    </div>
                            <div class="col-md-4">
								<div class="form-group">
		                            <label for="meta_desciption">Meta Description</label>
		                            <input type="text" class="form-control" id="meta_description" name="meta_description" value="<?=$meta_description; ?>" placeholder="Description"/>
	                            </div>  
		                    </div>
                        </div>
						<?php if($package_big_img!=''){ //ต้องทำการส่งค่า package_big_img มาจาก controller Package->create ก่อน ?>
                        <div class="form-group row">
                            <label class="col-md-1 col-form-label">Image</label>
                            <div class="col-md-4">
                            	<!--
								<img src="<?=base_url(); ?>gallery/package/big_imgs/<?=$package_big_img; ?>">
								-->								
								<img class="img-responsive" style="height:160px; width:260px;" src="<?=base_url(); ?>gallery/package/big_imgs/<?=$package_big_img; ?>" >
							</div>
                        </div>
                    	<?php }	
							if(is_numeric($update_id)){	
								echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">แก้ไขข้อมูล</button>';
							}else{
								echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">เพิ่มข้อมูล</button>';
                    		}
						?>
						<button type="button" id="previewContent" name="previewContent" value="Preview" class="btn btn-sm btn-primary m-r-5">พรีวิว</button>
                        <button type="submit" id="submit" name="submit" value="Cancel" class="btn btn-sm btn-warning m-r-5">ยกเลิก</button>
                    </form>
				</div>
				<!-- end panel-body -->
			</div>
			<!-- end panel -->
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
</div>
<!-- end #content -->

