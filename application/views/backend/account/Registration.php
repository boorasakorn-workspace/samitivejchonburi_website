
<!-- begin #content -->
<div id="content" class="content">
  <!-- begin breadcrumb -->
  <ol class="breadcrumb pull-right">
    <li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
    <li class="breadcrumb-item active">Account Form</li>
  </ol>
  <!-- end breadcrumb -->
  <!-- begin page-header -->
  <h1 class="page-header"><?=$headline; ?></h1>
  <!-- end page-header -->
  <!-- begin row -->
  <div class="row">
    <!-- begin col-12 -->
    <div class="col-lg-12">
      <!-- begin panel -->
      <div class="panel panel-inverse">
        <!-- begin panel-heading -->
        <div class="panel-heading">
          <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
          </div>
          <h4 class="panel-title">จัดการ Account</h4>
        </div>
        <!-- end panel-heading -->
        <!-- begin panel-body -->
        <div class="panel-body">
          <?=validation_errors("<p style='color:red;'>", "</p>") ?>
          <?php
            if(is_numeric($update_id)) {
          ?>
                        <div class="form-group row fileupload-buttonbar">
                            <div class="col-md-12">
                                <span class="fileinput-button m-r-3">
                  <?php if($acc_images==''){ ?>
                                    <i class="fa fa-plus"></i>
                                    <!--
                                    <a href="<?=base_url(); ?>backend/Account/upload_image/<?=$update_id; ?>">
                                      <button type="button" class="btn btn-primary" id="upload_img" name="upload_img">Upload Account Image</button>
                                    </a>
                                    -->

                                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#uploadImgModal">Upload Account Image</button>

                                  <!-- Modal -->
                                  <div id="uploadImgModal" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                      <!-- Modal content-->
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title">Upload Account Image</h4>
                                        </div>
                                        <div class="modal-body">
                                          <form id="AccountImageForm" name="AccountImageForm" enctype="multipart/form-data" method="post" >
                                      <label class="control-label">Please Choose a file from your Computer </label>
                                      <input type="file" name="file" size="20">
                                        </div>
                                        <div class="modal-footer">
                                      <button type="submit" id="Imagesubmit" name="submit" value="Submit" class="btn btn-block btn-success">Upload</button>
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      </form>
                                        </div>
                                      </div>

                                    </div>
                                  </div>

                                  <?php }else{ ?>
                                    <i class="fa fa-times"></i>
                                    <!--
                                    <a href="<?=base_url(); ?>backend/Account/delete_image/<?=$update_id; ?>">
                                      <button type="button" class="btn btn-danger" id="delete_img" name="delete_img">Delete Account Image</button>
                                    </a>
                                    -->
                                     <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteImgModal">Delete Account Image</button>

                                    <!-- Modal -->
                                    <div id="deleteImgModal" class="modal fade" role="dialog">
                                      <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Delete Account Image</h4>
                                          </div>
                                          <div class="modal-body">
                                            <p>Sure to delete account image?</p>
                                          </div>
                                          <div class="modal-footer">
                                            <a href="<?=base_url(); ?>backend/Account/delete_image/<?=$update_id; ?>">
                                              <button type="button" class="btn btn-danger" id="delete_img" name="delete_img">Delete Account Image</button>
                                            </a>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                          </div>
                                        </div>

                                      </div>
                                    </div>

                                  <?php } ?>
                                    <!--
                                    <a href="<?=base_url(); ?>backend/Account/delete_account/<?=$update_id; ?>">
                                      <button type="button" class="btn btn-danger" id="delete_account" name="delete_account">Delete Account</button>
                                    </a>
                                    -->
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">Delete Account</button>

                                    <!-- Modal -->
                                    <div id="deleteModal" class="modal fade" role="dialog">
                                      <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Delete Account</h4>
                                          </div>
                                          <div class="modal-body">
                                            <p>Sure to delete account?</p>
                                          </div>
                                          <div class="modal-footer">
                                            <a href="<?=base_url(); ?>backend/Account/delete_account/<?=$update_id; ?>">
                                              <button type="button" class="btn btn-danger" id="delete_account" name="delete_account">Delete Account</button>
                                            </a>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                          </div>
                                        </div>

                                      </div>
                                    </div>
                                    <?php /*
                                    <a href="<?=base_url(); ?>backend/Account/view_account_details/<?=$update_id; ?>">
                                        <button type="button" class="btn btn-success" id="view_account" name="view_account">View Account In Front</button>
                                    </a>
                                    */ ?> 
                                </span>
                            </div>
                        </div>
          <?php }
            $location_form = base_url()."backend/Account/Register/".$update_id;
          ?>

          <div class="row row-space-10">
              <div class="col-md-4" id="PreviewSection" style="width: 0px;height: 0px;opacity: 0;">
                <label for="account_preview_img" >Preview Upload Image |  </label>
                <button type="button" class="btn btn-danger" id="deletePreview" name="deletePreview">Delete</button>
                  <div class="form-group" id="preimage_section"></div>
              </div>
                  
          </div>

          <form class="row" action="<?= $location_form; ?>" method="POST" enctype="multipart/form-data">
            <div class="col-md-12">
                <div class="form-group">
                    <input type="text" class="form-control border-r bgc" name="acc_username" placeholder="Username" value="<?php echo $acc_username; ?>">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <input type="password" class="form-control border-r bgc" name="acc_password" placeholder="Password" value="<?=$acc_password; ?>">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <input type="password" class="form-control border-r bgc" name="acc_repeat_password" placeholder="Repeat password" value="<?=$acc_password; ?>">
                </div>
            </div>
            <div class="col-md-6">
                    <div class="form-group">
                    <input type="text" class="form-control border-r bgc" name="acc_firstname" placeholder="First Name" value="<?=$acc_firstname; ?>">
                </div>
            </div>
            <div class="col-md-6">
                    <div class="form-group">
                    <input type="text" class="form-control border-r bgc" name="acc_lastname" placeholder="Last Name" value="<?=$acc_lastname; ?>">
                </div>
            </div>
            <div class="col-md-6">
                    <div class="form-group">
                    <input type="text" class="form-control border-r bgc" name="acc_personal_id" placeholder="Personal ID" value="<?=$acc_personal_id; ?>">
                </div>
            </div>
            <div class="col-md-6">
                    <div class="form-group">
                    <input type="text" class="form-control border-r bgc" name="acc_telephone" placeholder="Telephone" value="<?=$acc_telephone; ?>">
                </div>
            </div>
            <div class="col-md-12">
                    <div class="form-group">
                    <textarea type="text" class="form-control border-r bgc" name="acc_address" placeholder="Address" value="<?=$acc_address; ?>"></textarea>
                </div>
            </div>
            <div class="col-md-6">
                    <div class="form-group">
                    <input type="text" class="form-control border-r bgc" name="acc_email" placeholder="Email" value="<?=$acc_email; ?>">
                </div>
            </div>
            <div class="col-md-6">
                    <div class="form-group">
                    <?php
                  $additional_dd_code = "id='selectGender' class='form-control'";
                    $options = array(
                            'ไม่ระบุ' => 'ไม่ระบุ',
                            'ชาย' => 'ชาย',
                            'หญิง' => 'หญิง'
                          );

                  echo form_dropdown('acc_gender', $options, $acc_gender, $additional_dd_code);
                  ?>
                </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
              <label for="status">สถานะ<span class="text-danger">*</span></label>
                <?php
                $additional_dd_code = "id='selectStatusCode' class='form-control'";
                  $options = array(
                          '' => 'โปรดเลือกสถานะของแอคเค้าท์',
                          '1' => 'Active',
                          '0' => 'Inactive'
                        );

                  echo form_dropdown('acc_status', $options, $acc_status, $additional_dd_code);
                ?>
              </div>
            </div>
            <div class="col-md-6">
                    <div class="form-group">
                    <label for="at_hospital">โรงพยาบาลที่แอคเค้าท์ประจำ<span class="text-danger">*</span></label>
                    <?php
                    $additional_dd_code = "class='form-control'";
                      $options = array(
                              'ทุกโรงพยาบาล' => 'ทุกโรงพยาบาล',
                              'สมิติเวช สุขุมวิท' => 'สมิติเวช สุขุมวิท',
                              'สมิติเวช ศรีนครินทร์' => 'สมิติเวช ศรีนครินทร์',
                              'สมิติเวช ศรีราชา' => 'สมิติเวช ศรีราชา',
                              'สมิติเวช ธนบุรี' => 'สมิติเวช ธนบุรี',
                              'สมิติเวช ชลบุรี' => 'สมิติเวช ชลบุรี',
                              'โรงพยาบาลเด็กสมิติเวช' => 'โรงพยาบาลเด็กสมิติเวช'
                            );

                      echo form_dropdown('acc_at_hospital', $options, $acc_at_hospital, $additional_dd_code);
                    ?>
                </div>
            </div>
            <?php if($acc_images!=''){ //ต้องทำการส่งค่า item_big_img มาจาก controller store_items->create ก่อน ?>
              <div class="col-md-12">
                <div class="form-group">
                    <label class="col-md-2 col-form-label">Big Image</label>
                    <div class="col-md-10">
                      <img src="<?=base_url(); ?>gallery/account/big_imgs/<?=$acc_images; ?>">
                    </div>
                </div>
              </div>
                      <?php }
              if(is_numeric($update_id)){

                echo '<div class="col-md-6">';
                echo '<button type="submit" name="submit" value="Submit" class="btn btn-block btn-primary m-r-5">แก้ไขข้อมูลแอคเค้าท์</button>';
                echo '</div>';
              }else{
                echo '<div class="col-md-6">';
                echo '<button type="submit" name="submit" value="Submit" class="btn btn-block btn-primary m-r-5">เพิ่มข้อมูลแอคเค้าท์</button>';
                echo '</div>';
                        }
            ?>
              <div class="col-md-6">
                <button type="submit" id="submit" name="submit" value="Cancel" class="btn btn-block btn-warning m-r-5">ยกเลิก</button>
              </div>
          </form>
        </div>
        <!-- end panel-body -->
      </div>
      <!-- end panel -->
    </div>
    <!-- end col-12 -->
  </div>
  <!-- end row -->
</div>
<!-- end #content -->
