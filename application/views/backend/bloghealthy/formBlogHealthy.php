<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
		<li class="breadcrumb-item active">Blog Healthy</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header"><?=$headline; ?></h1>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">เพิ่มข้อมูลบทความสุขภาพ</h4>
				</div>
				<!-- end panel-heading -->
				<iframe id="iframe_target" name="iframe_target" src="#" style="width:0;height:0;border:0px solid #fff;"></iframe>
				<!-- begin panel-body -->
				<div class="panel-body">
					<?=validation_errors("<p style='color:red;'>", "</p>") ?>
					<?php
						if(isset(explode('_',$update_id)[1]) && explode('_',$update_id)[1] == '2'){
							$language = "EN";
						}
						else{
							$language = "TH";
						}
						$update_id = explode('_',$update_id)[0];

						if(is_numeric($update_id)) {
					?>
                        <div class="form-group row fileupload-buttonbar">
                            <div class="col-md-12">
                                <span class="fileinput-button m-r-3">
									<?php if($blog_big_img==''){ ?>
                                    <i class="fa fa-plus"></i>
                                    <!--
                                    <a href="<?=base_url(); ?>backend/src/BlogHealthy/upload_image/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-primary" id="upload_img" name="upload_img">Upload Blog Image</button>
                                    </a>
	                                -->
									<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#uploadImgModal">Upload Blog Image</button>

									<!-- Modal -->
									<div id="uploadImgModal" class="modal fade" role="dialog">
									  <div class="modal-dialog">
									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">Upload Blog Image</h4>
									      </div>
									      <div class="modal-body">
						        			<?php /*
						        			$attributes = array('id'=>'BlogImageForm','target'=>'iframe_target','class' => 'form_horizontal');
						        			echo form_open_multipart('backend/src/BlogHealthy/modal_do_upload/'.$update_id, $attributes); */?>
						        			<form id="BlogImageForm" name="BlogImageForm" enctype="multipart/form-data" method="post" >
											<label class="control-label">Please Choose a file from your Computer </label>
											<input type="file" name="file" size="20">
									      </div>
									      <div class="modal-footer">
											<button type="submit" id="Imagesubmit" name="submit" value="Submit" class="btn btn-block btn-success">Upload</button>
									        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											</form>
									      </div>
									    </div>

									  </div>
									</div>
									
                                	<?php }else{ ?>
                                    <i class="fa fa-times"></i>
                                    <!--
									<a href="<?=base_url(); ?>backend/src/BlogHealthy/delete_image/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-danger" id="delete_img" name="delete_img">Delete Blog Image</button>
                                    </a>
                                    -->

									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteImgModal">Delete Blog Image</button>

									<!-- Modal -->
									<div id="deleteImgModal" class="modal fade" role="dialog">
									  <div class="modal-dialog">
									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">Delete Blog Image</h4>
									      </div>
									      <div class="modal-body">
									        <p>Sure to delete blog image?</p>
									      </div>
									      <div class="modal-footer">
									      	<a href="<?=base_url(); ?>backend/src/BlogHealthy/delete_image/<?=$update_id; ?>">
									      		<button type="button" class="btn btn-danger" id="delete_img" name="delete_img">Delete Blog Image</button>
									      	</a>
									        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
									      </div>
									    </div>

									  </div>
									</div>

                                	<?php } ?>
                                	<!--
                                    <a href="<?=base_url(); ?>backend/src/BlogHealthy/delete_blog/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-danger" id="delete_blog" name="delete_blog">Delete Blog</button>
                                    </a>
									-->

									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">Delete Blog</button>

									<!-- Modal -->
									<div id="deleteModal" class="modal fade" role="dialog">
									  <div class="modal-dialog">
									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">Delete Blog</h4>
									      </div>
									      <div class="modal-body">
									        <p>Sure to delete blog?</p>
									      </div>
									      <div class="modal-footer">
									      	<a href="<?=base_url(); ?>backend/src/BlogHealthy/delete_blog/<?=$update_id; ?>">
									      		<button type="button" class="btn btn-danger" id="delete_blog" name="delete_blog">Delete Blog</button>
									      	</a>
									        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
									      </div>
									    </div>

									  </div>
									</div>
									

                                    <a href="<?=base_url(); ?>backend/src/BlogHealthy/view_blog_details/<?=$update_id; ?>">
                                        <button type="button" class="btn btn-success" id="view_blog" name="view_blog"><i class="fas fa-eye"></i> View Blog In Front</button>
                                    </a>
                                    <?php
										if($language == "EN"){
											echo '<a href="'.base_url().'backend/src/BlogHealthy/create/'.explode('_',$update_id)[0].'">';
										}
										else{
											echo '<a href="'.base_url().'backend/src/BlogHealthy/create/'.explode('_',$update_id)[0]."_2".'">';
										}
                                    ?>
                                        <button type="button" class="btn btn-success" id="switch_language" name="switch_language">Switch Language</button>
                                    </a>
                                </span>
                            </div>
                        </div>
					<?php }
						$location_form = base_url()."backend/src/BlogHealthy/create/".$update_id;
					?>
					<form id="formContents" class="form-horizontal" action="<?= $location_form; ?>" method="POST" enctype="multipart/form-data">
						<div class="row row-space-10">
							<div class="col-md-4">
								<div class="form-group">
		                            <label for="blog_title">ชื่อ Title <span class="text-danger">*</span></label>
		                            <?php
		                            	if($language == 'EN'){
		                            		echo '<input type="hidden" class="form-control" id="blog_title" name="blog_title" value="'.$blog_title.'" />';
		                            		echo '<input type="text" class="form-control" id="blog_title_en" name="blog_title_en" placeholder="ENG Title" value="'.$blog_title_en.'" />';
				                        }
				                        else{
		                            		echo '<input type="text" class="form-control" id="blog_title" name="blog_title" placeholder="TH Title" value="'.$blog_title.'" />';
		                            		echo '<input type="hidden" class="form-control" id="blog_title_en" name="blog_title_en" value="'.$blog_title_en.'" />';
				                        }
		                            ?>		   

		                            <label for="doc_id">แพทย์เจ้าของบทความ</label>
	        	 					<select name="doc_id" id="doc_id" class="selectpicker" data-live-search="true">
			                            <option value="">ไม่มีแพทย์เจ้าของบทความ</option>
		        	 					<?php
		        	 						foreach($doctor_content_page as $key => $selection ) :?>
		        	 						<option value="<?php echo $selection['id'];?>" <?php if(isset($doc_id) && $doc_id ==  $selection['id']){echo 'selected';}?>>
		        	 							<?php 
		        	 								if($selection['id'] != ''){
		        	 									echo " - ".$selection['title'];
		        	 								}
		        	 								else{
		        	 									echo " ".$selection['title'];
		        	 								}
		        	 							?>
		        	 					</option>
	        	 					<?php endforeach;?>
		        	 				</select>
			                    </div>
		                    </div>
		                    <div class="col-md-4">
		                        <div class="form-group">
		                            <label for="blog_keywords">Blog Keywords</label>
		                            <textarea class="form-control" name="blog_keywords" rows="5" placeholder="Blog Keywords"><?=$blog_keywords; ?></textarea>
		                        </div>
		                    </div>
		                    <div class="col-md-4">
		                        <div class="form-group">
		                            <label for="blog_description">Blog Description</label>
		                            <?php
		                            	if($language == 'EN'){
		                            		echo '<textarea class="form-control" name="blog_description" style="display:none;">'.$blog_description.'</textarea>';
		                            		echo '<textarea class="form-control" name="blog_description_en" placeholder="ENG Description">'.$blog_description_en.'</textarea>';
				                        }
				                        else{
		                            		echo '<textarea class="form-control" name="blog_description" rows="5" placeholder="TH Description" >'.$blog_description.'</textarea>';
		                            		echo '<textarea class="form-control" style="display:none;" name="blog_description_en">'.$blog_description_en.'</textarea>';
				                        }
		                            ?>
		                        </div>
		                    </div>
		                </div>
		                <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="promotion_link_1">โปรโมชั่นที่เกี่ยวข้อง</label>
                            <div class="col-md-4">                      
        	 				<select name="promotion_link_1" id="promotion_link_1" class="selectpicker" data-live-search="true">
        	 					<option value="">ไม่มีโปรโมชั่นที่เกี่ยวข้อง</option>
        	 					<?php
        	 						foreach($content_page as $key => $selection ) :?>
        	 						<option value="<?php echo $selection['id'];?>" <?php if(isset($promotion_link_1) && $promotion_link_1 ==  $selection['id']){echo 'selected';}?>>
        	 							<?php 
        	 								if($selection['id'] != ''){
        	 									echo " - ".$selection['title'];
        	 								}
        	 								else{
        	 									echo " ".$selection['title'];
        	 								}
        	 							?></option>
        	 					<?php endforeach;?>  

        	 				</select>
							</div>
							<label class="col-md-2 col-form-label" for="promotion_link_2">โปรโมชั่นที่เกี่ยวข้อง</label>
                            <div class="col-md-4">                      
        	 				<select name="promotion_link_2" id="promotion_link_2" class="selectpicker" data-live-search="true">
        	 					<option value="">ไม่มีโปรโมชั่นที่เกี่ยวข้อง</option>
        	 					<?php
        	 						foreach($content_page as $key => $selection ) :?>
        	 						<option value="<?php echo $selection['id'];?>" <?php if(isset($promotion_link_2) && $promotion_link_2 ==  $selection['id']){echo 'selected';}?> >
        	 							<?php 
        	 								if($selection['id'] != ''){
        	 									echo " - ".$selection['title'];
        	 								}
        	 								else{
        	 									echo " ".$selection['title'];
        	 								}
        	 							?></option>
        	 					<?php endforeach;?>  

        	 				</select>
							</div>
                        </div>
		                <div class="row row-space-10">
		                    <div class="col-md-4" id="PreviewSection" style="width: 0px;height: 0px;opacity: 0;">
		                    	<label for="blog_preview_img" >Preview Upload Image |  </label>
		                    	<button type="button" class="btn btn-danger" id="deletePreview" name="deletePreview">Delete</button>
		                        <div class="form-group" id="preimage_section"></div>
		                    </div>
		                        
		                </div>
		                <div class="row row-space-10">
		                    <div class="col-md-12">
		                        <div class="form-group">
		                            <label for="blog_contents">Blog Contents</label>
		                            <?php
		                            	if($language == 'EN'){
		                            		echo '<textarea name="blog_contents" style="display:none;">'.$blog_contents.'</textarea>';
		                            		echo '<textarea id="editor1" name="blog_contents_en" placeholder="ENG Description">'.$blog_contents_en.'</textarea>';
				                        }
				                        else{
		                            		echo '<textarea id="editor1" name="blog_contents" >'.$blog_contents.'</textarea>';
		                            		echo '<textarea name="blog_contents_en" style="display:none;" placeholder="ENG Description">'.$blog_contents_en.'</textarea>';
				                        }
		                            ?>
		                        </div>
		                    </div>
		                </div>
						<div class="form-group row">
                            <div class="col-md-6">
								<div class="form-group">
	                                <label for="date_published">Date Published <span class="text-danger">*</span></label>
	                                <input type="text" class="form-control" id="datepicker-default" name="date_published" placeholder="Select Date" value="<?=$date_published; ?>" />
	                            </div>
	                        </div>
                            <div class="col-md-6">
                            <label for="blog_status">สถานะ</label>
							<?php
							$additional_dd_code = "id='selectStatusCode' class='form-control'";
								$options = array(
												'' => 'โปรดเลือกการเผยแพร่',
												'1' => 'Published',
												'0' => 'Unpublished'
											);

								echo form_dropdown('blog_status', $options, $blog_status, $additional_dd_code);
							?>
							</div>
                        </div>
						<div class="row row-space-10">
		                    <div class="col-md-6">
								<div class="form-group">
		                            <label for="video">Video (Youtube Link)</label>
	                            	<input type="text" class="form-control" id="blog_video" name="blog_video" placeholder="https://www.youtube.com/watch?v=foobar , https://youtu.be/foobar" value="<?=$blog_video; ?>" />
		                        </div>
		                    </div>
						</div>
						<div class="form-group row">
                            <div class="col-md-4">
								<div class="form-group">
		                            <label for="meta_author">Meta Author</label>
		                            <input type="text" class="form-control" id="meta_author" name="meta_author" value="<?=$meta_author; ?>" placeholder="Author" />
	                            </div>  
		                    </div>
                            <div class="col-md-4">
								<div class="form-group">
		                            <label for="meta_keyword">Meta Keyword</label>
		                            <input type="text" class="form-control" id="meta_keywords" name="meta_keywords" value="<?=$meta_keywords; ?>" placeholder="Keywords"/>
	                            </div>  
		                    </div>
                            <div class="col-md-4">
								<div class="form-group">
		                            <label for="meta_desciption">Meta Description</label>
		                            <input type="text" class="form-control" id="meta_description" name="meta_description" value="<?=$meta_description; ?>" placeholder="Description"/>
	                            </div>  
		                    </div>
                        </div>
						<?php if($blog_big_img!=''){ //ต้องทำการส่งค่า blog_big_img มาจาก controller BlogHealthy->create ก่อน ?>
                        <div class="form-group row">
                            <label class="col-md-1 col-form-label">Image</label>
                            <div class="col-md-4">
                            	<!--
								<img src="<?=base_url(); ?>gallery/blog/big_imgs/<?=$blog_big_img; ?>">
								-->
								<img class="img-responsive" style="height:120px; width:250px; box-shadow: 2px 3px 14px 0px rgba(0,0,0,0.75);border-radius: 10px 10px 0px 0px;" src="<?=base_url(); ?>gallery/blog/big_imgs/<?=$blog_big_img; ?>" >
							</div>
                        </div>
                    	<?php }
							if(is_numeric($update_id)){
								echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">แก้ไขข้อมูล</button>';
							}else{
								echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">เพิ่มข้อมูล</button>';
                    		}
						?>
						<button type="button" id="previewContent" name="previewContent" value="Preview" class="btn btn-sm btn-primary m-r-5">พรีวิว</button>
                        <button type="submit" id="submit" name="submit" value="Cancel" class="btn btn-sm btn-warning m-r-5">ยกเลิก</button>
                    </form>
				</div>
				<!-- end panel-body -->
			</div>
			<!-- end panel -->
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
</div>
<!-- end #content -->

