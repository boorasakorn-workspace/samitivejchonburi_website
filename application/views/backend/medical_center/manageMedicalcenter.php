<!-- begin #content -->
<div id="content" class="content">		
	<h1>จัดการส่วน Medical Center</h1>
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
		<li class="breadcrumb-item active">รายละเอียด Medical Center</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<?php
		$create_medicalcenter_url = base_url()."backend/src/Medicalcenter/create";
	?>
	<h1 class="page-header">
		<!-- Main --><!-- <a href="<?=$create_medicalcenter_url; ?>"><button type="button" class="btn btn-lime">Add New Medical Center</button></a></small> -->
	</h1>
	
	<?php
		if(isset($flash)){
			echo $flash;
		}
	?>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-lg-12">
			<div class="panel panel-inverse">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">จัดการ Medical Center</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<table id="data-table-buttons" class="table table-bordered">
						<thead>
							<tr>
								<th width="1%" class="text-nowrap">#</th>
								<th class="text-nowrap">ชื่อศูนย์พยาบาล</th>
								<th class="text-nowrap">สถานะ</th>
								<th class="text-nowrap">แก้ไข</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach($query->result() as $rows){
								$rows->medical_center_icon = $rows->medical_center_icon?$rows->medical_center_icon:'NoPic.png';
								$view_medicalcenter_url = base_url()."backend/src/Medicalcenter/view_medicalcenter_details/".$rows->uid;
								$edit_medicalcenter_url = base_url()."backend/src/Medicalcenter/create/".$rows->uid;
								$delete_medicalcenter_url = base_url()."backend/src/Medicalcenter/delete_medicalcenter/".$rows->uid;
								$status = $rows->status;
								if($status == 0){
									$status_label = "green";
									$status_desc = "Published";
								}else{
									$status_label = "secondary";
									$status_desc = "Unpublished";
								}
							?>
							<tr class="odd gradeX">
								<td width="1%" ><?=$rows->uid; ?></td>
								<td><img class="img-responsive" style="width:24px;height:24px;" src="<?=base_url().'gallery/medical_center/icon/'.$rows->medical_center_icon;?>"> <?=$rows->description; ?></td>
								<td class="text-center">
									<span class="label label-<?=$status_label; ?>"><?=$status_desc; ?></span>
								</td>
								<td width="30%" class="text-center">
									<a class="btn btn-xs btn-success" href="<?=$view_medicalcenter_url; ?>">
										<i class="fas fa-eye"></i>
									</a>
									<a class="btn btn-xs btn-warning" href="<?=$edit_medicalcenter_url; ?>">
										<i class="fas fa-edit"></i>
									</a>
									<a class="btn btn-xs btn-danger" href="<?=$delete_medicalcenter_url; ?>">
										<i class="fas fa-trash"></i>
									</a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
				<!-- end panel-body -->
			</div>
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
</div>