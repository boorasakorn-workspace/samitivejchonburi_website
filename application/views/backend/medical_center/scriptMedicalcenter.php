<script>
	$(document).ready(function() {
	    $("#Imagesubmit").click(function(e){
            e.preventDefault(); 
            var MedicalCenterLogoForm = document.getElementById('MedicalCenterLogoForm');
            var formData = new FormData(MedicalCenterLogoForm);
            
	        $.ajax({
			  url: "<?=base_url(); ?>backend/src/Medicalcenter/modal_do_upload/<?=$update_id; ?>",  
			  type: 'POST',
              data: formData,
             processData:false,
             contentType:false,
             cache:false,
             async:false,
			  success: function(result){
			  	$('#PreviewSection').removeAttr('style');

			  	var addPreviewSection = '<div id="preview_image_section"></div>';
			  	$('#preimage_section').html(addPreviewSection);

			  	var preview_quote = '<img class="img-responsive" style="height:275px; width:275px;" src="<?=base_url(); ?>gallery/medical_center/preview_imgs/'+JSON.parse(result)+'" >';
			  	$('#preview_image_section').html(preview_quote);

	        	$('#uploadImgModal').modal('hide');
			  }
			});
	        
	    });

	    $("#deletePreview").click(function(e){
            e.preventDefault(); 
	        $.ajax({
	        	url: "<?=base_url(); ?>backend/src/Medicalcenter/delete_preview/<?=$update_id; ?>",
				processData:false,
				contentType:false,
				cache:false,
				async:false,
			  success: function(result){
			  	$("#PreviewSection").css({
			  		'opacity': '0' ,
			  		'height': '0px' ,
			  		'width': '0px'
			  	});

			  	var addPreviewSection = '';
			  	$('#preimage_section').html(addPreviewSection);
			  }
			});
	        
	    });

	    $("#previewContent").click(function(e){
            e.preventDefault(); 
            var formContents = document.getElementById('formContents');
            var formData = new FormData(formContents);

	        $.ajax({
	        	url: "<?=base_url(); ?>backend/src/Medicalcenter/preview_Contents",
				type: 'POST',
				data: formData,
				processData:false,
				contentType:false,
				cache:false,
				async:false,
			  success: function(result){
			  	var w = window.open('about:blank');
			    w.document.open();
			    w.document.write(result);
			  }
			});	        
	    });

	});
</script>	