<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
		<li class="breadcrumb-item active">Medical Center Form</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header"><?=$headline; ?></h1>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">จัดการ Medical Center Details</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<?=validation_errors("<p style='color:red;'>", "</p>") ?>
					<?php
						if(is_numeric($update_id)) {
					?>
                        <div class="form-group row fileupload-buttonbar">
                            <div class="col-md-12">
                                <span class="fileinput-button m-r-3">
									<?php /* if($medical_center_imgs==''){ ?>
                                    <i class="fa fa-plus"></i>
                                    <!--
                                    <a href="<?=base_url(); ?>backend/src/Medicalcenter/upload_image/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-primary" id="upload_img" name="upload_img">Upload Medical Center Image</button>
                                    </a>
	                                -->

                                	<?php }else{ ?>
                                    <i class="fa fa-times"></i>
                                    <!--
									<a href="<?=base_url(); ?>backend/src/Medicalcenter/delete_image/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-danger" id="delete_img" name="delete_img">Delete Medical Center Image</button>
                                    </a>
	                                -->	                                

									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteImgModal">Delete Medical Center Image</button>

									<!-- Modal -->
									<div id="deleteImgModal" class="modal fade" role="dialog">
									  <div class="modal-dialog">
									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">Delete Medical Center Image</h4>
									      </div>
									      <div class="modal-body">
									        <p>Sure to delete medical center image?</p>
									      </div>
									      <div class="modal-footer">
									      	<a href="<?=base_url(); ?>backend/src/Medicalcenter/delete_image/<?=$update_id; ?>">
		                                    	<button type="button" class="btn btn-danger" id="delete_img" name="delete_img">Delete Medical Center Image</button>
		                                    </a>
									        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
									      </div>
									    </div>

									  </div>
									</div>

                                	<?php } */?>
                                	<!--
                                    <a href="<?=base_url(); ?>backend/src/Medicalcenter/delete_medicalcenter/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-danger" id="delete_medicalcenter" name="delete_medicalcenter">Delete Medical Center</button>
                                    </a>
	                                -->

									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">Delete Medical Center</button>

									<!-- Modal -->
									<div id="deleteModal" class="modal fade" role="dialog">
									  <div class="modal-dialog">
									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">Delete Medical Center</h4>
									      </div>
									      <div class="modal-body">
									        <p>Sure to delete medical center?</p>
									      </div>
									      <div class="modal-footer">
		                                    <a href="<?=base_url(); ?>backend/src/Medicalcenter/delete_medicalcenter/<?=$update_id; ?>">
		                                    	<button type="button" class="btn btn-danger" id="delete_medicalcenter" name="delete_medicalcenter">Delete Medical Center</button>
		                                    </a>
									        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
									      </div>
									    </div>

									  </div>
									</div>

                                    <a href="<?=base_url(); ?>backend/src/Medicalcenter/view_medicalcenter_details/<?=$update_id; ?>">
                                        <button type="button" class="btn btn-success" id="view_medicalcenter" name="view_medicalcenter"><i class="fas fa-eye"></i> View Medical Center In Front</button>
                                    </a>
                                    <!--
                                    <a href="<?=base_url(); ?>backend/src/Medicalcenter/upload_icon/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-primary" id="upload_img" name="upload_img">Upload Medical Center Icon</button>
                                    </a>
	                                -->

	                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#uploadImgModal">Upload Medical Center Logo</button>

									<!-- Modal -->
									<div id="uploadImgModal" class="modal fade" role="dialog">
									  <div class="modal-dialog">
									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">Upload Medical Center Logo</h4>
									      </div>
								        <form id="MedicalCenterLogoForm" name="MedicalCenterLogoForm" enctype="multipart/form-data" method="post" >
									      <div class="modal-body">						        			
											<label class="control-label">Please Choose a file from your Computer </label>
											<input type="file" name="file" id="file" size="20">
									      </div>
									      <div class="modal-footer">
											<button type="submit" id="Imagesubmit" name="submit" value="Submit" class="btn btn-block btn-success">Upload</button>
									        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									      </div>
										</form>
									    </div>

									  </div>
									</div>

                                </span>
                            </div>
                        </div>
					<?php }
						$location_form = base_url()."backend/src/Medicalcenter/create/".$update_id;
					?>
	                <div class="row row-space-10">
	                    <div class="col-md-4" id="PreviewSection" style="width: 0px;height: 0px;opacity: 0;">
	                    	<label for="doctor_preview_img" >Preview Upload Image |  </label>
	                    	<button type="button" class="btn btn-danger" id="deletePreview" name="deletePreview">Delete</button>
	                        <div class="form-group" id="preimage_section"></div>
	                    </div>
	                        
	                </div>
					<form id="formContents" class="form-horizontal" action="<?= $location_form; ?>" method="POST" enctype="multipart/form-data">

						<!-- Thailand -->
						<div class="hljs-wrapper">
							<h4 class="text-lime"><span class="bold">ชื่อ (ภาษาไทย) <i class="fas fa-hand-point-down"></i></span></h4>
						</div>

						<div class="row row-space-10">
		                    <div class="col-md-6">
								<div class="form-group">
		                            <label for="medical_center_name">ชื่อศูนย์พยาบาล<span class="text-danger">*</span></label>
		                            <input type="text" class="form-control" id="medical_center_name" name="description" value="<?=$description; ?>" />
		                        </div>
		                    </div>
		                    <div class="col-md-6">
								<div class="form-group">
		                            <label for="medical_center_details">รายละเอียดศูนย์พยาบาล</label>
	                            	<input type="text" class="form-control" id="medical_center_details" name="medical_center_details" value="<?=$medical_center_details; ?>" />
		                        </div>
		                    </div>
                   		</div>
                   		<div class="row row-space-10">
                   			<div class="col-md-6">
								<div class="form-group">
		                            <label for="medical_center_location">สถานที่ศูนย์พยาบาล<span class="text-danger">*</span></label>
		                            <input type="text" class="form-control" name="medical_center_location" value="<?=$medical_center_location; ?>" placeholder="ตึก A ชั้น 1">
		                        </div>
		                    </div>
		                    <div class="col-md-3">
								<div class="form-group">
		                            <label for="experience_th">วันทำการ<span class="text-danger">*</span></label>
		                            <input type="text" class="form-control" name="medical_center_office_daystxt" value="<?=$medical_center_office_daystxt; ?>" placeholder="จันทร์ - ศุกร์">
		                        </div>
		                    </div>
		                    <div class="col-md-3">
								<div class="form-group">
		                            <label for="experience_th">เวลาทำการ<span class="text-danger">*</span></label>
		                            <input type="text" class="form-control" name="medical_center_office_hours" value="<?=$medical_center_office_hours; ?>" placeholder="08:00น. - 18:00น.">
		                        </div>
		                    </div>
                   		</div>
                   		<div class="row row-space-10">
                   			<div class="col-md-4">
								<div class="form-group">
		                            <label for="medical_center_phone">เบอร์โทรศัพท์<span class="text-danger">*</span></label>
		                            <input type="text" class="form-control" id="medical_center_phone" name="medical_center_phone" value="<?=$medical_center_phone; ?>" />
		                        </div>
		                    </div>
		                    <div class="col-md-4">
								<div class="form-group">
		                            <label for="medical_center_email">อีเมลล์<span class="text-danger">*</span></label>
	                            	<input type="text" class="form-control" id="medical_center_email" name="medical_center_email" value="<?=$medical_center_email; ?>" />
		                        </div>
		                    </div>
		                    <div class="col-md-4">
								<div class="form-group">
		                            <label for="medical_center_is">ประเภทศูนย์และคลินิก<span class="text-danger">*</span></label>
	                            	<?php
										$additional_dd_code = "id='selectMedicalCenterIs' class='form-control'";
											$options = array(
															'1' => 'ศูนย์และคลินิกทั่วไป',
															'2' => 'ศูนย์และคลินิกผู้ใหญ่'
														);

											echo form_dropdown('medical_center_is', $options, $medical_center_is, $additional_dd_code);
									?>
		                        </div>
		                    </div>
                   		</div>
                   		<div class="row row-space-10">
							<div class="col-md-6">
								<div class="form-group">
		                            <label for="medical_center_meta_keywords">Meta Keywords<span class="text-danger">*</span></label>
									<textarea class="form-control" id="medical_center_meta_keywords" name="medical_center_meta_keywords" rows="3" placeholder="Keywords" /><?=$medical_center_meta_keywords; ?></textarea>
		                        </div>
		                    </div>
		                    <div class="col-md-6">
								<div class="form-group">
		                            <label for="medical_center_meta_description">Meta Descriptions<span class="text-danger">*</span></label>
									<textarea class="form-control" id="medical_center_meta_description" name="medical_center_meta_description" rows="3" placeholder="Description" /><?=$medical_center_meta_description; ?></textarea>
		                        </div>
		                    </div>
                   		</div>
                   		<div class="row row-space-10">
                   			<div class="col-md-12">
								<div class="form-group">
		                            <label for="medical_center_contents">Contents<span class="text-danger">*</span></label>
									<textarea class="ckeditor" id="editor5" name="medical_center_contents" rows="20" /><?=$medical_center_contents; ?></textarea>
		                        </div>
		                    </div>
                   		</div>

						<div class="form-group row">
                            <label class="col-md-1 col-form-label" for="status">สถานะ<span class="text-danger">*</span></label>
                            <div class="col-md-4">
							<?php
							$additional_dd_code = "id='selectStatusCode' class='form-control'";
								$options = array(
												'' => 'โปรดเลือกสถานะศูนย์การแพทย์',
												'0' => 'Published',
												'1' => 'Unpublished'
											);

								echo form_dropdown('status', $options, $status, $additional_dd_code);
							?>
							</div>
                        </div>

						<?php if(isset($medical_center_icon) && $medical_center_icon!=''){ //ต้องทำการส่งค่า item_big_img มาจาก controller store_items->create ก่อน ?>
                        <div class="form-group row">
                            <label class="col-md-1 col-form-label">Logo</label>
                            <div class="col-md-4">
                            	<!--
								<img src="<?=base_url(); ?>gallery/medical_center/big_imgs/<?=$medical_center_imgs; ?>">
								-->
								<img class="img-responsive" style="height:275px; width:275px;" src="<?=base_url(); ?>gallery/medical_center/icon/<?=$medical_center_icon; ?>">
							</div>
                        </div>
                    	<?php }
							if(is_numeric($update_id)){
								echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">แก้ไขข้อมูลศูนย์การแพทย์</button>';
							}else{
								echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">เพิ่มข้อมูลศูนย์การแพทย์</button>';
                    		}
						?>
						<button type="button" id="previewContent" name="previewContent" value="Preview" class="btn btn-sm btn-primary m-r-5">พรีวิว</button>
                        <button type="submit" id="submit" name="submit" value="Cancel" class="btn btn-sm btn-warning m-r-5">ยกเลิก</button>
                    </form>
				</div>
				<!-- end panel-body -->
			</div>
			<!-- end panel -->
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
</div>
<!-- end #content -->
