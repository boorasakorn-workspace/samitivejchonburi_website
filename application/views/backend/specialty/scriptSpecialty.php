<script>
	$(document).ready(function() {
	    $("#addParent").click(function(e){
            e.preventDefault(); 
            var addParentText = $("#parent option:selected").val();
            if($("#parent_name").val() != '' && addParentText != ''){
            	addParentText = "|"+addParentText;
            }
            $("#parent_name").val( $("#parent_name").val() + addParentText);
	    });
	    $("#addSpecialtyParent").click(function(e){
            e.preventDefault(); 
            var addSpecialtyParentText = $("#parent_specialty option:selected").val();
            if($("#parent_specialty_name").val() != '' && addSpecialtyParentText != ''){
            	addSpecialtyParentText = "|"+addSpecialtyParentText;
            }
            $("#parent_specialty_name").val( $("#parent_specialty_name").val() + addSpecialtyParentText);
	    });
	});
</script>	