<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
		<li class="breadcrumb-item active">Specialty</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header"><?=$headline; ?></h1>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<h4 class="panel-title">Specialty</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<?php if(isset($error_report)){
						echo "<p style='color:red;'>". $error_report ."</p>";
					}
					?>
					<?=validation_errors("<p style='color:red;'>", "</p>") ?>
					<?php
					if(is_numeric($update_id)) {
						?>
						<div class="form-group row fileupload-buttonbar">
							<div class="col-md-12">
								<span class="fileinput-button m-r-3">
									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">Delete Specialty</button>

									<!-- Modal -->
									<div id="deleteModal" class="modal fade" role="dialog">
									  <div class="modal-dialog">
									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">Delete Specialty</h4>
									      </div>
									      <div class="modal-body">
									        <p>Sure to delete specialty?</p>
									      </div>
									      <div class="modal-footer">
											<a href="<?=base_url(); ?>backend/src/Specialty/delete_specialty/<?=$update_id; ?>">
												<button type="button" class="btn btn-danger" id="delete_specialty" name="delete_specialty">Delete Specialty</button>
											</a>
									        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
									      </div>
									    </div>

									  </div>
									</div>
								</span>
							</div>
						</div>
					<?php }
					$location_form = base_url()."backend/src/Specialty/create/".$update_id;
					?>

					<form class="form-horizontal" action="<?= $location_form; ?>" method="POST" enctype="multipart/form-data">
						
						<div class="hljs-wrapper">
							<h4 class="text-lime"><span class="bold">Specialty</span></h4>
						</div>
						<div class="row row-space-10">
							<div class="col-md-4">
								<div class="form-group">
									<label for="specialty_name_th">Specialty TH<span class="text-danger">*</span></label>
									<input type="text" class="form-control" id="specialty_name_th" name="description" value="<?=$description; ?>" />
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="status">Status</label>
									<select class="form-control" name="status">
										<option value="0">Active</option>
										<option value="1">Inactive</option>									
									</select>
								</div>
							</div>
						</div>
						<?php 
						if(is_numeric($update_id)){
							echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">Edit Specialty</button>';
						}else{
							echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">Add Specialty</button>';
						}
						?>
						<button type="submit" id="submit" name="submit" value="Cancel" class="btn btn-sm btn-warning m-r-5">ยกเลิก</button>
					</form>
				</div>
				<!-- end panel-body -->
			</div>
			<!-- end panel -->
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
</div>
<!-- end #content -->
