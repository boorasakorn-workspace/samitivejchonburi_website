<!-- begin #content -->
<div id="content" class="content">	
	<div class="panel panel-success" data-sortable-id="ui-widget-11">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
			</div>
			<h4 class="panel-title"><?=$headline?></h4>
		</div>
		<div class="panel-body">
			<?php
				if(isset($error)){
					foreach($error as $key => $value){
						echo $value;
					}
				}

				if(isset($flash)){
					echo $flash;
				}
			?>
			<?php 
			$attributes = array('class' => 'form_horizontal');
			echo form_open_multipart('backend/src/Doctor/do_upload/'.$update_id, $attributes); ?>

			<label class="control-label">Please Choose a file from your Computer </label>
			
			<input type="file" name="userfile" size="20">
		</div>
		<div class="hljs-wrapper">
			<div class="row">
				<div class="col-md-6">
					<button type="submit" name="submit" value="Submit" class="btn btn-block btn-success">Upload</button>
				</div>
				<div class="col-md-6">
					<button type="submit" name="submit" value="Cancel" class="btn btn-block btn-warning">Cancel</button>
				</div>
			</div>
		</div>
		</form>
	</div>
</div>