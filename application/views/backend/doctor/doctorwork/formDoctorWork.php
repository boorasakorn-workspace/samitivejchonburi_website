<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
		<li class="breadcrumb-item active">Doctor Work Form</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header"><?=$headline; ?></h1>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">จัดการวันและเวลาปฏิบัติงานของแพทย์</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<?=validation_errors("<p style='color:red;'>", "</p>") ?>
					<?php
						if(is_numeric($update_id)) {
					?>
                        <div class="form-group row fileupload-buttonbar">
                            <div class="col-md-12">
                                <span class="fileinput-button m-r-3">
                                    <a href="<?=base_url(); ?>backend/src/DoctorWork/delete_doctorwork/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-danger" id="delete_doctorwork" name="delete_doctorwork">Delete Doctor Work</button>
                                    </a>
                                </span>
                            </div>
                        </div>
					<?php }
						$location_form = base_url()."backend/src/DoctorWork/create/".$update_id;
					?>
					<form class="form-horizontal" action="<?= $location_form; ?>" method="POST" enctype="multipart/form-data">
						<div class="row row-space-10">
		                    <div class="col-md-4">
								<div class="form-group">
		                            <label for="medical_id">โปรดเลือกแพทย์ที่จะทำการเพิ่มวันและเวลาปฏิบัติงาน<span class="text-danger">*</span></label>
		                            <select name="ms_careprovider_uid" class="form-control">
		                           	<?php if(!is_numeric($update_id)){ ?>
										<?php foreach($query as $row){ ?>
											<option value="<?=$row->care_uid; ?>"><?=$row->pname_th.' '.$row->firstname_th.' '.$row->lastname_th; ?></option>
										<?php } ?>
		                           	<?php }else{ ?>
										<option <?=($fetchid->care_uid==$care_uid ? 'selected="selected"':''); ?> value="<?=$query->care_uid; ?>">
											<?=$query->pname_th.' '.$query->firstname_th.' '.$query->lastname_th; ?>
										</option>
		                            <?php
		                            	foreach($result as $rows){
		                            ?>
		                            		<option class="text-center" value="<?=$rows->care_uid; ?>" <?=$care_uid == $rows->care_uid?'selected':'';?>><?=$rows->pname_th.' '.$rows->firstname_th.' '.$rows->lastname_th; ?></option>
		                            <?php }}
		                            	/*$additional_dd_code = "id='selectDoctorName' class='form-control'";
		                            	foreach($query->result_array() as $option){
		                            		$options = array(
		                            			$option['medical_id'] => $option['pname_th'].' '.$option['firstname_th'].' '.$option['lastname_th']
		                            		);
		                            	}

		                            	echo form_dropdown('medical_id',$options, set_value($medical_id, (isset($medical_id)) ? $medical_id : ''),$additional_dd_code);*/
		                            ?>
		                            </select>
		                        </div>
		                    </div>
		                </div>

						<div class="row row-space-10">
		                    <div class="col-md-4">
								<div class="form-group">
									<label for="work_days">วันทำงานแพทย์<span class="text-danger">*</span></label>
									<?php
									$additional_dd_code = "id='work_days' class='form-control'";
									$options = array(
										"วันอาทิตย์" => "วันอาทิตย์",
										"วันจันทร์" => "วันจันทร์",
										"วันอังคาร" => "วันอังคาร",
										"วันพุธ" => "วันพุธ",
										"วันพฤหัสบดี" => "วันพฤหัสบดี",
										"วันศุกร์" => "วันศุกร์",
										"วันเสาร์" => "วันเสาร์"
									);

									echo form_dropdown('caption_date', $options, $caption_date, $additional_dd_code);
									?>
		                            <!-- <input type="text" class="form-control" id="work_days" name="caption_date" placeholder="Example : วันจันทร์" value="<?=$caption_date; ?>" /> -->
		                        </div>
		                    </div>
		                    <div class="col-md-2">
		                        <label for="work_times_in">เวลาเข้างานแพทย์<span class="text-danger">*</span></label>
		                    	<div class="input-group date" id="timepicker_in">
									<input type="text" class="form-control" name="start_time" value="<?=$start_time; ?>" />
									<span class="input-group-addon">
									<i class="fa fa-clock"></i>
									</span>
								</div>
		                    </div>
		                    <div class="col-md-2">
		                        <label for="work_times_out">เวลาออกงานแพทย์<span class="text-danger">*</span></label>
		                    	<div class="input-group date" id="timepicker_out">
									<input type="text" class="form-control" name="end_time" value="<?=$end_time; ?>" />
									<span class="input-group-addon">
									<i class="fa fa-clock"></i>
									</span>
								</div>
		                    </div>
		                    <div class="col-md-4">
								<div class="form-group">
		                            <label for="out_of_days">หมายเหตุ</label>
	                            	<input type="text" class="form-control" id="out_of_days" name="remark" value="<?=$remark; ?>" />
		                        </div>
		                    </div>
                   		</div>

                   		<?php
							if(is_numeric($update_id)){
								echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">แก้ไขข้อมูลวันและเวลาปฏิบัติงานแพทย์</button>';
							}else{
								echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">เพิ่มข้อมูลวันและเวลาปฏิบัติงานแพทย์</button>';
                    		}
						?>
                        <button type="submit" id="submit" name="submit" value="Cancel" class="btn btn-sm btn-warning m-r-5">ยกเลิก</button>
                    </form>
				</div>
				<!-- end panel-body -->
			</div>
			<!-- end panel -->
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
</div>
<!-- end #content -->
