<!-- DOCTOR WORK -->
<!-- begin col-12 -->
<div class="col-lg-12" id="col_manage_doctorwork">
	<div class="panel panel-inverse">
		<!-- begin panel-heading -->
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
			</div>
			<h4 class="panel-title">จัดการวันและเวลาปฏิบัติงานของแพทย์</h4>
		</div>
		<!-- end panel-heading -->
		<!-- begin panel-body -->
		<div class="panel-body">
			<table id="data-table" class="table table-bordered">
				<thead>
					<tr>
						<th width="1%" class="text-nowrap">รหัสแพทย์</th>
						<th class="text-nowrap">วันปฏิบัติงาน</th>
						<th class="text-nowrap">เวลาเข้า - ออก</th>
						<th class="text-nowrap">วันหยุดแพทย์</th>
						<th class="text-nowrap">แก้ไข</th>
					</tr>
				</thead>
				<tbody>
					<tr class="odd gradeX">
						<td class="text-center" width="100%" colspan="5"> 
							<button id="add_formWork" name="add_formWork" value="<?=$medical_id; ?>" class="btn btn-sm btn-primary m-r-5">เพิ่มวันและเวลาปฏิบัติงานของแพทย์
							</button> 
						</td>
					</tr>
					<?php
					foreach($query->result() as $rows){
						$edit_doctorwork_url = base_url()."backend/src/DoctorWork/create_FormDoctor/".$rows->id;
						$delete_doctorwork_url = base_url()."backend/src/DoctorWork/delete_doctorwork_FormDoctor/".$rows->id;
					?>
					<tr class="odd gradeX">
						<td width="1%" ><?=$rows->medical_id; ?></td>
						<td class="text-center"><?=$rows->work_days; ?></td>
						<td class="text-center"><?=$rows->work_times_in.' - '.$rows->work_times_out; ?></td>
						<td class="text-center"><?=$rows->out_of_days; ?></td>
						<td width="30%" class="text-center">
							<a class="btn btn-xs btn-warning" value="<?=$edit_doctorwork_url; ?>">
								<i class="fas fa-edit"></i>
							</a>
							<a class="btn btn-xs btn-danger" value="<?=$delete_doctorwork_url; ?>">
								<i class="fas fa-trash"></i>
							</a>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->
</div>
<!-- end col-12 -->