<!-- begin #content -->
<div id="content" class="content">		
	<h1>จัดการคอนเทนต์</h1>
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
		<li class="breadcrumb-item active">คอนเทนต์</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<?php
		$create_header_url = base_url()."backend/src/Contents/create";
	?>
	<h1 class="page-header"><a href="<?=$create_header_url; ?>"><button type="button" class="btn btn-lime">Add New Contents</button></a></small></h1>
	
	<?php
		if(isset($flash)){
			echo $flash;
		}
	?>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-lg-12">
			<div class="panel panel-inverse">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">จัดการคอนเทนต์</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<table id="data-table-buttons" class="table table-bordered">
						<thead>
							<tr>
								<th width="1%" class="text-nowrap">#</th>
								<th class="text-nowrap">Title Name</th>
								<th class="text-nowrap">สถานะ</th>
								<th class="text-nowrap">แก้ไข</th>
							</tr>
						</thead>
						<tbody>
							<?php							
							foreach($query->result() as $rows){
								$view_contents_url = base_url()."backend/src/Contents/view_contents_details/".$rows->id;
								$edit_contents_url = base_url()."backend/src/Contents/create/".$rows->id;
								$delete_contents_url = base_url()."backend/src/Contents/delete_contents/".$rows->id;
								$status = $rows->contents_status;

								if($status == 1){
									$status_label = "green";
									$status_desc = "Published";
								}else{
									$status_label = "secondary";
									$status_desc = "Unpublished";
								}

							?>
							<tr class="odd gradeX">
								<td width="1%"><?=$rows->id; ?></td>
								<td><?=$rows->contents_title; ?></td>
								<td class="text-center">
									<span class="label label-<?=$status_label; ?>"><?=$status_desc; ?></span>
								</td>
								<td width="30%" class="text-center">
									<a class="btn btn-xs btn-success" href="<?=$view_contents_url; ?>">
										<i class="fas fa-eye"></i>
									</a>
									<a class="btn btn-xs btn-warning" href="<?=$edit_contents_url; ?>">
										<i class="fas fa-edit"></i>
									</a>
									<a class="btn btn-xs btn-danger" href="<?=$delete_contents_url; ?>">
										<i class="fas fa-trash"></i>
									</a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
				<!-- end panel-body -->
			</div>
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
</div>