<div id="content" class="content content-full-width inbox">
			<!-- begin vertical-box -->
			<div class="vertical-box with-grid">
				<!-- begin vertical-box-column -->
				<div class="vertical-box-column width-200 bg-silver hidden-xs">
					<!-- begin vertical-box -->
					<div class="vertical-box">
						<!-- begin wrapper -->
						<div class="wrapper bg-silver text-center">
							<a href="<?=base_url(); ?>backend/src/Email/Compose" class="btn btn-inverse p-l-40 p-r-40 btn-sm">
								Compose
							</a>
						</div>
						<!-- end wrapper -->
						<!-- begin vertical-box-row -->
						<div class="vertical-box-row">
							<!-- begin vertical-box-cell -->
							<div class="vertical-box-cell">
								<!-- begin vertical-box-inner-cell -->
								<div class="vertical-box-inner-cell">
									<!-- begin scrollbar -->
									<div data-scrollbar="true" data-height="100%">
										<!-- begin wrapper -->
										<div class="wrapper p-0">
											<div class="nav-title"><b>FOLDERS</b></div>
											<?php $this->view($sidelistemail); ?>
										</div>
										<!-- end wrapper -->
									</div>
									<!-- end scrollbar -->
								</div>
								<!-- end vertical-box-inner-cell -->
							</div>
							<!-- end vertical-box-cell -->
						</div>
						<!-- end vertical-box-row -->
					</div>
					<!-- end vertical-box -->
				</div>
				<!-- end vertical-box-column -->
				<!-- begin vertical-box-column -->
				<div class="vertical-box-column bg-white">
					<!-- begin vertical-box -->
					<div class="vertical-box">
						<!-- begin wrapper -->
						<div class="wrapper bg-silver-lighter">
							<!-- begin btn-toolbar -->
							<div class="btn-toolbar">
								<div class="btn-group m-r-5">
									<a href="javascript:;" class="p-t-5 pull-left m-r-3 m-t-2" data-click="email-select-all">
										<i class="far fa-square fa-fw text-muted f-s-16 l-minus-2"></i>
									</a>
								</div>
								<!-- begin btn-group (Duplicate) -->
								<div class="btn-group dropdown m-r-5">
									<button class="btn btn-white btn-sm" data-toggle="dropdown">
										View All <span class="caret m-l-3"></span>
									</button>									
									<ul class="dropdown-menu text-left text-sm">
										<?php 
										if(!isset($selection)){ echo '<li class="active">'; }
										else{ echo '<li>'; }
										?>
										<a href="<?=base_url(); ?>backend/src/Email/Inbox"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> All</a></li>
										<?php 
										if(isset($selection) && $selection == 0 ){ echo '<li class="active">'; }
										else{ echo '<li>'; }
										?>
										<a href="<?=base_url(); ?>backend/src/Email/Inbox/<?php echo $select='0';?>"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> Unread</a></li>
										<?php 
										if(isset($selection) && $selection == 1 ){ echo '<li class="active">'; }
										else{ echo '<li>'; }
										?><a href="<?=base_url(); ?>backend/src/Email/Inbox/<?php echo $select='1';?>"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> Already Read</a></li>
										<!-- <li><a href="javascript:;"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> Contacts</a></li>
										<li><a href="javascript:;"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> Groups</a></li>
										<li><a href="javascript:;"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> Newsletters</a></li>
										<li><a href="javascript:;"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> Social updates</a></li>
										<li><a href="javascript:;"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> Everything else</a></li> -->
									</ul>
 									
								</div>

								<!-- end btn-group -->
								<!-- begin btn-group -->
								<!-- 
								<div class="btn-group dropdown m-r-5">
									<button class="btn btn-white btn-sm" data-toggle="dropdown">
										View All <span class="caret m-l-3"></span>
									</button>									
									<ul class="dropdown-menu text-left text-sm">
										<li class="active"><a href="javascript:;"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> All</a></li>
										<li><a href="javascript:;"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> Unread</a></li>
										<li><a href="javascript:;"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> Contacts</a></li>
										<li><a href="javascript:;"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> Groups</a></li>
										<li><a href="javascript:;"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> Newsletters</a></li>
										<li><a href="javascript:;"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> Social updates</a></li>
										<li><a href="javascript:;"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> Everything else</a></li>
									</ul>
 									
								</div>
								-->
								<!-- end btn-group -->
								<!-- begin btn-group -->
								<div class="btn-group m-r-5">
									<button class="btn btn-sm btn-white" href="<?=$reflink;?>"><i class="fa fa-redo f-s-14 t-plus-1"></i></button>
								</div>
								<!-- end btn-group -->
								<!-- begin btn-group -->
								<div class="btn-group">
									<button class="btn btn-sm btn-white hide" data-email-action="delete"><i class="fa t-plus-1 fa-times f-s-14 m-r-3"></i> <span class="hidden-xs">Delete</span></button>
									<button class="btn btn-sm btn-white hide" data-email-action="archive"><i class="fa t-plus-1 fa-folder f-s-14 m-r-3"></i> <span class="hidden-xs">Archive</span></button>
									<button class="btn btn-sm btn-white hide" data-email-action="archive"><i class="fa t-plus-1 fa-trash f-s-14 m-r-3"></i> <span class="hidden-xs">Junk</span></button>
								</div>
								<!-- end btn-group -->
								<!-- begin btn-group -->
								<div class="btn-group ml-auto">
									<button class="btn btn-white btn-sm">
										<i class="fa fa-chevron-left f-s-14 t-plus-1"></i>
									</button>
									<button class="btn btn-white btn-sm">
										<i class="fa fa-chevron-right f-s-14 t-plus-1"></i>
									</button>
								</div>
								<!-- end btn-group -->
							</div>
							<!-- end btn-toolbar -->
						</div>
						<!-- end wrapper -->
						<!-- begin vertical-box-row -->
						<div class="vertical-box-row">
							<!-- begin vertical-box-cell -->
							<div class="vertical-box-cell">
								<!-- begin vertical-box-inner-cell -->
								<div class="vertical-box-inner-cell">
									<!-- begin scrollbar -->
									<div data-scrollbar="true" data-height="100%">
										<!-- begin list-email -->
										<ul class="list-group list-group-lg no-radius list-email">
											<?php 
											foreach ($query as $result) {
											?>
												<?php
												if($result->email_open > 0 ){
												?>
													<li class="list-group-item">
												<?php
												}else{
												?>
													<li class="list-group-item unread">
												<?php
												}
												?>												
												<div class="email-checkbox">
													<label>
														<i class="far fa-square"></i>
														<input type="checkbox" data-checked="email-checkbox" value="<?=$result->id;?>" />
													</label>
												</div>
												<a href="<?=base_url(); ?>backend/src/Email/MailDetails/<?=$result->id;?>" class="email-user bg-gradient-blue">
													<span class="text-white">F</span>
												</a>
												<div class="email-info">
													<a href="<?=base_url(); ?>backend/src/Email/MailDetails/<?=$result->id;?>">
														<!-- <span class="email-time">Today</span> -->
														<span class="email-sender"><?=$result->email_target;?></span>
														<span class="email-title"><?=$result->email_subject;?></span>
														<span class="email-desc"><?=$result->email_message;?></span>
													</a>
												</div>

												</li>
											<?php 
											}
											?>
										</ul>
										<!-- end list-email -->
									</div>
									<!-- end scrollbar -->
								</div>
								<!-- end vertical-box-inner-cell -->
							</div>
							<!-- end vertical-box-cell -->
						</div>
						<!-- end vertical-box-row -->
						<!-- begin wrapper -->
						<div class="wrapper bg-silver-lighter clearfix">
							<div class="btn-group pull-right">
								<!-- 
								<button class="btn btn-white btn-sm">
									<i class="fa fa-chevron-left f-s-14 t-plus-1"></i>
								</button>
								<button class="btn btn-white btn-sm">
									<i class="fa fa-chevron-right f-s-14 t-plus-1"></i>
								</button> 
								-->
							</div>
							<div class="m-t-5 text-inverse f-w-600"><?=$query_count;?> Messages</div>
						</div>
						<!-- end wrapper -->
					</div>
					<!-- end vertical-box -->
				</div>
				<!-- end vertical-box-column -->
			</div>
			<!-- end vertical-box -->
		</div>