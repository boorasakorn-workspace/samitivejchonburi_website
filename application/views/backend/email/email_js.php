<script type="text/javascript">
	$(document).ready(function() {
		console.log("ready");
		$(document).on('click', '#sendButton', function(e) {
			//e.preventDefault();
			if($('#email-to').text()){
				var targetEmail = $.trim($('#email-to').text());
				var t_m_arr = targetEmail.split('×');
			}
			if($('#email-cc-Cc').text()){
				var ccemail = $.trim($('#email-cc-Cc').text());
				var CC_t_m_arr = ccemail.split('×');
			}
			if($('#email-cc-Bcc').text()){
				var Bccemail = $.trim($('#email-cc-Bcc').text());
				var BCC_t_m_arr = Bccemail.split('×');
			}		
			var subject = $('#email-subject').val();
			var content = $('#wysihtml5').val();

			//console.log("All Target Email : " + targetEmail);
			//console.log("All CC Target Email : " + ccemail);
			//console.log("All BCC Target Email : " + Bccemail);
			
			if($('#email-to').text()){
				var target_email_to = t_m_arr[0];
				for (var i = 1 ; i < t_m_arr.length - 1; i++) {
					target_email_to = target_email_to + ',' + t_m_arr[i];
				}
				//console.log("All Current Email " + " : " + target_email_to);
			}

			if($('#email-cc-Cc').text()){
				var CC_target_email_to = CC_t_m_arr[0];
				for (var i = 1 ; i < CC_t_m_arr.length - 1; i++) {
					CC_target_email_to = CC_target_email_to + ',' + CC_t_m_arr[i];
				}
				//console.log("All Current CC Email " + " : " + CC_target_email_to);
			}
			if($('#email-cc-Bcc').text()){
				var BCC_target_email_to = BCC_t_m_arr[0];
				for (var i = 1 ; i < BCC_t_m_arr.length - 1; i++) {
					BCC_target_email_to = BCC_target_email_to + ',' + BCC_t_m_arr[i];

				}
				//console.log("All Current BCC Email " + " : " + BCC_target_email_to);
			}
			//console.log("Subject : " + subject);
			//console.log("Content " + content);
			/*
			var data = {
			    subject: subject,
			    email: target_email_to,
			    CCemail: CC_target_email_to,
			    BCCemail: BCC_target_email_to,
			    message: content
			};
			*/
			$.ajax({
			    type: "POST",
        		dataType : "json",
			    //url: "email_send/email_send.php",
			    url: "<?php echo base_url(); ?>backend/src/Email/Sending",
			    data: {
				    "subject": subject,
				    "email": target_email_to,
				    "CCemail": CC_target_email_to,
				    "BCCemail": BCC_target_email_to,
				    "message": content
				}
			});
	    	location.reload();
		});

		$(document).on('click', '#discardButton', function(e) {
	    	location.reload();
			//e.preventDefault();
	    });
	});
</script>  