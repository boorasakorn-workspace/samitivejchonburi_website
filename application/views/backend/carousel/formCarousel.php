<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
		<li class="breadcrumb-item active">Slide Carousel Form</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header"><?=$headline; ?></h1>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">จัดการ Slide Carousel Details</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<?=validation_errors("<p style='color:red;'>", "</p>") ?>
					<?php
						if(is_numeric($update_id)) {
					?>
                        <div class="form-group row fileupload-buttonbar">
                            <div class="col-md-12">
                                <span class="fileinput-button m-r-3">
									<?php if($carousel_big_imgs==''){ ?>
                                    <i class="fa fa-plus"></i>
                                    <a href="<?=base_url(); ?>backend/src/MainCarousel/upload_image/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-primary" id="upload_img" name="upload_img">Upload Slide Image</button>
                                    </a>
                                	<?php }else if($carousel_big_imgs!='' && $carousel_type==1){ ?>
                                    <i class="fa fa-times"></i>
                                    <!--
									<a href="<?=base_url(); ?>backend/src/MainCarousel/delete_image/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-danger" id="delete_img" name="delete_img">Delete Slide Image</button>
                                    </a>
	                                -->

									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteImgModal">Delete Slide Image</button>

									<!-- Modal -->
									<div id="deleteImgModal" class="modal fade" role="dialog">
									  <div class="modal-dialog">
									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">Delete Slide Image</h4>
									      </div>
									      <div class="modal-body">
									        <p>Sure to delete slide image?</p>
									      </div>
									      <div class="modal-footer">
									      	<a href="<?=base_url(); ?>backend/src/MainCarousel/delete_image/<?=$update_id; ?>">
		                                    	<button type="button" class="btn btn-danger" id="delete_img" name="delete_img">Delete Slide Image</button>
		                                    </a>
									        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
									      </div>
									    </div>

									  </div>
									</div>

                                	<?php } ?>
									<?php if($carousel_big_imgs==''){ ?>
                                	<a href="<?=base_url(); ?>backend/src/MainCarousel/upload_video/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-primary" id="upload_vdo" name="upload_vdo">Upload Slide Video</button>
                                    </a>
                                    <?php }else if($carousel_big_imgs!='' && $carousel_type==2){ ?>
                                    <i class="fa fa-times"></i>
                                    <!--
									<a href="<?=base_url(); ?>backend/src/MainCarousel/delete_vdo/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-danger" id="delete_vdo" name="delete_vdo">Delete Slide Video</button>
                                    </a>
	                                -->

									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteVdoModal">Delete Slide Video</button>

									<!-- Modal -->
									<div id="deleteVdoModal" class="modal fade" role="dialog">
									  <div class="modal-dialog">
									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">Delete Slide Video</h4>
									      </div>
									      <div class="modal-body">
									        <p>Sure to delete slide video?</p>
									      </div>
									      <div class="modal-footer">
									      	<a href="<?=base_url(); ?>backend/src/MainCarousel/delete_vdo/<?=$update_id; ?>">
		                                    	<button type="button" class="btn btn-danger" id="delete_vdo" name="delete_vdo">Delete Slide Video</button>
		                                    </a>
									        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
									      </div>
									    </div>

									  </div>
									</div>

                                    <?php }if($carousel_small_imgs==''){ ?>
                                	<a href="<?=base_url(); ?>backend/src/MainCarousel/upload_image_video/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-primary" id="upload_img_vdo" name="upload_img_vdo">Upload Image Video</button>
                                    </a>
                                    <?php }else if($carousel_small_imgs!='' && $carousel_type==2){ ?>
                                	<!--
                                    <a href="<?=base_url(); ?>backend/src/MainCarousel/delete_image_video/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-danger" id="delete_img_vdo" name="delete_img_vdo">Delete Image Video</button>
                                    </a>	
	                                -->

									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteImgVdoModal">Delete Image Video</button>

									<!-- Modal -->
									<div id="deleteImgVdoModal" class="modal fade" role="dialog">
									  <div class="modal-dialog">
									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">Delete Image Video</h4>
									      </div>
									      <div class="modal-body">
									        <p>Sure to delete image video?</p>
									      </div>
									      <div class="modal-footer">
									      	<a href="<?=base_url(); ?>backend/src/MainCarousel/delete_image_video/<?=$update_id; ?>">
		                                    	<button type="button" class="btn btn-danger" id="delete_img_vdo" name="delete_img_vdo">Delete Image Video</button>
		                                    </a>
									        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
									      </div>
									    </div>

									  </div>
									</div>

                                	<?php } ?>
                                	<!--
                                    <a href="<?=base_url(); ?>backend/src/MainCarousel/delete_carousel/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-danger" id="delete_carousel" name="delete_carousel">Delete Slide Carousel</button>
                                    </a>
	                                -->

									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">Delete Slide Carousel</button>

									<!-- Modal -->
									<div id="deleteModal" class="modal fade" role="dialog">
									  <div class="modal-dialog">
									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">Delete Slide Carousel</h4>
									      </div>
									      <div class="modal-body">
									        <p>Sure to delete slide carousel?</p>
									      </div>
									      <div class="modal-footer">
		                                    <a href="<?=base_url(); ?>backend/src/MainCarousel/delete_carousel/<?=$update_id; ?>">
		                                    	<button type="button" class="btn btn-danger" id="delete_carousel" name="delete_carousel">Delete Slide Carousel</button>
		                                    </a>
									        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
									      </div>
									    </div>

									  </div>
									</div>


                                    <a href="<?=base_url(); ?>backend/src/MainCarousel/view_carousel_details/<?=$update_id; ?>">
                                        <button type="button" class="btn btn-success" id="view_carousel" name="view_carousel"><i class="fas fa-eye"></i> View Slide Carousel In Front</button>
                                    </a>
                                </span>
                            </div>
                        </div>
					<?php }
						$location_form = base_url()."backend/src/MainCarousel/create/".$update_id;
					?>
					<form class="form-horizontal" action="<?= $location_form; ?>" method="POST" enctype="multipart/form-data">

						<div class="row row-space-10">
		                    <div class="col-md-12">
								<div class="form-group">
		                            <label for="carousel_title">ชื่อสไลด์</label>
		                            <input type="text" class="form-control" id="carousel_title" name="carousel_title" value="<?=$carousel_title; ?>"  placeholder="Title" />
		                        </div>
		                    </div>
		                </div>
		                <div class="row row-space-10">
		                    <div class="col-md-6">
								<div class="form-group">
		                            <label for="carousel_h1">หัวเรื่อง H1</label>
	                            	<input type="text" class="form-control" id="carousel_h1" name="carousel_h1" value="<?=$carousel_h1; ?>"  placeholder="Header Text" />
		                        </div>
		                    </div>
                   			<div class="col-md-6">
								<div class="form-group">
		                            <label for="carousel_h2">หัวเรื่อง H2</label>
		                            <input type="text" class="form-control" name="carousel_h2" value="<?=$carousel_h2; ?>" placeholder="Header Text" >
		                        </div>
		                    </div>
                   		</div>
                   		<div class="row row-space-10">
		                    <div class="col-md-6">
								<div class="form-group">
		                            <label for="carousel_button1">ข้อความในปุ่มที่ 1</label>
		                            <input type="text" class="form-control" name="carousel_button1" value="<?=$carousel_button1; ?>" placeholder="Button Text">
		                        </div>
		                    </div>
		                    <div class="col-md-6">
								<div class="form-group">
		                            <label for="carousel_button2">ข้อความในปุ่มที่ 2</label>
		                            <input type="text" class="form-control" name="carousel_button2" value="<?=$carousel_button2; ?>" placeholder="Button Text">
		                        </div>
		                    </div>
                   		</div>
                   		<div class="row row-space-10">
		                    <div class="col-md-6">
								<div class="form-group">
		                            <label for="carousel_button1">ลิ้งค์ปุ่มที่ 1</label>
		                            <input type="text" class="form-control" name="carousel_button1_link" value="<?=$carousel_button1_link; ?>" placeholder="Button Link">
		                        </div>
		                    </div>
		                    <div class="col-md-6">
								<div class="form-group">
		                            <label for="carousel_button2">ลิ้งค์ปุ่มที่ 2</label>
		                            <input type="text" class="form-control" name="carousel_button2_link" value="<?=$carousel_button2_link; ?>" placeholder="Button Link">
		                        </div>
		                    </div>
                   		</div>
                   		<div class="row row-space-10">
                   			<div class="col-md-12">
								<div class="form-group">
		                            <label for="carousel_description">รายละเอียดบทความสไลด์</label>
									<textarea class="ckeditor" id="editor4" name="carousel_description" rows="20" placeholder="Slide Description"/><?=$carousel_description; ?></textarea>
		                        </div>
		                    </div>
                   		</div>

						<div class="form-group row">
                            <label class="col-md-1 col-form-label" for="carousel_status">สถานะ<span class="text-danger">*</span></label>
                            <div class="col-md-4">
							<?php
							$additional_dd_code = "id='selectStatusCode' class='form-control'";
								$options = array(
												'' => 'โปรดเลือกสถานะสไลด์บทความ',
												'1' => 'Published',
												'0' => 'Unpublished'
											);

								echo form_dropdown('carousel_status', $options, $carousel_status, $additional_dd_code);
							?>
							</div>
                        </div>                        
						<div class="form-group row">
                            <div class="col-md-4">
								<div class="form-group">
		                            <label for="meta_author">Meta Author</label>
		                            <input type="text" class="form-control" id="meta_author" name="meta_author" value="<?=$meta_author; ?>" placeholder="Author" />
	                            </div>  
		                    </div>
                            <div class="col-md-4">
								<div class="form-group">
		                            <label for="meta_keyword">Meta Keyword</label>
		                            <input type="text" class="form-control" id="meta_keywords" name="meta_keywords" value="<?=$meta_keywords; ?>" placeholder="Keywords" />
	                            </div>  
		                    </div>
                            <div class="col-md-4">
								<div class="form-group">
		                            <label for="meta_desciption">Meta Description</label>
		                            <input type="text" class="form-control" id="meta_description" name="meta_description" value="<?=$meta_description; ?>" placeholder="Description" />
	                            </div>  
		                    </div>
                        </div>
						<?php if($carousel_big_imgs!=''){ //ต้องทำการส่งค่า item_big_img มาจาก controller store_items->create ก่อน ?>
                        <div class="form-group row">
                            <label class="col-md-1 col-form-label">Big Image</label>
                            <div class="col-md-4">
								<img src="<?=base_url(); ?>gallery/carousel/big_imgs/<?=$carousel_big_imgs; ?>">
							</div>
                        </div>
                    	<?php }
							if(is_numeric($update_id)){
								echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">แก้ไขข้อมูลสไลด์บทความ</button>';
							}else{
								echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">เพิ่มข้อมูลสไลด์บทความ</button>';
                    		}
						?>
                        <button type="submit" id="submit" name="submit" value="Cancel" class="btn btn-sm btn-warning m-r-5">ยกเลิก</button>
                    </form>
				</div>
				<!-- end panel-body -->
			</div>
			<!-- end panel -->
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
</div>
<!-- end #content -->
