<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<link rel="stylesheet" href="<?php echo base_url('public/css/bootstrap.min.css');?>">
<script src="<?php echo base_url('public/js/bootstrap.min.js');?>"></script>

<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
<title><?php echo $this->lang->line('welcome') ?></title>
<link rel="stylesheet" href="<?php echo base_url('public/font-awesome/css/font-awesome.min.css'); ?>">

<link href="https://fonts.googleapis.com/css?family=Mitr:200,300,400,500,600,700&display=swap" rel="stylesheet">
<html lang="en">
<head>
   <meta charset="utf-8">
   <title>แบบประเมินความเสี่ยงก่อนมาโรงพยาบาลต่อการติดเชื้อ COVID-19</title>

   <style type="text/css">
      a{
         text-decoration: none;
      }
      .h-covid{
         font-family: Mitr;
         font-weight: 400;
         font-size: 30px;
      }
      .txt{
         font-family: Mitr;
         font-weight: 300;
         font-size: 20px;
      }

      .txt-f{
         font-family: Mitr;
         font-weight: 400;
         font-size: 20px;
      }
      .txt-form{
         font-family: Mitr;
         font-weight: 300;
         font-size: 17px;
      }
      .txt-b{
         font-family: Mitr;
         font-weight: 400;
         font-size: 20px;
         color:#000;
      }
      .txt-s{
         font-family: Mitr;
         font-weight: 200;
         font-size: 19px;
      }
      .lang{
         font-family: Mitr;
         font-weight: 400;
         font-size: 17px;
         color: #000;
      }

      .green{
        background-color: #7ac37a;
        color: #fff;
     }
     .yellow{
        background-color: #fcfc8f;
        color: #000;
     }
     .red{
      background-color: #fe6868;
      color: #fff;
   }


   .btn-submit{
      color: #fff;
      background-color: #005f3b;
      border-color: #005f3b;
      font-family: Mitr;
      font-weight: 200;
      font-size: 25px;
   }
   .btn-demo{
      color: #fff;
      background-color: #c2a472;
      border-color: #c2a472;
      font-family: Mitr;
      font-weight: 200;
      font-size: 25px;
   }



</style>
</head>

<div class="container-fluid" style="background-color:#03794c;">
   <div>
      <a href="https://www.samitivejchonburi.com/Welcome/covid">
       <img src="<?php echo base_url('public/images/logo.png');?>">
    </a>
 </div>
</div>
<div class="container">

 <div class="row text-center">
   <div class="col-12">
    <div class="h-covid text-center">
      แบบคัดกรองผู้ป่วย ไวรัสโคโรนา 2019 (COVID-19)
   </div>
</div>
</div>
<div>
   <div class="txt">
      แบบสอบถามนี้เป็นการประเมินเบื้องต้นเท่านั้น การตัดสินใจส่งตรวจหรือไม่ ขึ้นอยู่กับการซักประวัติเพิ่มเติมและดุลยพินิจของแพทย์/พยาบาลเป็นหลัก
   </div>
  <!--  <div class="txt text-center">
      พื้นที่ที่มีการรายงานการระบาดต่อเนื่องของโรคติดเชื้อไวรัสโคโรนา สายพันธุ์ใหม่ 2019
      สาธารณรัฐแระชาชนจีน ฮ่องกง มาเก๊า เกาหลีใต้ อิตาลี อิหร่าน ฝรั่งเศส สเปน สหรัฐอเมริกา สวิตเซอร์แลนด์ นอร์เวย์ ญี่ปุ่น เดนมาร์ก เนเธอร์แลนด์ สวีเดน อังกฤษ เยอรมนี ออสเตรีย เบลเยียม มาเลเซีย แคนาดา โปรตุเกส บราซิล เซ็กเกีย อิสราเอล ออสเตรเลีย เกาะไอร์แสนด์ ปากีสถาน ตุรกี ชิลี ลักเซมเบิร์ก เอกวาดอร์ กรีซ ฟินแลนด์
   </div> -->
   <!-- <div class="txt"> -->
  <!--     <div class="txt-b">
         ***ผู้ป่วยเข้าเกณฑ์สอบสวนโรค Patient under investigation : PUI***
      </div>

      <div class="row mb-1 rounded">
         <div style="float: left;width: 100%;border-radius: 10px;padding: 3px;" class="green">
          <div> 0-5 คะแนน "ไม่มีความเสี่ยง"</div>
          <div>ข้อแนะนำวิธีการปฏิบัติตัวในช่วงของการระบาด Covid-19</div>
          <div> 1.หมั่นล้างมือให้สะอาดด้วยสบุ่ หริอแอลกอฮอล์เจล</div>
          <div> 2.ใช้หน้ากากอนามัยหรือผ้าปิดปากและจมูก เพื่อป้องกันการได้รับเชื้อโรค</div>
          <div>  3.Social Distancing การเว้นระยะห่างระหว่างตัวคุณกับบุคคลอื่น</div>
       </div>

    </div> -->
  <!--   <div class="row mb-1 rounded">
      <div style="float: left;width: 100%;border-radius: 10px;padding: 3px;" class="yellow">
        <div> 10-20 คะแนน "คุณมีความเสี่ยงอาจติดเชื้อ Covid-19"</div>
        <div >เรามีคำแนะนำให้ท่าน</div>
        <div>1.ใช้หน้ากากอนามัยหรือผ้าปิดปากและจมูก เพื่อป้องกันการแพร่เชื้อโรค</div>
        <div> 2.Social Distancing การเว้นระยะห่างระหว่างตัวคุณกับบุคคลอื่น</div>
        <div>เมื่อมาถึง ร.พ กรุณาแจ้งเจ้าหน้าทีทันที เพื่ออำนวยความสะดวกในการพบแพทย์ที่คลินิคระบบทางเดินหายใจ (ARI CLINIC) กรุณามาตรวจตั้งแต่เวลา 8.00-20.00 น.</div>
     </div>
  </div> -->
<!--   <div class="row mb-1 rounded">
   <div style="float: left;width: 100%;border-radius: 10px;padding: 3px;" class="red">
    <div> 25 คะแนนขึ้นไป "คุณมีความเสี่ยงสูง อาจติดเชื้อ Covid-19"</div> 
    <div>  เรามีคำแนะนำให้ท่าน</div>
    <div>  1.ใช้หน้ากากอนามัยหรือผ้าปิดจมูก เพื่อป้องการแพร่เชื้อโรค </div>
    <div>  2.Social Distancing การเว้นระยะระหว่างตัวคุณกับบุคคลอื่น</div>
    <div> กรุณาแจ้งเจ้าหน้าที่ เมื่อถึง ร.พ.เพื่ออำนวยความสะดวกในการเข้าห้องตรวจเฉพาะ</div>

 </div>  -->
<!--   <div class="text-center">
   แบบสอบถามนี้เป็นการประเมินเบื้องต้นเท่านั้น การตัดสินใจส่งตรวจหรือไม่ ขึ้นอยู่กับการซักประวัติเพิ่มเติมและดุลยพินิจของแพทย์/พยาบาลเป็นหลัก
</div> -->



<div id="yellow-detail" style="display: none;background-color:yellow;color:#000;padding: 3px;">
   <div>15-20 คะแนน มีความเสี่ยง Zone สีเหลือง  </div> 
   <div>กรุณาปฏิบัติตามข้อแนะนำ</div> 

</div>

</div>

</div>

</div>
<div class="container">
   <form method="post" id="submit" action="<?php echo base_url('Welcome/save_poll');?>"> 

      <div class="h-covid">
         กรุณาให้ข้อมูลตามความเป็นจริงเพื่อประโยชน์ของท่าน
      </div>
      <div class="txt">
         ชื่อ - นามสกุล *
      </div>
      <div class="form-group">
         <input type="text" name="name" class="form-control txt" placeholder="ระบุชื่อของคุณ" id="name">
      </div>
      <div class="txt">
         กรุณาระบุช่องทางการติดต่อ เบอร์โทร/อีเมล์/ไลน์/อื่นๆ
      </div>
      <div class="form-group">
         <input type="text" name="line" class="form-control txt" placeholder="ระบุช่องทางการติดต่อ">
      </div>
      <div class="txt">
         อายุของท่าน(ปี)
      </div>
      <div class="form-group">
         <input type="text" name="age" class="form-control txt" placeholder="ระบุอายุของคุณ">
      </div>
      <?php   $ip = $this->input->ip_address();?>
      <input type="hidden" name="point_sum"  id="point_sum">
      <input type="hidden" name="ip" value="<?php echo $ip;?>">
      <input type="hidden" id="point0" value="0">
      <input type="hidden" id="point1" value="0">
      <input type="hidden" id="point2" value="0">
      <input type="hidden" id="point3" value="0">
      <input type="hidden" id="point4" value="0">
      <input type="hidden" id="point5" value="0">
      <input type="hidden" id="point6" value="0">
      <input type="hidden" id="point7" value="0">
      <input type="hidden" id="point8" value="0">
      <div>
         <?php $no=0;?>
         <?php $qno=1;?>
         <div class="col-lg-12 mx-auto col-12">
            <table class="table txt table-bordered">
               <tr class="txt-b">
                  <td>ลำดับ</td>
                  <td>คำถาม</td>
                  <!-- <td>คำตอบ</td> -->
               </tr>
               <?php foreach ($ques as $result) :?>
                  <tr>
                     <td width="10%">
                      <?php echo $qno.'.';?>
                   </td>
                   <td width="50%">
                     <?php echo $result->question;?>
                     <div class="txt-b">
                        <input type="radio" name="answer<?php echo $no;?>" value="Y" data-point="<?php echo $result->choice1_point;?>"> <?php echo $result->choice1;?>
                     </div>
                     <div class="txt-b mb-2">
                        <input type="radio" name="answer<?php echo $no;?>" value='N' data-point="<?php echo $result->choice2_point;?>" checked="checked"> <?php echo $result->choice2;?>
                     </div>
                  </td>

               </tr>





               <?php if($qno==8){;?>
                  <tr><td colspan="3">
                     <img src="<?php echo base_url('public/images/p2n.jpg');?>" class="img-fluid">
                  </td>
               </tr>
            <?php                        };?>
            <div class="txt">
              <input type="hidden" name="queustion[]" value="<?php echo $result->uid;?>">
           </div>
           <?php $no++;?>
           <?php $qno++;?>
        <?php endforeach;?>
        <tr>
         <td colspan="3" class='txt'>
            <div>รวมคะแนน</div>
            <div id="point_total" style="padding: 5px;background-color: #ccc;"> 0</div>
            <!-- <div> -->
 <!--            ***ผู้ป่วยเข้าเกณฑ์ PUI
         </div>
         <div>
            <div style="float: left;width: 100%" id="green_zone">
               0-10 คะแนน ไม่มีความเสี่ยง  Zone สีเขียว
            </div>

         </div>
         <div>
            <div style="float: left;width: 100%" id="yellow_zone">
               15-20 คะแนน มีความเสี่ยง Zone สีเหลือง
            </div>
         </div>
         <div>
            <div style="float: left;width: 100%" id="red_zone">
            25 คะแนนขึ้นไป มีความเสี่ยงสูง Zone สีแดง</div> 
         </div> 

         <div>
            แบบสอบถามนี้เป็นการประเมินเบื้องต้นเท่านั้น การตัดสินใจส่งตรวจหรือไม่ ขึ้นอยู่กับการซักประวัติเพิ่มเติมและดุลยพินิจของแพทย์/พยาบาลเป็นหลัก
         </div> -->
         <div id="yellow-detail" style="display: none;background-color:yellow;color:#000;padding: 3px;">
            <div>15-20 คะแนน มีความเสี่ยง Zone สีเหลือง  </div> 
            <div>กรุณาปฏิบัติตามข้อแนะนำ</div> 
         </div>
      </td>
   </tr>
</table>
</div>
</div>

<i id="loading" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: relative;left: 40%;bottom: 1%;display: none; z-index: 9"></i>
<div class="row mt-4 mb-4">

   <div class="col-lg-6 col-12 text-center">
      <div class="txt-f">
       <div> หากท่านใช้ข้อมูลจริง</div>
       <div>กรุณากดที่ปุ่ม ทำการประเมิน</div>
    </div>
    <div>
      <div class="text-center mb-3 col-lg-6 mx-auto col-12">
         <button class="btn btn-submit btn-block" id="poll_save" type="button">ทำแบบประเมิน</button>
      </div>
   </div>
</div>

<div class="col-lg-6 col-12 text-center">
   <div class="txt-f">
      <div>หากท่านทดลองระบบ</div>
      <div>หรือไม่ใช้ข้อมูลตามจริงโปรดกดที่ ทดลองระบบ</div>
      <div class="text-center mb-3 col-lg-6 mx-auto col-12">
         <button class="btn btn-demo btn-block" type="button" id="btn-demo">ทดลองระบบ</button>
      </div>
   </div>
</div>
</div>
</form>
</div>
</div>
</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>



<div class="modal" id="alert">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

       <div class="modal-body text-center">
         <div id="alert-txt" class="txt"></div>
         <button type="button" class="btn btn-secondary txt" data-dismiss="modal">ตกลง</button>
      </div>




   </div>
</div>
</div>


<div class="modal" id="success">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-body txt">
         <div class="text-center">
            ประเมินแบบสอบถามเรียบร้อย
         </div>
         <div class="text-center">
            คะเแนนจากผลการประเมิน 
         </div> 
         <div id="point-modal" class="txt-b text-center">
         </div>
<!-- 

         <div id="green_zone_modal" >
            0-10 คะแนน ไม่มีความเสี่ยง  Zone สีเขียว
         </div>
         <div  id="yellow_zone_modal">
            15-20 คะแนน มีความเสี่ยง Zone สีเหลือง
         </div>
         <div  id="red_zone_modal">
            25 คะแนนขึ้นไป มีความเสี่ยงสูง Zone สีแดง
         </div> 
      -->




<!-- 

         <div class="mt-4" id="yellow-detail-modal" style="display: none;background-color:yellow;color:#000;padding: 3px;">
            <div>15-20 คะแนน มีความเสี่ยง Zone สีเหลือง  </div> 
            <div>กรุณาปฏิบัติตามข้อแนะนำ</div> 

         </div>
      -->



<!-- 
         <div class="mt-4" id="green-detail-modal" style="display: none;background-color:green;color:#fff;padding: 3px;">
            <div>0-10 คะแนน ไม่มีความเสี่ยง Zone สีเขียว  </div> 
            <div>การมาตรวจที่รพ.โดยไม่จำเป็น</div> 
            <div> 1. อาจทำให้ท่านมีโอกาสได้รับเชื้อมากขึ้น</div> 
            <div> 2. อาจจะไม่ได้รับการตรวจ / ผลการตรวจว่าไม่มีเชื้อ ไม่สามารถยืนยันว่า ไม่ติดเชื้อ (อาจอยู่ในระยะฟักตัว)</div> 
            <div> วิธีการปฏิบัติตัวในช่วงของการระบาด Covid-19</div> 
            <div> 1. หมั่นล้างมือให้สะอาดด้วยสบู่ หรือแอลกอฮอล์เจล</div> 
            <div> 2. หลีกเลี่ยงการสัมผัสผู้ที่มีอาการคล้ายไข้หวัด หรือหลีกเลี่ยงการไปที่มีฝูงชน</div> 
            <div> 3. ปรุงอาหารประเภทเนื้อสัตว์และไข่ให้สุกด้วยความร้อน</div> 
            <div> 4. ใช้ผ้าปิดปากหรือจมูก เพื่อป้องกันการได้รับเชื้อโรค</div> 
         </div>
      -->

   <!--       <div class="mt-4" id="red-detail-modal" style="display: none;background-color:red;color:#fff;padding: 3px;">
            <div>
               25 คะแนนขึ้นไป มีความเสี่ยงสูง Zone สีแดง   เรามีคำแนะนำให้ท่าน
            </div>
            <div>
               1. โปรดงดออกจากบ้าน 
            </div>
            <div>
               2. เลี่ยงการปฏิสัมพันธ์กับคนอื่น
            </div>
            <div>
               3. แพทย์และพยาบาลจะทำการติดต่อท่านเพื่อสอบถามอาการ เพื่อให้ความช่วยเหลืออื่นๆต่อไป
            </div>
         </div>
      -->

 <!--         <div class="txt">
            <div class="h-covid">คุณไม่มีความเสี่ยงในการได้รับเชื้อ COVID-19</div>

            <div>  การมาตรวจที่รพ.โดยไม่จำเป็น</div>
            <div> 1. อาจทำให้ท่านมีโอกาสได้รับเชื้อมากขึ้น</div>
            <div>  2. อาจจะไม่ได้รับการตรวจ /ผลตรวจว่าไม่มีเชื้อ ไม่สามารถยืนยันว่า ไม่ติดเชื้อ (อาจอยู่ในระยะฟักตัว)</div>

            <div>  วิธีปฎิบัติตัวในช่วงของการระบาด COVID-19</div>
            <div>  1. หมั่นล้างมือให้สะอาดด้วยสบู่หรือแอลกอฮอล์เจล</div>
            <div>  2. หลีกเลี่ยงการสัมผัสผู้ที่มีอาการคล้ายไข้หวัด หรือหลีกเลี่ยงการไปที่มีฝูงชน</div>
            <div>  3. ปรุงอาหารประเภทเนื้อสัตว์และไข่ให้สุกด้วยความร้อน</div>
            <div>  4. ให้ผ้าปิดปากหรือจมูก เพื่อป้องกันการได้รับเชื้อโรค</div>
         </div>

      </div> -->
      <!-- Modal footer -->
      <div class="modal-footer">
        <!-- <a href="<?php echo base_url('Welcome/print');?>" class="btn btn-submit txt" id="print">พิมพ์</a> -->
        <button type="button" class="btn btn-submit txt" data-dismiss="modal">ตกลง</button>
     </div>
  </div>
</div>
<script type="text/javascript">

   $('#btn-demo').click(function(){
    $('#loading').css('display','block');
    $('#green_zone').removeClass("green");
    $('#yellow_zone').removeClass("yellow");
    $('#red_zone').removeClass("red");
    $('#red-detail').css("display", "none");
    $('#yellow-detail').css("display", "none");
    $('#green-detail').css("display", "none");


    $('#green_zone_modal').removeClass("green");
    $('#yellow_zone_modal').removeClass("yellow");
    $('#red_zone_modal').removeClass("red");
    $('#red-detail-modal').css("display", "none");
    $('#yellow-detail-modal').css("display", "none");
    $('#green-detail-modal').css("display", "none");




    var point0=$('#point0').val();
    var point1=$('#point1').val();
    var point2=$('#point2').val();
    var point3=$('#point3').val();
    var point4=$('#point4').val();
    var point5=$('#point5').val();
    var point6=$('#point6').val();
    var point7=$('#point7').val();
    var point8=$('#point8').val();
    var total=parseInt(point0)+parseInt(point1)+parseInt(point2)+parseInt(point3)+parseInt(point4)+parseInt(point5)+parseInt(point6)+parseInt(point7)+parseInt(point8);
    // alert(total);
    $('#point_total').html(total);
    $('#point-modal').html(total);

    $('#point_sum').val(total);

    if(total>=0 && total<=10){
      // alert('green');
      $('#green_zone').addClass("green");
      $('#green-detail').css("display", "block");

      $('#green_zone_modal').addClass("green");
      $('#green-detail-modal').css("display", "block");

   }else if(total>= 15 && total <=20 ){
      $('#yellow_zone').addClass("yellow");
      $('#yellow-detail').css("display", "block");

      $('#yellow_zone_modal').addClass("yellow");
      $('#yellow-detail-modal').css("display", "block");


   }else if(total>25){
    $('#red_zone').addClass('red');
    $('#red-detail').css("display", "block");

    $('#red_zone_modal').addClass('red');
    $('#red-detail-modal').css("display", "block");

     // alert('red');
  }
  $('#success').modal('show');
  $('#loading').css('display','none');

});



   $('#poll_save').click(function(){
      $('#loading').css('display','block');
      if ($('#name').val()==""){
         $('#alert').modal('show');
         $('#alert-txt').html('กรุณาระบุชื่อของท่าน');
         $('#name').focus();
         $('#loading').css('display', 'none');
      }else{
       
      

      
      $('#green_zone').removeClass("green");
      $('#yellow_zone').removeClass("yellow");
      $('#red_zone').removeClass("red");
      $('#red-detail').css("display", "none");
      $('#yellow-detail').css("display", "none");
      $('#green-detail').css("display", "none");

      $('#green_zone_modal').removeClass("green");
      $('#yellow_zone_modal').removeClass("yellow");
      $('#red_zone_modal').removeClass("red");
     // $('#red-detail-modal').css("display", "none");
     // $('#yellow-detail-modal').css("display", "none");
     // $('#green-detail-modal').css("display", "none");


     var point0=$('#point0').val();
     var point1=$('#point1').val();
     var point2=$('#point2').val();
     var point3=$('#point3').val();
     var point4=$('#point4').val();
     var point5=$('#point5').val();
     var point6=$('#point6').val();
     var point7=$('#point7').val();
     var point8=$('#point8').val();
     var total=parseInt(point0)+parseInt(point1)+parseInt(point2)+parseInt(point3)+parseInt(point4)+parseInt(point5)+parseInt(point6)+parseInt(point7)+parseInt(point8);
    // alert(total);
    $('#point_total').html(total);
    $('#point-modal').html(total);

    $('#point_sum').val(total);
      // alert(total);
//     $.ajax({
//       url:"<?php echo base_url();?>Welcome/save_poll",
//       type:"post",
// data:new FormData($('#submit')[0]), //this is formData
// processData:false,
// contentType:false,
// cache:false,
// async:false,
// success: function(data){
//    console.log(data);
//    $('#poll_save').attr('disabled','disabled');
//    $('#success').modal('show');
//    $('#loading').css('display','none');
// }
// });
  $('#submit').submit();
}


if(total>=0 && total<=10){
      // alert('green');
      $('#green_zone').addClass("green");
      $('#green-detail').css("display", "block");

      $('#green_zone_modal').addClass("green");
      $('#green-detail-modal').css("display", "block");

   }else if(total>= 15 && total <=20 ){
      $('#yellow_zone').addClass("yellow");
      $('#yellow-detail').css("display", "block");


      $('#yellow_zone_modal').addClass("yellow");
      $('#yellow-detail-modal').css("display", "block");

   }else if(total>25){
    $('#red_zone').addClass('red');
    $('#red-detail').css("display", "block");

    $('#red_zone_modal').addClass('red');
    $('#red-detail-modal').css("display", "block");

     // alert('red');
  }
});

   $( "input[name='answer0']" ).change(function(){
      var point0=$(this).data('point');
      $('#point0').val(point0);

   });

   $( "input[name='answer1']" ).change(function(){
      var point1=$(this).data('point');
      $('#point1').val(point1);
   });

   $( "input[name='answer2']" ).change(function(){
      var point2=$(this).data('point');
      $('#point2').val(point2);
   });

   $( "input[name='answer3']" ).change(function(){
      var point3=$(this).data('point');
      $('#point3').val(point3);
   });

   $( "input[name='answer4']" ).change(function(){
      var point4=$(this).data('point');
      $('#point4').val(point4);
   });
   $( "input[name='answer5']" ).change(function(){
      var point5=$(this).data('point');
      $('#point5').val(point5);
   });
   $( "input[name='answer6']" ).change(function(){
      var point6=$(this).data('point');
      $('#point6').val(point6);
   });
   $( "input[name='answer7']" ).change(function(){
      var point7=$(this).data('point');
      $('#point7').val(point7);
   });
   $( "input[name='answer8']" ).change(function(){
      var point8=$(this).data('point');
      $('#point8').val(point8);
   });



   $('#province').on('change', function () {

      $('#loading').css('display', 'block');
      var province = $(this).val();
      if (province == '') {
         $('#amphure').prop('disabled', true);
      }
      else {
         $('#amphure').prop('disabled', false);
         $.ajax({
            url: "<?php echo base_url()?>Welcome/get_amphur",
            type: "POST",
            data: {'province': province},
            dataType: 'json',
            success: function (data) {
               $('#amphure').html(data);
               $('#loading').css('display', 'none');
            },
            error: function () {
               alert('โหลดไม่สำเร็จ');
            }
         });
      }
   });



</script>