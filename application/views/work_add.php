<?php echo form_open_multipart(base_url('backend/Ck_upload/upload_ck'), array('autocomplete'=>"off"));?>
<section style="padding: 5px;">
	<div>
		<textarea id="ckeditor" required></textarea>
	</div>

    <div>
        <textarea id="ckeditor2" required></textarea>
    </div>


	<div>
		<button type="submit">submit</button>
	</div>
</section>
<?php echo form_close();?>

<?php
    ############################################################################################################
    ## CK
    ############################################################################################################
?>
<script src="<?=base_url();?>assets_b/js/ckeditor/ckeditor.js"></script>

<script>
    CKEDITOR.replace( 'ckeditor', {
        extraPlugins : 'image2,codesnippet,uploadimage',

        codeSnippet_theme: 'monokai_sublime',
        height: 250,
        enterMode: CKEDITOR.ENTER_BR,
        filebrowserImageUploadUrl : '<?=base_url();?>backend/Ck_upload/upload_ck?type=image&path=work'
    });

    CKEDITOR.replace( 'ckeditor2', {
        extraPlugins : 'image2,codesnippet,uploadimage',

        codeSnippet_theme: 'monokai_sublime',
        height: 250,
        enterMode: CKEDITOR.ENTER_BR,
        filebrowserImageUploadUrl : '<?=base_url();?>backend/Ck_upload/upload_ck?type=image&path=work'
    });
</script>
