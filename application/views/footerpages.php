﻿<section class="ulockd-footer ulockd-pdng0">
  <div class="container footer-padding">
   <div class="row">
    <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3">
     <div class="ulockd-footer-widget">
      <h5 class="text-uppercase"><?=$this->lang->line('samitivej_locate_title');?></h5>
      <p class="ulockd-ftr-text tc-darkgold"><?=$this->lang->line('samitivej_locate_text');?></p>
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3882.0877370230014!2d100.9738343143078!3d13.344818210190631!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311d4a83e27c290f%3A0xbc01fd1910902d9d!2z4Liq4Lih4Li04LiV4Li04LmA4Lin4LiK4LiK4Lil4Lia4Li44Lij4Li1!5e0!3m2!1sth!2sth!4v1563176416514!5m2!1sth!2sth" width="250" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
   </div>

</div>
<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3">
  <div class="ulockd-footer-widget">
   <h5 class="text-uppercase"><?=$this->lang->line('samitivej_Our_Services');?></h5>
   <ul class="list-unstyled">
    <li><a href="#" class="tc-darkgold"><?=$this->lang->line('samitivej_About_Us');?></a> </li>
    <li><a href="<?php echo base_url();?>frontend/Home/Service" class="tc-darkgold"><?=$this->lang->line('samitivej_Service_Center');?></a> </li>
    <li><a href="<?php echo base_url();?>frontend/Home/Promotion" class="tc-darkgold"><?=$this->lang->line('samitivej_Package_Promotion');?></a> </li>
    <li><a href="https://express-apps.com/postview/selfregis_chonburi/Welcome" class="tc-darkgold"><?=$this->lang->line('samitivej_Find_a_Doctor');?></a> </li>
    <li><a href="https://express-apps.com/postview/selfregis_chonburi/Welcome" class="tc-darkgold"><?=$this->lang->line('samitivej_Request_an_Appointment');?></a> </li>
    <li><a href="#" class="tc-darkgold"><?=$this->lang->line('samitivej_Join_Us');?></a> </li>
 </ul>
</div>
<div class="ulockd-footer-widget">
   <h5 class="text-uppercase"><?=$this->lang->line('samitivej_Contact_Us');?></h5>
   <ul class="list-unstyled">
    <li><a href="tel:033038888" class="tc-darkgold slide-fontS"><span class="fa fa-phone"></span> 03-303-8888</a> </li>
    <li><a href="mailto:infosch@samitivej.co.th" class="tc-darkgold slide-fontS"><span class="fa fa-envelope"></span> infosch@samitivej.co.th</a></li>
    <li><a href="<?=base_url();?>" class="tc-darkgold slide-fontS"><span class="fa fa-window-maximize"></span> www.samitivejchonburi.com</a></li>
 </ul>
</div>
<div class="ulockd-footer-qlink">
 
   <ul class="list-inline footer-font-icon ulockd-mrgn1220">
    <li><a href="<?=$get_header->url_facebook; ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
    <!-- <li><a href="<?=$get_header->url_twitter; ?>" target="_blank"><i class="fa fa-twitter"></i></a></li> -->
    <li><a href="<?=$get_header->url_youtube; ?>" target="_blank"><i class="fa fa-youtube"></i></a></li>
    <li><a href="https://instagram.com/samitivejchonburi?igshid=112be7psm602m" target="_blank"><i class="fa fa-instagram"></i></a></li>
    <li><a href="https://lin.ee/aEJviRL" target="_blank"><i></i>LINE</a></li>
 </ul>
</div>
</div>
<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3">
  <div class="ulockd-footer-widget">
   <h5 class="text-uppercase"><?=$this->lang->line('samitivej_Hospital_Locations');?></h5>
   <ul class="list-unstyled">
    <li><a href="https://www.samitivejhospitals.com/sukhumvit/" class="tc-darkgold"><?=$this->lang->line('samitivej_Locate_Sukhumvit');?></a> </li>
    <li><a href="https://www.samitivejhospitals.com/srinakarin/" class="tc-darkgold"><?=$this->lang->line('samitivej_Locate_Srinakarin');?></a> </li>
    <li><a href="https://www.samitivejhospitals.com/thonburi/" class="tc-darkgold"><?=$this->lang->line('samitivej_Locate_Thonburi');?></a> </li>
    <li><a href="https://www.samitivejchinatown.com/" class="tc-darkgold"><?=$this->lang->line('samitivej_Locate_Chinatown');?></a> </li>
    <li><a href="https://www.samitivejsriracha.com/" class="tc-darkgold"><?=$this->lang->line('samitivej_Locate_Sriracha');?></a> </li>
    <li><a href="https://www.samitivejhospitals.com/samitivej-outreach-clinics/" class="tc-darkgold"><?=$this->lang->line('samitivej_Locate_Outreach_Clinics');?></a> </li>
    <li><a href="https://www.samitivejhospitals.com/international-childrens-hospital/" class="tc-darkgold"><?=$this->lang->line('samitivej_Locate_Children');?></a> </li>
 </ul>
</div>
</div>
<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3">
  <div class="ulockd-footer-widget">
   <h5 class="text-uppercase"><?=$this->lang->line('samitivej_Working_Time_title');?></h5>
   <ul class="list-unstyled tc-darkgold">
    <li class="slide-fontS"><?=$this->lang->line('samitivej_Working_Time_01');?></li>
    <li class="slide-fontS"><?=$this->lang->line('samitivej_Working_Time_02');?></li>
    <li class="slide-fontS"><?=$this->lang->line('samitivej_Working_Time_03');?></li>
    <li class="slide-fontS"><?=$this->lang->line('samitivej_Working_Time_04');?></li>
 </ul>
</div>
</div>
</div>
</div>
<div class="ulockd-copy-right bg-footer">
   <div class="container">
    <div class="row">
     <div class="col-md-12">
      <p class="color-white text-samitivej">© 2019 SAMITIVEJ CHONBURI HOSPITAL ALL RIGHT RESERVED</p>
   </div>
</div>
</div>
</div>
</section>
<a class="scrollToHome tc-green" href="#"><i class="fa fa-home"></i></a>
