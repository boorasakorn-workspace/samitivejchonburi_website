


<!DOCTYPE html>
<html dir="ltr" lang="en">

<!-- Mirrored from unlockdesizn.com/html/health-and-beauty/be-medical/demo/index-multipage.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 16 Nov 2018 03:52:44 GMT -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php
	if(!isset($meta_author)){

	}else{
		echo '<meta name="meta_author" content="';
		echo $meta_author;
		echo '">';
	}
	if(!isset($meta_keywords)){

	}else{
		echo '<meta name="meta_keywords" content="';
		echo $meta_keywords;
		echo '">';
	}
	if(!isset($meta_description)){

	}else{
		echo '<meta name="meta_description" content="';
		echo $meta_description;
		echo '">';
	}
?>
<!-- css file -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets_f/css/bootstrap.css?v=1001">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets_f/css/style.css">
<!-- Responsive stylesheet -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets_f/css/responsive.css?v=1001">
<!-- Title -->
<!-- <title><?=$get_header->header_title; ?></title> -->
<title><?=$this->lang->line('title_name');?></title>
<!-- Favicon -->
<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" type="image/x-icon" />
<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" />

<!-- logo icon -->
    <link rel="icon" type="image/png" href="https://www.samitivejhospitals.com/th/wp-content/themes/svh/images/svh-favicons-standard.png" />
    <link rel="icon" type="image/x-icon" href="https://www.samitivejhospitals.com/th/wp-content/themes/svh/images/svh-favicons-standard-ico.ico" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="https://www.samitivejhospitals.com/th/wp-content/themes/svh/images/svh-homescreen-144px.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="https://www.samitivejhospitals.com/th/wp-content/themes/svh/images/svh-homescreen-72px.png" />
    <link rel="apple-touch-icon-precomposed" href="https://www.samitivejhospitals.com/th/wp-content/themes/svh/images/svh-homescreen-57px.png" />

    <link rel="stylesheet" id="colors" href="<?php echo base_url(); ?>assets_f/css/colors/default.css?v=1001" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets_f/css/color-switcher.css?v=1001" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets_f/css/balloon.css?v=1001" type="text/css">
</head>
<body>
<div class="wrapper">

  <div id="preloader" class="preloader">
    <div id="pre" class="preloader_container"><div class="preloader_disabler btn btn-default">Disable Preloader</div></div>
  </div>

	<?php $this->view($head_navbar); ?>

<section class="bgc-darkgold1">
	<div class="container">
		<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center">
		          <div class="ulockd-main-title">
		            <h2 class="mt-separator">Preview sidemenu</h2>
		          </div>
		        </div>
			</div>
		<div class="row">
			<div class="col-md-4 col-lg-3 ulockd-pdng0 desktop">
				<div class="ulockd-faq-content">
					<?php
						$this->view('frontend/src/sidemenu/sidemenu');
					?>
						<p class="f-size1"><span class="icon_table text-thm2"></span> Testing sidemenu</p>
	                </div>
					
		        </div>
			
		</div>
	</div>
</section>


<!-- Footer Scripts -->


<!-- external javascripts -->
<!-- Wrapper End -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/jquery-1.12.4.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/bootstrap.min.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/bootsnav.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/parallax.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/scrollto.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/jquery.counterup.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/gallery.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/wow.min.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/slider.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/video-player.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/jquery.barfiller.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/timepicker.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/tweetie.js?v=1001"></script>
<!-- Custom script for all pages --> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/script.js?v=1001"></script>

 <script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/videohover.js?v=1001"></script>
			
<script type="text/javascript">

  $(document).ready(function() {
  	console.log('Ready');
	    $('#myModal').modal('show');
	  	$('#myModal').on("hidden.bs.modal", function(){
		    console.log("close");
		    $('[data-toggle="popover"]').popover('show');
	        setTimeout(function () {
	        $('[data-toggle="popover"]').popover('hide');
	        }, 1000);
	  });
  });

</script>  

</body>

</html>

