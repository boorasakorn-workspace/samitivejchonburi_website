<section class="style3 bgc-snowshade">
	<div class="container">
		<div class="row">	        
				<?php
					if(isset($bloghealthy)){
				?>
					<div class="course-box wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 300ms; animation-name: fadeInUp;">
						<div class="col-md-6">
				<?php
						$this->view('frontend/src/component_BlogNews/blog');
				?>				
						</div>				
					</div>
				<?php
					}
				?>

	          	<?php
	          		if(isset($blogcontents)){
	          	?>
		          	<div class="course-box wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 300ms; animation-name: fadeInUp;">
			          	<div class="col-md-6">
          		<?php 
						$this->view('frontend/src/component_BlogNews/news');
				?>		
						</div>				
					</div>
				<?php
					}
				?>

			</div>
		</div>
	</div>
</section>