<section class="bgc-darkgold1" >
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 text-center">
				<div class="ulockd-main-title">
					<h2 class="mt-separator"><?=$this->lang->line('samitivej_News_Text');?> <span class="tcolor-green">&</span> <?=$this->lang->line('samitivej_Activity_Text');?></h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-lg-3 ulockd-pdng0 desktop">
				<div class="ulockd-faq-content">
					<?php
						$this->view('frontend/src/sidemenu/sidemenu');
					?>

	                </div>
					
		        </div>
							<div class="col-md-8 col-lg-9">
								<?php /*
								<div class="row">
									<div class="col-md-12 ulockd-mrgn1210">
										<h2><?php echo $query->new_activity_name;?></h2>
										<!-- <div class="ulockd-project-sm-thumb">
											<img class="img-responsive img-whp" src="<?php echo base_url(); ?>gallery/news/big_imgs/<?php echo $query->new_activity_big_img;?>" alt="<?php echo $query->new_activity_big_img;?>">
										</div> -->
									</div>
								</div> */?>
								<div class="row">
									<div class="col-md-12 ulockd-mrgn1210">
										<div class="ulockd-pd-content">
											<div class="ulockd-bp-date">
												<ul class="list-inline course-feature">
													<?php if(isset($query->cwhen) && $query->cwhen != ''){ ?>
													<li><a href="#"><i class="fa fa-calendar text-thm2"></i> <?php echo $query->cwhen;?></a></li>
													<?php } 
														if(isset($query->acc_firstname) && $query->acc_firstname != ''){
													?>
													<li><a href="#"><i class="fa fa-user text-thm2"></i> By: <?php echo $query->acc_firstname;?></a></li>
													<?php }?>
									<!-- 
									<li><a href="#"><i class="fa fa-calendar text-thm2"></i> 28 April</a></li>
									<li><a href="#"><i class="fa fa-user text-thm2"></i> By: Admin</a></li>
									<li><a href="#"><i class="fa fa-server text-thm2"></i> Industry</a></li> 
								-->
							</ul>
						</div>
						<!-- <h3>Blog Image Post Example </h3> -->
						<p class="project-dp-one" style='word-break: break-all; word-wrap: break-word;'><?php echo html_entity_decode($query->description);?></p>
							<!-- 
							<p class="project-dp-two">toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?</p> 
						-->
							<?php 
								if(isset($query->news_video) && $query->news_video != ""){
									echo "<hr>";
									if (strpos($query->news_video, 'www.youtube.com') !== false) {
										$yt_link = explode("watch?v=",$query->news_video); 
							?>
										<iframe width="100%" height="320" src="https://www.youtube.com/embed/<?=$yt_link[1];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							<?php
									}elseif (strpos($query->news_video, 'youtu.be') !== false) {
										$yt_link = explode("youtu.be/",$query->news_video); 
							?>
										<iframe width="100%" height="320" src="https://www.youtube.com/embed/<?=$yt_link[1];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							<?php
									}
									else{
							?>
										<video style="height:320px; width:100%;" src="<?=base_url('gallery/doctor/videos/').$query->news_video;?>" controls></video>
							<?php
									}
								}
							?>
					</div>
				</div>
					<!-- 
					<div class="col-md-12 ulockd-mrgn1210">
						<div class="ulockd-bpd-thumb">
							<img class="img-responsive img-whp" src="<?php echo base_url(); ?>assets_f/images/blog/blog-details2.jpg" alt="blog-details2.jpg">
						</div>
						<div class="row">
							<div class="col-md-6">
								<img class="img-responsive img-whp" src="<?php echo base_url(); ?>assets_f/images/blog/blog-details4.jpg" alt="blog-details4.jpg">
							</div>
							<div class="col-md-6">
								<img class="img-responsive img-whp" src="<?php echo base_url(); ?>assets_f/images/blog/blog-details5.jpg" alt="blog-details5.jpg">
							</div>
						</div><br>
						<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
						<blockquote class="ulockd-mrgn1220">
						    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla accusantium omnis, debitis veniam, at fugiat numquam iusto eos alias asperiores voluptate, explicabo doloremque eveniet mollitia non quisquam dolores fugit magni magnam, veritatis pariatur! Ut, temporibus.</p>
						    <footer>Vice President <cite title="Source Title"> Muhibbur Rashid</cite></footer>
						</blockquote>
					</div> 
				-->
			</div>
		</div>
	</div>
</div>
</section>