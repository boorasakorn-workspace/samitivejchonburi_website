
<link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">

<section class="ulockd-about bgc-whitef0">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="about-box">
            <!-- <h1 class="title-bottom ulockd-mrgn620 ulockd-mrgn120">Welcome To <span class="text-cdarkg">Samitivej Chonburi hospital</span></h1><br> -->
            <div class="ulockd-main-title text-center">
                <h2 class="mt-separator hidden-xs"><?=$this->lang->line('samitivej_Package_Text');?> <span class="tcolor-green">&</span> <?=$this->lang->line('samitivej_Promotion_Text');?></h2>
                <h3 class="mt-separator visible-xs"><?=$this->lang->line('samitivej_Package_Text');?> <span class="tcolor-green">&</span> <?=$this->lang->line('samitivej_Promotion_Text');?></h3>
            </div>
            <div class="text-center" style="margin-top : -15px;margin-bottom: 40px;">
                <p><a class="btn btn-lg ulockd-btn-thm2 hidden-xs" href="<?php echo base_url();?>frontend/Home/Promotion"><?=$this->lang->line('samitivej_See_All');?></a><a class="btn btn-xs ulockd-btn-thm2 visible-xs" href="<?php echo base_url();?>frontend/Home/Promotion"><?=$this->lang->line('samitivej_See_All');?></a></p>
            </div>
            <div class="row" style="display: flex;">
             <?php
             $i=0; 
             foreach ($get_package as $result): ?>
              <?php 
              $StartDate = date("d/m/Y", strtotime($result->package_startdate));
              $EndDate = date("d/m/Y", strtotime($result->package_enddate));
              $i++;
              if($i<3){
                echo '<div class="col-xxs-6 col-xs-6 col-sm-6 col-md-3 hvr-float-shadow">';
              }
              else{
                echo '<div class="hidden-xs col-xxs-6 col-xs-6 col-sm-6 col-md-3 col-lg-3 hvr-float-shadow">';
              } ?>
                    <div class="pricing_table ">
                        <div class="pricing_table_header ulockd-bghthm bgc-package">
                            <h1 style="color:#fff;" class="title font_PackageTitle"><?php echo mb_substr($result->package_name,0,30,'UTF-8');?></h1>
                            <img class="img-responsive hidden-xs" style="height:160px; width:100%;" src="<?php echo base_url(); ?>gallery/package/small_imgs/<?php echo $result->package_small_img; ?>" alt="<?php echo $result->package_name; ?>">
                            <img class="img-responsive visible-xs" style="height:160px; width:100%;" src="<?php echo base_url(); ?>gallery/package/small_imgs/<?php echo $result->package_small_img; ?>" alt="<?php echo $result->package_name; ?>">
                            <span style="color:#fff;font-size:16px;;" class="price-value"><span style="color:gray;" class="month visible-xs"><?=$this->lang->line('samitivej_Start_Price');?></span>฿ <?php echo $result->package_unitprice;?>
                              <a class="visible-xs" href="<?php echo base_url();?>frontend/Home/PromotionDetails/<?php echo $result->id;?>"><?=$this->lang->line('samitivej_More_Details');?><i class="fa fa-arrow-circle-o-right"></i></a>
                                <span style="color:#fff;" class="month hidden-xs"><?=$this->lang->line('samitivej_Start_Price');?></span>
                            </span>
                            <span style="color: #000;;"><?=$StartDate.' - '.$EndDate;?></span>
                        </div>
                        <div class="pricing-content hidden-xs">
                            <ul>
                                <li><?=$this->lang->line('samitivej_Price_Desc');?></li>
                                <li><?=$this->lang->line('samitivej_Price_Desc_2');?></li>
                            </ul>
                            <div class="pricing_table_signup">
                                <a href="<?php echo base_url();?>frontend/Home/PromotionDetails/<?php echo $result->id;?>"><?=$this->lang->line('samitivej_More_Details');?><i class="fa fa-arrow-circle-o-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
    </div>
</div>
</div>
</section>