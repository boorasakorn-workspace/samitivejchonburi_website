<section class="bgc-darkgold1">
	<div class="container">
		<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center">
		          <div class="ulockd-main-title">
		            <h2 class="mt-separator"><?=$query->medical_center_name; ?></h2>
		          </div>
		        </div>
			</div>
		<div class="row">
			<div class="col-md-4 col-lg-3 ulockd-pdng0 desktop">
				<div class="ulockd-faq-content">
					<?php
						$this->view('frontend/src/sidemenu/sidemenu');
					?>

	                </div>
					
		        </div>
			<div class="col-md-8 col-lg-9">
				<?php /*
				<div class="row">
					<div class="col-md-12 ulockd-mrgn1210">
						<!-- <h2><?=$query->medical_center_contents; ?></h2> -->
						<div class="col-md-12 ulockd-mrgn1210">
							<?=html_entity_decode($query->medical_center_details); ?>
						</div>
					</div>
				</div> */ ?>
				<div class="row">					
					<div class="col-md-12 ulockd-mrgn1210">
						<?=html_entity_decode($query->medical_center_contents); ?>
					</div> 
				</div>
				<div class="row">
					<?php if(isset($doctor_related) && count($doctor_related) > 0){
						?>
						<h4 style="color:black;text-align:center;"><?=$this->lang->line('samitivej_Related_Doctor');?></h3>
						<?php foreach ($doctor_related as $doctor_related): 
							$pname = $doctor_related->pname_th;
							$firstname = $doctor_related->firstname_th;
							$lastname = $doctor_related->lastname_th;
							$full = $pname.' '.$firstname.' '.$lastname;
						?>
						<center>
						<div class="col-xxs-10 col-xs-6 col-sm-4 col-md-3">
							<div class="team-one text-center">
					            <div class="team-thumb">
					            	<a href="<?php echo base_url();?>frontend/Home/DoctorDetails/<?php echo $doctor_related->id;?>">
					                <img class="img-responsive img-whp" style="width:220px;height:240px;" src="<?php echo base_url(); ?>gallery/doctor/big_imgs/<?php echo $doctor_related->doctor_imgs; ?>" alt="<?php echo $full;?>">
					            	</a>
					            </div>
							</div>
							<?=$full;?>
						</div>
						</center>
						<?php endforeach; ?>
					<?php }
						?>
				</div>
			</div>
		</div>
	</div>
</section>