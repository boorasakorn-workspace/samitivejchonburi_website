<section class="our-team bgc-snowshade2">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3 text-center">
        <div class="ulockd-main-title">
          <h2 class="mt-separator hidden-xs"><?= $this->lang->line('samitivej_Our_Doctor'); ?></h2>
          <h3 class="mt-separator visible-xs"><?= $this->lang->line('samitivej_Our_Doctor'); ?></h3>
          <!-- <a class="btn btn-md ulockd-btn-thm2 hidden-xs" href="http://express-apps.com/postview/selfregis_chonburi/"><?= $this->lang->line('samitivej_See_All'); ?></a> -->
          <!-- <a class="btn btn-xs ulockd-btn-thm2 visible-xs" href="http://express-apps.com/postview/selfregis_chonburi/"><?= $this->lang->line('samitivej_See_All'); ?></a> -->

          <a class="btn btn-md ulockd-btn-thm2 hidden-xs" href="<?php echo base_url(); ?>frontend/Home/OurDoctors"><?= $this->lang->line('samitivej_See_All'); ?></a>
          <a class="btn btn-xs ulockd-btn-thm2 visible-xs" href="<?php echo base_url(); ?>frontend/Home/OurDoctors"><?= $this->lang->line('samitivej_See_All'); ?></a>
        </div>
      </div>
    </div>
    <div class="row">
      <?php $i = 0;
      foreach ($doctor as $result) :
        $pname = $result->pname_th;
        $firstname = $result->firstname_th;
        $lastname = $result->lastname_th;
        //$speciality = $result->speciality_th;
        $full = $pname . ' ' . $firstname . ' ' . $lastname;

        $doctor_med_center = '';
        $n = 0;
        $doctor_parent_medical_center_id = explode('|', $result->parent_medical_center_id);
        for ($i = 0; $n < 4 && $i < count($doctor_parent_medical_center_id); $i++) {
          foreach ($all_med_doctor as $med_center) {
            if ($doctor_parent_medical_center_id[$i] == $med_center->id) {
              $doctor_med_center .= $med_center->medical_center_name . '<br>';
              $n++;
            } elseif (!is_numeric($doctor_parent_medical_center_id[$i]) && $doctor_parent_medical_center_id[$i] != '') {
              $doctor_med_center .= $doctor_parent_medical_center_id[$i] . '<br>';
              $n++;
            }
          }
        }
        $doctor_med_center = implode(' ', array_unique(explode(' ', $doctor_med_center)));
      ?>
        <?php
        $i++;
        if ($i < 3) {
          echo '<div class="col-xxs-6 col-xs-6 col-sm-6 col-md-3">';
        } else {
          echo '<div class="hidden-xs col-xxs-6 col-xs-6 col-sm-6 col-md-3">';
        } ?>
        <div class="team-one text-center">
          <div class="team-thumb">
            <!-- <a href="https://express-apps.com/postview/selfregis_chonburi/Welcome/medical_page/<?= $result->uid; ?>"> -->
            <a href="<?php echo base_url(); ?>frontend/Home/DoctorDetails/<?php echo $result->uid; ?>">
              <img class="img-responsive img-whp hidden-xs" style="width:260px;height:320px;" src="<?php echo base_url(); ?>gallery/doctor/big_imgs/<?php echo $result->doctor_imgs; ?>" alt="<?php echo $full; ?>">
              <img class="img-responsive img-whp visible-xs" style="height:210px; width:100%;" src="<?php echo base_url(); ?>gallery/doctor/big_imgs/<?php echo $result->doctor_imgs; ?>" alt="<?php echo $full; ?>">
            </a>
            <div class="small-layer ulockd-bg">
              <ul class="list-inline font-icon-diamond">
                <!-- <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li> -->
                <!-- <li><a href="mailto:<?php echo $result->email; ?>"><i class="fa fa-envelope"></i></a></li> -->
              </ul>
            </div>
          </div>
          <div class="team-details">
            <!-- <a href="https://express-apps.com/postview/selfregis_chonburi/Welcome/medical_page/<?= $result->id; ?>"> -->
            <a href="<?php echo base_url(); ?>frontend/Home/DoctorDetails/<?php echo $result->uid; ?>">
              <p><?php echo $full; ?></p>
              <p class="member-name f-size1"><?= $doctor_med_center; ?></p>
            </a>
          </div>
        </div>
    </div>
  <?php endforeach; ?>
  </div>
  </div>
</section>