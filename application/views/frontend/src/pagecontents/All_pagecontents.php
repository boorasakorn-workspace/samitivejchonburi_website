<section  class="bgc-darkgold1 ">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 text-center">
				<div class="ulockd-main-title">
					<h2 class="mt-separator">Contents & Promotion</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<?php 
				$i = 0;
				foreach($query->result() as $rowsContents){ 
					$contentsID = $rowsContents->id;
					$subdesc = $rowsContents->contents_subdesc;
					//$description = $rowsContents->contents_description;
					$i++;
			?>
				<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4">
					<div class="project-box text-center">
						<div class="pb-thumb">
							<img class="img-responsive img-whp" src="<?php echo base_url(); ?>gallery/contents/small_imgs/<?=$rowsContents->contents_small_img; ?>" alt="<?=$rowsContents->contents_small_img; ?>">
							<div class="overlay">
								<h3><?=$rowsContents->contents_title; ?></h3>
							</div>
						</div>
						<div class="pb-details">
							<p class="f-size1"><?=$rowsContents->contents_title; ?></p>
							<p><?=strip_tags(character_limiter($subdesc,30,' ...')); ?></p>
							<!-- 
							<p><?=strip_tags(character_limiter($description,30,' ...')); ?></p> 
							-->
							<a class="btn ulockd-btn-thm2" href="<?php echo base_url();?>frontend/Home/PromotionDetails/<?php echo $contentsID;?>">อ่านต่อ</a>
						</div>
					</div>
				</div>
			<?php
					if($i%3 == 0){
						echo '</div><div class="row">';
					}
				} 
			?>
		</div>
		<div class="col-lg-12 col-md-offset-4">
			<nav aria-label="Page navigation navigation-lg">
			    <ul class="pagination pagination1">
			    	<li>
			        	<a href="#" aria-label="Previous">
		     		   		<span aria-hidden="true">ก่อนหน้า</span>
			        	</a>
					</li>
			    	<li class="active"><a href="#">1</a></li>
			    	<li><a href="#">2</a></li>
			    	<li><a href="#">3</a></li>
			    	<li><a href="#">4</a></li>
			    	<li><a href="#">5</a></li>
			    	<li>
			        	<a href="#" aria-label="Next">
			        		<span aria-hidden="true">ถัดไป</span>
			           	</a>
			    	</li>
			    </ul>
			</nav>
		</div>
	</div>
</section>