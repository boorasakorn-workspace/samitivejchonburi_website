<section  class="bgc-darkgold1">
	<div class="container">
		
		<div class="row">
			<!-- Mode PC -->
			<div class="desktop">
					<!-- menu left -->
				<div class="col-md-4 ">
					<div class="">
						<div class="ulockd-all-service text-left">
							<p class="f-size1">ข้อมูลสำหรับผู้ป่วย</p>
							<div class="list-group">
							    <a href="#" class="list-group-item">Primary Service</a>
							    <a href="#" class="list-group-item">Lab Test Service</a>
							    <a href="#" class="list-group-item">Dental Service</a>
							    <a href="#" class="list-group-item">Gynecology Service</a>
							    <a href="#" class="list-group-item">Cardiology Service</a>
							    <a href="#" class="list-group-item">Orthopedic Service</a>
							    <a href="#" class="list-group-item">Rehabilitation Service</a>
							</div>
						</div>
						<p class="f-size1">ข่าวสาร และกิจกรรม</p>
		            	<div class="bx-wrapper" style="max-width: 100%;"><div class="bx-viewport" aria-live="polite" style="width: 100%; overflow: hidden; position: relative; height: 394px;"><ul class="testimonial-carousel" style="width: auto; position: relative; transition-duration: 0s; transform: translate3d(0px, -372.2px, 0px);"><li style="float: none; list-style: none; position: relative; width: 234px; margin-bottom: 5px;" class="bx-clone" aria-hidden="true">
			            		<div class="media">
								 	<div class="media-left pull-left">
								    	<a href="#">
								      		<img class="media-object thumbnail" src="images/testimonial/2.jpg" alt="2.jpg">
								    	</a>
								  	</div>
								  	<div class="media-body">
								   		<p class="f-size1">Marloon James</p>
								    	<p class="ulockd-tcompliment"><span class="flaticon-straight-quotes"></span> I had the delight of being an outpatient in your Ambulatory Surgical Unit for foot surgery. From the moment I strolled in at the early hour of 6:00AM, until the time I was released, I experienced a staff that was cordial. <span class="flaticon-blocks-with-angled-cuts"></span></p>
								  	</div>
								</div>
		            		</li>
		            		<li style="float: none; list-style: none; position: relative; width: 234px; margin-bottom: 5px;" aria-hidden="false">
			            		<div class="media">
								 	<div class="media-left pull-left">
								    	<a href="#">
								      		<img class="media-object thumbnail" src="images/testimonial/1.jpg" alt="1.jpg">
								    	</a>
								  	</div>
								  	<div class="media-body">
								   		<p class="f-size1">Ethan Scott</p>
								    	<p class="ulockd-tcompliment"><span class="flaticon-straight-quotes"></span> I ordinarily don't compose letters, however I felt constrained to compose this one. This is in reference to the agony administration fixate situated on the fourth floor at the Comfot Home Old &amp; Health Care, Melbourne. <span class="flaticon-blocks-with-angled-cuts"></span></p>
								  	</div>
								</div>
		            		</li>
		            		<li style="float: none; list-style: none; position: relative; width: 234px; margin-bottom: 5px;" aria-hidden="true">
			            		<div class="media">
								 	<div class="media-left pull-left">
								    	<a href="#">
								      		<img class="media-object thumbnail" src="images/testimonial/2.jpg" alt="2.jpg">
								    	</a>
								  	</div>
								  	<div class="media-body">
								   		<p class="f-size1">Marloon James</p>
								    	<p class="ulockd-tcompliment"><span class="flaticon-straight-quotes"></span> I had the delight of being an outpatient in your Ambulatory Surgical Unit for foot surgery. From the moment I strolled in at the early hour of 6:00AM, until the time I was released, I experienced a staff that was cordial. <span class="flaticon-blocks-with-angled-cuts"></span></p>
								  	</div>
								</div>
		            		</li>
		            	<li style="float: none; list-style: none; position: relative; width: 234px; margin-bottom: 5px;" class="bx-clone" aria-hidden="true">
			            		<div class="media">
								 	<div class="media-left pull-left">
								    	<a href="#">
								      		<img class="media-object thumbnail" src="images/testimonial/1.jpg" alt="1.jpg">
								    	</a>
								  	</div>
								  	<div class="media-body">
								   		<p class="f-size1">Ethan Scott</p>
								    	<p class="ulockd-tcompliment"><span class="flaticon-straight-quotes"></span> I ordinarily don't compose letters, however I felt constrained to compose this one. This is in reference to the agony administration fixate situated on the fourth floor at the Comfot Home Old &amp; Health Care, Melbourne. <span class="flaticon-blocks-with-angled-cuts"></span></p>
								  	</div>
								</div>
		            		</li></ul></div><div class="bx-controls"></div></div>
					</div>				
				</div>
				<!-- appoinment doctors -->
				<div class="col-md-8  text-center">
					<div class="ulockd-main-title">
						<h2 class="mt-separator">การนัดหมายแพทย์</h2>
					</div>
				</div>
				<div class="col-md-8">
					<!-- Appointment Form Starts -->
                    <form id="appointment_form" name="appointment_form" class=" text-center" action="#" method="post" novalidate="novalidate">
                        <div class="row text-left"><p class="f-size1">- ข้อมูลผู้ป่วย -</p></div>
	                        	<div class="row">
	                        		<div class="col-md-3">
		                                <div class="form-group text-left">
		                                    <label for="form_name"><i class="fa fa-user-o"></i> ชื่อ <small>*</small></label>
		                                    <input id="firstname" name="form_name" class="form-control bgc" placeholder="ชื่อ" required="required" data-error="Name is required." type="text">
		                                    <div class="help-block with-errors"></div>
		                                </div>
		                            </div>
		                            <div class="col-md-3">
		                                <div class="form-group text-left">
		                                    <label for="form_name"> นามสกุล <small>*</small></label>
		                                    <input id="lastname" name="form_name" class="form-control bgc" placeholder="นามสกุล" required="required" data-error="Name is required." type="text">
		                                    <div class="help-block with-errors"></div>
		                                </div>
		                            </div>
		                            <div class="col-md-3">
		                            	<div class="form-group text-left">
		                            		<label for="form_name"> วันเกิด<small>*</small></label>
		                            		<input type="date" id="datetimepicker2" class="form-control bgc">
		                            	</div>
		                            </div>
		                            <div class="col-md-3">
		                                <div class="form-group text-left">
			                        	 	<label for="form_email"><i class="icon-Bisexual"></i> เพศ</label>
			                        	 	<select name="" class="form-control  bgc">
			                        	 		<option value="0" selected="selected">- เลือกเพศ -</option>
			                        	 		<option value="Male">ชาย</option>
			                        	 		<option value="Female">หญิง</option>
			                        	 	</select>
		                            	</div>
		                       		</div>
                    			</div>
							<!-- rows 2 -->
								<div class="row">
									<div class="col-md-6">
		                                <div class="form-group text-left">
		                                    <label for="form_email"><i class="fa fa-envelope-open-o"></i> Email <small>*</small></label>
		                                    <input id="form_email" name="form_email" class="form-control required email bgc" placeholder="enter an email" required="required" data-error="Email is required." type="email">
		                                    <div class="help-block with-errors"></div>
		                                </div>
	                            	</div>
	                            	<div class="col-md-6">
		                                <div class="form-group text-left">
		                                    <label for="form_phone"><i class="fa fa-phone"></i> เบอร์โทรศัพท์ <small>*</small></label>
		                                    <input id="form_phone" name="form_phone" class="form-control required bgc" placeholder="enter a phone" required="required" data-error="Phone Number is required." type="text">
		                                    <div class="help-block with-errors"></div>
		                                </div>
		                            </div>  
								</div>
	                    		<hr>
	                    		<div class="row text-left"><p class="f-size1">- เลือกแพทย์ -</p></div>
	                    		<div class="row">
	                    			<div class="col-md-6">
	                    	 			<div class="form-group text-left">
	                    	 				<label for="form_email"> สาขาเฉพาะทางของแพทย์ผู้เชี่ยวชาญ <small>*</small></label>
	                    	 				<select name="" id="input_4_11" class="form-control bgc" tabindex="13">
	                    	 					<option value="any">- ไม่ระบุความชำนาญ -</option>
	                    	 					<option value="1">เด็กและวัยรุ่น</option>
	                    	 					<option value="2">ตาและการมองเห็น</option>
	                    	 					<option value="3">สภาวะจิตใจและอารมณ์</option>
	                    	 					<option value="4">ผิวหนัง และเส้นผม</option>
	                    	 					<option value="5">สมองและระบบประสาท</option>
	                    	 					<option value="6">ตรวจและดูแลสุขภาพ</option>
	                    	 					<option value="7">ฟื้นฟูสมรรณภาพ</option>
	                    	 					<option value="8">ผ่าตัดทั่วไป และผ่าตัดเสริมความงาม</option>
	                    	 					<option value="9">สตรี ตรวจภายใน ฝากครรภ์ และทำคลอด</option>
	                    	 					<option value="10">หู คอ จมูก</option>
	                    	 					<option value="11">กระดูก ข้อ และการกีฬา</option>
	                    	 					<option value="12">อายุรกรรม</option>
	                    	 					<option value="13">มะเร็งและโรคเลือด</option>
	                    	 					<option value="14">ตับ ระบบทางเดินอาหารและทางเดินปัสสาวะ</option>
	                    	 					<option value="15">รักษาข้อ และ เข่า</option>
	                    	 					<option value="16">ต่อมไร้ท่อ ไทรอยด์ เบาหวาน</option>
	                    	 					<option value="17">ภาวะติดเชื้อ</option>
	                    	 					<option value="18">ไต</option>
	                    	 					<option value="19">ภูมิแพ้และภูมิคุ้มกัน</option>
	                    	 					<option value="20">ปอด</option>
	                    	 					<option value="21">หัวใจ</option>
	                    	 					<option value="22">ทำฟัน และ ดูแลทางช่องปาก</option>
	                    	 					<option value="23">อื่นๆ</option>
	                    	 				</select>
	                    	 				<div class="help-block with-errors"></div>
	                    	 			</div>
	                    			</div>
	                    			<div class="col-md-6">
	                    				<div class="form-group text-left">
	                    	 				<label for="form_email"> เลือกแพทย์ <small>*</small></label>
	                    	 				<select name="" id="input_4_12" class="form-control bgc" tabindex="14">
					                     		<option value="นพ. กฤษดา ไพรวัฒนานุพันธ์">นพ. กฤษดา ไพรวัฒนานุพันธ์</option>
					                   	 		<option value="รศ.นพ. กิตติ ต่อจรัส">รศ.นพ. กิตติ ต่อจรัส</option>
					                   	 		<option value="นพ. กีรติ ประเสริฐผล">นพ. กีรติ ประเสริฐผล</option>
					                   	 		<option value="นพ. จตุรงค์ ตันติมงคลสุข">นพ. จตุรงค์ ตันติมงคลสุข</option>
					                   	 		<option value="พญ. จิตติมา วงษ์โคเมท">พญ. จิตติมา วงษ์โคเมท</option>
					                   	 		<option value="พญ. จิตติมา เจริญสุข">พญ. จิตติมา เจริญสุข</option>
	                    	 				</select>
	                    	 				<div class="help-block with-errors"></div>
	                    	 			</div>
	                    			</div>
	                    		</div>
	                    		<hr>
	                    		<div class="row text-left"><p class="f-size1">- ข้อมูลนัดหมาย -</p></div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group text-left">
	                            			<label for="form_name"><i class="fa fa-file-text-o"></i> อธิบายปัญหาทางด้านสุขภาพของท่าน</label>
	                            			<textarea id="form_message" name="form_message" class="form-control bgc required" rows="6" placeholder="อธิบายปัญหาทางด้านสุขภาพของท่าน" required="required" data-error="Message is required." style="resize:vertical;"></textarea>
	                            			<div class="help-block with-errors"></div>
                        				</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group text-left">
                        					<label><i class="icon-Hospital"></i> เลือกโรงพยาบาล</label>
                        					<select name="hospital" class="form-control bgc">
                       							<option value="all"> - เลือกโรงพยาบาล - </option>
                        						<option value="1"> สมิติเวช สุขุมวิท</option>
                        						<option value="2"> สมิติเวช ศรีนครินทร์</option>
                        						<option value="3"> สมิติเวช ศรีราชา</option>
                        						<option value="4"> สมิติเวช ธนบุรี</option>
                        						<option value="6" selected="selected"> สมิติเวช ชลบุรี</option>
                        						<option value="7"> โรงพยาบาลเด็กสมิติเวช</option> 
                        					</select>
                        				</div>
									</div>
									<div class="col-md-4">
	                            		<div class="form-group text-left">
	                                		<label for="form_date"><i class="fa fa-calendar-check-o"></i> วันที่นัดหมาย</label>
	                                		<input type="text" value="" id="datetimepicker" name="form_date" class="form-control required datepicker bgc" placeholder="เลือกวันเวลานัด" required="required" data-error="Subject is required." type="text">
	                            		</div>
	                        		</div>
	                        		<div class="col-md-4">
	                            		<div class="form-group text-left">
	                                		<label for="form_date"><i class="fa fa-calendar-check-o"></i> วันที่นัดหมาย (สำรอง) <small>*</small></label>
	                                		<input type="text" value="" id="datetimepicker" name="form_date" class="form-control required datepicker bgc" placeholder="เลือกวันเวลานัด (สำรอง)" required="required" data-error="Subject is required." type="text">
	                            		</div>
	                       	 		</div> 
								</div>
                        		<div class="row">
                        			<div class="col-md-6 text-left"> 
                        				<label>คุณเคยขึ้นทะเบียนเป็นผู้ป่วย ที่โรงพยาบาลสมิติเวชแล้วหรือไม่ </label>
                        				<div class="form-group text-left">
                        					<input type="checkbox" name="" id="checkY" onclick="myFunction()">
                        					<label> ใช่</label>
                        					<input type="text" value="" id="inText2" name="inText" class="form-control required bgc" placeholder="เลขประจำตัวผู้ป่วย" style="display: none;">
                        				</div>
                        			</div>
                        			<div class="col-md-6 text-left">
                        				<label>Privacy policy</label>
                        				<div class="form-group text-left">
                        					<input type="checkbox" name="" id="">
                        					<label> Accept our privacy policy</label>
                        				</div>
                        			</div>
                       			</div>    
                       			<hr>
                         		<div class="row text-left"><p class="f-size1">- สมัครรับข่าวสารสุขภาพของเรา -</p></div>
                        		<div class="row">
                        			<div class="col-md-12">
                        				<div class="form-group text-left">
                        					<input type="checkbox" name="">
                        					<label> ใช่ ฉันต้องการรับข่าวสารสุขภาพจากโรงพยาบาลสมิติเวช</label>
                        				</div>
                        			</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group text-center">
		                            		<input id="form_botcheck" name="form_botcheck" class="form-control" value="" type="hidden">
		                            		<button type="submit" class="btn btn-lg ulockd-btn-thm2" data-loading-text="Getting Few Sec...">บันทึก</button>
		                        		</div>
									</div>
								</div>
							</form>
				</div>
			</div>
		<!-- mobile only -->
		<div class="mobile-only">
			<!-- appoinment doctors -->
			<div class="col-md-8  text-center">
					<div class="ulockd-main-title">
						<h2 class="mt-separator">การนัดหมายแพทย์</h2>
					</div>
				</div>
				<div class="col-md-8">
					<!-- Appointment Form Starts -->
                    <form id="appointment_form" name="appointment_form" class=" text-center" action="#" method="post" novalidate="novalidate">
                        <div class="row text-left"><p class="f-size1">- ข้อมูลผู้ป่วย -</p></div>
	                        	<div class="row">
	                        		<div class="col-md-3">
		                                <div class="form-group text-left">
		                                    <label for="form_name"><i class="fa fa-user-o"></i> ชื่อ <small>*</small></label>
		                                    <input id="firstname" name="form_name" class="form-control bgc" placeholder="ชื่อ" required="required" data-error="Name is required." type="text">
		                                    <div class="help-block with-errors"></div>
		                                </div>
		                            </div>
		                            <div class="col-md-3">
		                                <div class="form-group text-left">
		                                    <label for="form_name"> นามสกุล <small>*</small></label>
		                                    <input id="lastname" name="form_name" class="form-control bgc" placeholder="นามสกุล" required="required" data-error="Name is required." type="text">
		                                    <div class="help-block with-errors"></div>
		                                </div>
		                            </div>
		                            <div class="col-md-3">
		                            	<div class="form-group text-left">
		                            		<label for="form_name"> วันเกิด<small>*</small></label>
		                            		<input type="date" id="datetimepicker2" class="form-control bgc">
		                            	</div>
		                            </div>
		                            <div class="col-md-3">
		                                <div class="form-group text-left">
			                        	 	<label for="form_email"><i class="icon-Bisexual"></i> เพศ</label>
			                        	 	<select name="" class="form-control  bgc">
			                        	 		<option value="0" selected="selected">- เลือกเพศ -</option>
			                        	 		<option value="Male">ชาย</option>
			                        	 		<option value="Female">หญิง</option>
			                        	 	</select>
		                            	</div>
		                       		</div>
                    			</div>
							<!-- rows 2 -->
								<div class="row">
									<div class="col-md-6">
		                                <div class="form-group text-left">
		                                    <label for="form_email"><i class="fa fa-envelope-open-o"></i> Email <small>*</small></label>
		                                    <input id="form_email" name="form_email" class="form-control required email bgc" placeholder="enter an email" required="required" data-error="Email is required." type="email">
		                                    <div class="help-block with-errors"></div>
		                                </div>
	                            	</div>
	                            	<div class="col-md-6">
		                                <div class="form-group text-left">
		                                    <label for="form_phone"><i class="fa fa-phone"></i> เบอร์โทรศัพท์ <small>*</small></label>
		                                    <input id="form_phone" name="form_phone" class="form-control required bgc" placeholder="enter a phone" required="required" data-error="Phone Number is required." type="text">
		                                    <div class="help-block with-errors"></div>
		                                </div>
		                            </div>  
								</div>
	                    		<hr>
	                    		<div class="row text-left"><p class="f-size1">- เลือกแพทย์ -</p></div>
	                    		<div class="row">
	                    			<div class="col-md-6">
	                    	 			<div class="form-group text-left">
	                    	 				<label for="form_email"> สาขาเฉพาะทางของแพทย์ผู้เชี่ยวชาญ <small>*</small></label>
	                    	 				<select name="" id="input_4_11" class="form-control bgc" tabindex="13">
	                    	 					<option value="any">- ไม่ระบุความชำนาญ -</option>
	                    	 					<option value="1">เด็กและวัยรุ่น</option>
	                    	 					<option value="2">ตาและการมองเห็น</option>
	                    	 					<option value="3">สภาวะจิตใจและอารมณ์</option>
	                    	 					<option value="4">ผิวหนัง และเส้นผม</option>
	                    	 					<option value="5">สมองและระบบประสาท</option>
	                    	 					<option value="6">ตรวจและดูแลสุขภาพ</option>
	                    	 					<option value="7">ฟื้นฟูสมรรณภาพ</option>
	                    	 					<option value="8">ผ่าตัดทั่วไป และผ่าตัดเสริมความงาม</option>
	                    	 					<option value="9">สตรี ตรวจภายใน ฝากครรภ์ และทำคลอด</option>
	                    	 					<option value="10">หู คอ จมูก</option>
	                    	 					<option value="11">กระดูก ข้อ และการกีฬา</option>
	                    	 					<option value="12">อายุรกรรม</option>
	                    	 					<option value="13">มะเร็งและโรคเลือด</option>
	                    	 					<option value="14">ตับ ระบบทางเดินอาหารและทางเดินปัสสาวะ</option>
	                    	 					<option value="15">รักษาข้อ และ เข่า</option>
	                    	 					<option value="16">ต่อมไร้ท่อ ไทรอยด์ เบาหวาน</option>
	                    	 					<option value="17">ภาวะติดเชื้อ</option>
	                    	 					<option value="18">ไต</option>
	                    	 					<option value="19">ภูมิแพ้และภูมิคุ้มกัน</option>
	                    	 					<option value="20">ปอด</option>
	                    	 					<option value="21">หัวใจ</option>
	                    	 					<option value="22">ทำฟัน และ ดูแลทางช่องปาก</option>
	                    	 					<option value="23">อื่นๆ</option>
	                    	 				</select>
	                    	 				<div class="help-block with-errors"></div>
	                    	 			</div>
	                    			</div>
	                    			<div class="col-md-6">
	                    				<div class="form-group text-left">
	                    	 				<label for="form_email"> เลือกแพทย์ <small>*</small></label>
	                    	 				<select name="" id="input_4_12" class="form-control bgc" tabindex="14">
					                     		<option value="นพ. กฤษดา ไพรวัฒนานุพันธ์">นพ. กฤษดา ไพรวัฒนานุพันธ์</option>
					                   	 		<option value="รศ.นพ. กิตติ ต่อจรัส">รศ.นพ. กิตติ ต่อจรัส</option>
					                   	 		<option value="นพ. กีรติ ประเสริฐผล">นพ. กีรติ ประเสริฐผล</option>
					                   	 		<option value="นพ. จตุรงค์ ตันติมงคลสุข">นพ. จตุรงค์ ตันติมงคลสุข</option>
					                   	 		<option value="พญ. จิตติมา วงษ์โคเมท">พญ. จิตติมา วงษ์โคเมท</option>
					                   	 		<option value="พญ. จิตติมา เจริญสุข">พญ. จิตติมา เจริญสุข</option>
	                    	 				</select>
	                    	 				<div class="help-block with-errors"></div>
	                    	 			</div>
	                    			</div>
	                    		</div>
	                    		<hr>
	                    		<div class="row text-left"><p class="f-size1">- ข้อมูลนัดหมาย -</p></div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group text-left">
	                            			<label for="form_name"><i class="fa fa-file-text-o"></i> อธิบายปัญหาทางด้านสุขภาพของท่าน</label>
	                            			<textarea id="form_message" name="form_message" class="form-control bgc required" rows="6" placeholder="อธิบายปัญหาทางด้านสุขภาพของท่าน" required="required" data-error="Message is required." style="resize:vertical;"></textarea>
	                            			<div class="help-block with-errors"></div>
                        				</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group text-left">
                        					<label><i class="icon-Hospital"></i> เลือกโรงพยาบาล</label>
                        					<select name="hospital" class="form-control bgc">
                       							<option value="all"> - เลือกโรงพยาบาล - </option>
                        						<option value="1"> สมิติเวช สุขุมวิท</option>
                        						<option value="2"> สมิติเวช ศรีนครินทร์</option>
                        						<option value="3"> สมิติเวช ศรีราชา</option>
                        						<option value="4"> สมิติเวช ธนบุรี</option>
                        						<option value="6" selected="selected"> สมิติเวช ชลบุรี</option>
                        						<option value="7"> โรงพยาบาลเด็กสมิติเวช</option> 
                        					</select>
                        				</div>
									</div>
									<div class="col-md-4">
	                            		<div class="form-group text-left">
	                                		<label for="form_date"><i class="fa fa-calendar-check-o"></i> วันที่นัดหมาย</label>
	                                		<input type="text" value="" id="datetimepicker" name="form_date" class="form-control required datepicker bgc" placeholder="เลือกวันเวลานัด" required="required" data-error="Subject is required." type="text">
	                            		</div>
	                        		</div>
	                        		<div class="col-md-4">
	                            		<div class="form-group text-left">
	                                		<label for="form_date"><i class="fa fa-calendar-check-o"></i> วันที่นัดหมาย (สำรอง) <small>*</small></label>
	                                		<input type="text" value="" id="datetimepicker" name="form_date" class="form-control required datepicker bgc" placeholder="เลือกวันเวลานัด (สำรอง)" required="required" data-error="Subject is required." type="text">
	                            		</div>
	                       	 		</div> 
								</div>
                        		<div class="row">
                        			<div class="col-md-6 text-left"> 
                        				<label>คุณเคยขึ้นทะเบียนเป็นผู้ป่วย ที่โรงพยาบาลสมิติเวชแล้วหรือไม่ </label>
                        				<div class="form-group text-left">
                        					<input type="checkbox" name="" id="checkY" onclick="myFunction()">
                        					<label> ใช่</label>
                        					<input type="text" value="" id="inText" name="inText" class="form-control required bgc" placeholder="เลขประจำตัวผู้ป่วย" style="display: none;">
                        				</div>
                        			</div>
                        			<div class="col-md-6 text-left">
                        				<label>Privacy policy</label>
                        				<div class="form-group text-left">
                        					<input type="checkbox" name="" id="">
                        					<label> Accept our privacy policy</label>
                        				</div>
                        			</div>
                       			</div>    
                       			<hr>
                         		<div class="row text-left"><p class="f-size1">- สมัครรับข่าวสารสุขภาพของเรา -</p></div>
                        		<div class="row">
                        			<div class="col-md-12">
                        				<div class="form-group text-left">
                        					<input type="checkbox" name="">
                        					<label> ใช่ ฉันต้องการรับข่าวสารสุขภาพจากโรงพยาบาลสมิติเวช</label>
                        				</div>
                        			</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group text-center">
		                            		<input id="form_botcheck" name="form_botcheck" class="form-control" value="" type="hidden">
		                            		<button type="submit" class="btn btn-lg ulockd-btn-thm2" data-loading-text="Getting Few Sec...">บันทึก</button>
		                        		</div>
									</div>
								</div>
							</form>
				</div>
  		<!-- menu left -->
		</div>	
	</div>
</div>
</section>
<script>
	function myFunction() {
	    var checkb = document.getElementById("inText");
	    var checkb2 = document.getElementById("inText2");
	    if (checkb.style.display === "block" || checkb2.style.display === "block") {
	        checkb.style.display = "none";
	        checkb2.style.display = "none";
	        
	    } else {
	        checkb.style.display = "block";
	        checkb2.style.display = "block";
	    }
	}
</script>
