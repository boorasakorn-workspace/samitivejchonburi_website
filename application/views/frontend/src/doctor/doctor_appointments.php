<section  class="bgc-darkgold1">
	<div class="container">
		<div class="row">
			<div class="col-md-12  text-center">
				<div class="ulockd-main-title">
					<h2 class="mt-separator"><?=$this->lang->line('samitivej_Request_an_Appointment');?></h2>
				</div>
			</div>
		</div>
		<div class="row">
			<!-- Mode PC -->
			<!-- menu left -->
			<div class="col-md-4 col-lg-3 ulockd-pdng0 desktop">
				<div class="ulockd-faq-content">
					<?php
						$this->view('frontend/src/sidemenu/sidemenu');
					?>

	                </div>
					
		        </div>				<!-- appoinment doctors -->
							<div class="col-md-8 col-lg-9">
								<!-- Appointment Form Starts -->
								<?php 
								$location_form = base_url()."backend/src/DoctorAppointment/create/";
                    //<form id="appointment_form" name="appointment_form" class=" text-center " action="#" method="post" novalidate="novalidate">
								?>
								<form class="form-horizontal" action="<?php echo base_url('frontend/Home/Full_Appointment_Email_Send');?>" method="POST" enctype="multipart/form-data">
									<input type="hidden" id="appointment_title_text" name="appointment_title_text" value="<?=$this->lang->line('samitivej_Request_an_Appointment');?>">
									<div class="row text-left"><p class="f-size1">- <?=$this->lang->line('samitivej_Patient_Info_Title');?> -</p></div>
									<div class="row">
										<div class="col-md-3">
											<div class="form-group text-left">
												<label for="form_name"><i class="fa fa-user-o"></i> <?=$this->lang->line('samitivej_Firstname');?> <small>*</small></label>
												<input id="firstname" name="firstname" class="form-control bgc" placeholder="<?=$this->lang->line('samitivej_Firstname');?>" data-error="Name is required." type="text">
												<div class="help-block with-errors"></div>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group text-left">
												<label for="form_name"> <?=$this->lang->line('samitivej_Lastname');?> <small>*</small></label>
												<input id="lastname" name="lastname" class="form-control bgc" placeholder="<?=$this->lang->line('samitivej_Lastname');?>" data-error="Name is required." type="text">
												<div class="help-block with-errors"></div>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group text-left">
												<label for="form_name"> <?=$this->lang->line('samitivej_Birthday');?><small>*</small></label>
												<input type="date" id="birthdate"  name="birthdate" class="form-control bgc" id="birthdate">
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group text-left">
												<label for="gender"><i class="icon-Bisexual"></i> <?=$this->lang->line('samitivej_Doctor_Gender');?></label>
												<select id="gender" name="gender" class="form-control  bgc">
													<option value="" selected="selected">- <?=$this->lang->line('samitivej_Doctor_Select_Gender');?> -</option>
													<option value="Male"><?=$this->lang->line('samitivej_Doctor_Gender_Male');?></option>
													<option value="Female"><?=$this->lang->line('samitivej_Doctor_Gender_Female');?></option>
												</select>
											</div>
										</div>
									</div>
									<!-- rows 2 -->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group text-left">
												<label for="form_email"><i class="fa fa-envelope-open-o"></i> <?=$this->lang->line('samitivej_Email');?> <small>*</small></label>
												<input id="form_email" name="form_email" class="form-control required email bgc" placeholder="enter an email"  data-error="Email is required." type="email">
												<div class="help-block with-errors"></div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group text-left">
												<label for="form_phone"><i class="fa fa-phone"></i> <?=$this->lang->line('samitivej_Tel_no');?> <small>*</small></label>
												<input id="form_phone" name="form_phone" class="form-control required bgc" placeholder="enter a phone"  data-error="Phone Number is required." type="text">
												<div class="help-block with-errors"></div>
											</div>
										</div>  
									</div>
									<hr>
									<!-- <div class="row text-left"><p class="f-size1">- เลือกแพทย์ -</p></div> -->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group text-left">
												<label for="appointment_date"><i class="fa fa-calendar-check-o"></i> <?=$this->lang->line('samitivej_Appointment_Date_Select');?></label>
												<input type="text" id="appointment_date" name="appointment_date" class="form-control required datepicker bgc" placeholder="<?=$this->lang->line('samitivej_Appointment_Date_Select');?>"  data-error="Subject is required." type="text">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group text-left">
												<label for="appointment_sdate"><i class="fa fa-calendar-check-o"></i> <?=$this->lang->line('samitivej_SecondAppointment_Date_Select');?> <small>*</small></label>
												<input type="text" id="appointment_sdate" name="appointment_sdate" class="form-control required datepicker bgc" placeholder="<?=$this->lang->line('samitivej_SecondAppointment_Date_Select');?>"  data-error="Subject is required." type="text">
											</div>
										</div> 
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group text-left">
												<label for="form_name"> <?=$this->lang->line('samitivej_Service_Center');?></label>
												<select name="medical_center" class="form-control bgc" id="medical_center_txt">
													<option value="all" selected="selected"> <?=$this->lang->line('samitivej_All_Service');?></option>
													<?php foreach ($all_med_doctor as $result) :?>
														<option value="<?php echo $result->id;?>"><?php echo $result->medical_center_name;?></option>											
													<?php endforeach;?>
												</select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group text-left">
												<label for="form_name"><?=$this->lang->line('samitivej_Speciality');?></label>
												<select name="specialty" class="form-control bgc" id="specialty_txt">
													<option value="all" selected="selected"> <?=$this->lang->line('samitivej_All_Speciality');?></option>
													<?php foreach ($all_specialty as $result) :?>
														<option value="<?php echo $result->id;?>"><?php echo $result->specialty_name;?></option>											
													<?php endforeach;?>
												</select>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-md-6">
											<div class="form-group text-left">
												<label for="form_name"><?=$this->lang->line('samitivej_Sub_Speciality');?></label>
												<select name="sub_specialty" class="form-control bgc" id="sub_specialty_txt">
													<option value="all" selected="selected"> <?=$this->lang->line('samitivej_All_Speciality');?></option>
													<?php foreach ($all_sub_specialty as $result) :?>
														<option value="<?php echo $result->id;?>"><?php echo $result->sub_specialty_name;?></option>											
													<?php endforeach;?>
												</select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group text-left">
												<label for="form_email"> <?=$this->lang->line('samitivej_Select_Doctor');?> <small>*</small></label>
												<select name="doctor_medical_id" id="doctor_medical_id" class="form-control bgc" tabindex="14">
													<option value="">- <?=$this->lang->line('samitivej_All_Doctor_Select');?> -</option>
													<?php foreach ($all_doctor as $result) :?>
													<?php 
														$pname = $result->pname_th;
														$firstname = $result->firstname_th;
														$lastname = $result->lastname_th;
														$speciality = $result->speciality_th;
														$full = $pname.' '.$firstname.' '.$lastname;
														$education = $result->education_th;
														$doc_id = $result->id;
														?>
														<option value="<?php echo $doc_id;?>"><?php echo $full;?></option>
													<?php endforeach;?>
												</select>
												<div class="help-block with-errors"></div>
											</div>
										</div>
									</div>
									<hr>
									<div class="row text-left"><p class="f-size1">- <?=$this->lang->line('samitivej_Appointment_Info');?> -</p></div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group text-left">
												<label for="form_name"><i class="fa fa-file-text-o"></i> <?=$this->lang->line('samitivej_Describe_medical_problem');?></label>
												<textarea id="description" name="description" class="form-control bgc required" rows="6" placeholder="<?=$this->lang->line('samitivej_Describe_medical_problem');?>" data-error="Message is required." style="resize:vertical;"></textarea>
												<div class="help-block with-errors"></div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6 text-left"> 
											<label><?=$this->lang->line('samitivej_Be_Patient_Before');?> </label>
											<div class="form-group text-left">
												<input type="checkbox" name="" id="status" onclick="myFunction()">
												<label> <?=$this->lang->line('samitivej_Yes');?></label>
												<input type="text" value="" id="inText2" name="inText" class="form-control required bgc" placeholder="เลขประจำตัวผู้ป่วย" style="display: none;">
											</div>
										</div>
										<div class="col-md-6 text-left">
											<label><?=$this->lang->line('samitivej_Privacy_Policy');?></label>
											<div class="form-group text-left">
												<input type="checkbox" name="" id="">
												<label> <?=$this->lang->line('Accept_our_privacy_policy');?></label>
											</div>
										</div>
									</div>    
									<hr>
									<div class="row text-left"><p class="f-size1">- <?=$this->lang->line('Subscribe_News_Letter');?> -</p></div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group text-left">
												<input type="checkbox" name="sub_status" input="sub_status">
												<label> <?=$this->lang->line('Confirmed_Subscribe');?></label>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group text-center">
												<input id="form_botcheck" name="form_botcheck" class="form-control" value="" type="hidden">
												<button type="submit" value="submit" class="btn btn-lg ulockd-btn-thm2" id="btn-appointment" data-loading-text="Getting Few Sec..."><?=$this->lang->line('samitivej_Request_an_Appointment');?></button>
											</div>
										</div>
									</div>
								</form>
							</div>

						</div>
					</div>
				</section>



				<div class="modal" tabindex="-1" role="dialog" id="modal-alert">
					<div class="modal-dialog" role="document">
						<div class="modal-content">

							<div class="modal-body text-center">
								<div id="txt-alert"></div>
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							</div>




						</div>
					</div>
				</div>


				<script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
				<script>
					function myFunction() {
						var checkb = document.getElementById("inText");
						var checkb2 = document.getElementById("inText2");
						if (checkb.style.display === "block" || checkb2.style.display === "block") {
							checkb.style.display = "none";
							checkb2.style.display = "none";

						} else {
							checkb.style.display = "block";
							checkb2.style.display = "block";
						}
					}

					$( document ).ready(function() {
						$('#btn-appointment').click(function(){
							if($('#firstname').val()==""){

								$('#modal-alert').modal('show');
								$('#txt-alert').html('กรุณาระบุชื่อของคุณ');
								$('#firstname').focus();
								return false;
								
							}else if($('#lastname').val()==""){
								$('#modal-alert').modal('show');
								$('#txt-alert').html('กรุณาระบุชื่อสกุลของคุณ');
								$('#lastname').focus();
								return false;
								
							}else if($('#birthdate').val()==""){
								$('#modal-alert').modal('show');
								$('#txt-alert').html('กรุณาระบุวันเกิดของคุณ');
								$('#birthdate').focus();
								return false;
								
							}else if($('#gender').val()==""){
								$('#modal-alert').modal('show');
								$('#txt-alert').html('กรุณาระบุเพศของคุณ');
								$('#gender').focus();
								return false;
								
							}else if($('#form_email').val()==""){
								$('#modal-alert').modal('show');
								$('#txt-alert').html('กรุณาระบุอีเมล');
								$('#form_email').focus();
								return false;


							}else if($('#form_phone').val==""){
								$('#modal-alert').modal('show');
								$('#txt-alert').html('กรุณาระบุเบอร์โทรศัพท์');
								$('#form_phone').focus();
								return false;
								

							}else if($('#appointment_date').val()==""){
								$('#modal-alert').modal('show');
								$('#txt-alert').html('กรุณาระบุวันที่นัด');
								$('#appointment_date').focus();
								return false;
								
							}else if($('#appointment_sdate').val()==""){
								$('#modal-alert').modal('show');
								$('#txt-alert').html('กรุณาระบุวันที่นัดสำรอง');
								$('#appointment_sdate').focus();
								return false;
								
							}else if($('#doctor_speciality_id').val()==""){
								$('#modal-alert').modal('show');
								$('#txt-alert').html('กรุณาระบุความเชี่ยวชาญแพทย์');
								$('#doctor_speciality_id').focus();
								return false;

							}else if($('#doctor_medical_id').val()==""){
								$('#modal-alert').modal('show');
								$('#txt-alert').html('กรุณาเลือกแพทย์');
								$('#doctor_medical_id').focus();
								return false;

							}
							

						});

					});
				</script>
