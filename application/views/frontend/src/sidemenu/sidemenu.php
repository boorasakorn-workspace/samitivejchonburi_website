<?php if(isset($get_doctor) and !empty($get_doctor)){ 
	$pname = $get_doctor->pname_th;
	$firstname = $get_doctor->firstname_th;
	$lastname = $get_doctor->lastname_th;
	//$speciality = $get_doctor->speciality_th;
	$full = $pname.' '.$firstname.' '.$lastname;


	$doctor_med_center = '';
	$n = 0;
	$doctor_parent_medical_center_id = explode('|',$get_doctor->parent_medical_center_id);
	for($i=0;$n<1 && $i<count($doctor_parent_medical_center_id);$i++){
		foreach($all_med_doctor as $med_center){
			if($doctor_parent_medical_center_id[$i] == $med_center->id){
				$doctor_med_center .= $med_center->medical_center_name . '<br>';
				$n++ ;
			}
			elseif(!is_numeric($doctor_parent_medical_center_id[$i]) && $doctor_parent_medical_center_id[$i] != ''){
				$doctor_med_center .= $doctor_parent_medical_center_id[$i] . '<br>';
				$n++ ;
			}
		}
	}
	$doctor_med_center = implode(' ',array_unique(explode(' ', $doctor_med_center)));
	?>
<br>
<div class="row">
	<div class="col-xxs-12 col-xs-12 col-sm-12 col-md-12">
    <h3 style="color:black; font-weight: 540;">แพทย์เจ้าของบทความ</h3>
	</div>
	<div class="col-xxs-6 col-xs-6 col-sm-6 col-md-6">
	    <div class="team-one text-center">
            <div class="team-thumb">
            	<a href="<?php echo base_url();?>frontend/Home/DoctorDetails/<?php echo $get_doctor->id;?>">
                <img class="img-responsive img-whp hidden-xs" style="width:40%;" src="<?php echo base_url(); ?>gallery/doctor/big_imgs/<?php echo $get_doctor->doctor_imgs; ?>" alt="<?php echo $full;?>">
            	</a>
            </div>
		</div>
    </div>
	<div class="col-xxs-6 col-xs-6 col-sm-6 col-md-6">
		<div class="team-one">
            <div class="team-details">
              <a href="<?php echo base_url();?>frontend/Home/DoctorDetails/<?php echo $get_doctor->id;?>">
              <p class="member-name f-size1"><?php echo $full;?></p></a>
              <h6 style="color:grey; font-weight: 1000;"><?php echo $doctor_med_center;?></h6>
              <?php /*<h6 style="color:grey; font-weight: 1000;"><?php echo $speciality;?></h6>*/ ?>
              
            </div>
		</div>
    </div>
</div>
<br>
<?php } ?>
<?php
						foreach ($get_sidemenu as $sidemenu) {
							echo '<p class="f-size1"><span class="icon_menu-square_alt text-thm2"></span> '.$sidemenu->sidemenu_title.'</p>';
							$checksub = 0;
							foreach ($get_subsidemenu as $subsidemenu) {
								if($subsidemenu->parent_sidemenu_id == $sidemenu->id){
									if($checksub == 0){
										echo '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					            <div class="panel panel-default">';
					                    $checksub++;
									}
									echo '<div class="panel-heading" role="tab" id="heading">
						                    <h4 class="panel-title">
						                        <a role="button" style="color:#005f3b;"data-toggle="collapse" data-parent="#accordion" href="#collapse'.$subsidemenu->id.'" aria-expanded="false" aria-controls="collapse" class="collapsed">
						                      <i class="icon-Down-2 icon-1"></i>
						                      <i class="icon-Right-2 icon-2"></i>
						                      '.$subsidemenu->sub_sidemenu_title.'
						                        </a>
						                    </h4>
						                </div>';
						            $checksubsub = 0;
						            foreach ($get_subofsubmenu as $subofsubmenu) {
										if($subofsubmenu->parent_submenu_id == $subsidemenu->id){	            	
							            	if($checksubsub == 0){
							            		echo '<div id="collapse'.$subsidemenu->id.'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading" aria-expanded="false" style="height: 0px;">
									                            <div class="panel-body">
									                            	<div class="list-group">';
							            		$checksubsub++;
							            	}
							            	if($subofsubmenu->subofsubmenu_content != ''){
							            		echo '<a href="'.base_url().'frontend/Home/SubSubSidebarDetails/'.$subofsubmenu->id.'" class="list-group-item">'.$subofsubmenu->subofsubmenu_title.'</a>';
							            	}
							            	else if(strpos(strtoupper($subofsubmenu->subofsubmenu_url), strtoupper('frontend')) !== false){
							            		echo '<a href="'.base_url().$subofsubmenu->subofsubmenu_url.'" class="list-group-item">'.$subofsubmenu->subofsubmenu_title.'</a>';
							            	}
							            	else{
							            		echo '<a href="'.$subofsubmenu->subofsubmenu_url.'" class="list-group-item">'.$subofsubmenu->subofsubmenu_title.'</a>';
							            	}

							            }
						            }
						            if($checksubsub > 0){
						            	echo '</div></div></div>';
						            }
								}
							}
							if($checksub > 0){			
								echo '</div></div>';
							}
						}
?>