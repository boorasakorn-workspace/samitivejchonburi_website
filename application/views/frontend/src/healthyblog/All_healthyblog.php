<section class="bgc-darkgold1">
	<div class="container">
		<div class="row">
        <div class="col-md-6 col-md-offset-3 text-center">
         <div class="ulockd-main-title">
          <h2 class="mt-separator"> <?=$this->lang->line('samitivej_Healthy_Blog');?></h2>
       </div>
    </div>
 </div>
 <div class="row">
   <!-- <?php var_dump($query->result())?>  -->
   <div class="col-md-6">
      <div class="form-group">
         <form action="<?php echo base_url('frontend/Home/HealthyBlog');?>" method="post">
            <select name="search_blog_doctor" class="form-control">
               <option value="">เลือกแพทย์</option>
               <?php foreach($get_doctor as $result):?>
                  <option value="<?php echo $result->id;?>"><?php echo $result->id." ".$result->firstname_th." ".$result->lastname_th;?></option>
               <?php endforeach;?>
            </select>  
         </div>
      </div>
      <div class="col-md-6"> 
         <button class="btn btn ulockd-btn-thm2" type="submit">ค้นหาตามแพทย์</button>
      </div>
   </form>
</div>
<div class="row">
  <?php 
  $i = 0;
  foreach($query->result() as $rowsBlog){ 
    $blogID = $rowsBlog->id;
    $content = $rowsBlog->blog_contents;
    $content = str_replace ( "HIGHLIGHTS:","", $content );
    $created = explode(" ", $rowsBlog->created_at)[0];
    $created = explode("-", $created)[2].'/'.explode("-", $created)[1].'/'.explode("-", $created)[0];
    if(!empty($rowsBlog->doc_id)){
      foreach($get_doctor as $Doctor){
        if($Doctor->id == $rowsBlog->doc_id){							
          $pname = $Doctor->pname_th;
          $firstname = $Doctor->firstname_th;
          $lastname = $Doctor->lastname_th;
          $doctor_name = $pname.' '.$firstname.' '.$lastname;
       }
    }
 }
 $i++;
 ?>
 <div class="col-xxs-6 col-xs-6 col-sm-4 col-md-2" style="margin-bottom: 20px;">
    <div class="project-box text-center">
      <div class="pb-thumb">
        <img class="img-responsive img-whp" src="<?php echo base_url(); ?>gallery/blog/big_imgs/<?=$rowsBlog->blog_big_img; ?>" style="height:120px; width:280px;" alt="<?=$rowsBlog->blog_title; ?>">
     </div>
     <div class="pb-details">
        <p class="f-size1"><?=$rowsBlog->blog_title; ?></p>
        <?php
        if(!empty($rowsBlog->doc_id)){ ?>
           <p style="margin-top: -5px;margin-bottom: -5px;"><i class="fa fa-user text-thm2"></i> <?=$doctor_name;?> </p>
        <?php } else { ?>
           <p><?=strip_tags(character_limiter($content,30,' ...')); ?></p>
        <?php } ?>
							<?php /*
								if(!empty($rowsBlog->created_at)){ ?>
							<p style="margin-top: -5px;margin-bottom: 5px;"> <?=$created;?> </p>
                 <?php } */ ?>
                 <div class="btnblog">
                   <a class="btn ulockd-btn-thm2" href="<?php echo base_url();?>frontend/Home/HealthyblogDetails/<?php echo $blogID;?>"><?=$this->lang->line('samitivej_Read_More');?></a>
                </div>
             </div>
          </div>
       </div>
       <?php
       if($i%6 == 0){
         echo '</div><div class="row">';
      }
   } 
   ?>
</div>
<div class="col-lg-12 col-md-offset-4">
   <nav aria-label="Page navigation navigation-lg">
     <ul class="pagination pagination1">
       <li>
         <a href="#" aria-label="Previous">
          <span aria-hidden="true"><?=$this->lang->line('samitivej_Previous_Text');?></span>
       </a>
    </li>
    <li class="active"><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li>
      <a href="#" aria-label="Next">
        <span aria-hidden="true"><?=$this->lang->line('samitivej_Next_Text');?></span>
     </a>
  </li>
</ul>
</nav>
</div>
</div>

</section>