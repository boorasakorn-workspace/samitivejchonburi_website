<section class=" style3 bgc-snowshade">
		<div class="container">
			<div class="row">
        <div class="col-md-6 col-md-offset-3 text-center">
          <div class="ulockd-main-title">
            <h2 class="mt-separator hidden-xs"><?=$this->lang->line('samitivej_News_Text');?> <span class="tcolor-green">&</span> <?=$this->lang->line('samitivej_Activity_Text');?></h2>
            <h3 class="mt-separator visible-xs"><?=$this->lang->line('samitivej_News_Text');?> <span class="tcolor-green">&</span> <?=$this->lang->line('samitivej_Activity_Text');?></h3>
            <a class="btn btn-md ulockd-btn-thm2 hidden-xs" href="<?php echo base_url();?>frontend/Home/News"><?=$this->lang->line('samitivej_See_All');?></a>
            <a class="btn btn-xs ulockd-btn-thm2 visible-xs" href="<?php echo base_url();?>frontend/Home/News"><?=$this->lang->line('samitivej_See_All');?></a>
          </div>
        </div>
      </div>
			<div class="row">
				<div class="col-md-8">
					<div class="course-box wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 300ms; animation-name: fadeInUp;">						
						<?php 
							$i=0;
							foreach ($new_activity as $result) :
							$i++;
					        $n_desc = $result->description;
					        $n_desc = strip_tags($n_desc);
				            if($i<2){
				                echo '<div class="row">';
				            }
				            else{
				                //echo '<div class="hidden-xs row">';
				                echo '<div class="row">';
				            } ?>
							<div class=" col-sm-5 cb-thumb">
								<img class="img-responsive img-whp hidden-xs" src="<?php echo base_url(); ?>gallery/news/small_imgs/<?php echo $result->new_activity_small_img;?>" alt="<?php echo $result->new_activity_small_img;?>">
								<img class="img-responsive img-whp visible-xs" style="height:220px; width:100%;" src="<?php echo base_url(); ?>gallery/news/small_imgs/<?php echo $result->new_activity_small_img;?>" alt="<?php echo $result->new_activity_small_img;?>">
							</div>
							<div class=" col-md-7 cb-details style2" style='word-break: break-all; word-wrap: break-word;'>
								<h3><?php echo $result->new_activity_name;?></h3>
								
								<p><?php 
									/*
									echo substr($result->description, 0, 70);
									if(strlen($result->description)>70){
										echo "...";
									}
									*/
									echo character_limiter($n_desc,30);
									if($n_desc>30){
										echo "...";
									}
									?>
								</p>
								<ul class="list-inline course-feature">
									<?php if(isset($result->acc_firstname) && $result->acc_firstname != NULL){ ?>
									<li><a href="#"><i class="fa fa-user text-thm2"></i> By: <?php echo $result->acc_firstname;?></a></li>
									<?php } ?>
									<?php if(isset($result->cwhen) && $result->cwhen != NULL){ ?>
									<li><a href="#"><i class="fa fa-calendar text-thm2"></i> <?php echo $result->cwhen;?></a></li>
									<?php } ?>
									<!-- 
									<li><a href="#"><i class="fa fa-calendar text-thm2"></i>  28 April</a></li>
									<li><a href="#"><i class="fa fa-server text-thm2"></i> Industry</a></li> 
									-->
								</ul>
								<a class="btn btn-md ulockd-btn-thm2" href="<?php echo base_url();?>frontend/Home/NewsDetails/<?php echo $result->new_activity_id;?>"><?=$this->lang->line('samitivej_Read_More');?></a>


							</div>
						</div>
						<hr>
						
						
						<?php endforeach;?>
					</div>
				</div>
			</div>
		</div>
	</section>