<link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">

<section  class="bgc-darkgold1">
	<div class="container">
		<div class="row">
			<div class="col-md-12  text-center">
				<div class="ulockd-main-title">
					<h2 class="mt-separator" style="font-family: Prompt;">ติดต่อสอบถาม</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-8">
				<form id="Contact_form" name="Contact_form" class=" text-center" action="#" method="post" novalidate="novalidate">
					<div class="row text-left"><p class="f-size1" style="font-family: Prompt;">- ข้อมูลผู้ติดต่อ -</p></div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group text-left">
								<label for="form_name" style="font-family: Prompt;"><i class="fa fa-user-o"></i> ชื่อ <span style="color: #bc9b6a;">*</span></label>
								<input id="firstname" name="form_name" class="form-control bgc" placeholder="ชื่อ" required="required" data-error="Name is required." type="text">
								<div class="help-block with-errors"></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group text-left">
								<label for="form_name" style="font-family: Prompt;"> นามสกุล <span style="color: #bc9b6a;">*</span></label>
								<input id="lastname" name="form_name" class="form-control bgc" placeholder="นามสกุล" required="required" data-error="Name is required." type="text">
								<div class="help-block with-errors"></div>
							</div>
						</div>
					</div>
					<!-- rows 2 -->
					<div class="row">
						<div class="col-md-6">
							<div class="form-group text-left">
								<label for="form_email" style="font-family: Prompt;"><i class="fa fa-envelope-open-o"></i> อีเมล<span style="color: #bc9b6a;">*</span></label>
								<input id="form_email" name="form_email" class="form-control required email bgc" placeholder="enter an email" required="required" data-error="Email is required." type="email">
								<div class="help-block with-errors"></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group text-left">
								<label for="form_phone" style="font-family: Prompt;"><i class="fa fa-phone"></i> เบอร์โทรศัพท์</label>
								<input id="form_phone" name="form_phone" class="form-control required bgc" placeholder="enter a phone" required="required" data-error="Phone Number is required." type="text">
								<div class="help-block with-errors"></div>
							</div>
						</div>  
					</div>
					<hr>
					<div class="row text-left"><p class="f-size1" style="font-family: Prompt;">- กรุณาเลือกประเภทคำถาม -</p></div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group text-left">
								<label for="form_email" style="font-family: Prompt;"> เลือกคำถาม <span style="color: #bc9b6a;">*</span></label>
								<select name="" id="question" class="form-control bgc" tabindex="13" style="font-family: Prompt;">
									<option value="">กรุณาเลือกคำถาม</option>
									<option value="any">นัดหมายแพทย์</option>
									<option value="1">การเรียกเก็บเงิน</option>
									<option value="2">การประกันภัย</option>
								</select>
								<div class="help-block with-errors"></div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group text-left">
								<label for="form_name" style="font-family: Prompt;font-size: 15px;"><i class="fa fa-file-text-o"></i> กรุณาระบุคำถามของท่าน หรือ ให้ข้อคิดเห็น<span style="color: #bc9b6a;">*</span></label>
								<textarea id="form_message" name="form_message" class="form-control bgc required" rows="6" placeholder="กรุณาระบุคำถามของท่าน หรือ ให้ข้อคิดเห็น" required="required" data-error="Message is required." style="resize:vertical;"></textarea>
								<div class="help-block with-errors"></div>
							</div>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-12 text-left">
							<div style="font-family: Prompt;font-size: 15px;font-weight: bold;">นโยบายความเป็นส่วนตัว<span style="color: #bc9b6a;">*</span></div>
							<div class="form-group text-left">
								
								<div style="font-family: Prompt;font-size: 15px;"> 
									<input type="checkbox" name="" id=""> ยอมรับนโยบายความเป็นส่วนตัวของเรา </div>
							</div>

						</div>
						<div class="col-md-12 text-left">
							<div style="font-family: Prompt;font-size: 15px;font-weight: bold;">สมัครรับข่าวสารสุขภาพของเรา</div>
							<div class="form-group text-left">
								<div style="font-family: Prompt;font-size: 15px;">
									<input type="checkbox" name="">
								ใช่ ฉันต้องการรับข่าวสารสุขภาพจากโรงพยาบาลสมิติเวช</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group text-center">
								<input id="form_botcheck" name="form_botcheck" class="form-control" value="" type="hidden">
								<button type="submit" class="btn btn-md ulockd-btn-thm2" id="btn-contact" data-loading-text="Getting Few Sec...">บันทึก</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4">
				<div class="about-box">
					<p class="ulockd-mrgn120 f-size1" style="font-family: Prompt;font-size: 15px;">สถานที่ตั้ง</p>
					<div style="font-family: Prompt;font-size: 15px;">888/88 หมู่ 3 ถนนสุขุมวิท ต.บ้านสวน อ.เมือง จ.ชลบุรี 20000</div>
					<ul class="list-unstyled">
						<li style="font-family: Prompt;font-size: 15px;"><a href="#" class="slide-fontS"><i class="fa fa-phone text-thm2"></i> 03-303-8888</a></li>
						<li style="font-family: Prompt;font-size: 15px;"><a href="#" class="slide-fontS"><i class="fa fa-mobile text-thm2"></i> +66 33 038 912</a></li>
						<li style="font-family: Prompt;font-size: 15px;"><a href="#" class="slide-fontS"><i class="fa fa-envelope text-thm2"></i> dummy2@yourmail.com</a></li>
						<li style="font-family: Prompt;font-size: 15px;"><a href="#" class="slide-fontS"><i class="fa fa-map-signs text-thm2"></i> www.samitivejhospitals.com</a></li>
					</ul>
					<p class="f-size1">เชื่อมต่อกับเรา</p>
					<ul class="social_icons icon_md list-inline">
						<li><a class="bgc-fb color-white" href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a class="bgc-twtr color-white" href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a class="bgc-inst color-white" href="#"><i class="fa fa-instagram"></i></a></li>
					</ul>

					<div class="form-group text-left">
						<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15528.351283057713!2d100.976023!3d13.344813!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbc01fd1910902d9d!2z4Liq4Lih4Li04LiV4Li04LmA4Lin4LiK4LiK4Lil4Lia4Li44Lij4Li1!5e0!3m2!1sth!2sth!4v1543823038746" width="400" height="300" frameborder="0" style="border:0" allowfullscreen>
						</iframe>
					</div>
				</div>
			</div>
			
		</div>
		
	</div>
</section>

<div class="modal" tabindex="-1" role="dialog" id="modal-alert">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			
			<div class="modal-body text-center">
				<div id="txt-alert"></div>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
			


			
		</div>
	</div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script type="text/javascript">
	$( document ).ready(function() {
		$('#btn-contact').click(function(){
			if($('#firstname').val()==""){
				$('#modal-alert').modal('show');
				$('#txt-alert').html('กรุณาระบุชื่อของคุณ');
				$('#firstname').focus();
				return false;
			}else if($('#lastname').val()==""){
				$('#modal-alert').modal('show');
				$('#txt-alert').html('กรุณาระบุนามสกุลของคุณ');
				$('#lastname').focus();
				return false;
			}else if($('#form_email').val()==""){
				$('#modal-alert').modal('show');
				$('#txt-alert').html('กรุณาระบุอีเมลของคุณ');
				$('#form_email').focus();
				return false;
			}else if($('#question').val()==""){
				$('#modal-alert').modal('show');
				$('#txt-alert').html('กรุณาเลือกคำถาม');
				$('#question').focus();
				return false;
			}else if($('#form_message').val()==""){
				$('#modal-alert').modal('show');
				$('#txt-alert').html('กรุณาระบุคำถาม');
				$('#form_message').focus();
				return false;
			}
		});
	});
</script>