<section class="bgc-darkgold1">
	<div class="container">
		<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center">
		          <div class="ulockd-main-title">
		            <h2 class="mt-separator">รายละเอียดโปรโมชั่น</h2>
		          </div>
		        </div>
			</div>
		<div class="row">
			<div class="col-md-4 col-lg-3 ulockd-pdng0 desktop">
				<div class="ulockd-faq-content">
					<?php
						$this->view('frontend/src/sidemenu/sidemenu');
					?>

	                </div>
					
		        </div>
		<?php /*
			<div class="col-md-4 col-lg-3 ulockd-pdng0 desktop">
				<div class="ulockd-faq-content">
					<p class="f-size1"><span class="icon_menu-square_alt text-thm2"></span> ข้อมูลสำหรับผู้ป่วย</p>
	                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		                        <div class="panel panel-default">
			                        <div class="panel-heading" role="tab" id="headingOne">
			                            <h4 class="panel-title">
			                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#service" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
			                              <i class="icon-Down-2 icon-1"></i>
			                              <i class="icon-Right-2 icon-2"></i>
			                              ข้อควรรู้ก่อนมาใช้บริการ
			                                </a>
			                            </h4>
			                        </div>
			                        <div id="service" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
			                            <div class="panel-body">
			                            	<div class="list-group">
											    <a href="#" class="list-group-item">ประวัติการรักษา</a>
											    <a href="#" class="list-group-item">ข้อมูลประกันสุขภาพ</a>
											    <a href="#" class="list-group-item">โรงพยาบาลเด็กสมิติเวช</a>
											    <a href="#" class="list-group-item">โครงการเบิกจ่ายตรง</a>
											</div>

			                            </div>
			                        </div>
		                        </div>
		                        <div class="panel panel-default">
			                        <div class="panel-heading" role="tab" id="headingTwo">
			                            <h4 class="panel-title">
			                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			                                <i class="icon-Down-2 icon-1"></i>
			                                <i class="icon-Right-2 icon-2"></i>
			                               บริการระหว่างการใช้บริการ</a>
			                            </h4>
			                        </div>
			                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
			                            <div class="panel-body">
			                            	<div class="list-group">
											    <a href="#" class="list-group-item">ห้องพัก</a>
											    <a href="#" class="list-group-item">ร้านอาหาร และอื่น ๆ</a>
											</div>
			                            </div>
			                        </div>
		                        </div>
		                        <div class="panel panel-default">
			                        <div class="panel-heading" role="tab" id="headingThree">
			                            <h4 class="panel-title">
				                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
					                            <i class="icon-Down-2 icon-1"></i>
					                            <i class="icon-Right-2 icon-2"></i>
				                               การเตรียมตัวกลับบ้าน
				                            </a>
			                            </h4>
			                        </div>
			                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false" style="height: 0px;">
			                            <div class="panel-body">
			                              <div class="list-group">
											    <a href="#" class="list-group-item">รายงานผลการรักษา</a>
											    <a href="#" class="list-group-item">ข้อมูลทางการเงิน</a>
											</div>
			                            </div>
			                        </div>
		                        </div>
	                        </div>
					<p class="f-size1"><span class="icon_menu-square_alt text-thm2"></span> บริการของเรา</p>
	                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		                        <div class="panel panel-default">
			                        <div class="panel-heading" role="tab" id="headingOne">
			                            <h4 class="panel-title">
			                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#branch" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
			                              <i class="icon-Down-2 icon-1"></i>
			                              <i class="icon-Right-2 icon-2"></i>
			                              สาขาของโรพยาบาล
			                                </a>
			                            </h4>
			                        </div>
			                        <div id="branch" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
			                            <div class="panel-body">
			                            	<div class="list-group">
											    <a href="#" class="list-group-item">สมิติเวชสุขุมวิท</a>
											    <a href="#" class="list-group-item">สมิติเวชศรีนครินทร์</a>
											    <a href="#" class="list-group-item">โรงพยาบาลเด็กสมิติเวช</a>
											    <a href="#" class="list-group-item">สมิติเวชธนบุรี</a>
											    <a href="#" class="list-group-item">สมิติเวชชลบุรี</a>
											    <a href="#" class="list-group-item">สมิติเวช ไชน่า ทาวน์</a>
											    <a href="#" class="list-group-item">สมิติเวชศรีราชา</a>
											    <a href="#" class="list-group-item">คลินิกพิเศษสมิติเวช</a>
											</div>

			                            </div>
			                        </div>
		                        </div>
		                        <div class="panel panel-default">
			                        <div class="panel-heading" role="tab" id="headingTwo">
			                            <h4 class="panel-title">
			                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#clinic" aria-expanded="false" aria-controls="collapseTwo">
			                                <i class="icon-Down-2 icon-1"></i>
			                                <i class="icon-Right-2 icon-2"></i>
			                               ศูนย์ และคลินิกเด็กผู้ใหญ่</a>
			                            </h4>
			                        </div>
			                        <div id="clinic" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
			                            <div class="panel-body">
			                            	<div class="list-group">
											    <a href="#" class="list-group-item">สถาบันโรคตับและทางเดินอาหาร</a>
											    <a href="#" class="list-group-item">ศูนย์รักษาโรคกระดูกสันหลัง</a>
											    <a href="#" class="list-group-item">คลินิกป้องกันการคลอดก่อนกำหนด</a>
											    <a href="#" class="list-group-item">ศูนย์อุบัติเหตุและฉุกเฉิน</a>
											    <a href="#" class="list-group-item">ศูนย์สุขภาพสตรี</a>
											    <a href="#" class="list-group-item">ศูนย์ศัลยกรรม</a>
											    <a href="#" class="list-group-item">ศูนย์รักษากระดูกสันหลังคด</a>
											</div>

			                            </div>
			                        </div>
		                        </div>
		                        <div class="panel panel-default">
			                        <div class="panel-heading" role="tab" id="headingThree">
			                            <h4 class="panel-title">
				                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#programservice" aria-expanded="false" aria-controls="collapseThree">
					                            <i class="icon-Down-2 icon-1"></i>
					                            <i class="icon-Right-2 icon-2"></i>
				                               โปรแกรม และการบริการ
				                            </a>
			                            </h4>
			                        </div>
			                        <div id="programservice" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false" style="height: 0px;">
			                            <div class="panel-body">
			                              <div class="list-group">
											    <a href="#" class="list-group-item">Applications</a>
											    <a href="#" class="list-group-item">ศูนย์ Critical Care Complex</a>
											    <a href="#" class="list-group-item">360° Virtual Tours</a>
											    <a href="#" class="list-group-item">Samitivej Plus</a>
											    <a href="#" class="list-group-item">Pre-Screening & Games</a>
											    <a href="#" class="list-group-item">Divine นวัตกรรมสุขภาพ</a>
											    <a href="#" class="list-group-item">สมาชิกชีววัฒนะ</a>
											    <a href="#" class="list-group-item">โปรแกรมตรวจสุขภาพ</a>
											    <a href="#" class="list-group-item">แพ็กเกจและโปรโมชั่น</a>
											    <a href="#" class="list-group-item">เวชสำอางค์</a>
											</div>
			                            </div>
			                        </div>
		                        </div>
	                        </div>
						

						<p class="f-size1"><span class="icon_table text-thm2"></span> ข่าวสาร และกิจกรรมล่าสุด</p>
					<div class="bx-wrapper" style="max-width: 100%;">
						<div class="bx-viewport" aria-live="polite" style="width: 100%; overflow: hidden; position: relative; height: 394px;">
							<div class="bx-wrapper" style="max-width: 100%;">
								<div class="bx-viewport" aria-live="polite" style="width: 100%; overflow: hidden; position: relative; height: 315px;">
								<div class="bx-wrapper" style="max-width: 100%;">
									<div class="bx-viewport" aria-live="polite" style="width: 100%; overflow: hidden; position: relative; height: 310px;">
										<div class="bx-wrapper" style="max-width: 100%;">
											<div class="bx-viewport" aria-live="polite" style="width: 100%; overflow: hidden; position: relative; height: 304px;">
												<ul class="testimonial-carousel" style="width: auto; position: relative; transition-duration: 0s; transform: translate3d(0px, -1544.75px, 0px);">
													<li style="float: none; list-style: none; position: relative; width: 253px; margin-bottom: 5px;" class="bx-clone" aria-hidden="true">
			            								<div class="media">
								 							<div class="media-left pull-left">
														    	<a href="#">
														      		<img class="media-object thumbnail" src="http://localhost/samitivej/assets_f/images/healthyB/2.jpg" alt="1.jpg">
														    	</a>
								  							</div>
														  	<div class="media-body">
														   		<p class="f-size1">Ethan Scott</p>
														    	<p class="ulockd-tcompliment"><span class="flaticon-straight-quotes"></span> I ordinarily don't compose letters, however I felt constrained to compose this one...
															    	<strong><a href="#"> 20 Jan 2018 </a></strong>
															    	<span class="flaticon-blocks-with-angled-cuts"></span>
															    </p>
														  	</div>
														</div>
		            								</li>
		            								<li style="float: none; list-style: none; position: relative; width: 253px; margin-bottom: 5px;" class="bx-clone" aria-hidden="true">
			            								<div class="media">
														 	<div class="media-left pull-left">
														    	<a href="#">
														      		<img class="media-object thumbnail" src="http://localhost/samitivej/assets_f/images/healthyB/1.jpg" alt="2.jpg">
														    	</a>
														  	</div>
														  	<div class="media-body">
														   		<p class="f-size1">Marloon James</p>
														    	<p class="ulockd-tcompliment"><span class="flaticon-straight-quotes"></span> I had the delight of being an outpatient in your Ambulatory Surgical... 
															    	<strong><a href="#"> 20 Jan 2018 </a></strong>
															    	<span class="flaticon-blocks-with-angled-cuts"></span>
														    	</p>
														  	</div>
														</div>
		            								</li>
		            								<li style="float: none; list-style: none; position: relative; width: 253px; margin-bottom: 5px;" class="bx-clone" aria-hidden="true">
			            								<div class="media">
														 	<div class="media-left pull-left">
														    	<a href="#">
														      		<img class="media-object thumbnail" src="http://localhost/samitivej/assets_f/images/healthyB/2.jpg" alt="1.jpg">
														    	</a>
														  	</div>
														  	<div class="media-body">
														   		<p class="f-size1">Ethan Scott</p>
														    	<p class="ulockd-tcompliment"><span class="flaticon-straight-quotes"></span> I ordinarily don't compose letters, however I felt constrained to compose this one...
														    		<strong><a href="#"> 20 Jan 2018 </a></strong>
														    		<span class="flaticon-blocks-with-angled-cuts"></span>
														    	</p>
														  	</div>
														</div>
		            								</li>
		            								<li style="float: none; list-style: none; position: relative; width: 253px; margin-bottom: 5px;" class="bx-clone" aria-hidden="true">
									            		<div class="media">
														 	<div class="media-left pull-left">
														    	<a href="#">
														      		<img class="media-object thumbnail" src="http://localhost/samitivej/assets_f/images/healthyB/1.jpg" alt="2.jpg">
														    	</a>
														  	</div>
														  	<div class="media-body">
														   		<p class="f-size1">Marloon James</p>
														    	<p class="ulockd-tcompliment"><span class="flaticon-straight-quotes"></span> I had the delight of being an outpatient in your Ambulatory Surgical... 
														    		<strong><a href="#"> 20 Jan 2018 </a></strong>
														    		<span class="flaticon-blocks-with-angled-cuts"></span>
														    	</p>
														  	</div>
														</div>
		            								</li>
								            		<li style="float: none; list-style: none; position: relative; width: 253px; margin-bottom: 5px;" aria-hidden="true">
									            		<div class="media">
														 	<div class="media-left pull-left">
														    	<a href="#">
														      		<img class="media-object thumbnail" src="http://localhost/samitivej/assets_f/images/healthyB/2.jpg" alt="1.jpg">
														    	</a>
														  	</div>
														  	<div class="media-body">
														   		<p class="f-size1">Ethan Scott</p>
														    	<p class="ulockd-tcompliment"><span class="flaticon-straight-quotes"></span> I ordinarily don't compose letters, however I felt constrained to compose this one... 
														    		<strong><a href="#"> 20 Jan 2018 </a></strong>
														    		<span class="flaticon-blocks-with-angled-cuts"></span>
														    	</p>
														  	</div>
														</div>
								            		</li>
								            		<li style="float: none; list-style: none; position: relative; width: 253px; margin-bottom: 5px;" aria-hidden="false">
									            		<div class="media">
														 	<div class="media-left pull-left">
														    	<a href="#">
														      		<img class="media-object thumbnail" src="http://localhost/samitivej/assets_f/images/healthyB/1.jpg" alt="2.jpg">
														    	</a>
														  	</div>
														  	<div class="media-body">
														   		<p class="f-size1">Marloon James</p>
														    	<p class="ulockd-tcompliment"><span class="flaticon-straight-quotes"></span> I had the delight of being an outpatient in your Ambulatory Surgical...
														    		<strong><a href="#"> 20 Jan 2018 </a></strong>
														    		<span class="flaticon-blocks-with-angled-cuts"></span>
														    	</p>
														  	</div>
														</div>
								            		</li>
									            	<li style="float: none; list-style: none; position: relative; width: 253px; margin-bottom: 5px;" class="bx-clone" aria-hidden="true">
										            		<div class="media">
															 	<div class="media-left pull-left">
															    	<a href="#">
															      		<img class="media-object thumbnail" src="http://localhost/samitivej/assets_f/images/healthyB/2.jpg" alt="1.jpg">
															    	</a>
															  	</div>
															  	<div class="media-body">
															   		<p class="f-size1">Ethan Scott</p>
															    	<p class="ulockd-tcompliment"><span class="flaticon-straight-quotes"></span> I ordinarily don't compose letters, however I felt constrained to compose this one...
															    		<strong><a href="#"> 20 Jan 2018 </a></strong>
															    		<span class="flaticon-blocks-with-angled-cuts"></span></p>
															  	</div>
															</div>
									            		</li>
									            		<li style="float: none; list-style: none; position: relative; width: 253px; margin-bottom: 5px;" class="bx-clone" aria-hidden="true">
										            		<div class="media">
															 	<div class="media-left pull-left">
															    	<a href="#">
															      		<img class="media-object thumbnail" src="http://localhost/samitivej/assets_f/images/healthyB/1.jpg" alt="2.jpg">
															    	</a>
															  	</div>
															  	<div class="media-body">
															   		<p class="f-size1">Marloon James</p>
															    	<p class="ulockd-tcompliment"><span class="flaticon-straight-quotes"></span> I had the delight of being an outpatient in your Ambulatory Surgical... 
															    		<strong><a href="#"> 20 Jan 2018 </a></strong>
															    		<span class="flaticon-blocks-with-angled-cuts"></span></p>

															  	</div>
															</div>
		            									</li>
		            									<li style="float: none; list-style: none; position: relative; width: 253px; margin-bottom: 5px;" class="bx-clone" aria-hidden="true">
										            		<div class="media">
															 	<div class="media-left pull-left">
															    	<a href="#">
															      		<img class="media-object thumbnail" src="http://localhost/samitivej/assets_f/images/healthyB/2.jpg" alt="1.jpg">
															    	</a>
															  	</div>
															  	<div class="media-body">
															   		<p class="f-size1">Ethan Scott</p>
															    	<p class="ulockd-tcompliment"><span class="flaticon-straight-quotes"></span> I ordinarily don't compose letters, however I felt constrained to compose this one...
															    		<strong><a href="#"> 20 Jan 2018 </a></strong>
															    		<span class="flaticon-blocks-with-angled-cuts"></span></p>
															  	</div>
															</div>
									            		</li>
									            		<li style="float: none; list-style: none; position: relative; width: 253px; margin-bottom: 5px;" class="bx-clone" aria-hidden="true">
										            		<div class="media">
															 	<div class="media-left pull-left">
															    	<a href="#">
															      		<img class="media-object thumbnail" src="http://localhost/samitivej/assets_f/images/healthyB/1.jpg" alt="2.jpg">
															    	</a>
															  	</div>
															  	<div class="media-body">
															   		<p class="f-size1">Marloon James</p>
															    	<p class="ulockd-tcompliment"><span class="flaticon-straight-quotes"></span> I had the delight of being an outpatient in your Ambulatory Surgical... 
															    		<strong><a href="#"> 20 Jan 2018 </a></strong>
															    		<span class="flaticon-blocks-with-angled-cuts"></span></p>

															  	</div>
															</div>
		            									</li>
		            								</ul>
		            							</div>
		            							<div class="bx-controls"></div>
		            						</div>
		            					</div>
		            					<div class="bx-controls"></div>
		            				</div>
		            			</div>
		            			<div class="bx-controls"></div>
		            		</div>
		            	</div>
		            	<div class="bx-controls"></div>
		            </div>
	                </div>
					
		        </div>
		    */ ?>
			<div class="col-md-8 col-lg-9">
				<div class="row">
					<div class="col-md-12 ulockd-mrgn1210">
						<!-- <h2><?=$query->package_title; ?></h2> -->
						<div class="ulockd-pd-content">
							<ul class="list-inline course-feature">
								<li><a><i class="fa fa-calendar text-thm2"></i> ตั้งแต่วันที่ <?=$query->package_startdate; ?> ถึง <?=$query->package_enddate; ?></a></li>
							</ul>
						</div>
						<div class="col-md-12 ulockd-mrgn1210">
							<?=$query->package_subdesc; ?>
						</div>
						<!-- 
						<div class="ulockd-project-sm-thumb">
							<img class="img-responsive img-whp" src="<?php echo base_url(); ?>gallery/package/big_imgs/<?=$query->package_big_img; ?>" alt="">
						</div> 
						-->
					</div>
				</div>
				<div class="row">					
					<div class="col-md-12 ulockd-mrgn1210">
						<?=html_entity_decode($query->package_description); ?>
					</div> 
					<!-- 
					<div class="col-md-12 ulockd-mrgn1210">
						<div class="ulockd-pd-content">
							<div class="ulockd-bp-date">
								<ul class="list-inline course-feature">
									<li><a><i class="fa fa-calendar text-thm2"></i> ตั้งแต่วันที่ <?=$query->package_startdate; ?> ถึง <?=$query->package_enddate; ?></a></li>
								</ul>
							</div>
						</div>
					</div>		 
					-->			
					<!-- 
					<div class="col-md-12 ulockd-mrgn1210">
						<?=$query->package_description; ?>
					</div> 
					-->
				</div>
			</div>

			<div class="row">
				<?php
				if(isset($medical_center_package) && $medical_center_package != '' && !empty($medical_center_package)){ ?>
					<caption>
						<h3 style="color:black;text-align:center;"><?=$this->lang->line('samitivej_Related_Medical_Center');?></h3>
						<a href="<?=base_url('frontend/Home/ServiceDetails/').$medical_center_package->uid;?>">
							<h3 style="color:black;text-align:center;"><?=$medical_center_package->medical_center_name;?></h3>
						</a>
						<h4 style="color:black;text-align:center;"><?=$this->lang->line('samitivej_Related_Doctor');?></h3>
					</caption>
					<?php foreach ($doc_package as $doc_package): 
						$pname = $doc_package->pname_th;
						$firstname = $doc_package->firstname_th;
						$lastname = $doc_package->lastname_th;
						$full = $pname.' '.$firstname.' '.$lastname;
					?>
					<center>
					<div class="col-xxs-10 col-xs-6 col-sm-4 col-md-3">
						<div class="team-one text-center">
				            <div class="team-thumb">
				            	<a href="<?php echo base_url();?>frontend/Home/DoctorDetails/<?php echo $doc_package->id;?>">
				                <img class="img-responsive img-whp" style="width:220px;height:240px;" src="<?php echo base_url(); ?>gallery/doctor/big_imgs/<?php echo $doc_package->doctor_imgs; ?>" alt="<?php echo $full;?>">
				            	</a>
				            </div>
						</div>
						<?=$full;?>
					</div>
					</center>
					<?php endforeach; ?>
				<?php } ?>
			</div>
			<div class="row ulockd_bgc_f7 ulockd-bttc">
				
			</div>
		</div>
	</div>
</section>