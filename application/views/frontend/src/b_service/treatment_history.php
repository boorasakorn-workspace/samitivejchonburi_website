<section class="bgc-darkgold1">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 text-center">
		    	<div class="ulockd-main-title">
		        	<h2 class="mt-separator">ประวัติการรักษาทางการแพทย์</h2>
		    	</div>
		   </div>
		</div>
		<div class="row">
			<div class="col-md-4 col-lg-3 ulockd-pdng0 desktop">
				<div class="ulockd-faq-content">
					<?php
						$this->view('frontend/src/sidemenu/sidemenu');
					?>

	                </div>
					
		        </div>
				

			<div class="col-md-8 col-lg-9">
				<div class="row">
					<div class="col-md-12 ulockd-mrgn1210">
						<div class="ulockd-pd-content">
							<h3>ประวัติการรักษาทางการแพทย์</h3>
							<p class="project-dp-one">But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which</p>
							<p class="project-dp-two">toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?</p>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>
</section>