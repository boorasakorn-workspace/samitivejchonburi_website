
<link rel="stylesheet" href="<?php echo base_url(); ?>resource/style.css">
<link href="https://fonts.googleapis.com/css?family=Prompt:300&display=swap" rel="stylesheet">

<section class="ulockd-department ">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3 text-center">
        <div class="ulockd-main-title">
          <h2 class="mt-separator hidden-xs"><?=$this->lang->line('samitivej_Healthy_Blog');?></h2>
          <a href="<?php echo base_url();?>frontend/Home/HealthyBlog"><h3 class="mt-separator visible-xs"><?=$this->lang->line('samitivej_Healthy_Blog');?></h3></a>          
        </div>
        <div class="text-center" style="margin-top : -15px;margin-bottom: 40px;">
            <p><a class="btn btn-lg ulockd-btn-thm2 hidden-xs" href="<?php echo base_url();?>frontend/Home/HealthyBlog"><?=$this->lang->line('samitivej_See_All');?></a><a class="btn btn-xs ulockd-btn-thm2 visible-xs" href="<?php echo base_url();?>frontend/Home/HealthyBlog"><?=$this->lang->line('samitivej_See_All');?></a></p>
        </div>
      </div>
    </div>
    <div class="row">      
      <?php
      $i=0; 
      foreach ($blog_healthy as $result):
        $b_desc = $result->blog_description;
        $b_desc = strip_tags($b_desc); ?>
      <?php if($i == 0){?>
        <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-1 hidden-xs" style="min-height:100%;min-width: 100%opacity: 0"><br></div>
        <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-11">
      <?php }else if($i == 3){?>
      </div><div class="col-xxs-12 col-xs-6 col-sm-6 col-md-11 hidden-xs">
      <?php } ?>
      <?php 
      $i++;
      if($i<3){
        echo '<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4">';
      }
      else{
        echo '<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 hidden-xs">';
      } ?>
        <a href="<?php echo base_url();?>frontend/Home/HealthyblogDetails/<?php echo $result->id;?>">
          <div class="project-box text-center hoverOnZoom" style="background-color: #005f3b;">
            <?php if($i<4){ ?>
              <div style="position:relative;height:200px; min-width:100%;max-width:100%;border-top-right-radius: 35px;border-bottom-left-radius: 35px;background-image: linear-gradient(#e3e0e0,#cbcbcb,#e3dee0);">
              <img class="img-responsive" style="height:120px; min-width:80%;max-width:80%;position: absolute; top: -12%;right: 10%;box-shadow: 2px 3px 14px 0px rgba(0,0,0,0.75);border-radius: 10px 10px 0px 0px;" src="<?php echo base_url(); ?>gallery/blog/big_imgs/<?php echo $result->blog_big_img;?>" alt="<?php echo $result->blog_title;?>">
            <?php }else{ ?>
              <div style="position:relative;height:200px; min-width:100%;max-width:100%;border-top-right-radius: 35px;border-bottom-left-radius: 35px;background-image:  linear-gradient(#e3e0e0,#cbcbcb,#e3dee0);">
              <img class="img-responsive" style="height:120px; min-width:80%;max-width:80%;position: absolute; bottom: -12%;right: 10%;box-shadow: 2px 3px 14px 0px rgba(0,0,0,0.75);border-radius: 0px 0px 10px 10px;" src="<?php echo base_url(); ?>gallery/blog/big_imgs/<?php echo $result->blog_big_img;?>" alt="<?php echo $result->blog_title;?>">
            <?php } ?>
              <div>
              <?php if($i < 4){
                echo '<h4 style="color:#005f3b;text-align: left;padding-left: 5px;padding-top: 110px;text-shadow: 2px 1px 6px rgba(0, 95, 59, 0.27);">'.$result->blog_title.'</h4>';
              }else{
                echo '<h4 style="color:#005f3b;text-align: left;padding-left: 5px;padding-top: 15px;text-shadow: 2px 1px 6px rgba(0, 95, 59, 0.27);">'.$result->blog_title.'</h4>';
              }?>          
                <h6 style="font-weight: 540;color:black;text-align: left;margin-top:-5px;padding-left: 10px;text-shadow: 2px 1px 6px rgba(0, 0, 0, 0.3);"><?=$this->lang->line('samitivej_By')." : ".$result->doctor_name;?></h6>
              </div>
            </div>
          </div>
        </a>
        </br>
        </div>
      <?php
      if($i==3){
        echo '</div><div class="row">';
      }
      endforeach;?>
      </div>
    </div>
  </div>
</section>

