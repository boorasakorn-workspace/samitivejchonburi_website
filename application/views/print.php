<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<link rel="stylesheet" href="<?php echo base_url('public/css/bootstrap.min.css');?>">
<script src="<?php echo base_url('public/js/bootstrap.min.js');?>"></script>

<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
<title><?php echo $this->lang->line('welcome') ?></title>
<link rel="stylesheet" href="<?php echo base_url('public/font-awesome/css/font-awesome.min.css'); ?>">

<link href="https://fonts.googleapis.com/css?family=Mitr:200,300,400,500,600,700&display=swap" rel="stylesheet">
<html lang="en">
<head>
   <meta charset="utf-8">
   <style type="text/css">
      body {
       -webkit-print-color-adjust: exact !important;
    }

    a{
      text-decoration: none;
   }
   .h-covid{
      font-family: Mitr;
      font-weight: 400;
      font-size: 30px;
   }
   .txt{
      font-family: Mitr;
      font-weight: 300;
      font-size: 20px;
   }

   .txt-f{
      font-family: Mitr;
      font-weight: 400;
      font-size: 20px;
   }
   .txt-form{
      font-family: Mitr;
      font-weight: 300;
      font-size: 17px;
   }


   .txt-point{
      font-family: Mitr;
      font-weight: 400;
      font-size: 25px;
      color:#000;
   }
   .txt-b{
      font-family: Mitr;
      font-weight: 400;
      font-size: 20px;
      color:#000;
   }
   .txt-s{
      font-family: Mitr;
      font-weight: 300;
      font-size: 15px;
   }
   .lang{
      font-family: Mitr;
      font-weight: 400;
      font-size: 17px;
      color: #000;
   }

   .green{
      background-color: #7ac37a;
      color: #fff;
   }
   .yellow{
     background-color: #fcfc8f;
     color: #000;
  }
  .red{
   background-color: #fe6868;
   color: #fff;
}


.btn-submit{
   color: #fff;
   background-color: #005f3b;
   border-color: #005f3b;
   font-family: Mitr;
   font-weight: 200;
   font-size: 25px;
}
.btn-demo{
   color: #fff;
   background-color: #c2a472;
   border-color: #c2a472;
   font-family: Mitr;
   font-weight: 200;
   font-size: 25px;
}
@media print {
   @page {
     /* dimensions for the whole page */
     size: A5 landscape;

     margin: 0;
  }

  #print {
    display: none;
 }
 .green{
   background-color: #7ac37a;
   color: #fff;
}
.yellow{
  background-color: #fcfc8f;
  color: #000;
}
.red{
   background-color: #fe6868;
   color: #fff;
}

}


</style>
<div class="container-fluid" style="background-color:#03794c;">
   <div>
      <a href="https://www.samitivejchonburi.com/Welcome/covid">
        <img src="<?php echo base_url('public/images/logo.png');?>">
     </a>
  </div>
</div>
<!-- <body onload=window.print()> -->
   <div class="container">
      <div class="row">
         <div class="col-6 txt-b">ชื่อ-นามสกุล</div>
         <div class="col-6 txt"><?php echo $name;?></div>
      </div>
      <div class="row">
        <div class="col-6 txt-b">อายุ</div>
        <div class="col-6 txt"><?php echo $age." ";?>ปี</div>
     </div>
     <div class="row">
      <div class="col-6 txt-b">ช่องทางการติดต่อ</div>
      <div class="col-6 txt"><?php echo $line;?></div>

   </div>
   <div class="row">
      <div class="col-6 txt-b">ผลรวมคะแนน</div>
      <div class="col-6 txt-point"> <?php echo $point;?></div>
   </div>
   
   <div class="txt-b">
      คำแปลผลคะแนน
   </div>
   <?php if($point>=0 && $point<=5){;?>
      <div class="row txt" id="green-detail-modal" style="display: block;background-color:#7ac37a;color:#fff;padding: 3px;border-radius: 10px;">

         <div>0-5 คะแนน "ไม่มีความเสี่ยง"</div>
         <div>ข้อแนะนำวิธีการปฏิบัติตัวในช่วงของการระบาด Covid-19</div>
         <div>1.หมั่นล้างมือให้สะอาดด้วยสบุ่ หริอแอลกอฮอล์เจล</div>
         <div>2.ใช้หน้ากากอนามัยหรือผ้าปิดปากและจมูก เพื่อป้องกันการได้รับเชื้อโรค</div>
         <div>3.Social Distancing การเว้นระยะห่างระหว่างตัวคุณกับบุคคลอื่น</div>



        <!--  <div>0-10 คะแนน ไม่มีความเสี่ยง Zone สีเขียว  </div> 
         <div>การมาตรวจที่รพ.โดยไม่จำเป็น</div> 
         <div> 1. อาจทำให้ท่านมีโอกาสได้รับเชื้อมากขึ้น</div> 
         <div> 2. อาจจะไม่ได้รับการตรวจ / ผลการตรวจว่าไม่มีเชื้อ ไม่สามารถยืนยันว่า ไม่ติดเชื้อ (อาจอยู่ในระยะฟักตัว)</div> 
         <div> วิธีการปฏิบัติตัวในช่วงของการระบาด Covid-19</div> 
         <div> 1. หมั่นล้างมือให้สะอาดด้วยสบู่ หรือแอลกอฮอล์เจล</div> 
         <div> 2. หลีกเลี่ยงการสัมผัสผู้ที่มีอาการคล้ายไข้หวัด หรือหลีกเลี่ยงการไปที่มีฝูงชน</div> 
         <div> 3. ปรุงอาหารประเภทเนื้อสัตว์และไข่ให้สุกด้วยความร้อน</div> 
         <div> 4. ใช้ผ้าปิดปากหรือจมูก เพื่อป้องกันการได้รับเชื้อโรค</div>  -->
      </div>

   <?php  }else if($point>= 10 && $point <=20 ){;?>
     <div class="row txt" id="yellow-detail-modal" style="display: block;background-color:#fcfc8f;color:#000;padding: 3px;border-radius: 10px;">

      <div>10-20 คะแนน <font style="color: red;font-family: Mitr;font-weight: 400;font-size: 25px;">"คุณมีความเสี่ยงอาจติดเชื้อ Covid-19"</font></div>
      <div>เรามีคำแนะนำให้ท่าน</div>
      <div>1.ใช้หน้ากากอนามัยหรือผ้าปิดปากและจมูก เพื่อป้องกันการแพร่เชื้อโรค</div>
      <div>2.Social Distancing การเว้นระยะห่างระหว่างตัวคุณกับบุคคลอื่น</div>
      <div><font style="color: red;font-family: Mitr;font-weight: 400;font-size: 25px;">"เมื่อมาถึงโรงพยาบาล กรุณาแจ้งเจ้าหน้าที่ ทันที เพื่อเข้าตรวจที่ ARI"</font>กรุณาติดต่อ ตั้งแต่ 8.00-20.00 น.</div>



      <!-- <div>15-20 คะแนน มีความเสี่ยง Zone สีเหลือง  </div>  -->
      <!-- <div>กรุณาปฏิบัติตามข้อแนะนำ</div>  -->

   </div>


<?php }else if($point>=25){;?>
 <div  class="row txt "id="red-detail-modal" style="display: block;background-color:#fe6868;color:#fff;padding: 3px;border-radius: 10px;">

   <div>25 คะแนนขึ้นไป "มีความเสี่ยงสูงอาจติดเชื้อ Covid-19"</div>
   <div>เรามีคำแนะนำให้ท่าน</div>
   <div>1.ใช้หน้ากากอนามัยหรือผ้าปิดจมูก เพื่อป้องการแพร่เชื้อโรค</div>
   <div>2.Social Distancing การเว้นระยะระหว่างตัวคุณกับบุคคลอื่น</div>
   <div>กรุณาแจ้งเจ้าหน้าที่ เมื่อถึง ร.พ.เพื่ออำนวยความสะดวกในการเข้าห้องตรวจเฉพาะ</div>

<!-- 
   <div>
      25 คะแนนขึ้นไป มีความเสี่ยงสูง Zone สีแดง   เรามีคำแนะนำให้ท่าน
   </div>
   <div>
      1. โปรดงดออกจากบ้าน 
   </div>
   <div>
      2. เลี่ยงการปฏิสัมพันธ์กับคนอื่น
   </div>
   <div>
      3. แพทย์และพยาบาลจะทำการติดต่อท่านเพื่อสอบถามอาการ เพื่อให้ความช่วยเหลืออื่นๆต่อไป
   </div> -->
</div>
<?php };?>


<!-- <div class="txt-s">

   <div>
      การมาตรวจที่รพ.โดยไม่จำเป็น
   </div>
   <div>
      1. อาจทำให้ท่านมีโอกาสได้รับเชื้อมากขึ้น
   </div>
   <div>
      2. อาจจะไม่ได้รับการตรวจ /ผลตรวจว่าไม่มีเชื้อ ไม่สามารถยืนยันว่า ไม่ติดเชื้อ (อาจอยู่ในระยะฟักตัว)
   </div>
   <div>
      วิธีปฎิบัติตัวในช่วงของการระบาด COVID-19
   </div>
   <div>
      1. หมั่นล้างมือให้สะอาดด้วยสบู่หรือแอลกอฮอล์เจล
   </div>
   <div>
      2. หลีกเลี่ยงการสัมผัสผู้ที่มีอาการคล้ายไข้หวัด หรือหลีกเลี่ยงการไปที่มีฝูงชน
   </div>
   <div>
      3. ปรุงอาหารประเภทเนื้อสัตว์และไข่ให้สุกด้วยความร้อน
   </div>
   <div>
      4. ให้ผ้าปิดปากหรือจมูก เพื่อป้องกันการได้รับเชื้อโรค
   </div> -->
</div>
<div class="text-center mb-4 mt-4">
   <button class="btn btn-submit" onclick="window.print()" id="print">พิมพ์</button> 
</div>
</div>
</body>