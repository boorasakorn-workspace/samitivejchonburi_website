<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Home_model');
        $this->load->model("calendar_model");
        
        $this->load->library('email');

	    $config['protocol']    = 'smtp';
	    $config['smtp_host']    = 'smtp.mailtrap.io';
	    $config['smtp_port']    = '2525';
	    $config['smtp_timeout'] = '7';
	    $config['smtp_user']    = '4df5162eacf19e';
	    $config['smtp_pass']    = '1a9f436700ca7d';
	    $config['charset']    = 'utf-8';
	    $config['crlf']    = "\r\n";
	    $config['newline']    = "\r\n";
	    $config['mailtype'] = 'text'; // or html
	    $config['validation'] = TRUE; // bool whether to validate email or not      
	    $this->email->initialize($config);
	}

	public function language(){
		if(empty($this->session->userdata('language'))){
			$this->session->set_userdata("language", 'thai');
			$this->session->set_userdata("q_l", ' ');
		}
		$this->session->set_userdata("q_l", '_'.substr($this->session->userdata('language'), 0,2));
		if($this->session->userdata('q_l') == 'th' || $this->session->userdata('q_l') == '_th' ){
			$this->session->set_userdata("q_l", ' ');			
		}
		return $this->lang->load($this->session->userdata('language'), 'english');
			
	}

	public function change_language($lang){
		$this->session->set_userdata("language", $lang);
		$this->session->set_userdata("q_l", '_'.substr($lang, 0,2));
		if($this->session->userdata('q_l') == 'th' || $this->session->userdata('q_l') == '_th' ){
			$this->session->set_userdata("q_l", ' ');			
		}
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function index()
	{
		$this->load->library('session');

		$this->language();

    	//$this->lang->line('content');
		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();
		$data['get_carousel'] = $this->fetch_carousel()->result();

		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['slidecontents'] = 'frontend/src/slidecontents';
		$data['listcontents'] = 'frontend/src/listcontents';



		$data['get_package']=$this->Home_model->package();


		$data['blog_healthy']=$this->Home_model->bloghealthy();
		/*echo "<pre>"; print_r($data['package']); die();*/
		/*$data['package']=$this->load->view('frontend/src/package', $data);*/
		//blogcontent
		$data['doctor']=$this->Home_model->doctor();
		$data['all_med_doctor']=$this->Home_model->lang_all_medicial_center();

		$data['new_activity']=$this->Home_model->new_activity();

		$data['medical_center']=$this->Home_model->medicial_center();

		$data['package'] = 'frontend/src/package';
		$data['bloghealthy'] = 'frontend/src/bloghealthy';
		$data['recommend_doctor'] = 'frontend/src/recommend_doctor';
		$data['services'] = 'frontend/src/services';
		$data['blogcontents'] = 'frontend/src/blogcontents';
		$data['footerpages'] = 'frontend/src/footerpages';
		$data['modal_selfregis'] = 'frontend/src/modal_self';
		/*
		echo '<pre>'.var_export($this->session).'</pre>';
		die();*/
		$this->load->view('frontend/homepage', $data);
	}

	public function index_preview()
	{
		$this->load->library('session');

		$this->language();

    	//$this->lang->line('content');
		$data['preview'] = 'preview';
		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();
		$data['get_carousel'] = $this->fetch_carousel()->result();

		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['slidecontents'] = 'frontend/src/slidecontents';
		$data['listcontents'] = 'frontend/src/listcontents';



		$data['get_package']=$this->Home_model->package();

		$data['blog_healthy']=$this->Home_model->bloghealthy();
		/*echo "<pre>"; print_r($data['package']); die();*/
		/*$data['package']=$this->load->view('frontend/src/package', $data);*/
		//blogcontent
		$data['doctor']=$this->Home_model->doctor();
		$data['new_activity']=$this->Home_model->new_activity();

		$data['medical_center']=$this->Home_model->medicial_center();

		$data['package'] = 'frontend/src/package';
		$data['bloghealthy'] = 'frontend/src/bloghealthy';
		$data['recommend_doctor'] = 'frontend/src/recommend_doctor';
		$data['services'] = 'frontend/src/services';
		$data['blogcontents'] = 'frontend/src/blogcontents';
		$data['footerpages'] = 'frontend/src/footerpages';
		$data['modal_selfregis'] = 'frontend/src/modal_self';
		/*
		echo '<pre>'.var_export($this->session).'</pre>';
		die();*/
		$this->load->view('frontend/homepage', $data);
	}

	public function main($language=null){

		$this->language();
		$this->load->model('Home_model');
		$data['language']=$this->Home_model->get_nav_menu();

		$data['menu']=$this->Home_model->menu($language);


		$data['package']=$this->Home_model->package($language);
		//blogcontent
		$data['new_activity']=$this->Home_model->new_activity($language);

		$this->load->view('frontend/frontpage', $data);

	}

	public function DoctorDetails()
	{
		$this->language();
		$update_id = $this->uri->segment(4);

		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();

		$data['get_sidemenu'] = $this->fetch_sidemenu();
		$data['get_subsidemenu'] = $this->fetch_subsidemenu();
		$data['get_subofsubmenu'] = $this->fetch_subofsubmenu();

		$this->load->model('Home_model');
		$data['doc_content'] = $this->Home_model->bloghealthyofdoc($update_id); 
		$data['all_med_doctor']=$this->Home_model->lang_all_medicial_center();
		$data['all_specialty']=$this->Home_model->lang_all_specialty();
		$data['all_sub_specialty']=$this->Home_model->lang_all_sub_specialty();

		$ql = $this->session->userdata('q_l');
		if($ql == ' '){
			$ql = '_th';
		}
		//$mysql_query = "select * from tb_doctor where status='1' and id=".$update_id;
		$mysql_query = "select id, doctor_url, doctor_imgs, pname" .$ql. " as pname_th, firstname" .$ql. " as firstname_th, lastname" .$ql. " as lastname_th, medical_id, medical_master_id, education" .$ql. " as education_th, experience" .$ql. " as experience_th, phone, email, address, type" .$ql. " as type_th, speciality_id, speciality" .$ql. " as speciality_th, doctor_department, doctor_department_child, language" .$ql. " as language_th, doctor_description" .$ql. " as doctor_description, gender, at_hospital, status, come_when, out_when, created_at, updated_at, doctor_code, location_code, location_name, parent_medical_center_id , parent_specialty_id , parent_sub_specialty_id from tb_doctor where status='1' and id=".$update_id;
		$data['query'] = $this->_custom_query($mysql_query)->row();
		if($data['query'] == NULL){
			redirect('frontend/Home');
		}
		$mysql_query = "select * from tb_workdayhours where medical_id='".$data['query']->medical_id."'";
		$data['query_workdayhour'] = $this->_custom_query($mysql_query)->result();
		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['doctorDetail'] = 'frontend/src/doctor/doctor_details';
		$data['footerpages'] = 'frontend/src/footerpages';

		$this->load->view('frontend/homepage', $data);
	}

	public function DoctorDetails_preview()
	{
		$this->language();
		$update_id = $this->uri->segment(4);

		$data['preview'] = 'preview';
		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();

		$data['get_sidemenu'] = $this->fetch_sidemenu();
		$data['get_subsidemenu'] = $this->fetch_subsidemenu();
		$data['get_subofsubmenu'] = $this->fetch_subofsubmenu();

		$this->load->model('Home_model');
		$data['doc_content'] = $this->Home_model->bloghealthyofdoc($update_id); 
		$data['all_med_doctor']=$this->Home_model->lang_all_medicial_center();
		$data['all_specialty']=$this->Home_model->lang_all_specialty();
		$data['all_sub_specialty']=$this->Home_model->lang_all_sub_specialty();
		
		$ql = $this->session->userdata('q_l');
		if($ql == ' '){
			$ql = '_th';
		}
		//$mysql_query = "select * from tb_doctor where status='1' and id=".$update_id;
		$mysql_query = "select id, doctor_url, doctor_imgs, pname" .$ql. " as pname_th, firstname" .$ql. " as firstname_th, lastname" .$ql. " as lastname_th, medical_id, medical_master_id, education" .$ql. " as education_th, experience" .$ql. " as experience_th, phone, email, address, type" .$ql. " as type_th, speciality_id, speciality" .$ql. " as speciality_th, doctor_department, doctor_department_child, language" .$ql. " as language_th, doctor_description" .$ql. " as doctor_description, gender, at_hospital, status, come_when, out_when, created_at, updated_at, doctor_code, location_code, location_name, parent_medical_center_id , parent_specialty_id , parent_sub_specialty_id from tb_doctor where id=".$update_id;
		$data['query'] = $this->_custom_query($mysql_query)->row();
		if($data['query'] == NULL){
			redirect('frontend/Home');
		}		
		$mysql_query = "select * from tb_workdayhours where medical_id='".$data['query']->medical_id."'";
		$data['query_workdayhour'] = $this->_custom_query($mysql_query)->result();

		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['doctorDetail'] = 'frontend/src/doctor/doctor_details';
		$data['footerpages'] = 'frontend/src/footerpages';

		$this->load->view('frontend/homepage', $data);
	}

	public function OurDoctors()
	{
		$this->language();
		$this->load->model('Home_model');
		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();

		$data['doctor']=$this->Home_model->speciality();
		$data['alldoctor']=$this->Home_model->alldoctor();

		$data['all_med_doctor']=$this->Home_model->lang_all_medicial_center();
		$data['all_specialty']=$this->Home_model->lang_all_specialty();
		$data['all_sub_specialty']=$this->Home_model->lang_all_sub_specialty();
		
		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['ourDoctors'] = 'frontend/src/doctor/our_doctors';
		$data['footerpages'] = 'frontend/src/footerpages';

		$this->load->view('frontend/homepage', $data);
	}

	public function DoctorAppointment()
	{
		$this->language();

		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();		

		$data['get_sidemenu'] = $this->fetch_sidemenu()->result();
		$data['get_subsidemenu'] = $this->fetch_subsidemenu()->result();
		$data['get_subofsubmenu'] = $this->fetch_subofsubmenu()->result();

		$data['alldoctor']=$this->Home_model->alldoctor();

		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['doctorAppointment'] = 'frontend/src/doctor/doctor_appointments';
		$data['footerpages'] = 'frontend/src/footerpages';

		$data['speciality']=$this->Home_model->speciality();
		$this->load->view('frontend/homepage', $data);
	}

	public function Full_Appointment_Email_Send()
	{
		$this->language();

	    $appointment_subject = $this->input->post('appointment_title_text', TRUE);
	    $doctor_name = $this->input->post('doctor_medical_id', TRUE);
	    $patient_name = $this->input->post('firstname', TRUE);
	    $patient_surname = $this->input->post('lastname', TRUE);
	    $patient_birth = $this->input->post('birthday', TRUE);
	    $patient_gender = $this->input->post('gender', TRUE);
	    $patient_email = $this->input->post('form_email', TRUE);
	    $patient_phone = $this->input->post('form_phone', TRUE);
	    $patient_info_message = $this->input->post('description', TRUE);
	    $appointment_date = $this->input->post('appointment_date', TRUE);
	    $appointment_sdate = $this->input->post('appointment_sdate', TRUE);

	    $this->email->from('samitivej-chonburi@samitivej.com', 'Samitiveh Chonburi');
		$to = $patient_email;
		$subject = 'No reply : ' . $appointment_subject;
		$message = "\r\n".'Patient Name : '.$patient_name.' '.$patient_surname."\r\n".
		'Birthday : '.$patient_birth."\r\n".
		'Appointment Info :'.$patient_info_message."\r\n".
		'Appointment Date :'.$appointment_date."\r\n";
		if(!empty($appointment_sdate)){
			$message .= 'Second Appointment Date :'.$appointment_sdate."\r\n";
		}
		if(!empty($patient_email) || !empty($patient_phone)){
			$message .= 'Contact :'."\r\n";
			if(!empty($patient_email)){
				$message .= ' Email :'.$patient_email."\r\n";
			}
			if(!empty($patient_phone)){
				$message .= ' Phone :'.$patient_phone;
			}
						
		}
		$data['email_from'] = 'samitivej-chonburi@samitivej.com';
		$data['email_target'] = $to;
		$data['email_subject'] = $subject;
		//$data['email_message'] = $message;
		$data['email_message'] = str_replace('\r\n', '<br>', $message);
		$data['email_status'] = 1;
		$data['cuser'] = 0;

		$this->Home_model->_insert_email($data);

		$this->email->to($to);
	    $this->email->subject($subject);
	    $this->email->message($message);

		$this->email->send();
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function Appointment_Email_Send()
	{
		$this->language();

	    $appointment_subject = $this->input->post('appointment_title_text', TRUE);
	    $doctor_name = $this->input->post('doc_fullname', TRUE);
	    $patient_name = $this->input->post('firstname', TRUE);
	    $patient_surname = $this->input->post('lastname', TRUE);
	    $patient_birth = $this->input->post('birthday', TRUE);
	    $patient_gender = $this->input->post('gender', TRUE);
	    $patient_email = $this->input->post('form_email', TRUE);
	    $patient_phone = $this->input->post('form_phone', TRUE);
	    $patient_info_message = $this->input->post('form_message', TRUE);
	    $appointment_date = $this->input->post('appointment_date', TRUE);
	    $appointment_time = $this->input->post('appointment_time', TRUE);
	    $appointment_sdate = $this->input->post('appointment_sdate', TRUE);
	    $appointment_stime = $this->input->post('appointment_time', TRUE);

	    $this->email->from('samitivej-chonburi@samitivej.com', 'Samitiveh Chonburi');
		$to = $patient_email;
		$subject = 'No reply : ' . $appointment_subject;
		$message = "\r\n".'Patient Name : '.$patient_name.' '.$patient_surname."\r\n".
		'Birthday : '.$patient_birth."\r\n".
		'Appointment Info :'.$patient_info_message."\r\n".
		'Appointment Date :'.$appointment_date.' : '.$appointment_time."\r\n";
		if(!empty($appointment_sdate)){
			$message .= 'Second Appointment Date : '.$appointment_sdate.' : '.$appointment_stime."\r\n";
		}
		if(!empty($patient_email) || !empty($patient_phone)){
			$message .= 'Contact :'."\r\n";
			if(!empty($patient_email)){
				$message .= ' Email : '.$patient_email."\r\n";
			}
			if(!empty($patient_phone)){
				$message .= ' Phone : '.$patient_phone;
			}
						
		}
		
		$data['email_from'] = 'samitivej-chonburi@samitivej.com';
		$data['email_target'] = $to;
		$data['email_subject'] = $subject;
		//$data['email_message'] = $message;
		$data['email_message'] = str_replace('\r\n', '<br>', $message);
		$data['email_status'] = 1;
		$data['cuser'] = 0;

		$this->Home_model->_insert_email($data);

		$this->email->to($to);
	    $this->email->subject($subject);
	    $this->email->message($message);

		$this->email->send();
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function clinicAdults()
	{
		$this->language();
		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();


		$data['get_sidemenu'] = $this->fetch_sidemenu()->result();
		$data['get_subsidemenu'] = $this->fetch_subsidemenu()->result();
		$data['get_subofsubmenu'] = $this->fetch_subofsubmenu()->result();

		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['clinicAdults'] = 'frontend/src/clinicalcenter/clinic_for_adults';
		$data['footerpages'] = 'frontend/src/footerpages';

		$this->load->view('frontend/homepage', $data);
	}

	public function News()
	{
		$this->language();
		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();

		$ql = $this->session->userdata('q_l');
		$data['query'] = $this->db->query('select new_activity_name'.$ql.' as new_activity_name , description'.$ql.' as description , new_activity_big_img,new_activity_small_img,new_activity_id,cuser,cwhen,mwhen,meta_author,meta_description,meta_keywords , tb_account.* FROM new_activity LEFT JOIN tb_account on tb_account.acc_id = new_activity.cuser WHERE new_activity_status = 1')->result();


		$data['get_sidemenu'] = $this->fetch_sidemenu()->result();
		$data['get_subsidemenu'] = $this->fetch_subsidemenu()->result();
		$data['get_subofsubmenu'] = $this->fetch_subofsubmenu()->result();

		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['news'] = 'frontend/src/NewsAndEvents/All_news';
		$data['footerpages'] = 'frontend/src/footerpages';

		$this->load->view('frontend/homepage', $data);
	}

	public function NewsDetails($update_id)
	{
		$this->language();
		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();

		$ql = $this->session->userdata('q_l');
		$mysql_query = "select new_activity_name".$ql." as new_activity_name , description".$ql." as description , new_activity_big_img,new_activity_small_img,new_activity_id,cuser,cwhen,mwhen,meta_author,meta_description,meta_keywords , tb_account.* FROM new_activity LEFT JOIN tb_account on tb_account.acc_id = new_activity.cuser WHERE new_activity_status = 1 AND new_activity_id = ".$update_id;
		$data['query'] = $this->_custom_query($mysql_query)->row();

		$data['get_sidemenu'] = $this->fetch_sidemenu()->result();
		$data['get_subsidemenu'] = $this->fetch_subsidemenu()->result();
		$data['get_subofsubmenu'] = $this->fetch_subofsubmenu()->result();

		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['newsDetails'] = 'frontend/src/NewsAndEvents/news_details';
		$data['footerpages'] = 'frontend/src/footerpages';

		$this->load->view('frontend/homepage', $data);
	}

	public function NewsDetails_preview($update_id)
	{
		$this->language();
		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();

		$ql = $this->session->userdata('q_l');
		$data['preview'] = 'preview';
		$mysql_query = "select new_activity_name".$ql." as new_activity_name , description".$ql." as description , new_activity_big_img,new_activity_small_img,new_activity_id,cuser,cwhen,mwhen,meta_author,meta_description,meta_keywords , tb_account.* FROM new_activity LEFT JOIN tb_account on tb_account.acc_id = new_activity.cuser WHERE new_activity_id = ".$update_id;
		$data['query'] = $this->_custom_query($mysql_query)->row();

		$data['get_sidemenu'] = $this->fetch_sidemenu()->result();
		$data['get_subsidemenu'] = $this->fetch_subsidemenu()->result();
		$data['get_subofsubmenu'] = $this->fetch_subofsubmenu()->result();

		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['newsDetails'] = 'frontend/src/NewsAndEvents/news_details';
		$data['footerpages'] = 'frontend/src/footerpages';

		$this->load->view('frontend/homepage', $data);
	}
	public function HealthyBlog()
	{
		$this->language();
		$this->load->helper('text');
		/*
		$ql = $this->session->userdata('q_l');
		$mysql_query = "select blog_title".$ql." as blog_title_lang , * from tb_bloghealthy where blog_status='1' order by date_published desc";

		$data['query'] = $this->_custom_query($mysql_query);
		*/
		$ql = $this->session->userdata('q_l');
		$mysql_query = "select blog_title".$ql." as blog_title , blog_description".$ql." as blog_description , blog_contents".$ql." as blog_contents ,id,blog_url,blog_keywords,blog_status,date_published,c_user,m_user,created_at,updated_at,meta_author,meta_description,meta_keywords,blog_big_img,blog_small_img,promotion_link_1,promotion_link_2,doc_id from tb_bloghealthy where blog_status='1' order by id desc";

		$data['query'] = $this->_custom_query($mysql_query);

		$data['get_doctor']=$this->Home_model->alldoctor();
		$data['all_med_doctor']=$this->Home_model->lang_all_medicial_center();

		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();

		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['healthyBlog'] = 'frontend/src/healthyblog/All_healthyblog';
		$data['footerpages'] = 'frontend/src/footerpages';
		$this->load->view('frontend/homepage', $data);
	}

	public function HealthyblogDetails()
	{
		$this->language();
		$update_id = $this->uri->segment(4);

		if(!is_numeric($update_id)){
			redirect('frontend/Home/HealthyBlog');
		}

		$data['update_id'] = $update_id;

		$ql = $this->session->userdata('q_l');
		$mysql_query = "select blog_title".$ql." as blog_title , blog_description".$ql." as blog_description , blog_contents".$ql." as blog_contents ,id,blog_url,blog_keywords,blog_status,date_published,c_user,m_user,created_at,updated_at,meta_author,meta_description,meta_keywords,blog_big_img,blog_small_img,promotion_link_1,promotion_link_2,doc_id from tb_bloghealthy where blog_status='1'";
		$mysql_query .= "and id='$update_id'";

		$data['query'] = $this->_custom_query($mysql_query)->row();

		$data['get_doctor']=$this->Home_model->where_doctor($data['query']->doc_id);
		$data['all_med_doctor']=$this->Home_model->lang_all_medicial_center();
		foreach($this->_custom_query($mysql_query)->result() as $rows){ 
			$data['meta_author'] = $rows->meta_author;
			$data['meta_keywords'] = $rows->meta_keywords;
			$data['meta_description'] = $rows->meta_description;
		}

		if(isset($data['query']->promotion_link_1) && $data['query']->promotion_link_1 > 0){
			$p_mysql_query = "select package_title".$ql." as package_title , package_name".$ql." as package_name , package_description".$ql." as package_description , package_subdesc".$ql." as package_subdesc ,package_url,package_status,package_unitprice,package_big_img,package_small_img,id,c_user,m_user,created_at,package_startdate,package_enddate,updated_at,meta_author,meta_description,meta_keywords from tb_package where package_status='1' and id='".$data['query']->promotion_link_1."'";
			$data['promotion_1'] = $this->_custom_query($p_mysql_query)->row();	
		}
		if(isset($data['query']->promotion_link_2) && $data['query']->promotion_link_2 > 0){
			$p2_mysql_query = "select package_title".$ql." as package_title , package_name".$ql." as package_name , package_description".$ql." as package_description , package_subdesc".$ql." as package_subdesc ,package_url,package_status,package_unitprice,package_big_img,package_small_img,id,c_user,m_user,created_at,package_startdate,package_enddate,updated_at,meta_author,meta_description,meta_keywords from tb_package where package_status='1' and id='".$data['query']->promotion_link_2."'";
			$data['promotion_2'] = $this->_custom_query($p2_mysql_query)->row();
		}


		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();

		$data['get_sidemenu'] = $this->fetch_sidemenu()->result();
		$data['get_subsidemenu'] = $this->fetch_subsidemenu()->result();
		$data['get_subofsubmenu'] = $this->fetch_subofsubmenu()->result();

		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['healthyblogDetails'] = 'frontend/src/healthyblog/healthyblog_details';
		$data['footerpages'] = 'frontend/src/footerpages';

		$this->load->view('frontend/homepage', $data);
	}

	public function HealthyblogDetails_preview()
	{
		$this->language();
		$update_id = $this->uri->segment(4);

		if(!is_numeric($update_id)){
			redirect('frontend/Home/HealthyBlog');
		}

		$data['update_id'] = $update_id;
		$data['preview'] = 'preview';

		$ql = $this->session->userdata('q_l');
		$mysql_query = "select blog_title".$ql." as blog_title , blog_description".$ql." as blog_description , blog_contents".$ql." as blog_contents ,id,blog_url,blog_keywords,blog_status,date_published,c_user,m_user,created_at,updated_at,meta_author,meta_description,meta_keywords,blog_big_img,blog_small_img,promotion_link_1,promotion_link_2 from tb_bloghealthy where ";
		$mysql_query .= " id='$update_id'";

		$data['query'] = $this->_custom_query($mysql_query)->row();
		$data['get_doctor']=$this->Home_model->where_doctor($data['query']->doc_id);
		$data['all_med_doctor']=$this->Home_model->lang_all_medicial_center();

		foreach($this->_custom_query($mysql_query)->result() as $rows){ 
			$data['meta_author'] = $rows->meta_author;
			$data['meta_keywords'] = $rows->meta_keywords;
			$data['meta_description'] = $rows->meta_description;
		}
		if(isset($data['query']->promotion_link_1) && $data['query']->promotion_link_1 > 0){
			$p_mysql_query = "select package_title".$ql." as package_title , package_name".$ql." as package_name , package_description".$ql." as package_description , package_subdesc".$ql." as package_subdesc ,package_url,package_status,package_unitprice,package_big_img,package_small_img,id,c_user,m_user,created_at,package_startdate,package_enddate,updated_at,meta_author,meta_description,meta_keywords from tb_package where package_status='1' and id='".$data['query']->promotion_link_1."'";
			$data['promotion_1'] = $this->_custom_query($p_mysql_query)->row();	
		}
		if(isset($data['query']->promotion_link_2) && $data['query']->promotion_link_2 > 0){
			$p2_mysql_query = "select package_title".$ql." as package_title , package_name".$ql." as package_name , package_description".$ql." as package_description , package_subdesc".$ql." as package_subdesc ,package_url,package_status,package_unitprice,package_big_img,package_small_img,id,c_user,m_user,created_at,package_startdate,package_enddate,updated_at,meta_author,meta_description,meta_keywords from tb_package where package_status='1' and id='".$data['query']->promotion_link_2."'";
			$data['promotion_2'] = $this->_custom_query($p2_mysql_query)->row();
		}


		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();

		$data['get_sidemenu'] = $this->fetch_sidemenu()->result();
		$data['get_subsidemenu'] = $this->fetch_subsidemenu()->result();
		$data['get_subofsubmenu'] = $this->fetch_subofsubmenu()->result();

		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['healthyblogDetails'] = 'frontend/src/healthyblog/healthyblog_details';
		$data['footerpages'] = 'frontend/src/footerpages';

		$this->load->view('frontend/homepage', $data);
	}

	public function Promotion()
	{
		$this->language();
		$this->load->helper('text');


		$ql = $this->session->userdata('q_l');
		$mysql_query = "select package_title".$ql." as package_title , package_name".$ql." as package_name , package_description".$ql." as package_description , package_subdesc".$ql." as package_subdesc ,package_url,package_status,package_big_img,package_small_img,id,c_user,m_user,created_at,updated_at,meta_author,meta_description,meta_keywords,package_startdate,package_enddate from tb_package where package_status='1'";
		$mysql_query .= "and package_startdate <= ";
		$mysql_query .= "'" . date('Y-m-d') . "' ";
		$mysql_query .= "and package_enddate >= ";
		$mysql_query .= "'" . date('Y-m-d') . "' ";
		$mysql_query .= "order by id desc";
		$data['query'] = $this->_custom_query($mysql_query);

		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();

		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['promotion'] = 'frontend/src/package/All_promotion';
		$data['footerpages'] = 'frontend/src/footerpages';

		$this->load->view('frontend/homepage', $data);
	}

	public function PromotionDetails()
	{
		$this->language();
		$update_id = $this->uri->segment(4);

		if(!is_numeric($update_id)){
			redirect('frontend/Home/Promotion');
		}	 

		$data['update_id'] = $update_id;

		$ql = $this->session->userdata('q_l');

		$mysql_query = "select package_title".$ql." as package_title , package_name".$ql." as package_name , package_description".$ql." as package_description , package_subdesc".$ql." as package_subdesc ,package_url,package_status,package_unitprice,package_big_img,package_small_img,id,c_user,m_user,created_at,package_startdate,package_enddate,updated_at,meta_author,meta_description,meta_keywords,relate_medical_center_id from tb_package where package_status='1'";
		$mysql_query .= "and id = '".$update_id."' ";
		$mysql_query .= "and package_startdate <= ";
		$mysql_query .= "'" . date('Y-m-d') . "' ";
		$mysql_query .= "and package_enddate >= ";
		$mysql_query .= "'" . date('Y-m-d') . "' ";
		$data['query'] = $this->_custom_query($mysql_query)->row();

		foreach($this->_custom_query($mysql_query)->result() as $rows){ 
			$data['meta_author'] = $rows->meta_author;
			$data['meta_keywords'] = $rows->meta_keywords;
			$data['meta_description'] = $rows->meta_description;
		}

		$this->load->model('Home_model');
		$data['medical_center_package'] = $this->Home_model->id_medicial_center($data['query']->relate_medical_center_id); 
		$data['doc_package'] = $this->Home_model->where_related_doctor($data['query']->relate_medical_center_id); 

		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();

		$data['get_sidemenu'] = $this->fetch_sidemenu()->result();
		$data['get_subsidemenu'] = $this->fetch_subsidemenu()->result();
		$data['get_subofsubmenu'] = $this->fetch_subofsubmenu()->result();

		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['promotionDetails'] = 'frontend/src/package/promotion_details';
		$data['footerpages'] = 'frontend/src/footerpages';

		$this->load->view('frontend/homepage', $data);
	}


	public function PromotionDetails_preview()
	{
		$this->language();
		$update_id = $this->uri->segment(4);

		if(!is_numeric($update_id)){
			redirect('frontend/Home/Promotion');
		}	 

		$data['preview'] = 'preview';
		$data['update_id'] = $update_id;

		$ql = $this->session->userdata('q_l');

		$mysql_query = "select package_title".$ql." as package_title , package_name".$ql." as package_name , package_description".$ql." as package_description , package_subdesc".$ql." as package_subdesc ,package_url,package_status,package_unitprice,package_big_img,package_small_img,id,c_user,m_user,created_at,package_startdate,package_enddate,updated_at,meta_author,meta_description,meta_keywords from tb_package where ";
		$mysql_query .= "id = '".$update_id."' ";
		$data['query'] = $this->_custom_query($mysql_query)->row();

		foreach($this->_custom_query($mysql_query)->result() as $rows){ 
			$data['meta_author'] = $rows->meta_author;
			$data['meta_keywords'] = $rows->meta_keywords;
			$data['meta_description'] = $rows->meta_description;
		}

		$this->load->model('Home_model');
		$data['medical_center_package'] = $this->Home_model->id_medicial_center($data['query']->relate_medical_center_id); 
		$data['doc_package'] = $this->Home_model->where_related_doctor($data['query']->relate_medical_center_id); 

		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();

		$data['get_sidemenu'] = $this->fetch_sidemenu()->result();
		$data['get_subsidemenu'] = $this->fetch_subsidemenu()->result();
		$data['get_subofsubmenu'] = $this->fetch_subofsubmenu()->result();

		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['promotionDetails'] = 'frontend/src/package/promotion_details';
		$data['footerpages'] = 'frontend/src/footerpages';

		$this->load->view('frontend/homepage', $data);
	}


	public function NavDetails()
	{
		$this->language();
		$update_id = $this->uri->segment(4);

		if(!is_numeric($update_id)){
			redirect('frontend/Home');
		}	 

		$data['update_id'] = $update_id;

		$mysql_query = "select * from tb_navigation where id='$update_id' ";
		$mysql_query .= "and nav_status > ";
		$mysql_query .= "'0' ";
		$data['query'] = $this->_custom_query($mysql_query)->row();
/*
		foreach($this->_custom_query($mysql_query)->result() as $rows){ 
			$data['meta_author'] = $rows->meta_author;
			$data['meta_keywords'] = $rows->meta_keywords;
			$data['meta_description'] = $rows->meta_description;
		}
*/
		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();

		$data['get_sidemenu'] = $this->fetch_sidemenu();
		$data['get_subsidemenu'] = $this->fetch_subsidemenu();
		$data['get_subofsubmenu'] = $this->fetch_subofsubmenu();

		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['navDetails'] = 'frontend/src/navbar/nav_details';
		$data['footerpages'] = 'frontend/src/footerpages';

		$this->load->view('frontend/homepage', $data);
	}

	public function SubNavDetails()
	{
		$this->language();
		$update_id = $this->uri->segment(4);

		if(!is_numeric($update_id)){
			redirect('frontend/Home');
		}	 

		$data['update_id'] = $update_id;

		$mysql_query = "select * from tb_subnavigation where id='$update_id' ";
		$mysql_query .= "and sub_nav_status = ";
		$mysql_query .= "'1' ";
		$data['query'] = $this->_custom_query($mysql_query)->row();
/*
		foreach($this->_custom_query($mysql_query)->result() as $rows){ 
			$data['meta_author'] = $rows->meta_author;
			$data['meta_keywords'] = $rows->meta_keywords;
			$data['meta_description'] = $rows->meta_description;
		}
*/
		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();

		$data['get_sidemenu'] = $this->fetch_sidemenu();
		$data['get_subsidemenu'] = $this->fetch_subsidemenu();
		$data['get_subofsubmenu'] = $this->fetch_subofsubmenu();

		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['subnavDetails'] = 'frontend/src/navbar/sub_nav_details';
		$data['footerpages'] = 'frontend/src/footerpages';
		$this->load->view('frontend/homepage', $data);
	}

	public function SidebarDetails()
	{
		$this->language();
		$update_id = $this->uri->segment(4);

		if(!is_numeric($update_id)){
			redirect('frontend/Home');
		}	 

		$data['update_id'] = $update_id;

		$mysql_query = "select * from tb_sidemenu where id='$update_id' ";
		$mysql_query .= "and sidemenu_status > ";
		$mysql_query .= "'0' ";
		$data['query'] = $this->_custom_query($mysql_query)->row();
/*
		foreach($this->_custom_query($mysql_query)->result() as $rows){ 
			$data['meta_author'] = $rows->meta_author;
			$data['meta_keywords'] = $rows->meta_keywords;
			$data['meta_description'] = $rows->meta_description;
		}
*/
		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();

		$data['get_sidemenu'] = $this->fetch_sidemenu();
		$data['get_subsidemenu'] = $this->fetch_subsidemenu();
		$data['get_subofsubmenu'] = $this->fetch_subofsubmenu();

		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['SidebarDetails'] = 'frontend/src/sidemenu/sidebar_details';
		$data['footerpages'] = 'frontend/src/footerpages';

		$this->load->view('frontend/homepage', $data);
	}

	public function SubSidebarDetails()
	{
		$this->language();
		$update_id = $this->uri->segment(4);

		if(!is_numeric($update_id)){
			redirect('frontend/Home');
		}	 

		$data['update_id'] = $update_id;

		$mysql_query = "select * from tb_subofsidemenu where id='$update_id' ";
		$mysql_query .= "and sub_sidemenu_status > ";
		$mysql_query .= "'0' ";
		$data['query'] = $this->_custom_query($mysql_query)->row();
/*
		foreach($this->_custom_query($mysql_query)->result() as $rows){ 
			$data['meta_author'] = $rows->meta_author;
			$data['meta_keywords'] = $rows->meta_keywords;
			$data['meta_description'] = $rows->meta_description;
		}
*/
		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();

		$data['get_sidemenu'] = $this->fetch_sidemenu();
		$data['get_subsidemenu'] = $this->fetch_subsidemenu();
		$data['get_subofsubmenu'] = $this->fetch_subofsubmenu();

		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['SubSidebarDetails'] = 'frontend/src/sidemenu/sub_sidebar_details';
		$data['footerpages'] = 'frontend/src/footerpages';

		$this->load->view('frontend/homepage', $data);
	}

	public function SubSubSidebarDetails()
	{
		$this->language();
		$update_id = $this->uri->segment(4);

		if(!is_numeric($update_id)){
			redirect('frontend/Home');
		}	 

		$data['update_id'] = $update_id;

		$mysql_query = "select * from tb_subofsubmenu where id='$update_id' ";
		$mysql_query .= "and subofsubmenu_status > ";
		$mysql_query .= "'0' ";
		$data['query'] = $this->_custom_query($mysql_query)->row();
/*
		foreach($this->_custom_query($mysql_query)->result() as $rows){ 
			$data['meta_author'] = $rows->meta_author;
			$data['meta_keywords'] = $rows->meta_keywords;
			$data['meta_description'] = $rows->meta_description;
		}
*/
		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();

		$data['get_sidemenu'] = $this->fetch_sidemenu();
		$data['get_subsidemenu'] = $this->fetch_subsidemenu();
		$data['get_subofsubmenu'] = $this->fetch_subofsubmenu();

		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['SubSubSidebarDetails'] = 'frontend/src/sidemenu/subofsubmenu_details';
		$data['footerpages'] = 'frontend/src/footerpages';

		$this->load->view('frontend/homepage', $data);
	}

	public function Service()
	{
		$this->language();
		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();

		$data['medical_center']=$this->Home_model->allmedicial_center();


		$data['get_sidemenu'] = $this->fetch_sidemenu()->result();
		$data['get_subsidemenu'] = $this->fetch_subsidemenu()->result();
		$data['get_subofsubmenu'] = $this->fetch_subofsubmenu()->result();


		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['service'] = 'frontend/src/service/All_service';
		$data['footerpages'] = 'frontend/src/footerpages';

		$this->load->view('frontend/homepage', $data);
	}

	public function ServiceDetails()
	{
		$this->language();
		$update_id = $this->uri->segment(4);

		if(!is_numeric($update_id)){
			redirect('frontend/Home/Service');
		}	 

		$data['update_id'] = $update_id;

		$mysql_query = "select * from tb_medical_center where id='$update_id' ";
		$mysql_query .= "and status = ";
		$mysql_query .= "'1' ";
		$data['query'] = $this->_custom_query($mysql_query)->row();
/*
		foreach($this->_custom_query($mysql_query)->result() as $rows){ 
			$data['meta_author'] = $rows->medical_center_meta_author;
			$data['meta_keywords'] = $rows->medical_center_meta_keywords;
			$data['meta_description'] = $rows->medical_center_meta_description;
		}
*/
		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();

		$data['get_sidemenu'] = $this->fetch_sidemenu()->result();
		$data['get_subsidemenu'] = $this->fetch_subsidemenu()->result();
		$data['get_subofsubmenu'] = $this->fetch_subofsubmenu()->result();

		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['serviceDetails'] = 'frontend/src/service/service_details';
		$data['footerpages'] = 'frontend/src/footerpages';

		$this->load->view('frontend/homepage', $data);
	}

	public function ServiceDetails_preview()
	{
		$this->language();
		$update_id = $this->uri->segment(4);

		if(!is_numeric($update_id)){
			redirect('frontend/Home/Service');
		}	 

		$data['update_id'] = $update_id;
		$data['preview'] = 'preview';
		$mysql_query = "select * from tb_medical_center where id='$update_id' ";
		$data['query'] = $this->_custom_query($mysql_query)->row();
/*
		foreach($this->_custom_query($mysql_query)->result() as $rows){ 
			$data['meta_author'] = $rows->medical_center_meta_author;
			$data['meta_keywords'] = $rows->medical_center_meta_keywords;
			$data['meta_description'] = $rows->medical_center_meta_description;
		}
*/
		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();

		$data['get_sidemenu'] = $this->fetch_sidemenu()->result();
		$data['get_subsidemenu'] = $this->fetch_subsidemenu()->result();
		$data['get_subofsubmenu'] = $this->fetch_subofsubmenu()->result();

		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['serviceDetails'] = 'frontend/src/service/service_details';
		$data['footerpages'] = 'frontend/src/footerpages';

		$this->load->view('frontend/homepage', $data);
	}
	public function ContactUS()
	{
		$this->language();
		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();

		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['contactUs'] = 'frontend/src/aboutUS/contact_us';
		$data['footerpages'] = 'frontend/src/footerpages';

		$this->load->view('frontend/homepage', $data);
	}

	public function TreatmentHistory()
	{
		$this->language();
		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();

		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['TreatmentHistory'] = 'frontend/src/b_service/treatment_history';
		$data['footerpages'] = 'frontend/src/footerpages';

		$this->load->view('frontend/homepage', $data);
	}



	public function fetch_carousel()
	{
			/*$this->db->select('*');
			$this->db->from('tb_maincarousel');
			$this->db->where('carousel_status',1);
			$this->db->order_by('created_at','ASC');

			$query = $this->db->get();*/
			$mysql_query = "select * from tb_maincarousel where carousel_status=1 order by created_at asc";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}


		public function fetch_header()
		{
			$mysql_query = "select * from tb_header where header_status=1";
			$query = $this->_custom_query($mysql_query)->row();

			return $query;
		}

		public function fetch_nav(){
			$ql = $this->session->userdata('q_l');
			$mysql_query = "select nav_title".$ql." as nav_title ,nav_url,nav_content,id
			from tb_navigation
			where nav_status=1
			order by order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}

		public function fetch_subnav(){
			$ql = $this->session->userdata('q_l');
			$mysql_query = "select tb_subnavigation.id as subid,tb_subnavigation.sub_nav_title".$ql." as sub_nav_title ,tb_subnavigation.sub_nav_url,tb_subnavigation.sub_nav_content,tb_subnavigation.parent_nav_id,
			tb_navigation.id
			from tb_subnavigation
			inner join tb_navigation
			on tb_subnavigation.parent_nav_id=tb_navigation.id
			where tb_subnavigation.sub_nav_status=1
			and tb_subnavigation.parent_nav_id=tb_navigation.id
			order by tb_subnavigation.order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}


		public function fetch_sidemenu(){

			$ql = $this->session->userdata('q_l');

			$mysql_query = "select sidemenu_title".$ql." as sidemenu_title ".",sidemenu_url,sidemenu_content,id
			from tb_sidemenu
			where sidemenu_status=1
			order by order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}

		public function fetch_subsidemenu(){

			$ql = $this->session->userdata('q_l');
		
			$mysql_query = "select parent_sidemenu_id,sub_sidemenu_title".$ql." as sub_sidemenu_title ".",sub_sidemenu_url,sub_sidemenu_content,id
			from tb_subofsidemenu
			where sub_sidemenu_status=1
			order by order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}

		public function fetch_subofsubmenu(){

			$ql = $this->session->userdata('q_l');

			$mysql_query = "select parent_submenu_id,subofsubmenu_title".$ql." as subofsubmenu_title ".",subofsubmenu_url,subofsubmenu_content,id
			from tb_subofsubmenu
			where subofsubmenu_status=1
			order by order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}

		function get_subnav($order_by)
		{
			$this->load->model('Mdl_frontend');
			$query = $this->Mdl_frontend->get_subnav($order_by);
			return $query;
		}

		function get_nav($order_by)
		{
			$this->load->model('Mdl_frontend');
			$query = $this->Mdl_frontend->get_nav($order_by);
			return $query;
		}

		function get_with_limit($limit, $offset, $order_by)
		{
			$this->load->model('Mdl_frontend');
			$query = $this->Mdl_frontend->get_with_limit($limit, $offset, $order_by);
			return $query;
		}

		function get_where_subnav($id)
		{
			$this->load->model('Mdl_frontend');
			$query = $this->Mdl_frontend->get_where_subnav($id);
			return $query;
		}

		function get_where_nav($id)
		{
			$this->load->model('Mdl_frontend');
			$query = $this->Mdl_frontend->get_where($id);
			return $query;
		}

		function get_where_custom_subnav($col, $value)
		{
			$this->load->model('Mdl_frontend');
			$query = $this->Mdl_frontend->get_where_custom_subnav($col, $value);
			return $query;
		}

		function get_where_custom_nav($col, $value)
		{
			$this->load->model('Mdl_frontend');
			$query = $this->Mdl_frontend->get_where_custom($col, $value);
			return $query;
		}

		function _insert_subnav($data)
		{
			$this->load->model('Mdl_frontend');
			$this->Mdl_frontend->_insert_subnav($data);
		}

		function _insert_nav($data)
		{
			$this->load->model('Mdl_frontend');
			$this->Mdl_frontend->_insert($data);
		}

		function _update_subnav($id, $data)
		{
			$this->load->model('Mdl_frontend');
			$this->Mdl_frontend->_update_subnav($id, $data);
		}

		function _update_nav($id, $data)
		{
			$this->load->model('Mdl_frontend');
			$this->Mdl_frontend->_update($id, $data);
		}

		function _delete_subnav($id)
		{
			$this->load->model('Mdl_frontend');
			$this->Mdl_frontend->_delete_subnav($id);
		}

		function _delete_nav($id)
		{
			$this->load->model('Mdl_frontend');
			$this->Mdl_frontend->_delete($id);
		}

		function count_where($column, $value)
		{
			$this->load->model('Mdl_frontend');
			$count = $this->Mdl_frontend->count_where($column, $value);
			return $count;
		}

		function get_max_subnav()
		{
			$this->load->model('Mdl_frontend');
			$max_id = $this->Mdl_frontend->get_max_subnav();
			return $max_id;
		}

		function get_max_nav()
		{
			$this->load->model('Mdl_frontend');
			$max_id = $this->Mdl_frontend->get_max();
			return $max_id;
		}

		function _custom_query($mysql_query)
		{
			$this->load->model('Mdl_frontend');
			$query = $this->Mdl_frontend->_custom_query($mysql_query);
			return $query;
		}


	}
