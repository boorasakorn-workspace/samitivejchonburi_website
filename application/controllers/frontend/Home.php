<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();

    $this->load->model('Front_Model');
    $this->load->model('Front_Home_Model');

    $this->load->library('session');
    $this->load->library('email');

    $config['protocol']    = 'smtp';
    $config['smtp_host']    = 'smtp.mailtrap.io';
    $config['smtp_port']    = '2525';
    $config['smtp_timeout'] = '7';
    $config['smtp_user']    = '4df5162eacf19e';
    $config['smtp_pass']    = '1a9f436700ca7d';
    $config['charset']    = 'utf-8';
    $config['crlf']    = '\r\n';
    $config['newline']    = '\r\n';
    $config['mailtype'] = 'text'; // or html
    $config['validation'] = TRUE; // bool whether to validate email or not      
    $this->email->initialize($config);
  }

  public function language()
  {
    if (empty($this->session->userdata('language'))) {
      $this->session->set_userdata("language", 'thai');
      $this->session->set_userdata("q_l", ' ');
    }
    $this->session->set_userdata("q_l", '_' . substr($this->session->userdata('language'), 0, 2));
    if ($this->session->userdata('q_l') == 'th' || $this->session->userdata('q_l') == '_th') {
      $this->session->set_userdata("q_l", ' ');
    }
    return $this->lang->load($this->session->userdata('language'), 'english');
  }

  public function change_language($lang)
  {
    $this->session->set_userdata("language", $lang);
    $this->session->set_userdata("q_l", '_' . substr($lang, 0, 2));
    if ($this->session->userdata('q_l') == 'th' || $this->session->userdata('q_l') == '_th') {
      $this->session->set_userdata("q_l", ' ');
    }
    redirect($_SERVER['HTTP_REFERER']);
  }

  public function Index()
  {

    $this->language();
    $data = $this->Front_Model->General();

    //Modal Selfregister (Auto Popup)
    $data['modal_selfregis'] = 'frontend/src/modal_self';

    //Carousel
    $data['slidecontents'] = 'frontend/src/slidecontents';
    $data['get_carousel'] = $this->Front_Home_Model->get_slide_carousel();

    //Listcontent
    $data['listcontents'] = 'frontend/src/listcontents';

    //Services
    $data['services'] = 'frontend/src/services';
    $data['medical_center'] = $this->Front_Home_Model->pre_medicial_center();

    //Package
    $data['package'] = 'frontend/src/package';
    $data['get_package'] = $this->Front_Home_Model->pre_package();

    //Bloghealthy
    $data['bloghealthy'] = 'frontend/src/bloghealthy';
    if (!(count($this->Front_Home_Model->pre_bloghealthy()) < 2)) {
      $data['blog_healthy'] = $this->Front_Home_Model->pre_bloghealthy();
    }

    //Recommend Doctor
    $data['recommend_doctor'] = 'frontend/src/recommend_doctor';
    $data['doctor'] = $this->Front_Home_Model->pre_doctor();
    $data['all_med_doctor'] = $this->Front_Home_Model->lang_all_medicial_center();

    //News and activity //Blogcontents
    $data['blogcontents'] = 'frontend/src/blogcontents';
    if (!(count($this->Front_Home_Model->pre_news()) < 2)) {
      $data['new_activity'] = $this->Front_Home_Model->pre_news();
    } else {
      $data['new_activity'] = array();
    }


    $this->load->view('frontend/homepage', $data);
  }
  public function Preview_Page()
  {
    $this->language();

    $data = $this->Front_Model->Detailpage();

    $this->load->view('frontend/homepage', $data);
  }
  //Service
  public function Service()
  {

    $this->language();
    $data = $this->Front_Model->Detailpage();

    $data['medical_center'] = $this->Front_Home_Model->lang_all_medicial_center();
    $data['service'] = 'frontend/src/service/All_service';

    $this->load->view('frontend/homepage', $data);
  }

  public function ServiceDetails()
  {
    $this->language();

    $update_id = $this->uri->segment(4);
    $redirectParent = 'frontend/Home/Service';
    $mainTable = 'ms_service_location';
    $this->_check_Available_self($update_id, $redirectParent, $mainTable, 'uid', 'status');

    $data = $this->Front_Model->Detailpage();

    $data['update_id'] = $update_id;
    $data['query'] = $this->Front_Home_Model->id_medicial_center($update_id);
    $data['doctor_related'] = $this->Front_Home_Model->doctor_related_medcenter($update_id);
    $data['serviceDetails'] = 'frontend/src/service/service_details';

    $this->load->view('frontend/homepage', $data);
  }

  //Package
  public function Promotion()
  {
    $this->language();
    $this->load->helper('text');

    $data = $this->Front_Model->General();

    $data['query'] = $this->Front_Home_Model->all_promotion();
    $data['promotion'] = 'frontend/src/package/All_promotion';

    $this->load->view('frontend/homepage', $data);
  }

  public function PromotionDetails()
  {
    $this->language();

    $update_id = $this->uri->segment(4);
    $redirectParent = 'frontend/Home/Promotion';
    $mainTable = 'tb_package';
    $this->_check_Available($update_id, $redirectParent, $mainTable, 'id', 'package_status');

    $data = $this->Front_Model->Detailpage();

    $data['update_id'] = $update_id;
    $data['query'] = $this->Front_Home_Model->id_promotion($update_id);
    $data['promotionDetails'] = 'frontend/src/package/promotion_details';

    $data['medical_center_package'] = $this->Front_Home_Model->id_medicial_center($data['query']->relate_medical_center_id);
    $data['doc_package'] = $this->Front_Home_Model->get_related_doctor_id($data['query']->relate_medical_center_id);

    $this->load->view('frontend/homepage', $data);
  }

  //BlogHealthy
  public function HealthyBlog()
  {
    $this->language();
    $this->load->helper('text');

    $data = $this->Front_Model->General();
    $id = $this->input->post('search_blog_doctor');
    $data['query'] = $this->Front_Home_Model->all_bloghealthy($id);

    $data['healthyBlog'] = 'frontend/src/healthyblog/All_healthyblog';

    $data['get_doctor'] = $this->Front_Home_Model->all_doctor();
    $data['all_med_doctor'] = $this->Front_Home_Model->lang_all_medicial_center();

    $this->load->view('frontend/homepage', $data);
  }

  public function get_blog_doctor_id()
  {
    $this->language();
    $this->load->helper('text');
    $id = $this->input->post('search_blog_doctor');

    $data = $this->Front_Model->General();

    $data['query'] = $this->Front_Home_Model->all_bloghealthy($id);
    // $data['query'] = $this->Front_Home_Model->get_blog_doctor($id);

    $data['healthyBlog'] = 'frontend/src/healthyblog/All_healthyblog';

    $data['get_doctor'] = $this->Front_Home_Model->all_doctor();
    $data['all_med_doctor'] = $this->Front_Home_Model->lang_all_medicial_center();

    $this->load->view('frontend/homepage', $data);

    // var_dump($data);
    // $data['healthyBlog'] = 'frontend/src/healthyblog/All_healthyblog';
  }

  public function HealthyblogDetails()
  {
    $this->language();

    $update_id = $this->uri->segment(4);
    $redirectParent = 'frontend/Home/HealthyBlog';
    $mainTable = 'tb_bloghealthy';
    $this->_check_Available($update_id, $redirectParent, $mainTable, 'id', 'blog_status');

    $data = $this->Front_Model->Detailpage();

    $data['update_id'] = $update_id;
    $data['query'] = $this->Front_Home_Model->id_bloghealthy($update_id);
    $data['healthyblogDetails'] = 'frontend/src/healthyblog/healthyblog_details';

    $data['get_doctor'] = $this->Front_Home_Model->id_doctor($data['query']->doc_id);
    $data['all_med_doctor'] = $this->Front_Home_Model->lang_all_medicial_center();

    if (isset($data['query']->promotion_link_1) && $data['query']->promotion_link_1 > 0) {
      $data['promotion_1'] = $this->Front_Home_Model->id_promotion($data['query']->promotion_link_1);
    }
    if (isset($data['query']->promotion_link_2) && $data['query']->promotion_link_2 > 0) {
      $data['promotion_2'] = $this->Front_Home_Model->id_promotion($data['query']->promotion_link_2);
    }

    $this->load->view('frontend/homepage', $data);
  }

  //Recommend Doctor
  public function OurDoctors()
  {
    redirect('https://express-apps.com/postview/selfregis_chonburi/doctor');


    $this->language();
    $this->load->helper('text');

    $data = $this->Front_Model->General();

    $data['alldoctor'] = $this->Front_Home_Model->all_doctor();
    $data['ourDoctors'] = 'frontend/src/doctor/our_doctors';

    $data['all_med_doctor'] = $this->Front_Home_Model->lang_all_medicial_center();
    $data['all_specialty'] = $this->Front_Home_Model->lang_all_specialty();
    $data['all_sub_specialty'] = $this->Front_Home_Model->lang_all_sub_specialty();

    $this->load->view('frontend/homepage', $data);
  }



  public function DoctorDetails()
  {
    $this->language();

    $update_id = $this->uri->segment(4);
    redirect("https://express-apps.com/postview/selfregis_chonburi/Welcome/medical_page/$update_id");
    $redirectParent = 'frontend/Home/OurDoctors';
    $mainTable = 'tb_doctor';
    $this->_check_Available($update_id, $redirectParent, $mainTable, 'id', 'status');

    $data = $this->Front_Model->Detailpage();

    $data['update_id'] = $update_id;
    $data['query'] = $this->Front_Home_Model->id_doctor($update_id);
    $data['doctorDetail'] = 'frontend/src/doctor/doctor_details';

    $data['query_workdayhour'] = $this->Front_Home_Model->id_doctor_workdayhour($data['query']->medical_id);
    $data['doc_content'] = $this->Front_Home_Model->id_doctor_bloghealthy($update_id);
    $data['all_med_doctor'] = $this->Front_Home_Model->lang_all_medicial_center();
    $data['all_specialty'] = $this->Front_Home_Model->lang_all_specialty();
    $data['all_sub_specialty'] = $this->Front_Home_Model->lang_all_sub_specialty();

    $this->load->view('frontend/homepage', $data);
  }

  //News and Activity
  public function News()
  {
    $this->language();

    $data = $this->Front_Model->Detailpage();


    $ql = $this->session->userdata('q_l');
    $data['query'] = $this->Front_Home_Model->all_news();
    $data['news'] = 'frontend/src/NewsAndEvents/All_news';

    $this->load->view('frontend/homepage', $data);
  }

  public function NewsDetails()
  {
    $this->language();

    $update_id = $this->uri->segment(4);
    $redirectParent = 'frontend/Home/News';
    $mainTable = 'new_activity';
    $this->_check_Available($update_id, $redirectParent, $mainTable, 'new_activity_id', 'new_activity_status');

    $data = $this->Front_Model->Detailpage();

    $data['update_id'] = $update_id;
    $data['query'] = $this->Front_Home_Model->id_news($update_id);
    $data['newsDetails'] = 'frontend/src/NewsAndEvents/news_details';

    $this->load->view('frontend/homepage', $data);
  }

  public function ContactUS()
  {
    $this->language();

    $data = $this->Front_Model->General();

    $data['contactUs'] = 'frontend/src/aboutUS/contact_us';

    $this->load->view('frontend/homepage', $data);
  }

  public function NavDetails()
  {
    $this->language();

    $update_id = $this->uri->segment(4);
    $redirectParent = 'frontend/Home';
    $mainTable = 'tb_navigation';
    $this->_check_Available($update_id, $redirectParent, $mainTable, 'id', 'nav_status');

    $data = $this->Front_Model->Detailpage();

    $data['update_id'] = $update_id;
    $data['query'] = $this->Front_Home_Model->id_nav($update_id);
    $data['navDetails'] = 'frontend/src/navbar/nav_details';

    $this->load->view('frontend/homepage', $data);
  }

  public function SubNavDetails()
  {
    $this->language();

    $update_id = $this->uri->segment(4);
    $redirectParent = 'frontend/Home';
    $mainTable = 'tb_subnavigation';
    $this->_check_Available($update_id, $redirectParent, $mainTable, 'id', 'sub_nav_status');

    $data = $this->Front_Model->Detailpage();

    $data['update_id'] = $update_id;
    $data['query'] = $this->Front_Home_Model->id_subnav($update_id);
    $data['subnavDetails'] = 'frontend/src/navbar/sub_nav_details';

    $this->load->view('frontend/homepage', $data);
  }

  public function SidebarDetails()
  {
    $this->language();

    $update_id = $this->uri->segment(4);
    $redirectParent = 'frontend/Home';
    $mainTable = 'tb_sidemenu';
    $this->_check_Available($update_id, $redirectParent, $mainTable, 'id', 'sidemenu_status');

    $data = $this->Front_Model->Detailpage();

    $data['update_id'] = $update_id;
    $data['query'] = $this->Front_Home_Model->id_sidebar($update_id);
    $data['SidebarDetails'] = 'frontend/src/sidemenu/sidebar_details';

    $this->load->view('frontend/homepage', $data);
  }

  public function SubSidebarDetails()
  {
    $this->language();

    $update_id = $this->uri->segment(4);
    $redirectParent = 'frontend/Home';
    $mainTable = 'tb_subofsidemenu';
    $this->_check_Available($update_id, $redirectParent, $mainTable, 'id', 'sub_sidemenu_status');

    $data = $this->Front_Model->Detailpage();

    $data['update_id'] = $update_id;
    $data['query'] = $this->Front_Home_Model->id_subsidebar($update_id);
    $data['SubSidebarDetails'] = 'frontend/src/sidemenu/sub_sidebar_details';

    $this->load->view('frontend/homepage', $data);
  }

  public function SubSubSidebarDetails()
  {
    $this->language();

    $update_id = $this->uri->segment(4);
    $redirectParent = 'frontend/Home';
    $mainTable = 'tb_subofsubmenu';
    $this->_check_Available($update_id, $redirectParent, $mainTable, 'id', 'subofsubmenu_status');

    $data = $this->Front_Model->Detailpage();

    $data['update_id'] = $update_id;
    $data['query'] = $this->Front_Home_Model->id_sub_subsidebar($update_id);
    $data['SubSubSidebarDetails'] = 'frontend/src/sidemenu/subofsubmenu_details';

    $this->load->view('frontend/homepage', $data);
  }

  public function DoctorAppointment()
  {
    $this->language();

    $data = $this->Front_Model->Detailpage();

    $data['all_doctor'] = $this->Front_Home_Model->all_doctor();
    $data['all_med_doctor'] = $this->Front_Home_Model->lang_all_medicial_center();
    $data['all_specialty'] = $this->Front_Home_Model->lang_all_specialty();
    $data['all_sub_specialty'] = $this->Front_Home_Model->lang_all_sub_specialty();
    $data['doctorAppointment'] = 'frontend/src/doctor/doctor_appointments';

    $this->load->view('frontend/homepage', $data);
  }

  public function Full_Appointment_Email_Send()
  {
    $this->language();

    if ($this->input->post('firstname', TRUE) != '' && $this->input->post('description', TRUE) != '') {
      $appointment_subject = $this->input->post('appointment_title_text', TRUE);
      $doctor_name = $this->input->post('doctor_medical_id', TRUE);
      $patient_name = $this->input->post('firstname', TRUE);
      $patient_surname = $this->input->post('lastname', TRUE);
      $patient_birth = $this->input->post('birthday', TRUE);
      $patient_gender = $this->input->post('gender', TRUE);
      $patient_email = $this->input->post('form_email', TRUE);
      $patient_phone = $this->input->post('form_phone', TRUE);
      $patient_info_message = $this->input->post('description', TRUE);
      $appointment_date = $this->input->post('appointment_date', TRUE);
      $appointment_sdate = $this->input->post('appointment_sdate', TRUE);

      $this->email->from('samitivej-chonburi@samitivej.com', 'Samitiveh Chonburi');
      $to = $patient_email;
      $subject = 'No reply : ' . $appointment_subject;
      $message = '\r\n' . 'Patient Name : ' . $patient_name . ' ' . $patient_surname . '\r\n' .
        'Birthday : ' . $patient_birth . '\r\n' .
        'Appointment Info :' . $patient_info_message . '\r\n' .
        'Appointment Date :' . $appointment_date . '\r\n';
      if (!empty($appointment_sdate)) {
        $message .= 'Second Appointment Date :' . $appointment_sdate . '\r\n';
      }
      if (!empty($patient_email) || !empty($patient_phone)) {
        $message .= 'Contact :' . '\r\n';
        if (!empty($patient_email)) {
          $message .= ' - Email :' . $patient_email . '\r\n';
        }
        if (!empty($patient_phone)) {
          $message .= ' - Phone :' . $patient_phone;
        }
      }
      $data['email_from'] = 'samitivej-chonburi@samitivej.com';
      $data['email_target'] = $to;
      $data['email_subject'] = $subject;
      $data['email_message'] = str_replace('\r\n', '<br>', $message);
      $data['email_status'] = 1;
      $data['cuser'] = 0;

      $this->Front_Home_Model->_insert_email($data);

      $this->email->to($to);
      $this->email->subject($subject);
      $this->email->message($message);

      $this->email->send();
      redirect($_SERVER['HTTP_REFERER']);
    }
  }

  public function Appointment_Email_Send()
  {
    $this->language();
    if ($this->input->post('firstname', TRUE) != '' && $this->input->post('form_message', TRUE) != '') {
      $appointment_subject = $this->input->post('appointment_title_text', TRUE);
      $doctor_name = $this->input->post('doc_fullname', TRUE);
      $patient_name = $this->input->post('firstname', TRUE);
      $patient_surname = $this->input->post('lastname', TRUE);
      $patient_birth = $this->input->post('birthday', TRUE);
      $patient_gender = $this->input->post('gender', TRUE);
      $patient_email = $this->input->post('form_email', TRUE);
      $patient_phone = $this->input->post('form_phone', TRUE);
      $patient_info_message = $this->input->post('form_message', TRUE);
      $appointment_date = $this->input->post('appointment_date', TRUE);
      $appointment_time = $this->input->post('appointment_time', TRUE);
      $appointment_sdate = $this->input->post('appointment_sdate', TRUE);
      $appointment_stime = $this->input->post('appointment_time', TRUE);

      $this->email->from('samitivej-chonburi@samitivej.com', 'Samitiveh Chonburi');
      $to = $patient_email;
      $subject = 'No reply : ' . $appointment_subject;
      $message = '\r\n' . 'Patient Name : ' . $patient_name . ' ' . $patient_surname . '\r\n' .
        'Birthday : ' . $patient_birth . '\r\n' .
        'Appointment Info :' . $patient_info_message . '\r\n' .
        'Appointment Date :' . $appointment_date . ' : ' . $appointment_time . '\r\n';
      if (!empty($appointment_sdate)) {
        $message .= 'Second Appointment Date : ' . $appointment_sdate . ' : ' . $appointment_stime . '\r\n';
      }
      if (!empty($patient_email) || !empty($patient_phone)) {
        $message .= 'Contact :' . '\r\n';
        if (!empty($patient_email)) {
          $message .= ' - Email : ' . $patient_email . '\r\n';
        }
        if (!empty($patient_phone)) {
          $message .= ' - Phone : ' . $patient_phone;
        }
      }

      $data['email_from'] = 'samitivej-chonburi@samitivej.com';
      $data['email_target'] = $to;
      $data['email_subject'] = $subject;
      $data['email_message'] = str_replace('\r\n', '<br>', $message);
      $data['email_status'] = 1;
      $data['cuser'] = 0;

      $this->Front_Home_Model->_insert_email($data);

      $this->email->to($to);
      $this->email->subject($subject);
      $this->email->message($message);

      $this->email->send();
    }
    redirect($_SERVER['HTTP_REFERER']);
  }

  //Additional Function
  function _check_Available($update_id, $redirect, $tableCheck, $idCustom = 'id', $statusCustom = 'status')
  {
    $this->load->model('Check_Model');
    if (!is_numeric($update_id)) {
      redirect($redirect);
    } else if (!($this->Check_Model->Available($tableCheck, $update_id, $idCustom, $statusCustom))) {
      redirect($redirect);
    }
  }

  function _check_Available_self($update_id, $redirect, $tableCheck, $idCustom = 'uid', $statusCustom = 'status')
  {
    $this->load->model('Check_Model');
    if (!is_numeric($update_id)) {
      redirect($redirect);
    } else if (!($this->Check_Model->AvailableSelf($tableCheck, $update_id, $idCustom, $statusCustom))) {
      redirect($redirect);
    }
  }

  function _custom_query($mysql_query)
  {
    $query = $this->Front_Model->_custom_query($mysql_query);
    return $query;
  }
}
