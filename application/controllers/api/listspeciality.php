<?php
defined('BASEPATH') or exit('No direct script access allowed');
require(APPPATH . '/libraries/RestController.php');
require(APPPATH . '/libraries/Format.php');

use chriskacerguis\RestServer\RestController;

class listspeciality extends RestController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('API_MDL');
    }

    public function index_get()
    {
        $id = $this->get('id');
        $list = $this->get('list');
        switch ($list) {
            case 'clinic':
                $data = $this->API_MDL->get_list_clinic($id);
                break;
            case 'specialty':
                $data = $this->API_MDL->get_list_specialty($id);
                break;
            case 'subspecialty':
                $data = $this->API_MDL->get_list_subspecialty($id);
                break;            
            default:
                $this->response([
                    'status' => false,
                    'message' => "No request lists"
                ], 404);
                return false;
                break;
        }
        if($data){
            $this->response($data, 200);
        }else{
            if($id){
                $response = [
                    'status' => false,
                    'message' => "No such $list list found"
                ];
            }else{
                $response = [
                    'status' => false,
                    'message' => "No $list lists were found"
                ];
            }
            $this->response($response, 404);
        }

    }
    
    public function index_post()
    {

    }
 
    public function index_put()
    {

    }
 
    public function index_delete()
    {

    }
}
