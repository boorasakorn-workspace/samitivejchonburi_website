<?php
defined('BASEPATH') or exit('No direct script access allowed');
require(APPPATH . '/libraries/RestController.php');
require(APPPATH . '/libraries/Format.php');

use chriskacerguis\RestServer\RestController;

class careprovider extends RestController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('API_MDL');
    }

    public function index_get()
    {
        $medical = $this->get('medical');
        $specialty = $this->get('specialty');
        $subspecialty = $this->get('subspecialty');

        if($medical){
            $data = $this->API_MDL->get_careprovider_med($medical);
        }else if($specialty){
            $data = $this->API_MDL->get_careprovider_sp($specialty);
        }else if($subspecialty){
            $data = $this->API_MDL->get_careprovider_subsp($subspecialty);
        }else{
            $data = NULL;
        }
        if($data){
            $this->response($data, 200);
        }else{
            $response = [
                'status' => false,
                'message' => 'No careproviders were found'
            ];
            $this->response($response, 404);
        }
    }
    
    public function index_post()
    {
        $input = $this->input->post();
        $data = $this->API_MDL->get_careprovider($input);
        if($data){
            $this->response($data, 200);
        }else{
            $response = [
                'status' => false,
                'message' => 'No careproviders were found'
            ];
            $this->response($response, 404);
        }
    }
 
    public function index_put()
    {

    }
 
    public function index_delete()
    {

    }
}
