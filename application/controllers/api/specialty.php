<?php
defined('BASEPATH') or exit('No direct script access allowed');
require(APPPATH . '/libraries/RestController.php');
require(APPPATH . '/libraries/Format.php');

use chriskacerguis\RestServer\RestController;

class specialty extends RestController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('API_MDL');
    }

    public function index_get()
    {
        $id = $this->get('id');

        $data = $this->API_MDL->get_specialty($id);
        if($data){
            $this->response($data, 200);
        }else{
            if($id){
                $response = [
                    'status' => false,
                    'message' => 'No such specialty found'
                ];
            }else{
                $response = [
                    'status' => false,
                    'message' => 'No specialties were found'
                ];
            }
            $this->response($response, 404);
        }

    }
    
    public function index_post()
    {

    }
 
    public function index_put()
    {

    }
 
    public function index_delete()
    {

    }
}
