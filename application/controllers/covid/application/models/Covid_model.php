<?php 

class Covid_model extends CI_Model{

   public function get_question($lang){
      $this->db->select('*');
      $this->db->from('covid_survey');
      $this->db->where('lang',$lang);
      $this->db->order_by('uid','asc');
      $covid_survey=$this->db->get();
      return $covid_survey->result();
   }
   public function get_province(){
    $this->db->select('*');
    $this->db->from('ms_provinces');
    $this->db->order_by('PROVINCE_NAME','asc');
    $query=$this->db->get();
    return $query->result();
 }

 public function save_data($data){
   $this->db->insert('covid_ip', $data);
}

public function save_poller($data){


   $this->db->insert('covid_poller', $data);

}
public function save_poller_question($data){


   $this->db->insert('covid_poller_question', $data);

}

public function get_province_query($PROVINCE_ID)
{
   $query = $this->db->get_where('ms_amphures', array('PROVINCE_ID' => $PROVINCE_ID));
   $this->db->order_by('PROVINCE_NAME=','asc');
   return $query->result();
}


}