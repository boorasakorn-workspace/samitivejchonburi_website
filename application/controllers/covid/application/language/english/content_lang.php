<?php

defined('BASEPATH') OR exit('No direct script access allowed');
$lang['hospital'] = 'Samitivej Chonburi Hospital';
$lang['poll_txt']='RISK ASSESSMENT FORM BEFORE HOSPITAL VISIT FOR COVID-19 INFECTION';
$lang['welcome'] = '14 days before , Did you have any risk below ?';
$lang['message'] = 'Please answer the questions truthfully';
$lang['choice1']='Yes';
$lang['choice2']='No';

$lang['poll_save']='Submit';


$lang['eng']='This self assessment tool is for encouraging people to be more aware of their self protection from health risks and reducing unnecessary hospital visits';


$lang['symptom']='Please choose symptom(s) made you decide to visit the hospital.';

$lang['symptom1']='Fever';
$lang['symptom2']='Cough';
$lang['symptom3']='Running nose';
$lang['symptom4']='Sore throat';
$lang['symptom5']='Phlegm';
$lang['symptom6']='Difficult to breath';

$lang['symptom7']='No abnormal symptoms';