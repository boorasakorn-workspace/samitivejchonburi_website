<?php

defined('BASEPATH') OR exit('No direct script access allowed');
$lang['hospital']='醫院SAMITIVEJ CHONBURI , 曼谷 泰国';
$lang['welcome'] = 'ในช่วง 14 วันก่อนหน้า ท่านมีประวัติเสี่ยงต่อการติดเชื้อ หรือไม่ดังนี้';
$lang['poll_txt'] = '2019年新型冠状病毒感染医院就诊前的风险评估表';
$lang['message']='请如实回答下列问题';
$lang['welcome']=' 在生病前的14天内, 您接触风险';
$lang['form_title']='
自我评估工具是用于鼓励人们保护自己免受疾病风险和减少不必要的医院就诊。';
$lang['choice1']='是';
$lang['choice2']='不是';

$lang['poll_save']='Submit';

$lang['symptom']='您目前没有接触 2019年新型冠状病毒的风险。  请继续评估表。  主要症状';

$lang['symptom1']='发烧';

$lang['symptom2']='咳嗽';
$lang['symptom3']='流鼻涕';
$lang['symptom4']='喉咙痛';
$lang['symptom5']='有痰';
$lang['symptom6']='呼吸困难';
$lang['symptom7']='无异常症状';


