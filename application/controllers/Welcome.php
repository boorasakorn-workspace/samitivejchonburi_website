<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct(){
		parent::__construct();
	}

	public function covid($language=null)
	{

      if($language==null){
         $language='thai';
      }
      $this->session->set_userdata('site_lang',  "thai");

      $this->load->model('Covid_model');
      $data['ques']=$this->Covid_model->get_question($language);
      $data['province_list']=$this->Covid_model->get_province();
      // echo '<pre>';
      // var_dump($data['province_list']);
      // echo '</pre>';
      $this->load->view('welcome_message',$data);
   }

   public function switchLang($language = "") {
      $this->session->set_userdata('site_lang', $language);
      $this->load->model('Covid_model');
      $data['ques']=$this->Covid_model->get_question($language);
      $data['province_list']=$this->Covid_model->get_province();
      $this->load->view('welcome_message',$data);
      // header('Location: http://27.254.59.21:8080/postview/covid/Welcome/covid/'.$language);
   }

   public function save_poll(){
      $this->load->model('Covid_model');
      $ip = array(
         'ip_address' => $this->input->post('ip'),
      );
      $this->Covid_model->save_data($ip);
      if($this->db->affected_rows() > 0){
         $id=$this->db->insert_id();
         $poller = array(
            'ip_address_uid' => $this->input->post('ip'),
            'name' => $this->input->post('name'),
            'line' => $this->input->post('line'),
            'age' => $this->input->post('age'),
            'point'=>$this->input->post('point_sum'),
            'province_id' => $this->input->post('province'),
            'amphure_id' => $this->input->post('amphure'),
         );
         $this->Covid_model->save_poller($poller);
         if($this->db->affected_rows() > 0){
            $id=$this->db->insert_id();
            for($i=0;$i<count($_POST["queustion"]);$i++)
            {
               // echo "queustion $i = ".$_POST["queustion"][$i]."<br>";
         // echo "answer $i = ".$_POST["answer".$i]."<br>";
               // $a=$this->input->post('answer'.$i);
               // echo $a;
               $poller_question = array(
                  'poller_uid' => $id,
                  'question_uid' => $_POST["queustion"][$i],
                  'answer'=>$this->input->post('answer'.$i),
               );
               $result =  $this->Covid_model->save_poller_question($poller_question);
               if($this->input->post('answer'.$i)=='Y'){
               }else{
               }
            }
         }
      }
      // echo '<pre>';
      // var_dump($poller);
      // echo '</pre>';
      // exit();
      // echo json_decode($result);
      $this->load->view('print',$poller);
   }


   public function get_amphur()
   {
      $this->load->model('Covid_model');
      $PROVINCE_ID = $this->input->post('province');
      $provinces = $this->Covid_model->get_province_query($PROVINCE_ID);
      if (count($provinces) > 0) {
         $pro_select_box = '';
         $pro_select_box .= '<option value="">เลือกอำเภอ</option>';
         foreach ($provinces as $province) {
            $pro_select_box .= '<option value="' . $province->AMPHUR_ID . '">' . $province->AMPHUR_NAME . '</option>';
         }
         echo json_encode($pro_select_box);
      }
   }

   public function print(){
      $this->load->view('print');
   }

}
