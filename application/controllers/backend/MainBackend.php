<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MainBackend extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Mdl_site_security');
		$this->Mdl_site_security->_make_sure_is_admin();

		$this->load->library('session');
        $this->load->library('email');

	    $config['protocol']    = 'smtp';
	    $config['smtp_host']    = 'smtp.mailtrap.io';
	    $config['smtp_port']    = '2525';
	    $config['smtp_timeout'] = '7';
	    $config['smtp_user']    = '4df5162eacf19e';
	    $config['smtp_pass']    = '1a9f436700ca7d';
	    $config['charset']    = 'utf-8';
	    $config['crlf']    = "\r\n";
	    $config['newline']    = "\r\n";
	    $config['mailtype'] = 'text'; // or html
	    $config['validation'] = TRUE; // bool whether to validate email or not      
	    $this->email->initialize($config);
	}

	public function Index_preview()
	{
        $this->session->set_flashdata('preview', 'preview');
        redirect('frontend/Home/Index');
	}

	public function Preview_Page()
	{
		$update_id = $this->uri->segment(4);
        $this->session->set_flashdata('preview', 'preview');
        redirect('frontend/Home/Preview_Page/');
	}

	//Service_preview
	public function ServiceDetails()
	{
		$update_id = $this->uri->segment(4);
        $this->session->set_flashdata('preview', 'preview');
        redirect('frontend/Home/ServiceDetails/'.$update_id);
	}

	//Package_preview
	public function PromotionDetails()
	{
		$update_id = $this->uri->segment(4);
        $this->session->set_flashdata('preview', 'preview');
        redirect('frontend/Home/PromotionDetails/'.$update_id);
	}

	//BlogHealthy_preview
	public function HealthyblogDetails()
	{
		$update_id = $this->uri->segment(4);
        $this->session->set_flashdata('preview', 'preview');
        redirect('frontend/Home/HealthyblogDetails/'.$update_id);
	}

	//Recommend Doctor_preview
	public function DoctorDetails()
	{
		$update_id = $this->uri->segment(4);
        $this->session->set_flashdata('preview', 'preview');
        redirect('frontend/Home/DoctorDetails/'.$update_id);
	}

	//News and Activity_preview
	public function NewsDetails()
	{
		$update_id = $this->uri->segment(4);
        $this->session->set_flashdata('preview', 'preview');
        redirect('frontend/Home/NewsDetails/'.$update_id);
	}

	//Navbar And Sidebar_preview
	public function NavDetails()
	{
		$update_id = $this->uri->segment(4);
        $this->session->set_flashdata('preview', 'preview');
        redirect('frontend/Home/NavDetails/'.$update_id);
	}

	public function SubNavDetails()
	{
		$update_id = $this->uri->segment(4);
        $this->session->set_flashdata('preview', 'preview');
        redirect('frontend/Home/SubNavDetails/'.$update_id);
	}

	public function SidebarDetails()
	{
		$update_id = $this->uri->segment(4);
        $this->session->set_flashdata('preview', 'preview');
        redirect('frontend/Home/SidebarDetails/'.$update_id);
	}

	public function SubSidebarDetails()
	{
		$update_id = $this->uri->segment(4);
        $this->session->set_flashdata('preview', 'preview');
        redirect('frontend/Home/SubSidebarDetails/'.$update_id);
	}

	public function SubSubSidebarDetails()
	{
		$update_id = $this->uri->segment(4);
        $this->session->set_flashdata('preview', 'preview');
        redirect('frontend/Home/SubSubSidebarDetails/'.$update_id);
	}

	//Additional Function
	function _custom_query($mysql_query)
	{
		$query = $this->Front_Model->_custom_query($mysql_query);
		return $query;
	}


}
