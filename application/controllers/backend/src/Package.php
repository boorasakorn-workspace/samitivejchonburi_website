<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Package extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->model('Mdl_site_security');
		$this->Mdl_site_security->_make_sure_is_admin();

		$this->load->library('session');

		$this->load->model('Back_Model');
	}

	function view_package_details($update_id)
	{			
		redirect('backend/MainBackend/PromotionDetails/'.$update_id);
	}

	public function language()
	{
		if(empty($this->session->userdata('language'))){
			$this->session->set_userdata("language", 'thai');
			$this->session->set_userdata("q_l", ' ');
		}
		$this->session->set_userdata("q_l", '_'.substr($this->session->userdata('language'), 0,2));
		if($this->session->userdata('q_l') == 'th' || $this->session->userdata('q_l') == '_th' ){
			$this->session->set_userdata("q_l", ' ');			
		}
		return $this->lang->load($this->session->userdata('language'), 'english');			
	}

	public function change_language($lang)
	{
		$this->session->set_userdata("language", $lang);
		$this->session->set_userdata("q_l", '_'.substr($lang, 0,2));
		if($this->session->userdata('q_l') == 'th' || $this->session->userdata('q_l') == '_th' ){
			$this->session->set_userdata("q_l", ' ');			
		}
		redirect($_SERVER['HTTP_REFERER']);
	}

	function preview_Contents()
	{			
		$this->language();	
		
		$this->load->model('Front_Model');
		$this->load->model('Front_Home_Model');
		$data = $this->Front_Model->Detailpage();

		$data['query'] = json_decode(json_encode($this->input->post()));
		$data['promotionDetails'] = 'frontend/src/package/promotion_details';

		$data['medical_center_package'] = $this->Front_Home_Model->id_medicial_center($data['query']->relate_medical_center_id); 
		$data['doc_package'] = $this->Front_Home_Model->get_related_doctor_id($data['query']->relate_medical_center_id); 

		$data['previewContent'] = "previewContent";
		$this->load->view('frontend/homepage', $data);
	}

	//////// Process Delete All 1.Colours 2.Sizes 3.Big Image 4.Small Image 5.Data Record
	function _process_delete($update_id)
	{

		// Delete Package Logo Big & Small Image
		$data = $this->fetch_data_from_db($update_id);
		$big_imgs = $data['package_big_img'];
		$small_imgs = $data['package_small_img'];

		$big_imgs_path = './gallery/package/big_imgs/'.$big_imgs;
		$small_imgs_path = './gallery/package/small_imgs/'.$small_imgs;

			// Remove the images
			if(file_exists($big_imgs_path)){
				unlink($big_imgs_path);
			}
			if(file_exists($small_imgs_path)){
				unlink($small_imgs_path);
			}

		// Delete Items Record from tb_package database
		$this->_delete($update_id);
	}

	//////// Delete Package Process from controller backend/src/Package/delete_package
	function delete_package_process($update_id)
	{
		if(!is_numeric($update_id)){
			redirect('backend/src/Package/manage');
		}

		$submit = $this->input->post('submit', TRUE);

		if($submit=="Cancel"){
			redirect('backend/src/Package/create/'.$update_id);
		}elseif($submit=="Yes - Delete Package"){
			$this->_process_delete($update_id);

			$flash_msg = "Deleted Package was Successfully";
			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
			$this->session->set_flashdata('item', $value);

			redirect('backend/src/Package/manage');
		}

	}

	//////// Delete Package Page
	function delete_package($update_id)
	{
		if(!is_numeric($update_id)){
			redirect('backend/src/Package/manage');
		}

		$data['headline'] = "Delete Package";
		$data['update_id'] = $update_id;
		
		$data['content'] = 'backend/package/delete_package';

		$data = $this->Back_Model->General($data);
		$this->load->view('backend/dashboard', $data);
	}

	//////// Delete Image Thumbnail
	function delete_image($update_id)
	{

		if(!is_numeric($update_id)){
			redirect('backend/src/Package/manage');
		}

		$data = $this->fetch_data_from_db($update_id);
		$big_imgs = $data['package_big_img'];
		$small_imgs = $data['package_small_img'];

		$big_imgs_path = './gallery/package/big_imgs/'.$big_imgs;
		$small_imgs_path = './gallery/package/small_imgs/'.$small_imgs;

		// Remove the images
		if(file_exists($big_imgs_path)){
			unlink($big_imgs_path);
		}
		if(file_exists($small_imgs_path)){
			unlink($small_imgs_path);
		}

		// Update Remove the images to database
		unset($data);
		$data['package_big_img'] = "";
		$data['package_small_img'] = "";
		$this->_update($update_id, $data);

		$flash_msg = "The image was successfully deleted."; // Alert message
		$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
		$this->session->set_flashdata('item', $value); // สร้าง session alert

		redirect('backend/src/Package/create/'.$update_id);
	}

	//////// Set Image Thumbnail
	function _generate_thumbnail($file_name)
	{
		$config['image_library'] = 'gd2';
		$config['source_image'] = './gallery/package/big_imgs/'.$file_name;
		$config['new_image'] = './gallery/package/small_imgs/'.$file_name;
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = TRUE;
		$config['width']         = 360;
		$config['height']       = 208;

		$this->load->library('image_lib', $config);

		$this->image_lib->resize();
	}

	//////// Upload Image Package Do Upload
	function do_upload($update_id)
	{

		if(!is_numeric($update_id)){
			redirect('backend/src/Package/manage');
		}

		$submit = $this->input->post('submit', TRUE);
		if($submit == "Cancel"){
			redirect('backend/src/Package/create/'.$update_id);
		}

		$config['upload_path'] = './gallery/package/big_imgs/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = 200;
		$config['max_width'] = 1500;
		$config['max_height'] = 1268;

		$this->load->library('upload', $config);

		if(!$this->upload->do_upload('userfile'))
		{
			$data['error'] = array('error' => $this->upload->display_errors("<p style='color:red;'>","</p>"));
			$data['headline'] = "Upload Error";
			$data['update_id'] = $update_id;
			
			$data['content'] = 'backend/package/upload_image';

			$data = $this->Back_Model->General($data);
			$this->load->view('backend/dashboard', $data);

		}else{
			// Upload Successful
			
			$data = array('upload_data' => $this->upload->data());

			$upload_data = $data['upload_data'];
			$file_name = $upload_data['file_name'];
			$this->_generate_thumbnail($file_name);

			// Update Image to Database
			$update_data['package_big_img'] = $file_name;
			$update_data['package_small_img'] = explode('.', $file_name)[0]."_thumb.".explode('.', $file_name)[1];
			$this->_update($update_id, $update_data);

			$data['headline'] = "Upload Success";
			$data['update_id'] = $update_id;			
			
			$data['content'] = 'backend/package/upload_image';

			$data = $this->Back_Model->General($data);
			$this->load->view('backend/dashboard', $data);
		}
	}

	// PREVIEW IMAGE //
	function modal_do_upload($update_id)
	{

		if(!is_numeric($update_id)){
			redirect('backend/src/Package/manage');
		}

		$config['upload_path'] = './gallery/package/preview_imgs/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = 200;
		$config['max_width'] = 1500;
		$config['max_height'] = 1268;

		$this->load->library('upload', $config);

		if(!$this->upload->do_upload('file'))
		{
			// Upload Error
			$data['error'] = array('error' => $this->upload->display_errors("<p style='color:red;'>","</p>"));
			$data['headline'] = "Upload Error";
			$data['update_id'] = $update_id;
			


		}else{
			// Upload Successful			
			$data = array('upload_data' => $this->upload->data());

			$upload_data = $data['upload_data'];
			$file_name = $upload_data['file_name'];
			$this->_generate_thumbnail($file_name);

			// Update Image to Database
			$update_data['package_preview_img'] = $file_name;
			$this->_update($update_id, $update_data);

			$data['headline'] = "Upload Success";
			$data['update_id'] = $update_id;			

			echo json_encode($update_data['package_preview_img']);

		}
	}
	function fetch_preview_image($update_id) //  Fetch ข้อมูลจาก Database
	{			
		if(!is_numeric($update_id)){
			redirect('backend/src/Package/manage');
		}

		$query = $this->get_where($update_id);
		foreach($query->result() as $row){				
			$data['package_preview_img'] = $row->package_preview_img;
		}

		if(!isset($data)){
			$data = "";
		}

		return $data;
	}
	function delete_preview($update_id)
	{
		
		if(!is_numeric($update_id)){
			redirect('backend/src/Package/manage');
		}

		$data = $this->fetch_preview_image($update_id);
		$preview_imgs = $data['package_preview_img'];

		$preview_imgs_path = './gallery/package/preview_imgs/'.$preview_imgs;

		// Remove the images
		if(file_exists($preview_imgs_path)){
			unlink($preview_imgs_path);
		}

		unset($data);
		$data['package_preview_img'] = "";
		$this->_update($update_id, $data);
		echo json_encode("Success");
	}
	function upload_preview($update_id){
		$preview_img_name = $this->fetch_preview_image($update_id)['package_preview_img'];

		if($preview_img_name != ""){

			$preview_path = 'gallery/package/preview_imgs/'.$preview_img_name;
			$big_path = 'gallery/package/big_imgs/'.$preview_img_name;
			copy($preview_path, $big_path);
			$this->_generate_thumbnail($preview_img_name);

			$update_data['package_big_img'] = $preview_img_name;
			$update_data['package_small_img'] = explode('.', $preview_img_name)[0]."_thumb.".explode('.', $preview_img_name)[1];
			$this->_update($update_id, $update_data);

			$this->delete_preview($update_id);
		}
	}
	// PREVIEW IMAGE //

	//////// Upload Image Package
	function upload_image($update_id)
	{
		
		if(!is_numeric($update_id)){
			redirect('backend/src/Package/manage');
		}

		$data['headline'] = "Upload Image​ Package";
		$data['update_id'] = $update_id;
		
		$data['content'] = 'backend/Package/upload_image';
		
		
		$data = $this->Back_Model->General($data);
		$this->load->view('backend/dashboard', $data);
	}

	//////// Add & Update Package
	function create()
	{		
		$this->load->library('timedate');
  
		$update_id = $this->uri->segment(5);
		if(isset(explode('_',$update_id)[1])){
			$lang = "_".explode('_',$update_id)[1];
		}
		else{
			$lang = "";
		}
		$update_id = explode('_',$update_id)[0];
		$submit = $this->input->post('submit', TRUE);

		if($submit == "Cancel"){
			redirect('backend/src/Package/manage');
		}

		// ถ้า $submit = value->Submit
		if($submit == "Submit"){
			// Process the Form at create.php
			$this->load->library('form_validation'); // Load Library From Validation
			$this->form_validation->set_rules('package_title', '<b>โปรดกรอกชื่อ Title</b>', 'required|max_length[240]');
			if(isset(explode('_',$this->uri->segment(5))[1])){
				$this->form_validation->set_rules('package_title_en', '<b>โปรดกรอกชื่อ Title En</b>', 'required|max_length[240]');					
			}
			//$this->form_validation->set_rules('package_type', '<b>โปรดเลือกประเภทแพ็กเกจ</b>', 'required|numeric');
			$this->form_validation->set_rules('package_startdate', '<b>โปรดเลือกวันเริ่มแพ็กเกจ</b>', 'required');
			$this->form_validation->set_rules('package_enddate', '<b>โปรดเลือกวันสิ้นสุดแพ็กเกจ</b>', 'required');
	    	//$this->form_validation->set_rules('package_status', '<b>โปรดเลือกการเผยแพร่</b>', 'required|numeric');

	    	if($this->form_validation->run() == TRUE){ // ถ้า form_validation ทำงานแล้วถูกต้อง
	    		// Get The Variables
	    		$data = $this->fetch_data_from_post(); // ให้ $data = รับค่าจาก From method="POST"
///////////////////////////////////////////////////////////////////////
				if($data['package_name'] == ''){
					$data['package_name'] = $data['package_title'];
				}
				if($data['package_name_en'] == ''){
					$data['package_name_en'] = $data['package_title_en'];
				}
///////////////////////////////////////////////////////////////////////
	    		$title = $this->url_slug($data['package_title']);
	    		$data['package_url'] = $title; // item_url = item_title

	    		//$data['date_published'] = $this->timedate->make_timestamp_from_datepicker($data['date_published']);
	    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id
	    			$this->upload_preview($update_id);
	    			// Update Items Details
	    			$data['m_user'] = $this->session->userdata('ses_id');
	    			$this->_update($update_id, $data); // Update ลง Database
	    			$flash_msg = "Update Package were Successfully"; // Alert message
					$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
					$this->session->set_flashdata('item', $value); // สร้าง session alert
					redirect('backend/src/package/create/'.$update_id.$lang);
	    		}else{
	    			// Insert a New Items
	    			$data['c_user'] = $this->session->userdata('ses_id');
	    			$this->_insert($data);
	    			$update_id = $this->get_max(); // Get the ID at new item

	    			$flash_msg = "Insert Package were Successfully";
					$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
					$this->session->set_flashdata('item', $value);
					redirect('backend/src/package/create/'.$update_id.$lang);
	    		}
	    	}
		}

		if((is_numeric($update_id)) && ($submit!="Submit")){
			$data = $this->fetch_data_from_db($update_id);
		}else{
			$data = $this->fetch_data_from_post();
			$data['package_big_img'] = "";
		}

		if(!is_numeric($update_id)){
			$data['headline'] = "เพิ่มข้อมูลแพ็กเกจ";
		}else{
			$data['headline'] = "แก้ไขข้อมูลแพ็กเกจ";
		}

		$data['update_id'] = $update_id.$lang;		      	

		$this->load->model('Home_model');
	    $data['medical_center']=$this->Home_model->all_medicial_center();

		$data['content'] = 'backend/package/formPackage';
		
		$data['Additionalscript'] = 'backend/package/scriptPackage';
		$data['replace2'] = 'backend/src/ck/replace2';

		$data = $this->Back_Model->General($data);
		$this->load->view('backend/dashboard', $data);
	}

	//////// Package Healthy Detail
	function manage()
	{

		$data['query'] = $this->get('created_at');
		
		$data['content'] = 'backend/package/managePackage';

		$data = $this->Back_Model->General($data);
		$this->load->view('backend/dashboard', $data);
	}

	function url_slug($text)
	{
		$array = array("'", '"', '\\', '/', '.', ',', '!', '+', '*', '%', '$', '#', '@', '(', ')', '[', ']', '?', '>', '<', '|', '’', '“', '”', '‘', '’', ';', ':', '–');
		$text = str_replace($array, '', $text); // Replaces all spaces with hyphens.
		$text = preg_replace('/[^A-Za-z0-9ก-๙เ-า\\-]/', '-', $text); // Removes special chars.
		$text = preg_replace('/-+/', "-", $text);
		$text = trim($text, '-');
		$text = urldecode($text);

		if (empty($text)){
			return NULL;
		}

		return $text;
	}

	//////// Fetch Data at method POST
	function fetch_data_from_post() // Fetch ข้อมูลจาก Input POST
	{
		$data['package_title'] = $this->input->post('package_title', TRUE);
		$data['package_title_en'] = $this->input->post('package_title_en', TRUE);
		//$data['package_type'] = $this->input->post('package_type', TRUE);
		$data['relate_medical_center_id'] = $this->input->post('relate_medical_center_id', TRUE);
		//$data['package_gallery'] = $this->input->post('package_gallery', TRUE);
		$data['package_name'] = $this->input->post('package_name', TRUE);
		$data['package_name_en'] = $this->input->post('package_name_en', TRUE);
		$data['package_subdesc'] = $this->input->post('package_subdesc', TRUE);
		$data['package_subdesc_en'] = $this->input->post('package_subdesc_en', TRUE);
		$data['package_description'] = html_escape($this->input->post('package_description', FALSE));
		$data['package_description_en'] = html_escape($this->input->post('package_description_en', FALSE));
		$data['package_unitprice'] = $this->input->post('package_unitprice', TRUE);
		$data['package_status'] = $this->input->post('package_status', TRUE);
		$data['package_startdate'] = $this->input->post('package_startdate', TRUE);
		$data['package_enddate'] = $this->input->post('package_enddate', TRUE);
		$data['package_startdate'] = date('Y-m-d',strtotime($data['package_startdate']));
		$data['package_enddate'] = date('Y-m-d',strtotime($data['package_enddate']));

		$cur_date = date('Y-m-d');
		$cur_date = strtotime($cur_date);
		$stime = strtotime($data['package_startdate']);
		$etime = strtotime($data['package_enddate']);
		/*
		if($cur_date >= $stime && $cur_date <= $etime){
			$data['package_status'] = '1';
		}
		else{
			$data['package_status'] = '0';
		}
		*/

		$data['meta_author'] = $this->input->post('meta_author', TRUE);
		$data['meta_keywords'] = $this->input->post('meta_keywords', TRUE);
		$data['meta_description'] = $this->input->post('meta_description', TRUE);
		
		return $data;
	}

	//////// Fetch Data at Database
	function fetch_data_from_db($update_id) //  Fetch ข้อมูลจาก Database
	{
		
		if(!is_numeric($update_id)){
			redirect('backend/src/Package/manage');
		}

		$query = $this->get_where($update_id);
		foreach($query->result() as $row){
			$data['package_url'] = $row->package_url;
			$data['package_title'] = $row->package_title;
			$data['package_title_en'] = $row->package_title_en;
			//$data['package_type'] = $row->package_type;
			$data['relate_medical_center_id'] = $row->relate_medical_center_id;
			//$data['package_gallery'] = $row->package_gallery;
			$data['package_name'] = $row->package_name;
			$data['package_name_en'] = $row->package_name_en;
			$data['package_subdesc'] = $row->package_subdesc;
			$data['package_subdesc_en'] = $row->package_subdesc_en;
			$data['package_description'] = $row->package_description;
			$data['package_description_en'] = $row->package_description_en;
			$data['package_unitprice'] = $row->package_unitprice;
			$data['package_startdate'] = $row->package_startdate;
			$data['package_enddate'] = $row->package_enddate;
			$data['package_status'] = $row->package_status;
			$data['package_big_img'] = $row->package_big_img;
			$data['package_small_img'] = $row->package_small_img;
			$data['created_at'] = $row->created_at;
			$data['updated_at'] = $row->updated_at;
			$data['c_user'] = $row->c_user;
			$data['m_user'] = $row->m_user;
			
			$cur_date = strtotime(date('Y-m-d'));
			$stime = strtotime($data['package_startdate']);
			$etime = strtotime($data['package_enddate']);
			/*
			if($cur_date >= $stime && $cur_date <= $etime){
				$data['package_status'] = '1';
			}
			else{
				$data['package_status'] = '0';
			}
			*/
			$data['package_startdate'] = date('m/d/Y',strtotime($data['package_startdate']));
			$data['package_enddate'] = date('m/d/Y',strtotime($data['package_enddate']));
							
			$data['meta_author'] = $row->meta_author;
			$data['meta_keywords'] = $row->meta_keywords;
			$data['meta_description'] = $row->meta_description;
		}

		if(!isset($data)){
			$data = "";
		}

		return $data;
	}

	function get($order_by)
	{
		$this->load->model('Mdl_formpackage');
		$query = $this->Mdl_formpackage->get($order_by);
		return $query;
	}

	function get_with_limit($limit, $offset, $order_by)
	{
		$this->load->model('Mdl_formpackage');
		$query = $this->Mdl_formpackage->get_with_limit($limit, $offset, $order_by);
		return $query;
	}

	function get_where($id)
	{
		$this->load->model('Mdl_formpackage');
		$query = $this->Mdl_formpackage->get_where($id);
		return $query;
	}

	function get_where_custom($col, $value)
	{
		$this->load->model('Mdl_formpackage');
		$query = $this->Mdl_formpackage->get_where_custom($col, $value);
		return $query;
	}

	function _insert($data)
	{
		$this->load->model('Mdl_formpackage');
		$this->Mdl_formpackage->_insert($data);
	}

	function _update($id, $data)
	{
		$this->load->model('Mdl_formpackage');
		$this->Mdl_formpackage->_update($id, $data);
	}

	function _delete($id)
	{
		$this->load->model('Mdl_formpackage');
		$this->Mdl_formpackage->_delete($id);
	}

	function count_where($column, $value)
	{
		$this->load->model('Mdl_formpackage');
		$count = $this->Mdl_formpackage->count_where($column, $value);
		return $count;
	}

	function get_max()
	{
		$this->load->model('Mdl_formpackage');
		$max_id = $this->Mdl_formpackage->get_max();
		return $max_id;
	}

	function _custom_query($mysql_query)
	{
		$this->load->model('Mdl_formpackage');
		$query = $this->Mdl_formpackage->_custom_query($mysql_query);
		return $query;
	}
	
}
