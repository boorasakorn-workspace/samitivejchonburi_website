<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Specialty extends CI_Controller {

		function __construct()
		{
			parent::__construct();
		}

		public function fetch_header()
		{
			$mysql_query = "select * from tb_header where header_status=1";
			$query = $this->_custom_query($mysql_query)->row();

			return $query;
		}
        
		public function fetch_nav(){
			$mysql_query = "select nav_title,nav_url,nav_content,id
			from tb_navigation
			where nav_status=1
			order by order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}

		public function fetch_subnav(){
			$mysql_query = "select tb_subnavigation.id as subid,tb_subnavigation.sub_nav_title,tb_subnavigation.sub_nav_url,tb_subnavigation.sub_nav_content,tb_subnavigation.parent_nav_id,
			tb_navigation.id
			from tb_subnavigation
			inner join tb_navigation
			on tb_subnavigation.parent_nav_id=tb_navigation.id
			where tb_subnavigation.sub_nav_status=1
			and tb_subnavigation.parent_nav_id=tb_navigation.id
			order by tb_subnavigation.order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}

		public function fetch_sidemenu(){
			$mysql_query = "select sidemenu_title,sidemenu_url,sidemenu_content,id
			from tb_sidemenu
			where sidemenu_status=1
			order by order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}

		public function fetch_subsidemenu(){
			$mysql_query = "select parent_sidemenu_id,sub_sidemenu_title,sub_sidemenu_url,sub_sidemenu_content,id
			from tb_subofsidemenu
			where sub_sidemenu_status=1
			order by order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}

		public function fetch_subofsubmenu(){
			$mysql_query = "select parent_submenu_id,subofsubmenu_title,subofsubmenu_url,subofsubmenu_content,id
			from tb_subofsubmenu
			where subofsubmenu_status=1
			order by order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}

		//////// Process Delete
		//Specialty
		function _process_delete_specialty($update_id)
		{

			$this->load->model('Mdl_site_security');
			$this->Mdl_site_security->_make_sure_is_admin();

			$flash_msg = "Deleted Specialty was Successfully";
			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
			$this->session->set_flashdata('item', $value);

			// Delete Items Record
			$this->_delete($update_id);
		}
		//////// Delete Specialty
		function delete_specialty($update_id)
		{

			$this->load->model('Mdl_site_security');
			$this->Mdl_site_security->_make_sure_is_admin();

			if(!is_numeric($update_id)){
				redirect('backend/src/Specialty/manage');
			}

			$this->load->library('session');

			$this->_process_delete_specialty($update_id);
			redirect('backend/src/Specialty/manage');
		}

		//Sub Specialty
		function _process_delete_sub_specialty($update_id)
		{

			$this->load->model('Mdl_site_security');
			$this->Mdl_site_security->_make_sure_is_admin();

			$flash_msg = "Deleted Sub Specialty was Successfully";
			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
			$this->session->set_flashdata('item', $value);

			// Delete Items Record
			$this->_delete_sub($update_id);
		}
		//////// Delete Sub Specialty
		function delete_sub_specialty($update_id)
		{

			$this->load->model('Mdl_site_security');
			$this->Mdl_site_security->_make_sure_is_admin();

			if(!is_numeric($update_id)){
				redirect('backend/src/Specialty/manage');
			}

			$this->load->library('session');

			$this->_process_delete_sub_specialty($update_id);
			redirect('backend/src/Specialty/manage');
		}

		//////// Add & Update Specialty
		function create()
		{
			$this->load->library('session');
			$this->load->library('timedate');

		    $this->load->model('Mdl_site_security');
		    $this->Mdl_site_security->_make_sure_is_admin();
      
			$update_id = $this->uri->segment(5);
			$submit = $this->input->post('submit', TRUE);

			if($submit == "Cancel"){
				redirect('backend/src/Specialty/manage');
			}

			// ถ้า $submit = value->Submit
			if($submit == "Submit"){
				$this->load->library('form_validation'); // Load Library From Validation
				$this->form_validation->set_rules('specialty_name_th', '<b>Required Specialty TH</b>', 'required|max_length[240]');
				if(isset(explode('_',$this->uri->segment(5))[1])){
					$this->form_validation->set_rules('specialty_name_en', '<b>Required Specialty En</b>', 'required|max_length[240]');
				}

		    	if($this->form_validation->run() == TRUE){
		    		$data = $this->fetch_data_from_post();

		    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id

		    			// Update Items Details
		    			$data['m_user'] = $this->session->userdata('ses_id');
		    			$this->_update($update_id, $data); // Update ลง Database
		    			$flash_msg = "Update Specialty were Successfully"; // Alert message
						$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
						$this->session->set_flashdata('item', $value); // สร้าง session alert
						redirect('backend/src/specialty/create/'.$update_id.$lang);
		    		}else{

		    			// Insert a New Items
		    			$data['c_user'] = $this->session->userdata('ses_id');
		    			$this->_insert($data);
		    			$update_id = $this->get_max(); // Get the ID at new item

		    			$flash_msg = "Insert Specialty were Successfully";
						$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
						$this->session->set_flashdata('item', $value);
						redirect('backend/src/specialty/create/'.$update_id.$lang);
		    		}
		    	}
			}

			if((is_numeric($update_id)) && ($submit!="Submit")){
				$data = $this->fetch_data_from_db($update_id);
			}else{
				$data = $this->fetch_data_from_post();
			}

			if(!is_numeric($update_id)){
				$data['headline'] = "Add Specialty";
			}else{
				$data['headline'] = "Edit Specialty";
			}

			$data['update_id'] = $update_id.$lang;
			$data['flash'] = $this->session->flashdata('item');

			// SESSION
	      	$data['username'] = $this->session->userdata('ses_username');
	      	$data['fullname'] = $this->session->userdata('ses_full_name');
	      	$data['position'] = $this->session->userdata('ses_position');
	      	$data['pic'] = $this->session->userdata('ses_pic');
	      	// END SESSION

			$data['header'] = 'backend/src/header';
			$data['sidebar'] = 'backend/src/sidebar';
			$data['content'] = 'backend/specialty/formSpecialty';
			$data['script'] = 'backend/src/script';
			//$data['Additionalscript'] = 'backend/specialty/scriptSpecialty';
			$data['replace2'] = 'backend/src/ck/replace2';
			
			$this->load->model('Mdl_formemail');
			$data['count'] = $this->Mdl_formemail->count_array(array('email_status' => 1, 'email_open' => 0));

			$this->load->view('backend/dashboard', $data);
		}

		//////// Add & Update Specialty
		function create_sub()
		{
			$this->load->library('session');
			$this->load->library('timedate');

		    $this->load->model('Mdl_site_security');
		    $this->Mdl_site_security->_make_sure_is_admin();
      
			$update_id = $this->uri->segment(5);
			$submit = $this->input->post('submit', TRUE);

			if($submit == "Cancel"){
				redirect('backend/src/Specialty/manage_sub/');
			}

			// ถ้า $submit = value->Submit
			if($submit == "Submit"){
				$this->load->library('form_validation'); // Load Library From Validation
				$this->form_validation->set_rules('sub_specialty_name_th', '<b>Required Sub Specialty TH</b>', 'required|max_length[240]');
				if(isset(explode('_',$this->uri->segment(5))[1])){
					$this->form_validation->set_rules('sub_specialty_name_en', '<b>Required Sub Specialty En</b>', 'required|max_length[240]');
				}

		    	if($this->form_validation->run() == TRUE){
		    		$data = $this->fetch_data_from_post();

		    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id

		    			// Update Items Details
		    			$data['m_user'] = $this->session->userdata('ses_id');
		    			$this->_update_sub($update_id, $data); // Update ลง Database
		    			$flash_msg = "Update Sub Specialty were Successfully"; // Alert message
						$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
						$this->session->set_flashdata('item', $value); // สร้าง session alert
						redirect('backend/src/specialty/create_sub/'.$update_id.$lang);
		    		}else{

		    			// Insert a New Items
		    			$data['c_user'] = $this->session->userdata('ses_id');
		    			$this->_insert_sub($data);
		    			$update_id = $this->get_max(); // Get the ID at new item

		    			$flash_msg = "Insert Sub Specialty were Successfully";
						$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
						$this->session->set_flashdata('item', $value);
						redirect('backend/src/specialty/create_sub/'.$update_id.$lang);
		    		}
		    	}
			}

			if((is_numeric($update_id)) && ($submit!="Submit")){
				$data = $this->sub_fetch_data_from_db($update_id);
			}else{
				$data = $this->fetch_data_from_post();
			}

			if(!is_numeric($update_id)){
				$data['headline'] = "Add Sub Specialty";
			}else{
				$data['headline'] = "Edit Sub Specialty";
			}

			$data['update_id'] = $update_id.$lang;
			$data['flash'] = $this->session->flashdata('item');

			// SESSION
	      	$data['username'] = $this->session->userdata('ses_username');
	      	$data['fullname'] = $this->session->userdata('ses_full_name');
	      	$data['position'] = $this->session->userdata('ses_position');
	      	$data['pic'] = $this->session->userdata('ses_pic');
	      	// END SESSION

			$data['header'] = 'backend/src/header';
			$data['sidebar'] = 'backend/src/sidebar';
			$data['content'] = 'backend/specialty/formSubSpecialty';
			$data['script'] = 'backend/src/script';
			//$data['Additionalscript'] = 'backend/specialty/scriptSpecialty';
			$data['replace2'] = 'backend/src/ck/replace2';
			
			$this->load->model('Mdl_formemail');
			$data['count'] = $this->Mdl_formemail->count_array(array('email_status' => 1, 'email_open' => 0));

			$this->load->view('backend/dashboard', $data);
		}

		//////// Specialty Manage
		function manage()
		{

	        $this->load->model('Mdl_site_security');
	        $this->Mdl_site_security->_make_sure_is_admin();
      
			$data['query'] = $this->get('specialty_name_th');

			$data['flash'] = $this->session->flashdata('item');

			// SESSION
	      	$data['username'] = $this->session->userdata('ses_username');
	      	$data['fullname'] = $this->session->userdata('ses_full_name');
	      	$data['position'] = $this->session->userdata('ses_position');
	      	$data['pic'] = $this->session->userdata('ses_pic');
	      	// END SESSION

			$data['get_sidemenu'] = $this->fetch_sidemenu()->result();
        	$data['get_subsidemenu'] = $this->fetch_subsidemenu()->result();
       		$data['get_subofsubmenu'] = $this->fetch_subofsubmenu()->result();

			$data['header'] = 'backend/src/header';
			$data['sidebar'] = 'backend/src/sidebar';
			$data['content'] = 'backend/specialty/manageSpecialty';
			$data['script'] = 'backend/src/script';
			
			$this->load->model('Mdl_formemail');
			$data['count'] = $this->Mdl_formemail->count_array(array('email_status' => 1, 'email_open' => 0));

			$this->load->view('backend/dashboard', $data);
		}

		//////// Sub Specialty Manage
		function manage_sub()
		{

	        $this->load->model('Mdl_site_security');
	        $this->Mdl_site_security->_make_sure_is_admin();
      
			$data['query'] = $this->get_sub('sub_specialty_name_th');

			$data['flash'] = $this->session->flashdata('item');

			// SESSION
	      	$data['username'] = $this->session->userdata('ses_username');
	      	$data['fullname'] = $this->session->userdata('ses_full_name');
	      	$data['position'] = $this->session->userdata('ses_position');
	      	$data['pic'] = $this->session->userdata('ses_pic');
	      	// END SESSION

			$data['get_sidemenu'] = $this->fetch_sidemenu()->result();
        	$data['get_subsidemenu'] = $this->fetch_subsidemenu()->result();
       		$data['get_subofsubmenu'] = $this->fetch_subofsubmenu()->result();

			$data['header'] = 'backend/src/header';
			$data['sidebar'] = 'backend/src/sidebar';
			$data['content'] = 'backend/specialty/manageSubSpecialty';
			$data['script'] = 'backend/src/script';
			
			$this->load->model('Mdl_formemail');
			$data['count'] = $this->Mdl_formemail->count_array(array('email_status' => 1, 'email_open' => 0));

			$this->load->view('backend/dashboard', $data);
		}

		function url_slug($text)
		{
			$array = array("'", '"', '\\', '/', '.', ',', '!', '+', '*', '%', '$', '#', '@', '(', ')', '[', ']', '?', '>', '<', '|', '’', '“', '”', '‘', '’', ';', ':', '–');
			$text = str_replace($array, '', $text); // Replaces all spaces with hyphens.
			$text = preg_replace('/[^A-Za-z0-9ก-๙เ-า\\-]/', '-', $text); // Removes special chars.
			$text = preg_replace('/-+/', "-", $text);
			$text = trim($text, '-');
			$text = urldecode($text);

			if (empty($text)){
				return NULL;
			}

			return $text;
		}

		//////// Fetch Data at method POST
		function fetch_data_from_post() // Fetch ข้อมูลจาก Input POST
		{
			$check = $this->input->post();
			if( isset($check['specialty_name_th']) && isset($check['sub_specialty_name_en']) ){
				$data['parent_name'] = $this->input->post('parent_name', TRUE);
				$data['specialty_name_th'] = $this->input->post('specialty_name_th', TRUE);
				$data['specialty_name_en'] = $this->input->post('specialty_name_en', TRUE);
			}
			elseif( isset($check['sub_specialty_name_th']) && isset($check['sub_specialty_name_en'])){
				$data['parent_name'] = $this->input->post('parent_name', TRUE);
				$data['parent_specialty_name'] = $this->input->post('parent_specialty_name', TRUE);
				$data['sub_specialty_name_th'] = $this->input->post('sub_specialty_name_th', TRUE);
				$data['sub_specialty_name_en'] = $this->input->post('sub_specialty_name_en', TRUE);
			}
			$data['status'] = $this->input->post('status', TRUE);

			return $data;

			/*
			$data['parent_name'] = $this->input->post('parent_name', TRUE);
			$data['parent_specialty_name'] = $this->input->post('parent_specialty_name', TRUE);

			$data['specialty_name_th'] = $this->input->post('specialty_name_th', TRUE);
			$data['specialty_name_en'] = $this->input->post('specialty_name_en', TRUE);

			$data['sub_specialty_name_th'] = $this->input->post('sub_specialty_name_th', TRUE);
			$data['sub_specialty_name_en'] = $this->input->post('sub_specialty_name_en', TRUE);

			$data['status'] = $this->input->post('status', TRUE);
			*/
		}

		//////// Fetch Data at Database
		function fetch_data_from_db($update_id) //  Fetch ข้อมูลจาก Database
		{
			
			if(!is_numeric($update_id)){
				redirect('backend/src/Specialty/manage');
			}

			$query = $this->get_where($update_id);
			foreach($query->result() as $row){
				$data['specialty_url'] = $row->specialty_url;
				$data['specialty_title'] = $row->specialty_title;
				$data['specialty_title_en'] = $row->specialty_title_en;
				$data['specialty_type'] = $row->specialty_type;
				//$data['specialty_gallery'] = $row->specialty_gallery;
				$data['specialty_name'] = $row->specialty_name;
				$data['specialty_name_en'] = $row->specialty_name_en;
				$data['specialty_subdesc'] = $row->specialty_subdesc;
				$data['specialty_subdesc_en'] = $row->specialty_subdesc_en;
				$data['specialty_description'] = $row->specialty_description;
				$data['specialty_description_en'] = $row->specialty_description_en;
				$data['specialty_unitprice'] = $row->specialty_unitprice;
				$data['specialty_startdate'] = $row->specialty_startdate;
				$data['specialty_enddate'] = $row->specialty_enddate;
				$data['specialty_status'] = $row->specialty_status;
				$data['specialty_big_img'] = $row->specialty_big_img;
				$data['specialty_small_img'] = $row->specialty_small_img;
				$data['created_at'] = $row->created_at;
				$data['updated_at'] = $row->updated_at;
				$data['c_user'] = $row->c_user;
				$data['m_user'] = $row->m_user;
				
				$cur_date = strtotime(date('Y-m-d'));
				$stime = strtotime($data['specialty_startdate']);
				$etime = strtotime($data['specialty_enddate']);
				if($cur_date >= $stime && $cur_date <= $etime){
					$data['specialty_status'] = '1';
				}
				else{
					$data['specialty_status'] = '0';
				}

				$data['specialty_startdate'] = date('m/d/Y',strtotime($data['specialty_startdate']));
				$data['specialty_enddate'] = date('m/d/Y',strtotime($data['specialty_enddate']));
								
				$data['meta_author'] = $row->meta_author;
				$data['meta_keywords'] = $row->meta_keywords;
				$data['meta_description'] = $row->meta_description;
			}

			if(!isset($data)){
				$data = "";
			}

			return $data;
		}

		function get($order_by)
		{
			$this->load->model('Mdl_formspecialty');
			$query = $this->Mdl_formspecialty->get($order_by);
			return $query;
		}

		function get_with_limit($limit, $offset, $order_by)
		{
			$this->load->model('Mdl_formspecialty');
			$query = $this->Mdl_formspecialty->get_with_limit($limit, $offset, $order_by);
			return $query;
		}

		function get_where($id)
		{
			$this->load->model('Mdl_formspecialty');
			$query = $this->Mdl_formspecialty->get_where($id);
			return $query;
		}

		function get_where_custom($col, $value)
		{
			$this->load->model('Mdl_formspecialty');
			$query = $this->Mdl_formspecialty->get_where_custom($col, $value);
			return $query;
		}

		function _insert($data)
		{
			$this->load->model('Mdl_formspecialty');
			$this->Mdl_formspecialty->_insert($data);
		}

		function _update($id, $data)
		{
			$this->load->model('Mdl_formspecialty');
			$this->Mdl_formspecialty->_update($id, $data);
		}

		function _delete($id)
		{
			$this->load->model('Mdl_formspecialty');
			$this->Mdl_formspecialty->_delete($id);
		}

		function count_where($column, $value)
		{
			$this->load->model('Mdl_formspecialty');
			$count = $this->Mdl_formspecialty->count_where($column, $value);
			return $count;
		}

		function get_max()
		{
			$this->load->model('Mdl_formspecialty');
			$max_id = $this->Mdl_formspecialty->get_max();
			return $max_id;
		}

		function _custom_query($mysql_query)
		{
			$this->load->model('Mdl_formspecialty');
			$query = $this->Mdl_formspecialty->_custom_query($mysql_query);
			return $query;
		}
		
	}
