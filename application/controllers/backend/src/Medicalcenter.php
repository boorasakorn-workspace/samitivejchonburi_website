<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Medicalcenter extends CI_Controller {

		function __construct()
		{
			parent::__construct();
			$this->load->model('Mdl_site_security');
			$this->Mdl_site_security->_make_sure_is_admin();

			$this->load->library('session');

			$this->load->model('Back_Model');
			$this->load->model('Home_model');
			$this->load->model('Mdl_formmedicalcenter');
		}

	public function language(){
		if(empty($this->session->userdata('language'))){
			$this->session->set_userdata("language", 'thai');
			$this->session->set_userdata("q_l", ' ');
		}
		$this->session->set_userdata("q_l", '_'.substr($this->session->userdata('language'), 0,2));
		if($this->session->userdata('q_l') == 'th' || $this->session->userdata('q_l') == '_th' ){
			$this->session->set_userdata("q_l", ' ');			
		}
		return $this->lang->load($this->session->userdata('language'), 'english');
			
	}

	public function change_language($lang){
		$this->session->set_userdata("language", $lang);
		$this->session->set_userdata("q_l", '_'.substr($lang, 0,2));
		if($this->session->userdata('q_l') == 'th' || $this->session->userdata('q_l') == '_th' ){
			$this->session->set_userdata("q_l", ' ');			
		}
		redirect($_SERVER['HTTP_REFERER']);
	}

		function view_medicalcenter_details($update_id)
		{
			redirect('backend/MainBackend/ServiceDetails/'.$update_id);
		}


	function preview_Contents()
	{
		
		$this->load->model('Front_Model');
		$this->load->model('Front_Home_Model');
		$data = $this->Front_Model->Detailpage();

		$data['query'] = json_decode(json_encode($this->input->post()));
		$data['serviceDetails'] = 'frontend/src/service/service_details';

		$data['previewContent'] = "previewContent";
		$this->load->view('frontend/homepage', $data);
	}

		//////// Process Delete All 1.Colours 2.Sizes 3.Big Image 4.Small Image 5.Data Record
		function _process_delete($update_id)
		{
			// Delete Medicalcenter Logo Big & Small Image
			$data = $this->fetch_data_from_db($update_id);
			$big_imgs = $data['medical_center_imgs'];

			$big_imgs_path = './gallery/medical_center/big_imgs/'.$big_imgs;

				// Remove the images
				if(file_exists($big_imgs_path)){
					unlink($big_imgs_path);
				}
			
			// Delete Medicalcenter Record from tb_medicalcenter database
			$this->_delete($update_id);
		}

		//////// Delete Medicalcenter Process from controller backend/src/Medicalcenter/delete_medicalcenter
		function delete_medicalcenter_process($update_id)
		{		
			if(!is_numeric($update_id)){
				redirect('backend/src/Medicalcenter/manage');
			}

			$submit = $this->input->post('submit', TRUE);

			if($submit=="Cancel"){
				redirect('backend/src/Medicalcenter/create/'.$update_id);
			}elseif($submit=="Yes - Delete Medical Center"){
				$this->_process_delete($update_id);

				$flash_msg = "Deleted Medical Center was Successfully";
				$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
				$this->session->set_flashdata('item', $value);

				redirect('backend/src/Medicalcenter/manage');
			}
		}

		//////// Delete Medicalcenter Page
		function delete_medicalcenter($update_id)
		{

			if(!is_numeric($update_id)){
				redirect('backend/src/Medicalcenter/manage');
			}
			
			$data['headline'] = "Delete Medical Center";
			$data['update_id'] = $update_id;
			
			$data['content'] = 'backend/medical_center/delete_medicalcenter';
			
			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

		//////// Delete Image Thumbnail
		function delete_image($update_id)
		{
			if(!is_numeric($update_id)){
				redirect('backend/src/Medicalcenter/manage');
			}	    

			$data = $this->fetch_data_from_db($update_id);
			$big_imgs = $data['medical_center_imgs'];

			$big_imgs_path = './gallery/medical_center/big_imgs/'.$big_imgs;

			// Remove the images
			if(file_exists($big_imgs_path)){
				unlink($big_imgs_path);
			}

			// Update Remove the images to database
			unset($data);
			$data['medical_center_imgs'] = "";
			$this->_update($update_id, $data);

			$flash_msg = "The image was successfully deleted."; // Alert message
			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
			$this->session->set_flashdata('item', $value); // สร้าง session alert

			redirect('backend/src/Medicalcenter/create/'.$update_id);
		}

		//////// Set Image Thumbnail
		function _generate_thumbnail($file_name)
		{
			$config['image_library'] = 'gd2';
			$config['source_image'] = './gallery/medical_center/big_imgs/'.$file_name;
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = TRUE;
			$config['width']         = 235;
			$config['height']       = 96;

			$this->load->library('image_lib', $config);

			$this->image_lib->resize();
		}

		// PREVIEW IMAGE //
		function modal_do_upload($update_id)
		{

			if(!is_numeric($update_id)){
				redirect('backend/src/Medicalcenter/manage');
			}

			$config['upload_path'] = './gallery/medical_center/preview_imgs/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = 2000;
			$config['max_width'] = 15000;
			$config['max_height'] = 12680;

			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('file'))
			{
				// Upload Error

				$data['error'] = array('error' => $this->upload->display_errors("<p style='color:red;'>","</p>"));
				$data['headline'] = "Upload Error";
				$data['update_id'] = $update_id;
				
			}else{
				// Upload Successful
				
				$data = array('upload_data' => $this->upload->data());

				$upload_data = $data['upload_data'];
				$file_name = $upload_data['file_name'];
				//$this->_generate_thumbnail($file_name);

				// Update Image to Database
				$update_data['medical_center_preview_icon'] = $file_name;
				$this->_update($update_id, $update_data);

				$data['headline'] = "Upload Success";
				$data['update_id'] = $update_id;
				

				echo json_encode($update_data['medical_center_preview_icon']);
			}
		}
		function fetch_preview_image($update_id) //  Fetch ข้อมูลจาก Database
		{			
			if(!is_numeric($update_id)){
				redirect('backend/src/Medicalcenter/manage');
			}

			$query = $this->get_where($update_id);
			foreach($query->result() as $row){				
				$data['medical_center_preview_icon'] = $row->medical_center_preview_icon;
			}

			if(!isset($data)){
				$data = "";
			}

			return $data;
		}
		function delete_preview($update_id)
		{

			if(!is_numeric($update_id)){
				redirect('backend/src/Medicalcenter/manage');
			}

			$data = $this->fetch_preview_image($update_id);
			$preview_imgs = $data['medical_center_preview_icon'];

			$preview_imgs_path = './gallery/medical_center/preview_imgs/'.$preview_imgs;

			// Remove the images
			if(file_exists($preview_imgs_path)){
				unlink($preview_imgs_path);
			}

			unset($data);
			$data['medical_center_preview_icon'] = "";
			$this->_update($update_id, $data);
			echo json_encode("Success");
		}
		function upload_preview($update_id){
			$preview_img_name = $this->fetch_preview_image($update_id)['medical_center_preview_icon'];
			if($preview_img_name != ""){
				$preview_path = 'gallery/medical_center/preview_imgs/'.$preview_img_name;
				$big_path = 'gallery/medical_center/icon/'.$preview_img_name;
				copy($preview_path, $big_path);

				$update_data['medical_center_icon'] = $preview_img_name;
				$this->_update($update_id, $update_data);

				$this->delete_preview($update_id);

			}
		}
		// PREVIEW IMAGE //


		//////// Upload Image Medicalcenter Do Upload
		function do_upload($update_id)
		{			

			if(!is_numeric($update_id)){
				redirect('backend/src/Medicalcenter/manage');
			}

			$submit = $this->input->post('submit', TRUE);
			if($submit == "Cancel"){
				redirect('backend/src/Medicalcenter/create/'.$update_id);
			}

			$config['upload_path'] = './gallery/medical_center/big_imgs/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = 204800;
			$config['max_width'] = 1024;
			$config['max_height'] = 768;

			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('userfile'))
			{
				$data['error'] = array('error' => $this->upload->display_errors("<p style='color:red;'>","</p>"));
				$data['headline'] = "Upload Error";
				$data['update_id'] = $update_id;
				
				$data['content'] = 'backend/medical_center/upload_image';

				$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);

			}else{
				// Upload Successful
				
				$data = array('upload_data' => $this->upload->data());

				$upload_data = $data['upload_data'];
				$file_name = $upload_data['file_name'];
				$this->_generate_thumbnail($file_name);

				// Update Image to Database
				$update_data['medical_center_imgs'] = $file_name;
				$this->_update($update_id, $update_data);
				
				$flash_msg = "Upload Medical Center Image were Successfully";
				$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
				$this->session->set_flashdata('item', $value);

				$data['headline'] = "Upload Success";
				$data['update_id'] = $update_id;
								
				$data['content'] = 'backend/medical_center/upload_image';

				$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
			}
		}

		//////// Upload Image Medicalcenter Do Upload
		function do_upload_icon($update_id)
		{

			if(!is_numeric($update_id)){
				redirect('backend/src/Medicalcenter/manage');
			}

			$submit = $this->input->post('submit', TRUE);
			if($submit == "Cancel"){
				redirect('backend/src/Medicalcenter/create/'.$update_id);
			}

			$config['upload_path'] = './gallery/medical_center/icon/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = 2048000;
			$config['max_width'] = 2048;
			$config['max_height'] = 2048;

			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('userfile'))
			{
				$data['error'] = array('error' => $this->upload->display_errors("<p style='color:red;'>","</p>"));
				$data['headline'] = "Upload Error";
				$data['update_id'] = $update_id;
				
				$data['content'] = 'backend/medical_center/upload_icon';
				
				$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);

			}else{
				// Upload Successful
				
				$data = array('upload_data' => $this->upload->data());

				$upload_data = $data['upload_data'];
				$file_name = $upload_data['file_name'];

				// Update Image to Database
				$update_data['medical_center_icon'] = $file_name;
				$this->_update($update_id, $update_data);
				
				$flash_msg = "Upload Medical Center Icon were Successfully";
				$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
				$this->session->set_flashdata('item', $value);

				$data['headline'] = "Upload Success";
				$data['update_id'] = $update_id;
				
				$data['content'] = 'backend/medical_center/upload_icon';
				
				$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
			}
		}


		//////// Upload Image Medicalcenter
		function upload_image($update_id)
		{

			if(!is_numeric($update_id)){
				redirect('backend/src/Medicalcenter/manage');
			}

			$data['headline'] = "Upload Image​ Medical Center";
			$data['update_id'] = $update_id;
			
			$data['content'] = 'backend/medical_center/upload_image';			
			
			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

		//////// Upload Image Medicalcenter
		function upload_icon($update_id)
		{

			if(!is_numeric($update_id)){
				redirect('backend/src/Medicalcenter/manage');
			}
			
			$data['headline'] = "Upload Icon Medical Center";
			$data['update_id'] = $update_id;			
			
			$data['content'] = 'backend/medical_center/upload_icon';			
			
			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}
		//////// Add & Update Medicalcenter
		function create()
		{

			error_reporting (E_ALL ^ E_NOTICE);
			$update_id = $this->uri->segment(5);
			$submit = $this->input->post('submit', TRUE);

			if($submit == "Cancel"){
				redirect('backend/src/Medicalcenter/manage');
			}

			// ถ้า $submit = value->Submit
			if($submit == "Submit"){
				// Process the Form at create.php
				$this->load->library('form_validation'); // Load Library From Validation

				$this->form_validation->set_rules('description', '<b>โปรดกรอกเฉพาะชื่อศูนย์พยาบาล</b>', 'required|max_length[150]');
		    	$this->form_validation->set_rules('medical_center_contents', '<b>โปรดกรอกประวัติการศึกษา</b>', 'required');
		    	//$this->form_validation->set_rules('medical_center_location', '<b>โปรดกรอกสถานที่ เช่น ตึก A ชั้น 1</b>', 'required');
		    	$this->form_validation->set_rules('medical_center_office_daystxt', '<b>โปรดกรอกวันทำการ เช่น จันทร์-ศุกร์</b>', 'required');
		    	//$this->form_validation->set_rules('medical_center_office_hours', '<b>โปรดกรอกเวลาทำการ</b>', 'required');
		    	//$this->form_validation->set_rules('medical_center_phone', '<b>โปรดกรอกเบอร์โทรศัพท์</b>', 'required|max_length[30]|min_length[10]');
		    	//$this->form_validation->set_rules('medical_center_email', '<b>โปรดกรอกอีเมลล์สำหรับติดต่อ</b>', 'required|max_length[150]|valid_email');
		    	//$this->form_validation->set_rules('medical_center_meta_keywords', '<b>โปรดกรอกรายละเอียด Meta Keywords</b>', 'required');
		    	//$this->form_validation->set_rules('medical_center_meta_description', '<b>โปรดกรอกรายละเอียด Meta Descriptions</b>', 'required');
		    	$this->form_validation->set_rules('medical_center_is', '<b>โปรดเลือกประเภทของศูนย์แพทย์</b>', 'required');
		    	$this->form_validation->set_rules('status', '<b>โปรดเลือกสถานะของศูนย์พยาบาล</b>', 'required');

		    	if($this->form_validation->run() == TRUE){ // ถ้า form_validation ทำงานแล้วถูกต้อง
		    		// Get The Variables
		    		$data = $this->fetch_data_from_post(); // ให้ $data = รับค่าจาก From method="POST"
		    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id
		    			$this->upload_preview($update_id);

		    			// Update Medicalcenter Details
		    			$this->_update($update_id, $data); // Update ลง Database
		    			$flash_msg = "Update Medical Center were Successfully"; // Alert message
						$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
						$this->session->set_flashdata('item', $value); // สร้าง session alert
						redirect('backend/src/Medicalcenter/create/'.$update_id);
		    		}else{
		    			// Insert a New Medicalcenter
		    			$this->_insert($data);
		    			$update_id = $this->get_max(); // Get the ID at new Medicalcenter

		    			$flash_msg = "Insert Medical Center were Successfully";
						$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
						$this->session->set_flashdata('item', $value);
						redirect('backend/src/Medicalcenter/create/'.$update_id);
		    		}
		    	}
			}
			
			if((is_numeric($update_id)) && ($submit!="Submit")){
				$data = $this->fetch_data_from_db($update_id);
			}else{
				$data = $this->fetch_data_from_post();
				$data['medical_center_logo'] = "";
			}

			if(!is_numeric($update_id)){
				$data['headline'] = "เพิ่มข้อมูล Medical Center";
			}else{
				$data['headline'] = "แก้ไขข้อมูล Medical Center";
			}

			$data['update_id'] = $update_id;
			
			$data['content'] = 'backend/medical_center/formMedicalcenter';
			
			$data['Additionalscript'] = 'backend/medical_center/scriptMedicalcenter';
			$data['replace5'] = 'backend/src/ck/replace5';

			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

		//////// Medicalcenter Detail
		function manage()
		{
			$data['query'] = $this->Mdl_formmedicalcenter->get('order_priority');
			//$data['query'] = $this->get('medical_center_name');
			
			$data['content'] = 'backend/medical_center/manageMedicalcenter';

			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

		//////// Fetch Data at method POST
		function fetch_data_from_post() // Fetch ข้อมูลจาก Input POST
		{
			$data['description'] = $this->input->post('description', TRUE);
			$data['medical_center_details'] = $this->input->post('medical_center_details', TRUE);
			$data['medical_center_location'] = $this->input->post('medical_center_location', TRUE);
			$data['medical_center_contents'] = html_escape($this->input->post('medical_center_contents', FALSE));
			$data['medical_center_office_daystxt'] = $this->input->post('medical_center_office_daystxt', TRUE);
			$data['medical_center_office_hours'] = $this->input->post('medical_center_office_hours', TRUE);	

			//$data['medical_center_location'] = "สมิติเวช";
			//$data['medical_center_office_hours'] = "08:00น. - 18:00น.";			
			//$data['medical_center_phone'] = "0000000000";
			//$data['medical_center_email'] = "email@email.com";
			//$data['medical_center_meta_keywords'] = $data['medical_center_name'];
			//$data['medical_center_meta_description'] = $data['medical_center_name'];

			$data['medical_center_phone'] = $this->input->post('medical_center_phone', TRUE);
			$data['medical_center_email'] = $this->input->post('medical_center_email', TRUE);
			$data['medical_center_meta_keywords'] = $this->input->post('medical_center_meta_keywords', TRUE);
			$data['medical_center_meta_description'] = $this->input->post('medical_center_meta_description', TRUE);
			$data['status'] = $this->input->post('status', TRUE);
			// $data['medical_center_url'] = $this->input->post('medical_center_url', TRUE);
			$data['medical_center_is'] = $this->input->post('medical_center_is', TRUE);
			
			return $data;
		}

		//////// Fetch Data at Database
		function fetch_data_from_db($update_id) //  Fetch ข้อมูลจาก Database
		{
			
			if(!is_numeric($update_id)){
				redirect('backend/src/Medicalcenter/manage');
			}

			$query = $this->get_where($update_id);
			$data = $query->row_array();
			/*
			foreach($query->result() as $row){
				
				$data['medical_center_name'] = $row->medical_center_name;
				$data['medical_center_details'] = $row->medical_center_details;
				$data['medical_center_contents'] = $row->medical_center_contents;
				$data['medical_center_location'] = $row->medical_center_location;
				$data['medical_center_office_daystxt'] = $row->medical_center_office_daystxt;
				$data['medical_center_office_hours'] = $row->medical_center_office_hours;
				$data['medical_center_phone'] = $row->medical_center_phone;
				$data['medical_center_email'] = $row->medical_center_email;
				$data['medical_center_meta_keywords'] = $row->medical_center_meta_keywords;
				$data['medical_center_meta_description'] = $row->medical_center_meta_description;
				$data['order_priority'] = $row->order_priority;
				$data['medical_center_url'] = $row->medical_center_url;
				$data['status'] = $row->status;
				$data['medical_center_is'] = $row->medical_center_is;
				$data['medical_center_imgs'] = $row->medical_center_imgs;
				$data['medical_center_icon'] = $row->medical_center_icon;
			}
			*/
			if(!isset($data)){
				$data = array();
			}

			return $data;
		}

		function get($order_by)
		{
			$this->load->model('Mdl_formmedicalcenter');
			$query = $this->Mdl_formmedicalcenter->get($order_by);
			return $query;
		}

		function get_with_limit($limit, $offset, $order_by)
		{
			$this->load->model('Mdl_formmedicalcenter');
			$query = $this->Mdl_formmedicalcenter->get_with_limit($limit, $offset, $order_by);
			return $query;
		}

		function get_where($id)
		{
			$this->load->model('Mdl_formmedicalcenter');
			$query = $this->Mdl_formmedicalcenter->get_where($id);
			return $query;
		}

		function get_where_custom($col, $value)
		{
			$this->load->model('Mdl_formmedicalcenter');
			$query = $this->Mdl_formmedicalcenter->get_where_custom($col, $value);
			return $query;
		}

		function _insert($data)
		{
			$this->load->model('Mdl_formmedicalcenter');
			$this->Mdl_formmedicalcenter->_insert($data);
		}

		function _update($id, $data)
		{
			$this->load->model('Mdl_formmedicalcenter');
			$this->Mdl_formmedicalcenter->_update($id, $data);
		}

		function _delete($id)
		{
			$this->load->model('Mdl_formmedicalcenter');
			$this->Mdl_formmedicalcenter->_delete($id);
		}

		function count_where($column, $value)
		{
			$this->load->model('Mdl_formmedicalcenter');
			$count = $this->Mdl_formmedicalcenter->count_where($column, $value);
			return $count;
		}

		function get_max()
		{
			$this->load->model('Mdl_formmedicalcenter');
			$max_id = $this->Mdl_formmedicalcenter->get_max();
			return $max_id;
		}

		function _custom_query($mysql_query)
		{
			$this->load->model('Mdl_formmedicalcenter');
			$query = $this->Mdl_formmedicalcenter->_custom_query($mysql_query);
			return $query;
		}
	}
