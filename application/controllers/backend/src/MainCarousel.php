<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MainCarousel extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Mdl_site_security');
		$this->Mdl_site_security->_make_sure_is_admin();

		$this->load->library('session');

		$this->load->model('Back_Model');
	}

	function view_carousel_details($update_id)
	{
		redirect('backend/MainBackend/index_preview/');
	}

	//////// Delete Image Thumbnail
	function delete_image_video($update_id)
	{

		if(!is_numeric($update_id)){
			redirect('backend/src/MainCarousel/manage');
		}

		$data = $this->fetch_data_from_db($update_id);
		$small_imgs = $data['carousel_small_imgs'];

		$small_imgs_path = './gallery/carousel/video/small_imgs/'.$small_imgs;

		// Remove the images
		if(file_exists($small_imgs_path)){
			unlink($small_imgs_path);
		}

		// Update Remove the images to database
		unset($data);
		$data['carousel_small_imgs'] = "";
		$this->_update($update_id, $data);

		$flash_msg = "The image video was successfully deleted."; // Alert message
		$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
		$this->session->set_flashdata('item', $value); // สร้าง session alert

		redirect('backend/src/MainCarousel/create/'.$update_id);
	}

	//////// Upload Image Slide Do Upload
	function do_upload_image_video($update_id)
	{
		if(!is_numeric($update_id)){
			redirect('backend/src/MainCarousel/manage');
		}	    

		$submit = $this->input->post('submit', TRUE);
		if($submit == "Cancel"){
			redirect('backend/src/MainCarousel/create/'.$update_id);
		}

		$config['upload_path'] = './gallery/carousel/video/small_imgs/';
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size'] = 300;
		$config['max_width'] = 2100;
		$config['max_height'] = 1100;

		$this->load->library('upload', $config);

		if(!$this->upload->do_upload('userfile'))
		{
			$data['error'] = array('error' => $this->upload->display_errors("<p style='color:red;'>","</p>"));
			$data['headline'] = "Upload Error";
			$data['update_id'] = $update_id;
			
			$data['content'] = 'backend/carousel/upload_image_video';

			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);

		}else{
			// Upload Successful
			
			$data = array('upload_data' => $this->upload->data());

			$upload_data = $data['upload_data'];
			$file_name = $upload_data['file_name'];

			// Update Image to Database
			$update_data['carousel_small_imgs'] = explode('.', $file_name)[0]."_thumb.".explode('.', $file_name)[1];
			$this->_update($update_id, $update_data);

			$data['headline'] = "Upload Success";
			$data['update_id'] = $update_id;
			
			$data['content'] = 'backend/carousel/upload_image_video';
			
			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}
	}

	//////// Upload Image Slide
	function upload_image_video($update_id)
	{
		if(!is_numeric($update_id)){
			redirect('backend/src/MainCarousel/manage');
		}
		
		$data['headline'] = "Upload Image​ for Video Slide";
		$data['update_id'] = $update_id;
		
		$data['content'] = 'backend/carousel/upload_image_video';

		$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
	}

	//////// Delete Video
	function delete_vdo($update_id)
	{
		if(!is_numeric($update_id)){
			redirect('backend/src/MainCarousel/manage');
		}		    

		$data = $this->fetch_data_from_db($update_id);
		$big_imgs = $data['carousel_big_imgs'];

		$big_imgs_path = './gallery/carousel/video/'.$big_imgs;

		// Remove the images
		if(file_exists($big_imgs_path)){
			unlink($big_imgs_path);
		}

		// Update Remove the images to database
		unset($data);
		$data['carousel_big_imgs'] = "";
		$this->_update($update_id, $data);

		$flash_msg = "The Video was successfully deleted."; // Alert message
		$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
		$this->session->set_flashdata('item', $value); // สร้าง session alert

		redirect('backend/src/MainCarousel/create/'.$update_id);
	}

	//////// Upload Video Slide Do Upload
	function do_upload_video($update_id)
	{
		
		if(!is_numeric($update_id)){
			redirect('backend/src/MainCarousel/manage');
		}

		$submit = $this->input->post('submit', TRUE);
		if($submit == "Cancel"){
			redirect('backend/src/MainCarousel/create/'.$update_id);
		}

		$configVideo['upload_path'] = './gallery/carousel/video/';
        $configVideo['max_size'] = '20240000';
        $configVideo['allowed_types'] = 'avi|flv|wmv|mp3|mp4';
        $configVideo['overwrite'] = FALSE;
        $configVideo['remove_spaces'] = TRUE;

		$this->load->library('upload', $configVideo);
		$this->upload->initialize($configVideo);

		if(!$this->upload->do_upload('userfile'))
		{
			$data['error'] = array('error' => $this->upload->display_errors("<p style='color:red;'>","</p>"));
			$data['headline'] = "Upload Error";
			$data['update_id'] = $update_id;
			
			$data['content'] = 'backend/carousel/upload_video';
    
			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);

		}else{
			// Upload Successful
			
			$data = array('upload_data' => $this->upload->data());

			// Update Image to Database

			$upload_data = $data['upload_data'];
			$file_name = $upload_data['file_name'];
			$update_data['carousel_big_imgs'] = $file_name;
			$update_data['carousel_type'] = $this->input->post('video');
			$this->_update($update_id, $update_data);

			$data['headline'] = "Upload Success";
			$data['update_id'] = $update_id;
			
			$data['content'] = 'backend/carousel/upload_video';
    
			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}
	}

	//////// Upload Image Slide
	function upload_video($update_id)
	{
		if(!is_numeric($update_id)){
			redirect('backend/src/MainCarousel/manage');
		}		    
		
		$data['headline'] = "Upload Video Slide";
		$data['update_id'] = $update_id;
		
		$data['content'] = 'backend/carousel/upload_video';

		$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
	}

	//////// Process Delete All 1.Colours 2.Sizes 3.Big Image 4.Small Image 5.Data Record
	function _process_delete($update_id)
	{
		// Delete Slide Logo Big & Small Image
		$data = $this->fetch_data_from_db($update_id);
		$big_imgs = $data['carousel_big_imgs'];
		$small_imgs = $data['carousel_small_imgs'];

		$big_imgs_path = './gallery/carousel/big_imgs/'.$big_imgs;
		$small_imgs_path = './gallery/carousel/small_imgs/'.$small_imgs;

			// Remove the images
			if(file_exists($big_imgs_path)){
				unlink($big_imgs_path);
			}
			if(file_exists($small_imgs_path)){
				unlink($small_imgs_path);
			}

		// Delete Slide Record from tb_maincarousel database
		$this->_delete($update_id);
	}

	//////// Delete Slide Process from controller backend/src/MainCarousel/delete_carousel
	function delete_carousel_process($update_id)
	{
		if(!is_numeric($update_id)){
			redirect('backend/src/MainCarousel/manage');
		}		

		$submit = $this->input->post('submit', TRUE);

		if($submit=="Cancel"){
			redirect('backend/src/MainCarousel/create/'.$update_id);
		}elseif($submit=="Yes - Delete Slide Carousel"){
			$this->_process_delete($update_id);

			$flash_msg = "Deleted Slide was Successfully";
			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
			$this->session->set_flashdata('item', $value);

			redirect('backend/src/MainCarousel/manage');
		}

	}

	//////// Delete Slide Page
	function delete_carousel($update_id)
	{
		if(!is_numeric($update_id)){
			redirect('backend/src/MainCarousel/manage');
		}

		$data['headline'] = "Delete Slide";
		$data['update_id'] = $update_id;
					
		$data['content'] = 'backend/carousel/delete_carousel';

		$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
	}

	//////// Delete Image Thumbnail
	function delete_image($update_id)
	{
		
		if(!is_numeric($update_id)){
			redirect('backend/src/MainCarousel/manage');
		}
	    
		$data = $this->fetch_data_from_db($update_id);
		$big_imgs = $data['carousel_big_imgs'];
		$small_imgs = $data['carousel_small_imgs'];

		$big_imgs_path = './gallery/carousel/big_imgs/'.$big_imgs;
		$small_imgs_path = './gallery/carousel/small_imgs/'.$small_imgs;

		// Remove the images
		if(file_exists($big_imgs_path)){
			unlink($big_imgs_path);
		}
		if(file_exists($small_imgs_path)){
			unlink($small_imgs_path);
		}

		// Update Remove the images to database
		unset($data);
		$data['carousel_big_imgs'] = "";
		$data['carousel_small_imgs'] = "";
		$this->_update($update_id, $data);

		$flash_msg = "The image was successfully deleted."; // Alert message
		$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
		$this->session->set_flashdata('item', $value); // สร้าง session alert

		redirect('backend/src/MainCarousel/create/'.$update_id);
	}

	//////// Set Image Thumbnail
	function _generate_thumbnail($file_name)
	{	

		$config['image_library'] = 'gd2';
		$config['source_image'] = './gallery/carousel/big_imgs/'.$file_name;
		$config['new_image'] = './gallery/carousel/small_imgs/'.$file_name;
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = TRUE;
		$config['width']         = 768;
		$config['height']       = 276;

		$this->load->library('image_lib', $config);

		$this->image_lib->resize();
	}

	//////// Upload Image Slide Do Upload
	function do_upload($update_id)
	{

		if(!is_numeric($update_id)){
			redirect('backend/src/MainCarousel/manage');
		}		    

		$submit = $this->input->post('submit', TRUE);
		if($submit == "Cancel"){
			redirect('backend/src/MainCarousel/create/'.$update_id);
		}

		$config['upload_path'] = './gallery/carousel/big_imgs/';
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size'] = 300;
		$config['max_width'] = 2100;
		$config['max_height'] = 1100;

		$this->load->library('upload', $config);

		if(!$this->upload->do_upload('userfile'))
		{
			$data['error'] = array('error' => $this->upload->display_errors("<p style='color:red;'>","</p>"));
			$data['headline'] = "Upload Error";
			$data['update_id'] = $update_id;
			
			$data['content'] = 'backend/carousel/upload_image';

			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);

		}else{
			// Upload Successful
			
			$data = array('upload_data' => $this->upload->data());

			$upload_data = $data['upload_data'];
			$file_name = $upload_data['file_name'];
			$this->_generate_thumbnail($file_name);

			// Update Image to Database
			$update_data['carousel_big_imgs'] = $file_name;
			$update_data['carousel_small_imgs'] = $file_name;
			$update_data['carousel_type'] = $this->input->post('image');
			$this->_update($update_id, $update_data);

			$data['headline'] = "Upload Success";
			$data['update_id'] = $update_id;
			
			$data['content'] = 'backend/carousel/upload_image';

			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}
	}

	//////// Upload Image Slide
	function upload_image($update_id)
	{
		
		if(!is_numeric($update_id)){
			redirect('backend/src/MainCarousel/manage');
		}
		
		$data['headline'] = "Upload Image​ Slide";
		$data['update_id'] = $update_id;
					
		$data['content'] = 'backend/carousel/upload_image';
		        
		$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
	}

	//////// Add & Update Slide
	function create()
	{

		$update_id = $this->uri->segment(5);
		$submit = $this->input->post('submit', TRUE);

		if($submit == "Cancel"){
			redirect('backend/src/MainCarousel/manage');
		}

		// ถ้า $submit = value->Submit
		if($submit == "Submit"){
			// Process the Form at create.php
			$this->load->library('form_validation'); // Load Library From Validation

			$this->form_validation->set_rules('carousel_title', '<b>โปรดกรอกเฉพาะชื่อ Title</b>', 'required|max_length[240]');
	    	// $this->form_validation->set_rules('carousel_h1', '<b>โปรดเกรอกหัวเรื่อง Slide ขนาด H1</b>', 'required|max_length[70]');
	    	// $this->form_validation->set_rules('carousel_h2', '<b>โปรดเกรอกหัวเรื่อง Slide ขนาด H2</b>', 'required|max_length[70]');
	    	// $this->form_validation->set_rules('carousel_button1', '<b>โปรดใส่ข้อความในปุ่มที่ 1</b>', 'required|max_length[40]');
	    	// $this->form_validation->set_rules('carousel_button2', '<b>โปรดใส่ข้อความในปุ่มที่ 2</b>', 'required|max_length[40]');
	    	// $this->form_validation->set_rules('carousel_description', '<b>โปรดกรอกรายละเอียดบทความ Slide</b>', 'required');
	    	$this->form_validation->set_rules('carousel_status', '<b>โปรดเลือกการเผยแพร่</b>', 'required|numeric');

	    	if($this->form_validation->run() == TRUE){ // ถ้า form_validation ทำงานแล้วถูกต้อง
	    		// Get The Variables
	    		$data = $this->fetch_data_from_post(); // ให้ $data = รับค่าจาก From method="POST"
	    		$data['carousel_url'] = url_title($data['carousel_title']); // item_url = item_title
	    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id

	    			// Update Items Details
	    			$this->_update($update_id, $data); // Update ลง Database
	    			$flash_msg = "Update Slide were Successfully"; // Alert message
					$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
					$this->session->set_flashdata('item', $value); // สร้าง session alert
					redirect('backend/src/MainCarousel/create/'.$update_id);
	    		}else{
	    			// Insert a New Items
	    			$this->_insert($data);
	    			$update_id = $this->get_max(); // Get the ID at new item

	    			$flash_msg = "Insert Slide were Successfully";
					$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
					$this->session->set_flashdata('item', $value);
					redirect('backend/src/MainCarousel/create/'.$update_id);
	    		}
	    	}
		}

		if((is_numeric($update_id)) && ($submit!="Submit")){
			$data = $this->fetch_data_from_db($update_id);
		}else{
			$data = $this->fetch_data_from_post();
			$data['carousel_big_imgs'] = "";
		}

		if(!is_numeric($update_id)){
			$data['headline'] = "เพิ่มข้อมูล Slide Carousel";
		}else{
			$data['headline'] = "แก้ไขข้อมูล Slide Carousel";
		}

		$data['update_id'] = $update_id;
		
		$data['content'] = 'backend/carousel/formCarousel';
					
		$data['replace4'] = 'backend/src/ck/replace4';

		$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
	}

	//////// Slide Detail
	function manage()
	{
		$data['query'] = $this->get('carousel_title');
		
		$data['content'] = 'backend/carousel/manageCarousel';
		
		$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
	}

	//////// Fetch Data at method POST
	function fetch_data_from_post() // Fetch ข้อมูลจาก Input POST
	{
		$data['carousel_title'] = $this->input->post('carousel_title', TRUE);
		$data['carousel_h1'] = $this->input->post('carousel_h1', TRUE);
		$data['carousel_h2'] = $this->input->post('carousel_h2', TRUE);
		$data['carousel_button1'] = $this->input->post('carousel_button1', TRUE);
		$data['carousel_button2'] = $this->input->post('carousel_button2', TRUE);
		$data['carousel_button1_link'] = $this->input->post('carousel_button1_link', TRUE);
		$data['carousel_button2_link'] = $this->input->post('carousel_button2_link', TRUE);
		$data['carousel_description'] = html_escape($this->input->post('carousel_description', FALSE));
		$data['carousel_status'] = $this->input->post('carousel_status', TRUE);

		$data['meta_author'] = $this->input->post('meta_author', TRUE);
		$data['meta_keywords'] = $this->input->post('meta_keywords', TRUE);
		$data['meta_description'] = $this->input->post('meta_description', TRUE);
		
		return $data;
	}

	//////// Fetch Data at Database
	function fetch_data_from_db($update_id) //  Fetch ข้อมูลจาก Database
	{

		if(!is_numeric($update_id)){
			redirect('backend/src/Header/manage');
		}

		$query = $this->get_where($update_id);
		foreach($query->result() as $row){

			$data['carousel_title'] = $row->carousel_title;
			$data['carousel_url'] = $row->carousel_url;
			$data['carousel_h1'] = $row->carousel_h1;
			$data['carousel_h2'] = $row->carousel_h2;
			$data['carousel_button1'] = $row->carousel_button1;
			$data['carousel_button2'] = $row->carousel_button2;
			$data['carousel_button1_link'] = $row->carousel_button1_link;
			$data['carousel_button2_link'] = $row->carousel_button2_link;
			$data['carousel_description'] = $row->carousel_description;
			$data['carousel_status'] = $row->carousel_status;
			$data['carousel_big_imgs'] = $row->carousel_big_imgs;
			$data['carousel_small_imgs'] = $row->carousel_small_imgs;
			$data['carousel_type'] = $row->carousel_type;
			$data['created_at'] = $row->created_at;
			$data['updated_at'] = $row->updated_at;
			
			$data['meta_author'] = $row->meta_author;
			$data['meta_keywords'] = $row->meta_keywords;
			$data['meta_description'] = $row->meta_description;
		}

		if(!isset($data)){
			$data = "";
		}

		return $data;
	}

	function get($order_by)
	{
		$this->load->model('Mdl_formcarousel');
		$query = $this->Mdl_formcarousel->get($order_by);
		return $query;
	}

	function get_with_limit($limit, $offset, $order_by)
	{
		$this->load->model('Mdl_formcarousel');
		$query = $this->Mdl_formcarousel->get_with_limit($limit, $offset, $order_by);
		return $query;
	}

	function get_where($id)
	{
		$this->load->model('Mdl_formcarousel');
		$query = $this->Mdl_formcarousel->get_where($id);
		return $query;
	}

	function get_where_custom($col, $value)
	{
		$this->load->model('Mdl_formcarousel');
		$query = $this->Mdl_formcarousel->get_where_custom($col, $value);
		return $query;
	}

	function _insert($data)
	{
		$this->load->model('Mdl_formcarousel');
		$this->Mdl_formcarousel->_insert($data);
	}

	function _update($id, $data)
	{
		$this->load->model('Mdl_formcarousel');
		$this->Mdl_formcarousel->_update($id, $data);
	}

	function _delete($id)
	{
		$this->load->model('Mdl_formcarousel');
		$this->Mdl_formcarousel->_delete($id);
	}

	function count_where($column, $value)
	{
		$this->load->model('Mdl_formcarousel');
		$count = $this->Mdl_formcarousel->count_where($column, $value);
		return $count;
	}

	function get_max()
	{
		$this->load->model('Mdl_formcarousel');
		$max_id = $this->Mdl_formcarousel->get_max();
		return $max_id;
	}

	function _custom_query($mysql_query)
	{
		$this->load->model('Mdl_formcarousel');
		$query = $this->Mdl_formcarousel->_custom_query($mysql_query);
		return $query;
	}
}
