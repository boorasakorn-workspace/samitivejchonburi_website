<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class DoctorWork extends CI_Controller {

		function __construct()
		{
			parent::__construct();

			$this->load->model('Mdl_site_security');
			$this->Mdl_site_security->_make_sure_is_admin();

			$this->load->library('session');

			$this->load->model('Back_Model');
		}

		public function language(){
			if(empty($this->session->userdata('language'))){
				$this->session->set_userdata("language", 'thai');
				$this->session->set_userdata("q_l", ' ');
			}
			$this->session->set_userdata("q_l", '_'.substr($this->session->userdata('language'), 0,2));
			if($this->session->userdata('q_l') == 'th' || $this->session->userdata('q_l') == '_th' ){
				$this->session->set_userdata("q_l", ' ');			
			}
			return $this->lang->load($this->session->userdata('language'), 'english');
				
		}

		public function change_language($lang){
			$this->session->set_userdata("language", $lang);
			$this->session->set_userdata("q_l", '_'.substr($lang, 0,2));
			if($this->session->userdata('q_l') == 'th' || $this->session->userdata('q_l') == '_th' ){
				$this->session->set_userdata("q_l", ' ');			
			}
			redirect($_SERVER['HTTP_REFERER']);
		}

		function view_doctorwork_details($update_id)
		{
			redirect('backend/src/DoctorWork/manage');
		}

		//////// Process Delete All
		function _process_delete($update_id)
		{
			// Delete Doctor Work
			$data = $this->fetch_data_from_db($update_id);

			// Delete Doctor Work Record from tb_workdayhours database
			$this->_delete($update_id);
		}

		//////// Delete Doctor Work Process from controller backend/src/DoctorWork/delete_doctorwork
		function delete_doctorwork_process($update_id)
		{			
			if(!is_numeric($update_id)){
				redirect('backend/src/DoctorWork/manage');
			}			

			$submit = $this->input->post('submit', TRUE);

			if($submit=="Cancel"){
				redirect('backend/src/DoctorWork/create/'.$update_id);
			}elseif($submit=="Yes - Delete Doctor Work"){
				$this->_process_delete($update_id);

				$flash_msg = "Deleted Doctor Work was Successfully";
				$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
				$this->session->set_flashdata('item', $value);

				redirect('backend/src/DoctorWork/manage');
			}
		}

		//////// Delete Doctor Work Page
		function delete_doctorwork($update_id)
		{

			if(!is_numeric($update_id)){
				redirect('backend/src/DoctorWork/manage');
			}			

			$data['headline'] = "Delete Doctor Work";
			$data['update_id'] = $update_id;
			
			$data['content'] = 'backend/doctor/doctorwork/delete_doctorwork';
			
			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

		//////// Add & Update Doctor Work
		function create()
		{

			error_reporting (E_ALL ^ E_NOTICE);
			$update_id = $this->uri->segment(5);
			$submit = $this->input->post('submit', TRUE);

			if($submit == "Cancel"){
				redirect('backend/src/DoctorWork/manage');
			}

			// ถ้า $submit = value->Submit
			if($submit == "Submit"){
				// Process the Form at create.php
				$this->load->library('form_validation'); // Load Library From Validation

				$this->form_validation->set_rules('caption_date', '<b>โปรดกรอกวันทำงาน Ex. วันจันทร์</b>', 'required|max_length[150]');
		    	$this->form_validation->set_rules('start_time', '<b>โปรดใส่เวลาเข้างาน</b>', 'required');
		    	$this->form_validation->set_rules('end_time', '<b>โปรดใส่เวลาออกงาน</b>', 'required');

		    	if($this->form_validation->run() == TRUE){ // ถ้า form_validation ทำงานแล้วถูกต้อง
		    		// Get The Variables
					$data = $this->fetch_data_from_post(); // ให้ $data = รับค่าจาก From method="POST"
					unset($data['submit']);
		    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id

		    			// Update Doctor Work Details
		    			$this->_update($update_id, $data); // Update ลง Database
		    			$flash_msg = "Update Doctor Work were Successfully"; // Alert message
						$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
						$this->session->set_flashdata('item', $value); // สร้าง session alert
						redirect('backend/src/DoctorWork/create/'.$update_id);
		    		}else{
		    			// Insert a New Doctor
		    			$this->_insert($data);
		    			$update_id = $this->get_max(); // Get the ID at new Doctor

		    			$flash_msg = "Insert Doctor Work were Successfully";
						$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
						$this->session->set_flashdata('item', $value);
						redirect('backend/src/DoctorWork/create/'.$update_id);
		    		}
		    	}
			}

			if((is_numeric($update_id)) && ($submit!="Submit")){
				$data = $this->fetch_data_from_db($update_id);
			}else{
				$data = $this->fetch_data_from_post();
			}

			if(!is_numeric($update_id)){
				$data['headline'] = "เพิ่มข้อมูล Doctor Work";
			}else{
				$data['headline'] = "แก้ไขข้อมูล Doctor Work";
			}

			if(!is_numeric($update_id)){
				$mysql_query = "
				SELECT  ms_care.uid as care_uid,
				ms_care.medical_id,
				ms_care.forename as firstname_th,
				ms_care.surname as lastname_th,
				ms_title.titlename as pname_th
				FROM ms_care
				LEFT JOIN ms_title on ms_title.uid = ms_care.ms_title_uid
				WHERE ms_care.status = 0";

				//$mysql_query = "select medical_id,pname_th,firstname_th,lastname_th from tb_doctor where status=1";
				$query = $this->_custom_query($mysql_query);

				$data['query'] = $query->result();
			}else{
				$fetchid = $this->get_where($update_id);

				$mysql_query = "
				SELECT  ms_care.uid as care_uid,
				ms_care.medical_id,
				ms_care.forename as firstname_th,
				ms_care.surname as lastname_th,
				ms_title.titlename as pname_th,
				ms_schedule.uid as id,
				ms_schedule.ms_careprovider_uid,
				ms_schedule.caption_date,
				ms_schedule.start_time,
				ms_schedule.end_time,
				ms_schedule.remark
				FROM ms_care
				INNER JOIN ms_schedule on ms_schedule.ms_careprovider_uid = ms_care.uid
				LEFT JOIN ms_title on ms_title.uid = ms_care.ms_title_uid
				WHERE ms_schedule.uid=$update_id";
				$query = $this->_custom_query($mysql_query);


				$mysql_query2 = "
				SELECT  ms_care.uid as care_uid,
				ms_care.medical_id,
				ms_care.forename as firstname_th,
				ms_care.surname as lastname_th,
				ms_title.titlename as pname_th
				FROM ms_care
				LEFT JOIN ms_title on ms_title.uid = ms_care.ms_title_uid
				WHERE ms_care.status = 0";

				//$mysql_query = "select medical_id,pname_th,firstname_th,lastname_th from tb_doctor where status=1";
				$query2 = $this->_custom_query($mysql_query2);

				$data= $query->row_array();
			$data['fetchid'] = $fetchid->row();
			$data['result'] = $query2->result();
			}

			$data['update_id'] = $update_id;
			
			$data['content'] = 'backend/doctor/doctorwork/formDoctorWork';

			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

		//////// Doctor Work Detail
		function manage()
		{
			$mysql_query = "
				SELECT ms_care.uid as care_uid,
				ms_care.medical_id,
				ms_care.forename as firstname_th,
				ms_care.surname as lastname_th,
				ms_title.titlename as pname_th,
				ms_schedule.uid as id,
				ms_schedule.ms_careprovider_uid,
				ms_schedule.caption_date,
				ms_schedule.start_time,
				ms_schedule.end_time,
				ms_schedule.remark
				FROM ms_care
				INNER JOIN ms_schedule on ms_schedule.ms_careprovider_uid = ms_care.uid
				LEFT JOIN ms_title on ms_title.uid = ms_care.ms_title_uid			
			";
			$fetch = $this->_custom_query($mysql_query);

			// $mysql_query = "select
			// 	tb_doctor.medical_id,
			// 	tb_doctor.pname_th,
			// 	tb_doctor.firstname_th,
			// 	tb_doctor.lastname_th,
			// 	tb_workdayhours.id,
			// 	tb_workdayhours.medical_id,
			// 	tb_workdayhours.work_days,
			// 	tb_workdayhours.out_of_days,
			// 	tb_workdayhours.work_times_in,
			// 	tb_workdayhours.work_times_out
			// 	from tb_doctor
			// 	inner join tb_workdayhours on tb_doctor.medical_id=tb_workdayhours.medical_id
			// 	where tb_doctor.medical_id=tb_workdayhours.medical_id";
			// $fetch = $this->_custom_query($mysql_query);
			
			$data['query'] = $fetch;
			
			$data['content'] = 'backend/doctor/doctorwork/manageDoctorWork';			

			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

		//////// Fetch Data at method POST
		function fetch_data_from_post() // Fetch ข้อมูลจาก Input POST
		{
			$data = $this->input->post();
			/*
			$data['work_days'] = $this->input->post('work_days', TRUE);
			$data['out_of_days'] = $this->input->post('out_of_days', TRUE);
			$data['work_times_in'] = $this->input->post('work_times_in', TRUE);
			$data['work_times_out'] = $this->input->post('work_times_out', TRUE);
			$data['medical_id'] = $this->input->post('medical_id', TRUE);
			*/
			return $data;
		}

		//////// Fetch Data at Database
		function fetch_data_from_db($update_id) //  Fetch ข้อมูลจาก Database
		{

			if(!is_numeric($update_id)){
				redirect('backend/src/DoctorWork/manage');
			}

			$query = $this->get_where($update_id);
			$query = $query->row_array();
			/*
			foreach($query->result() as $row){

				$data['work_days'] = $row->work_days;
				$data['out_of_days'] = $row->out_of_days;
				$data['work_times_in'] = $row->work_times_in;
				$data['work_times_out'] = $row->work_times_out;
				$data['medical_id'] = $row->medical_id;
			}
			*/
			if(!isset($data)){
				$data = "";
			}

			return $data;
		}

		function get($order_by)
		{
			$this->load->model('Mdl_formdoctorwork');
			$query = $this->Mdl_formdoctorwork->get($order_by);
			return $query;
		}

		function get_with_limit($limit, $offset, $order_by)
		{
			$this->load->model('Mdl_formdoctorwork');
			$query = $this->Mdl_formdoctorwork->get_with_limit($limit, $offset, $order_by);
			return $query;
		}

		function get_where($id)
		{
			$this->load->model('Mdl_formdoctorwork');
			$query = $this->Mdl_formdoctorwork->get_where($id);
			return $query;
		}

		function get_where_custom($col, $value)
		{
			$this->load->model('Mdl_formdoctorwork');
			$query = $this->Mdl_formdoctorwork->get_where_custom($col, $value);
			return $query;
		}

		function _insert($data)
		{
			$this->load->model('Mdl_formdoctorwork');
			$this->Mdl_formdoctorwork->_insert($data);
		}

		function _update($id, $data)
		{
			$this->load->model('Mdl_formdoctorwork');
			$this->Mdl_formdoctorwork->_update($id, $data);
		}

		function _delete($id)
		{
			$this->load->model('Mdl_formdoctorwork');
			$this->Mdl_formdoctorwork->_delete($id);
		}

		function count_where($column, $value)
		{
			$this->load->model('Mdl_formdoctorwork');
			$count = $this->Mdl_formdoctorwork->count_where($column, $value);
			return $count;
		}

		function get_max()
		{
			$this->load->model('Mdl_formdoctorwork');
			$max_id = $this->Mdl_formdoctorwork->get_max();
			return $max_id;
		}

		function _custom_query($mysql_query)
		{
			$this->load->model('Mdl_formdoctorwork');
			$query = $this->Mdl_formdoctorwork->_custom_query($mysql_query);
			return $query;
		}


		//////// Add & Update Doctor Work
		function mini_create()
		{

			$update_id = $this->uri->segment(5);
			$submit = $this->input->post('submit', TRUE);

			if($submit == "Cancel"){
				redirect('backend/src/DoctorWork/manage');
			}

			// ถ้า $submit = value->Submit
			if($submit == "Submit"){
				// Process the Form at create.php
				$this->load->library('form_validation'); // Load Library From Validation

				$this->form_validation->set_rules('work_days', '<b>โปรดกรอกวันทำงาน Ex. วันจันทร์ - ศุกร์</b>', 'required|max_length[150]');
		    	$this->form_validation->set_rules('work_times_in', '<b>โปรดใส่เวลาเข้างาน</b>', 'required');
		    	$this->form_validation->set_rules('work_times_out', '<b>โปรดใส่เวลาออกงาน</b>', 'required');

		    	if($this->form_validation->run() == TRUE){ // ถ้า form_validation ทำงานแล้วถูกต้อง
		    		// Get The Variables
		    		$data = $this->fetch_data_from_post(); // ให้ $data = รับค่าจาก From method="POST"
		    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id

		    			// Update Doctor Work Details
		    			$this->_update($update_id, $data); // Update ลง Database
		    			$flash_msg = "Update Doctor Work were Successfully"; // Alert message
						$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
						$this->session->set_flashdata('item', $value); // สร้าง session alert
						redirect('backend/src/DoctorWork/mini_create/'.$update_id);
		    		}else{
		    			// Insert a New Doctor
		    			$this->_insert($data);
		    			$update_id = $this->get_max(); // Get the ID at new Doctor

		    			$flash_msg = "Insert Doctor Work were Successfully";
						$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
						$this->session->set_flashdata('item', $value);
						redirect('backend/src/DoctorWork/mini_create/'.$update_id);
		    		}
		    	}
			}

			if((is_numeric($update_id)) && ($submit!="Submit")){
				$data = $this->fetch_data_from_db($update_id);
			}else{
				$data = $this->fetch_data_from_post();
			}

			if(!is_numeric($update_id)){
				$data['headline'] = "เพิ่มข้อมูล Doctor Work";
			}else{
				$data['headline'] = "แก้ไขข้อมูล Doctor Work";
			}

			if(!is_numeric($update_id)){
				$mysql_query = "select medical_id,pname_th,firstname_th,lastname_th from tb_doctor where status=1";
				$query = $this->_custom_query($mysql_query);

				$data['query'] = $query->result();
			}else{
				$fetchid = $this->get_where($update_id);

				$mysql_query = "select
				tb_doctor.id,
				tb_doctor.medical_id,
				tb_doctor.pname_th,
				tb_doctor.firstname_th,
				tb_doctor.lastname_th,
				tb_workdayhours.id,
				tb_workdayhours.medical_id,
				tb_workdayhours.work_days,
				tb_workdayhours.out_of_days,
				tb_workdayhours.work_times_in,
				tb_workdayhours.work_times_out
				from tb_doctor
				inner join tb_workdayhours on tb_doctor.medical_id=tb_workdayhours.medical_id
				where tb_doctor.medical_id=tb_workdayhours.medical_id
				and
				tb_workdayhours.id=$update_id";
				$query = $this->_custom_query($mysql_query);


				$mysql_query2 = "select medical_id,pname_th,firstname_th,lastname_th from tb_doctor where status=1";
				$query2 = $this->_custom_query($mysql_query2);

			$data['fetchid'] = $fetchid->row();
			$data['query'] = $query->row();
			$data['result'] = $query2->result();
			}

			$data['update_id'] = $update_id;
			
			$data['content'] = 'backend/doctor/doctorwork/formDoctorWork_mini';

			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}



	}
