<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BlogHealthy extends CI_Controller {
	function __construct()
	{
		parent::__construct();

		$this->load->model('Mdl_site_security');
		$this->Mdl_site_security->_make_sure_is_admin();

		$this->load->library('session');

		$this->load->model('Back_Model');
	}

	public function work()
	{
		$this->load->view('work_add');
	}

	function view_blog_details($update_id)
	{			
		redirect('backend/MainBackend/HealthyblogDetails/'.$update_id);
	}

	public function language()
	{
		if(empty($this->session->userdata('language'))){
			$this->session->set_userdata("language", 'thai');
			$this->session->set_userdata("q_l", ' ');
		}
		$this->session->set_userdata("q_l", '_'.substr($this->session->userdata('language'), 0,2));
		if($this->session->userdata('q_l') == 'th' || $this->session->userdata('q_l') == '_th' ){
			$this->session->set_userdata("q_l", ' ');			
		}
		return $this->lang->load($this->session->userdata('language'), 'english');			
	}

	public function change_language($lang)
	{
		$this->session->set_userdata("language", $lang);
		$this->session->set_userdata("q_l", '_'.substr($lang, 0,2));
		if($this->session->userdata('q_l') == 'th' || $this->session->userdata('q_l') == '_th' ){
			$this->session->set_userdata("q_l", ' ');			
		}
		redirect($_SERVER['HTTP_REFERER']);
	}

	function preview_Contents()
	{			
		$this->language();
		
		$this->load->model('Front_Model');
		$this->load->model('Front_Home_Model');
		$data = $this->Front_Model->Detailpage();

		$data['query'] = json_decode(json_encode($this->input->post()));
		$data['healthyblogDetails'] = 'frontend/src/healthyblog/healthyblog_details';

		$data['get_doctor']=$this->Front_Home_Model->id_doctor($data['query']->doc_id);
		$data['all_med_doctor']=$this->Front_Home_Model->lang_all_medicial_center();

		if(isset($data['query']->promotion_link_1) && $data['query']->promotion_link_1 > 0){
			$data['promotion_1'] = $this->Front_Home_Model->id_promotion($data['query']->promotion_link_1);	
		}
		if(isset($data['query']->promotion_link_2) && $data['query']->promotion_link_2 > 0){
			$data['promotion_2'] = $this->Front_Home_Model->id_promotion($data['query']->promotion_link_2);
		}

		$data['previewContent'] = "previewContent";
		$this->load->view('frontend/homepage', $data);
	}

	//////// Process Delete All 1.Colours 2.Sizes 3.Big Image 4.Small Image 5.Data Record
	function _process_delete($update_id)
	{
		// Delete Blog Logo Big & Small Image
		$data = $this->fetch_data_from_db($update_id);
		$big_imgs = $data['blog_big_img'];
		$small_imgs = $data['blog_small_img'];

		$big_imgs_path = './gallery/blog/big_imgs/'.$big_imgs;
		$small_imgs_path = './gallery/blog/small_imgs/'.$small_imgs;

			// Remove the images
			if(file_exists($big_imgs_path)){
				unlink($big_imgs_path);
			}
			if(file_exists($small_imgs_path)){
				unlink($small_imgs_path);
			}

		// Delete Items Record from tb_bloghealthy database
		$this->_delete($update_id);
	}

	//////// Delete Blog Process from controller backend/src/BlogHealthy/delete_blog
	function delete_blog_process($update_id)
	{
		if(!is_numeric($update_id)){
			redirect('backend/src/BlogHealthy/manage');
		}

		$submit = $this->input->post('submit', TRUE);

		if($submit=="Cancel"){
			redirect('backend/src/BlogHealthy/create/'.$update_id);
		}elseif($submit=="Yes - Delete Blog"){
			$this->_process_delete($update_id);

			$flash_msg = "Deleted Blog was Successfully";
			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
			$this->session->set_flashdata('item', $value);

			redirect('backend/src/BlogHealthy/manage');
		}

	}

	//////// Delete Blog Page
	function delete_blog($update_id)
	{
		if(!is_numeric($update_id)){
			redirect('backend/src/BlogHealthy/manage');
		}
		
		$data['headline'] = "Delete Blog";
		$data['update_id'] = $update_id;
		
		$data['content'] = 'backend/bloghealthy/delete_blog';	
		
		$data = $this->Back_Model->General($data);
		$this->load->view('backend/dashboard', $data);
	}

	//////// Delete Image Thumbnail
	function delete_image($update_id)
	{				
		if(!is_numeric($update_id)){
			redirect('backend/src/BlogHealthy/manage');
		}    

		$data = $this->fetch_data_from_db($update_id);
		$big_imgs = $data['blog_big_img'];
		$small_imgs = $data['blog_small_img'];

		$big_imgs_path = './gallery/blog/big_imgs/'.$big_imgs;
		$small_imgs_path = './gallery/blog/small_imgs/'.$small_imgs;

		// Remove the images
		if(file_exists($big_imgs_path)){
			unlink($big_imgs_path);
		}
		if(file_exists($small_imgs_path)){
			unlink($small_imgs_path);
		}

		// Update Remove the images to database
		unset($data);
		$data['blog_big_img'] = "";
		$data['blog_small_img'] = "";
		$this->_update($update_id, $data);

		$flash_msg = "The image was successfully deleted."; // Alert message
		$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
		$this->session->set_flashdata('item', $value); // สร้าง session alert

		redirect('backend/src/BlogHealthy/create/'.$update_id);
	}

	//////// Set Image Thumbnail
	function _generate_thumbnail($file_name)
	{
		$config['image_library'] = 'gd2';
		$config['source_image'] = './gallery/blog/big_imgs/'.$file_name;
		$config['new_image'] = './gallery/blog/small_imgs/'.$file_name;
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = TRUE;
		$config['width']         = 360;
		$config['height']       = 208;

		$this->load->library('image_lib', $config);

		$this->image_lib->resize();
	}

	//////// Upload Image Blog Do Upload
	function do_upload($update_id)
	{				
		if(!is_numeric($update_id)){
			redirect('backend/src/BlogHealthy/manage');
		}	    

		$submit = $this->input->post('submit', TRUE);
		if($submit == "Cancel"){
			redirect('backend/src/BlogHealthy/create/'.$update_id);
		}

		$config['upload_path'] = './gallery/blog/big_imgs/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = 200;
		$config['max_width'] = 1500;
		$config['max_height'] = 1268;

		$this->load->library('upload', $config);

		if(!$this->upload->do_upload('userfile'))
		{
			$data['error'] = array('error' => $this->upload->display_errors("<p style='color:red;'>","</p>"));
			$data['headline'] = "Upload Error";
			$data['update_id'] = $update_id;
			
			$data['content'] = 'backend/bloghealthy/upload_image';						
		
			$data = $this->Back_Model->General($data);
			$this->load->view('backend/dashboard', $data);

		}else{
			// Upload Successful
			
			$data = array('upload_data' => $this->upload->data());

			$upload_data = $data['upload_data'];
			$file_name = $upload_data['file_name'];
			$this->_generate_thumbnail($file_name);

			// Update Image to Database
			$update_data['blog_big_img'] = $file_name;
			$update_data['blog_small_img'] = explode('.', $file_name)[0]."_thumb.".explode('.', $file_name)[1];
			$this->_update($update_id, $update_data);

			$data['headline'] = "Upload Success";
			$data['update_id'] = $update_id;
			
			$data['content'] = 'backend/bloghealthy/upload_image';
		
			$data = $this->Back_Model->General($data);
			$this->load->view('backend/dashboard', $data);
		}
	}

	// PREVIEW IMAGE //
	function modal_do_upload($update_id)
	{

		if(!is_numeric($update_id)){
			redirect('backend/src/BlogHealthy/manage');
		}

		$config['upload_path'] = './gallery/blog/preview_imgs/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = 200;
		$config['max_width'] = 1500;
		$config['max_height'] = 1268;

		$this->load->library('upload', $config);

		if(!$this->upload->do_upload('file'))
		{
			// Upload Error

			$data['error'] = array('error' => $this->upload->display_errors("<p style='color:red;'>","</p>"));
			$data['headline'] = "Upload Error";
			$data['update_id'] = $update_id;
	
		}else{
			// Upload Successful
			
			$data = array('upload_data' => $this->upload->data());

			$upload_data = $data['upload_data'];
			$file_name = $upload_data['file_name'];
			$this->_generate_thumbnail($file_name);

			// Update Image to Database
			$update_data['blog_preview_img'] = $file_name;
			$this->_update($update_id, $update_data);

			$data['headline'] = "Upload Success";
			$data['update_id'] = $update_id;
			echo json_encode($update_data['blog_preview_img']);

		}
	}
	function fetch_preview_image($update_id) //  Fetch ข้อมูลจาก Database
	{			
		if(!is_numeric($update_id)){
			redirect('backend/src/BlogHealthy/manage');
		}

		$query = $this->get_where($update_id);
		foreach($query->result() as $row){				
			$data['blog_preview_img'] = $row->blog_preview_img;
		}

		if(!isset($data)){
			$data = "";
		}

		return $data;
	}
	function delete_preview($update_id)
	{	
		
		if(!is_numeric($update_id)){
			redirect('backend/src/BlogHealthy/manage');
		}

		$data = $this->fetch_preview_image($update_id);
		$preview_imgs = $data['blog_preview_img'];

		$preview_imgs_path = './gallery/blog/preview_imgs/'.$preview_imgs;

		// Remove the images
		if(file_exists($preview_imgs_path)){
			unlink($preview_imgs_path);
		}

		unset($data);
		$data['blog_preview_img'] = "";
		$this->_update($update_id, $data);
		echo json_encode("Success");
	}
	function upload_preview($update_id){
		$preview_img_name = $this->fetch_preview_image($update_id)['blog_preview_img'];			
		if($preview_img_name != ""){				
			$preview_path = 'gallery/blog/preview_imgs/'.$preview_img_name;
			$big_path = 'gallery/blog/big_imgs/'.$preview_img_name;
			copy($preview_path, $big_path);
			$this->_generate_thumbnail($preview_img_name);

			$update_data['blog_big_img'] = $preview_img_name;
			$update_data['blog_small_img'] = explode('.', $preview_img_name)[0]."_thumb.".explode('.', $preview_img_name)[1];
			$this->_update($update_id, $update_data);

			$this->delete_preview($update_id);
		}
	}
	// PREVIEW IMAGE //

	//////// Upload Image Blog
	function upload_image($update_id)
	{

		if(!is_numeric($update_id)){
			redirect('backend/src/BlogHealthy/manage');
		}
		
		$data['headline'] = "Upload Image​ Blog";
		$data['update_id'] = $update_id;
		
		$data['content'] = 'backend/BlogHealthy/upload_image';		
		
		$data = $this->Back_Model->General($data);
		$this->load->view('backend/dashboard', $data);
	}

	//////// Add & Update Blog
	function create()
	{		
		$this->load->library('Timedate');		
  
		$update_id = $this->uri->segment(5);
		if(isset(explode('_',$update_id)[1])){
			$lang = "_".explode('_',$update_id)[1];
		}
		else{
			$lang = "";
		}
		$update_id = explode('_',$update_id)[0];
		$submit = $this->input->post('submit', TRUE);

		if($submit == "Cancel"){
			redirect('backend/src/BlogHealthy/manage');
		}

		// ถ้า $submit = value->Submit
		if($submit == "Submit"){
			// Process the Form at create.php
			$this->load->library('form_validation'); // Load Library From Validation

			$this->form_validation->set_rules('blog_title', '<b>โปรดกรอกเฉพาะชื่อ Title</b>', 'required|max_length[240]');
	    	$this->form_validation->set_rules('blog_status', '<b>โปรดเลือกการเผยแพร่</b>', 'required|numeric');

	    	if($this->form_validation->run() == TRUE){ // ถ้า form_validation ทำงานแล้วถูกต้อง
	    		// Get The Variables
	    		$data = $this->fetch_data_from_post(); // ให้ $data = รับค่าจาก From method="POST"
	    		$title = $this->url_slug($data['blog_title']);
	    		$data['blog_url'] = $title; // item_url = item_title

	    		//$data['date_published'] = $this->timedate->make_timestamp_from_datepicker($data['date_published']);
	    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id
	    			$this->upload_preview($update_id);
	    			// Update Items Details		    			
	    			$data['m_user'] = $this->session->userdata('ses_id');
	    			$this->_update($update_id, $data); // Update ลง Database
	    			$flash_msg = "Update Blog were Successfully"; // Alert message
					$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
					$this->session->set_flashdata('item', $value); // สร้าง session alert
					redirect('backend/src/bloghealthy/create/'.$update_id);
	    		}else{
	    			// Insert a New Items
	    			$data['c_user'] = $this->session->userdata('ses_id');
	    			$this->_insert($data);
	    			$update_id = $this->get_max(); // Get the ID at new item

	    			$flash_msg = "Insert Blog were Successfully";
					$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
					$this->session->set_flashdata('item', $value);
					redirect('backend/src/bloghealthy/create/'.$update_id);
	    		}
	    	}
		}

		if((is_numeric($update_id)) && ($submit!="Submit")){
			$data = $this->fetch_data_from_db($update_id);
		}else{
			$data = $this->fetch_data_from_post();
			$data['blog_big_img'] = "";
		}

		if(!is_numeric($update_id)){
			$data['headline'] = "เพิ่มข้อมูลบทความสุขภาพ";
		}else{
			$data['headline'] = "แก้ไขข้อมูลบทความสุขภาพ";
		}
		/*
		if($data['date_published'] > 0){
			// Format unique timestamp to datepicker
			$data['date_published'];
		}
		*/

		$this->load->model('Mdl_alltable');
		$data['content_page'] = $this->Mdl_alltable->get_package();
		$data['doctor_content_page'] = $this->Mdl_alltable->get_doctor();
		
		$data['update_id'] = $update_id.$lang;
		
		$data['content'] = 'backend/bloghealthy/formBlogHealthy';
		
		$data['Additionalscript'] = 'backend/bloghealthy/scriptBlog';
		$data['replace1'] = 'backend/src/ck/replace1';
		
		
		$data = $this->Back_Model->General($data);
		$this->load->view('backend/dashboard', $data);
	}

	//////// Blog Healthy Detail
	function manage()
	{
  
		$data['query'] = $this->get('blog_title');
		
		$data['content'] = 'backend/bloghealthy/manageBlogHealthy';
		
		$data = $this->Back_Model->General($data);
		$this->load->view('backend/dashboard', $data);
	}

	function url_slug($text)
	{
		$array = array("'", '"', '\\', '/', '.', ',', '!', '+', '*', '%', '$', '#', '@', '(', ')', '[', ']', '?', '>', '<', '|', '’', '“', '”', '‘', '’', ';', ':', '–');
		$text = str_replace($array, '', $text); // Replaces all spaces with hyphens.
		$text = preg_replace('/[^A-Za-z0-9ก-๙เ-า\\-]/', '-', $text); // Removes special chars.
		$text = preg_replace('/-+/', "-", $text);
		$text = trim($text, '-');
		$text = urldecode($text);

		if (empty($text)){
			return NULL;
		}

		return $text;
	}

	//////// Fetch Data at method POST
	function fetch_data_from_post() // Fetch ข้อมูลจาก Input POST
	{
		$data['blog_title'] = $this->input->post('blog_title', TRUE);
		$data['blog_title_en'] = $this->input->post('blog_title_en', TRUE);
		$data['blog_contents'] = html_escape($this->input->post('blog_contents', FALSE));
		$data['blog_contents_en'] = html_escape($this->input->post('blog_contents_en', FALSE));
		$data['blog_keywords'] = $this->input->post('blog_keywords', TRUE);
		$data['blog_description'] = $this->input->post('blog_description', TRUE);
		$data['blog_description_en'] = $this->input->post('blog_description_en', TRUE);
		$data['blog_status'] = $this->input->post('blog_status', TRUE);
		$data['date_published'] = $this->input->post('date_published', TRUE);
		$data['blog_video'] = $this->input->post('blog_video', TRUE);

		$data['promotion_link_1'] = $this->input->post('promotion_link_1', TRUE);
		$data['promotion_link_2'] = $this->input->post('promotion_link_2', TRUE);

		$data['meta_author'] = $this->input->post('meta_author', TRUE);
		$data['meta_keywords'] = $this->input->post('meta_keywords', TRUE);
		$data['meta_description'] = $this->input->post('meta_description', TRUE);

		$data['doc_id'] = $this->input->post('doc_id', TRUE);
		
		return $data;
	}

	//////// Fetch Data at Database
	function fetch_data_from_db($update_id) //  Fetch ข้อมูลจาก Database
	{
		
		if(!is_numeric($update_id)){
			redirect('backend/src/BlogHealthy/manage');
		}

		$query = $this->get_where($update_id);
		foreach($query->result() as $row){
			
			$data['blog_url'] = $row->blog_url;
			$data['blog_title'] = $row->blog_title;
			$data['blog_title_en'] = $row->blog_title_en;
			$data['blog_contents'] = $row->blog_contents;
			$data['blog_contents_en'] = $row->blog_contents_en;
			$data['blog_keywords'] = $row->blog_keywords;
			$data['blog_description'] = $row->blog_description;
			$data['blog_description_en'] = $row->blog_description_en;
			$data['blog_status'] = $row->blog_status;
			$data['blog_big_img'] = $row->blog_big_img;
			$data['blog_small_img'] = $row->blog_small_img;
			$data['blog_video'] = $row->blog_video;
			$data['date_published'] = $row->date_published;
			$data['created_at'] = $row->created_at;
			$data['updated_at'] = $row->updated_at;
			$data['promotion_link_1'] = $row->promotion_link_1;
			$data['promotion_link_2'] = $row->promotion_link_2;
			
			$data['meta_author'] = $row->meta_author;
			$data['meta_keywords'] = $row->meta_keywords;
			$data['meta_description'] = $row->meta_description;

			$data['doc_id'] = $row->doc_id;
		}

		if(!isset($data)){
			$data = "";
		}

		return $data;
	}

	function get($order_by)
	{
		$this->load->model('Mdl_formbloghealthy');
		$query = $this->Mdl_formbloghealthy->get($order_by);
		return $query;
	}

	function get_with_limit($limit, $offset, $order_by)
	{
		$this->load->model('Mdl_formbloghealthy');
		$query = $this->Mdl_formbloghealthy->get_with_limit($limit, $offset, $order_by);
		return $query;
	}

	function get_where($id)
	{
		$this->load->model('Mdl_formbloghealthy');
		$query = $this->Mdl_formbloghealthy->get_where($id);
		return $query;
	}

	function get_where_custom($col, $value)
	{
		$this->load->model('Mdl_formbloghealthy');
		$query = $this->Mdl_formbloghealthy->get_where_custom($col, $value);
		return $query;
	}

	function _insert($data)
	{
		$this->load->model('Mdl_formbloghealthy');
		$this->Mdl_formbloghealthy->_insert($data);
	}

	function _update($id, $data)
	{
		$this->load->model('Mdl_formbloghealthy');
		$this->Mdl_formbloghealthy->_update($id, $data);
	}

	function _delete($id)
	{
		$this->load->model('Mdl_formbloghealthy');
		$this->Mdl_formbloghealthy->_delete($id);
	}

	function count_where($column, $value)
	{
		$this->load->model('Mdl_formbloghealthy');
		$count = $this->Mdl_formbloghealthy->count_where($column, $value);
		return $count;
	}

	function get_max()
	{
		$this->load->model('Mdl_formbloghealthy');
		$max_id = $this->Mdl_formbloghealthy->get_max();
		return $max_id;
	}

	function _custom_query($mysql_query)
	{
		$this->load->model('Mdl_formbloghealthy');
		$query = $this->Mdl_formbloghealthy->_custom_query($mysql_query);
		return $query;
	}
}
