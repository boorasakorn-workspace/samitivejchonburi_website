<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Email extends CI_Controller {

		function __construct()
		{
			parent::__construct();

			$this->load->model('Mdl_site_security');
			$this->Mdl_site_security->_make_sure_is_admin();

			$this->load->library('session');
            $this->load->library('email');
			$this->load->model('Mdl_formemail');
			$this->load->model('Back_Model');

		    $config['protocol']    = 'smtp';
		    $config['smtp_host']    = 'smtp.mailtrap.io';
		    $config['smtp_port']    = '2525';
		    $config['smtp_timeout'] = '7';
		    $config['smtp_user']    = '4df5162eacf19e';
		    $config['smtp_pass']    = '1a9f436700ca7d';
		    $config['charset']    = 'utf-8';
		    $config['crlf']    = "\r\n";
		    $config['newline']    = "\r\n";
		    $config['mailtype'] = 'text'; // or html
		    $config['validation'] = TRUE; // bool whether to validate email or not      
		    $this->email->initialize($config);
		}

		public function Inbox($select = NULL)
		{	      	
	      	if($select != NULL){
	      		$condition = array('email_status' => 1, 'email_open' => $select);
	      	}
	      	else{
	      		$condition = array('email_status' => 1);
	      	}
	      	$data['query'] = $this->Mdl_formemail->get_where_array($condition)->result();
	      	$data['query_count'] = $this->Mdl_formemail->get_where_array($condition)->num_rows();
	      	$data['selection'] = $select;
	      	$data['reflink'] = base_url('backend/src/Email/Inbox/').$select;			
			
			$data['sidelistemail'] = 'backend/email/sidelist';
			$data['content'] = 'backend/email/inboxmail';
			
			$data['additionalscript'] = 'backend/email/inbox_script';
			
			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

		public function Compose()
		{
			$data['sidelistemail'] = 'backend/email/sidelist';
			$data['content'] = 'backend/email/compose';
			
			$data['scriptjs'] = 'backend/email/email_js';

			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

		public function Sending()
		{
		    $this->email->from('samitivej-chonburi@samitivej.com', 'Samitiveh Chonburi');
	      	if($this->input->post('email', TRUE) !== null && strlen($this->input->post('email', TRUE)) > 0){
		    	$to = $this->input->post('email', TRUE);
		    	$this->email->to($to);
				$data['email_target'] = $to;
		    }
	      	if($this->input->post('CCemail', TRUE) !== null && strlen($this->input->post('CCemail', TRUE)) > 0){
	      		$CC = $this->input->post('CCemail', TRUE);
				$this->email->cc($CC);     
				$data['email_target_cc'] = $CC; 	
	      	}
	      	if($this->input->post('BCCemail', TRUE) !== null && strlen($this->input->post('BCCemail', TRUE)) > 0){
	      		$BCC = $this->input->post('BCCemail', TRUE);
	      		$this->email->bcc($BCC);
				$data['email_target_bcc'] = $BCC; 	
	      	}

		    $subject = $this->input->post('subject', TRUE);
		    $message = $this->input->post('message', TRUE);

			$data['email_from'] = 'samitivej-chonburi@samitivej.com';
			$data['email_subject'] = $subject;
			$data['email_message'] = $message;
			$data['email_status'] = 2;
			$data['cuser'] = $this->session->userdata('ses_id');

			$this->Mdl_formemail->_insert($data);

		    $this->email->subject($subject);
		    $this->email->message($message);  

		    $this->email->send();
		}

		public function MailDetails()
		{
			$update_id = $this->uri->segment(5);
			if(!is_numeric($update_id)){
				redirect('backend/src/Email/Inbox');
			}

	      	$update['email_open'] = 1;
			$this->Mdl_formemail->_update($update_id, $update);

			$data['query'] = $this->Mdl_formemail->get_where($update_id)->row();
		    			
			$data['sidelistemail'] = 'backend/email/sidelist';
			$data['content'] = 'backend/email/details';
						
			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

	}