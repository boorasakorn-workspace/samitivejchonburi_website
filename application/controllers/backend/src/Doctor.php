<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->model('Mdl_site_security');
		$this->Mdl_site_security->_make_sure_is_admin();

		$this->load->library('session');

		$this->load->model('Back_Model');		
		
	}

	function view_doctor_details($update_id)
	{
		redirect('backend/MainBackend/DoctorDetails/'.$update_id);
	}

	public function language()
	{
		if(empty($this->session->userdata('language'))){
			$this->session->set_userdata("language", 'thai');
			$this->session->set_userdata("q_l", ' ');
		}
		$this->session->set_userdata("q_l", '_'.substr($this->session->userdata('language'), 0,2));
		if($this->session->userdata('q_l') == 'th' || $this->session->userdata('q_l') == '_th' ){
			$this->session->set_userdata("q_l", ' ');			
		}
		return $this->lang->load($this->session->userdata('language'), 'english');			
	}

	public function change_language($lang)
	{
		$this->session->set_userdata("language", $lang);
		$this->session->set_userdata("q_l", '_'.substr($lang, 0,2));
		if($this->session->userdata('q_l') == 'th' || $this->session->userdata('q_l') == '_th' ){
			$this->session->set_userdata("q_l", ' ');			
		}
		redirect($_SERVER['HTTP_REFERER']);
	}

	function preview_Contents()
	{			
		$this->language();
		
		$this->load->model('Front_Model');
		$this->load->model('Front_Home_Model');
		$data = $this->Front_Model->Detailpage();

		$data['query'] = $this->input->post();
		$data['doctorDetail'] = 'frontend/src/doctor/doctor_details';

		$data['query_workdayhour']=$this->Front_Home_Model->id_doctor_workdayhour($data['query']['medical_id']);
		$data['all_med_doctor']=$this->Front_Home_Model->lang_all_medicial_center();
		$data['all_specialty']=$this->Front_Home_Model->lang_all_specialty();
		$data['all_sub_specialty']=$this->Front_Home_Model->lang_all_sub_specialty();

		$ql = $this->session->userdata('q_l');
		if($ql == ' '){
			$ql = '_th';
		}
		$data['query']['doctor_description'] = $this->input->post('doctor_description'.$ql,TRUE);
		$data['query']['pname_th'] = $this->input->post('pname'.$ql,TRUE);
		$data['query']['firstname_th'] = $this->input->post('firstname'.$ql,TRUE);
		$data['query']['lastname_th'] = $this->input->post('lastname'.$ql,TRUE);
		$data['query']['education_th'] = $this->input->post('education'.$ql,TRUE);
		$data['query']['type_th'] = $this->input->post('type'.$ql,TRUE);
		$data['query']['language_th'] = $this->input->post('language'.$ql,TRUE);
		if(isset($data['query']['hidden_id']) && $data['query']['hidden_id'] != ''){
			$data['query']['doctor_imgs_pre'] = $this->Front_Home_Model->id_doctor($data['query']['hidden_id'])->doctor_preview_imgs;
			if(!isset($data['query']['doctor_imgs_pre']) || $data['query']['doctor_imgs_pre'] == ''){
				$data['query']['doctor_img'] = $this->Front_Home_Model->id_doctor($data['query']['hidden_id'])->doctor_img;
			}			
		}		

		if($this->input->post('parent_medical_center_name', TRUE) !== null || $this->input->post('parent_specialty_name', TRUE) !== null  || $this->input->post('parent_sub_specialty_name', TRUE) !== null ){

			$check['parent_medical_center_name'] = $this->input->post('parent_medical_center_name', TRUE);
			$check['parent_specialty_name'] = $this->input->post('parent_specialty_name', TRUE);
			$check['parent_sub_specialty_name'] = $this->input->post('parent_sub_specialty_name', TRUE);

			$check['parent_medical_center_name'] = array_values(array_unique(explode("|",$check['parent_medical_center_name'])));
			$check['parent_specialty_name'] = array_values(array_unique(explode("|",$check['parent_specialty_name'])));
			$check['parent_sub_specialty_name'] = array_values(array_unique(explode("|",$check['parent_sub_specialty_name'])));


			$data['query']['parent_medical_center_id'] = '';
			for($loopParent = 0;$loopParent < count($check['parent_medical_center_name']);$loopParent++){
				$result = $this->get_id_parent($check['parent_medical_center_name'][$loopParent]);
				if($result->num_rows() > 0){
					foreach($result->result() as $row){
						if($data['query']['parent_medical_center_id'] != ''){
							$data['query']['parent_medical_center_id'] .= '|';
						}
						$data['query']['parent_medical_center_id'] .= $row->id;
					}
				}
				else{
					if($data['query']['parent_medical_center_id'] != ''){
						$data['query']['parent_medical_center_id'] .= '|';
					}
					$data['query']['parent_medical_center_id'] .= $check['parent_medical_center_name'][$loopParent];
				}

			}

			$data['query']['parent_specialty_id'] = '';
			for($loopSubParent = 0;$loopSubParent < count($check['parent_specialty_name']);$loopSubParent++){
				$result = $this->get_id_subparent($check['parent_specialty_name'][$loopSubParent]);
				if($result->num_rows() > 0){
					foreach($result->result() as $row){
						if($data['query']['parent_specialty_id'] != ''){
							$data['query']['parent_specialty_id'] .= '|';
						}
						$data['query']['parent_specialty_id'] .= $row->id;
					}
				}
				else{
					if($data['query']['parent_specialty_id'] != ''){
						$data['query']['parent_specialty_id'] .= '|';
					}
					$data['query']['parent_specialty_id'] .= $check['parent_specialty_name'][$loopSubParent];
				}

			}

			$data['query']['parent_sub_specialty_id'] = '';
			for($loopParent_Sub = 0;$loopParent_Sub < count($check['parent_sub_specialty_name']);$loopParent_Sub++){
				$result = $this->get_id_subsubparent($check['parent_sub_specialty_name'][$loopParent_Sub]);
				if($result->num_rows() > 0){
					foreach($result->result() as $row){
						if($data['query']['parent_sub_specialty_id'] != ''){
							$data['query']['parent_sub_specialty_id'] .= '|';
						}
						$data['query']['parent_sub_specialty_id'] .= $row->id;
					}
				}
				else{
					if($data['query']['parent_sub_specialty_id'] != ''){
						$data['query']['parent_sub_specialty_id'] .= '|';
					}
					$data['query']['parent_sub_specialty_id'] .= $check['parent_sub_specialty_name'][$loopParent_Sub];
				}

			}

		}

		$data['query'] = json_decode(json_encode( $data['query'] ));

		$data['previewContent'] = "previewContent";
		$this->load->view('frontend/homepage', $data);
	}


	function import_doctor()
	{
		$this->load->model('Mdl_formdoctor');
		$query = $this->Mdl_formdoctor->_custom_postgresql_query();
		die();
	}

	//////// Process Delete All 1.Colours 2.Sizes 3.Big Image 4.Small Image 5.Data Record
	function _process_delete($update_id)
	{
		// Delete Doctor Logo Big & Small Image
		$data = $this->fetch_data_from_db($update_id);
		$big_imgs = $data['doctor_img'];

		$big_imgs_path = './gallery/doctor/big_imgs/'.$big_imgs;

		// Remove the images
		if(file_exists($big_imgs_path)){
			unlink($big_imgs_path);
		}		
			
		$resize_imgs_path = './gallery/doctor/resize_imgs/'.$big_imgs;
			
		if(file_exists($resize_imgs_path)){
			unlink($resize_imgs_path);
		}

		// Delete Doctor Record from tb_doctor database
		$this->_delete($update_id);
	}

	//////// Delete Doctor Process from controller backend/src/Doctor/delete_doctor
	function delete_doctor_process($update_id)
	{
		if(!is_numeric($update_id)){
			redirect('backend/src/Doctor/manage');
		}		

		$submit = $this->input->post('submit', TRUE);

		if($submit=="Cancel"){
			redirect('backend/src/Doctor/create/'.$update_id);
		}elseif($submit=="Yes - Delete Doctor"){
			$this->_process_delete($update_id);

			$flash_msg = "Deleted Doctor was Successfully";
			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
			$this->session->set_flashdata('item', $value);

			redirect('backend/src/Doctor/manage');
		}

	}

	//////// Delete Doctor Page
	function delete_doctor($update_id)
	{

		if(!is_numeric($update_id)){
			redirect('backend/src/Doctor/manage');
		}		

		$data['headline'] = "Delete Doctor";
		$data['update_id'] = $update_id;

		$data['content'] = 'backend/doctor/delete_doctor';
		
		$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
	}

		//////// Delete Image Thumbnail
	function delete_image($update_id)
	{

		if(!is_numeric($update_id)){
			redirect('backend/src/Doctor/manage');
		}

		$data = $this->fetch_data_from_db($update_id);
		$big_imgs = $data['doctor_img'];

		$big_imgs_path = './gallery/doctor/big_imgs/'.$big_imgs;

		// Remove the images
		if(file_exists($big_imgs_path)){
			unlink($big_imgs_path);
		}

		// Update Remove the images to database
		unset($data);
		$data['doctor_img'] = "";
		$this->_update($update_id, $data);

		$flash_msg = "The image was successfully deleted."; // Alert message
		$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
		$this->session->set_flashdata('item', $value); // สร้าง session alert

		redirect('backend/src/Doctor/create/'.$update_id);

		}

		//////// Delete Image Thumbnail
	function delete_video($update_id)
	{

		if(!is_numeric($update_id)){
			redirect('backend/src/Doctor/manage');
		}

		$data = $this->fetch_data_from_db($update_id);
		$big_imgs = $data['doctor_video'];

		$big_imgs_path = './gallery/doctor/videos/'.$big_imgs;

		// Remove the images
		if(file_exists($big_imgs_path)){
			unlink($big_imgs_path);
		}

		// Update Remove the images to database
		unset($data);
		$data['doctor_video'] = "";
		$this->_update($update_id, $data);

		$flash_msg = "The video was successfully deleted."; // Alert message
		$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
		$this->session->set_flashdata('item', $value); // สร้าง session alert

		redirect('backend/src/Doctor/create/'.$update_id);

		}

		//////// Set Image Thumbnail
		function _generate_thumbnail($file_name)
		{

			$config['image_library'] = 'gd2';
			$config['source_image'] = './gallery/doctor/preview_imgs/'.$file_name;
			$config['new_image'] = './gallery/doctor/resize_imgs/'.$file_name;
			$config['create_thumb'] = FALSE;
			$config['maintain_ratio'] = FALSE;//$config['maintain_ratio'] = TRUE;
			$config['width']         = 260;
			$config['height']       = 320;

			$this->load->library('image_lib', $config);

			$this->image_lib->resize();
			if ( ! $this->image_lib->resize()){
			   //echo $this->image_lib->display_errors();
			}
			return true;
			
			$config['image_library'] = 'gd2';
			$config['source_image'] = './gallery/doctor/big_imgs/'.$file_name;
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = TRUE;
			$config['width']         = 235;
			$config['height']       = 96;

			$this->load->library('image_lib', $config);

			$this->image_lib->resize();
		}

		// PREVIEW IMAGE //
		function modal_do_upload($update_id)
		{
			
			if(!is_numeric($update_id)){
				redirect('backend/src/Doctor/manage');
			}

			$config['upload_path'] = './gallery/doctor/preview_imgs/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = 0;

			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('file'))
			{
				// Upload Error
				$data['error'] = array('error' => $this->upload->display_errors("<p style='color:red;'>","</p>"));
				$data['headline'] = "Upload Error";
				$data['update_id'] = $update_id;

			}else{
				// Upload Successful
				$data = array('upload_data' => $this->upload->data());
				$upload_data = $data['upload_data'];
				$file_name = $upload_data['file_name'];
				$this->_generate_thumbnail($file_name);

				// Update Image to Database
				$update_data['doctor_preview_imgs'] = $file_name;
				$this->_update($update_id, $update_data);

				$data['headline'] = "Upload Success";
				$data['update_id'] = $update_id;
				
				echo json_encode($update_data['doctor_preview_imgs']);
			}

		}
		function fetch_preview_image($update_id) //  Fetch ข้อมูลจาก Database
		{			
			if(!is_numeric($update_id)){
				redirect('backend/src/Doctor/manage');
			}

			$query = $this->get_where($update_id);
			foreach($query->result() as $row){				
				$data['doctor_preview_imgs'] = $row->doctor_preview_imgs;
			}

			if(!isset($data)){
				$data = "";
			}

			return $data;
		}
		function delete_preview($update_id)
		{

			if(!is_numeric($update_id)){
				redirect('backend/src/Doctor/manage');
			}		    

			$data = $this->fetch_preview_image($update_id);
			$preview_imgs = $data['doctor_preview_imgs'];

			$preview_imgs_path = './gallery/doctor/preview_imgs/'.$preview_imgs;

			/*
			// Remove the images
			if(file_exists($preview_imgs_path)){
				unlink($preview_imgs_path);
			}
			
			$resize_imgs_path = './gallery/doctor/resize_imgs/'.$preview_imgs;
			
			if(file_exists($resize_imgs_path)){
				unlink($resize_imgs_path);
			}
			*/

			unset($data);
			$data['doctor_preview_imgs'] = "";
			$this->_update($update_id, $data);
			echo '<p style="display:none;">Success</p>';
		}
		function upload_preview($update_id){
			$preview_img_name = $this->fetch_preview_image($update_id)['doctor_preview_imgs'];			
			if($preview_img_name != ""){
				$preview_path = 'gallery/doctor/preview_imgs/'.$preview_img_name;
				$big_path = 'gallery/doctor/big_imgs/'.$preview_img_name;
				copy($preview_path, $big_path);

				$update_data['doctor_img'] = $preview_img_name;
				$this->_update($update_id, $update_data);

				$this->delete_preview($update_id);
			}
		}
		// PREVIEW IMAGE //


		// PREVIEW IMAGE //
		function modal_do_upload_vid($update_id)
		{
			
			if(!is_numeric($update_id)){
				redirect('backend/src/Doctor/manage');
			}

			$config['upload_path'] = './gallery/doctor/preview_vids/';
			$config['allowed_types'] = 'mp4|mpeg|mpg|mp4|mpe|qt|mov|avi';
			$config['max_size'] = 0;

			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('file_vid'))
			{
				// Upload Error
				$data['error'] = array('error' => $this->upload->display_errors("<p style='color:red;'>","</p>"));
				$data['headline'] = "Upload Error";
				$data['update_id'] = $update_id;

			}else{
				// Upload Successful
				$data = array('upload_data' => $this->upload->data());
				$upload_data = $data['upload_data'];
				$file_name = $upload_data['file_name'];

				// Update Image to Database
				$update_data['doctor_preview_video'] = $file_name;
				$this->_update($update_id, $update_data);

				$data['headline'] = "Upload Success";
				$data['update_id'] = $update_id;
				
				echo json_encode($update_data['doctor_preview_video']);
			}

		}
		function fetch_preview_video($update_id) //  Fetch ข้อมูลจาก Database
		{			
			if(!is_numeric($update_id)){
				redirect('backend/src/Doctor/manage');
			}

			$query = $this->get_where($update_id);
			foreach($query->result() as $row){				
				$data['doctor_preview_video'] = $row->doctor_preview_video;
			}

			if(!isset($data)){
				$data = "";
			}

			return $data;
		}
		function delete_preview_video($update_id)
		{

			if(!is_numeric($update_id)){
				redirect('backend/src/Doctor/manage');
			}		    

			$data = $this->fetch_preview_video($update_id);
			$preview_imgs = $data['doctor_preview_video'];

			$preview_imgs_path = './gallery/doctor/preview_vids/'.$doctor_preview_video;

			// Remove the images
			if(file_exists($preview_imgs_path)){
				unlink($preview_imgs_path);
			}

			unset($data);
			$data['doctor_preview_video'] = "";
			$this->_update($update_id, $data);
			echo json_encode("Success");
		}
		function upload_preview_video($update_id){
			$preview_img_name = $this->fetch_preview_video($update_id)['doctor_preview_video'];			
			if($preview_img_name != ""){
				$preview_path = 'gallery/doctor/preview_vids/'.$preview_img_name;
				$big_path = 'gallery/doctor/videos/'.$preview_img_name;
				copy($preview_path, $big_path);

				$update_data['doctor_video'] = $preview_img_name;
				$this->_update($update_id, $update_data);

				$this->delete_preview_video($update_id);
			}
		}
		// PREVIEW IMAGE //


		//////// Upload Image Doctor Do Upload
		function do_upload($update_id)
		{

			if(!is_numeric($update_id)){
				redirect('backend/src/Doctor/manage');
			}

			$submit = $this->input->post('submit', TRUE);
			if($submit == "Cancel"){
				redirect('backend/src/Doctor/create/'.$update_id);
			}

			$config['upload_path'] = './gallery/doctor/big_imgs/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = 204800;
			$config['max_width'] = 1024;
			$config['max_height'] = 768;

			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('userfile'))
			{
				$data['error'] = array('error' => $this->upload->display_errors("<p style='color:red;'>","</p>"));
				$data['headline'] = "Upload Error";
				$data['update_id'] = $update_id;

				$data['content'] = 'backend/doctor/upload_image';			

				$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);

			}else{
				// Upload Successful
				
				$data = array('upload_data' => $this->upload->data());

				$upload_data = $data['upload_data'];
				$file_name = $upload_data['file_name'];
				$this->_generate_thumbnail($file_name);

				// Update Image to Database
				$update_data['doctor_img'] = $file_name;
				$this->_update($update_id, $update_data);
				
				$flash_msg = "Upload Image were Successfully";
				$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
				$this->session->set_flashdata('item', $value);

				$data['headline'] = "Upload Success";
				$data['update_id'] = $update_id;
				
				$data['content'] = 'backend/doctor/upload_image';
				
				$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
			}
		}

		//////// Upload Image Doctor
		function upload_image($update_id)
		{

			if(!is_numeric($update_id)){
				redirect('backend/src/Doctor/manage');
			}

			$data['headline'] = "Upload Image​ Doctor";
			$data['update_id'] = $update_id;
			
			$data['content'] = 'backend/doctor/upload_image';			
			
			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

		function createSelfregis(){
			error_reporting (E_ALL ^ E_NOTICE);
			$SelfRegisData = $this->input->post();
			$update_id = $this->uri->segment(5);
			$data['doctor_img'] = "";
			
		    $this->load->model('Home_model');
			$data['headline'] = "เพิ่มข้อมูล Doctor";
		    $data['update_id'] = $update_id;

		    $data['data_speciality']=$this->Home_model->s_speciality();

		    $data['medical_center']=$this->Home_model->all_medicial_center();
		    $data['all_specialty']=$this->Home_model->all_specialty();		    
		    $data['all_sub_specialty']=$this->Home_model->all_sub_specialty();

		    $data['content'] = 'backend/doctor/formDoctor';
			$data['Additionalscript'] = 'backend/doctor/scriptDoctor';

			$data['replace6'] = 'backend/src/ck/replace6';
			foreach($SelfRegisData as $key => $val){
				$data[$key] = $val;
			}
		    
		    $data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);

		}

		//////// Add & Update Doctor
		function create()
		{
			error_reporting (E_ALL ^ E_NOTICE);
			$this->load->model('Mdl_formdoctor');
			$update_id = $this->uri->segment(5);
			$submit = $this->input->post('submit', TRUE);

			if($submit == "Cancel"){
				redirect('backend/src/Doctor/manage');
			}

			// ถ้า $submit = value->Submit
			if($submit == "Submit"){
				// Process the Form at create.php
				$this->load->library('form_validation'); // Load Library From Validation

				$this->form_validation->set_rules('forename', '<b>โปรดกรอกเฉพาะชื่อภาษาไทย</b>', 'required|max_length[150]');
				$this->form_validation->set_rules('surname', '<b>โปรดกรอกเฉพาะนามสกุลภาษาไทย</b>', 'required|max_length[150]');
				$this->form_validation->set_rules('education', '<b>โปรดกรอกประวัติการศึกษา</b>', 'required');
				/*$this->form_validation->set_rules('experience_th', '<b>โปรดกรอกประสบการณ์ทำงาน</b>', 'required');*/
				//$this->form_validation->set_rules('speciality_th', '<b>โปรดกรอกความเชี่ยวชาญทางการแพทย์</b>', 'required');
		    	//$this->form_validation->set_rules('doctor_department', '<b>โปรดเลือกแผนกประจำการ</b>', 'required');
				$this->form_validation->set_rules('forename_en', '<b>โปรดกรอกเฉพาะชื่อภาษาอังกฤษ</b>', 'required|max_length[150]');
				$this->form_validation->set_rules('surname_en', '<b>โปรดกรอกเฉพาะนามสกุลอังกฤษ</b>', 'required|max_length[150]');
				$this->form_validation->set_rules('education_en', '<b>โปรดกรอกประวัติการศึกษาภาษาอังกฤษ</b>', 'required');
				/*$this->form_validation->set_rules('experience_en', '<b>โปรดเลือกประสบการณ์ทำงานภาษาอังกฤษ</b>', 'required');*/
				//$this->form_validation->set_rules('speciality_en', '<b>โปรดกรอกความเชี่ยวชาญทางการแพทย์ภาษาอังกฤษ</b>', 'required');
				//$this->form_validation->set_rules('phone', '<b>โปรดกรอกเบอร์โทรศัพท์</b>', 'required|numeric|max_length[30]|min_length[10]');
				//$this->form_validation->set_rules('email', '<b>โปรดกรอกอีเมลล์สำหรับติดต่อ</b>', 'required|max_length[150]|valid_email');
				//$this->form_validation->set_rules('type_th', '<b>โปรดเลือกสังกัตโรงพยาบาล</b>', 'required');
				//$this->form_validation->set_rules('address', '<b>โปรดกรอกที่อยู่ของแพทย์</b>', 'required|min_length[15]');
				// //$this->form_validation->set_rules('medical_id', '<b>โปรดกรอกรหัสประจำตัวแพทย์</b>', 'required');
				//$this->form_validation->set_rules('gender', '<b>โปรดเลือกเพศของแพทย์</b>', 'required');
				$this->form_validation->set_rules('status', '<b>โปรดเลือกสถานะของแพทย์</b>', 'required');
				$this->form_validation->set_rules('at_hospital', '<b>โปรดเลือกโรงพยาบาลของแพทย์</b>', 'required');

		    	if($this->form_validation->run() == TRUE){ // ถ้า form_validation ทำงานแล้วถูกต้อง
		    		// Get The Variables
					$data = $this->fetch_data_from_post(); // ให้ $data = รับค่าจาก From method="POST"
					unset($data['hidden_id']);
					unset($data['parent']);
					unset($data['parent_medical_center_name']);
					unset($data['parent_specialty']);
					unset($data['parent_specialty_name']);
					unset($data['parent_sub_specialty']);
					unset($data['parent_sub_specialty_name']);
					unset($data['submit']);
		    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id
		    			$this->upload_preview($update_id);
		    			$this->upload_preview_video($update_id);

		    			// Update Doctor Details
		    			$this->_update($update_id, $data); // Update ลง Database
		    			$flash_msg = "Update Doctor were Successfully"; // Alert message
		    			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
						$this->session->set_flashdata('item', $value); // สร้าง session alert
						redirect('backend/src/Doctor/create/'.$update_id);
					}else{
		    			// Insert a New Doctor
						$this->_insert($data);
		    			$update_id = $this->get_max(); // Get the ID at new Doctor

		    			$flash_msg = "Insert Doctor were Successfully";
		    			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
		    			$this->session->set_flashdata('item', $value);
		    			redirect('backend/src/Doctor/create/'.$update_id);
		    		}
		    	}
		    }

		    if((is_numeric($update_id)) && ($submit!="Submit")){
		    	$data = $this->fetch_data_from_db($update_id);
		    }else{
		    	$data = $this->fetch_data_from_post();
		    	$data['doctor_img'] = "";
		    }

		    if(!is_numeric($update_id)){
		    	$data['headline'] = "เพิ่มข้อมูล Doctor";
		    }else{
		    	$data['headline'] = "แก้ไขข้อมูล Doctor";
				$data['query'] = $data;
				// $mysql_query = "select
				// 	tb_doctor.medical_id,
				// 	tb_doctor.pname_th,
				// 	tb_doctor.firstname_th,
				// 	tb_doctor.lastname_th,
				// 	tb_workdayhours.id,
				// 	tb_workdayhours.medical_id,
				// 	tb_workdayhours.work_days,
				// 	tb_workdayhours.out_of_days,
				// 	tb_workdayhours.work_times_in,
				// 	tb_workdayhours.work_times_out
				// 	from tb_doctor
				// 	inner join tb_workdayhours on tb_doctor.medical_id=tb_workdayhours.medical_id
				// 	where tb_doctor.medical_id=tb_workdayhours.medical_id and tb_doctor.id = ".$update_id;
				// $fetch = $this->_custom_query($mysql_query);
				
				// $data['query'] = $fetch;
			
		    }

		    $data['update_id'] = $update_id;

		    $this->load->model('Home_model');
			$data['drop_pname'] = $this->Mdl_formdoctor->getPName();
			$data['drop_gender'] = $this->Mdl_formdoctor->getGender();
		    //$data['data_speciality']=$this->Home_model->s_speciality();

		    $data['medical_center']=$this->Home_model->all_medicial_center();
		    $data['all_specialty']=$this->Home_model->all_specialty();		    
		    $data['all_sub_specialty']=$this->Home_model->all_sub_specialty();

		    $data['content'] = 'backend/doctor/formDoctor';
			$data['Additionalscript'] = 'backend/doctor/scriptDoctor';

			$data['replace6'] = 'backend/src/ck/replace6';
		    
		    $data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

		//////// Doctor Detail
		function manage()
		{
			
			$this->load->model('Mdl_formdoctor');			

			$data['query'] = $this->Mdl_formdoctor->getDbSelfRegister();

			//$data['query'] = $this->get('firstname_th');
			
			$data['content'] = 'backend/doctor/manageDoctor';
			
			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

		function url_slug($text)
		{
			$array = array("'", '"', '\\', '/', '.', ',', '!', '+', '*', '%', '$', '#', '@', '(', ')', '[', ']', '?', '>', '<', '|', '’', '“', '”', '‘', '’', ';', ':', '–');
			$text = str_replace($array, '', $text); // Replaces all spaces with hyphens.
			$text = preg_replace('/[^A-Za-z0-9ก-๙เ-า\\-]/', '-', $text); // Removes special chars.
			$text = preg_replace('/-+/', "-", $text);
			$text = trim($text, '-');
			$text = urldecode($text);

			if (empty($text)){
				return NULL;
			}

			return $text;
		}

		//////// Fetch Data at method POST
		function fetch_data_from_post() // Fetch ข้อมูลจาก Input POST
		{
			$data = $this->input->post();
			/*
			$data['pname_th'] = $this->input->post('pname_th', TRUE);
			$data['firstname_th'] = $this->input->post('firstname_th', TRUE);
			$data['lastname_th'] = $this->input->post('lastname_th', TRUE);
			$data['pname_en'] = $this->input->post('pname_en', TRUE);
			$data['firstname_en'] = $this->input->post('firstname_en', TRUE);
			$data['lastname_en'] = $this->input->post('lastname_en', TRUE);
			$data['education_th'] = html_escape($this->input->post('education_th', FALSE));
			$data['education_en'] = html_escape($this->input->post('education_en', FALSE));
			$data['doctor_video'] = $this->input->post('doctor_video', TRUE);
			$data['experience_th'] = $this->input->post('experience_th', TRUE);
			// $data['experience_en'] = $this->input->post('experience_en', TRUE);
			// $data['phone'] = $this->input->post('phone', TRUE);
			// $data['email'] = $this->input->post('email', TRUE);
			$data['address'] = $this->input->post('address', TRUE);
			$data['type_th'] = $this->input->post('type_th', TRUE);
			$data['type_en'] = $this->input->post('type_en', TRUE);
			//$data['speciality_th'] = $this->input->post('speciality_th', TRUE);
			//$data['speciality_en'] = $this->input->post('speciality_en', TRUE);
			$data['doctor_department'] = $this->input->post('doctor_department', TRUE);
			$data['doctor_department_child'] = $this->input->post('doctor_department', TRUE);
			$data['language_th'] = $this->input->post('language_th', TRUE);
			$data['language_en'] = $this->input->post('language_en', TRUE);
			//$data['gender'] = $this->input->post('gender', TRUE);
			$data['status'] = $this->input->post('status', TRUE);
			$data['at_hospital'] = $this->input->post('at_hospital', TRUE);
			$data['medical_id'] = $this->input->post('medical_id', TRUE);
			*/
			$data['doctor_description'] = html_escape($this->input->post('doctor_description', FALSE));
			$data['doctor_description_en'] = html_escape($this->input->post('doctor_description_en', FALSE));


			if($this->input->post('parent_medical_center_name', TRUE) !== null || $this->input->post('parent_specialty_name', TRUE) !== null  || $this->input->post('parent_sub_specialty_name', TRUE) !== null ){

				$check['parent_medical_center_name'] = $this->input->post('parent_medical_center_name', TRUE);
				$check['parent_specialty_name'] = $this->input->post('parent_specialty_name', TRUE);
				$check['parent_sub_specialty_name'] = $this->input->post('parent_sub_specialty_name', TRUE);

				$check['parent_medical_center_name'] = array_values(array_unique(explode("|",$check['parent_medical_center_name'])));
				$check['parent_specialty_name'] = array_values(array_unique(explode("|",$check['parent_specialty_name'])));
				$check['parent_sub_specialty_name'] = array_values(array_unique(explode("|",$check['parent_sub_specialty_name'])));


				$data['parent_medical_center_id'] = '';
				for($loopParent = 0;$loopParent < count($check['parent_medical_center_name']);$loopParent++){
					$result = $this->get_id_parent($check['parent_medical_center_name'][$loopParent]);
					if($result->num_rows() > 0){
						foreach($result->result() as $row){
							if($data['parent_medical_center_id'] != ''){
								$data['parent_medical_center_id'] .= '|';
							}
							$data['parent_medical_center_id'] .= $row->id;
						}
					}
					else{
						if($data['parent_medical_center_id'] != ''){
							$data['parent_medical_center_id'] .= '|';
						}
						$data['parent_medical_center_id'] .= $check['parent_medical_center_name'][$loopParent];
					}

				}

				$data['parent_specialty_id'] = '';
				for($loopSubParent = 0;$loopSubParent < count($check['parent_specialty_name']);$loopSubParent++){
					$result = $this->get_id_subparent($check['parent_specialty_name'][$loopSubParent]);
					if($result->num_rows() > 0){
						foreach($result->result() as $row){
							if($data['parent_specialty_id'] != ''){
								$data['parent_specialty_id'] .= '|';
							}
							$data['parent_specialty_id'] .= $row->id;
						}
					}
					else{
						if($data['parent_specialty_id'] != ''){
							$data['parent_specialty_id'] .= '|';
						}
						$data['parent_specialty_id'] .= $check['parent_specialty_name'][$loopSubParent];
					}

				}

				$data['parent_sub_specialty_id'] = '';
				for($loopParent_Sub = 0;$loopParent_Sub < count($check['parent_sub_specialty_name']);$loopParent_Sub++){
					$result = $this->get_id_subsubparent($check['parent_sub_specialty_name'][$loopParent_Sub]);
					if($result->num_rows() > 0){
						foreach($result->result() as $row){
							if($data['parent_sub_specialty_id'] != ''){
								$data['parent_sub_specialty_id'] .= '|';
							}
							$data['parent_sub_specialty_id'] .= $row->id;
						}
					}
					else{
						if($data['parent_sub_specialty_id'] != ''){
							$data['parent_sub_specialty_id'] .= '|';
						}
						$data['parent_sub_specialty_id'] .= $check['parent_sub_specialty_name'][$loopParent_Sub];
					}

				}

			}

			return $data;
		}

		//////// Fetch Data at Database
		function fetch_data_from_db($update_id) //  Fetch ข้อมูลจาก Database
		{
			
			if(!is_numeric($update_id)){
				redirect('backend/src/Doctor/manage');
			}

			$query = $this->get_where($update_id);
			$data = $query->row_array();

			if(!isset($data)){
				$data = "";
			}else{
				$data['parent_medical_center_id'] = array_values(array_unique(explode("|",$data['parent_medical_center_id'])));
				$data['parent_specialty_id'] = array_values(array_unique(explode("|",$data['parent_specialty_id'])));
				$data['parent_sub_specialty_id'] = array_values(array_unique(explode("|",$data['parent_sub_specialty_id'])));

				$data['parent_medical_center_name'] = '';
				for($loopParent = 0;$loopParent < count($data['parent_medical_center_id']);$loopParent++){
					$result = $this->get_name_parent($data['parent_medical_center_id'][$loopParent]);
					if($result->num_rows() > 0){
						foreach($result->result() as $row){
							if($data['parent_medical_center_name'] != ''){
								$data['parent_medical_center_name'] .= '|';
							}
							$data['parent_medical_center_name'] .= $row->medical_center_name;
						}
					}
					else{
						if($data['parent_medical_center_name'] != ''){
							$data['parent_medical_center_name'] .= '|';
						}
						$data['parent_medical_center_name'] .= $data['parent_medical_center_id'][$loopParent];
					}

				}
				$data['parent_specialty_name'] = '';
				for($loopSubParent = 0;$loopSubParent < count($data['parent_specialty_id']);$loopSubParent++){
					$result = $this->get_name_subparent($data['parent_specialty_id'][$loopSubParent]);
					if($result->num_rows() > 0){
						foreach($result->result() as $row){
							if($data['parent_specialty_name'] != ''){
								$data['parent_specialty_name'] .= '|';
							}
							$data['parent_specialty_name'] .= $row->specialty_name_th;
						}
					}
					else{
						if($data['parent_specialty_name'] != ''){
							$data['parent_specialty_name'] .= '|';
						}
						$data['parent_specialty_name'] .= $data['parent_specialty_id'][$loopSubParent];
					}

				}
				$data['parent_sub_specialty_name'] = '';
				for($loopParent_Sub = 0;$loopParent_Sub < count($data['parent_sub_specialty_id']);$loopParent_Sub++){
					$result = $this->get_name_subsubparent($data['parent_sub_specialty_id'][$loopParent_Sub]);
					if($result->num_rows() > 0){
						foreach($result->result() as $row){
							if($data['parent_sub_specialty_name'] != ''){
								$data['parent_sub_specialty_name'] .= '|';
							}
							$data['parent_sub_specialty_name'] .= $row->sub_specialty_name_th;
						}
					}
					else{
						if($data['parent_sub_specialty_name'] != ''){
							$data['parent_sub_specialty_name'] .= '|';
						}
						$data['parent_sub_specialty_name'] .= $data['parent_sub_specialty_id'][$loopParent_Sub];
					}

				}
			}

			return $data;
		}


		function get_name_parent($idparent)
		{
			$this->load->model('Mdl_formsubspecialty');
			$query = $this->Mdl_formsubspecialty->get_name_parent($idparent);
			return $query;
		}

		function get_name_subparent($idsubparent)
		{
			$this->load->model('Mdl_formsubspecialty');
			$query = $this->Mdl_formsubspecialty->get_name_subparent($idsubparent);
			return $query;
		}

		function get_name_subsubparent($idsubsubparent)
		{
			$this->load->model('Mdl_formsubspecialty');
			$query = $this->Mdl_formsubspecialty->get_name_subsubparent($idsubsubparent);
			return $query;
		}

		function get_id_parent($parentname)
		{
			$this->load->model('Mdl_formsubspecialty');
			$query = $this->Mdl_formsubspecialty->get_id_parent($parentname);
			return $query;
		}

		function get_id_subparent($subparentname)
		{
			$this->load->model('Mdl_formsubspecialty');
			$query = $this->Mdl_formsubspecialty->get_id_subparent($subparentname);
			return $query;
		}

		function get_id_subsubparent($subname)
		{
			$this->load->model('Mdl_formsubspecialty');
			$query = $this->Mdl_formsubspecialty->get_id_subsubparent($subname);
			return $query;
		}

		function get($order_by)
		{
			$this->load->model('Mdl_formdoctor');
			$query = $this->Mdl_formdoctor->get($order_by);
			return $query;
		}

		function get_with_limit($limit, $offset, $order_by)
		{
			$this->load->model('Mdl_formdoctor');
			$query = $this->Mdl_formdoctor->get_with_limit($limit, $offset, $order_by);
			return $query;
		}

		function get_where($id)
		{
			$this->load->model('Mdl_formdoctor');
			$query = $this->Mdl_formdoctor->get_where($id);
			return $query;
		}

		function get_where_custom($col, $value)
		{
			$this->load->model('Mdl_formdoctor');
			$query = $this->Mdl_formdoctor->get_where_custom($col, $value);
			return $query;
		}

		function _insert($data)
		{
			$this->load->model('Mdl_formdoctor');
			$this->Mdl_formdoctor->_insert($data);
		}

		function _update($id, $data)
		{
			$this->load->model('Mdl_formdoctor');
			$this->Mdl_formdoctor->_update($id, $data);
		}

		function _delete($id)
		{
			$this->load->model('Mdl_formdoctor');
			$this->Mdl_formdoctor->_delete($id);
		}

		function count_where($column, $value)
		{
			$this->load->model('Mdl_formdoctor');
			$count = $this->Mdl_formdoctor->count_where($column, $value);
			return $count;
		}

		function get_max()
		{
			$this->load->model('Mdl_formdoctor');
			$max_id = $this->Mdl_formdoctor->get_max();
			return $max_id;
		}

		function _custom_query($mysql_query)
		{
			$this->load->model('Mdl_formdoctor');
			$query = $this->Mdl_formdoctor->_custom_query($mysql_query);
			return $query;
		}

				//////// Add & Update Doctor
		function create_test()
		{

			$update_id = $this->uri->segment(5);
			$submit = $this->input->post('submit', TRUE);

			if($submit == "Cancel"){
				redirect('backend/src/Doctor/manage');
			}

			// ถ้า $submit = value->Submit
			if($submit == "Submit"){
				// Process the Form at create.php
				$this->load->library('form_validation'); // Load Library From Validation

				$this->form_validation->set_rules('forename', '<b>โปรดกรอกเฉพาะชื่อภาษาไทย</b>', 'required|max_length[150]');
				$this->form_validation->set_rules('surename', '<b>โปรดกรอกเฉพาะนามสกุลภาษาไทย</b>', 'required|max_length[150]');
				$this->form_validation->set_rules('education', '<b>โปรดกรอกประวัติการศึกษา</b>', 'required');
				/*$this->form_validation->set_rules('experience_th', '<b>โปรดกรอกประสบการณ์ทำงาน</b>', 'required');*/
				//$this->form_validation->set_rules('speciality_th', '<b>โปรดกรอกความเชี่ยวชาญทางการแพทย์</b>', 'required');
		    	//$this->form_validation->set_rules('doctor_department', '<b>โปรดเลือกแผนกประจำการ</b>', 'required');
				$this->form_validation->set_rules('forename_en', '<b>โปรดกรอกเฉพาะชื่อภาษาอังกฤษ</b>', 'required|max_length[150]');
				$this->form_validation->set_rules('surename_en', '<b>โปรดกรอกเฉพาะนามสกุลอังกฤษ</b>', 'required|max_length[150]');
				$this->form_validation->set_rules('education_en', '<b>โปรดกรอกประวัติการศึกษาภาษาอังกฤษ</b>', 'required');
				/*$this->form_validation->set_rules('experience_en', '<b>โปรดเลือกประสบการณ์ทำงานภาษาอังกฤษ</b>', 'required');*/
				//$this->form_validation->set_rules('speciality_en', '<b>โปรดกรอกความเชี่ยวชาญทางการแพทย์ภาษาอังกฤษ</b>', 'required');
				//$this->form_validation->set_rules('phone', '<b>โปรดกรอกเบอร์โทรศัพท์</b>', 'required|numeric|max_length[30]|min_length[10]');
				//$this->form_validation->set_rules('email', '<b>โปรดกรอกอีเมลล์สำหรับติดต่อ</b>', 'required|max_length[150]|valid_email');
				//$this->form_validation->set_rules('type_th', '<b>โปรดเลือกสังกัตโรงพยาบาล</b>', 'required');
				//$this->form_validation->set_rules('address', '<b>โปรดกรอกที่อยู่ของแพทย์</b>', 'required|min_length[15]');
				$this->form_validation->set_rules('medical_id', '<b>โปรดกรอกรหัสประจำตัวแพทย์</b>', 'required');
				//$this->form_validation->set_rules('gender', '<b>โปรดเลือกเพศของแพทย์</b>', 'required');
				$this->form_validation->set_rules('status', '<b>โปรดเลือกสถานะของแพทย์</b>', 'required');
				$this->form_validation->set_rules('at_hospital', '<b>โปรดเลือกโรงพยาบาลของแพทย์</b>', 'required');

		    	if($this->form_validation->run() == TRUE){ // ถ้า form_validation ทำงานแล้วถูกต้อง
		    		// Get The Variables
		    		$data = $this->fetch_data_from_post(); // ให้ $data = รับค่าจาก From method="POST"
		    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id
		    			$this->upload_preview($update_id);

		    			// Update Doctor Details
		    			$this->_update($update_id, $data); // Update ลง Database
		    			$flash_msg = "Update Doctor were Successfully"; // Alert message
		    			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
						$this->session->set_flashdata('item', $value); // สร้าง session alert
						redirect('backend/src/Doctor/create/'.$update_id);
					}else{
		    			// Insert a New Doctor
						$this->_insert($data);
		    			$update_id = $this->get_max(); // Get the ID at new Doctor

		    			$flash_msg = "Insert Doctor were Successfully";
		    			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
		    			$this->session->set_flashdata('item', $value);
		    			redirect('backend/src/Doctor/create/'.$update_id);
		    		}
		    	}
		    }

		    if((is_numeric($update_id)) && ($submit!="Submit")){
		    	$data = $this->fetch_data_from_db($update_id);
		    }else{
		    	$data = $this->fetch_data_from_post();
		    	$data['doctor_img'] = "";
		    }

		    if(!is_numeric($update_id)){
		    	$data['headline'] = "เพิ่มข้อมูล Doctor";
		    }else{
		    	$data['headline'] = "แก้ไขข้อมูล Doctor";

				$mysql_query = "select
					tb_doctor.medical_id,
					tb_doctor.pname_th,
					tb_doctor.firstname_th,
					tb_doctor.lastname_th,
					tb_workdayhours.id,
					tb_workdayhours.medical_id,
					tb_workdayhours.work_days,
					tb_workdayhours.out_of_days,
					tb_workdayhours.work_times_in,
					tb_workdayhours.work_times_out
					from tb_doctor
					inner join tb_workdayhours on tb_doctor.medical_id=tb_workdayhours.medical_id
					where tb_doctor.medical_id=tb_workdayhours.medical_id and tb_doctor.id = ".$update_id;
				$fetch = $this->_custom_query($mysql_query);
				
				$data['query'] = $fetch;
			
		    }

		    $data['update_id'] = $update_id;

		    $this->load->model('Home_model');

		    $data['data_speciality']=$this->Home_model->s_speciality();

		    $data['medical_center']=$this->Home_model->all_medicial_center();
		    $data['all_specialty']=$this->Home_model->all_specialty();		    
		    $data['all_sub_specialty']=$this->Home_model->all_sub_specialty();

		    $data['content'] = 'backend/doctor/formDoctor';
			$data['Additionalscript'] = 'backend/doctor/scriptDoctor';
		    
		    $data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}
	}
