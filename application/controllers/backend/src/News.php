<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->model('Mdl_site_security');
		$this->Mdl_site_security->_make_sure_is_admin();

		$this->load->library('session');

		$this->load->model('Back_Model');
	}

	function view_news_details($update_id)
	{
		redirect('backend/MainBackend/NewsDetails/'.$update_id);
	}

	public function language()
	{
		if(empty($this->session->userdata('language'))){
			$this->session->set_userdata("language", 'thai');
			$this->session->set_userdata("q_l", ' ');
		}
		$this->session->set_userdata("q_l", '_'.substr($this->session->userdata('language'), 0,2));
		if($this->session->userdata('q_l') == 'th' || $this->session->userdata('q_l') == '_th' ){
			$this->session->set_userdata("q_l", ' ');			
		}
		return $this->lang->load($this->session->userdata('language'), 'english');			
	}

	public function change_language($lang)
	{
		$this->session->set_userdata("language", $lang);
		$this->session->set_userdata("q_l", '_'.substr($lang, 0,2));
		if($this->session->userdata('q_l') == 'th' || $this->session->userdata('q_l') == '_th' ){
			$this->session->set_userdata("q_l", ' ');			
		}
		redirect($_SERVER['HTTP_REFERER']);
	}

	function preview_Contents()
	{			
		$this->language();
		
		$this->load->model('Front_Model');
		$this->load->model('Front_Home_Model');
		$data = $this->Front_Model->Detailpage();

		$data['query'] = json_decode(json_encode($this->input->post()));
		$data['newsDetails'] = 'frontend/src/NewsAndEvents/news_details';

		$data['previewContent'] = "previewContent";
		$this->load->view('frontend/homepage', $data);
	}

	//////// Process Delete All 1.Colours 2.Sizes 3.Big Image 4.Small Image 5.Data Record
	function _process_delete($update_id)
	{
		// Delete News Logo Big & Small Image
		$data = $this->fetch_data_from_db($update_id);
		$big_imgs = $data['new_activity_big_img'];
		$small_imgs = $data['new_activity_small_img'];

		$big_imgs_path = './gallery/news/big_imgs/'.$big_imgs;
		$small_imgs_path = './gallery/news/small_imgs/'.$small_imgs;

			// Remove the images
			if(file_exists($big_imgs_path)){
				unlink($big_imgs_path);
			}
			if(file_exists($small_imgs_path)){
				unlink($small_imgs_path);
			}

		// Delete Items Record from new_activity database
		$this->_delete($update_id);
	}

	//////// Delete News Process from controller backend/src/News/delete_news
	function delete_news_process($update_id)
	{

		if(!is_numeric($update_id)){
			redirect('backend/src/News/manage');
		}

		$submit = $this->input->post('submit', TRUE);

		if($submit=="Cancel"){
			redirect('backend/src/News/create/'.$update_id);
		}elseif($submit=="Yes - Delete News"){
			$this->_process_delete($update_id);

			$flash_msg = "Deleted News was Successfully";
			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
			$this->session->set_flashdata('item', $value);

			redirect('backend/src/News/manage');
		}
	}

	//////// Delete News Page
	function delete_news($update_id)
	{
		if(!is_numeric($update_id)){
			redirect('backend/src/News/manage');
		}

		$data['headline'] = "Delete News";
		$data['update_id'] = $update_id;
		
		$data['content'] = 'backend/news/delete_news';

		$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
	}

	//////// Delete Image Thumbnail
	function delete_image($update_id)
	{

		if(!is_numeric($update_id)){
			redirect('backend/src/News/manage');
		}	    

		$data = $this->fetch_data_from_db($update_id);
		$big_imgs = $data['new_activity_big_img'];
		$small_imgs = $data['new_activity_small_img'];

		$big_imgs_path = './gallery/news/big_imgs/'.$big_imgs;
		$small_imgs_path = './gallery/news/small_imgs/'.$small_imgs;

		// Remove the images
		if(file_exists($big_imgs_path)){
			unlink($big_imgs_path);
		}
		if(file_exists($small_imgs_path)){
			unlink($small_imgs_path);
		}

		// Update Remove the images to database
		unset($data);
		$data['new_activity_big_img'] = "";
		$data['new_activity_small_img'] = "";
		$this->_update($update_id, $data);

		$flash_msg = "The image was successfully deleted."; // Alert message
		$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
		$this->session->set_flashdata('item', $value); // สร้าง session alert

		redirect('backend/src/News/create/'.$update_id);
	}

	//////// Set Image Thumbnail
	function _generate_thumbnail($file_name)
	{
		$config['image_library'] = 'gd2';
		$config['source_image'] = './gallery/news/big_imgs/'.$file_name;
		$config['new_image'] = './gallery/news/small_imgs/'.$file_name;
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = TRUE;
		$config['width']         = 360;
		$config['height']       = 208;

		$this->load->library('image_lib', $config);

		$this->image_lib->resize();
	}

	// PREVIEW IMAGE //
	function modal_do_upload($update_id)
	{		
		if(!is_numeric($update_id)){
			redirect('backend/src/News/manage');
		}
	    
		$config['upload_path'] = './gallery/news/preview_imgs/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = 200;
		$config['max_width'] = 1500;
		$config['max_height'] = 1268;

		$this->load->library('upload', $config);

		if(!$this->upload->do_upload('file'))
		{
			// Upload Error
			$data['error'] = array('error' => $this->upload->display_errors("<p style='color:red;'>","</p>"));
			$data['headline'] = "Upload Error";
			$data['update_id'] = $update_id;

		}else{
			// Upload Successful			
			$data = array('upload_data' => $this->upload->data());

			$upload_data = $data['upload_data'];
			$file_name = $upload_data['file_name'];
			$this->_generate_thumbnail($file_name);

			// Update Image to Database
			$update_data['new_activity_preview_img'] = $file_name;
			$this->_update($update_id, $update_data);

			$data['headline'] = "Upload Success";
			$data['update_id'] = $update_id;			

			echo json_encode($update_data['new_activity_preview_img']);

		}
	}
	function fetch_preview_image($update_id) //  Fetch ข้อมูลจาก Database
	{			
		if(!is_numeric($update_id)){
			redirect('backend/src/News/manage');
		}

		$query = $this->get_where($update_id);
		foreach($query->result() as $row){				
			$data['new_activity_preview_img'] = $row->new_activity_preview_img;
		}

		if(!isset($data)){
			$data = "";
		}

		return $data;
	}
	function delete_preview($update_id)
	{

		if(!is_numeric($update_id)){
			redirect('backend/src/News/manage');
		}
	    
		$data = $this->fetch_preview_image($update_id);
		$preview_imgs = $data['new_activity_preview_img'];

		$preview_imgs_path = './gallery/news/preview_imgs/'.$preview_imgs;

		// Remove the images
		if(file_exists($preview_imgs_path)){
			unlink($preview_imgs_path);
		}

		unset($data);
		$data['new_activity_preview_img'] = "";
		$this->_update($update_id, $data);
		echo json_encode("Success");
	}
	function upload_preview($update_id){
		$preview_img_name = $this->fetch_preview_image($update_id)['new_activity_preview_img'];			
		if($preview_img_name != ""){				
			$preview_path = 'gallery/news/preview_imgs/'.$preview_img_name;
			$big_path = 'gallery/news/big_imgs/'.$preview_img_name;
			copy($preview_path, $big_path);
			$this->_generate_thumbnail($preview_img_name);

			$update_data['new_activity_big_img'] = $preview_img_name;
			$update_data['new_activity_small_img'] = explode('.', $preview_img_name)[0]."_thumb.".explode('.', $preview_img_name)[1];
			$this->_update($update_id, $update_data);

			$this->delete_preview($update_id);
		}
	}
	// PREVIEW IMAGE //

	//////// Upload Image News Do Upload
	function do_upload($update_id)
	{

		if(!is_numeric($update_id)){
			redirect('backend/src/News/manage');
		}

		$submit = $this->input->post('submit', TRUE);
		if($submit == "Cancel"){
			redirect('backend/src/News/create/'.$update_id);
		}

		$config['upload_path'] = './gallery/news/big_imgs/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = 200;
		$config['max_width'] = 1500;
		$config['max_height'] = 1268;

		$this->load->library('upload', $config);

		if(!$this->upload->do_upload('userfile'))
		{
			$data['error'] = array('error' => $this->upload->display_errors("<p style='color:red;'>","</p>"));
			$data['headline'] = "Upload Error";
			$data['update_id'] = $update_id;
			
			$data['content'] = 'backend/news/upload_image';

			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);

		}else{
			// Upload Successful
			
			$data = array('upload_data' => $this->upload->data());

			$upload_data = $data['upload_data'];
			$file_name = $upload_data['file_name'];
			$this->_generate_thumbnail($file_name);

			// Update Image to Database
			$update_data['new_activity_big_img'] = $file_name;
			$update_data['new_activity_small_img'] = explode('.', $file_name)[0]."_thumb.".explode('.', $file_name)[1];
			$this->_update($update_id, $update_data);

			$data['headline'] = "Upload Success";
			$data['update_id'] = $update_id;
			
			$data['content'] = 'backend/news/upload_image';
			
			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}
	}

	//////// Upload Image News
	function upload_image($update_id)
	{
		if(!is_numeric($update_id)){
			redirect('backend/src/News/manage');
		}
		
		$data['headline'] = "Upload Image​ News";
		$data['update_id'] = $update_id;
				
		$data['content'] = 'backend/News/upload_image';		
		
		$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
	}

	//////// Add & Update News
	function create()
	{		
		$this->load->library('timedate');
  
		$update_id = $this->uri->segment(5);
		if(isset(explode('_',$update_id)[1])){
			$lang = "_".explode('_',$update_id)[1];
		}
		else{
			$lang = "";
		}
		$update_id = explode('_',$update_id)[0];
		$submit = $this->input->post('submit', TRUE);

		if($submit == "Cancel"){
			redirect('backend/src/News/manage');
		}
		// ถ้า $submit = value->Submit
		if($submit == "Submit"){
			// Process the Form at create.php
			$this->load->library('form_validation'); // Load Library From Validation
			$this->form_validation->set_rules('new_activity_name', '<b>โปรดกรอกชื่อ Title</b>', 'required|max_length[240]');
			if(isset(explode('_',$this->uri->segment(5))[1])){
				$this->form_validation->set_rules('new_activity_name_en', '<b>โปรดกรอกชื่อ Title En</b>', 'required|max_length[240]');					
			}
			$this->form_validation->set_rules('new_activity_status', '<b>โปรดเลือกสถานะการเผยแพร่</b>', 'required|numeric');/*
			$this->form_validation->set_rules('news_startdate', '<b>โปรดเลือกวันเริ่มแพ็กเกจ</b>', 'required');
			$this->form_validation->set_rules('news_enddate', '<b>โปรดเลือกวันสิ้นสุดแพ็กเกจ</b>', 'required');*/
	    	//$this->form_validation->set_rules('news_status', '<b>โปรดเลือกการเผยแพร่</b>', 'required|numeric');

	    	if($this->form_validation->run() == TRUE){ // ถ้า form_validation ทำงานแล้วถูกต้อง
	    		// Get The Variables
	    		$data = $this->fetch_data_from_post(); // ให้ $data = รับค่าจาก From method="POST"
///////////////////////////////////////////////////////////////////////
	    		//$data['news_name'] = $data['news_title'];
///////////////////////////////////////////////////////////////////////
	    		//$title = $this->url_slug($data['news_title']);
	    		//$data['news_url'] = $title; // item_url = item_title

	    		//$data['date_published'] = $this->timedate->make_timestamp_from_datepicker($data['date_published']);
	    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id
	    			$this->upload_preview($update_id);

	    			// Update Items Details
	    			$data['new_activity_id'] = $update_id;
	    			$this->_update($update_id, $data); // Update ลง Database
	    			$flash_msg = "Update News were Successfully"; // Alert message
					$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
					$this->session->set_flashdata('item', $value); // สร้าง session alert
					redirect('backend/src/news/create/'.$update_id.$lang);
	    		}else{
	    			// Insert a New Items
	    			$this->_insert($data);
	    			$update_id = $this->get_max(); // Get the ID at new item

	    			$flash_msg = "Insert News were Successfully";
					$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
					$this->session->set_flashdata('item', $value);
					redirect('backend/src/news/manage/');
	    		}
	    	}
		}

		if((is_numeric($update_id)) && ($submit!="Submit")){
			$data = $this->fetch_data_from_db($update_id);
		}else{
			$data = $this->fetch_data_from_post();
			$data['new_activity_big_img'] = "";
		}

		if(!is_numeric($update_id)){
			$data['headline'] = "เพิ่มข้อมูลข่าวสารและกิจกรรม";
		}else{
			$data['headline'] = "แก้ไขข้อมูลข่าวสารและกิจกรรม";
		}

		$data['update_id'] = $update_id.$lang;
		
		$data['content'] = 'backend/news/formNews';
		
		$data['Additionalscript'] = 'backend/news/scriptNews';
		$data['replace3'] = 'backend/src/ck/replace3';

		$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
	}

	//////// News Healthy Detail
	function manage()

	{
		$data['query'] = $this->get('new_activity_id');
		
		$data['content'] = 'backend/news/manageNews';
		
		$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
	}

	function url_slug($text)
	{
		$array = array("'", '"', '\\', '/', '.', ',', '!', '+', '*', '%', '$', '#', '@', '(', ')', '[', ']', '?', '>', '<', '|', '’', '“', '”', '‘', '’', ';', ':', '–');
		$text = str_replace($array, '', $text); // Replaces all spaces with hyphens.
		$text = preg_replace('/[^A-Za-z0-9ก-๙เ-า\\-]/', '-', $text); // Removes special chars.
		$text = preg_replace('/-+/', "-", $text);
		$text = trim($text, '-');
		$text = urldecode($text);

		if (empty($text)){
			return NULL;
		}

		return $text;
	}

	//////// Fetch Data at method POST
	function fetch_data_from_post() // Fetch ข้อมูลจาก Input POST
	{
		//$data['new_activity_id'] = $this->input->post('new_activity_id', TRUE);
		$data['new_activity_name'] = $this->input->post('new_activity_name', TRUE);
		$data['new_activity_name_en'] = $this->input->post('new_activity_name_en', TRUE);
		$data['description'] = html_escape($this->input->post('description', FALSE));
		$data['description_en'] = html_escape($this->input->post('description_en', FALSE));
		$data['news_video'] = html_escape($this->input->post('news_video', FALSE));
		//$data['new_activity_big_img'] = $this->input->post('new_activity_big_img', TRUE);
		//$data['new_activity_small_img'] = $this->input->post('new_activity_small_img', TRUE);
		$data['new_activity_status'] = $this->input->post('new_activity_status', TRUE);
		$data['cuser'] = $this->session->userdata('ses_id');
		$data['cwhen'] = $this->input->post('cwhen', TRUE);
		$data['mwhen'] = $this->input->post('mwhen', TRUE);
		
		$data['meta_author'] = $this->input->post('meta_author', TRUE);
		$data['meta_keywords'] = $this->input->post('meta_keywords', TRUE);
		$data['meta_description'] = $this->input->post('meta_description', TRUE);
		
		return $data;
	}

	//////// Fetch Data at Database
	function fetch_data_from_db($update_id) //  Fetch ข้อมูลจาก Database
	{
		
		if(!is_numeric($update_id)){
			redirect('backend/src/News/manage');
		}

		$query = $this->get_where($update_id);
		foreach($query->result() as $row){
			$data['new_activity_id'] = $row->new_activity_id;
			$data['new_activity_name'] = $row->new_activity_name;
			$data['new_activity_name_en'] = $row->new_activity_name_en;
			$data['description'] = $row->description;
			$data['description_en'] = $row->description_en;
			$data['new_activity_big_img'] = $row->new_activity_big_img;
			$data['new_activity_small_img'] = $row->new_activity_small_img;
			$data['news_video'] = $row->news_video;
			$data['new_activity_status'] = $row->new_activity_status;
			$data['cuser'] = $row->cuser;
			$data['cwhen'] = $row->cwhen;
			$data['mwhen'] = $row->mwhen;
			
			$data['meta_author'] = $row->meta_author;
			$data['meta_keywords'] = $row->meta_keywords;
			$data['meta_description'] = $row->meta_description;
			
		}

		if(!isset($data)){
			$data = "";
		}

		return $data;
	}

	function get($order_by)
	{
		$this->load->model('Mdl_formnews');
		$query = $this->Mdl_formnews->get($order_by);
		return $query;
	}

	function get_with_limit($limit, $offset, $order_by)
	{
		$this->load->model('Mdl_formnews');
		$query = $this->Mdl_formnews->get_with_limit($limit, $offset, $order_by);
		return $query;
	}

	function get_where($id)
	{
		$this->load->model('Mdl_formnews');
		$query = $this->Mdl_formnews->get_where($id);
		return $query;
	}

	function get_where_custom($col, $value)
	{
		$this->load->model('Mdl_formnews');
		$query = $this->Mdl_formnews->get_where_custom($col, $value);
		return $query;
	}

	function _insert($data)
	{
		$this->load->model('Mdl_formnews');
		$this->Mdl_formnews->_insert($data);
	}

	function _update($id, $data)
	{
		$this->load->model('Mdl_formnews');
		$this->Mdl_formnews->_update($id, $data);
	}

	function _delete($id)
	{
		$this->load->model('Mdl_formnews');
		$this->Mdl_formnews->_delete($id);
	}

	function count_where($column, $value)
	{
		$this->load->model('Mdl_formnews');
		$count = $this->Mdl_formnews->count_where($column, $value);
		return $count;
	}

	function get_max()
	{
		$this->load->model('Mdl_formnews');
		$max_id = $this->Mdl_formnews->get_max();
		return $max_id;
	}

	function _custom_query($mysql_query)
	{
		$this->load->model('Mdl_formnews');
		$query = $this->Mdl_formnews->_custom_query($mysql_query);
		return $query;
	}
	
}
