<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Specialty extends CI_Controller {

		function __construct()
		{
			parent::__construct();

			$this->load->model('Mdl_site_security');
			$this->Mdl_site_security->_make_sure_is_admin();

			$this->load->library('session');

			$this->load->model('Back_Model');
		}

		//////// Process Delete
		//Specialty
		function _process_delete_specialty($update_id)
		{

			$flash_msg = "Deleted Specialty was Successfully";
			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
			$this->session->set_flashdata('item', $value);

			// Delete Items Record
			$this->_delete($update_id);
		}
		//////// Delete Specialty
		function delete_specialty($update_id)
		{

			if(!is_numeric($update_id)){
				redirect('backend/src/Specialty/manage');
			}	

			$this->_process_delete_specialty($update_id);
			redirect('backend/src/Specialty/manage');
		}

		//Sub Specialty
		function _process_delete_sub_specialty($update_id)
		{

			$flash_msg = "Deleted Sub Specialty was Successfully";
			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
			$this->session->set_flashdata('item', $value);

			// Delete Items Record
			$this->_delete_sub($update_id);
		}
		//////// Delete Sub Specialty
		function delete_sub_specialty($update_id)
		{

			if(!is_numeric($update_id)){
				redirect('backend/src/Specialty/manage_sub');
			}			

			$this->_process_delete_sub_specialty($update_id);
			redirect('backend/src/Specialty/manage_sub');
		}

		//////// Add & Update Specialty
		function create()
		{
			
			error_reporting (E_ALL ^ E_NOTICE);
			$this->load->library('timedate');
      
			$update_id = $this->uri->segment(5);
			$submit = $this->input->post('submit', TRUE);

			if($submit == "Cancel"){
				redirect('backend/src/Specialty/manage/');
			}

			// ถ้า $submit = value->Submit
			if($submit == "Submit"){
				$this->load->library('form_validation'); // Load Library From Validation
				$this->form_validation->set_rules('description', '<b>Required Sub Specialty TH</b>', 'required|max_length[240]');
		    	if($this->form_validation->run() == TRUE && $this->check_name_duplicate($this->input->post('description', TRUE)) < 1){
					$data = $this->fetch_data_from_post();
					unset($data['submit']);

		    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id

		    			// Update Items Details
		    			$data['muser'] = $this->session->userdata('ses_id');
		    			$this->_update($update_id, $data); // Update ลง Database
		    			$flash_msg = "Update Sub Specialty were Successfully"; // Alert message
						$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
						$this->session->set_flashdata('item', $value); // สร้าง session alert
						redirect('backend/src/Specialty/create/'.$update_id);
		    		}else{

		    			// Insert a New Items
		    			$data['cuser'] = $this->session->userdata('ses_id');
		    			$this->_insert($data);
		    			$update_id = $this->get_max(); // Get the ID at new item

		    			$flash_msg = "Insert Sub Specialty were Successfully";
						$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
						$this->session->set_flashdata('item', $value);
						redirect('backend/src/Specialty/create/'.$update_id);
		    		}
		    	}
			}

			if((is_numeric($update_id)) && ($submit!="Submit")){
				$data = $this->fetch_data_from_db($update_id);
			}else{
				$data = $this->fetch_data_from_post();
			}

			if(!is_numeric($update_id)){
				$data['headline'] = "Add Specialty";
			}else{
				$data['headline'] = "Edit Specialty";
			}

			if($this->check_name_duplicate($this->input->post('description', TRUE)) > 0){
				$data['error_report'] = "Specialty Name Duplicate";

			}

			$data['update_id'] = $update_id;
			
			// $data['dropdown_parent'] = $this->sub_get_parent('id')->result();
			
			$data['content'] = 'backend/specialty/formSpecialty';
			
			$data['Additionalscript'] = 'backend/specialty/scriptSpecialty';
			
			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

		//////// Add & Update Specialty
		function create_sub()
		{
			
			error_reporting (E_ALL ^ E_NOTICE);
			$this->load->library('timedate');

			$update_id = $this->uri->segment(5);
			$submit = $this->input->post('submit', TRUE);

			if($submit == "Cancel"){
				redirect('backend/src/Specialty/manage_sub/');
			}

			// ถ้า $submit = value->Submit
			if($submit == "Submit"){
				$this->load->library('form_validation'); // Load Library From Validation
				$this->form_validation->set_rules('description', '<b>Required Sub Specialty TH</b>', 'required|max_length[240]');

		    	if($this->form_validation->run() == TRUE && $this->sub_check_name_duplicate($this->input->post('description', TRUE)) < 1){
					$data = $this->fetch_data_from_post();
					unset($data['submit']);

		    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id

		    			// Update Items Details
		    			$data['muser'] = $this->session->userdata('ses_id');
		    			$this->_update_sub($update_id, $data); // Update ลง Database
		    			$flash_msg = "Update Sub Specialty were Successfully"; // Alert message
						$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
						$this->session->set_flashdata('item', $value); // สร้าง session alert
						redirect('backend/src/Specialty/create_sub/'.$update_id);
		    		}else{

		    			// Insert a New Items
		    			$data['cuser'] = $this->session->userdata('ses_id');
		    			$this->_insert_sub($data);
		    			$update_id = $this->get_sub_max(); // Get the ID at new item

		    			$flash_msg = "Insert Sub Specialty were Successfully";
						$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
						$this->session->set_flashdata('item', $value);
						redirect('backend/src/Specialty/create_sub/'.$update_id);
		    		}
		    	}
			}

			if((is_numeric($update_id)) && ($submit!="Submit")){
				$data = $this->sub_fetch_data_from_db($update_id);
			}else{
				$data = $this->fetch_data_from_post();
			}

			if(!is_numeric($update_id)){
				$data['headline'] = "Add Sub Specialty";
			}else{
				$data['headline'] = "Edit Sub Specialty";
			}

			if($this->sub_check_name_duplicate($this->input->post('description', TRUE)) > 0){
				$data['error_report'] = "Sub Specialty Name Duplicate";

			}

			$data['update_id'] = $update_id;
			
			// $data['dropdown_parent'] = $this->sub_get_parent('uid')->result();
			// $data['dropdown_specialty_parent'] = $this->get('uid')->result();
			
			$data['content'] = 'backend/specialty/formSubSpecialty';
			
			$data['Additionalscript'] = 'backend/specialty/scriptSpecialty';

			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

		//////// Specialty Manage
		function manage()
		{

      
			$data['query'] = $this->get('description');			
			
			$data['content'] = 'backend/specialty/manageSpecialty';
			
			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

		//////// Sub Specialty Manage
		function manage_sub()
		{

			$submit = $this->input->post('submit', TRUE);
			$update_id = $this->uri->segment(5);
			if((is_numeric($update_id)) && ($submit!="Submit")){
				$data['query'] = $this->get_parent_sub($update_id);
			}
			else{
				$data['query'] = $this->get_sub('description');
			}
			
			$data['content'] = 'backend/specialty/manageSubSpecialty';
			
			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

		function url_slug($text)
		{
			$array = array("'", '"', '\\', '/', '.', ',', '!', '+', '*', '%', '$', '#', '@', '(', ')', '[', ']', '?', '>', '<', '|', '’', '“', '”', '‘', '’', ';', ':', '–');
			$text = str_replace($array, '', $text); // Replaces all spaces with hyphens.
			$text = preg_replace('/[^A-Za-z0-9ก-๙เ-า\\-]/', '-', $text); // Removes special chars.
			$text = preg_replace('/-+/', "-", $text);
			$text = trim($text, '-');
			$text = urldecode($text);

			if (empty($text)){
				return NULL;
			}

			return $text;
		}

		//////// Fetch Data at method POST
		function fetch_data_from_post() // Fetch ข้อมูลจาก Input POST
		{
			$data = $this->input->post();

			return $data;
		}

		//////// Fetch Data at Database
		function fetch_data_from_db($update_id) //  Fetch ข้อมูลจาก Database
		{
			
			if(!is_numeric($update_id)){
				redirect('backend/src/Specialty/manage');
			}

			$query = $this->get_where($update_id);
			$data = $query->row_array();
			if(!isset($data)){
				$data = "";
			}

			return $data;
		}
		//////// Fetch Data at Database
		function sub_fetch_data_from_db($update_id) //  Fetch ข้อมูลจาก Database
		{
			
			if(!is_numeric($update_id)){
				redirect('backend/src/Specialty/manage');
			}

			$query = $this->get_where_sub($update_id);
			$data = $query->row_array();
			if(!isset($data)){
				$data = "";
			}
			return $data;
		}

		function check_name_duplicate($namecheck)
		{
			$this->load->model('Mdl_formspecialty');
			$query = $this->Mdl_formspecialty->check_name_duplicate($namecheck);
			return $query;
		}
		function sub_check_name_duplicate($namecheck)
		{
			$this->load->model('Mdl_formsubspecialty');
			$query = $this->Mdl_formsubspecialty->check_name_duplicate($namecheck);
			return $query;
		}
		function sub_get_name_parent($idparent)
		{
			$this->load->model('Mdl_formsubspecialty');
			$query = $this->Mdl_formsubspecialty->get_name_parent($idparent);
			return $query;
		}

		function sub_get_id_parent($parentname)
		{
			$this->load->model('Mdl_formsubspecialty');
			$query = $this->Mdl_formsubspecialty->get_id_parent($parentname);
			return $query;
		}

		function sub_get_name_subparent($idsubparent)
		{
			$this->load->model('Mdl_formsubspecialty');
			$query = $this->Mdl_formsubspecialty->get_name_subparent($idsubparent);
			return $query;
		}

		function sub_get_id_subparent($subparentname)
		{
			$this->load->model('Mdl_formsubspecialty');
			$query = $this->Mdl_formsubspecialty->get_id_subparent($subparentname);
			return $query;
		}

		function sub_get_parent($order_by)
		{
			$this->load->model('Mdl_formsubspecialty');
			$query = $this->Mdl_formsubspecialty->get_parent($order_by);
			return $query;
		}

		function get($order_by)
		{
			$this->load->model('Mdl_formspecialty');
			$query = $this->Mdl_formspecialty->get($order_by);
			return $query;
		}

		function get_where($id)
		{
			$this->load->model('Mdl_formspecialty');
			$query = $this->Mdl_formspecialty->get_where($id);
			return $query;
		}

		function _insert($data)
		{
			$this->load->model('Mdl_formspecialty');
			$this->Mdl_formspecialty->_insert($data);
		}

		function _update($id, $data)
		{
			$this->load->model('Mdl_formspecialty');
			$this->Mdl_formspecialty->_update($id, $data);
		}

		function _delete($id)
		{
			$this->load->model('Mdl_formspecialty');
			$this->Mdl_formspecialty->_delete($id);
		}

		function get_max()
		{
			$this->load->model('Mdl_formspecialty');
			$max_id = $this->Mdl_formspecialty->get_max();
			return $max_id;
		}

		function get_sub($order_by)
		{
			$this->load->model('Mdl_formsubspecialty');
			$query = $this->Mdl_formsubspecialty->get($order_by);
			return $query;
		}

		function get_where_sub($id)
		{
			$this->load->model('Mdl_formsubspecialty');
			$query = $this->Mdl_formsubspecialty->get_where($id);
			return $query;
		}

		function get_parent_sub($id)
		{
			$this->load->model('Mdl_formsubspecialty');
			$query = $this->Mdl_formsubspecialty->get_parent_sub($id);
			return $query;
		}

		function _insert_sub($data)
		{
			$this->load->model('Mdl_formsubspecialty');
			$this->Mdl_formsubspecialty->_insert($data);
		}

		function _update_sub($id, $data)
		{
			$this->load->model('Mdl_formsubspecialty');
			$this->Mdl_formsubspecialty->_update($id, $data);
		}


		function _delete_sub($id)
		{
			$this->load->model('Mdl_formsubspecialty');
			$this->Mdl_formsubspecialty->_delete($id);
		}

		function get_sub_max()
		{
			$this->load->model('Mdl_formsubspecialty');
			$max_id = $this->Mdl_formsubspecialty->get_max();
			return $max_id;
		}

		function _custom_query($mysql_query)
		{
			$this->load->model('Mdl_formspecialty');
			$query = $this->Mdl_formspecialty->_custom_query($mysql_query);
			return $query;
		}
		
	}
