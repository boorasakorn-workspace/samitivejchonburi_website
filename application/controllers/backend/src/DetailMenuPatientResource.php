<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class DetailMenuPatientResource extends CI_Controller {

		function __construct()
		{
			parent::__construct();
		}

		//////// Process Delete All 1.Colours 2.Sizes 3.Big Image 4.Small Image 5.Data Record
		function _process_delete($update_id)
		{

$this->load->model('Mdl_site_security');
$this->Mdl_site_security->_make_sure_is_admin();

			// Delete Patient Logo Big & Small Image
			$data = $this->fetch_data_from_db($update_id);
			$big_imgs = $data['big_img'];
			$small_imgs = $data['small_img'];

			$big_imgs_path = './gallery/patient/big_imgs/'.$big_imgs;
			$small_imgs_path = './gallery/patient/small_imgs/'.$small_imgs;

				// Remove the images
				if(file_exists($big_imgs_path)){
					unlink($big_imgs_path);
				}
				if(file_exists($small_imgs_path)){
					unlink($small_imgs_path);
				}

			// Delete Items Record from tb_DetailMenuPatientResource database
			$this->_delete($update_id);
		}

		//////// Delete Patient Process from controller backend/src/DetailMenuPatientResource/delete_patient
		function delete_Detail_process($update_id)
		{

$this->load->model('Mdl_site_security');
$this->Mdl_site_security->_make_sure_is_admin();

			if(!is_numeric($update_id)){
				redirect('backend/src/DetailMenuPatientResource/manage');
			}

			$this->load->library('session');

			$submit = $this->input->post('submit', TRUE);

			if($submit=="Cancel"){
				redirect('backend/src/DetailMenuPatientResource/create/'.$update_id);
			}elseif($submit=="Yes - Delete MedicalHistory"){
				$this->_process_delete($update_id);

				$flash_msg = "Deleted Detail was Successfully";
				$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
				$this->session->set_flashdata('item', $value);

				redirect($_SERVER['HTTP_REFERER']);
			}


		}

		//////// Delete Patient Page
		function delete_patient($update_id)
		{

$this->load->model('Mdl_site_security');
$this->Mdl_site_security->_make_sure_is_admin();

			if(!is_numeric($update_id)){
				redirect($_SERVER['HTTP_REFERER']);
			}

			$this->load->library('session');

			$data['headline'] = "Delete Patient";
			$data['update_id'] = $update_id;
			$data['flash'] = $this->session->flashdata('item');

			// SESSION
		    $data['username'] = $this->session->userdata('ses_username');
		    $data['fullname'] = $this->session->userdata('ses_full_name');
		    $data['position'] = $this->session->userdata('ses_position');
		    $data['pic'] = $this->session->userdata('ses_pic');
		    // END SESSION
			
			$data['header'] = 'backend/src/header';
			$data['sidebar'] = 'backend/src/sidebar';
			$data['content'] = 'backend/DetailMenuPatientResource/delete_patient';
			$data['script'] = 'backend/src/script';
			
			$this->load->view('backend/dashboard', $data);
		}

		//////// Delete Image Thumbnail
		function delete_image($update_id)
		{
			
$this->load->model('Mdl_site_security');
$this->Mdl_site_security->_make_sure_is_admin();

			if(!is_numeric($update_id)){
				redirect($_SERVER['HTTP_REFERER']);
			}

		    $this->load->library('session');

			$data = $this->fetch_data_from_db($update_id);
			$big_imgs = $data['big_img'];
			$small_imgs = $data['small_img'];

			$big_imgs_path = './gallery/patient/big_imgs/'.$big_imgs;
			$small_imgs_path = './gallery/patient/small_imgs/'.$small_imgs;

			// Remove the images
			if(file_exists($big_imgs_path)){
				unlink($big_imgs_path);
			}
			if(file_exists($small_imgs_path)){
				unlink($small_imgs_path);
			}

			// Update Remove the images to database
			unset($data);
			$data['big_img'] = "";
			$data['small_img'] = "";
			$this->_update($update_id, $data);

			$flash_msg = "The image was successfully deleted."; // Alert message
			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
			$this->session->set_flashdata('item', $value); // สร้าง session alert

			redirect('backend/src/DetailMenuPatientResource/create/'.$update_id);
		}

		//////// Set Image Thumbnail
		function _generate_thumbnail($file_name)
		{
			$config['image_library'] = 'gd2';
			$config['source_image'] = './gallery/patient/big_imgs/'.$file_name;
			$config['new_image'] = './gallery/patient/small_imgs/'.$file_name;
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = TRUE;
			$config['width']         = 360;
			$config['height']       = 208;

			$this->load->library('image_lib', $config);

			$this->image_lib->resize();
		}

		//////// Upload Image Patient Do Upload
		function do_upload($update_id)
		{
			
$this->load->model('Mdl_site_security');
$this->Mdl_site_security->_make_sure_is_admin();

			if(!is_numeric($update_id)){
				redirect($_SERVER['HTTP_REFERER']);
			}

		    $this->load->library('session');

			$submit = $this->input->post('submit', TRUE);
			if($submit == "Cancel"){
				redirect('backend/src/DetailMenuPatientResource/create/'.$update_id);
			}

			$config['upload_path'] = './gallery/patient/big_imgs/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = 200;
			$config['max_width'] = 1500;
			$config['max_height'] = 1268;

			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('userfile'))
			{
				$data['error'] = array('error' => $this->upload->display_errors("<p style='color:red;'>","</p>"));
				$data['headline'] = "Upload Error";
				$data['update_id'] = $update_id;
				$data['flash'] = $this->session->flashdata('item');

				// SESSION
		      	$data['username'] = $this->session->userdata('ses_username');
		      	$data['fullname'] = $this->session->userdata('ses_full_name');
		      	$data['position'] = $this->session->userdata('ses_position');
		      	$data['pic'] = $this->session->userdata('ses_pic');
		     	// END SESSION

				$data['header'] = 'backend/src/header';
				$data['sidebar'] = 'backend/src/sidebar';
				$data['content'] = 'backend/DetailMenuPatientResource/upload_image';
				$data['script'] = 'backend/src/script';
				
				$this->load->view('backend/dashboard', $data);

			}else{
				// Upload Successful
				
				$data = array('upload_data' => $this->upload->data());

				$upload_data = $data['upload_data'];
				$file_name = $upload_data['file_name'];
				$this->_generate_thumbnail($file_name);

				// Update Image to Database
				$update_data['big_img'] = $file_name;
				$update_data['small_img'] = explode('.', $file_name)[0]."_thumb.".explode('.', $file_name)[1];
				$this->_update($update_id, $update_data);

				$data['headline'] = "Upload Success";
				$data['update_id'] = $update_id;
				$data['flash'] = $this->session->flashdata('item');
				
				// SESSION
		      	$data['username'] = $this->session->userdata('ses_username');
		      	$data['fullname'] = $this->session->userdata('ses_full_name');
		      	$data['position'] = $this->session->userdata('ses_position');
		      	$data['pic'] = $this->session->userdata('ses_pic');
		      	// END SESSION

				$data['header'] = 'backend/src/header';
				$data['sidebar'] = 'backend/src/sidebar';
				$data['content'] = 'backend/DetailMenuPatientResource/upload_image';
				$data['script'] = 'backend/src/script';
				
				$this->load->view('backend/dashboard', $data);
			}
		}

		//////// Upload Image Patient
		function upload_image($update_id)
		{
			
$this->load->model('Mdl_site_security');
$this->Mdl_site_security->_make_sure_is_admin();

			if(!is_numeric($update_id)){
				redirect($_SERVER['HTTP_REFERER']);
			}

		    $this->load->library('session');
			
			$data['headline'] = "Upload Image​ Patient";
			$data['update_id'] = $update_id;
			$data['flash'] = $this->session->flashdata('item');

			// SESSION
	      	$data['username'] = $this->session->userdata('ses_username');
	      	$data['fullname'] = $this->session->userdata('ses_full_name');
	      	$data['position'] = $this->session->userdata('ses_position');
	      	$data['pic'] = $this->session->userdata('ses_pic');
	      	// END SESSION
			
			$data['header'] = 'backend/src/header';
			$data['sidebar'] = 'backend/src/sidebar';
			$data['content'] = 'backend/DetailMenuPatientResource/upload_image';
			$data['script'] = 'backend/src/script';
			
			$this->load->view('backend/dashboard', $data);
		}

		//////// Add & Update Patient
		function create($pages = null)
		{
			$this->load->library('session');
			$this->load->library('Timedate');
			
			$this->load->model('Mdl_site_security');
			$this->Mdl_site_security->_make_sure_is_admin();


			if($pages == 'Accommodations'){
				$update_id = $this->uri->segment(6);

				$submit = $this->input->post('submit', TRUE);

				if($submit == "Cancel"){
					redirect('backend/src/DetailMenuPatientResource/manageAccommodations');
				}

				// ถ้า $submit = value->Submit
				if($submit == "Submit"){
					// Process the Form at create.php
					$this->load->library('form_validation'); // Load Library From Validation

					$this->form_validation->set_rules('pt_title', '<b>โปรดกรอกเฉพาะชื่อ Title</b>', 'required|max_length[240]');
			    	$this->form_validation->set_rules('pt_status', '<b>โปรดเลือกการเผยแพร่</b>', 'required|numeric');

			    	if($this->form_validation->run() == TRUE){ // ถ้า form_validation ทำงานแล้วถูกต้อง
			    		// Get The Variables
			    		$data = $this->fetch_data_from_post(); // ให้ $data = รับค่าจาก From method="POST"
			    		$title = $this->url_slug($data['pt_title']);
			    		$data['pt_url'] = $title; // item_url = item_title

			    		$data['date_published'] = $this->timedate->make_timestamp_from_datepicker($data['date_published']);
			    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id

			    			// Update Items Details		    			
			    			$data['m_user'] = $this->session->userdata('ses_id');
			    			$this->_update($update_id, $data); // Update ลง Database
			    			$flash_msg = "Update Accommodations were Successfully"; // Alert message
							$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
							$this->session->set_flashdata('item', $value); // สร้าง session alert
							redirect('backend/src/DetailMenuPatientResource/create/'.$update_id);
			    		}else{
			    			// Insert a New Items
			    			$data['c_user'] = $this->session->userdata('ses_id');
			    			$this->_insert($data);
			    			$update_id = $this->get_max(); // Get the ID at new item

			    			$flash_msg = "Insert Accommodations were Successfully";
							$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
							$this->session->set_flashdata('item', $value);
							redirect('backend/src/DetailMenuPatientResource/create/'.$update_id);
			    		}
			    	}
				}

				if((is_numeric($update_id)) && ($submit!="Submit")){
					$data = $this->fetch_data_from_db($update_id);
				}else{
					$data = $this->fetch_data_from_post();
					$data['big_imgs'] = "";
				}

				if(!is_numeric($update_id)){
					$data['headline'] = "เพิ่มข้อมูลข้อมูลผู้ป่วย - ห้องพัก";
				}else{
					$data['headline'] = "แก้ไขข้อมูลผู้ป่วย - ห้องพัก";
				}

				if($data['date_published'] > 0){
					// Format unique timestamp to datepicker
					$data['date_published'];
				}

				$data['update_id'] = $update_id;
				$data['flash'] = $this->session->flashdata('item');

				// SESSION
		      	$data['username'] = $this->session->userdata('ses_username');
		      	$data['fullname'] = $this->session->userdata('ses_full_name');
		      	$data['position'] = $this->session->userdata('ses_position');
		      	$data['pic'] = $this->session->userdata('ses_pic');
		      	// END SESSION

				$data['header'] = 'backend/src/header';
				$data['sidebar'] = 'backend/src/sidebar';
				$data['content'] = 'backend/DetailMenuPatientResource/formAccommodations';
				$data['script'] = 'backend/src/script';
				
				$this->load->view('backend/dashboard', $data);

			}
			else if($pages == 'MedicalHistory'){
				$update_id = $this->uri->segment(6);

				$submit = $this->input->post('submit', TRUE);

				if($submit == "Cancel"){
					redirect('backend/src/DetailMenuPatientResource/manageMedicalHistory');
				}

				// ถ้า $submit = value->Submit
				if($submit == "Submit"){
					// Process the Form at create.php
					$this->load->library('form_validation'); // Load Library From Validation

					$this->form_validation->set_rules('pt_title', '<b>โปรดกรอกเฉพาะชื่อ Title</b>', 'required|max_length[240]');
			    	$this->form_validation->set_rules('pt_status', '<b>โปรดเลือกการเผยแพร่</b>', 'required|numeric');

			    	if($this->form_validation->run() == TRUE){ // ถ้า form_validation ทำงานแล้วถูกต้อง
			    		// Get The Variables
			    		$data = $this->fetch_data_from_post(); // ให้ $data = รับค่าจาก From method="POST"
			    		$title = $this->url_slug($data['pt_title']);
			    		$data['pt_url'] = $title; // item_url = item_title

			    		$data['date_published'] = $this->timedate->make_timestamp_from_datepicker($data['date_published']);
			    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id

			    			// Update Items Details		    			
			    			$data['m_user'] = $this->session->userdata('ses_id');
			    			$this->_update($update_id, $data); // Update ลง Database
			    			$flash_msg = "Update MedicalHistory were Successfully"; // Alert message
							$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
							$this->session->set_flashdata('item', $value); // สร้าง session alert
							redirect('backend/src/DetailMenuPatientResource/create/'.$update_id);
			    		}else{
			    			// Insert a New Items
			    			$data['c_user'] = $this->session->userdata('ses_id');
			    			$this->_insert($data);
			    			$update_id = $this->get_max(); // Get the ID at new item

			    			$flash_msg = "Insert MedicalHistory were Successfully";
							$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
							$this->session->set_flashdata('item', $value);
							redirect('backend/src/DetailMenuPatientResource/create/'.$update_id);
			    		}
			    	}
				}

				if((is_numeric($update_id)) && ($submit!="Submit")){
					$data = $this->fetch_data_from_db($update_id);
				}else{
					$data = $this->fetch_data_from_post();
					$data['big_imgs'] = "";
				}

				if(!is_numeric($update_id)){
					$data['headline'] = "เพิ่มข้อมูลข้อมูลผู้ป่วย - ห้องพัก";
				}else{
					$data['headline'] = "แก้ไขข้อมูลผู้ป่วย - ห้องพัก";
				}

				if($data['date_published'] > 0){
					// Format unique timestamp to datepicker
					$data['date_published'];
				}

				$data['update_id'] = $update_id;
				$data['flash'] = $this->session->flashdata('item');

				// SESSION
		      	$data['username'] = $this->session->userdata('ses_username');
		      	$data['fullname'] = $this->session->userdata('ses_full_name');
		      	$data['position'] = $this->session->userdata('ses_position');
		      	$data['pic'] = $this->session->userdata('ses_pic');
		      	// END SESSION

				$data['header'] = 'backend/src/header';
				$data['sidebar'] = 'backend/src/sidebar';
				$data['content'] = 'backend/DetailMenuPatientResource/formMedicalHistory';
				$data['script'] = 'backend/src/script';
				
				$this->load->view('backend/dashboard', $data);

			}
			else if($pages == 'Insurance'){
				$update_id = $this->uri->segment(6);

				$submit = $this->input->post('submit', TRUE);

				if($submit == "Cancel"){
					redirect('backend/src/DetailMenuPatientResource/manageInsurance');
				}

				// ถ้า $submit = value->Submit
				if($submit == "Submit"){
					// Process the Form at create.php
					$this->load->library('form_validation'); // Load Library From Validation

					$this->form_validation->set_rules('pt_title', '<b>โปรดกรอกเฉพาะชื่อ Title</b>', 'required|max_length[240]');
			    	$this->form_validation->set_rules('pt_status', '<b>โปรดเลือกการเผยแพร่</b>', 'required|numeric');

			    	if($this->form_validation->run() == TRUE){ // ถ้า form_validation ทำงานแล้วถูกต้อง
			    		// Get The Variables
			    		$data = $this->fetch_data_from_post(); // ให้ $data = รับค่าจาก From method="POST"
			    		$title = $this->url_slug($data['pt_title']);
			    		$data['pt_url'] = $title; // item_url = item_title

			    		$data['date_published'] = $this->timedate->make_timestamp_from_datepicker($data['date_published']);
			    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id

			    			// Update Items Details		    			
			    			$data['m_user'] = $this->session->userdata('ses_id');
			    			$this->_update($update_id, $data); // Update ลง Database
			    			$flash_msg = "Update Insurance were Successfully"; // Alert message
							$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
							$this->session->set_flashdata('item', $value); // สร้าง session alert
							redirect('backend/src/DetailMenuPatientResource/create/'.$update_id);
			    		}else{
			    			// Insert a New Items
			    			$data['c_user'] = $this->session->userdata('ses_id');
			    			$this->_insert($data);
			    			$update_id = $this->get_max(); // Get the ID at new item

			    			$flash_msg = "Insert Insurance were Successfully";
							$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
							$this->session->set_flashdata('item', $value);
							redirect('backend/src/DetailMenuPatientResource/create/'.$update_id);
			    		}
			    	}
				}

				if((is_numeric($update_id)) && ($submit!="Submit")){
					$data = $this->fetch_data_from_db($update_id);
				}else{
					$data = $this->fetch_data_from_post();
					$data['big_imgs'] = "";
				}

				if(!is_numeric($update_id)){
					$data['headline'] = "เพิ่มข้อมูลข้อมูลผู้ป่วย - ห้องพัก";
				}else{
					$data['headline'] = "แก้ไขข้อมูลผู้ป่วย - ห้องพัก";
				}

				if($data['date_published'] > 0){
					// Format unique timestamp to datepicker
					$data['date_published'];
				}

				$data['update_id'] = $update_id;
				$data['flash'] = $this->session->flashdata('item');

				// SESSION
		      	$data['username'] = $this->session->userdata('ses_username');
		      	$data['fullname'] = $this->session->userdata('ses_full_name');
		      	$data['position'] = $this->session->userdata('ses_position');
		      	$data['pic'] = $this->session->userdata('ses_pic');
		      	// END SESSION

				$data['header'] = 'backend/src/header';
				$data['sidebar'] = 'backend/src/sidebar';
				$data['content'] = 'backend/DetailMenuPatientResource/formInsurance';
				$data['script'] = 'backend/src/script';
				
				$this->load->view('backend/dashboard', $data);

			}
			else if($pages == 'DirectDisbursementProject'){
				$update_id = $this->uri->segment(6);

				$submit = $this->input->post('submit', TRUE);

				if($submit == "Cancel"){
					redirect('backend/src/DetailMenuPatientResource/manageRestaurantsShop');
				}

				// ถ้า $submit = value->Submit
				if($submit == "Submit"){
					// Process the Form at create.php
					$this->load->library('form_validation'); // Load Library From Validation

					$this->form_validation->set_rules('pt_title', '<b>โปรดกรอกเฉพาะชื่อ Title</b>', 'required|max_length[240]');
			    	$this->form_validation->set_rules('pt_status', '<b>โปรดเลือกการเผยแพร่</b>', 'required|numeric');

			    	if($this->form_validation->run() == TRUE){ // ถ้า form_validation ทำงานแล้วถูกต้อง
			    		// Get The Variables
			    		$data = $this->fetch_data_from_post(); // ให้ $data = รับค่าจาก From method="POST"
			    		$title = $this->url_slug($data['pt_title']);
			    		$data['pt_url'] = $title; // item_url = item_title

			    		$data['date_published'] = $this->timedate->make_timestamp_from_datepicker($data['date_published']);
			    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id

			    			// Update Items Details		    			
			    			$data['m_user'] = $this->session->userdata('ses_id');
			    			$this->_update($update_id, $data); // Update ลง Database
			    			$flash_msg = "Update RestaurantsShop were Successfully"; // Alert message
							$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
							$this->session->set_flashdata('item', $value); // สร้าง session alert
							redirect('backend/src/DetailMenuPatientResource/create/'.$update_id);
			    		}else{
			    			// Insert a New Items
			    			$data['c_user'] = $this->session->userdata('ses_id');
			    			$this->_insert($data);
			    			$update_id = $this->get_max(); // Get the ID at new item

			    			$flash_msg = "Insert RestaurantsShop were Successfully";
							$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
							$this->session->set_flashdata('item', $value);
							redirect('backend/src/DetailMenuPatientResource/create/'.$update_id);
			    		}
			    	}
				}

				if((is_numeric($update_id)) && ($submit!="Submit")){
					$data = $this->fetch_data_from_db($update_id);
				}else{
					$data = $this->fetch_data_from_post();
					$data['big_imgs'] = "";
				}

				if(!is_numeric($update_id)){
					$data['headline'] = "เพิ่มข้อมูลข้อมูลผู้ป่วย - ห้องพัก";
				}else{
					$data['headline'] = "แก้ไขข้อมูลผู้ป่วย - ห้องพัก";
				}

				if($data['date_published'] > 0){
					// Format unique timestamp to datepicker
					$data['date_published'];
				}

				$data['update_id'] = $update_id;
				$data['flash'] = $this->session->flashdata('item');

				// SESSION
		      	$data['username'] = $this->session->userdata('ses_username');
		      	$data['fullname'] = $this->session->userdata('ses_full_name');
		      	$data['position'] = $this->session->userdata('ses_position');
		      	$data['pic'] = $this->session->userdata('ses_pic');
		      	// END SESSION

				$data['header'] = 'backend/src/header';
				$data['sidebar'] = 'backend/src/sidebar';
				$data['content'] = 'backend/DetailMenuPatientResource/formRestaurantsShop';
				$data['script'] = 'backend/src/script';
				
				$this->load->view('backend/dashboard', $data);

			}
			else if($pages == 'RestaurantsShop'){
				$update_id = $this->uri->segment(6);

				$submit = $this->input->post('submit', TRUE);

				if($submit == "Cancel"){
					redirect('backend/src/DetailMenuPatientResource/manageRestaurantsShop');
				}

				// ถ้า $submit = value->Submit
				if($submit == "Submit"){
					// Process the Form at create.php
					$this->load->library('form_validation'); // Load Library From Validation

					$this->form_validation->set_rules('pt_title', '<b>โปรดกรอกเฉพาะชื่อ Title</b>', 'required|max_length[240]');
			    	$this->form_validation->set_rules('pt_status', '<b>โปรดเลือกการเผยแพร่</b>', 'required|numeric');

			    	if($this->form_validation->run() == TRUE){ // ถ้า form_validation ทำงานแล้วถูกต้อง
			    		// Get The Variables
			    		$data = $this->fetch_data_from_post(); // ให้ $data = รับค่าจาก From method="POST"
			    		$title = $this->url_slug($data['pt_title']);
			    		$data['pt_url'] = $title; // item_url = item_title

			    		$data['date_published'] = $this->timedate->make_timestamp_from_datepicker($data['date_published']);
			    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id

			    			// Update Items Details		    			
			    			$data['m_user'] = $this->session->userdata('ses_id');
			    			$this->_update($update_id, $data); // Update ลง Database
			    			$flash_msg = "Update RestaurantsShop were Successfully"; // Alert message
							$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
							$this->session->set_flashdata('item', $value); // สร้าง session alert
							redirect('backend/src/DetailMenuPatientResource/create/'.$update_id);
			    		}else{
			    			// Insert a New Items
			    			$data['c_user'] = $this->session->userdata('ses_id');
			    			$this->_insert($data);
			    			$update_id = $this->get_max(); // Get the ID at new item

			    			$flash_msg = "Insert RestaurantsShop were Successfully";
							$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
							$this->session->set_flashdata('item', $value);
							redirect('backend/src/DetailMenuPatientResource/create/'.$update_id);
			    		}
			    	}
				}

				if((is_numeric($update_id)) && ($submit!="Submit")){
					$data = $this->fetch_data_from_db($update_id);
				}else{
					$data = $this->fetch_data_from_post();
					$data['big_imgs'] = "";
				}

				if(!is_numeric($update_id)){
					$data['headline'] = "เพิ่มข้อมูลข้อมูลผู้ป่วย - ห้องพัก";
				}else{
					$data['headline'] = "แก้ไขข้อมูลผู้ป่วย - ห้องพัก";
				}

				if($data['date_published'] > 0){
					// Format unique timestamp to datepicker
					$data['date_published'];
				}

				$data['update_id'] = $update_id;
				$data['flash'] = $this->session->flashdata('item');

				// SESSION
		      	$data['username'] = $this->session->userdata('ses_username');
		      	$data['fullname'] = $this->session->userdata('ses_full_name');
		      	$data['position'] = $this->session->userdata('ses_position');
		      	$data['pic'] = $this->session->userdata('ses_pic');
		      	// END SESSION

				$data['header'] = 'backend/src/header';
				$data['sidebar'] = 'backend/src/sidebar';
				$data['content'] = 'backend/DetailMenuPatientResource/formRestaurantsShop';
				$data['script'] = 'backend/src/script';
				
				$this->load->view('backend/dashboard', $data);

			}
			else if($pages == 'MedicalRecordsReports'){
				$update_id = $this->uri->segment(6);

				$submit = $this->input->post('submit', TRUE);

				if($submit == "Cancel"){
					redirect('backend/src/DetailMenuPatientResource/manageMedicalRecordsReports');
				}

				// ถ้า $submit = value->Submit
				if($submit == "Submit"){
					// Process the Form at create.php
					$this->load->library('form_validation'); // Load Library From Validation

					$this->form_validation->set_rules('pt_title', '<b>โปรดกรอกเฉพาะชื่อ Title</b>', 'required|max_length[240]');
			    	$this->form_validation->set_rules('pt_status', '<b>โปรดเลือกการเผยแพร่</b>', 'required|numeric');

			    	if($this->form_validation->run() == TRUE){ // ถ้า form_validation ทำงานแล้วถูกต้อง
			    		// Get The Variables
			    		$data = $this->fetch_data_from_post(); // ให้ $data = รับค่าจาก From method="POST"
			    		$title = $this->url_slug($data['pt_title']);
			    		$data['pt_url'] = $title; // item_url = item_title

			    		$data['date_published'] = $this->timedate->make_timestamp_from_datepicker($data['date_published']);
			    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id

			    			// Update Items Details		    			
			    			$data['m_user'] = $this->session->userdata('ses_id');
			    			$this->_update($update_id, $data); // Update ลง Database
			    			$flash_msg = "Update MedicalRecordsReports were Successfully"; // Alert message
							$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
							$this->session->set_flashdata('item', $value); // สร้าง session alert
							redirect('backend/src/DetailMenuPatientResource/create/'.$update_id);
			    		}else{
			    			// Insert a New Items
			    			$data['c_user'] = $this->session->userdata('ses_id');
			    			$this->_insert($data);
			    			$update_id = $this->get_max(); // Get the ID at new item

			    			$flash_msg = "Insert MedicalRecordsReports were Successfully";
							$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
							$this->session->set_flashdata('item', $value);
							redirect('backend/src/DetailMenuPatientResource/create/'.$update_id);
			    		}
			    	}
				}

				if((is_numeric($update_id)) && ($submit!="Submit")){
					$data = $this->fetch_data_from_db($update_id);
				}else{
					$data = $this->fetch_data_from_post();
					$data['big_imgs'] = "";
				}

				if(!is_numeric($update_id)){
					$data['headline'] = "เพิ่มข้อมูลข้อมูลผู้ป่วย - ห้องพัก";
				}else{
					$data['headline'] = "แก้ไขข้อมูลผู้ป่วย - ห้องพัก";
				}

				if($data['date_published'] > 0){
					// Format unique timestamp to datepicker
					$data['date_published'];
				}

				$data['update_id'] = $update_id;
				$data['flash'] = $this->session->flashdata('item');

				// SESSION
		      	$data['username'] = $this->session->userdata('ses_username');
		      	$data['fullname'] = $this->session->userdata('ses_full_name');
		      	$data['position'] = $this->session->userdata('ses_position');
		      	$data['pic'] = $this->session->userdata('ses_pic');
		      	// END SESSION

				$data['header'] = 'backend/src/header';
				$data['sidebar'] = 'backend/src/sidebar';
				$data['content'] = 'backend/DetailMenuPatientResource/formMedicalRecordsReports';
				$data['script'] = 'backend/src/script';
				
				$this->load->view('backend/dashboard', $data);

			}
			else if($pages == 'FinancialBillingMatters'){
				$update_id = $this->uri->segment(6);

				$submit = $this->input->post('submit', TRUE);

				if($submit == "Cancel"){
					redirect('backend/src/DetailMenuPatientResource/manageFinancialBillingMatters');
				}

				// ถ้า $submit = value->Submit
				if($submit == "Submit"){
					// Process the Form at create.php
					$this->load->library('form_validation'); // Load Library From Validation

					$this->form_validation->set_rules('pt_title', '<b>โปรดกรอกเฉพาะชื่อ Title</b>', 'required|max_length[240]');
			    	$this->form_validation->set_rules('pt_status', '<b>โปรดเลือกการเผยแพร่</b>', 'required|numeric');

			    	if($this->form_validation->run() == TRUE){ // ถ้า form_validation ทำงานแล้วถูกต้อง
			    		// Get The Variables
			    		$data = $this->fetch_data_from_post(); // ให้ $data = รับค่าจาก From method="POST"
			    		$title = $this->url_slug($data['pt_title']);
			    		$data['pt_url'] = $title; // item_url = item_title

			    		$data['date_published'] = $this->timedate->make_timestamp_from_datepicker($data['date_published']);
			    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id

			    			// Update Items Details		    			
			    			$data['m_user'] = $this->session->userdata('ses_id');
			    			$this->_update($update_id, $data); // Update ลง Database
			    			$flash_msg = "Update FinancialBillingMatters were Successfully"; // Alert message
							$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
							$this->session->set_flashdata('item', $value); // สร้าง session alert
							redirect('backend/src/DetailMenuPatientResource/create/'.$update_id);
			    		}else{
			    			// Insert a New Items
			    			$data['c_user'] = $this->session->userdata('ses_id');
			    			$this->_insert($data);
			    			$update_id = $this->get_max(); // Get the ID at new item

			    			$flash_msg = "Insert FinancialBillingMatters were Successfully";
							$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
							$this->session->set_flashdata('item', $value);
							redirect('backend/src/DetailMenuPatientResource/create/'.$update_id);
			    		}
			    	}
				}

				if((is_numeric($update_id)) && ($submit!="Submit")){
					$data = $this->fetch_data_from_db($update_id);
				}else{
					$data = $this->fetch_data_from_post();
					$data['big_imgs'] = "";
				}

				if(!is_numeric($update_id)){
					$data['headline'] = "เพิ่มข้อมูลข้อมูลผู้ป่วย - ห้องพัก";
				}else{
					$data['headline'] = "แก้ไขข้อมูลผู้ป่วย - ห้องพัก";
				}

				if($data['date_published'] > 0){
					// Format unique timestamp to datepicker
					$data['date_published'];
				}

				$data['update_id'] = $update_id;
				$data['flash'] = $this->session->flashdata('item');

				// SESSION
		      	$data['username'] = $this->session->userdata('ses_username');
		      	$data['fullname'] = $this->session->userdata('ses_full_name');
		      	$data['position'] = $this->session->userdata('ses_position');
		      	$data['pic'] = $this->session->userdata('ses_pic');
		      	// END SESSION

				$data['header'] = 'backend/src/header';
				$data['sidebar'] = 'backend/src/sidebar';
				$data['content'] = 'backend/DetailMenuPatientResource/formFinancialBillingMatters';
				$data['script'] = 'backend/src/script';
				
				$this->load->view('backend/dashboard', $data);

			}
			
			
		}

		//////// Patient Detail
		function manage($detail=null)
		{
      $this->load->model('Mdl_site_security');
      $this->Mdl_site_security->_make_sure_is_admin();

      		if($detail == 'MedicalHistory'){//ประวัติการรักษา
      			$mysql_query = "SELECT * FROM tb_patientresources WHERE pt_service_name='MedicalHistory'";
				$rs = $this->_custom_query($mysql_query);
				foreach($rs->result() as $res){
					$data['medicalhistory'] = $res;
					// echo "<pre>"; print_r($res); die();
				}

				// $data['query'] = $this->get('patient_title');
				$data['flash'] = $this->session->flashdata('item');

				// SESSION
		      	$data['username'] = $this->session->userdata('ses_username');
		      	$data['fullname'] = $this->session->userdata('ses_full_name');
		      	$data['position'] = $this->session->userdata('ses_position');
		      	$data['pic'] = $this->session->userdata('ses_pic');
		      	// END SESSION

				$data['header'] = 'backend/src/header';
				$data['sidebar'] = 'backend/src/sidebar';
				$data['content'] = 'backend/DetailMenuPatientResource/manageMedicalHistory';
				$data['script'] = 'backend/src/script';
      		}else if($detail == 'Insurance'){ //ประกันสุขภาพ
      			$mysql_query = "SELECT * FROM tb_patientresources WHERE pt_service_name='Insurance'";
				$rs = $this->_custom_query($mysql_query);
				foreach($rs->result() as $res){
					$data['insurance'] = $res;
					// echo "<pre>"; print_r($res); die();
				}

				$data['flash'] = $this->session->flashdata('item');

				// SESSION
		      	$data['username'] = $this->session->userdata('ses_username');
		      	$data['fullname'] = $this->session->userdata('ses_full_name');
		      	$data['position'] = $this->session->userdata('ses_position');
		      	$data['pic'] = $this->session->userdata('ses_pic');
		      	// END SESSION

				$data['header'] = 'backend/src/header';
				$data['sidebar'] = 'backend/src/sidebar';
				$data['content'] = 'backend/DetailMenuPatientResource/manageInsurance';
				$data['script'] = 'backend/src/script';
      		}else if($detail == 'DirectDisbursementProject'){ //โครงการเบิกจ่ายตรง
      			$mysql_query = "SELECT * FROM tb_patientresources WHERE pt_service_name='DirectDisbursementProject'";
				$rs = $this->_custom_query($mysql_query);
				foreach($rs->result() as $res){
					$data['direct'] = $res;
					// echo "<pre>"; print_r($res); die();
				}

				$data['flash'] = $this->session->flashdata('item');

				// SESSION
		      	$data['username'] = $this->session->userdata('ses_username');
		      	$data['fullname'] = $this->session->userdata('ses_full_name');
		      	$data['position'] = $this->session->userdata('ses_position');
		      	$data['pic'] = $this->session->userdata('ses_pic');
		      	// END SESSION

				$data['header'] = 'backend/src/header';
				$data['sidebar'] = 'backend/src/sidebar';
				$data['content'] = 'backend/DetailMenuPatientResource/manageDirectDisbursementProject';
				$data['script'] = 'backend/src/script';
      		}else if($detail == 'Accommodations'){ //ห้องพัก

      			$mysql_query = "SELECT * FROM tb_patientresources WHERE pt_service_name='Accommodations'";
				$rs = $this->_custom_query($mysql_query);
				foreach($rs->result() as $res){
					$data['accom'] = $res;
					// echo "<pre>"; print_r($res); die();
				}

				$data['flash'] = $this->session->flashdata('item');

				// SESSION
		      	$data['username'] = $this->session->userdata('ses_username');
		      	$data['fullname'] = $this->session->userdata('ses_full_name');
		      	$data['position'] = $this->session->userdata('ses_position');
		      	$data['pic'] = $this->session->userdata('ses_pic');
		      	// END SESSION

				$data['header'] = 'backend/src/header';
				$data['sidebar'] = 'backend/src/sidebar';
				$data['content'] = 'backend/DetailMenuPatientResource/manageAccommodations';
				$data['script'] = 'backend/src/script';
      		}else if($detail == 'RestaurantsShop'){ //ร้านอาหารและอื่นๆ

      			$mysql_query = "SELECT * FROM tb_patientresources WHERE pt_service_name='RestaurantsShop'";
				$rs = $this->_custom_query($mysql_query);
				foreach($rs->result() as $res){
					$data['resshop'] = $res;
					// echo "<pre>"; print_r($res); die();
				}

				$data['flash'] = $this->session->flashdata('item');

				// SESSION
		      	$data['username'] = $this->session->userdata('ses_username');
		      	$data['fullname'] = $this->session->userdata('ses_full_name');
		      	$data['position'] = $this->session->userdata('ses_position');
		      	$data['pic'] = $this->session->userdata('ses_pic');
		      	// END SESSION

				$data['header'] = 'backend/src/header';
				$data['sidebar'] = 'backend/src/sidebar';
				$data['content'] = 'backend/DetailMenuPatientResource/manageRestaurantShop';
				$data['script'] = 'backend/src/script';
      		}else if($detail == 'MedicalRecordsReports'){ //รายงานผลการรักษา

      			$mysql_query = "SELECT * FROM tb_patientresources WHERE pt_service_name='MedicalRecordsReports'";
				$rs = $this->_custom_query($mysql_query);
				foreach($rs->result() as $res){
					$data['medrecordreport'] = $res;
					// echo "<pre>"; print_r($res); die();
				}

				$data['flash'] = $this->session->flashdata('item');

				// SESSION
		      	$data['username'] = $this->session->userdata('ses_username');
		      	$data['fullname'] = $this->session->userdata('ses_full_name');
		      	$data['position'] = $this->session->userdata('ses_position');
		      	$data['pic'] = $this->session->userdata('ses_pic');
		      	// END SESSION

				$data['header'] = 'backend/src/header';
				$data['sidebar'] = 'backend/src/sidebar';
				$data['content'] = 'backend/DetailMenuPatientResource/manageMedicalRecordsReports';
				$data['script'] = 'backend/src/script';
      		}else if($detail == 'FinancialBillingMatters'){ //การเงินและการเรียกเก็บเงิน

      			$mysql_query = "SELECT * FROM tb_patientresources WHERE pt_service_name='FinancialBillingMatters'";
				$rs = $this->_custom_query($mysql_query);
				foreach($rs->result() as $res){
					$data['financial'] = $res;
				}

				$data['flash'] = $this->session->flashdata('item');

				// SESSION
		      	$data['username'] = $this->session->userdata('ses_username');
		      	$data['fullname'] = $this->session->userdata('ses_full_name');
		      	$data['position'] = $this->session->userdata('ses_position');
		      	$data['pic'] = $this->session->userdata('ses_pic');
		      	// END SESSION

				$data['header'] = 'backend/src/header';
				$data['sidebar'] = 'backend/src/sidebar';
				$data['content'] = 'backend/DetailMenuPatientResource/manageFinancialBillingMatters';
				$data['script'] = 'backend/src/script';
      		}
      
			
			
			$this->load->view('backend/dashboard', $data);
		}

		function url_slug($text)
		{
			$array = array("'", '"', '\\', '/', '.', ',', '!', '+', '*', '%', '$', '#', '@', '(', ')', '[', ']', '?', '>', '<', '|', '’', '“', '”', '‘', '’', ';', ':', '–');
			$text = str_replace($array, '', $text); // Replaces all spaces with hyphens.
			$text = preg_replace('/[^A-Za-z0-9ก-๙เ-า\\-]/', '-', $text); // Removes special chars.
			$text = preg_replace('/-+/', "-", $text);
			$text = trim($text, '-');
			$text = urldecode($text);

			if (empty($text)){
				return NULL;
			}

			return $text;
		}

		//////// Fetch Data at method POST
		function fetch_data_from_post() // Fetch ข้อมูลจาก Input POST
		{
			$data['pt_title'] = $this->input->post('pt_title', TRUE);
			$data['pt_description'] = $this->input->post('pt_description', TRUE);
			$data['pt_status'] = $this->input->post('pt_status', TRUE);
			$data['date_published'] = $this->input->post('date_published', TRUE);

			$data['meta_author'] = $this->input->post('meta_author', TRUE);
			$data['meta_keywords'] = $this->input->post('meta_keywords', TRUE);
			$data['meta_description'] = $this->input->post('meta_description', TRUE);
			
			return $data;
		}

		//////// Fetch Data at Database
		function fetch_data_from_db($update_id) //  Fetch ข้อมูลจาก Database
		{
			
			if(!is_numeric($update_id)){
				redirect($_SERVER['HTTP_REFERER']);
			}

			$query = $this->get_where($update_id);
			foreach($query->result() as $row){
				
				$data['pt_url'] = $row->pt_url;
				$data['pt_title'] = $row->pt_title;
				$data['pt_description'] = $row->pt_description;
				$data['pt_status'] = $row->pt_status;
				$data['big_imgs'] = $row->big_imgs;
				$data['date_published'] = $row->date_published;
				$data['created_at'] = $row->created_at;
				$data['updated_at'] = $row->updated_at;
				
				$data['meta_author'] = $row->meta_author;
				$data['meta_keywords'] = $row->meta_keywords;
				$data['meta_description'] = $row->meta_description;
			}

			if(!isset($data)){
				$data = "";
			}

			return $data;
		}

		function get($order_by)
		{
			$this->load->model('Mdl_formpatientresources');
			$query = $this->Mdl_formpatientresources->get($order_by);
			return $query;
		}

		function get_with_limit($limit, $offset, $order_by)
		{
			$this->load->model('Mdl_formpatientresources');
			$query = $this->Mdl_formpatientresources->get_with_limit($limit, $offset, $order_by);
			return $query;
		}

		function get_where($id)
		{
			$this->load->model('Mdl_formpatientresources');
			$query = $this->Mdl_formpatientresources->get_where($id);
			return $query;
		}

		function get_where_custom($col, $value)
		{
			$this->load->model('Mdl_formpatientresources');
			$query = $this->Mdl_formpatientresources->get_where_custom($col, $value);
			return $query;
		}

		function _insert($data)
		{
			$this->load->model('Mdl_formpatientresources');
			$this->Mdl_formpatientresources->_insert($data);
		}

		function _update($id, $data)
		{
			$this->load->model('Mdl_formpatientresources');
			$this->Mdl_formpatientresources->_update($id, $data);
		}

		function _delete($id)
		{
			$this->load->model('Mdl_formpatientresources');
			$this->Mdl_formpatientresources->_delete($id);
		}

		function count_where($column, $value)
		{
			$this->load->model('Mdl_formpatientresources');
			$count = $this->Mdl_formpatientresources->count_where($column, $value);
			return $count;
		}

		function get_max()
		{
			$this->load->model('Mdl_formpatientresources');
			$max_id = $this->Mdl_formpatientresources->get_max();
			return $max_id;
		}

		function _custom_query($mysql_query)
		{
			$this->load->model('Mdl_formpatientresources');
			$query = $this->Mdl_formpatientresources->_custom_query($mysql_query);
			return $query;
		}
	}
