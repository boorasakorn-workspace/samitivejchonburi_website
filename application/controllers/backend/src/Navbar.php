<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Navbar extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Mdl_site_security');
		$this->Mdl_site_security->_make_sure_is_admin();

		$this->load->library('session');

		$this->load->model('Back_Model');
	}

	function view_menu_details($update_id)
	{
		if(!is_numeric($update_id)){
			redirect('backend/src/Navbar/manage');
		}
		redirect('backend/MainBackend/Preview_Page');
	}

	/////////////////////////        Navigation      /////////////////////////

	function _process_delete($update_id)
	{
		$this->_delete($update_id);
	}

	//////// Delete Menu Process from controller backend/src/Navbar/delete_menu
	function delete_menu_process($update_id)
	{
		if(!is_numeric($update_id)){
			redirect('backend/src/Navbar/manage');
		}

		$this->load->library('session');

		$submit = $this->input->post('submit', TRUE);

		if($submit=="Cancel"){
			redirect('backend/src/Navbar/manage/'.$update_id);
		}elseif($submit=="Yes - Delete Menu"){
			$this->_process_delete($update_id);

			$flash_msg = "Deleted Navbar was Successfully";
			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
			$this->session->set_flashdata('item', $value);
			redirect('backend/src/Navbar/manage');
		}


	}

	//////// Delete Menu Record
	function delete_menu($update_id)
	{
		if(!is_numeric($update_id)){
			redirect('backend/src/Navbar/manage');
		}

		$this->load->library('session');

		$data['headline'] = "Delete Menu";
		$data['update_id'] = $update_id;
		
		
		$data['content'] = 'backend/Navbar/delete_navbar';
		
		$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
	}

	function _count_sub_nav()
	{
		$mysql_query = "select * from tb_navigation inner join tb_subnavigation on tb_navigation.id=tb_subnavigation.parent_nav_id where tb_subnavigation.sub_nav_status=1 and tb_navigation.id=tb_subnavigation.parent_nav_id";
		$query = $this->_custom_query($mysql_query);
		$num_rows = $query->num_rows();
		return $num_rows;
	}

	function _get_nav_title($update_id)
	{
		$data = $this->fetch_data_from_db($update_id);
		$nav_title = $data['nav_title'];
		$nav_title_en = $data['nav_title_en'];
		return $nav_title;
	}

	//////// Add & Update Navbar
	function create()
	{
		$this->load->library('session');

		$update_id = $this->uri->segment(5);
		$submit = $this->input->post('submit', TRUE);

		if($submit == "Cancel"){
			redirect('backend/src/Navbar/manage');
		}

		// ถ้า $submit = value->Submit
		if($submit == "Submit"){
				// Process the Form at create.php
				$this->load->library('form_validation'); // Load Library From Validation

				$this->form_validation->set_rules('nav_title', '<b>โปรดกรอกเฉพาะชื่อ Title</b>', 'required|max_length[240]');
				$this->form_validation->set_rules('nav_status', '<b>โปรดเลือกการเผยแพร่</b>', 'required|numeric');

		    	if($this->form_validation->run() == TRUE){ // ถ้า form_validation ทำงานแล้วถูกต้อง
		    		// Get The Variables
		    		$data = $this->fetch_data_from_post(); // ให้ $data = รับค่าจาก From method="POST"
		    		
		    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id

		    			// Update Items Details
		    			$this->_update($update_id, $data); // Update ลง Database
		    			$flash_msg = "Update Menu were Successfully"; // Alert message
		    			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
						$this->session->set_flashdata('item', $value); // สร้าง session alert
						redirect('backend/src/Navbar/create/'.$update_id);
					}else{
		    			// Insert a New Items
						$this->_insert($data);
		    			$update_id = $this->get_max(); // Get the ID at new item

		    			$flash_msg = "Insert Menu were Successfully";
		    			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
		    			$this->session->set_flashdata('item', $value);
		    			redirect('backend/src/Navbar/create/'.$update_id);
		    		}
		    	}
		    }

		    if((is_numeric($update_id)) && ($submit!="Submit")){
		    	$data = $this->fetch_data_from_db($update_id);
		    }else{
		    	$data = $this->fetch_data_from_post();
		    }

		    if(!is_numeric($update_id)){
		    	$data['headline'] = "เพิ่มข้อมูล Menu";
		    }else{
		    	$data['headline'] = "แก้ไขข้อมูล Menu";
		    }

		    $data['options'] = $this->_get_dropdown_options($update_id);
		    $data['num_dropdown_options'] = count($data['options']);
		    $data['update_id'] = $update_id;
		    

			$this->load->model('Mdl_alltable');
			$data['content_page'] = $this->Mdl_alltable->get_all_content_page();
   
		    $data['content'] = 'backend/navbar/formNavbar';		    

			$data['replace4'] = 'backend/src/ck/replace4';

		    $data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

		//////// Navbar Detail
		function manage()
		{			
			
			$data['query'] = $this->get('nav_title');

			$data['subnav'] = $this;
			
			$data['content'] = 'backend/navbar/manageNavbar';

			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

		//////// Fetch Data at method POST
		function fetch_data_from_post() // Fetch ข้อมูลจาก Input POST
		{
			$data['nav_title'] = $this->input->post('nav_title', TRUE);
			$data['nav_title_en'] = $this->input->post('nav_title_en', TRUE);
			if($this->input->post('nav_url_content', TRUE) != ''){
				$data['nav_url'] = $this->input->post('nav_url_content', TRUE);
				$data['nav_content'] = '';
			}
			elseif($this->input->post('nav_url', TRUE) != ''){
				$data['nav_url'] = $this->input->post('nav_url', TRUE);
				$data['nav_content'] = '';
			}
			else{
				$data['nav_content'] = html_escape($this->input->post('nav_content', FALSE));
			}
			$data['nav_status'] = $this->input->post('nav_status', TRUE);
			$data['order_priority'] = $this->input->post('order_priority', TRUE);

			return $data;
		}

		//////// Fetch Data at Database
		function fetch_data_from_db($update_id) //  Fetch ข้อมูลจาก Database
		{

			if(!is_numeric($update_id)){
				redirect('backend/src/Navbar/manage');
			}

			$query = $this->get_where($update_id);
			foreach($query->result() as $row){

				$data['nav_title'] = $row->nav_title;
				$data['nav_title_en'] = $row->nav_title_en;
				$data['nav_url'] = $row->nav_url;
				$data['nav_url_content'] = $row->nav_url;
				$data['nav_content'] = $row->nav_content;
				$data['nav_status'] = $row->nav_status;
				$data['order_priority'] = $row->order_priority;
				$data['created_at'] = $row->created_at;
				$data['updated_at'] = $row->edited_at;
			}

			if(!isset($data)){
				$data = "";
			}

			return $data;
		}

		/////////////////////////        End Navigation      /////////////////////////

		/////////////////////////        Sub Navigation      /////////////////////////

		function _process_delete_subnav($update_id)
		{
			$this->_delete_subnav($update_id);
		}

		//////// Delete Menu Process from controller backend/src/Navbar/delete_menu
		function delete_menu_subnav_process($update_id)
		{
			
			if(!is_numeric($update_id)){
				redirect('backend/src/Navbar/manage_subnav');
			}

			$this->load->library('session');

			$submit = $this->input->post('submit', TRUE);

			if($submit=="Cancel"){
				redirect('backend/src/Navbar/manage_subnav/'.$update_id);
			}elseif($submit=="Yes - Delete Sub Menu"){
				$this->_process_delete_subnav($update_id);

				$flash_msg = "Deleted sub menu was Successfully";
				$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
				$this->session->set_flashdata('item', $value);

				redirect('backend/src/Navbar/manage_subnav');
			}

		}

		//////// Function แสดงหมวดหมู่หากมีหมวดหมู่อยู่แล้ว (Dropdown Parent Category)
		function _get_dropdown_options($update_id)
		{
			if(!is_numeric($update_id)){
				$update_id = 0;
			}

			$options[''] = "เลือกหมวดหมู่เมนู";

			$mysql_query = "select * from tb_navigation";
			$query = $this->_custom_query($mysql_query);
			if($query->num_rows() > 0){
				foreach($query->result() as $row){
					$options[$row->id] = $row->nav_title;
				}
			}

			return $options;
		}

		//////// Delete Menu Record
		function delete_menu_subnav($update_id)
		{

			if(!is_numeric($update_id)){
				redirect('backend/src/Navbar/manage_subnav');
			}

			$this->load->library('session');

			$data['headline'] = "Delete Sub Menu";
			$data['update_id'] = $update_id;
			
			
			$data['content'] = 'backend/Navbar/delete_subnavbar';
			
			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}		

		function _count_sub_of_sub_nav()
		{
			$mysql_query = "select * from tb_subnavigation
			inner join tb_subofsubnavigation
			on tb_subnavigation.id=tb_subofsubnavigation.parent_nav_id
			where tb_subofsubnavigation.sub_navof_sub_nav_status=1
			and
			tb_subnavigation.id=tb_subofsubnavigation.parent_nav_id";
			$query = $this->_custom_query($mysql_query);
			$num_rows = $query->num_rows();
			return $num_rows;
		}

		function _get_subnav_title($update_id)
		{
			$data = $this->fetch_from_db_subnav($update_id);
			$sub_nav_title = $data['sub_nav_title'];
			$sub_nav_title_en = $data['sub_nav_title_en'];
			return $sub_nav_title;
		}

		//////// Add & Update Navbar
		function create_subnav()
		{
			$this->load->library('session');

			$update_id = $this->uri->segment(5);
			$submit = $this->input->post('submit', TRUE);
			if($submit == "Cancel"){
				redirect('backend/src/Navbar/manage_subnav/'.$this->input->post('parent', TRUE));
			}

			// ถ้า $submit = value->Submit
			if($submit == "Submit"){

				// Process the Form at create.php
				$this->load->library('form_validation'); // Load Library From Validation

				$this->form_validation->set_rules('sub_nav_title', '<b>โปรดกรอกเฉพาะชื่อเมนูย่อย</b>', 'required|max_length[240]');
				$this->form_validation->set_rules('sub_nav_status', '<b>โปรดเลือกการเผยแพร่</b>', 'required|numeric');

		    	if($this->form_validation->run() == TRUE){ // ถ้า form_validation ทำงานแล้วถูกต้อง
		    		// Get The Variables
		    		$data = $this->fetch_from_post_subnav(); // ให้ $data = รับค่าจาก From method="POST"
		    		
		    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id

		    			// Update Items Details
		    			$this->_update_subnav($update_id, $data); // Update ลง Database
		    			$flash_msg = "Update Sub Menu were Successfully"; // Alert message
		    			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
						$this->session->set_flashdata('item', $value); // สร้าง session alert
						redirect('backend/src/Navbar/create_subnav/'.$update_id);
					}else{
		    			// Insert a New Items
						$this->_insert_subnav($data);
		    			$update_id = $this->get_max_subnav(); // Get the ID at new item

		    			$flash_msg = "Insert Sub Menu were Successfully";
		    			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
		    			$this->session->set_flashdata('item', $value);
		    			redirect('backend/src/Navbar/create_subnav/'.$update_id);
		    		}
		    	}
		    }

		    if((is_numeric($update_id)) && ($submit!="Submit")){
		    	$data = $this->fetch_from_db_subnav($update_id);
		    }else{
		    	$data = $this->fetch_from_post_subnav();
		    }

		    if(!is_numeric($update_id)){
		    	$data['headline'] = "เพิ่มข้อมูล Sub Menu";
		    }else{
		    	$data['headline'] = "แก้ไขข้อมูล Sub Menu";
		    }

		    $data['update_id'] = $update_id;
		    

		    $data['options'] = $this->_get_dropdown_options($update_id);
		    
		    $data['content'] = 'backend/navbar/formSubNavbar';
	      	
			$this->load->model('Mdl_alltable');
			$data['content_page'] = $this->Mdl_alltable->get_all_content_page();
			
			$data['replace4'] = 'backend/src/ck/replace4';
			
		    $data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

		//////// Sub Navbar Detail
		function manage_subnav($update_id = NULL, $submit = NULL)
		{
			
			$this->load->model('Mdl_formnavbar');
			if(is_numeric($update_id)){
				
				$query = $this->get_where_custom_subnav('parent_nav_id',$update_id);

				$data['query'] = $query;
				$data['update_id'] = $update_id;
				$data['subofsubnav'] = $this;
			}else
			if($submit=="Cancel" && (is_numeric($update_id))){

				
				$mysql_query = "select * from tb_navigation where id=$update_id";
				$data['query'] = $this->_custom_query($mysql_query);
				$data['subofsubnav'] = $this;
			}else if(!is_numeric($update_id)){

				
				$data['query'] = $this->get_subnav('sub_nav_title');
				$data['subofsubnav'] = $this;
			}
			
			$data['content'] = 'backend/navbar/manageSubNavbar';

			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

		//////// Fetch Data at method POST
		function fetch_from_post_subnav() // Fetch ข้อมูลจาก Input POST
		{
			$data['sub_nav_title'] = $this->input->post('sub_nav_title', TRUE);
			$data['sub_nav_title_en'] = $this->input->post('sub_nav_title_en', TRUE);
			if($this->input->post('sub_nav_url_content', TRUE) != ''){
				$data['sub_nav_url'] = $this->input->post('sub_nav_url_content', TRUE);
				$data['sub_nav_content'] = '';
			}
			elseif($this->input->post('sub_nav_url', TRUE) != ''){
				$data['sub_nav_url'] = $this->input->post('sub_nav_url', TRUE);
				$data['sub_nav_content'] = '';
			}
			else{
				$data['sub_nav_content'] = html_escape($this->input->post('sub_nav_content', FALSE));
			}
			$data['sub_nav_status'] = $this->input->post('sub_nav_status', TRUE);
			$data['parent_nav_id'] = $this->input->post('parent_nav_id', TRUE);
			$data['order_priority'] = $this->input->post('order_priority', TRUE);

			return $data;
		}

		//////// Fetch Data at Database
		function fetch_from_db_subnav($update_id) //  Fetch ข้อมูลจาก Database
		{
			if(!is_numeric($update_id)){
				redirect('backend/src/Navbar/manage_subnav');
			}

			$query = $this->get_where_subnav($update_id);
			foreach($query->result() as $row){
				$data['sub_nav_title'] = $row->sub_nav_title;
				$data['sub_nav_title_en'] = $row->sub_nav_title_en;
				$data['sub_nav_url'] = $row->sub_nav_url;
				$data['sub_nav_url_content'] = $row->sub_nav_url;
				$data['sub_nav_content'] = $row->sub_nav_content;
				$data['sub_nav_status'] = $row->sub_nav_status;
				$data['parent_nav_id'] = $row->parent_nav_id;
				$data['order_priority'] = $row->order_priority;
				$data['created_at'] = $row->created_at;
				$data['updated_at'] = $row->edited_at;
			}

			if(!isset($data)){
				$data = "";
			}

			return $data;
		}
		/////////////////////////      End Sub Navigation      /////////////////////////

		function get_subnav($order_by)
		{
			$this->load->model('Mdl_formnavbar');
			$query = $this->Mdl_formnavbar->get_subnav($order_by);
			return $query;
		}

		function get($order_by)
		{
			$this->load->model('Mdl_formnavbar');
			$query = $this->Mdl_formnavbar->get($order_by);
			return $query;
		}

		function get_with_limit($limit, $offset, $order_by)
		{
			$this->load->model('Mdl_formnavbar');
			$query = $this->Mdl_formnavbar->get_with_limit($limit, $offset, $order_by);
			return $query;
		}

		function get_where_subnav($id)
		{
			$this->load->model('Mdl_formnavbar');
			$query = $this->Mdl_formnavbar->get_where_subnav($id);
			return $query;
		}

		function get_where($id)
		{
			$this->load->model('Mdl_formnavbar');
			$query = $this->Mdl_formnavbar->get_where($id);
			return $query;
		}

		function get_where_custom_subnav($col, $value)
		{
			$this->load->model('Mdl_formnavbar');
			$query = $this->Mdl_formnavbar->get_where_custom_subnav($col, $value);
			return $query;
		}

		function get_where_custom($col, $value)
		{
			$this->load->model('Mdl_formnavbar');
			$query = $this->Mdl_formnavbar->get_where_custom($col, $value);
			return $query;
		}

		function _insert_subnav($data)
		{
			$this->load->model('Mdl_formnavbar');
			$this->Mdl_formnavbar->_insert_subnav($data);
		}

		function _insert($data)
		{
			$this->load->model('Mdl_formnavbar');
			$this->Mdl_formnavbar->_insert($data);
		}

		function _update_subnav($id, $data)
		{
			$this->load->model('Mdl_formnavbar');
			$this->Mdl_formnavbar->_update_subnav($id, $data);
		}

		function _update($id, $data)
		{
			$this->load->model('Mdl_formnavbar');
			$this->Mdl_formnavbar->_update($id, $data);
		}

		function _delete_subnav($id)
		{
			$this->load->model('Mdl_formnavbar');
			$this->Mdl_formnavbar->_delete_subnav($id);
		}

		function _delete($id)
		{
			$this->load->model('Mdl_formnavbar');
			$this->Mdl_formnavbar->_delete($id);
		}

		function count_where($column, $value)
		{
			$this->load->model('Mdl_formnavbar');
			$count = $this->Mdl_formnavbar->count_where($column, $value);
			return $count;
		}

		function get_max_subnav()
		{
			$this->load->model('Mdl_formnavbar');
			$max_id = $this->Mdl_formnavbar->get_max_subnav();
			return $max_id;
		}

		function get_max()
		{
			$this->load->model('Mdl_formnavbar');
			$max_id = $this->Mdl_formnavbar->get_max();
			return $max_id;
		}

		function _custom_query($mysql_query)
		{
			$this->load->model('Mdl_formnavbar');
			$query = $this->Mdl_formnavbar->_custom_query($mysql_query);
			return $query;
		}
	}
