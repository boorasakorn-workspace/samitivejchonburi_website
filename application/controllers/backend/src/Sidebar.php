<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sidebar extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->model('Mdl_site_security');
		$this->Mdl_site_security->_make_sure_is_admin();

		$this->load->library('session');

		$this->load->model('Back_Model');
	}

	function view_menu_details($update_id)
	{
		redirect('backend/MainBackend/Preview_Page');
	}

	/////////////////////////        Navigation      /////////////////////////

	function _process_delete($update_id)
	{
		$this->_delete($update_id);
	}

	//////// Delete Menu Process from controller backend/src/Sidebar/delete_menu
	function delete_menu_process($update_id)
	{
		if(!is_numeric($update_id)){
			redirect('backend/src/Sidebar/manage');
		}

		$this->load->library('session');

		$submit = $this->input->post('submit', TRUE);

		if($submit=="Cancel"){
			redirect('backend/src/Sidebar/manage/'.$update_id);
		}elseif($submit=="Yes - Delete Menu"){
			$this->_process_delete($update_id);

			$flash_msg = "Deleted Navbar was Successfully";
			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
			$this->session->set_flashdata('item', $value);

			redirect('backend/src/Sidebar/manage');
		}

	}

	//////// Delete Menu Record
	function delete_menu($update_id)
	{
		if(!is_numeric($update_id)){
			redirect('backend/src/Sidebar/manage');
		}

		$this->load->library('session');

		$data['headline'] = "Delete Menu";
		$data['update_id'] = $update_id;
		
		$data['content'] = 'backend/sidemenu/delete_navbar';

		$data = $this->Back_Model->General($data);
		$this->load->view('backend/dashboard', $data);
	}

	//////// Add & Update Navbar
	function create()
	{

		$this->load->library('session');

		$update_id = $this->uri->segment(5);
		$submit = $this->input->post('submit', TRUE);

		if($submit == "Cancel"){
			redirect('backend/src/Sidebar/manage');
		}

		// ถ้า $submit = value->Submit
		if($submit == "Submit"){
				// Process the Form at create.php
				$this->load->library('form_validation'); // Load Library From Validation

				$this->form_validation->set_rules('sidemenu_title', '<b>โปรดกรอกเฉพาะชื่อ Title</b>', 'required|max_length[240]');
				$this->form_validation->set_rules('sidemenu_status', '<b>โปรดเลือกการเผยแพร่</b>', 'required|numeric');

		    	if($this->form_validation->run() == TRUE){ // ถ้า form_validation ทำงานแล้วถูกต้อง
		    		// Get The Variables
		    		$data = $this->fetch_data_from_post(); // ให้ $data = รับค่าจาก From method="POST"

		    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id

		    			// Update Items Details
		    			$this->_update($update_id, $data); // Update ลง Database
		    			$flash_msg = "Update Menu were Successfully"; // Alert message
		    			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
						$this->session->set_flashdata('item', $value); // สร้าง session alert
						redirect('backend/src/Sidebar/create/'.$update_id);
					}else{
		    			// Insert a New Items
						$this->_insert($data);
		    			$update_id = $this->get_max(); // Get the ID at new item

		    			$flash_msg = "Insert Menu were Successfully";
		    			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
		    			$this->session->set_flashdata('item', $value);
		    			redirect('backend/src/Sidebar/create/'.$update_id);
		    		}
		    	}
		    }

		    if((is_numeric($update_id)) && ($submit!="Submit")){
		    	$data = $this->fetch_data_from_db($update_id);
		    }else{
		    	$data = $this->fetch_data_from_post();
		    }

		    if(!is_numeric($update_id)){
		    	$data['headline'] = "เพิ่มข้อมูล Menu";
		    }else{
		    	$data['headline'] = "แก้ไขข้อมูล Menu";
		    }

		    $data['options'] = $this->_get_dropdown_options($update_id);
		    $data['num_dropdown_options'] = count($data['options']);
		    $data['update_id'] = $update_id;
		    
			$this->load->model('Mdl_alltable');
			$data['content_page'] = $this->Mdl_alltable->get_all_content_page();

		    $data['content'] = 'backend/sidemenu/formNavbar';
		    
			$data['replace4'] = 'backend/src/ck/replace4';

		    $data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

	function _count_sub_nav()
	{
		$mysql_query = "select * from tb_sidemenu inner join tb_subofsidemenu on tb_sidemenu.id=tb_subofsidemenu.parent_sidemenu_id where tb_subofsidemenu.sub_sidemenu_status=1 and tb_sidemenu.id=tb_subofsidemenu.parent_sidemenu_id";
		$query = $this->_custom_query($mysql_query);
		$num_rows = $query->num_rows();
		return $num_rows;
	}

	function _get_nav_title($update_id)
	{
		$data = $this->fetch_data_from_db($update_id);
		$nav_title = $data['sidemenu_title'];
		$nav_title_en = $data['sidemenu_title_en'];
		return $nav_title;
	}
		//////// Navbar Detail
		function manage()
		{
			
			$data['query'] = $this->get('sidemenu_title');

			$data['subnav'] = $this;
			
			$data['get_header'] = $this->fetch_header();
			$data['get_nav'] = $this->fetch_nav()->result();
			$data['get_subnav'] = $this->fetch_subnav()->result();
			
			$data['get_sidemenu'] = $this->fetch_sidemenu();
        	$data['get_subsidemenu'] = $this->fetch_subsidemenu();
       		$data['get_subofsubmenu'] = $this->fetch_subofsubmenu();			
			
			$data['content'] = 'backend/sidemenu/manageNavbar';

			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}


		//////// Fetch Data at method POST
		function fetch_data_from_post() // Fetch ข้อมูลจาก Input POST
		{
			$data['sidemenu_title'] = $this->input->post('sidemenu_title', TRUE);
			$data['sidemenu_title_en'] = $this->input->post('sidemenu_title_en', TRUE);
			if($this->input->post('sidemenu_url_content', TRUE) != ''){
				$data['sidemenu_url'] = $this->input->post('sidemenu_url_content', TRUE);
				$data['sidemenu_content'] = '';
			}
			elseif($this->input->post('sidemenu_url', TRUE) != ''){
				$data['sidemenu_url'] = $this->input->post('sidemenu_url', TRUE);
				$data['sidemenu_content'] = '';
			}
			else{
				$data['sidemenu_content'] = html_escape($this->input->post('sidemenu_content', FALSE));
			}
			$data['sidemenu_status'] = $this->input->post('sidemenu_status', TRUE);
			$data['order_priority'] = $this->input->post('order_priority', TRUE);

			return $data;
		}

		//////// Fetch Data at Database
		function fetch_data_from_db($update_id) //  Fetch ข้อมูลจาก Database
		{

			if(!is_numeric($update_id)){
				redirect('backend/src/Sidebar/manage');
			}

			$query = $this->get_where($update_id);
			foreach($query->result() as $row){

				$data['sidemenu_title'] = $row->sidemenu_title;
				$data['sidemenu_title_en'] = $row->sidemenu_title_en;
				$data['sidemenu_url'] = $row->sidemenu_url;
				$data['sidemenu_url_content'] = $row->sidemenu_url;
				$data['sidemenu_content'] = $row->sidemenu_content;
				$data['sidemenu_status'] = $row->sidemenu_status;
				$data['order_priority'] = $row->order_priority;
				$data['created_at'] = $row->created_at;
				$data['updated_at'] = $row->edited_at;
			}

			if(!isset($data)){
				$data = "";
			}

			return $data;
		}

		/////////////////////////        End Navigation      /////////////////////////

		/////////////////////////        Sub Navigation      /////////////////////////

		function _process_delete_subnav($update_id)
		{
			$this->_delete_subnav($update_id);
		}

		//////// Delete Menu Process from controller backend/src/Sidebar/delete_menu
		function delete_menu_subnav_process($update_id)
		{
			if(!is_numeric($update_id)){
				redirect('backend/src/Sidebar/manage_subnav');
			}

			$this->load->library('session');

			$submit = $this->input->post('submit', TRUE);

			if($submit=="Cancel"){
				redirect('backend/src/Sidebar/manage_subnav/'.$update_id);
			}elseif($submit=="Yes - Delete Sub Menu"){
				$this->_process_delete_subnav($update_id);

				$flash_msg = "Deleted sub menu was Successfully";
				$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
				$this->session->set_flashdata('item', $value);

				redirect('backend/src/Sidebar/manage_subnav');
			}

		}

		//////// Function แสดงหมวดหมู่หากมีหมวดหมู่อยู่แล้ว (Dropdown Parent Category)
		function _get_dropdown_options($update_id)
		{
			if(!is_numeric($update_id)){
				$update_id = 0;
			}

			$options[''] = "เลือกหมวดหมู่เมนู";

			$mysql_query = "select * from tb_sidemenu";
			$query = $this->_custom_query($mysql_query);
			if($query->num_rows() > 0){
				foreach($query->result() as $row){
					$options[$row->id] = $row->sidemenu_title;
				}
			}

			return $options;
		}

		//////// Delete Menu Record
		function delete_menu_subnav($update_id)
		{
			if(!is_numeric($update_id)){
				redirect('backend/src/Sidebar/manage_subnav');
			}

			$this->load->library('session');

			$data['headline'] = "Delete Sub Menu";
			$data['update_id'] = $update_id;

			$data['content'] = 'backend/sidemenu/delete_subnavbar';

			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

		function _count_sub_of_sub_nav()
		{
			$mysql_query = "select * from tb_subofsidemenu
			inner join tb_subofsubmenu
			on tb_subofsidemenu.id=tb_subofsubmenu.parent_submenu_id
			where tb_subofsubmenu.subofsubmenu_status=1
			and
			tb_subofsidemenu.id=tb_subofsubmenu.parent_submenu_id";
			$query = $this->_custom_query($mysql_query);
			$num_rows = $query->num_rows();
			return $num_rows;
		}

		function _get_subnav_title($update_id)
		{
			$data = $this->fetch_from_db_subnav($update_id);
			$sub_nav_title = $data['sub_sidemenu_title'];
			$sub_nav_title_en = $data['sub_sidemenu_title_en'];
			return $sub_nav_title;
		}
		
		//////// Add & Update Navbar
		function create_subnav()
		{

			$this->load->library('session');

			$update_id = $this->uri->segment(5);
			$submit = $this->input->post('submit', TRUE);

			if($submit == "Cancel"){
				redirect('backend/src/Sidebar/manage_subnav/'.$this->input->post('parent', TRUE));
			}

			// ถ้า $submit = value->Submit
			if($submit == "Submit"){
				// Process the Form at create.php
				$this->load->library('form_validation'); // Load Library From Validation

				$this->form_validation->set_rules('sub_sidemenu_title', '<b>โปรดกรอกเฉพาะชื่อเมนูย่อย</b>', 'required|max_length[240]');
				$this->form_validation->set_rules('sub_sidemenu_status', '<b>โปรดเลือกการเผยแพร่</b>', 'required|numeric');

		    	if($this->form_validation->run() == TRUE){ // ถ้า form_validation ทำงานแล้วถูกต้อง
		    		// Get The Variables
		    		$data = $this->fetch_from_post_subnav(); // ให้ $data = รับค่าจาก From method="POST"

		    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id

		    			// Update Items Details
		    			$this->_update_subnav($update_id, $data); // Update ลง Database
		    			$flash_msg = "Update Sub Menu were Successfully"; // Alert message
		    			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
						$this->session->set_flashdata('item', $value); // สร้าง session alert
						redirect('backend/src/Sidebar/create_subnav/'.$update_id);
					}else{
		    			// Insert a New Items
						$this->_insert_subnav($data);
		    			$update_id = $this->get_max_subnav(); // Get the ID at new item

		    			$flash_msg = "Insert Sub Menu were Successfully";
		    			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
		    			$this->session->set_flashdata('item', $value);
		    			redirect('backend/src/Sidebar/create_subnav/'.$update_id);
		    		}
		    	}
		    }

		    if((is_numeric($update_id)) && ($submit!="Submit")){
		    	$data = $this->fetch_from_db_subnav($update_id);
		    }else{
		    	$data = $this->fetch_from_post_subnav();
		    }

		    if(!is_numeric($update_id)){
		    	$data['headline'] = "เพิ่มข้อมูล Sub Menu";
		    }else{
		    	$data['headline'] = "แก้ไขข้อมูล Sub Menu";
		    }

		    $data['update_id'] = $update_id;		    

		    $data['options'] = $this->_get_dropdown_options($update_id);    
		    
		    $data['content'] = 'backend/sidemenu/formSubNavbar';	      	
	      	
			$this->load->model('Mdl_alltable');
			$data['content_page'] = $this->Mdl_alltable->get_all_content_page();
			
			$data['replace4'] = 'backend/src/ck/replace4';

		    $data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

		//////// Sub Navbar Detail
		function manage_subnav($update_id = NULL, $submit = NULL)
		{
			$this->load->model('Mdl_formsidebar');
			if(is_numeric($update_id)){
				
				$query = $this->get_where_custom_subnav('parent_sidemenu_id',$update_id);
				$data['query'] = $query;
				$data['update_id'] = $update_id;
				$data['subofsubnav'] = $this;
			}else
			if($submit=="Cancel" && (is_numeric($update_id))){

				
				$mysql_query = "select * from tb_sidemenu where id=$update_id";
				$data['query'] = $this->_custom_query($mysql_query);
				$data['subofsubnav'] = $this;
			}else if(!is_numeric($update_id)){

				
				$data['query'] = $this->get_subnav('sub_sidemenu_title');
				$data['subofsubnav'] = $this;
			}

			$data['content'] = 'backend/sidemenu/manageSubNavbar';
			
			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

		//////// Fetch Data at method POST
		function fetch_from_post_subnav() // Fetch ข้อมูลจาก Input POST
		{
			$data['sub_sidemenu_title'] = $this->input->post('sub_sidemenu_title', TRUE);
			$data['sub_sidemenu_title_en'] = $this->input->post('sub_sidemenu_title_en', TRUE);
			if($this->input->post('sub_sidemenu_url_content', TRUE) != ''){
				$data['sub_sidemenu_url'] = $this->input->post('sub_sidemenu_url_content', TRUE);
				$data['sub_sidemenu_content'] = '';
			}
			elseif($this->input->post('sub_sidemenu_url', TRUE) != ''){
				$data['sub_sidemenu_url'] = $this->input->post('sub_sidemenu_url', TRUE);
				$data['sub_sidemenu_content'] = '';
			}
			else{
				$data['sub_sidemenu_content'] = html_escape($this->input->post('sub_sidemenu_content', FALSE));
			}
			$data['sub_sidemenu_status'] = $this->input->post('sub_sidemenu_status', TRUE);
			$data['parent_sidemenu_id'] = $this->input->post('parent_sidemenu_id', TRUE);
			$data['order_priority'] = $this->input->post('order_priority', TRUE);

			return $data;
		}

		//////// Fetch Data at Database
		function fetch_from_db_subnav($update_id) //  Fetch ข้อมูลจาก Database
		{
			if(!is_numeric($update_id)){
				redirect('backend/src/Sidebar/manage_subnav');
			}

			$query = $this->get_where_subnav($update_id);
			foreach($query->result() as $row){

				$data['sub_sidemenu_title'] = $row->sub_sidemenu_title;
				$data['sub_sidemenu_title_en'] = $row->sub_sidemenu_title_en;
				$data['sub_sidemenu_url'] = $row->sub_sidemenu_url;
				$data['sub_sidemenu_url_content'] = $row->sub_sidemenu_url;
				$data['sub_sidemenu_content'] = $row->sub_sidemenu_content;
				$data['sub_sidemenu_status'] = $row->sub_sidemenu_status;
				$data['parent_sidemenu_id'] = $row->parent_sidemenu_id;
				$data['order_priority'] = $row->order_priority;
				$data['created_at'] = $row->created_at;
				$data['updated_at'] = $row->edited_at;
			}

			if(!isset($data)){
				$data = "";
			}

			return $data;
		}
		/////////////////////////      End Sub Navigation      /////////////////////////

		/////////////////////////        SubSub Navigation      /////////////////////////

		function _process_delete_subsubnav($update_id)
		{
			$this->_delete_subsubnav($update_id);

		}

		//////// Delete Menu Process from controller backend/src/Sidebar/delete_menu
		function delete_menu_subsubnav_process($update_id)
		{
			if(!is_numeric($update_id)){
				redirect('backend/src/Sidebar/manage_subsubnav');
			}

			$this->load->library('session');

			$submit = $this->input->post('submit', TRUE);

			if($submit=="Cancel"){
				redirect('backend/src/Sidebar/manage_subsubnav/'.$update_id);
			}elseif($submit=="Yes - Delete Sub Menu"){
				$this->_process_delete_subsubnav($update_id);

				$flash_msg = "Deleted sub menu was Successfully";
				$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
				$this->session->set_flashdata('item', $value);

				redirect('backend/src/Sidebar/manage_subsubnav');
			}
		}

		//////// Function แสดงหมวดหมู่หากมีหมวดหมู่อยู่แล้ว (Dropdown Parent Category)
		function _subget_dropdown_options($update_id)
		{

			if(!is_numeric($update_id)){
				$update_id = 0;
			}

			$options[''] = "เลือกหมวดหมู่เมนู";

			$mysql_query = "select * from tb_subofsidemenu";
			$query = $this->_custom_query($mysql_query);
			if($query->num_rows() > 0){
				foreach($query->result() as $row){
					$options[$row->id] = $row->sub_sidemenu_title;
				}
			}

			return $options;
		}

		//////// Delete Menu Record
		function delete_menu_subsubnav($update_id)
		{

			if(!is_numeric($update_id)){
				redirect('backend/src/Sidebar/manage_subsubnav');
			}

			$this->load->library('session');

			$data['headline'] = "Delete Sub of Sub Menu";
			$data['update_id'] = $update_id;
			
			$data['content'] = 'backend/sidemenu/delete_subsubnavbar';

			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

		function _get_subsubnav_title($update_id)
		{
			$data = $this->fetch_from_db_subsubnav($update_id);
			$subofsubmenu_title = $data['subofsubmenu_title'];
			$subofsubmenu_title_en = $data['subofsubmenu_title_en'];
			return $sub_nav_title;
		}

		//////// Add & Update Navbar
		function create_subsubnav()
		{
			$this->load->library('session');

			$update_id = $this->uri->segment(5);
			$submit = $this->input->post('submit', TRUE);

			if($submit == "Cancel"){
				redirect('backend/src/Sidebar/manage_subsubnav/'.$this->input->post('parent', TRUE));
			}

			// ถ้า $submit = value->Submit
			if($submit == "Submit"){
				// Process the Form at create.php
				$this->load->library('form_validation'); // Load Library From Validation

				$this->form_validation->set_rules('subofsubmenu_title', '<b>โปรดกรอกเฉพาะชื่อเมนูย่อย</b>', 'required|max_length[240]');
				$this->form_validation->set_rules('subofsubmenu_status', '<b>โปรดเลือกการเผยแพร่</b>', 'required|numeric');

		    	if($this->form_validation->run() == TRUE){ // ถ้า form_validation ทำงานแล้วถูกต้อง
		    		// Get The Variables
		    		$data = $this->fetch_from_post_subsubnav(); // ให้ $data = รับค่าจาก From method="POST"
		    		
		    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id

		    			// Update Items Details
		    			$this->_update_subsubnav($update_id, $data); // Update ลง Database
		    			$flash_msg = "Update Sub Menu were Successfully"; // Alert message
		    			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
						$this->session->set_flashdata('item', $value); // สร้าง session alert
						redirect('backend/src/Sidebar/create_subsubnav/'.$update_id);
					}else{
		    			// Insert a New Items
						$this->_insert_subsubnav($data);
		    			$update_id = $this->get_max_subnav(); // Get the ID at new item

		    			$flash_msg = "Insert Sub Menu were Successfully";
		    			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
		    			$this->session->set_flashdata('item', $value);
		    			redirect('backend/src/Sidebar/create_subsubnav/'.$update_id);
		    		}
		    	}
		    }

		    if((is_numeric($update_id)) && ($submit!="Submit")){
		    	$data = $this->fetch_from_db_subsubnav($update_id);
		    }else{
		    	$data = $this->fetch_from_post_subsubnav();
		    }

		    if(!is_numeric($update_id)){
		    	$data['headline'] = "เพิ่มข้อมูล Sub Menu";
		    }else{
		    	$data['headline'] = "แก้ไขข้อมูล Sub Menu";
		    }

		    $data['update_id'] = $update_id;		    

		    $data['options'] = $this->_subget_dropdown_options($update_id);
		    
		    $data['content'] = 'backend/sidemenu/formSubSubNavbar';
	      	
			$this->load->model('Mdl_alltable');
			$data['content_page'] = $this->Mdl_alltable->get_all_content_page();
			
			$data['replace4'] = 'backend/src/ck/replace4';

		    $data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}

		//////// Sub Navbar Detail
		function manage_subsubnav($update_id = NULL, $submit = NULL)
		{

			$this->load->model('Mdl_formsidebar');
			if(is_numeric($update_id)){
				
				$query = $this->get_where_custom_subsubnav('parent_submenu_id',$update_id);
				$data['query'] = $query;
				$data['update_id'] = $update_id;				
				$data['subofsubnav'] = $this;
			}else
			if($submit=="Cancel" && (is_numeric($update_id))){

				
				$mysql_query = "select * from tb_subofsidemenu where id=$update_id";
				$data['query'] = $this->_custom_query($mysql_query);				
				$data['subofsubnav'] = $this;
			}else if(!is_numeric($update_id)){

				
				$data['query'] = $this->get_subsubnav('subofsubmenu_title');				
				$data['subofsubnav'] = $this;
			}			
			
			$data['content'] = 'backend/sidemenu/manageSubSubNavbar';
			
			$data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
		}
		//////// Fetch Data at method POST
		function fetch_from_post_subsubnav() // Fetch ข้อมูลจาก Input POST
		{
			$data['subofsubmenu_title'] = $this->input->post('subofsubmenu_title', TRUE);
			$data['subofsubmenu_title_en'] = $this->input->post('subofsubmenu_title_en', TRUE);
			if($this->input->post('subofsubmenu_url_content', TRUE) != ''){
				$data['subofsubmenu_url'] = $this->input->post('subofsubmenu_url_content', TRUE);
				$data['subofsubmenu_content'] = '';
			}
			elseif($this->input->post('subofsubmenu_url', TRUE) != ''){
				$data['subofsubmenu_url'] = $this->input->post('subofsubmenu_url', TRUE);
				$data['subofsubmenu_content'] = '';
			}
			else{
				$data['subofsubmenu_content'] = html_escape($this->input->post('subofsubmenu_content', FALSE));
			}
			$data['subofsubmenu_status'] = $this->input->post('subofsubmenu_status', TRUE);
			$data['parent_submenu_id'] = $this->input->post('parent_submenu_id', TRUE);
			$data['order_priority'] = $this->input->post('order_priority', TRUE);

			return $data;
		}

		//////// Fetch Data at Database
		function fetch_from_db_subsubnav($update_id) //  Fetch ข้อมูลจาก Database
		{
			if(!is_numeric($update_id)){
				redirect('backend/src/Sidebar/manage_subsubnav');
			}

			$query = $this->get_where_subsubnav($update_id);
			foreach($query->result() as $row){

				$data['subofsubmenu_title'] = $row->subofsubmenu_title;
				$data['subofsubmenu_title_en'] = $row->subofsubmenu_title_en;
				$data['subofsubmenu_url'] = $row->subofsubmenu_url;
				$data['subofsubmenu_url_content'] = $row->subofsubmenu_url;
				$data['subofsubmenu_content'] = $row->subofsubmenu_content;
				$data['subofsubmenu_status'] = $row->subofsubmenu_status;
				$data['parent_submenu_id'] = $row->parent_submenu_id;
				$data['order_priority'] = $row->order_priority;
				$data['created_at'] = $row->created_at;
				$data['updated_at'] = $row->edited_at;
			}

			if(!isset($data)){
				$data = "";
			}

			return $data;
		}
		/////////////////////////      End SubSub Navigation      /////////////////////////
		
		function get_subsubnav($order_by)
		{
			$this->load->model('Mdl_formsidebar');
			$query = $this->Mdl_formsidebar->get_subsubnav($order_by);
			return $query;
		}

		function get_subnav($order_by)
		{
			$this->load->model('Mdl_formsidebar');
			$query = $this->Mdl_formsidebar->get_subnav($order_by);
			return $query;
		}

		function get($order_by)
		{
			$this->load->model('Mdl_formsidebar');
			$query = $this->Mdl_formsidebar->get($order_by);
			return $query;
		}

		function get_with_limit($limit, $offset, $order_by)
		{
			$this->load->model('Mdl_formsidebar');
			$query = $this->Mdl_formsidebar->get_with_limit($limit, $offset, $order_by);
			return $query;
		}

		function get_where_subsubnav($id)
		{
			$this->load->model('Mdl_formsidebar');
			$query = $this->Mdl_formsidebar->get_where_subsubnav($id);
			return $query;
		}

		function get_where_subnav($id)
		{
			$this->load->model('Mdl_formsidebar');
			$query = $this->Mdl_formsidebar->get_where_subnav($id);
			return $query;
		}

		function get_where($id)
		{
			$this->load->model('Mdl_formsidebar');
			$query = $this->Mdl_formsidebar->get_where($id);
			return $query;
		}

		function get_where_custom_subsubnav($col, $value)
		{
			$this->load->model('Mdl_formsidebar');
			$query = $this->Mdl_formsidebar->get_where_custom_subsubnav($col, $value);
			return $query;
		}

		function get_where_custom_subnav($col, $value)
		{
			$this->load->model('Mdl_formsidebar');
			$query = $this->Mdl_formsidebar->get_where_custom_subnav($col, $value);
			return $query;
		}

		function get_where_custom($col, $value)
		{
			$this->load->model('Mdl_formsidebar');
			$query = $this->Mdl_formsidebar->get_where_custom($col, $value);
			return $query;
		}

		function _insert_subsubnav($data)
		{
			$this->load->model('Mdl_formsidebar');
			$this->Mdl_formsidebar->_insert_subsubnav($data);
		}

		function _insert_subnav($data)
		{
			$this->load->model('Mdl_formsidebar');
			$this->Mdl_formsidebar->_insert_subnav($data);
		}

		function _insert($data)
		{
			$this->load->model('Mdl_formsidebar');
			$this->Mdl_formsidebar->_insert($data);
		}

		function _update_subsubnav($id, $data)
		{
			$this->load->model('Mdl_formsidebar');
			$this->Mdl_formsidebar->_update_subsubnav($id, $data);
		}

		function _update_subnav($id, $data)
		{
			$this->load->model('Mdl_formsidebar');
			$this->Mdl_formsidebar->_update_subnav($id, $data);
		}

		function _update($id, $data)
		{
			$this->load->model('Mdl_formsidebar');
			$this->Mdl_formsidebar->_update($id, $data);
		}

		function _delete_subsubnav($id)
		{
			$this->load->model('Mdl_formsidebar');
			$this->Mdl_formsidebar->_delete_subsubnav($id);
		}

		function _delete_subnav($id)
		{
			$this->load->model('Mdl_formsidebar');
			$this->Mdl_formsidebar->_delete_subnav($id);
		}

		function _delete($id)
		{
			$this->load->model('Mdl_formsidebar');
			$this->Mdl_formsidebar->_delete($id);
		}

		function count_where($column, $value)
		{
			$this->load->model('Mdl_formsidebar');
			$count = $this->Mdl_formsidebar->count_where($column, $value);
			return $count;
		}

		function get_max_subsubnav()
		{
			$this->load->model('Mdl_formsidebar');
			$max_id = $this->Mdl_formsidebar->get_max_subsubnav();
			return $max_id;
		}

		function get_max_subnav()
		{
			$this->load->model('Mdl_formsidebar');
			$max_id = $this->Mdl_formsidebar->get_max_subnav();
			return $max_id;
		}

		function get_max()
		{
			$this->load->model('Mdl_formsidebar');
			$max_id = $this->Mdl_formsidebar->get_max();
			return $max_id;
		}

		function _custom_query($mysql_query)
		{
			$this->load->model('Mdl_formsidebar');
			$query = $this->Mdl_formsidebar->_custom_query($mysql_query);
			return $query;
		}


		public function fetch_header()
		{
			$mysql_query = "select * from tb_header where header_status=1";
			$query = $this->_custom_query($mysql_query)->row();

			return $query;
		}

		public function fetch_nav(){
			$mysql_query = "select nav_title,nav_url,nav_content,id
			from tb_navigation
			where nav_status=1
			order by order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}

		public function fetch_sidemenu(){
			$mysql_query = "select sidemenu_title,sidemenu_url,sidemenu_content,id
			from tb_sidemenu
			where sidemenu_status=1
			order by order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}

		public function fetch_subsidemenu(){
			$mysql_query = "select parent_sidemenu_id,sub_sidemenu_title,sub_sidemenu_url,sub_sidemenu_content,id
			from tb_subofsidemenu
			where sub_sidemenu_status=1
			order by order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}

		public function fetch_subofsubmenu(){
			$mysql_query = "select parent_submenu_id,subofsubmenu_title,subofsubmenu_url,subofsubmenu_content,id
			from tb_subofsubmenu
			where subofsubmenu_status=1
			order by order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}

		public function fetch_subnav(){
			$mysql_query = "select tb_subnavigation.id as subid,tb_subnavigation.sub_nav_title,tb_subnavigation.sub_nav_url,tb_subnavigation.sub_nav_content,tb_subnavigation.parent_nav_id,
			tb_navigation.id
			from tb_subnavigation
			inner join tb_navigation
			on tb_subnavigation.parent_nav_id=tb_navigation.id
			where tb_subnavigation.sub_nav_status=1
			and tb_subnavigation.parent_nav_id=tb_navigation.id
			order by tb_subnavigation.order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}
	}
