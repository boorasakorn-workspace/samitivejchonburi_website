<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Contents extends CI_Controller {

		function __construct()
		{
			parent::__construct();
		}

		public function fetch_header()
		{
			$mysql_query = "select * from tb_header where header_status=1";
			$query = $this->_custom_query($mysql_query)->row();

			return $query;
		}

		public function fetch_nav(){
			$mysql_query = "select nav_title,nav_url,nav_content,id
			from tb_navigation
			where nav_status=1
			order by order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}

		public function fetch_subnav(){
			$mysql_query = "select tb_subnavigation.id as subid,tb_subnavigation.sub_nav_title,tb_subnavigation.sub_nav_url,tb_subnavigation.sub_nav_content,tb_subnavigation.parent_nav_id,
			tb_navigation.id
			from tb_subnavigation
			inner join tb_navigation
			on tb_subnavigation.parent_nav_id=tb_navigation.id
			where tb_subnavigation.sub_nav_status=1
			and tb_subnavigation.parent_nav_id=tb_navigation.id
			order by tb_subnavigation.order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}

		public function fetch_sidemenu(){
			$mysql_query = "select sidemenu_title,sidemenu_url,sidemenu_content,id
			from tb_sidemenu
			where sidemenu_status=1
			order by order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}

		public function fetch_subsidemenu(){
			$mysql_query = "select parent_sidemenu_id,sub_sidemenu_title,sub_sidemenu_url,sub_sidemenu_content,id
			from tb_subofsidemenu
			where sub_sidemenu_status=1
			order by order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}

		public function fetch_subofsubmenu(){
			$mysql_query = "select parent_submenu_id,subofsubmenu_title,subofsubmenu_url,subofsubmenu_content,id
			from tb_subofsubmenu
			where subofsubmenu_status=1
			order by order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}

		function view_contents_details($update_id)
		{
			$this->load->model('Mdl_site_security');
			$this->Mdl_site_security->_make_sure_is_admin();

			if(!is_numeric($update_id)){
				redirect('backend/src/Contents/manage');
			}


			// Fetch the Items Details By ID
			$data = $this->fetch_data_from_db($update_id);
			$data['update_id'] = $update_id;
			$data['flash'] = $this->session->flashdata('item');

			$mysql_query = "select * from tb_contents where id='$update_id'";
			$data['query'] = $this->_custom_query($mysql_query)->row();
			
			$data['get_header'] = $this->fetch_header();
			$data['get_nav'] = $this->fetch_nav()->result();
			$data['get_subnav'] = $this->fetch_subnav()->result();

			$data['head_navbar'] = 'frontend/src/head_navbar';
			$data['contentsDetails'] = 'frontend/src/pagecontents/pagecontents_details';
			$data['footerpages'] = 'frontend/src/footerpages';

			$query = $this->get_where($update_id);
			foreach($query->result() as $row){
				$data['meta_author'] = $row->meta_author;
				$data['meta_keywords'] = $row->meta_keywords;
				$data['meta_description'] = $row->meta_description;
			}

			$this->load->view('frontend/homepage', $data);

		}

		//////// Process Delete All 1.Colours 2.Sizes 3.Big Image 4.Small Image 5.Data Record
		function _process_delete($update_id)
		{

			$this->load->model('Mdl_site_security');
			$this->Mdl_site_security->_make_sure_is_admin();

			// Delete Contents Logo Big & Small Image
			$data = $this->fetch_data_from_db($update_id);
			$big_imgs = $data['contents_big_img'];
			$small_imgs = $data['contents_small_img'];

			$big_imgs_path = './gallery/pagecontents/big_imgs/'.$big_imgs;
			$small_imgs_path = './gallery/pagecontents/small_imgs/'.$small_imgs;

				// Remove the images
				if(file_exists($big_imgs_path)){
					unlink($big_imgs_path);
				}
				if(file_exists($small_imgs_path)){
					unlink($small_imgs_path);
				}

			// Delete Items Record from tb_contents database
			$this->_delete($update_id);
		}

		//////// Delete Contents Process from controller backend/src/Contents/delete_contents
		function delete_contents_process($update_id)
		{

			$this->load->model('Mdl_site_security');
			$this->Mdl_site_security->_make_sure_is_admin();

			if(!is_numeric($update_id)){
				redirect('backend/src/Contents/manage');
			}

			$this->load->library('session');

			$submit = $this->input->post('submit', TRUE);

			if($submit=="Cancel"){
				redirect('backend/src/Contents/create/'.$update_id);
			}elseif($submit=="Yes - Delete Contents"){
				$this->_process_delete($update_id);

				$flash_msg = "Deleted Contents was Successfully";
				$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
				$this->session->set_flashdata('item', $value);

				redirect('backend/src/Contents/manage');
			}


		}

		//////// Delete Contents Page
		function delete_contents($update_id)
		{

			$this->load->model('Mdl_site_security');
			$this->Mdl_site_security->_make_sure_is_admin();

			if(!is_numeric($update_id)){
				redirect('backend/src/Contents/manage');
			}

			$this->load->library('session');

			$data['headline'] = "Delete Contents";
			$data['update_id'] = $update_id;
			$data['flash'] = $this->session->flashdata('item');

			// SESSION
		    $data['username'] = $this->session->userdata('ses_username');
		    $data['fullname'] = $this->session->userdata('ses_full_name');
		    $data['position'] = $this->session->userdata('ses_position');
		    $data['pic'] = $this->session->userdata('ses_pic');
		    // END SESSION
			
			$data['header'] = 'backend/src/header';
			$data['sidebar'] = 'backend/src/sidebar';
			$data['content'] = 'backend/pagecontents/delete_contents';
			$data['script'] = 'backend/src/script';
			
			$this->load->model('Mdl_formemail');
			$data['count'] = $this->Mdl_formemail->count_array(array('email_status' => 1, 'email_open' => 0));

			$this->load->view('backend/dashboard', $data);
		}

		//////// Delete Image Thumbnail
		function delete_image($update_id)
		{
			
			$this->load->model('Mdl_site_security');
			$this->Mdl_site_security->_make_sure_is_admin();

			if(!is_numeric($update_id)){
				redirect('backend/src/Contents/manage');
			}

		    $this->load->library('session');

			$data = $this->fetch_data_from_db($update_id);
			$big_imgs = $data['contents_big_img'];
			$small_imgs = $data['contents_small_img'];

			$big_imgs_path = './gallery/pagecontents/big_imgs/'.$big_imgs;
			$small_imgs_path = './gallery/pagecontents/small_imgs/'.$small_imgs;

			// Remove the images
			if(file_exists($big_imgs_path)){
				unlink($big_imgs_path);
			}
			if(file_exists($small_imgs_path)){
				unlink($small_imgs_path);
			}

			// Update Remove the images to database
			unset($data);
			$data['contents_big_img'] = "";
			$data['contents_small_img'] = "";
			$this->_update($update_id, $data);

			$flash_msg = "The image was successfully deleted."; // Alert message
			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
			$this->session->set_flashdata('item', $value); // สร้าง session alert

			redirect('backend/src/Contents/create/'.$update_id);
		}

		//////// Set Image Thumbnail
		function _generate_thumbnail($file_name)
		{
			$config['image_library'] = 'gd2';
			$config['source_image'] = './gallery/pagecontents/big_imgs/'.$file_name;
			$config['new_image'] = './gallery/pagecontents/small_imgs/'.$file_name;
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = TRUE;
			$config['width']         = 360;
			$config['height']       = 208;

			$this->load->library('image_lib', $config);

			$this->image_lib->resize();
		}

		//////// Upload Image Contents Do Upload
		function do_upload($update_id)
		{
			
			$this->load->model('Mdl_site_security');
			$this->Mdl_site_security->_make_sure_is_admin();

			if(!is_numeric($update_id)){
				redirect('backend/src/Contents/manage');
			}

		    $this->load->library('session');

			$submit = $this->input->post('submit', TRUE);
			if($submit == "Cancel"){
				redirect('backend/src/Contents/create/'.$update_id);
			}

			$config['upload_path'] = './gallery/pagecontents/big_imgs/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = 200;
			$config['max_width'] = 1500;
			$config['max_height'] = 1268;

			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('userfile'))
			{
				$data['error'] = array('error' => $this->upload->display_errors("<p style='color:red;'>","</p>"));
				$data['headline'] = "Upload Error";
				$data['update_id'] = $update_id;
				$data['flash'] = $this->session->flashdata('item');

				// SESSION
		      	$data['username'] = $this->session->userdata('ses_username');
		      	$data['fullname'] = $this->session->userdata('ses_full_name');
		      	$data['position'] = $this->session->userdata('ses_position');
		      	$data['pic'] = $this->session->userdata('ses_pic');
		     	// END SESSION

				$data['header'] = 'backend/src/header';
				$data['sidebar'] = 'backend/src/sidebar';
				$data['content'] = 'backend/pagecontents/upload_image';
				$data['script'] = 'backend/src/script';
				
			$this->load->model('Mdl_formemail');
			$data['count'] = $this->Mdl_formemail->count_array(array('email_status' => 1, 'email_open' => 0));

				$this->load->view('backend/dashboard', $data);

			}else{
				// Upload Successful
				
				$data = array('upload_data' => $this->upload->data());

				$upload_data = $data['upload_data'];
				$file_name = $upload_data['file_name'];
				$this->_generate_thumbnail($file_name);

				// Update Image to Database
				$update_data['contents_big_img'] = $file_name;
				$update_data['contents_small_img'] = explode('.', $file_name)[0]."_thumb.".explode('.', $file_name)[1];
				$this->_update($update_id, $update_data);

				$data['headline'] = "Upload Success";
				$data['update_id'] = $update_id;
				$data['flash'] = $this->session->flashdata('item');
				
				// SESSION
		      	$data['username'] = $this->session->userdata('ses_username');
		      	$data['fullname'] = $this->session->userdata('ses_full_name');
		      	$data['position'] = $this->session->userdata('ses_position');
		      	$data['pic'] = $this->session->userdata('ses_pic');
		      	// END SESSION

				$data['header'] = 'backend/src/header';
				$data['sidebar'] = 'backend/src/sidebar';
				$data['content'] = 'backend/pagecontents/upload_image';
				$data['script'] = 'backend/src/script';
				
			$this->load->model('Mdl_formemail');
			$data['count'] = $this->Mdl_formemail->count_array(array('email_status' => 1, 'email_open' => 0));

				$this->load->view('backend/dashboard', $data);
			}
		}

		//////// Upload Image Contents
		function upload_image($update_id)
		{
			
			$this->load->model('Mdl_site_security');
			$this->Mdl_site_security->_make_sure_is_admin();

			if(!is_numeric($update_id)){
				redirect('backend/src/Contents/manage');
			}

			$this->load->model('Mdl_formemail');
			$data['count'] = $this->Mdl_formemail->count_array(array('email_status' => 1, 'email_open' => 0));

		    $this->load->library('session');
			
			$data['headline'] = "Upload Image​ Contents";
			$data['update_id'] = $update_id;
			$data['flash'] = $this->session->flashdata('item');

			// SESSION
	      	$data['username'] = $this->session->userdata('ses_username');
	      	$data['fullname'] = $this->session->userdata('ses_full_name');
	      	$data['position'] = $this->session->userdata('ses_position');
	      	$data['pic'] = $this->session->userdata('ses_pic');
	      	// END SESSION
			
			$data['header'] = 'backend/src/header';
			$data['sidebar'] = 'backend/src/sidebar';
			$data['content'] = 'backend/pagecontents/upload_image';
			$data['script'] = 'backend/src/script';
			
			$this->load->view('backend/dashboard', $data);
		}

		//////// Add & Update Contents
		function create()
		{
			$this->load->library('session');
			$this->load->library('timedate');

		    $this->load->model('Mdl_site_security');
		    $this->Mdl_site_security->_make_sure_is_admin();
      
			$update_id = $this->uri->segment(5);
			if(isset(explode('_',$update_id)[1])){
				$lang = "_".explode('_',$update_id)[1];
			}
			else{
				$lang = "";
			}
			$update_id = explode('_',$update_id)[0];
			$submit = $this->input->post('submit', TRUE);

			if($submit == "Cancel"){
				redirect('backend/src/Contents/manage');
			}

			// ถ้า $submit = value->Submit
			if($submit == "Submit"){
				// Process the Form at create.php
				$this->load->library('form_validation'); // Load Library From Validation
				$this->form_validation->set_rules('contents_title', '<b>โปรดกรอกชื่อ Title</b>', 'required|max_length[240]');
				if(isset(explode('_',$this->uri->segment(5))[1])){
					$this->form_validation->set_rules('contents_title_en', '<b>โปรดกรอกชื่อ Title En</b>', 'required|max_length[240]');					
				}
		    	$this->form_validation->set_rules('contents_status', '<b>โปรดเลือกการเผยแพร่</b>', 'required|numeric');

		    	if($this->form_validation->run() == TRUE){ // ถ้า form_validation ทำงานแล้วถูกต้อง
		    		// Get The Variables
		    		$data = $this->fetch_data_from_post(); // ให้ $data = รับค่าจาก From method="POST"
///////////////////////////////////////////////////////////////////////
		    		$data['contents_name'] = $data['contents_title'];
///////////////////////////////////////////////////////////////////////
		    		$title = $this->url_slug($data['contents_title']);
		    		$data['contents_url'] = $title; // item_url = item_title

		    		//$data['date_published'] = $this->timedate->make_timestamp_from_datepicker($data['date_published']);
		    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id

		    			// Update Items Details
		    			$data['m_user'] = $this->session->userdata('ses_id');
		    			$this->_update($update_id, $data); // Update ลง Database
		    			$flash_msg = "Update Contents were Successfully"; // Alert message
						$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
						$this->session->set_flashdata('item', $value); // สร้าง session alert
						redirect('backend/src/contents/create/'.$update_id.$lang);
		    		}else{
		    			// Insert a New Items
		    			$data['c_user'] = $this->session->userdata('ses_id');
		    			$this->_insert($data);
		    			$update_id = $this->get_max(); // Get the ID at new item

		    			$flash_msg = "Insert Contents were Successfully";
						$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
						$this->session->set_flashdata('item', $value);
						redirect('backend/src/contents/create/'.$update_id.$lang);
		    		}
		    	}
			}

			if((is_numeric($update_id)) && ($submit!="Submit")){
				$data = $this->fetch_data_from_db($update_id);
			}else{
				$data = $this->fetch_data_from_post();
				$data['contents_big_img'] = "";
			}

			if(!is_numeric($update_id)){
				$data['headline'] = "เพิ่มข้อมูลคอนเทนต์";
			}else{
				$data['headline'] = "แก้ไขข้อมูลคอนเทนต์";
			}
			$data['update_id'] = $update_id.$lang;
			$data['flash'] = $this->session->flashdata('item');

			// SESSION
	      	$data['username'] = $this->session->userdata('ses_username');
	      	$data['fullname'] = $this->session->userdata('ses_full_name');
	      	$data['position'] = $this->session->userdata('ses_position');
	      	$data['pic'] = $this->session->userdata('ses_pic');
	      	// END SESSION

			$data['header'] = 'backend/src/header';
			$data['sidebar'] = 'backend/src/sidebar';
			$data['content'] = 'backend/pagecontents/formContents';
			$data['script'] = 'backend/src/script';

			$this->load->model('Mdl_formemail');
			$data['count'] = $this->Mdl_formemail->count_array(array('email_status' => 1, 'email_open' => 0));

			$this->load->view('backend/dashboard', $data);
		}

		//////// Contents Healthy Detail
		function manage()
		{

	        $this->load->model('Mdl_site_security');
	        $this->Mdl_site_security->_make_sure_is_admin();
      
			$data['query'] = $this->get('contents_title');

			$data['flash'] = $this->session->flashdata('item');

			// SESSION
	      	$data['username'] = $this->session->userdata('ses_username');
	      	$data['fullname'] = $this->session->userdata('ses_full_name');
	      	$data['position'] = $this->session->userdata('ses_position');
	      	$data['pic'] = $this->session->userdata('ses_pic');
	      	// END SESSION

			$data['header'] = 'backend/src/header';
			$data['sidebar'] = 'backend/src/sidebar';
			$data['content'] = 'backend/pagecontents/manageContents';
			$data['script'] = 'backend/src/script';

			$this->load->model('Mdl_formemail');
			$data['count'] = $this->Mdl_formemail->count_array(array('email_status' => 1, 'email_open' => 0));

			$this->load->view('backend/dashboard', $data);
		}

		function url_slug($text)
		{
			$array = array("'", '"', '\\', '/', '.', ',', '!', '+', '*', '%', '$', '#', '@', '(', ')', '[', ']', '?', '>', '<', '|', '’', '“', '”', '‘', '’', ';', ':', '–');
			$text = str_replace($array, '', $text); // Replaces all spaces with hyphens.
			$text = preg_replace('/[^A-Za-z0-9ก-๙เ-า\\-]/', '-', $text); // Removes special chars.
			$text = preg_replace('/-+/', "-", $text);
			$text = trim($text, '-');
			$text = urldecode($text);

			if (empty($text)){
				return NULL;
			}

			return $text;
		}

		//////// Fetch Data at method POST
		function fetch_data_from_post() // Fetch ข้อมูลจาก Input POST
		{
			$data['contents_title'] = $this->input->post('contents_title', TRUE);
			$data['contents_title_en'] = $this->input->post('contents_title_en', TRUE);
			$data['contents_name'] = $this->input->post('contents_name', TRUE);
			$data['contents_name_en'] = $this->input->post('contents_name_en', TRUE);
			$data['contents_subdesc'] = $this->input->post('contents_subdesc', TRUE);
			$data['contents_subdesc_en'] = $this->input->post('contents_subdesc_en', TRUE);
			$data['contents_description'] = $this->input->post('contents_description', TRUE);
			$data['contents_description_en'] = $this->input->post('contents_description_en', TRUE);
			$data['contents_status'] = $this->input->post('contents_status', TRUE);

			$data['meta_author'] = $this->input->post('meta_author', TRUE);
			$data['meta_keywords'] = $this->input->post('meta_keywords', TRUE);
			$data['meta_description'] = $this->input->post('meta_description', TRUE);
			
			return $data;
		}

		//////// Fetch Data at Database
		function fetch_data_from_db($update_id) //  Fetch ข้อมูลจาก Database
		{
			
			if(!is_numeric($update_id)){
				redirect('backend/src/Contents/manage');
			}

			$query = $this->get_where($update_id);
			foreach($query->result() as $row){
				$data['contents_url'] = $row->contents_url;
				$data['contents_title'] = $row->contents_title;
				$data['contents_title_en'] = $row->contents_title_en;
				$data['contents_name'] = $row->contents_name;
				$data['contents_name_en'] = $row->contents_name_en;
				$data['contents_subdesc'] = $row->contents_subdesc;
				$data['contents_subdesc_en'] = $row->contents_subdesc_en;
				$data['contents_description'] = $row->contents_description;
				$data['contents_description_en'] = $row->contents_description_en;
				$data['contents_status'] = $row->contents_status;
				$data['contents_big_img'] = $row->contents_big_img;
				$data['contents_small_img'] = $row->contents_small_img;
				$data['created_at'] = $row->created_at;
				$data['updated_at'] = $row->updated_at;
				$data['c_user'] = $row->c_user;
				$data['m_user'] = $row->m_user;
				
				$data['meta_author'] = $row->meta_author;
				$data['meta_keywords'] = $row->meta_keywords;
				$data['meta_description'] = $row->meta_description;
			}

			if(!isset($data)){
				$data = "";
			}

			return $data;
		}

		function get($order_by)
		{
			$this->load->model('Mdl_formcontents');
			$query = $this->Mdl_formcontents->get($order_by);
			return $query;
		}

		function get_with_limit($limit, $offset, $order_by)
		{
			$this->load->model('Mdl_formcontents');
			$query = $this->Mdl_formcontents->get_with_limit($limit, $offset, $order_by);
			return $query;
		}

		function get_where($id)
		{
			$this->load->model('Mdl_formcontents');
			$query = $this->Mdl_formcontents->get_where($id);
			return $query;
		}

		function get_where_custom($col, $value)
		{
			$this->load->model('Mdl_formcontents');
			$query = $this->Mdl_formcontents->get_where_custom($col, $value);
			return $query;
		}

		function _insert($data)
		{
			$this->load->model('Mdl_formcontents');
			$this->Mdl_formcontents->_insert($data);
		}

		function _update($id, $data)
		{
			$this->load->model('Mdl_formcontents');
			$this->Mdl_formcontents->_update($id, $data);
		}

		function _delete($id)
		{
			$this->load->model('Mdl_formcontents');
			$this->Mdl_formcontents->_delete($id);
		}

		function count_where($column, $value)
		{
			$this->load->model('Mdl_formcontents');
			$count = $this->Mdl_formcontents->count_where($column, $value);
			return $count;
		}

		function get_max()
		{
			$this->load->model('Mdl_formcontents');
			$max_id = $this->Mdl_formcontents->get_max();
			return $max_id;
		}

		function _custom_query($mysql_query)
		{
			$this->load->model('Mdl_formcontents');
			$query = $this->Mdl_formcontents->_custom_query($mysql_query);
			return $query;
		}
		
	}
