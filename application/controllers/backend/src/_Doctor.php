<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Mdl_site_security');
		$this->Mdl_site_security->_make_sure_is_admin();
	}

	public function fetch_header()
	{
		$mysql_query = "select * from tb_header where header_status=1";
		$query = $this->_custom_query($mysql_query)->row();

		return $query;
	}

	public function fetch_nav(){
		$mysql_query = "select nav_title,nav_url,id
		from tb_navigation
		where nav_status=1
		order by order_priority asc
		";
		$query = $this->_custom_query($mysql_query);

		return $query;
	}

	public function fetch_subnav(){
		$mysql_query = "select tb_subnavigation.sub_nav_title,tb_subnavigation.sub_nav_url,tb_subnavigation.parent_nav_id,
		tb_navigation.id
		from tb_subnavigation
		inner join tb_navigation
		on tb_subnavigation.parent_nav_id=tb_navigation.id
		where tb_subnavigation.sub_nav_status=1
		and tb_subnavigation.parent_nav_id=tb_navigation.id
		order by tb_subnavigation.order_priority asc
		";
		$query = $this->_custom_query($mysql_query);

		return $query;
	}

		public function fetch_sidemenu(){
			$mysql_query = "select sidemenu_title,sidemenu_url,sidemenu_content,id
			from tb_sidemenu
			where sidemenu_status=1
			order by order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}

		public function fetch_subsidemenu(){
			$mysql_query = "select parent_sidemenu_id,sub_sidemenu_title,sub_sidemenu_url,sub_sidemenu_content,id
			from tb_subofsidemenu
			where sub_sidemenu_status=1
			order by order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}

		public function fetch_subofsubmenu(){
			$mysql_query = "select parent_submenu_id,subofsubmenu_title,subofsubmenu_url,subofsubmenu_content,id
			from tb_subofsubmenu
			where subofsubmenu_status=1
			order by order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}

	function view_doctor_details($update_id)
	{
		redirect('frontend/Home/DoctorDetails_preview/'.$update_id);
		/*
		$this->load->model('Mdl_site_security');
		$this->Mdl_site_security->_make_sure_is_admin();

		if(!is_numeric($update_id)){
			redirect('backend/src/Doctor/manage');
		}

			// Fetch the Doctor Details By ID
		$data = $this->fetch_data_from_db($update_id);
		$data['update_id'] = $update_id;
		$data['flash'] = $this->session->flashdata('item');


		$mysql_query = "select * from tb_doctor where id='$update_id'";
		$data['query'] = $this->_custom_query($mysql_query)->row();

		$mysql_query = "select * from tb_workdayhours where medical_id='".$data['query']->medical_id."'";
		$data['query_workdayhour'] = $this->_custom_query($mysql_query)->result();
		$query = $this->get_where($update_id);
		/*		
		foreach($query->result() as $row){
			$data['meta_author'] = $row->meta_author;
			$data['meta_keywords'] = $row->meta_keywords;
			$data['meta_description'] = $row->meta_description;
		}*/

		/*
		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['doctorDetail'] = 'frontend/src/doctor/doctor_details';
		$data['footerpages'] = 'frontend/src/footerpages';

			
		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();
		
		$data['get_sidemenu'] = $this->fetch_sidemenu()->result();
    	$data['get_subsidemenu'] = $this->fetch_subsidemenu()->result();
   		$data['get_subofsubmenu'] = $this->fetch_subofsubmenu()->result();
		$this->load->view('frontend/homepage', $data);
		*/
	}

	function import_doctor()
	{

		$this->load->model('Mdl_site_security');
		$this->Mdl_site_security->_make_sure_is_admin();

		$this->load->model('Mdl_formdoctor');
		$query = $this->Mdl_formdoctor->_custom_postgresql_query();
		die();
	}

		//////// Process Delete All 1.Colours 2.Sizes 3.Big Image 4.Small Image 5.Data Record
	function _process_delete($update_id)
	{

		$this->load->model('Mdl_site_security');
		$this->Mdl_site_security->_make_sure_is_admin();

			// Delete Doctor Logo Big & Small Image
		$data = $this->fetch_data_from_db($update_id);
		$big_imgs = $data['doctor_imgs'];

		$big_imgs_path = './gallery/doctor/big_imgs/'.$big_imgs;

				// Remove the images
		if(file_exists($big_imgs_path)){
			unlink($big_imgs_path);
		}

			// Delete Doctor Record from tb_doctor database
		$this->_delete($update_id);
	}

		//////// Delete Doctor Process from controller backend/src/Doctor/delete_doctor
	function delete_doctor_process($update_id)
	{

		$this->load->model('Mdl_site_security');
		$this->Mdl_site_security->_make_sure_is_admin();

		if(!is_numeric($update_id)){
			redirect('backend/src/Doctor/manage');
		}

		$this->load->library('session');

		$submit = $this->input->post('submit', TRUE);

		if($submit=="Cancel"){
			redirect('backend/src/Doctor/create/'.$update_id);
		}elseif($submit=="Yes - Delete Doctor"){
			$this->_process_delete($update_id);

			$flash_msg = "Deleted Doctor was Successfully";
			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
			$this->session->set_flashdata('item', $value);

			redirect('backend/src/Doctor/manage');
		}


	}

		//////// Delete Doctor Page
	function delete_doctor($update_id)
	{

		$this->load->model('Mdl_site_security');
		$this->Mdl_site_security->_make_sure_is_admin();

		if(!is_numeric($update_id)){
			redirect('backend/src/Doctor/manage');
		}

		$this->load->library('session');

		$data['headline'] = "Delete Doctor";
		$data['update_id'] = $update_id;
		$data['flash'] = $this->session->flashdata('item');

			// SESSION
		$data['username'] = $this->session->userdata('ses_username');
		$data['fullname'] = $this->session->userdata('ses_full_name');
		$data['position'] = $this->session->userdata('ses_position');
		$data['pic'] = $this->session->userdata('ses_pic');
	      	// END SESSION

		$data['header'] = 'backend/src/header';
		$data['sidebar'] = 'backend/src/sidebar';
		$data['content'] = 'backend/doctor/delete_doctor';
		$data['script'] = 'backend/src/script';

		$this->load->model('Mdl_formemail');
		$data['count'] = $this->Mdl_formemail->count_array(array('email_status' => 1, 'email_open' => 0));

		$this->load->view('backend/dashboard', $data);
	}

		//////// Delete Image Thumbnail
	function delete_image($update_id)
	{

		$this->load->model('Mdl_site_security');
		$this->Mdl_site_security->_make_sure_is_admin();

		if(!is_numeric($update_id)){
			redirect('backend/src/Doctor/manage');
		}

		$this->load->library('session');

		$data = $this->fetch_data_from_db($update_id);
		$big_imgs = $data['doctor_imgs'];

		$big_imgs_path = './gallery/doctor/big_imgs/'.$big_imgs;

			// Remove the images
		if(file_exists($big_imgs_path)){
			unlink($big_imgs_path);
		}

			// Update Remove the images to database
		unset($data);
		$data['doctor_imgs'] = "";
		$this->_update($update_id, $data);

			$flash_msg = "The image was successfully deleted."; // Alert message
			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
			$this->session->set_flashdata('item', $value); // สร้าง session alert

			redirect('backend/src/Doctor/create/'.$update_id);
		}

		//////// Set Image Thumbnail
		function _generate_thumbnail($file_name)
		{
			
			$this->load->model('Mdl_site_security');
			$this->Mdl_site_security->_make_sure_is_admin();

			$config['image_library'] = 'gd2';
			$config['source_image'] = './gallery/doctor/big_imgs/'.$file_name;
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = TRUE;
			$config['width']         = 235;
			$config['height']       = 96;

			$this->load->library('image_lib', $config);

			$this->image_lib->resize();
		}

		// PREVIEW IMAGE //
		function modal_do_upload($update_id)
		{
			
			$this->load->model('Mdl_site_security');
			$this->Mdl_site_security->_make_sure_is_admin();

			if(!is_numeric($update_id)){
				redirect('backend/src/Doctor/manage');
			}

		    $this->load->library('session');

			$config['upload_path'] = './gallery/doctor/preview_imgs/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = 200;
			$config['max_width'] = 1500;
			$config['max_height'] = 1268;

			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('file'))
			{
				// Upload Error

				$data['error'] = array('error' => $this->upload->display_errors("<p style='color:red;'>","</p>"));
				$data['headline'] = "Upload Error";
				$data['update_id'] = $update_id;
				$data['flash'] = $this->session->flashdata('item');


			}else{
				// Upload Successful
				
				$data = array('upload_data' => $this->upload->data());

				$upload_data = $data['upload_data'];
				$file_name = $upload_data['file_name'];
				$this->_generate_thumbnail($file_name);

				// Update Image to Database
				$update_data['doctor_preview_imgs'] = $file_name;
				$this->_update($update_id, $update_data);

				$data['headline'] = "Upload Success";
				$data['update_id'] = $update_id;
				$data['flash'] = $this->session->flashdata('item');

				echo json_encode($update_data['doctor_preview_imgs']);

			}
		}
		function fetch_preview_image($update_id) //  Fetch ข้อมูลจาก Database
		{			
			if(!is_numeric($update_id)){
				redirect('backend/src/Doctor/manage');
			}

			$query = $this->get_where($update_id);
			foreach($query->result() as $row){				
				$data['doctor_preview_imgs'] = $row->doctor_preview_imgs;
			}

			if(!isset($data)){
				$data = "";
			}

			return $data;
		}
		function delete_preview($update_id)
		{
			
			$this->load->model('Mdl_site_security');
			$this->Mdl_site_security->_make_sure_is_admin();

			if(!is_numeric($update_id)){
				redirect('backend/src/Doctor/manage');
			}

		    $this->load->library('session');

			$data = $this->fetch_preview_image($update_id);
			$preview_imgs = $data['doctor_preview_imgs'];

			$preview_imgs_path = './gallery/doctor/preview_imgs/'.$preview_imgs;

			// Remove the images
			if(file_exists($preview_imgs_path)){
				unlink($preview_imgs_path);
			}

			unset($data);
			$data['doctor_preview_imgs'] = "";
			$this->_update($update_id, $data);
			echo json_encode("Success");
		}
		function upload_preview($update_id){
			$preview_img_name = $this->fetch_preview_image($update_id)['doctor_preview_imgs'];			
			if($preview_img_name != ""){
				$preview_path = 'gallery/doctor/preview_imgs/'.$preview_img_name;
				$big_path = 'gallery/doctor/big_imgs/'.$preview_img_name;
				copy($preview_path, $big_path);

				$update_data['doctor_imgs'] = $preview_img_name;
				$this->_update($update_id, $update_data);

				$this->delete_preview($update_id);
			}
		}
		// PREVIEW IMAGE //


		//////// Upload Image Doctor Do Upload
		function do_upload($update_id)
		{
			
			$this->load->model('Mdl_site_security');
			$this->Mdl_site_security->_make_sure_is_admin();

			if(!is_numeric($update_id)){
				redirect('backend/src/Doctor/manage');
			}

			$this->load->library('session');

			$submit = $this->input->post('submit', TRUE);
			if($submit == "Cancel"){
				redirect('backend/src/Doctor/create/'.$update_id);
			}

			$config['upload_path'] = './gallery/doctor/big_imgs/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = 204800;
			$config['max_width'] = 1024;
			$config['max_height'] = 768;

			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('userfile'))
			{
				$data['error'] = array('error' => $this->upload->display_errors("<p style='color:red;'>","</p>"));
				$data['headline'] = "Upload Error";
				$data['update_id'] = $update_id;
				$data['flash'] = $this->session->flashdata('item');

				// SESSION
				$data['username'] = $this->session->userdata('ses_username');
				$data['fullname'] = $this->session->userdata('ses_full_name');
				$data['position'] = $this->session->userdata('ses_position');
				$data['pic'] = $this->session->userdata('ses_pic');
		      	// END SESSION

				$data['header'] = 'backend/src/header';
				$data['sidebar'] = 'backend/src/sidebar';
				$data['content'] = 'backend/doctor/upload_image';
				$data['script'] = 'backend/src/script';
				
			$this->load->model('Mdl_formemail');
			$data['count'] = $this->Mdl_formemail->count_array(array('email_status' => 1, 'email_open' => 0));

				$this->load->view('backend/dashboard', $data);

			}else{
				// Upload Successful
				
				$data = array('upload_data' => $this->upload->data());

				$upload_data = $data['upload_data'];
				$file_name = $upload_data['file_name'];
				$this->_generate_thumbnail($file_name);

				// Update Image to Database
				$update_data['doctor_imgs'] = $file_name;
				$this->_update($update_id, $update_data);
				
				$flash_msg = "Upload Image were Successfully";
				$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
				$this->session->set_flashdata('item', $value);

				$data['headline'] = "Upload Success";
				$data['update_id'] = $update_id;
				$data['flash'] = $this->session->flashdata('item');

				// SESSION
				$data['username'] = $this->session->userdata('ses_username');
				$data['fullname'] = $this->session->userdata('ses_full_name');
				$data['position'] = $this->session->userdata('ses_position');
				$data['pic'] = $this->session->userdata('ses_pic');
		      	// END SESSION
				
				$data['header'] = 'backend/src/header';
				$data['sidebar'] = 'backend/src/sidebar';
				$data['content'] = 'backend/doctor/upload_image';
				$data['script'] = 'backend/src/script';
				
			$this->load->model('Mdl_formemail');
			$data['count'] = $this->Mdl_formemail->count_array(array('email_status' => 1, 'email_open' => 0));

				$this->load->view('backend/dashboard', $data);
			}
		}

		//////// Upload Image Doctor
		function upload_image($update_id)
		{
			
			$this->load->model('Mdl_site_security');
			$this->Mdl_site_security->_make_sure_is_admin();

			if(!is_numeric($update_id)){
				redirect('backend/src/Doctor/manage');
			}

			$this->load->model('Mdl_formemail');
			$data['count'] = $this->Mdl_formemail->count_array(array('email_status' => 1, 'email_open' => 0));

			$this->load->library('session');
			
			$data['headline'] = "Upload Image​ Doctor";
			$data['update_id'] = $update_id;
			$data['flash'] = $this->session->flashdata('item');

			// SESSION
			$data['username'] = $this->session->userdata('ses_username');
			$data['fullname'] = $this->session->userdata('ses_full_name');
			$data['position'] = $this->session->userdata('ses_position');
			$data['pic'] = $this->session->userdata('ses_pic');
	      	// END SESSION
			
			$data['header'] = 'backend/src/header';
			$data['sidebar'] = 'backend/src/sidebar';
			$data['content'] = 'backend/doctor/upload_image';
			$data['script'] = 'backend/src/script';
			
			$this->load->view('backend/dashboard', $data);
		}

		//////// Add & Update Doctor
		function create()
		{
			
			$this->load->model('Mdl_site_security');
			$this->Mdl_site_security->_make_sure_is_admin();

			$this->load->library('session');

			$update_id = $this->uri->segment(5);
			$submit = $this->input->post('submit', TRUE);

			if($submit == "Cancel"){
				redirect('backend/src/Doctor/manage');
			}

			// ถ้า $submit = value->Submit
			if($submit == "Submit"){
				// Process the Form at create.php
				$this->load->library('form_validation'); // Load Library From Validation

				$this->form_validation->set_rules('firstname_th', '<b>โปรดกรอกเฉพาะชื่อภาษาไทย</b>', 'required|max_length[150]');
				$this->form_validation->set_rules('lastname_th', '<b>โปรดกรอกเฉพาะนามสกุลภาษาไทย</b>', 'required|max_length[150]');
				$this->form_validation->set_rules('education_th', '<b>โปรดกรอกประวัติการศึกษา</b>', 'required');
				/*$this->form_validation->set_rules('experience_th', '<b>โปรดกรอกประสบการณ์ทำงาน</b>', 'required');*/
				//$this->form_validation->set_rules('speciality_th', '<b>โปรดกรอกความเชี่ยวชาญทางการแพทย์</b>', 'required');
		    	$this->form_validation->set_rules('doctor_department', '<b>โปรดเลือกแผนกประจำการ</b>', 'required');
				$this->form_validation->set_rules('firstname_en', '<b>โปรดกรอกเฉพาะชื่อภาษาอังกฤษ</b>', 'required|max_length[150]');
				$this->form_validation->set_rules('lastname_en', '<b>โปรดกรอกเฉพาะนามสกุลอังกฤษ</b>', 'required|max_length[150]');
				$this->form_validation->set_rules('education_en', '<b>โปรดกรอกประวัติการศึกษาภาษาอังกฤษ</b>', 'required');
				/*$this->form_validation->set_rules('experience_en', '<b>โปรดเลือกประสบการณ์ทำงานภาษาอังกฤษ</b>', 'required');*/
				//$this->form_validation->set_rules('speciality_en', '<b>โปรดกรอกความเชี่ยวชาญทางการแพทย์ภาษาอังกฤษ</b>', 'required');
				$this->form_validation->set_rules('phone', '<b>โปรดกรอกเบอร์โทรศัพท์</b>', 'required|numeric|max_length[30]|min_length[10]');
				$this->form_validation->set_rules('email', '<b>โปรดกรอกอีเมลล์สำหรับติดต่อ</b>', 'required|max_length[150]|valid_email');
				$this->form_validation->set_rules('type_th', '<b>โปรดเลือกสังกัตโรงพยาบาล</b>', 'required');
				//$this->form_validation->set_rules('address', '<b>โปรดกรอกที่อยู่ของแพทย์</b>', 'required|min_length[15]');
				$this->form_validation->set_rules('medical_id', '<b>โปรดกรอกรหัสประจำตัวแพทย์</b>', 'required');
				$this->form_validation->set_rules('gender', '<b>โปรดเลือกเพศของแพทย์</b>', 'required');
				$this->form_validation->set_rules('status', '<b>โปรดเลือกสถานะของแพทย์</b>', 'required');
				$this->form_validation->set_rules('at_hospital', '<b>โปรดเลือกโรงพยาบาลของแพทย์</b>', 'required');

		    	if($this->form_validation->run() == TRUE){ // ถ้า form_validation ทำงานแล้วถูกต้อง
		    		// Get The Variables
		    		$data = $this->fetch_data_from_post(); // ให้ $data = รับค่าจาก From method="POST"
		    		$title = $this->url_slug($data['firstname_th']);
		    		$data['doctor_url'] = $title; // item_url = item_title
		    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id
		    			$this->upload_preview($update_id);

		    			// Update Doctor Details
		    			$this->_update($update_id, $data); // Update ลง Database
		    			$flash_msg = "Update Doctor were Successfully"; // Alert message
		    			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
						$this->session->set_flashdata('item', $value); // สร้าง session alert
						redirect('backend/src/Doctor/create/'.$update_id);
					}else{
		    			// Insert a New Doctor
						$this->_insert($data);
		    			$update_id = $this->get_max(); // Get the ID at new Doctor

		    			$flash_msg = "Insert Doctor were Successfully";
		    			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
		    			$this->session->set_flashdata('item', $value);
		    			redirect('backend/src/Doctor/create/'.$update_id);
		    		}
		    	}
		    }

		    if((is_numeric($update_id)) && ($submit!="Submit")){
		    	$data = $this->fetch_data_from_db($update_id);
		    }else{
		    	$data = $this->fetch_data_from_post();
		    	$data['doctor_imgs'] = "";
		    }

		    if(!is_numeric($update_id)){
		    	$data['headline'] = "เพิ่มข้อมูล Doctor";
		    }else{
		    	$data['headline'] = "แก้ไขข้อมูล Doctor";
		    }

		    $data['update_id'] = $update_id;
		    $data['flash'] = $this->session->flashdata('item');

			// SESSION
		    $data['username'] = $this->session->userdata('ses_username');
		    $data['fullname'] = $this->session->userdata('ses_full_name');
		    $data['position'] = $this->session->userdata('ses_position');
		    $data['pic'] = $this->session->userdata('ses_pic');
	      	// END SESSION

		    $this->load->model('Home_model');

		    $data['header'] = 'backend/src/header';
		    $data['sidebar'] = 'backend/src/sidebar';

		    $data['data_speciality']=$this->Home_model->s_speciality();
		    $data['medical_center']=$this->Home_model->medicial_center();
		    $data['content'] = 'backend/doctor/formDoctor';
			$data['Additionalscript'] = 'backend/doctor/scriptDoctor';
		    $data['script'] = 'backend/src/script';

			$this->load->model('Mdl_formemail');
			$data['count'] = $this->Mdl_formemail->count_array(array('email_status' => 1, 'email_open' => 0));

		    $this->load->view('backend/dashboard', $data);
		}

		//////// Doctor Detail
		function manage()
		{
			
			$this->load->model('Mdl_site_security');
			$this->load->model('Mdl_formdoctor');
			$this->Mdl_site_security->_make_sure_is_admin();

			$res = $this->Mdl_formdoctor->getDbSelfRegister();
			


			$data['query'] = $this->get('firstname_th');

			$data['flash'] = $this->session->flashdata('item');

			// SESSION
			$data['username'] = $this->session->userdata('ses_username');
			$data['fullname'] = $this->session->userdata('ses_full_name');
			$data['position'] = $this->session->userdata('ses_position');
			$data['pic'] = $this->session->userdata('ses_pic');
	      	// END SESSION

			$data['header'] = 'backend/src/header';
			$data['sidebar'] = 'backend/src/sidebar';
			$data['content'] = 'backend/doctor/manageDoctor';
			$data['script'] = 'backend/src/script';
			
			$this->load->model('Mdl_formemail');
			$data['count'] = $this->Mdl_formemail->count_array(array('email_status' => 1, 'email_open' => 0));

			$this->load->view('backend/dashboard', $data);
		}

		function url_slug($text)
		{
			$array = array("'", '"', '\\', '/', '.', ',', '!', '+', '*', '%', '$', '#', '@', '(', ')', '[', ']', '?', '>', '<', '|', '’', '“', '”', '‘', '’', ';', ':', '–');
			$text = str_replace($array, '', $text); // Replaces all spaces with hyphens.
			$text = preg_replace('/[^A-Za-z0-9ก-๙เ-า\\-]/', '-', $text); // Removes special chars.
			$text = preg_replace('/-+/', "-", $text);
			$text = trim($text, '-');
			$text = urldecode($text);

			if (empty($text)){
				return NULL;
			}

			return $text;
		}

		//////// Fetch Data at method POST
		function fetch_data_from_post() // Fetch ข้อมูลจาก Input POST
		{

			if($this->input->post('special_id') != ''){
				$data['speciality_id'] = array_unique($this->input->post('special_id', TRUE));
				$temp = "";
				$data['speciality_th'] = '';
				$data['speciality_en'] = '';
				if(isset($data['speciality_id'])){
					for ($i=0;$i<count($data['speciality_id']);$i++){
						$temp .= $data['speciality_id'][$i];

						$sp = $this->db->where('speciality_id', $data['speciality_id'][$i]);
						$sp = $this->db->get('speciality');
						$sp = $sp->row();
						$data['speciality_th'] .= $sp->speciality_name;
						$data['speciality_en'] .= $sp->speciality_name_en;

						if(!($i == count($data['speciality_id'])-1)){
							$temp .= ",";
							$data['speciality_th'] .= " ";
							$data['speciality_en'] .= " ";
						}
					}

				}
			$data['speciality_id'] = $temp;
			}else{
				$data['speciality_id'] = $this->input->post('special_id', TRUE);
			}


			$data['pname_th'] = $this->input->post('pname_th', TRUE);
			$data['firstname_th'] = $this->input->post('firstname_th', TRUE);
			$data['lastname_th'] = $this->input->post('lastname_th', TRUE);
			$data['pname_en'] = $this->input->post('pname_en', TRUE);
			$data['firstname_en'] = $this->input->post('firstname_en', TRUE);
			$data['lastname_en'] = $this->input->post('lastname_en', TRUE);
			$data['education_th'] = $this->input->post('education_th', TRUE);
			$data['education_en'] = $this->input->post('education_en', TRUE);
			/*$data['experience_th'] = $this->input->post('experience_th', TRUE);*/
			/*$data['experience_en'] = $this->input->post('experience_en', TRUE);*/
			$data['phone'] = $this->input->post('phone', TRUE);
			$data['email'] = $this->input->post('email', TRUE);
			$data['address'] = $this->input->post('address', TRUE);
			$data['type_th'] = $this->input->post('type_th', TRUE);
			$data['type_en'] = $this->input->post('type_en', TRUE);
			//$data['speciality_th'] = $this->input->post('speciality_th', TRUE);
			//$data['speciality_en'] = $this->input->post('speciality_en', TRUE);
			$data['doctor_department'] = $this->input->post('doctor_department', TRUE);
			$data['doctor_department_child'] = $this->input->post('doctor_department', TRUE);
			$data['language_th'] = $this->input->post('language_th', TRUE);
			$data['language_en'] = $this->input->post('language_en', TRUE);
			$data['gender'] = $this->input->post('gender', TRUE);
			$data['status'] = $this->input->post('status', TRUE);
			$data['at_hospital'] = $this->input->post('at_hospital', TRUE);
			$data['medical_id'] = $this->input->post('medical_id', TRUE);
			$data['doctor_description_th'] = $this->input->post('doctor_description_th', TRUE);
			$data['doctor_description_en'] = $this->input->post('doctor_description_en', TRUE);

			return $data;
		}

		//////// Fetch Data at Database
		function fetch_data_from_db($update_id) //  Fetch ข้อมูลจาก Database
		{
			
			if(!is_numeric($update_id)){
				redirect('backend/src/Doctor/manage');
			}

			$query = $this->get_where($update_id);
			foreach($query->result() as $row){
				
				$data['pname_th'] = $row->pname_th;
				$data['firstname_th'] = $row->firstname_th;
				$data['lastname_th'] = $row->lastname_th;
				$data['pname_en'] = $row->pname_en;
				$data['firstname_en'] = $row->firstname_en;
				$data['lastname_en'] = $row->lastname_en;
				$data['education_th'] = $row->education_th;
				$data['education_en'] = $row->education_en;
				$data['experience_th'] = $row->experience_th;
				$data['experience_en'] = $row->experience_en;
				$data['address'] = $row->address;
				$data['phone'] = $row->phone;
				$data['email'] = $row->email;
				$data['type_th'] = $row->type_th;
				$data['type_en'] = $row->type_en;
				$data['gender'] = $row->gender;
				$data['status'] = $row->status;
				$data['at_hospital'] = $row->at_hospital;
				$data['speciality_id'] = $row->speciality_id;
				$data['speciality_th'] = $row->speciality_th;
				$data['speciality_en'] = $row->speciality_en;
				$data['doctor_department'] = $row->doctor_department;
				$data['doctor_department_child'] = $row->doctor_department;
				$data['language_th'] = $row->language_th;
				$data['language_en'] = $row->language_en;
				$data['doctor_imgs'] = $row->doctor_imgs;
				$data['medical_id'] = $row->medical_id;
				$data['doctor_description_th'] = $row->doctor_description_th;
				$data['doctor_description_en'] = $row->doctor_description_en;
			}

			if(!isset($data)){
				$data = "";
			}

			return $data;
		}

		function get($order_by)
		{
			$this->load->model('Mdl_formdoctor');
			$query = $this->Mdl_formdoctor->get($order_by);
			return $query;
		}

		function get_with_limit($limit, $offset, $order_by)
		{
			$this->load->model('Mdl_formdoctor');
			$query = $this->Mdl_formdoctor->get_with_limit($limit, $offset, $order_by);
			return $query;
		}

		function get_where($id)
		{
			$this->load->model('Mdl_formdoctor');
			$query = $this->Mdl_formdoctor->get_where($id);
			return $query;
		}

		function get_where_custom($col, $value)
		{
			$this->load->model('Mdl_formdoctor');
			$query = $this->Mdl_formdoctor->get_where_custom($col, $value);
			return $query;
		}

		function _insert($data)
		{
			$this->load->model('Mdl_formdoctor');
			$this->Mdl_formdoctor->_insert($data);
		}

		function _update($id, $data)
		{
			$this->load->model('Mdl_formdoctor');
			$this->Mdl_formdoctor->_update($id, $data);
		}

		function _delete($id)
		{
			$this->load->model('Mdl_formdoctor');
			$this->Mdl_formdoctor->_delete($id);
		}

		function count_where($column, $value)
		{
			$this->load->model('Mdl_formdoctor');
			$count = $this->Mdl_formdoctor->count_where($column, $value);
			return $count;
		}

		function get_max()
		{
			$this->load->model('Mdl_formdoctor');
			$max_id = $this->Mdl_formdoctor->get_max();
			return $max_id;
		}

		function _custom_query($mysql_query)
		{
			$this->load->model('Mdl_formdoctor');
			$query = $this->Mdl_formdoctor->_custom_query($mysql_query);
			return $query;
		}
	}
