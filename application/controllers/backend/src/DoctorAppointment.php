<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class DoctorAppointment extends CI_Controller {

		function __construct()
		{
			parent::__construct();
		}

		public function fetch_header()
		{
			$mysql_query = "select * from tb_header where header_status=1";
			$query = $this->_custom_query($mysql_query)->row();

			return $query;
		}

		public function fetch_nav(){
			$mysql_query = "select nav_title,nav_url,nav_content,id
			from tb_navigation
			where nav_status=1
			order by order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}

		public function fetch_subnav(){
			$mysql_query = "select tb_subnavigation.id as subid,tb_subnavigation.sub_nav_title,tb_subnavigation.sub_nav_url,tb_subnavigation.sub_nav_content,tb_subnavigation.parent_nav_id,
			tb_navigation.id
			from tb_subnavigation
			inner join tb_navigation
			on tb_subnavigation.parent_nav_id=tb_navigation.id
			where tb_subnavigation.sub_nav_status=1
			and tb_subnavigation.parent_nav_id=tb_navigation.id
			order by tb_subnavigation.order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}

		public function fetch_sidemenu(){
			$mysql_query = "select sidemenu_title,sidemenu_url,sidemenu_content,id
			from tb_sidemenu
			where sidemenu_status=1
			order by order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}

		public function fetch_subsidemenu(){
			$mysql_query = "select parent_sidemenu_id,sub_sidemenu_title,sub_sidemenu_url,sub_sidemenu_content,id
			from tb_subofsidemenu
			where sub_sidemenu_status=1
			order by order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}

		public function fetch_subofsubmenu(){
			$mysql_query = "select parent_submenu_id,subofsubmenu_title,subofsubmenu_url,subofsubmenu_content,id
			from tb_subofsubmenu
			where subofsubmenu_status=1
			order by order_priority asc
			";
			$query = $this->_custom_query($mysql_query);

			return $query;
		}

		//////// Add & Update Package
		function create()
		{
			$this->load->library('timedate');
    		// Get The Variables
    		$data = $this->fetch_data_from_post(); // ให้ $data = รับค่าจาก From method="POST"
    		$this->_insert($data);
    		redirect(base_url().'frontend/Home/doctorAppointment','refresh');
		}

		//////// Fetch Data at method POST
		function fetch_data_from_post() // Fetch ข้อมูลจาก Input POST
		{
			$data['firstname'] = $this->input->post('firstname', TRUE);
			$data['lastname'] = $this->input->post('lastname', TRUE);
			$data['birthdate'] = $this->input->post('birthdate', TRUE);
			$data['gender'] = $this->input->post('gender', TRUE);
			$data['email'] = $this->input->post('form_email', TRUE);
			$data['phone'] = $this->input->post('form_phone', TRUE);
			$data['medical_master_id'] = $this->input->post('medical_master_id', TRUE);
			$data['doctor_speciality_id'] = $this->input->post('doctor_speciality_id', TRUE);
			$data['doctor_medical_id'] = $this->input->post('doctor_medical_id', TRUE);
			$data['appointment_date'] = $this->input->post('appointment_date', TRUE);
			//var_dump($data['appointment_date']); die();
			$data['appointment_sdate'] = $this->input->post('appointment_sdate', TRUE);
			$data['description'] = $this->input->post('description', TRUE);
			$data['status'] = $this->input->post('status', TRUE);
			$data['sub_status'] = $this->input->post('subscribe', TRUE);

			$data['birthdate'] = date('Y-m-d',strtotime($data['birthdate']));	

			return $data;
		}

		//////// Fetch Data at Database
		function fetch_data_from_db($update_id) //  Fetch ข้อมูลจาก Database
		{
			
			if(!is_numeric($update_id)){
				redirect('backend/src/Package/manage');
			}

			$query = $this->get_where($update_id);
			foreach($query->result() as $row){
				$data['package_url'] = $row->package_url;
				$data['package_title'] = $row->package_title;
				$data['package_type'] = $row->package_type;
				$data['package_gallery'] = $row->package_gallery;
				$data['package_name'] = $row->package_name;
				$data['package_subdesc'] = $row->package_subdesc;
				$data['package_description'] = $row->package_description;
				$data['package_unitprice'] = $row->package_unitprice;
				$data['package_startdate'] = $row->package_startdate;
				$data['package_enddate'] = $row->package_enddate;
				$data['package_status'] = $row->package_status;
				$data['package_big_img'] = $row->package_big_img;
				$data['package_small_img'] = $row->package_small_img;
				$data['created_at'] = $row->created_at;
				$data['updated_at'] = $row->updated_at;
				
				$cur_date = strtotime(date('Y-m-d'));
				$stime = strtotime($data['package_startdate']);
				$etime = strtotime($data['package_enddate']);
				if($cur_date >= $stime && $cur_date <= $etime){
					$data['package_status'] = '1';
				}
				else{
					$data['package_status'] = '0';
				}

				$data['package_startdate'] = date('m/d/Y',strtotime($data['package_startdate']));
				$data['package_enddate'] = date('m/d/Y',strtotime($data['package_enddate']));
								
				$data['meta_author'] = $row->meta_author;
				$data['meta_keywords'] = $row->meta_keywords;
				$data['meta_description'] = $row->meta_description;
			}

			if(!isset($data)){
				$data = "";
			}

			return $data;
		}

		function get($order_by)
		{
			$this->load->model('Mdl_formdoctorappointment');
			$query = $this->Mdl_formpackage->get($order_by);
			return $query;
		}

		function get_with_limit($limit, $offset, $order_by)
		{
			$this->load->model('Mdl_formdoctorappointment');
			$query = $this->Mdl_formdoctorappointment->get_with_limit($limit, $offset, $order_by);
			return $query;
		}

		function get_where($id)
		{
			$this->load->model('Mdl_formdoctorappointment');
			$query = $this->Mdl_formdoctorappointment->get_where($id);
			return $query;
		}

		function get_where_custom($col, $value)
		{
			$this->load->model('Mdl_formdoctorappointment');
			$query = $this->Mdl_formdoctorappointment->get_where_custom($col, $value);
			return $query;
		}

		function _insert($data)
		{
			$this->load->model('Mdl_formdoctorappointment');
			$this->Mdl_formdoctorappointment->_insert($data);
		}

		function _update($id, $data)
		{
			$this->load->model('Mdl_formdoctorappointment');
			$this->Mdl_formdoctorappointment->_update($id, $data);
		}

		function _delete($id)
		{
			$this->load->model('Mdl_formdoctorappointment');
			$this->Mdl_formdoctorappointment->_delete($id);
		}

		function count_where($column, $value)
		{
			$this->load->model('Mdl_formdoctorappointment');
			$count = $this->Mdl_formdoctorappointment->count_where($column, $value);
			return $count;
		}

		function get_max()
		{
			$this->load->model('Mdl_formdoctorappointment');
			$max_id = $this->Mdl_formdoctorappointment->get_max();
			return $max_id;
		}

		function _custom_query($mysql_query)
		{
			$this->load->model('Mdl_formdoctorappointment');
			$query = $this->Mdl_formdoctorappointment->_custom_query($mysql_query);
			return $query;
		}
		
	}
