<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Header extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Mdl_site_security');
		$this->Mdl_site_security->_make_sure_is_admin();

		$this->load->library('session');

		$this->load->model('Back_Model');
	}

	function view_header_details($update_id)
	{			
		redirect('backend/MainBackend/index_preview/');
	}

	//////// Process Delete All 1.Colours 2.Sizes 3.Big Image 4.Small Image 5.Data Record
	function _process_delete($update_id)
	{
		// Delete Header Logo Big & Small Image
		$data = $this->fetch_data_from_db($update_id);
		$big_imgs = $data['header_logo_big_img'];
		$small_imgs = $data['header_logo_small_img'];

		$big_imgs_path = './gallery/logo/big_imgs/'.$big_imgs;
		$small_imgs_path = './gallery/logo/small_imgs/'.$small_imgs;

			// Remove the images
			if(file_exists($big_imgs_path)){
				unlink($big_imgs_path);
			}
			if(file_exists($small_imgs_path)){
				unlink($small_imgs_path);
			}

		// Delete Items Record from store_items database
		$this->_delete($update_id);
	}

	//////// Delete Header Process from controller backend/src/Header/delete_header
	function delete_header_process($update_id)
	{
		if(!is_numeric($update_id)){
			redirect('backend/src/Header/manage');
		}

		$submit = $this->input->post('submit', TRUE);

		if($submit=="Cancel"){
			redirect('backend/src/Header/create/'.$update_id);
		}elseif($submit=="Yes - Delete Header"){
			$this->_process_delete($update_id);

			$flash_msg = "Deleted Header was Successfully";
			$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
			$this->session->set_flashdata('item', $value);

			redirect('backend/src/Header/manage');
		}
	}

	//////// Delete Header Page
	function delete_header($update_id)
	{
		if(!is_numeric($update_id)){
			redirect('backend/src/Header/manage');
		}

		$data['headline'] = "Delete Header";
		$data['update_id'] = $update_id;
		

		$data['content'] = 'backend/header/delete_header';

		$data = $this->Back_Model->General($data);
		$this->load->view('backend/dashboard', $data);
	}

	//////// Delete Image Thumbnail
	function delete_image($update_id)
	{
		if(!is_numeric($update_id)){
			redirect('backend/src/Header/manage');
		}

		$data = $this->fetch_data_from_db($update_id);
		$big_imgs = $data['header_logo_big_img'];
		$small_imgs = $data['header_logo_small_img'];

		$big_imgs_path = './gallery/logo/big_imgs/'.$big_imgs;
		$small_imgs_path = './gallery/logo/small_imgs/'.$small_imgs;

		// Remove the images
		if(file_exists($big_imgs_path)){
			unlink($big_imgs_path);
		}
		if(file_exists($small_imgs_path)){
			unlink($small_imgs_path);
		}

		// Update Remove the images to database
		unset($data);
		$data['header_logo_big_img'] = "";
		$data['header_logo_small_img'] = "";
		$this->_update($update_id, $data);

		$flash_msg = "The image was successfully deleted."; // Alert message
		$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
		$this->session->set_flashdata('item', $value); // สร้าง session alert

		redirect('backend/src/Header/create/'.$update_id);
	}

	//////// Set Image Thumbnail
	function _generate_thumbnail($file_name)
	{
		$config['image_library'] = 'gd2';
		$config['source_image'] = './gallery/logo/big_imgs/'.$file_name;
		$config['new_image'] = './gallery/logo/small_imgs/'.$file_name;
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = TRUE;
		$config['width']         = 291;
		$config['height']       = 50;

		$this->load->library('image_lib', $config);

		$this->image_lib->resize();
	}

	// PREVIEW IMAGE //
	function modal_do_upload($update_id)
	{
		if(!is_numeric($update_id)){
			redirect('backend/src/Header/manage');
		}

		$config['upload_path'] = './gallery/logo/preview_imgs/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = 200;
		$config['max_width'] = 1500;
		$config['max_height'] = 1268;

		$this->load->library('upload', $config);

		if(!$this->upload->do_upload('file'))
		{
			// Upload Error
			$data['error'] = array('error' => $this->upload->display_errors("<p style='color:red;'>","</p>"));
			$data['headline'] = "Upload Error";
			$data['update_id'] = $update_id;
			

		}else{
			// Upload Successful				
			$data = array('upload_data' => $this->upload->data());

			$upload_data = $data['upload_data'];
			$file_name = $upload_data['file_name'];
			$this->_generate_thumbnail($file_name);

			// Update Image to Database
			$update_data['header_logo_preview_img'] = $file_name;
			$this->_update($update_id, $update_data);

			$data['headline'] = "Upload Success";
			$data['update_id'] = $update_id;
			

			echo json_encode($update_data['header_logo_preview_img']);
		}
	}
	function fetch_preview_image($update_id) //  Fetch ข้อมูลจาก Database
	{			
		if(!is_numeric($update_id)){
			redirect('backend/src/Header/manage');
		}

		$query = $this->get_where($update_id);
		foreach($query->result() as $row){				
			$data['header_logo_preview_img'] = $row->header_logo_preview_img;
		}

		if(!isset($data)){
			$data = "";
		}

		return $data;
	}
	function delete_preview($update_id)
	{
		if(!is_numeric($update_id)){
			redirect('backend/src/Header/manage');
		}
	    
		$data = $this->fetch_preview_image($update_id);
		$preview_imgs = $data['header_logo_preview_img'];

		$preview_imgs_path = './gallery/logo/preview_imgs/'.$preview_imgs;

		// Remove the images
		if(file_exists($preview_imgs_path)){
			unlink($preview_imgs_path);
		}

		unset($data);
		$data['header_logo_preview_img'] = "";
		$this->_update($update_id, $data);
		echo json_encode("Success");
	}
	function upload_preview($update_id){
		$preview_img_name = $this->fetch_preview_image($update_id)['header_logo_preview_img'];			
		if($preview_img_name != ""){
			$preview_path = 'gallery/logo/preview_imgs/'.$preview_img_name;
			$big_path = 'gallery/logo/big_imgs/'.$preview_img_name;
			copy($preview_path, $big_path);
			$this->_generate_thumbnail($preview_img_name);

			$update_data['header_logo_big_img'] = $preview_img_name;
			$update_data['header_logo_small_img'] = explode('.', $preview_img_name)[0]."_thumb.".explode('.', $preview_img_name)[1];
			$this->_update($update_id, $update_data);

			$this->delete_preview($update_id);
		}
	}
	// PREVIEW IMAGE //

	//////// Upload Image Header Do Upload
	function do_upload($update_id)
	{
		if(!is_numeric($update_id)){
			redirect('backend/src/Header/manage');
		}

		$submit = $this->input->post('submit', TRUE);
		if($submit == "Cancel"){
			redirect('backend/src/Header/create/'.$update_id);
		}

		$config['upload_path'] = './gallery/logo/big_imgs/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = 100;
		$config['max_width'] = 1024;
		$config['max_height'] = 768;

		$this->load->library('upload', $config);

		if(!$this->upload->do_upload('userfile'))
		{
			$data['error'] = array('error' => $this->upload->display_errors("<p style='color:red;'>","</p>"));
			$data['headline'] = "Upload Error";
			$data['update_id'] = $update_id;
			
							
			$data['content'] = 'backend/header/upload_image';

			$data = $this->Back_Model->General($data);
			$this->load->view('backend/dashboard', $data);

		}else{
			// Upload Successful				
			$data = array('upload_data' => $this->upload->data());

			$upload_data = $data['upload_data'];
			$file_name = $upload_data['file_name'];
			$this->_generate_thumbnail($file_name);

			// Update Image to Database
			$update_data['header_logo_big_img'] = $file_name;
			$update_data['header_logo_small_img'] = $file_name;
			$this->_update($update_id, $update_data);

			$data['headline'] = "Upload Success";
			$data['update_id'] = $update_id;
			
			
			$data['content'] = 'backend/header/upload_image';

			$data = $this->Back_Model->General($data);
			$this->load->view('backend/dashboard', $data);
		}
	}

	//////// Upload Image Header
	function upload_image($update_id)
	{
		
		if(!is_numeric($update_id)){
			redirect('backend/src/Header/manage');
		}

		$data['headline'] = "Upload Image​ Header";
		$data['update_id'] = $update_id;
		

		$data['content'] = 'backend/header/upload_image';
		
		$data = $this->Back_Model->General($data);
		$this->load->view('backend/dashboard', $data);
	}

	//////// Add & Update Header
	function create()
	{
		
		$update_id = $this->uri->segment(5);
		$submit = $this->input->post('submit', TRUE);

		if($submit == "Cancel"){
			redirect('backend/src/Header/manage');
		}

		// ถ้า $submit = value->Submit
		if($submit == "Submit"){
			// Process the Form at create.php
			$this->load->library('form_validation'); // Load Library From Validation

			$this->form_validation->set_rules('header_title', '<b>โปรดกรอกเฉพาะชื่อ Title</b>', 'required|max_length[240]');
	    	$this->form_validation->set_rules('header_status', '<b>โปรดเลือกการเผยแพร่</b>', 'required|numeric');

	    	if($this->form_validation->run() == TRUE){ // ถ้า form_validation ทำงานแล้วถูกต้อง
	    		// Get The Variables
	    		$data = $this->fetch_data_from_post(); // ให้ $data = รับค่าจาก From method="POST"
	    		$title = $this->url_slug($data['header_title']);
	    		$data['header_url'] = $title; // item_url = item_title
	    		if(is_numeric($update_id)){ // ถ้ารับค่ามาเป็นตัวเลข $update_id
	    			$this->upload_preview($update_id);

	    			// Update Items Details
	    			$this->_update($update_id, $data); // Update ลง Database
	    			$flash_msg = "Update Header were Successfully"; // Alert message
					$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
					$this->session->set_flashdata('item', $value); // สร้าง session alert
					redirect('backend/src/Header/create/'.$update_id);
	    		}else{
	    			// Insert a New Items
	    			$this->_insert($data);
	    			$update_id = $this->get_max(); // Get the ID at new item

	    			$flash_msg = "Insert Header were Successfully";
					$value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
					$this->session->set_flashdata('item', $value);
					redirect('backend/src/Header/create/'.$update_id);
	    		}
	    	}
		}

		if((is_numeric($update_id)) && ($submit!="Submit")){
			$data = $this->fetch_data_from_db($update_id);
		}else{
			$data = $this->fetch_data_from_post();
			$data['header_logo_big_img'] = "";
		}

		if(!is_numeric($update_id)){
			$data['headline'] = "เพิ่มข้อมูล Header";
		}else{
			$data['headline'] = "แก้ไขข้อมูล Header";
		}

		$data['update_id'] = $update_id;
		

		$data['content'] = 'backend/header/formHeader';
		$data['Additionalscript'] = 'backend/header/scriptHeader';

		$data = $this->Back_Model->General($data);
		$this->load->view('backend/dashboard', $data);
	}

	//////// Header Detail
	function manage()
	{
		$data['query'] = $this->get('header_title');

		
		
		$data['content'] = 'backend/header/manageHeader';

		$data = $this->Back_Model->General($data);
		$this->load->view('backend/dashboard', $data);
	}

	function url_slug($text)
	{
		$array = array("'", '"', '\\', '/', '.', ',', '!', '+', '*', '%', '$', '#', '@', '(', ')', '[', ']', '?', '>', '<', '|', '’', '“', '”', '‘', '’', ';', ':', '–');
		$text = str_replace($array, '', $text); // Replaces all spaces with hyphens.
		$text = preg_replace('/[^A-Za-z0-9ก-๙เ-า\\-]/', '-', $text); // Removes special chars.
		$text = preg_replace('/-+/', "-", $text);
		$text = trim($text, '-');
		$text = urldecode($text);

		if (empty($text)){
			return NULL;
		}

		return $text;
	}

	//////// Fetch Data at method POST
	function fetch_data_from_post() // Fetch ข้อมูลจาก Input POST
	{
		$data['header_title'] = $this->input->post('header_title', TRUE);
		$data['header_status'] = $this->input->post('header_status', TRUE);
		$data['url_facebook'] = $this->input->post('url_facebook', TRUE);
		$data['url_twitter'] = $this->input->post('url_twitter', TRUE);
		$data['url_instagram'] = $this->input->post('url_instagram', TRUE);
		$data['url_youtube'] = $this->input->post('url_youtube', TRUE);

		return $data;
	}

	//////// Fetch Data at Database
	function fetch_data_from_db($update_id) //  Fetch ข้อมูลจาก Database
	{
		
		if(!is_numeric($update_id)){
			redirect('backend/src/Header/manage');
		}

		$query = $this->get_where($update_id);
		foreach($query->result() as $row){
			
			$data['header_title'] = $row->header_title;
			$data['header_url'] = $row->header_url;
			$data['url_facebook'] = $row->url_facebook;
			$data['url_twitter'] = $row->url_twitter;
			$data['url_instagram'] = $row->url_instagram;
			$data['url_youtube'] = $row->url_youtube;
			$data['header_status'] = $row->header_status;
			$data['header_logo_big_img'] = $row->header_logo_big_img;
			$data['header_logo_small_img'] = $row->header_logo_small_img;
			$data['created_at'] = $row->created_at;
			$data['updated_at'] = $row->edited_at;
		}

		if(!isset($data)){
			$data = "";
		}

		return $data;
	}

	function get($order_by)
	{
		$this->load->model('Mdl_formheader');
		$query = $this->Mdl_formheader->get($order_by);
		return $query;
	}

	function get_with_limit($limit, $offset, $order_by)
	{
		$this->load->model('Mdl_formheader');
		$query = $this->Mdl_formheader->get_with_limit($limit, $offset, $order_by);
		return $query;
	}

	function get_where($id)
	{
		$this->load->model('Mdl_formheader');
		$query = $this->Mdl_formheader->get_where($id);
		return $query;
	}

	function get_where_custom($col, $value)
	{
		$this->load->model('Mdl_formheader');
		$query = $this->Mdl_formheader->get_where_custom($col, $value);
		return $query;
	}

	function _insert($data)
	{
		$this->load->model('Mdl_formheader');
		$this->Mdl_formheader->_insert($data);
	}

	function _update($id, $data)
	{
		$this->load->model('Mdl_formheader');
		$this->Mdl_formheader->_update($id, $data);
	}

	function _delete($id)
	{
		$this->load->model('Mdl_formheader');
		$this->Mdl_formheader->_delete($id);
	}

	function count_where($column, $value)
	{
		$this->load->model('Mdl_formheader');
		$count = $this->Mdl_formheader->count_where($column, $value);
		return $count;
	}

	function get_max()
	{
		$this->load->model('Mdl_formheader');
		$max_id = $this->Mdl_formheader->get_max();
		return $max_id;
	}

	function _custom_query($mysql_query)
	{
		$this->load->model('Mdl_formheader');
		$query = $this->Mdl_formheader->_custom_query($mysql_query);
		return $query;
	}
}
