  <?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  class Account extends CI_Controller {

    public function __construct()
    {
      parent::__construct();

      $this->load->library('session');

      $this->load->model('Back_Model');
    }

    public function delete_account_process($update_id)
    {
      $this->load->model('Mdl_site_security');
      $this->Mdl_site_security->_make_sure_is_admin();
      
      if(!is_numeric($update_id)){
        redirect('backend/Account/manage');
      }
      $submit = $this->input->post('submit', TRUE);

      if($submit=="Cancel"){
        redirect('backend/Account/Register/'.$update_id);
      }elseif($submit=="Yes - Delete Account"){
        $this->_process_delete($update_id);

        $flash_msg = "Deleted Account was Successfully";
        $value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
        $this->session->set_flashdata('item', $value);

        redirect('backend/Account/manage');
      }


    }

    function _process_delete($update_id)
    {

      $this->load->model('Mdl_site_security');
      $this->Mdl_site_security->_make_sure_is_admin();

      $data = $this->fetch_data_from_db($update_id);
      $big_imgs = $data['acc_images'];

      $big_imgs_path = './gallery/account/big_imgs/'.$big_imgs;

        // Remove the images
        if(file_exists($big_imgs_path)){
          unlink($big_imgs_path);
        }

      // Delete Items Record from tb_package database
      $this->_delete($update_id);
    }

    public function delete_account($update_id)
    {

      $this->load->model('Mdl_site_security');
      $this->Mdl_site_security->_make_sure_is_admin();

      if(!is_numeric($update_id)){
        redirect('backend/Account/manage');
      }      

      $data['headline'] = "Delete Account";
      $data['update_id'] = $update_id;
      
      $data['content'] = 'backend/account/delete_account';
              
      $data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
    }

    public function delete_image($update_id)
    {
      if(!is_numeric($update_id)){
        redirect('backend/Account/manage');
      }

      $data = $this->fetch_data_from_db($update_id);
      $big_imgs = $data['acc_images'];

      $big_imgs_path = './gallery/account/big_imgs/'.$big_imgs;

      // Remove the images
      if(file_exists($big_imgs_path)){
        unlink($big_imgs_path);
      }

      // Update Remove the images to database
      unset($data);
      $data['acc_images'] = "";
      $this->_update($update_id, $data);

      $flash_msg = "The image was successfully deleted."; // Alert message
      $value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
      $this->session->set_flashdata('item', $value); // สร้าง session alert

      redirect('backend/Account/Register/'.$update_id);
    }

    public function _generate_thumbnail($file_name)
    {
      $config['image_library'] = 'gd2';
      $config['source_image'] = './gallery/account/big_imgs/'.$file_name;
      $config['create_thumb'] = TRUE;
      $config['maintain_ratio'] = TRUE;
      $config['width']         = 235;
      $config['height']       = 96;

      $this->load->library('image_lib', $config);

      $this->image_lib->resize();
    }

    // PREVIEW IMAGE //
    function modal_do_upload($update_id)
    {
      
      $this->load->model('Mdl_site_security');
      $this->Mdl_site_security->_make_sure_is_admin();

      if(!is_numeric($update_id)){
        redirect('backend/Account/manage');
      }

      $config['upload_path'] = './gallery/account/preview_imgs/';
      $config['allowed_types'] = 'gif|jpg|png';
      $config['max_size'] = 200;
      $config['max_width'] = 1500;
      $config['max_height'] = 1268;

      $this->load->library('upload', $config);

      if(!$this->upload->do_upload('file'))
      {
        // Upload Error
        $data['error'] = array('error' => $this->upload->display_errors("<p style='color:red;'>","</p>"));
        $data['headline'] = "Upload Error";
        $data['update_id'] = $update_id;        

      }else{
        // Upload Successful        
        $data = array('upload_data' => $this->upload->data());

        $upload_data = $data['upload_data'];
        $file_name = $upload_data['file_name'];
        $this->_generate_thumbnail($file_name);

        // Update Image to Database
        $update_data['acc_preview_images'] = $file_name;
        $this->_update($update_id, $update_data);

        $data['headline'] = "Upload Success";
        $data['update_id'] = $update_id;        

        echo json_encode($update_data['acc_preview_images']);

      }
    }
    function fetch_preview_image($update_id) //  Fetch ข้อมูลจาก Database
    {     
      if(!is_numeric($update_id)){
        redirect('backend/Account/manage');
      }

      $query = $this->get_where($update_id);
      foreach($query->result() as $row){        
        $data['acc_preview_images'] = $row->acc_preview_images;
      }

      if(!isset($data)){
        $data = "";
      }

      return $data;
    }
    function fetch_acc_image($update_id) //  Fetch ข้อมูลจาก Database
    {     
      if(!is_numeric($update_id)){
        redirect('backend/Account/manage');
      }

      $query = $this->get_where($update_id);
      foreach($query->result() as $row){        
        $data['acc_images'] = $row->acc_images;
      }

      if(!isset($data)){
        $data = "";
      }

      return $data['acc_images'];
    }
    function fetch_acc_password($update_id) //  Fetch ข้อมูลจาก Database
    {     
      if(!is_numeric($update_id)){
        redirect('backend/Account/manage');
      }

      $query = $this->get_where($update_id);
      foreach($query->result() as $row){        
        $data['acc_password'] = $row->acc_password;
      }

      if(!isset($data)){
        $data = "";
      }

      return $data['acc_password'];
    }
    function delete_preview($update_id)
    {
      
      $this->load->model('Mdl_site_security');
      $this->Mdl_site_security->_make_sure_is_admin();

      if(!is_numeric($update_id)){
        redirect('backend/Account/manage');
      }

      $data = $this->fetch_preview_image($update_id);
      $preview_imgs = $data['acc_preview_images'];

      $preview_imgs_path = './gallery/account/preview_imgs/'.$preview_imgs;

      // Remove the images
      if(file_exists($preview_imgs_path)){
        unlink($preview_imgs_path);
      }

      unset($data);
      $data['acc_preview_images'] = "";
      $this->_update($update_id, $data);
      echo json_encode("Success");
    }
    function upload_preview($update_id){
      $preview_img_name = $this->fetch_preview_image($update_id)['acc_preview_images'];

      if($preview_img_name != ""){

        $preview_path = 'gallery/account/preview_imgs/'.$preview_img_name;
        $big_path = 'gallery/account/big_imgs/'.$preview_img_name;
        copy($preview_path, $big_path);
        $this->_generate_thumbnail($preview_img_name);

        $update_data['acc_images'] = $preview_img_name;
        $this->_update($update_id, $update_data);

        $this->delete_preview($update_id);
      }
    }

    // PREVIEW IMAGE //

    public function do_upload($update_id)
    {
      $this->load->model('Mdl_site_security');
      $this->Mdl_site_security->_make_sure_is_admin();

      if(!is_numeric($update_id)){
        redirect('backend/Account/manage');
      }

      $submit = $this->input->post('submit', TRUE);
      if($submit == "Cancel"){
        redirect('backend/Account/Register/'.$update_id);
      }

      $config['upload_path'] = './gallery/account/big_imgs/';
      $config['allowed_types'] = 'gif|jpg|png';
      $config['max_size'] = 204800;
      $config['max_width'] = 1024;
      $config['max_height'] = 768;

      $this->load->library('upload', $config);

      if(!$this->upload->do_upload('userfile'))
      {
        $data['error'] = array('error' => $this->upload->display_errors("<p style='color:red;'>","</p>"));
        $data['headline'] = "Upload Error";
        $data['update_id'] = $update_id;
        
        $data['content'] = 'backend/account/upload_image';        
        
        $data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);

      }else{
        // Upload Successful
        
        $data = array('upload_data' => $this->upload->data());

        $upload_data = $data['upload_data'];
        $file_name = $upload_data['file_name'];
        $this->_generate_thumbnail($file_name);

        // Update Image to Database
        $update_data['acc_images'] = $file_name;
        $this->_update($update_id, $update_data);
        
        $flash_msg = "Upload Image were Successfully";
        $value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
        $this->session->set_flashdata('item', $value);

        $data['headline'] = "Upload Success";
        $data['update_id'] = $update_id;
        
        $data['content'] = 'backend/account/upload_image';        
        
        $data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
      }
    }

    public function upload_image($update_id)
    {

      $this->load->model('Mdl_site_security');
      $this->Mdl_site_security->_make_sure_is_admin();

      if(!is_numeric($update_id)){
        redirect('backend/Account/manage');
      }
            
      $data['headline'] = "Upload Image​ Account";
      $data['update_id'] = $update_id;
      
      $data['content'] = 'backend/account/upload_image';
      
      
      $data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
    }

    public function Register()
    {
      $this->load->model('Mdl_site_security');
      $this->Mdl_site_security->_make_sure_is_admin();

        $update_id = $this->uri->segment(4);/*
        echo "<pre>"; print_r($update_id); die();*/
        $submit = $this->input->post('submit', TRUE);                

        if($submit == "Cancel"){
          redirect('backend/Account/manage');
        }

        // ถ้า $submit = value->Submit
        if($submit == "Submit"){
          // Process the Form at create.php
          $this->load->library('form_validation'); // Load Library From Validation

          $this->form_validation->set_rules('acc_firstname', '<b>โปรดกรอกเฉพาะชื่อแอคเค้าท์</b>', 'required|max_length[150]');
          $this->form_validation->set_rules('acc_lastname', '<b>โปรดกรอกเฉพาะนามสกุลแอคเค้าท์</b>', 'required|max_length[150]');
          $this->form_validation->set_rules('acc_personal_id', '<b>โปรดกรอกเลขบัตรประชาชน</b>', 'required');
          $this->form_validation->set_rules('acc_email', '<b>โปรดกรอกอีเมลล์สำหรับติดต่อ</b>', 'required|max_length[150]|valid_email');
          $this->form_validation->set_rules('acc_status', '<b>โปรดเลือกสถานะของแอคเค้าท์</b>', 'required');
          $this->form_validation->set_rules('acc_at_hospital', '<b>โปรดเลือกโรงพยาบาลของแอคเค้าท์</b>', 'required');
          /*
          if(!($this->input->post('acc_password', TRUE) == $this->fetch_acc_password($update_id))){
            $this->form_validation->set_rules('acc_password', '<b>โปรดกรอก Password</b>', 'required|min_length[7]|max_length[35]');
            $this->form_validation->set_rules('acc_repeat_password', '<b>โปรดกรอก Password ให้ตรงกัน</b>', 'required|matches[acc_password]');
          }
          */

            if($this->form_validation->run() == TRUE){ // ถ้า form_validation ทำงานแล้วถูกต้อง
              // Get The Variables              
              /*$password = $this->input->post('acc_password', TRUE); // ให้ $data = รับค่าจาก From method="POST"*/
              $data = $this->fetch_data_from_post();
              /*echo "<pre>"; print_r($data); die();*/
              $this->load->model('Mdl_site_security');

              //if(!($data['acc_password'] == $this->fetch_acc_password($update_id))){
                $data['acc_password'] = $this->Mdl_site_security->_hash_string($data['acc_password']);
              //}
                              
              if(is_numeric($update_id)){                
                $this->upload_preview($update_id);

                // Update Items Details
                $this->_update($update_id, $data); // Update ลง Database
                $flash_msg = "Update Account were Successfully"; // Alert message
                $value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
                $this->session->set_flashdata('item', $value); // สร้าง session alert

                if($this->session->userdata('ses_id') == $update_id){
                    
                    $this->session->set_userdata("ses_username", $data['acc_username']); 
                    $this->session->set_userdata("ses_full_name", $data['acc_firstname']." ".$data['acc_lastname']);
                    $this->session->set_userdata("ses_pic", $this->fetch_acc_image($update_id));
                }

                redirect('backend/Account/Register/'.$update_id);
              }else{
                // Insert a New Doctor
                $this->_insert($data);
                $update_id = $this->get_max(); // Get the ID at new Doctor

                $flash_msg = "Insert Doctor were Successfully";
                $value = "<div class='alert alert-success fade show'>".$flash_msg."</div>";
                $this->session->set_flashdata('item', $value);
                redirect('backend/Account/Register/'.$update_id);
              }
            }
            
        }

        if((is_numeric($update_id)) && ($submit!="Submit")){
          $data = $this->fetch_data_from_db($update_id);
        }else{
          $data = $this->fetch_data_from_post();
          $data['acc_images'] = "";
        }

        if(!is_numeric($update_id)){
          $data['headline'] = "เพิ่ม Account";
        }else{
          $data['headline'] = "แก้ไข Account";
        }

        $data['update_id'] = $update_id;
                
        $data['content'] = 'backend/account/Registration';
        $data['Additionalscript'] = 'backend/account/scriptAccount';

        $data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
    }

    public function manage()
    {
      $this->load->model('Mdl_site_security');
      $this->Mdl_site_security->_make_sure_is_admin();

        $data['query'] = $this->get('acc_firstname');
        
        $data['content'] = 'backend/account/manageAccount';
                
        $data = $this->Back_Model->General($data);$this->load->view('backend/dashboard', $data);
    }

    public function loginChk(){
      

      $this->load->model('Mdl_site_security');
      $submit = $this->input->post('submit',TRUE);
      if($submit != 'Login'){
        redirect('frontend/Home');
      }

      if($submit == 'Login'){
        $this->load->library('form_validation');
        $this->form_validation->set_rules('acc_username','กรุณากรอก Username','trim|required|max_length[98]');
        $this->form_validation->set_rules('acc_password','กรุณากรอก Password','trim|required|min_length[5]|max_length[253]');

        /*$data = $this->show_data_from_login();*/
        if($this->form_validation->run()==TRUE){
          $check = $this->Mdl_site_security->check_login();

          /*echo "<pre>"; print_r($check); die();*/

          if($check != FALSE){
            foreach ($check as $row){
              $session_data = array(
                        'ses_id' => $row->acc_id,
                        'ses_username' => $row->acc_username,
                        'ses_full_name' => $row->acc_firstname." ".$row->acc_lastname,
                        'ses_position' => $row->acc_position,
                        'ses_status' => $row->acc_status,
                        'ses_pic' => $row->acc_images
                      );

              $this->session->set_userdata($session_data);
            }

            if($session_data['ses_position']=='100' && $session_data['ses_status']=='1'){
              echo json_encode(['success'=>true,'redirect'=>base_url('backend/Dashboard')]);
              //redirect('backend/Dashboard');
            }
            else{
              echo json_encode(['success'=>false,'reason'=>3]);
              //redirect('frontend/Home');
            }
          }else{
            echo json_encode(['success'=>false,'reason'=>2]);
            //redirect('frontend/Home');
          }

          
        }
        else{
          echo json_encode(['success'=>false,'reason'=>1]);
          //redirect(base_url('frontend/Home'));
        }
      }
    }

    public function show_data_from_login(){
      $data['acc_username'] = $this->input->post('acc_username',TRUE);
      $data['acc_password'] = $this->input->post('acc_password',TRUE);
      return $data;
    }

    public function fetch_data_from_post() // Fetch ข้อมูลจาก Input POST
    {
      $data['acc_firstname'] = $this->input->post('acc_firstname', TRUE);
      $data['acc_lastname'] = $this->input->post('acc_lastname', TRUE);
      $data['acc_personal_id'] = $this->input->post('acc_personal_id', TRUE);
      $data['acc_telephone'] = $this->input->post('acc_telephone', TRUE);
      $data['acc_address'] = $this->input->post('acc_address', TRUE);
      $data['acc_email'] = $this->input->post('acc_email', TRUE);
      $data['acc_gender'] = $this->input->post('acc_gender', TRUE);
      $data['acc_at_hospital'] = $this->input->post('acc_at_hospital', TRUE);
      $data['acc_username'] = $this->input->post('acc_username', TRUE);
      $data['acc_password'] = $this->input->post('acc_password', TRUE);
      $data['acc_status'] = $this->input->post('acc_status', TRUE);

      return $data;
    }

    //////// Fetch Data at Database
    public function fetch_data_from_db($update_id) //  Fetch ข้อมูลจาก Database
    {
      
      if(!is_numeric($update_id)){
        redirect('backend/Account/manage');
      }

      $query = $this->get_where($update_id);
      foreach($query->result() as $row){
        
        $data['acc_firstname'] = $row->acc_firstname;
        $data['acc_lastname'] = $row->acc_lastname;
        $data['acc_personal_id'] = $row->acc_personal_id;
        $data['acc_telephone'] = $row->acc_telephone;
        $data['acc_address'] = $row->acc_address;
        $data['acc_email'] = $row->acc_email;
        $data['acc_gender'] = $row->acc_gender;
        $data['acc_at_hospital'] = $row->acc_at_hospital;
        $data['acc_status'] = $row->acc_status;
        $data['acc_username'] = $row->acc_username;
        $data['acc_password'] = $row->acc_password;
        $data['acc_images'] = $row->acc_images;
      }

      if(!isset($data)){
        $data = "";
      }

      return $data;
    }

      ///////////////////////// USER //////////////////////////////////

    function get($order_by)
    {
      $this->load->model('Mdl_account');
      $query = $this->Mdl_account->get($order_by);
      return $query;
    }

    function get_with_limit($limit, $offset, $order_by)
    {
      $this->load->model('Mdl_account');
      $query = $this->Mdl_account->get_with_limit($limit, $offset, $order_by);
      return $query;
    }

    function get_where($id)
    {
      $this->load->model('Mdl_account');
      $query = $this->Mdl_account->get_where($id);
      return $query;
    }

    function get_where_custom($col, $value)
    {
      $this->load->model('Mdl_account');
      $query = $this->Mdl_account->get_where_custom($col, $value);
      return $query;
    }

    function _insert($data)
    {
      $this->load->model('Mdl_account');
      $this->Mdl_account->_insert($data);
    }

    function _update($id, $data)
    {
      $this->load->model('Mdl_account');
      $this->Mdl_account->_update($id, $data);
    }

    function _delete($id)
    {
      $this->load->model('Mdl_account');
      $this->Mdl_account->_delete($id);
    }

    function count_where($column, $value)
    {
      $this->load->model('Mdl_account');
      $count = $this->Mdl_account->count_where($column, $value);
      return $count;
    }

    function get_max()
    {
      $this->load->model('Mdl_account');
      $max_id = $this->Mdl_account->get_max();
      return $max_id;
    }

    function _custom_query($mysql_query)
    {
      $this->load->model('Mdl_account');
      $query = $this->Mdl_account->_custom_query($mysql_query);
      return $query;
    }

}