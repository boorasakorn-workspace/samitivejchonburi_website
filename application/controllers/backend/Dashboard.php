<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Dashboard extends CI_Controller {

		function __construct()
		{
			parent::__construct();

			$this->load->model('Mdl_site_security');
			$this->Mdl_site_security->_make_sure_is_admin();

			$this->load->library('session');

			$this->load->model('Back_Model');
		}

		function index()
		{
			$data = $this->Back_Model->initGeneral();
			$this->load->view('backend/dashboard', $data);
		}

		function logout(){
			$this->session->unset_userdata('ses_id');
			$this->session->unset_userdata('ses_username');
			$this->session->unset_userdata('ses_full_name');
			$this->session->unset_userdata('ses_position');
			$this->session->unset_userdata('ses_status');
			$this->session->unset_userdata('ses_pic');
            redirect('frontend/Home');
		}
	}
