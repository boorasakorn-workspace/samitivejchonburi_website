<section class="ulockd-divider bgc-whitef8">
    <div class="container">
      <div class="row">
        <div class="col-xxs-6 col-xs-6 col-sm-3 col-md-3 col-lg-3 ulockd-pdng0 ulockd-pdng01">
          <div class="feature-box2 ulockd-mrgn12-100 bgc-success" style="padding: 40px 25px 90px 25px;height: 320px;">
            <div class="details">
              <h3><?=$this->lang->line('samitivej_Find_a_Doctor');?></h3>
              <p class="listcontent "><?=$this->lang->line('samitivej_Find_a_Doctor_text');?></p> 
              <a href="<?php echo base_url();?>frontend/Home/OurDoctors" class="btn btn-md ulockd-btn-white"><?=$this->lang->line('samitivej_Find_a_Doctor');?></a>
            </div>
          </div>
        </div>
        <div class="col-xxs-6 col-xs-6 col-sm-3 col-md-3 col-lg-3 ulockd-pdng0 ulockd-pdng01">
          <div class="feature-box2 ulockd-mrgn12-100 bgc-success1" style="padding: 40px 25px 90px 25px;height: 320px;">
            <div class="details">
              <h3><?=$this->lang->line('samitivej_Request_an_Appointment');?></h3>
              <p class="listcontent "><?=$this->lang->line('samitivej_Request_an_Appointment_text');?></p>
              <a href="<?php echo base_url();?>frontend/Home/doctorAppointment" data-toggle="modal" class="btn btn-md ulockd-btn-white"><span class="flaticon-hospital-call"></span> <?=$this->lang->line('samitivej_Request_an_Appointment');?></a>

            </div>
          </div>
        </div>
        <div class="col-xxs-6 col-xs-6 col-sm-3 col-md-3 col-lg-3 ulockd-pdng0 ulockd-pdng01">
          <div class="feature-box2 ulockd-mrgn12-100 bgc-success2" style="padding: 40px 25px 30px 25px;height: 320px;">
            <div class="details">
              <h3><?=$this->lang->line('samitivej_Hotline');?></h3>
              <p class="lead1"><span class="flaticon-delivery color-white"></span> +(66) 33 038 912</p>
              <p class="listcontent hidden-xs"><?=$this->lang->line('samitivej_Hotline_text');?></p>
              <a href="<?php echo base_url();?>frontend/Home/ContactUs" data-toggle="modal" class="btn btn-md ulockd-btn-white"><span class="flaticon-hospital-call"></span> <?=$this->lang->line('samitivej_Contact');?></a>
            </div>
          </div>
        </div>
        <div class="col-xxs-6 col-xs-6 col-sm-3 col-md-3 col-lg-3 ulockd-pdng0 ulockd-pdng01">
          <div class="feature-box2 ulockd-mrgn12-100 bgc-success3" style="padding: 40px 25px 50px 25px;height: 320px;">
            <h3><span class="flaticon-passage-of-time color-white icon"></span> <?=$this->lang->line('samitivej_Working_Time_title');?></h3>
            <div class="details">
              <ul class="list-group lead2">
              <li><strong class="hidden-xs"><?=$this->lang->line('samitivej_Mon-Fri');?></strong><h5 class="visible-xs"><?=$this->lang->line('samitivej_Mon-Fri');?></h4> <span class="pull-right"> 8:00 - 20:30</span></li>
              <li><strong class="hidden-xs"><?=$this->lang->line('samitivej_Saturday');?></strong><h5 class="visible-xs"><?=$this->lang->line('samitivej_Saturday');?></h4> <span class="pull-right"> 8:30 - 18:00</span></li>
              <li><strong class="hidden-xs"><?=$this->lang->line('samitivej_Sunday');?></strong><h5 class="visible-xs"><?=$this->lang->line('samitivej_Sunday');?></h4> <span class="pull-right"> 9:00 - 14:30</span></li>
            </ul>
            </div>
            
          </div>
        </div>
      </div>      
      <!-- Product Popup View Start-->
      <div class="modal fade appointment_view" id="appointment_view">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header text-center">
                      <a href="#" data-dismiss="modal" class="class pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                      <h3 class="modal-title">Appointment Form. </h3>
                  </div>
                  <div class="modal-body">
                      <div class="row">
                          <div class="col-md-10 col-md-offset-1">
                  <!-- Appointment Form Starts -->
                            <form id="appointment_form" name="appointment_form" class="appointment_form text-center" action="#" method="post" novalidate="novalidate">
                                <div class="messages"></div>
                                <div class="row">
                                    <div class="col-md-12">
                            <h2 class="text-thm2">Appointment with Expert</h2>
                            <p>Let's Appointment With our Exparts Who Try to Solved Your Problem.</p>
                                        <div class="form-group text-left">
                                            <label for="form_name"><i class="fa fa-user-o"></i> Name <small>*</small></label>
                                            <input id="form_name" name="form_name" class="form-control" placeholder="enter a name" required="required" data-error="Name is required." type="text">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group text-left">
                                            <label for="form_email"><i class="fa fa-envelope-open-o"></i> Email <small>*</small></label>
                                            <input id="form_email" name="form_email" class="form-control required email" placeholder="enter an email" required="required" data-error="Email is required." type="email">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group text-left">
                                            <label for="form_date"><i class="fa fa-calendar-check-o"></i> Date <small>*</small></label>
                                            <input id="form_date" name="form_date" class="form-control required datepicker" placeholder="enter a date" required="required" data-error="Subject is required." type="text">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div> 
                                    <div class="col-md-6">
                                        <div class="form-group text-left">
                                            <label for="form_phone"><i class="fa fa-phone"></i> Phone <small>*</small></label>
                                            <input id="form_phone" name="form_phone" class="form-control required" placeholder="enter a phone" required="required" data-error="Phone Number is required." type="text">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>  
                                </div>
                                <div class="form-group text-left">
                                    <label for="form_name"><i class="fa fa-file-text-o"></i> Message</label>
                                    <textarea id="form_message" name="form_message" class="form-control required" rows="5" placeholder="type in a message" required="required" data-error="Message is required."></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group text-center">
                                    <input id="form_botcheck" name="form_botcheck" class="form-control" value="" type="hidden">
                                    <button type="submit" class="btn btn-lg ulockd-btn-thm2" data-loading-text="Getting Few Sec...">Send</button>
                                </div>
                            </form>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <!-- Product Popup View End-->
    </div>
  </section>
