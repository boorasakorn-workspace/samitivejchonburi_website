<!-- BlogHealthy -->

<div class="row">
    <div class="col-md-6 col-md-offset-3 text-center">
      <div class="ulockd-main-title">
        <h2 class="mt-separator hidden-xs"><?=$this->lang->line('samitivej_Healthy_Blog');?></h2>
        <h3 class="mt-separator visible-xs"><?=$this->lang->line('samitivej_Healthy_Blog');?></h3>
        <a class="btn btn-md ulockd-btn-thm2" href="<?php echo base_url();?>frontend/Home/HealthyBlog"><?=$this->lang->line('samitivej_See_All');?></a>
      </div>
    </div>
</div>
<div class="row">
    <div class="course-box wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 300ms; animation-name: fadeInUp;">

        <div class="row">
            <div class="fancybox-gallery-slider" style="min-height:100%;">
                <?php foreach ($blog_healthy as $result) : ?>
                    <?php 
                    $b_desc = $result->blog_description;
                    ?>                    
                    <div class="gs-thumb project-box hoverOnZoom" style="background-color: #f9f9f9;" align="center">
                        <div class="pb-thumb">
                            <a href="<?=base_url().'frontend/Home/HealthyblogDetails/'.$result->id;?>">
                                <img class="img-responsive" style="width: 70%;height: 240px;"src="<?=base_url().'gallery/blog/big_imgs/'.$result->blog_big_img;?>" alt="<?=$result->blog_title;?>">
                                <div class="overlay" style="padding-top: 20px;">
                                    <h4 style="font-weight: 540;color:black;padding-left:50px;padding-right: 50px;"><?=$result->blog_title;?>
                                        <?php if(isset($result->doctor_name) && $result->doctor_name != ""){?>
                                            <?=$this->lang->line('samitivej_By')." : ".$result->doctor_name;?>
                                        <?php } ?>
                                    </h4>
                                    <h6 style="font-weight: 540;color:black;padding-left:80px;padding-right: 80px;"><?=$b_desc;?></h6>
                                </div>
                            </a>
                        </div>
                    </div>                    
                <?php endforeach; ?>
            </div>
        </div>

    </div>
</div>