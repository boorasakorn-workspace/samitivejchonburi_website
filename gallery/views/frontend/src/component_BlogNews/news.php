<!-- News And Activity -->

<div class="row">
    <div class="col-md-6 col-md-offset-3 text-center">
      <div class="ulockd-main-title">
        <h2 class="mt-separator hidden-xs"><?=$this->lang->line('samitivej_News_Text');?> <span class="tcolor-green">&</span> <?=$this->lang->line('samitivej_Activity_Text');?></h2>
        <h3 class="mt-separator visible-xs"><?=$this->lang->line('samitivej_News_Text');?> <span class="tcolor-green">&</span> <?=$this->lang->line('samitivej_Activity_Text');?></h3>
        <a class="btn btn-md ulockd-btn-thm2" href="<?php echo base_url();?>frontend/Home/News"><?=$this->lang->line('samitivej_See_All');?></a>
      </div>
    </div>
</div>
<div class="row">
    <div class="course-box wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 300ms; animation-name: fadeInUp;">

        <div class="row">
            <div class="fancybox-gallery-slider" style="min-height:100%;">
                <?php foreach ($new_activity as $result) : ?>
                    <?php 
                    $n_desc = $result->description;
                    $n_desc = strip_tags($n_desc);
                    $n_desc = character_limiter($n_desc,160);
                    ?>                    
                    <div class="gs-thumb project-box hoverOnZoom" style="background-color: #f9f9f9;" align="center">
                        <div class="pb-thumb">
                            <a href="<?=base_url().'frontend/Home/NewsDetails/'.$result->new_activity_id;?>">                            
                              <img class="img-responsive" style="width: 70%;height: 240px;"src="<?=base_url().'gallery/news/big_imgs/'.$result->new_activity_big_img;?>" alt="<?=$result->new_activity_name;?>">
                              <div class="overlay" style="padding-top: 20px;">
                                  <h4 style="font-weight: 540;color:black;padding-left:50px;padding-right: 50px;"><?=$result->new_activity_name;?></h4>
                                  <ul class="list-inline course-feature">
                                    <?php if(isset($result->acc_firstname) && $result->acc_firstname != NULL){ ?>
                                      <li><a><i class="fa fa-user text-thm2"></i> By: <?php echo $result->acc_firstname;?></a></li>
                                    <?php } ?>
                                    <?php if(isset($result->cwhen) && $result->cwhen != NULL){ ?>
                                      <li><a><i class="fa fa-calendar text-thm2"></i> <?php echo $result->cwhen;?></a></li>
                                    <?php } ?>
                                  </ul>
                                  <h6 style="font-weight: 540;color:black;padding-left:80px;padding-right: 80px;"><?=$n_desc;?></h6>
                              </div>
                            </a>
                        </div>
                    </div>                    
                <?php endforeach; ?>
            </div>
        </div>

    </div>
</div>