<section class="bgc-darkgold1">
	<div class="container">
		<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center">
		          <div class="ulockd-main-title">
		            <h2 class="mt-separator"><?=$query->sub_nav_title; ?></h2>
		          </div>
		        </div>
			</div>
		<div class="row">
			<div class="col-md-4 col-lg-3 ulockd-pdng0 desktop">
				<div class="ulockd-faq-content">
					<?php
						$this->view('frontend/src/sidemenu/sidemenu');
					?>

	                </div>
					
		        </div>


			<div class="col-md-8 col-lg-9">
				<div class="row">					
					<div class="col-md-12 ulockd-mrgn1210">
						<?=html_entity_decode($query->sub_nav_content); ?>
					</div> 
				</div>
			</div>
		</div>
	</div>
</section>