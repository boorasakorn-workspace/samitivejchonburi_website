<section class="bgc-darkgold1">
	<div class="container">
		<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center">
		          <div class="ulockd-main-title">
		            <h2 class="mt-separator"><?=$this->lang->line('samitivej_Service');?></h2>
		          </div>
		        </div>
			</div>
		<div class="row">
			<div class="col-md-4 col-lg-3 ulockd-pdng0 desktop">
				<div class="ulockd-faq-content">
					<?php
						$this->view('frontend/src/sidemenu/sidemenu');
					?>

	                </div>
					
		        </div>
					

			<div class="col-md-8 col-lg-9">
		      <div class="row">
		      <?php
		        $i=0;
		        foreach ($medical_center as $medical_center) :
		       	?>
		          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 wow fadeInUp"  data-wow-duration="1s">
		            <div class="img-box1 text-center" align="center">
		              <div class="thumb">
		                <img class="img-responsive" src="<?=base_url();?>gallery/medical_center/icon/<?php echo $medical_center->medical_center_icon;?>" alt="<?php echo $medical_center->medical_center_name;?>">
		              </div>
		              <div class="ibox-details">
		                <a href="<?=base_url();?>frontend/Home/ServiceDetails/<?php echo $medical_center->id;?>"><p class="f-size"><?php echo $medical_center->medical_center_name;?></p></a>
		              </div>
		            </div>
		          </div>
		        <?php
		        $i++;
		        if($i%3 == 0){
		          echo '</div><br><div class="row">';
		        }
		        endforeach;
		      ?>
	            <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 wow fadeInUp"  data-wow-duration="1.6s">
	              <div class="img-box1 text-center" align="center">
	                <div class="thumb">
	                  <img class="img-responsive" src="<?=base_url();?>assets_f/images/icon_f/call.png" alt="call.png">
	                </div>
	                <div class="ibox-details">
	                  <a href="https://selfservice.samitivejchonburi.com/"><p class="f-size">ลงทะเบียน</p></a>
	                </div>
	              </div>
	            </div>';
		      </div>
			</div>			
		</div>
	</div>
</section>