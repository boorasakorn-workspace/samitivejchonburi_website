  <section class="service-two" style="padding :0;">
    <!-- <div class="container"> -->

      <div id="trending-items" class="section-container bg-silver">
        <!-- BEGIN container -->
        <!-- <div class="container"> -->

          <a href="<?php echo base_url();?>frontend/Home/Service">
              <div class="service-button text-center">
                <?=$this->lang->line('samitivej_Service');?>
              </div>
          </a>

          <div class="course-box wow fadeInUp" data-wow-duration="1s" style="background-color: #f0f0f0; visibility: visible; animation-duration: 300ms; animation-name: fadeInUp;">
            <!-- BEGIN row -->
            <div class="row" style="background-color:rgba(0,128,81,1);border-top-width: 5px;border-color: rgba(0,128,81,1);padding-bottom:10px;">
              <div class="testimonial-class">

              <?php foreach ($medical_center as $medical_center) : ?>

              <!-- BEGIN col-2 -->
              <div style="background-color:rgba(0,128,81,1); padding-top: 0px;padding-bottom: 0px; padding-left: 1px;padding-right: 1px;">
                <!-- BEGIN item -->
                <div class="item item-thumbnail" style="background-color:rgba(255,255,255,1);" align="center">
                  <a href="<?=base_url().'frontend/Home/ServiceDetails/'.$medical_center->id;?>" class="item-image">
                    <img style="width:18%;" src="<?=base_url().'gallery/medical_center/icon/'.$medical_center->medical_center_icon;?>" alt="<?=$medical_center->medical_center_name;?>" />
                  </a>
                  <p class="medical_center_description"><?=$medical_center->medical_center_name;?></p>
                </div>
                <!-- END item -->
              </div>
              <!-- END col-2 -->

              <?php endforeach; ?>

                <!-- BEGIN col-2 -->
                <div style="background-color:rgba(0,128,81,1); padding-top: 0px;padding-bottom: 1px; padding-left: 1px;padding-right: 1px;">
                  <!-- BEGIN item -->
                  <div class="item item-thumbnail" style="background-color:rgba(255,255,255,1);" align="center">
                    <a href="<?=base_url().'frontend/Home/ServiceDetails/'.$medical_center->id;?>" class="item-image">
                      <img style="width:18%;" src="<?=base_url().'assets_f/images/icon_f/call.png';?>" alt="Self Register" />
                    </a>
                    <p class="medical_center_description">Self Register</p>
                  </div>
                  <!-- END item -->
                </div>
                <!-- END col-2 -->

              </div>
            </div>
            <!-- END row -->
          </div>

        <!-- </div> -->
        <!-- END container -->
      </div>
    <!-- </div> -->   
  </section>