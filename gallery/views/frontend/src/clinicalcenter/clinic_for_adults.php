<section  class="bgc-darkgold1">
	<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center">
					<div class="ulockd-main-title">
						<h2 class="mt-separator">ศูนย์ และคลินิกผู้ใหญ่</h2>
						<p>
							<form action="t" method="get" class="form-group" accept-charset="utf-8">
                        	<div class="row">
	                        	<div class="col-md-6">
	                        	 <div class="form-group text-left">
	                        	 	<input id="name-doctor" name="name-doctor" class="form-control border-r bgc" placeholder="ชื่อศูนย์" data-error="Name is required." type="text">
	                        	 	 <div class="help-block with-errors"></div>
	                        	 </div>
	                        	</div>
	                        	<div class="col-md-6">
                        			<div class="form-group text-left">
                        				<select name="hospital" class="form-control border-r bgc">
                        					<option value="all"> ทุกโรงพยาบาล</option>
                        					<option value="1"> สมิติเวช สุขุมวิท</option>
                        					<option value="2"> สมิติเวช ศรีนครินทร์</option>
                        					<option value="3"> สมิติเวช ศรีราชา</option>
                        					<option value="4"> สมิติเวช ธนบุรี</option>
                        					<option value="6" selected="selected"> สมิติเวช ชลบุรี</option>
                        					<option value="7"> โรงพยาบาลเด็กสมิติเวช</option> 
                        				</select>
                        			</div>
                        		</div>
                        	</div>
                        	<div class="row">
                        		<div class="col-xxs-12 col-xs-6 col-md-6 col-sm-push-1">
                        			<button type="reset" class="btn btn-sm ulockd-btn-thm2 block">รีเช็ต</button>
                        		</div>
                        		<div class="col-xxs-12 col-xs-6 col-md-6 col-sm-pull-1">
                        			<button type="submit" class="btn btn-sm ulockd-btn-thm2 block">กรองผลการค้นหา</button>                        		
                        		</div>
                        		
                        	</div>
                        </form>
						</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-6">
					<div class="ficon-box hvr-shadow">
						<div class="row">
							<div class="col-md-5">
								<img class="img-responsive img-whp" src="<?php echo base_url();?>assets_f/images/clinic/1.jpg" alt="1.jpg">
							</div>
							<div class="col-md-7">
								<div class="fib-details">
								<p class="f-size1">คลินิกจิตเวช</p>
								<p>บริการด้านต่าง ๆ แก่ผู้ป่วยตั้งแต่ผู้ป่วยจากโรคจิตเภทและอาการคุ้มคลั่ง สู่อาการซึมเศร้าและความแปรปรวนทางจิต</p>
								<a href="#">ข้อมูลเพิ่มเติม <i class="fa fa-angle-double-right"></i></a>
							</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-6">
					<div class="ficon-box hvr-shadow">
						<div class="row">
							<div class="col-md-5">
								<img class="img-responsive img-whp" src="<?php echo base_url();?>assets_f/images/clinic/2.jpg" alt="2.jpg">
							</div>
							<div class="col-md-7">
								<div class="fib-details">
								<p class="f-size1">คลินิกทันตกรรม</p>
								<p>คลินิกทันตกรรมของเราได้รับความไว้วางใจสูงสุดในประเทศไทย และมีความเฉพาะทางในการให้บริการทั้งคนไทยและต่างชาติ</p>
								<a href="#">ข้อมูลเพิ่มเติม <i class="fa fa-angle-double-right"></i></a>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div><br>
			<div class="row">
				<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-6">
					<div class="ficon-box hvr-shadow">
						<div class="row">
							<div class="col-md-5">
								<img class="img-responsive img-whp" src="<?php echo base_url();?>assets_f/images/clinic/3.jpg" alt="3.jpg">
							</div>
							<div class="col-md-7">
								<div class="fib-details">
								<p class="f-size1">คลินิกหู คอ และจมูก</p>
								<p>คลินิกหู คอ และจมูกที่โรงพยาบาลสมิติเวช ให้บริการวินิจฉัยและรักษาโรคทางหูคอและจมูกที่เกิดขึ้น เราได้รับการรับรองคุณภาพจาก JCI </p>
								<a href="#">ข้อมูลเพิ่มเติม <i class="fa fa-angle-double-right"></i></a>
							</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-6">
					<div class="ficon-box hvr-shadow">
						<div class="row">
							<div class="col-md-5">
								<img class="img-responsive img-whp" src="<?php echo base_url();?>assets_f/images/clinic/3.jpg" alt="3.jpg">
							</div>
							<div class="col-md-7">
								<div class="fib-details">
								<p class="f-size1">คลินิกหู คอ และจมูก</p>
								<p>คลินิกหู คอ และจมูกที่โรงพยาบาลสมิติเวช ให้บริการวินิจฉัยและรักษาโรคทางหูคอและจมูกที่เกิดขึ้น เราได้รับการรับรองคุณภาพจาก JCI </p>
								<a href="#">ข้อมูลเพิ่มเติม <i class="fa fa-angle-double-right"></i></a>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div><br>
			<div class="row">
				<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-6">
					<div class="ficon-box hvr-shadow">
						<div class="row">
							<div class="col-md-5">
								<img class="img-responsive img-whp" src="<?php echo base_url();?>assets_f/images/clinic/1.jpg" alt="1.jpg">
							</div>
							<div class="col-md-7">
								<div class="fib-details">
								<p class="f-size1">คลินิกจิตเวช</p>
								<p>บริการด้านต่าง ๆ แก่ผู้ป่วยตั้งแต่ผู้ป่วยจากโรคจิตเภทและอาการคุ้มคลั่ง สู่อาการซึมเศร้าและความแปรปรวนทางจิต</p>
								<a href="#">ข้อมูลเพิ่มเติม <i class="fa fa-angle-double-right"></i></a>
							</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-6">
					<div class="ficon-box hvr-shadow">
						<div class="row">
							<div class="col-md-5">
								<img class="img-responsive img-whp" src="<?php echo base_url();?>assets_f/images/clinic/1.jpg" alt="1.jpg">
							</div>
							<div class="col-md-7">
								<div class="fib-details">
								<p class="f-size1">คลินิกจิตเวช</p>
								<p>บริการด้านต่าง ๆ แก่ผู้ป่วยตั้งแต่ผู้ป่วยจากโรคจิตเภทและอาการคุ้มคลั่ง สู่อาการซึมเศร้าและความแปรปรวนทางจิต</p>
								<a href="#">ข้อมูลเพิ่มเติม <i class="fa fa-angle-double-right"></i></a>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div><br>
			<div class="row">
				<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-6">
					<div class="ficon-box hvr-shadow">
						<div class="row">
							<div class="col-md-5">
								<img class="img-responsive img-whp" src="<?php echo base_url();?>assets_f/images/clinic/1.jpg" alt="1.jpg">
							</div>
							<div class="col-md-7">
								<div class="fib-details">
								<p class="f-size1">คลินิกจิตเวช</p>
								<p>บริการด้านต่าง ๆ แก่ผู้ป่วยตั้งแต่ผู้ป่วยจากโรคจิตเภทและอาการคุ้มคลั่ง สู่อาการซึมเศร้าและความแปรปรวนทางจิต</p>
								<a href="#">ข้อมูลเพิ่มเติม <i class="fa fa-angle-double-right"></i></a>
							</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-6">
					<div class="ficon-box hvr-shadow">
						<div class="row">
							<div class="col-md-5">
								<img class="img-responsive img-whp" src="<?php echo base_url();?>assets_f/images/clinic/1.jpg" alt="1.jpg">
							</div>
							<div class="col-md-7">
								<div class="fib-details">
								<p class="f-size1">คลินิกจิตเวช</p>
								<p>บริการด้านต่าง ๆ แก่ผู้ป่วยตั้งแต่ผู้ป่วยจากโรคจิตเภทและอาการคุ้มคลั่ง สู่อาการซึมเศร้าและความแปรปรวนทางจิต</p>
								<a href="#">ข้อมูลเพิ่มเติม <i class="fa fa-angle-double-right"></i></a>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div><br>
			<div class="col-lg-12 col-md-offset-4">
			<nav aria-label="Page navigation navigation-lg">
			    <ul class="pagination pagination1">
			    	<li>
			        	<a href="#" aria-label="Previous">
		     		   		<span aria-hidden="true">ก่อนหน้า</span>
			        	</a>
					</li>
			    	<li class="active"><a href="#">1</a></li>
			    	<li><a href="#">2</a></li>
			    	<li><a href="#">3</a></li>
			    	<li><a href="#">4</a></li>
			    	<li><a href="#">5</a></li>
			    	<li>
			        	<a href="#" aria-label="Next">
			        		<span aria-hidden="true">ถัดไป</span>
			           	</a>
			    	</li>
			    </ul>
			</nav>
		</div>
		</div>
</section>