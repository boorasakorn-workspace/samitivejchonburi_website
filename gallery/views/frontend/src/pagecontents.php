
<link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">

<section class="ulockd-about">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="about-box">
            <!-- <h1 class="title-bottom ulockd-mrgn620 ulockd-mrgn120">Welcome To <span class="text-cdarkg">Samitivej Chonburi hospital</span></h1><br> -->
            <div class="ulockd-main-title text-center">
                <h2 class="mt-separator">Contents <span class="tcolor-green">&</span> Promotion</h2>
                <p><a class="btn btn-lg ulockd-btn-thm2" href="<?php echo base_url();?>frontend/Home/Promotion">ดูทั้งหมด</a></p>
            </div>
            <div class="row">


             <?php foreach ($get_contents as $result): ?>
                <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 hvr-float-shadow">
                    <div class="pricing_table ">
                        <div class="pricing_table_header ulockd-bghthm">
                            <h3 style="color:gray;" class="title"><?php echo $result->contents_name;?></h3>
                            <img class="img-responsive" style="height:220px; width:100%;" src="<?php echo base_url(); ?>gallery/contents/small_imgs/<?php echo $result->contents_small_img; ?>" alt="<?php echo $result->contents_big_img; ?>">
                            <span style="color:gray;" class="price-value">฿ <?php echo $result->contents_unitprice;?>
                                <span style="color:gray;" class="month">ราคาเริ่มต้น</span>
                            </span>
                        </div>
                        <div class="pricing-content">
                            <ul>
                                <li>ราคาแพ็กเกจรวมค่าแพทย์และค่าบริการโรงพยาบาลแล้ว</li>
                                <li>ใช้บริการที่โรงพยาบาลสมิติเวชชลบุรีเท่านั้น</li>
                            </ul>
                            <div class="pricing_table_signup">
                                <a href="<?php echo base_url();?>frontend/Home/PromotionDetails/<?php echo $result->id;?>">รายละเอียดเพิ่มเติม<i class="fa fa-arrow-circle-o-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
    </div>
</div>
</div>
</section>