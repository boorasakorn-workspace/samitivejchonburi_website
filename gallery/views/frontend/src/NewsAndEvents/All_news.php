<section  class="bgc-darkgold1">
	<div class="container">
		<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center">
		          <div class="ulockd-main-title">
		            <h2 class="mt-se	rator"><?=$this->lang->line('samitivej_News_Text');?> <span class="tcolor-green">&</span> <?=$this->lang->line('samitivej_Activity_Text');?></h2>
		          </div>
		        </div>
			</div>
			<div class="row">
				<div class="col-md-4 col-lg-3 ulockd-pdng0 desktop">
				<div class="ulockd-faq-content">
					<?php
						$this->view('frontend/src/sidemenu/sidemenu');
					?>

	                </div>
					
		        </div>


				<div class="col-md-8 col-lg-9">
				<?php foreach ($query as $result) :?>
				<div class="row">
					<div class="col-md-12 ulockd-mrgn1210">
						<article>
						<div class="col-md-6">
							<div class="ulockd-project-sm-thumb">
								<img class="img-responsive img-whp" src="<?php echo base_url(); ?>gallery/news/small_imgs/<?php echo $result->new_activity_small_img;?>" alt="<?php echo $result->new_activity_small_img;?>">
							</div>
						</div>
						<div class="col-md-6">
							<h3><?php echo $result->new_activity_name;?> </h3>
							<ul class="list-inline course-feature">
									<li><a href="#"><i class="fa fa-calendar text-thm2"></i> <?php echo $result->cwhen;?></a></li>
									<li><a href="#"><i class="fa fa-user text-thm2"></i> By: <?php echo $result->acc_firstname;?></a></li>
									<!-- <li><a href="#"><i class="fa fa-server text-thm2"></i> Industry</a></li> -->
								</ul>
							
							<p class="project-dp-one" style='word-break: break-all; word-wrap: break-word;'>
								<?php echo strip_tags(substr($result->description, 0, 100));
									if(strlen($result->description)>100){
										echo "...";
									}?>
							<a href="<?php echo base_url();?>frontend/Home/NewsDetails/<?php echo $result->new_activity_id;?>"><?=$this->lang->line('samitivej_Read_More');?> <i class="fa fa-angle-double-right"></i></a></p>
						
						</div>
						</article>
					</div>
				</div>
				<?php endforeach;?>

				<div class="col-lg-12 col-md-offset-4">
			<nav aria-label="Page navigation navigation-lg">
			    <ul class="pagination pagination1">
			    	<li>
			        	<a href="#" aria-label="Previous">
		     		   		<span aria-hidden="true"><?=$this->lang->line('samitivej_Previous_Text');?></span>
			        	</a>
					</li>
			    	<li class="active"><a href="#">1</a></li>
			    	<li><a href="#">2</a></li>
			    	<li><a href="#">3</a></li>
			    	<li><a href="#">4</a></li>
			    	<li><a href="#">5</a></li>
			    	<li>
			        	<a href="#" aria-label="Next">
			        		<span aria-hidden="true"><?=$this->lang->line('samitivej_Next_Text');?></span>
			           	</a>
			    	</li>
			    </ul>
			</nav>
		</div>
				</div>
			</div>
			</div>

			
		</div>
</section>