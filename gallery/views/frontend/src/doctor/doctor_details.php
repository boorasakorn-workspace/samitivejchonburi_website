<?php
	$pname = $query->pname_th;
	$firstname = $query->firstname_th;
	$lastname = $query->lastname_th;
	//$speciality = $query->speciality_th;
	$full = $pname.' '.$firstname.' '.$lastname;
	$education = $query->education_th;
	if(isset($query->id)){
		$doc_id = $query->id;
	}

	$doctor_med_center = '';
	$doctor_parent_medical_center_id = explode('|',$query->parent_medical_center_id);
	for($i=0;$i<count($doctor_parent_medical_center_id);$i++){
	  foreach($all_med_doctor as $med_center){
	    if($doctor_parent_medical_center_id[$i] == $med_center->id){
	      $doctor_med_center .= $med_center->medical_center_name . '<br>';
	    }
	    elseif(!is_numeric($doctor_parent_medical_center_id[$i]) && $doctor_parent_medical_center_id[$i] != ''){
	      $doctor_med_center .= $doctor_parent_medical_center_id[$i] . '<br>';
	    }
	  }
	}
	$doctor_med_center = implode(' ',array_unique(explode(' ', $doctor_med_center)));

	$doctor_specialty = '';
	$doctor_parent_specialty_id = explode('|',$query->parent_specialty_id);
	for($i=0;$i<count($doctor_parent_specialty_id);$i++){
	  foreach($all_specialty as $med_specialty){
	    if($doctor_parent_specialty_id[$i] == $med_specialty->id){
	      $doctor_specialty .= $med_specialty->specialty_name . ' ';
	    }
	    elseif(!is_numeric($doctor_parent_specialty_id[$i]) && $doctor_parent_specialty_id[$i] != ''){
	      $doctor_specialty .= $doctor_parent_specialty_id[$i] . ' ';
	    }
	  }
	}
	$doctor_specialty = implode(' ',array_unique(explode(' ', $doctor_specialty)));

	$doctor_sub_specialty = '';
	$doctor_parent_sub_specialty_id = explode('|',$query->parent_sub_specialty_id);
	for($i=0;$i<count($doctor_parent_sub_specialty_id);$i++){
	  foreach($all_sub_specialty as $med_sub_specialty){
	    if($doctor_parent_sub_specialty_id[$i] == $med_sub_specialty->id){
	      $doctor_sub_specialty .= $med_sub_specialty->sub_specialty_name . ' ';
	    }
	    elseif(!is_numeric($doctor_parent_sub_specialty_id[$i]) && $doctor_parent_sub_specialty_id[$i] != ''){
	      $doctor_sub_specialty .= $doctor_parent_sub_specialty_id[$i] . ' ';
	    }
	  }
	}
	$doctor_sub_specialty = implode(' ',array_unique(explode(' ', $doctor_sub_specialty)));
		
?>
<section  class="bgc-darkgold1">
	<div class="ulockd-divider1 ulockd_bgp4">
		<div class="container">
			<h2 class="mt-separator text-center" style="color: #006640;"><?=$this->lang->line('samitivej_Doctor_Info');?></h2>
		</div>
	</div><br>
	<div class="container">
			<div class="row">
				<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4">
					<div class="team-one style2">
						<div class="team-thumb">
							<?php 
							if(isset($query->doctor_imgs_pre) && $query->doctor_imgs_pre != ''){
							?>
							<img class="img-responsive img-whp" src="<?php echo base_url(); ?>gallery/doctor/preview_imgs/<?php echo $query->doctor_imgs_pre; ?>" >
							<?php
							}elseif(isset($query->doctor_imgs) && $query->doctor_imgs != ''){
							?>
							<img class="img-responsive img-whp" src="<?php echo base_url(); ?>gallery/doctor/big_imgs/<?php echo $query->doctor_imgs; ?>" alt="<?php echo $query->doctor_imgs; ?>">
							<?php 
							}else{
							?>
							<img class="img-responsive img-whp" src="<?php echo base_url(); ?>gallery/doctor/NoPic.png">							
							<?php 
							}
							?>

						</div>
						<div class="team-details text-left">
							<p class="member-name f-size1"><?php echo $full; ?></p>
							<p class="member-post"><?php echo $doctor_med_center; ?></p>
							<ul class="list-inline team-icon">	
								<li><a href="mailto:<?php echo $query->email; ?>"><i class="fa fa-envelope"></i> <?=$this->lang->line('samitivej_Email');?></a></li>						
	  							<!-- <li id="btn_appoint"><a class="btn btn-lg appoint_btn text-center" data-toggle="modal" href="<?=base_url().'frontend/Home/doctorAppointment';?>"><?=$this->lang->line('samitivej_Request_an_Appointment');?></a></li> -->
	  							<!-- 
	  							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
	  							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
	  							<li><a href="#"><i class="fa fa-skype"></i></a></li>
	  							<li><a href="mailto:name@email.com"><i class="fa fa-envelope"></i> Email</a></li>	  
	  							<li id="btn_appoint"><a class="btn btn-lg appoint_btn text-center" data-toggle="modal" href="#appointment_view">นัดหมายแพทย์</a></li> 
	  							-->
	  						</ul>
							
						</div>
					</div>
					<!-- 
					<h3>วีดีโอแนะนำแพทย์</h3>
				   	   <ul class="list-unstyled">
				       		<li><iframe class="h200" src="http://www.youtube.com/embed/qKoVto0Wegg?autoplay=0" allowfullscreen=""></iframe></li>
			           </ul> 
			       	-->
			       	<?php 
			       		$doc_calendar_gen = 'frontend/src/doctor/doctor_calendar_gen';
			       		$this->view($doc_calendar_gen);
			       		$this->view('frontend/src/doctor/work_table');
			       	?>
				</div>

				<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-8">
				    <div class="thumbnail ">
				        <div class="caption">
							<ul class="list-unstyled ulockd-pesonar-info">
								<li><strong><?=$this->lang->line('samitivej_Fullname');?> :</strong> <?php echo $full; ?></li>
								<?php if(isset($doctor_specialty) && $doctor_specialty != ''){ ?>
								<li><strong><?=$this->lang->line('samitivej_Speciality');?> :</strong> <?php echo $doctor_specialty; ?></li>
								<?php } if(isset($doctor_specialty) && $doctor_sub_specialty != ''){ ?>
								<li><strong><?=$this->lang->line('samitivej_Sub_Speciality');?> :</strong> <?php echo $doctor_sub_specialty; ?>
								<?php } ?>
								<hr>
								<li><strong><?=$this->lang->line('samitivej_Education');?> :</strong>
									<p><?php echo html_entity_decode($education); ?></p>
								</li>
							</ul><hr>
							<?php /*
							<ul class="list-inline">
								<li><strong><?=$this->lang->line('samitivej_Tel_no');?> :</strong><?php echo $query->phone; ?></li>
							</ul> */ ?>
							<?php /*
							<ul class="list-unstyled ulockd-pesonar-info">
								<li><strong><?=$this->lang->line('samitivej_Email');?> :</strong><?php echo $query->email; ?></li>
							</ul> <hr> */ ?>
							<p><strong><?=$this->lang->line('samitivej_Doctor_Description');?> :</strong><?php echo html_entity_decode($query->doctor_description)?></p>							
							<?php 
								if(isset($query->doctor_video) && $query->doctor_video != ""){
									echo "<hr>";
									if (strpos($query->doctor_video, 'www.youtube.com') !== false) {
										$yt_link = explode("watch?v=",$query->doctor_video); 
							?>
										<iframe width="100%" height="320" src="https://www.youtube.com/embed/<?=$yt_link[1];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							<?php
									}elseif (strpos($query->doctor_video, 'youtu.be') !== false) {
										$yt_link = explode("youtu.be/",$query->doctor_video); 
							?>
										<iframe width="100%" height="320" src="https://www.youtube.com/embed/<?=$yt_link[1];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							<?php
									}
									else{
							?>
										<video style="height:320px; width:100%;" src="<?=base_url('gallery/doctor/videos/').$query->doctor_video;?>" controls></video>
							<?php
									}
								}
							?>
				        </div>
				    </div>
				    <?php if(isset($query_workdayhour) && $query_workdayhour != NULL){?>
				     <?php $this->view('frontend/src/doctor/work_table'); ?> 
				    
				    <!-- <table class="table ulockd-one text-center hidden-xs hidden-sm hidden-md hidden-lg hidden-xl" style="border:none;"> -->
				    <table class="table ulockd-one text-center" style="border:none;">
						<caption><h3><?=$this->lang->line('samitivej_Doctor_Appointment_Table');?></h3></caption> 
						<thead> 
							<tr class="bgc"> 
								<th><?=$this->lang->line('samitivej_Day_Appointment_Table');?></th> 
								<th class="ulockd-hour"><?=$this->lang->line('samitivej_Hour_Appointment_Table');?></th>
							</tr> 
						</thead> 
						<tbody>
							<?php 
								$doc_workdays = ''; 
								$doc_worktimes = ''; 
							?>
							<?php foreach ($query_workdayhour as $workday) :?>
							<tr>
								<th scope="row"> 
								<?php
									if (isset($workday->work_days) && !empty($workday->work_days) && strpos($workday->work_days, '-') !== false) {
										$days = explode(" - ", $workday->work_days);
										echo $this->lang->line('samitivej_day_'.$days[0]).' - '.$this->lang->line('samitivej_day_'.$days[1]);
										if($days[0] == 'วันจันทร์' || $days[0] == 'จันทร์' || $days[1] == 'วันจันทร์' || $days[1] == 'จันทร์' ){
											$doc_workdays .= 'Monday ';
										}
										if($days[0] == 'วันอังคาร' || $days[0] == 'อังคาร' || $days[1] == 'วันอังคาร' || $days[1] == 'อังคาร' ){
											$doc_workdays .= 'Tuesday ';
										}
										if($days[0] == 'วันพุธ' || $days[0] == 'พุธ' || $days[1] == 'วันพุธ' || $days[1] == 'พุธ' ){
											$doc_workdays .= 'Wednesday ';
										}
										if($days[0] == 'วันพฤหัสบดี' || $days[0] == 'พฤหัสบดี' || $days[1] == 'วันพฤหัสบดี' || $days[1] == 'พฤหัสบดี' ){
											$doc_workdays .= 'Thursday ';
										}
										if($days[0] == 'วันศุกร์' || $days[0] == 'ศุกร์' || $days[1] == 'วันศุกร์' || $days[1] == 'ศุกร์' ){
											$doc_workdays .= 'Friday ';
										}
										if($days[0] == 'วันเสาร์' || $days[0] == 'เสาร์' || $days[1] == 'วันเสาร์' || $days[1] == 'เสาร์' ){
											$doc_workdays .= 'Saturday ';
										}
										if($days[0] == 'วันอาทิตย์' || $days[0] == 'อาทิตย์' || $days[1] == 'วันอาทิตย์' || $days[1] == 'อาทิตย์' ){
											$doc_workdays .= 'Sunday ';
										}
										$doc_worktimes .= $workday->work_times_in." - ".$workday->work_times_out."_";
										$doc_worktimes .= $workday->work_times_in." - ".$workday->work_times_out."_";
									}
									else if(isset($workday->work_days) && !empty($workday->work_days)){
										echo $this->lang->line('samitivej_day_'.$workday->work_days);
										if($workday->work_days == 'วันจันทร์' || $workday->work_days == 'จันทร์'){
											$doc_workdays .= 'Monday ';
										}
										else if($workday->work_days == 'วันอังคาร' || $workday->work_days == 'อังคาร' ){
											$doc_workdays .= 'Tuesday ';
										}
										else if($workday->work_days == 'วันพุธ' || $workday->work_days == 'พุธ'){
											$doc_workdays .= 'Wednesday ';
										}
										else if($workday->work_days == 'วันพฤหัสบดี' || $workday->work_days == 'พฤหัสบดี'){
											$doc_workdays .= 'Thursday ';
										}
										else if($workday->work_days == 'วันศุกร์' || $workday->work_days == 'ศุกร์'){
											$doc_workdays .= 'Friday ';
										}
										else if($workday->work_days == 'วันเสาร์' || $workday->work_days == 'เสาร์'){
											$doc_workdays .= 'Saturday ';
										}
										else if($workday->work_days == 'วันอาทิตย์' || $workday->work_days == 'อาทิตย์'){
											$doc_workdays .= 'Sunday ';
										}
										$doc_worktimes .= $workday->work_times_in." - ".$workday->work_times_out."_";
									}
								?>
								</th>
								<td><?php echo $workday->work_times_in." - ".$workday->work_times_out;
								?></td>
							</tr>
							<?php endforeach?>
						</tbody> 
					</table>
					<?php }
					if(!empty($doc_workdays)){
					?>
						<input type="hidden" id="doctor_workdays" name="doctor_workdays" value="<?=$doc_workdays;?>">
					<?php
					}if(!empty($doc_worktimes)){
					?>
						<input type="hidden" id="doctor_worktimes" name="doctor_worktimes" value="<?=$doc_worktimes;?>">
					<?php
					}
					?>
				</div>
			</div>
			<div class="row">
				<?php
				if(isset($doc_content) && $doc_content > 0 && !empty($doc_content)){ ?>
					<caption><h3 style="color:black;text-align:center;"><?=$this->lang->line('samitivej_Healthy_Blog').' '.$this->lang->line('samitivej_Of').' '.$full;?></h3></caption>
					<?php foreach ($doc_content as $doc_content): ?>
					<div class="col-xxs-4 col-xs-2 col-sm-2 col-md-2">
						<div class="project-box text-center">
				            <a href="<?php echo base_url();?>frontend/Home/PromotionDetails/<?php echo $doc_content->id;?>">
				                <center><h5 style="color:black;font-weight: 550;"> <?php echo $this->lang->line('samitivej_Healthy_Blog') . ' : ' . $doc_content->blog_title;?></h5></center>
				            </a> 
							<figure class="department_box imghvr-push-down">
					            <img class="img-responsive" style="height:150px; width:320px;" src="<?php echo base_url(); ?>gallery/blog/big_imgs/<?php echo $doc_content->blog_big_img;?>" alt="<?php echo $doc_content->blog_title;?>">
					            <a href="<?php echo base_url();?>frontend/Home/PromotionDetails/<?php echo $doc_content->id;?>">
					              <figcaption>
					                <h4 style="color:white;"> <?php echo $doc_content->blog_title;?></h4>
					                <div style="color:white;" class="healty-description"><?php echo $doc_content->blog_title;?>
					                </div>
					              </figcaption>
					            </a> 
					        </figure>
				    	</div>
				    </div>

					<?php endforeach; ?>
				<?php } ?>
			</div>
			<div class="row ulockd_bgc_f7 ulockd-bttc">
				
			</div>
	</div>
			
			<!-- Popup appointments-->
			<div class="modal fade appointment_view" id="appointment_view">
			    <div class="modal-dialog">
			        <div class="modal-content bgc-darkgold1">
			            <div class="modal-header head-modal text-center">
			                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			                <h3 class="modal-title text-center color-white"><span class="flaticon-lock"></span> <?=$this->lang->line('samitivej_Request_an_Appointment');?></h3>
			            </div>
			            <div class="modal-body">
			                <div class="row">
			                    <div class="col-md-10 col-md-offset-1">
									<!-- Appointment Form Starts -->
				                    <form id="appointment_form" name="appointment_form" class=" text-center" action="<?php echo base_url('frontend/Home/Appointment_Email_Send');?>" method="post" novalidate="novalidate">
            							<div class="row text-left"><p class="f-size1">- <?=$this->lang->line('samitivej_Patient_Info_Title');?> -</p></div>
			                        	<div class="row">
			                        		<div class="col-md-3">
				                                <div class="form-group text-left">
				                                	<input type="hidden" id="appointment_title_text" name="appointment_title_text" value="<?=$this->lang->line('samitivej_Request_an_Appointment');?>">
				                    				<input type="hidden" id="doc_id" name="doc_id" value="<?=$doc_id;?>">
				                    				<input type="hidden" id="doc_fullname" name="doc_fullname" value="<?=$full;?>">
				                                    <label for="form_name"><i class="fa fa-user-o"></i> <?=$this->lang->line('samitivej_Firstname');?> <small>*</small></label>
				                                    <input id="firstname" name="firstname" class="form-control bgc" placeholder="<?=$this->lang->line('samitivej_Firstname');?>" required="required" data-error="Name is required." type="text">
				                                    <div class="help-block with-errors"></div>
				                                </div>
				                            </div>
				                            <div class="col-md-3">
				                                <div class="form-group text-left">
				                                    <label for="form_name"> <?=$this->lang->line('samitivej_Lastname');?> <small>*</small></label>
				                                    <input id="lastname" name="lastname" class="form-control bgc" placeholder="<?=$this->lang->line('samitivej_Lastname');?>" required="required" data-error="Name is required." type="text">
				                                    <div class="help-block with-errors"></div>
				                                </div>
				                            </div>
				                            <div class="col-md-3">
				                            	<div class="form-group text-left">
				                            		<label for="form_name"> <?=$this->lang->line('samitivej_Birthday');?><small>*</small></label>
				                            		<input type="date" name="birthday" id="datetimepicker2" class="form-control bgc">
				                            	</div>
				                            </div>
				                            <div class="col-md-3">
				                                <div class="form-group text-left">
					                        	 	<label for="form_email"><i class="icon-Bisexual"></i> <?=$this->lang->line('samitivej_Doctor_Gender');?></label>
					                        	 	<select name="gender" class="form-control  bgc">
					                        	 		<option value="0" selected="selected">- <?=$this->lang->line('samitivej_Doctor_Select_Gender');?> -</option>
					                        	 		<option value="Male"><?=$this->lang->line('samitivej_Doctor_Gender_Male');?></option>
					                        	 		<option value="Female"><?=$this->lang->line('samitivej_Doctor_Gender_Female');?></option>
					                        	 	</select>
				                            	</div>
				                       		</div>
		                    			</div>
				<!-- rows 2 -->
										<div class="row">
											<div class="col-md-6">
						                        <div class="form-group text-left">
						                            <label for="form_email"><i class="fa fa-envelope-open-o"></i> <?=$this->lang->line('samitivej_Email');?> <small>*</small></label>
						                            <input id="form_email" name="form_email" class="form-control required email bgc" placeholder="enter an email" required="required" data-error="Email is required." type="email">
						                            <div class="help-block with-errors"></div>
						                        </div>
						                	</div>
						                	<div class="col-md-6">
						                        <div class="form-group text-left">
						                            <label for="form_phone"><i class="fa fa-phone"></i> <?=$this->lang->line('samitivej_Tel_no');?> <small>*</small></label>
						                            <input id="form_phone" name="form_phone" class="form-control required bgc" placeholder="enter a phone" required="required" data-error="Phone Number is required." type="text">
						                            <div class="help-block with-errors"></div>
						                        </div>
						                    </div>  
										</div>
						        		<hr>
										<div class="row text-left"><p class="f-size1">- <?=$this->lang->line('samitivej_Appointment_Info');?> -</p></div>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group text-left">
						                			<label for="form_name"><i class="fa fa-file-text-o"></i> <?=$this->lang->line('samitivej_Describe_medical_problem');?></label>
						                			<textarea id="form_message" name="form_message" class="form-control bgc required" rows="6" placeholder="<?=$this->lang->line('samitivej_Describe_medical_problem');?>" required="required" data-error="Message is required." style="resize:vertical;"></textarea>
						                			<div class="help-block with-errors"></div>
						        				</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
						                		<div class="form-group text-left">
						                    		<label for="form_date"><i class="fa fa-calendar-check-o"></i> <?=$this->lang->line('samitivej_Appointment_Date_Select');?></label>
						                    		<input type="time" id="appointment_time" name="appointment_time" class="form-control required datepicker bgc" placeholder="เลือกเวลานัด"  data-error="Subject is required." type="text">
						                    		<input type="hidden" value="" id="datetimepicker" name="form_date" class="form-control required datepicker bgc" placeholder="<?=$this->lang->line('samitivej_Appointment_Date_Select');?>" required="required" data-error="Subject is required.">
						                		</div>
						            		</div>
						            		<div class="col-md-6">
						                		<div class="form-group text-left" id="TestZone">
						                    		<label for="form_date"><i class="fa fa-calendar-check-o"></i> <?=$this->lang->line('samitivej_SecondAppointment_Date_Select');?></label>
													<div rendezvous class="form-control required datepicker bgc" name="appointment_sdate" id="datetimepicker3"></div>
						                		</div>

						           	 		</div> 
										</div>
						        		<div class="row">
						        			<div class="col-md-6 text-left"> 
						        				<label><?=$this->lang->line('samitivej_Be_Patient_Before');?> </label>
						        				<div class="form-group text-left">
						        					<input type="checkbox" name="" id="checkY" onclick="myFunction()">
						        					<label> <?=$this->lang->line('samitivej_Yes');?></label>
						        					<input type="text" value="" id="inText3" name="inText3" class="form-control required bgc" placeholder="เลขประจำตัวผู้ป่วย" style="display: none;">
						        				</div>
						        			</div>
						        			<div class="col-md-6 text-left">
						        				<label><?=$this->lang->line('samitivej_Privacy_Policy');?></label>
						        				<div class="form-group text-left">
						        					<input type="checkbox" name="" id="">
						        					<label> <?=$this->lang->line('Accept_our_privacy_policy');?></label>
						        				</div>
						        			</div>
						       			</div>    
											<hr>
											<div class="row text-left"><p class="f-size1">- <?=$this->lang->line('Subscribe_News_Letter');?> -</p></div>
						        		<div class="row">
						        			<div class="col-md-12">
						        				<div class="form-group text-left">
						        					<input type="checkbox" name="">
						        					<label> <?=$this->lang->line('Confirmed_Subscribe');?></label>
						        				</div>
						        			</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group text-center">
						                    		<input id="form_botcheck" name="form_botcheck" class="form-control" value="" type="hidden">
						                    		<button type="submit" class="btn btn-lg ulockd-btn-thm2" data-loading-text="Getting Few Sec..."><?=$this->lang->line('samitivej_Request_an_Appointment');?></button>
						                		</div>
											</div>
										</div>
									</form>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
			<!-- Product Popup View End-->
		</div>
</section>
<script>
	function myFunction() {
	    var checkb3 = document.getElementById("inText3");
	    if (checkb3.style.display === "block") {
	        checkb3.style.display = "none";
	        
	    } else {
	        checkb3.style.display = "block";
	    }
	}
</script>

<?php /*
<section  class="bgc-darkgold1">
	<div class="ulockd-divider1 ulockd_bgp4">
		<div class="container">
			<h2 class="mt-separator text-center" style="color: #006640;">รายละเอียดแพทย์</h2>
		</div>
	</div><br>
	<div class="container">
			<div class="row">
				<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4">
					<div class="team-one style2">
						<div class="team-thumb">
							<img class="img-responsive img-whp" src="<?php echo base_url(); ?>assets_f/images/doctor/1.jpg" alt="1.jpg">
						</div>
						<div class="team-details text-left">
							<p class="member-name f-size1">พญ. อิสราภรณ์ ธนรัฐศิริวรกุล</p>
							<p class="member-post">สาขาสูติศาสตร์-นรีเวชวิทยา</p>
							<ul class="list-inline team-icon">
	  							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
	  							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
	  							<li><a href="#"><i class="fa fa-skype"></i></a></li>
	  							<li><a href="mailto:name@email.com"><i class="fa fa-envelope"></i> Email</a></li>
	  							<li id="btn_appoint"><a class="btn btn-lg appoint_btn text-center" data-toggle="modal" href="#appointment_view">นัดหมายแพทย์</a></li>
	  						</ul>
							
						</div>
					</div>
					<h3>วีดีโอแนะนำแพทย์</h3>
				   	   <ul class="list-unstyled">
				       		<li><iframe class="h200" src="http://www.youtube.com/embed/qKoVto0Wegg?autoplay=0" allowfullscreen=""></iframe></li>
			           </ul>
				</div>

				<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-8">
				    <div class="thumbnail ">
				        <div class="caption">
							<ul class="list-unstyled ulockd-pesonar-info">
								<li><strong>Name :</strong> พญ. อิสราภรณ์ ธนรัฐศิริวรกุล</li>
								<li><strong>ความชำนาญ :</strong> สาขาสูติศาสตร์-นรีเวชวิทยา อนุสาขาเวชศาสตร์การเจริญพันธุ์</li>
								<hr>
								<li><strong>คุณวุฒิทางการศึกษา :</strong>
									<p>- แพทยศาสตรบัณฑิต คณะแพทยศาสตร์ จุฬาลงกรณ์มหาวิทยาลัย เมื่อปี พ.ศ.2551 <br>
									- วุฒิบัตรผู้เชี่ยวชาญสาขาสูติศาสตร์และนรีเวชวิทยา เมื่อปี พ.ศ. 2556 <br>
									- วุฒิบัติผู้เชี่ยวชาญอนุสาขาเวชศาสตร์การเจริญพันธุ์ เมื่อปี พ.ศ. 2558</p>
								</li>
							</ul><hr>
							<ul class="list-inline">
								<li><strong>Phone :</strong> +61 3 8376 6284.</li>
								<li><strong>Fax :</strong> 865461245.</li>
								
								
							</ul>
							<ul class="list-unstyled ulockd-pesonar-info">
								<li><strong>Mail :</strong> henryJones@hotmail.com .</li>
							</ul><hr>
							<p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain.</p>
							<p>This is Our Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste commodi animi, totam earum harum quibusdam dolor dolore magnam! Temporibus laborum quas sint. Beatae eos odit doloremque repellendus sequi libero vero, eius excepturi, est deleniti dignissimos totam earum iste rerum atque ut voluptatem soluta officiis, necessitatibus. Sapiente doloribus, suscipit inventore sit.</p>
							<p>This is Our Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste commodi animi, totam earum harum quibusdam dolor dolore magnam! Temporibus laborum quas sint.</p>
				        </div>
				    </div>
				    <table class="table ulockd-one text-center" style="border:none;">
							<caption><h3>ตาราง วัน-เวลา นัดหมายแพทย์</h3></caption> 
							<thead> 
								<tr class="bgc"> 
									<th>Day</th> 
									<th class="ulockd-hour">Hour</th>
								</tr> 
							</thead> 
							<tbody>
								<tr> 
									<th scope="row">Mon-Tues</th> 
									<td>9.00 - 22.00</td>
								</tr>
								<tr>
									<th scope="row">Wednesday</th> 
									<td>9.00 - 20.00</td>
								</tr>
								<tr>
									<th scope="row">Thursday</th> 
									<td>9.00 - 21.00</td>
								</tr>
								<tr>
									<th scope="row">Friday</th> 
									<td>9.00 - 19.00</td>
								</tr>
								<tr>
									<th scope="row">Saturday</th> 
									<td>9.00 - 18.00</td>
								</tr>
							</tbody> 
						</table>
				</div>
			</div>
			<div class="row ulockd_bgc_f7 ulockd-bttc">
				
			</div>
	</div>
			
			<!-- Popup appointments-->
			<div class="modal fade appointment_view" id="appointment_view" style="display: none;">
						    <div class="modal-dialog">
						        <div class="modal-content bgc-darkgold1">
						            <div class="modal-header head-modal text-center">
						                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						                <h3 class="modal-title text-center color-white"><span class="flaticon-lock"></span> นัดหมายแพทย์</h3>
						            </div>
						            <div class="modal-body">
						                <div class="row">
						                    <div class="col-md-10 col-md-offset-1">
												<!-- Appointment Form Starts -->
							                    <form id="appointment_form" name="appointment_form" class=" text-center" action="#" method="post" novalidate="novalidate">
                        							<div class="row text-left"><p class="f-size1">- ข้อมูลผู้ป่วย -</p></div>
						                        	<div class="row">
						                        		<div class="col-md-3">
							                                <div class="form-group text-left">
							                                    <label for="form_name"><i class="fa fa-user-o"></i> ชื่อ <small>*</small></label>
							                                    <input id="firstname" name="form_name" class="form-control bgc" placeholder="ชื่อ" required="required" data-error="Name is required." type="text">
							                                    <div class="help-block with-errors"></div>
							                                </div>
							                            </div>
							                            <div class="col-md-3">
							                                <div class="form-group text-left">
							                                    <label for="form_name"> นามสกุล <small>*</small></label>
							                                    <input id="lastname" name="form_name" class="form-control bgc" placeholder="นามสกุล" required="required" data-error="Name is required." type="text">
							                                    <div class="help-block with-errors"></div>
							                                </div>
							                            </div>
							                            <div class="col-md-3">
							                            	<div class="form-group text-left">
							                            		<label for="form_name"> วันเกิด<small>*</small></label>
							                            		<input type="date" id="datetimepicker2" class="form-control bgc">
							                            	</div>
							                            </div>
							                            <div class="col-md-3">
							                                <div class="form-group text-left">
								                        	 	<label for="form_email"><i class="icon-Bisexual"></i> เพศ</label>
								                        	 	<select name="" class="form-control  bgc">
								                        	 		<option value="0" selected="selected">- เลือกเพศ -</option>
								                        	 		<option value="Male">ชาย</option>
								                        	 		<option value="Female">หญิง</option>
								                        	 	</select>
							                            	</div>
							                       		</div>
					                    			</div>
							<!-- rows 2 -->
													<div class="row">
														<div class="col-md-6">
							                                <div class="form-group text-left">
							                                    <label for="form_email"><i class="fa fa-envelope-open-o"></i> Email <small>*</small></label>
							                                    <input id="form_email" name="form_email" class="form-control required email bgc" placeholder="enter an email" required="required" data-error="Email is required." type="email">
							                                    <div class="help-block with-errors"></div>
							                                </div>
						                            	</div>
						                            	<div class="col-md-6">
							                                <div class="form-group text-left">
							                                    <label for="form_phone"><i class="fa fa-phone"></i> เบอร์โทรศัพท์ <small>*</small></label>
							                                    <input id="form_phone" name="form_phone" class="form-control required bgc" placeholder="enter a phone" required="required" data-error="Phone Number is required." type="text">
							                                    <div class="help-block with-errors"></div>
							                                </div>
							                            </div>  
													</div>
						                    		<hr>
	                    							<div class="row text-left"><p class="f-size1">- เลือกแพทย์ -</p></div>
	                    							<div class="row">
						                    			<div class="col-md-6">
						                    	 			<div class="form-group text-left">
						                    	 				<label for="form_email"> สาขาเฉพาะทางของแพทย์ผู้เชี่ยวชาญ <small>*</small></label>
						                    	 				<select name="" id="input_4_11" class="form-control bgc" tabindex="13">
						                    	 					<option value="any">- ไม่ระบุความชำนาญ -</option>
						                    	 					<option value="1">เด็กและวัยรุ่น</option>
						                    	 					<option value="2">ตาและการมองเห็น</option>
						                    	 					<option value="3">สภาวะจิตใจและอารมณ์</option>
						                    	 					<option value="4">ผิวหนัง และเส้นผม</option>
						                    	 					<option value="5">สมองและระบบประสาท</option>
						                    	 					<option value="6">ตรวจและดูแลสุขภาพ</option>
						                    	 					<option value="7">ฟื้นฟูสมรรณภาพ</option>
						                    	 					<option value="8">ผ่าตัดทั่วไป และผ่าตัดเสริมความงาม</option>
						                    	 					<option value="9">สตรี ตรวจภายใน ฝากครรภ์ และทำคลอด</option>
						                    	 					<option value="10">หู คอ จมูก</option>
						                    	 					<option value="11">กระดูก ข้อ และการกีฬา</option>
						                    	 					<option value="12">อายุรกรรม</option>
						                    	 					<option value="13">มะเร็งและโรคเลือด</option>
						                    	 					<option value="14">ตับ ระบบทางเดินอาหารและทางเดินปัสสาวะ</option>
						                    	 					<option value="15">รักษาข้อ และ เข่า</option>
						                    	 					<option value="16">ต่อมไร้ท่อ ไทรอยด์ เบาหวาน</option>
						                    	 					<option value="17">ภาวะติดเชื้อ</option>
						                    	 					<option value="18">ไต</option>
						                    	 					<option value="19">ภูมิแพ้และภูมิคุ้มกัน</option>
						                    	 					<option value="20">ปอด</option>
						                    	 					<option value="21">หัวใจ</option>
						                    	 					<option value="22">ทำฟัน และ ดูแลทางช่องปาก</option>
						                    	 					<option value="23">อื่นๆ</option>
						                    	 				</select>
						                    	 				<div class="help-block with-errors"></div>
						                    	 			</div>
						                    			</div>
						                    			<div class="col-md-6">
						                    				<div class="form-group text-left">
						                    	 				<label for="form_email"> เลือกแพทย์ <small>*</small></label>
						                    	 				<select name="" id="input_4_12" class="form-control bgc" tabindex="14">
										                     		<option value="นพ. กฤษดา ไพรวัฒนานุพันธ์">นพ. กฤษดา ไพรวัฒนานุพันธ์</option>
										                   	 		<option value="รศ.นพ. กิตติ ต่อจรัส">รศ.นพ. กิตติ ต่อจรัส</option>
										                   	 		<option value="นพ. กีรติ ประเสริฐผล">นพ. กีรติ ประเสริฐผล</option>
										                   	 		<option value="นพ. จตุรงค์ ตันติมงคลสุข">นพ. จตุรงค์ ตันติมงคลสุข</option>
										                   	 		<option value="พญ. จิตติมา วงษ์โคเมท">พญ. จิตติมา วงษ์โคเมท</option>
										                   	 		<option value="พญ. จิตติมา เจริญสุข">พญ. จิตติมา เจริญสุข</option>
						                    	 				</select>
						                    	 				<div class="help-block with-errors"></div>
						                    	 			</div>
						                    			</div>
						                    		</div>
					                    			
	                    			                <hr>
	                    							<div class="row text-left"><p class="f-size1">- ข้อมูลนัดหมาย -</p></div>
													<div class="row">
														<div class="col-md-12">
															<div class="form-group text-left">
						                            			<label for="form_name"><i class="fa fa-file-text-o"></i> อธิบายปัญหาทางด้านสุขภาพของท่าน</label>
						                            			<textarea id="form_message" name="form_message" class="form-control bgc required" rows="6" placeholder="อธิบายปัญหาทางด้านสุขภาพของท่าน" required="required" data-error="Message is required." style="resize:vertical;"></textarea>
						                            			<div class="help-block with-errors"></div>
					                        				</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-4">
															<div class="form-group text-left">
					                        					<label><i class="icon-Hospital"></i> เลือกโรงพยาบาล</label>
					                        					<select name="hospital" class="form-control bgc">
					                       							<option value="all"> - เลือกโรงพยาบาล - </option>
					                        						<option value="1"> สมิติเวช สุขุมวิท</option>
					                        						<option value="2"> สมิติเวช ศรีนครินทร์</option>
					                        						<option value="3"> สมิติเวช ศรีราชา</option>
					                        						<option value="4"> สมิติเวช ธนบุรี</option>
					                        						<option value="6" selected="selected"> สมิติเวช ชลบุรี</option>
					                        						<option value="7"> โรงพยาบาลเด็กสมิติเวช</option> 
					                        					</select>
					                        				</div>
														</div>
														<div class="col-md-4">
						                            		<div class="form-group text-left">
						                                		<label for="form_date"><i class="fa fa-calendar-check-o"></i> วันที่นัดหมาย</label>
						                                		<input type="text" value="" id="datetimepicker" name="form_date" class="form-control required datepicker bgc" placeholder="เลือกวันเวลานัด" required="required" data-error="Subject is required.">
						                            		</div>
						                        		</div>
						                        		<div class="col-md-4">
						                            		<div class="form-group text-left">
						                                		<label for="form_date"><i class="fa fa-calendar-check-o"></i> วันที่นัดหมาย (สำรอง) <small>*</small></label>
						                                		<input type="text" value="" id="datetimepicker3" name="form_date" class="form-control required datepicker bgc" placeholder="เลือกวันเวลานัด (สำรอง)" required="required" data-error="Subject is required.">
						                            		</div>
						                       	 		</div> 
													</div>
					                        		<div class="row">
					                        			<div class="col-md-6 text-left"> 
					                        				<label>คุณเคยขึ้นทะเบียนเป็นผู้ป่วย ที่โรงพยาบาลสมิติเวชแล้วหรือไม่ </label>
					                        				<div class="form-group text-left">
					                        					<input type="checkbox" name="" id="checkY" onclick="myFunction()">
					                        					<label> ใช่</label>
					                        					<input type="text" value="" id="inText3" name="inText3" class="form-control required bgc" placeholder="เลขประจำตัวผู้ป่วย" style="display: none;">
					                        				</div>
					                        			</div>
					                        			<div class="col-md-6 text-left">
					                        				<label>Privacy policy</label>
					                        				<div class="form-group text-left">
					                        					<input type="checkbox" name="" id="">
					                        					<label> Accept our privacy policy</label>
					                        				</div>
					                        			</div>
					                       			</div>    
                       								<hr>
                         							<div class="row text-left"><p class="f-size1">- สมัครรับข่าวสารสุขภาพของเรา -</p></div>
					                        		<div class="row">
					                        			<div class="col-md-12">
					                        				<div class="form-group text-left">
					                        					<input type="checkbox" name="">
					                        					<label> ใช่ ฉันต้องการรับข่าวสารสุขภาพจากโรงพยาบาลสมิติเวช</label>
					                        				</div>
					                        			</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="form-group text-center">
							                            		<input id="form_botcheck" name="form_botcheck" class="form-control" value="" type="hidden">
							                            		<button type="submit" class="btn btn-lg ulockd-btn-thm2" data-loading-text="Getting Few Sec...">บันทึก</button>
							                        		</div>
														</div>
													</div>
												</form>
						                    </div>
						                </div>
						            </div>
						        </div>
						    </div>
			</div>
			<!-- Product Popup View End-->
		</div>
</section>
<script>
	function myFunction() {
	    var checkb3 = document.getElementById("inText3");
	    if (checkb3.style.display === "block") {
	        checkb3.style.display = "none";
	        
	    } else {
	        checkb3.style.display = "block";
	    }
	}
</script>
*/
?>