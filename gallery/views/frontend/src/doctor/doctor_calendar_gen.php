<?php 
	$doc_workdays = ''; 
	$doc_worktimes = ''; 
?>
<?php foreach ($query_workdayhour as $workday) :?>
	<?php
		if (isset($workday->work_days) && !empty($workday->work_days) && strpos($workday->work_days, '-') !== false) {
			$days = explode(" - ", $workday->work_days);
			if($days[0] == 'วันจันทร์' || $days[0] == 'จันทร์' || $days[1] == 'วันจันทร์' || $days[1] == 'จันทร์' ){
				$doc_workdays .= 'Monday ';
			}
			if($days[0] == 'วันอังคาร' || $days[0] == 'อังคาร' || $days[1] == 'วันอังคาร' || $days[1] == 'อังคาร' ){
				$doc_workdays .= 'Tuesday ';
			}
			if($days[0] == 'วันพุธ' || $days[0] == 'พุธ' || $days[1] == 'วันพุธ' || $days[1] == 'พุธ' ){
				$doc_workdays .= 'Wednesday ';
			}
			if($days[0] == 'วันพฤหัสบดี' || $days[0] == 'พฤหัสบดี' || $days[1] == 'วันพฤหัสบดี' || $days[1] == 'พฤหัสบดี' ){
				$doc_workdays .= 'Thursday ';
			}
			if($days[0] == 'วันศุกร์' || $days[0] == 'ศุกร์' || $days[1] == 'วันศุกร์' || $days[1] == 'ศุกร์' ){
				$doc_workdays .= 'Friday ';
			}
			if($days[0] == 'วันเสาร์' || $days[0] == 'เสาร์' || $days[1] == 'วันเสาร์' || $days[1] == 'เสาร์' ){
				$doc_workdays .= 'Saturday ';
			}
			if($days[0] == 'วันอาทิตย์' || $days[0] == 'อาทิตย์' || $days[1] == 'วันอาทิตย์' || $days[1] == 'อาทิตย์' ){
				$doc_workdays .= 'Sunday ';
			}
			$doc_worktimes .= $workday->work_times_in." - ".$workday->work_times_out."_";
			$doc_worktimes .= $workday->work_times_in." - ".$workday->work_times_out."_";
		}
		else if(isset($workday->work_days) && !empty($workday->work_days)){
			if($workday->work_days == 'วันจันทร์' || $workday->work_days == 'จันทร์'){
				$doc_workdays .= 'Monday ';
			}
			else if($workday->work_days == 'วันอังคาร' || $workday->work_days == 'อังคาร' ){
				$doc_workdays .= 'Tuesday ';
			}
			else if($workday->work_days == 'วันพุธ' || $workday->work_days == 'พุธ'){
				$doc_workdays .= 'Wednesday ';
			}
			else if($workday->work_days == 'วันพฤหัสบดี' || $workday->work_days == 'พฤหัสบดี'){
				$doc_workdays .= 'Thursday ';
			}
			else if($workday->work_days == 'วันศุกร์' || $workday->work_days == 'ศุกร์'){
				$doc_workdays .= 'Friday ';
			}
			else if($workday->work_days == 'วันเสาร์' || $workday->work_days == 'เสาร์'){
				$doc_workdays .= 'Saturday ';
			}
			else if($workday->work_days == 'วันอาทิตย์' || $workday->work_days == 'อาทิตย์'){
				$doc_workdays .= 'Sunday ';
			}
			$doc_worktimes .= $workday->work_times_in." - ".$workday->work_times_out."_";
		}
	?>
<?php endforeach?>