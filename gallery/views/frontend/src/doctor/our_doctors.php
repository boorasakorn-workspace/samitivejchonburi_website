
<link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">
<section  class="bgc-darkgold1">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 text-center">
				<div class="ulockd-main-title">
					<h2 class="mt-separator" ><?=$this->lang->line('samitivej_All_Doctor_Title');?></h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="ulockd-contact-form">
					<div class="row">
						<div class="col-md-12">
							<h3><?=$this->lang->line('samitivej_All_Doctor_Search');?> <i class=" icon_search"></i><hr></h3>
						</div>
					</div>
					<form action="OurDoctors" method="get" class="form-group" accept-charset="utf-8">
						<div class="row">							
							<div class="col-md-12">
								<div class="form-group text-left">
									<label for="form_email"><i class="fa fa-user-o"></i> <?=$this->lang->line('samitivej_Doctor_name');?></label>
									<input id="doctor_name" name="doctor_name" class="form-control border-r bgc" placeholder="<?=$this->lang->line('samitivej_Doctor_name');?>" required="required" data-error="Name is required." type="text">
									<div class="help-block with-errors"></div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group text-left">
									<label for="form_name"><i class="fa fa-envelope-open-o"></i> <?=$this->lang->line('samitivej_Service_Center');?></label>
									<select name="medical_center" class="form-control border-r bgc" id="medical_center_txt">
										<option value="all" selected="selected"> <?=$this->lang->line('samitivej_All_Service');?></option>
										<?php foreach ($all_med_doctor as $result) :?>
											<option value="<?php echo $result->id;?>"><?php echo $result->medical_center_name;?></option>											
										<?php endforeach;?>
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group text-left">
									<label for="form_name"><i class="fa fa-envelope-open-o"></i> <?=$this->lang->line('samitivej_Speciality');?></label>
									<select name="specialty" class="form-control border-r bgc" id="specialty_txt">
										<option value="all" selected="selected"> <?=$this->lang->line('samitivej_All_Speciality');?></option>
										<?php foreach ($all_specialty as $result) :?>
											<option value="<?php echo $result->id;?>"><?php echo $result->specialty_name;?></option>											
										<?php endforeach;?>
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group text-left">
									<label for="form_name"><i class="fa fa-envelope-open-o"></i> <?=$this->lang->line('samitivej_Sub_Speciality');?></label>
									<select name="sub_specialty" class="form-control border-r bgc" id="sub_specialty_txt">
										<option value="all" selected="selected"> <?=$this->lang->line('samitivej_All_Speciality');?></option>
										<?php foreach ($all_sub_specialty as $result) :?>
											<option value="<?php echo $result->id;?>"><?php echo $result->sub_specialty_name;?></option>											
										<?php endforeach;?>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group text-left">
									<label><i class="icon-Flag-2"></i> <?=$this->lang->line('samitivej_Doctor_Language');?></label>
									<select name="lang" class="form-control border-r bgc">
										<option value="all" selected="selected"> <?=$this->lang->line('samitivej_All_Language');?></option>
										<option value="44"> จีน</option>
										<option value="48"> จีนแต้จิ่ว</option>
										<option value="47"> จีนแมนดาริน</option>
										<option value="53"> ญี่ปุ่น</option>
										<option value="54"> ลาว</option>
										<option value="49"> อังกฤษ</option>
										<option value="56"> ไทย</option> 
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group text-left">
									<label for="form_email"><i class="icon-Bisexual"></i> <?=$this->lang->line('samitivej_Doctor_Gender');?></label>
									<select name="gender" class="form-control border-r bgc">
										<option value="All" selected="selected"><?=$this->lang->line('samitivej_Doctor_Gender_All');?></option>
										<option value="Male"><?=$this->lang->line('samitivej_Doctor_Gender_Male');?></option>
										<option value="Female"><?=$this->lang->line('samitivej_Doctor_Gender_Female');?></option>
									</select>
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group text-left">
									<lbel class="form-date"><i class="fa fa-calendar-check-o"></i> <?=$this->lang->line('samitivej_Doctor_Work_Time');?></lbel>
									<input id="date" name="form_date" class="form-control border-r bgc datepicker" placeholder="<?=$this->lang->line('samitivej_Doctor_Work_Time');?>" type="text">
								</div>
							</div>
							<!-- 
							<div class="col-md-4">
								<div class="form-group text-left">
									<label><i class="icon-Hospital"></i> โรงพยาบาล</label>
									<select name="hospital" class="form-control border-r bgc">
										<option value="all"> ทุกโรงพยาบาล</option>
										<option value="1"> สมิติเวช สุขุมวิท</option>
										<option value="2"> สมิติเวช ศรีนครินทร์</option>
										<option value="3"> สมิติเวช ศรีราชา</option>
										<option value="4"> สมิติเวช ธนบุรี</option>
										<option value="6" selected="selected"> สมิติเวช ชลบุรี</option>
										<option value="7"> โรงพยาบาลเด็กสมิติเวช</option> 
									</select>
								</div>
							</div> -->
						</div>
						<div class="row">
							<div class="col-md-12">
								<button type="submit" class="btn btn-lg ulockd-btn-thm2 block" id="btn-search"><?=$this->lang->line('samitivej_Doctor_Filter_Result');?></button>
							</div>

						</div>
					</form>
				</div>
			</div>
		</div>
		<hr>
		<div class="row ulockd-mrgn1225">
			<?php
			$i = 0; 
			foreach ($alldoctor as $result) :?>  
				<?php 
				$pname = $result->pname_th;
				$firstname = $result->firstname_th;
				$lastname = $result->lastname_th;
				//$speciality = $result->speciality_th;
				$full = $pname.' '.$firstname.' '.$lastname;
				$i++;
				
				$doctor_med_center = '';
				$doctor_parent_medical_center_id = explode('|',$result->parent_medical_center_id);
				for($n=0;$n<count($doctor_parent_medical_center_id);$n++){
				  foreach($all_med_doctor as $med_center){
				    if($doctor_parent_medical_center_id[$n] == $med_center->id){
				      $doctor_med_center .= $med_center->medical_center_name . ' ';
				    }
				    elseif(!is_numeric($doctor_parent_medical_center_id[$n]) && $doctor_parent_medical_center_id[$n] != ''){
				      $doctor_med_center .= $doctor_parent_medical_center_id[$n] . ' ';
				    }
				  }
				}
				$doctor_med_center = implode(' ',array_unique(explode(' ', $doctor_med_center)));
				?>
				<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3" style="margin-bottom: 4em;">               
					<div class="team-one" style="height:100%;">
                  <a href="<?php echo base_url();?>frontend/Home/DoctorDetails/<?php echo $result->id;?>">                  
						<div class="team-thumb">
							<img class="img-responsive img-whp" src="<?php echo base_url(); ?>gallery/doctor/big_imgs/<?php echo $result->doctor_imgs; ?>"  style="width:260px;height:320px;"  alt="<?php echo $result->doctor_imgs; ?>">
							<?php /*
							<div class="team-overlay">
								<ul class="list-inline team-icon style2">
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-envelope"></i></a></li>
								</ul>
							</div>
							*/ ?>
						</div>
                  </a>                  
						<div class="team-details text-left" style="position:relative;">
							<a href="<?php echo base_url();?>frontend/Home/DoctorDetails/<?php echo $result->id;?>">
								<p class="member-name f-size1"><?php echo $full;?></p>
							</a>
							<p class="member-post" style="font-size: 12px;"><?php echo $doctor_med_center;?></p>
							<p style="font-size: 12px;">ภาษา ไทย - อังกฤษ</p>
							<a class="btn btn-default ulockd-btn-thm2" style="position: absolute;top:160px;left:0px;" href="<?php echo base_url();?>frontend/Home/DoctorDetails/<?php echo $result->id;?>"><?=$this->lang->line('samitivej_More_Details');?></a> 
							<a class="btn btn-default ulockd-btn-thm2" style="position: absolute;top:160px;right:0px;" href="<?php echo base_url();?>frontend/Home/DoctorAppointment/<?php echo $result->id;?>"><?=$this->lang->line('samitivej_Request_an_Appointment');?></a>
						</div>
					</div>
               
				</div>
				<?php
				if($i%4 == 0){
					echo '</div><div class="row ulockd-mrgn1225"><br>';
				} 
			endforeach;?>
			<!--  -->
			<div class="col-lg-12 col-md-offset-4">
				<nav aria-label="Page navigation navigation-lg">
					<ul class="pagination pagination1">
						<li>
							<a href="#" aria-label="Previous">
								<span aria-hidden="true"><?=$this->lang->line('samitivej_Previous_Text');?></span>
							</a>
						</li>
						<li class="active"><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li>
							<a href="#" aria-label="Next">
								<span aria-hidden="true"><?=$this->lang->line('samitivej_Next_Text');?></span>
							</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>

	</section>


	<script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

	<script type="text/javascript">
		$( document ).ready(function() {
			$('#btn-search').click(function(){
				if($('#name-doctor').val()=="" || $('#specialty_txt').val()==""){
					alert('ccxcxc');
					return false;
				}
			});
		});
	</script>