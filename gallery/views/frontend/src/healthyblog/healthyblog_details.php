<section class="bgc-darkgold1">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 text-center">
	          <div class="ulockd-main-title">
	            <h2 class="mt-separator">บทความสุขภาพ</h2>
	          </div>
	        </div>
		</div>
		<div class="row">
			<div class="col-md-4 col-lg-3 ulockd-pdng0 desktop">
				<div class="ulockd-faq-content">
					<?php
						$this->view('frontend/src/sidemenu/sidemenu');
					?>

	                </div>
					
		        </div>


			<div class="col-md-8 col-lg-9">
				<?php /*
				<div class="row">
					<div class="col-md-12 ulockd-mrgn1210">
						<h3 style="color:black; font-weight: 600;"><?=$query->blog_title; ?></h3>
						<!-- <div class="ulockd-project-sm-thumb">
							<img class="img-responsive img-whp" src="<?php echo base_url(); ?>gallery/blog/big_imgs/<?=$query->blog_big_img; ?>" alt="">
						</div> -->
					</div>
				</div> */?>
				<div class="row">
					<!-- 
					<div class="col-md-12 ulockd-mrgn1210">
						<div class="ulockd-pd-content">
							<div class="ulockd-bp-date">
								<ul class="list-inline course-feature">
									<li><a href="#"><i class="fa fa-calendar text-thm2"></i> 28 April</a></li>
									<li><a href="#"><i class="fa fa-user text-thm2"></i> By: Admin</a></li>
									<li><a href="#"><i class="fa fa-server text-thm2"></i> Industry</a></li> 
								</ul>
							</div>
						</div>
					</div>
					-->
					<div class="col-md-12 ulockd-mrgn1210">
						<?=html_entity_decode($query->blog_contents); ?>
						<?php 
								if(isset($query->blog_video) && $query->blog_video != ""){
									echo "<hr>";
									if (strpos($query->blog_video, 'www.youtube.com') !== false) {
										$yt_link = explode("watch?v=",$query->blog_video); 
							?>
										<iframe width="100%" height="320" src="https://www.youtube.com/embed/<?=$yt_link[1];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							<?php
									}elseif (strpos($query->blog_video, 'youtu.be') !== false) {
										$yt_link = explode("youtu.be/",$query->blog_video); 
							?>
										<iframe width="100%" height="320" src="https://www.youtube.com/embed/<?=$yt_link[1];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							<?php
									}
									else{
							?>
										<video style="height:320px; width:100%;" src="<?=base_url('gallery/doctor/videos/').$query->blog_video;?>" controls></video>
							<?php
									}
								}
							?>
					</div>							
					<div class="col-lg-12">
						<?php 
						if(isset($promotion_1) || isset($promotion_2)){ ?>
							<h3> <?=$this->lang->line('samitivej_Related_Promotion');?> </h3>
						<?php
						}
						if(isset($promotion_1)){ ?>
							<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-6">
								<a href="<?php echo base_url();?>frontend/Home/PromotionDetails/<?php echo $promotion_1->id;?>"><h4 style="color:black;"> <?php echo $promotion_1->package_title;?></h4></a>
								<figure class="department_box imghvr-push-down">
						            <img class="img-responsive" style="height:260px; width:620px;" src="<?php echo base_url(); ?>gallery/package/big_imgs/<?php echo $promotion_1->package_big_img;?>" alt="<?php echo $promotion_1->package_title;?>">
						            <a href="<?php echo base_url();?>frontend/Home/PromotionDetails/<?php echo $promotion_1->id;?>">
						              <figcaption>
						                <h4 style="color:black;"> <?php echo $promotion_1->package_title;?></h4>
						                <div style="color:black;" class="healty-description"><?php echo $promotion_1->package_title;?>
						                </div>
						              </figcaption>
						            </a> 
						        </figure>
						    </div>
						<?php } ?>
						<?php
						if(isset($promotion_2)){ ?>
							<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-6">
								<a href="<?php echo base_url();?>frontend/Home/PromotionDetails/<?php echo $promotion_2->id;?>"><h4 style="color:black;"> <?php echo $promotion_2->package_title;?></h4></a>
								<figure class="department_box imghvr-push-down">
						            <img class="img-responsive" style="height:260px; width:620px;" src="<?php echo base_url(); ?>gallery/package/big_imgs/<?php echo $promotion_2->package_big_img;?>" alt="<?php echo $promotion_2->package_title;?>">
						            <a href="<?php echo base_url();?>frontend/Home/PromotionDetails/<?php echo $promotion_2->id;?>">
						              <figcaption>
						                <h4 style="color:black;"> <?php echo $promotion_2->package_title;?></h4>
						                <div style="color:black;" class="healty-description"><?php echo $promotion_2->package_title;?>
						                </div>
						              </figcaption>
						            </a> 
						        </figure>
						    </div>

						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>