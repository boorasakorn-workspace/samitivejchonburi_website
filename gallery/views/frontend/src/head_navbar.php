<style>
  /* Popover */
  .popover {
    max-width:none; 
    width:250px;
    height:120px;
  }
</style>
<!-- Header Header Top -->
    <div class="header-top bgc-green1">
      <div class="container">
        <?php
          if(isset($preview)){ ?>
                  <div class="row" style="background-color: #FFF;">
                    <div class="col-lg-2" style="color: #008051;">
                      Backend Preview
                    </div>
                    <div class="col-lg-7">
                      <button id="previewBackButton" type="button" style="
                        width: 1000px;
                        font-family: Arial, Helvetica, sans-serif;
                        font-weight: 700;
                        text-align: center;
                        text-decoration: none;
                        text-transform: uppercase;
                        color: #FFF;
                        background-color : #008051;
                        border-color: #FFF;
                        border-radius: 0;" 
                        onclick="history.back(-1)" 
                        class="btn btn-primary">กลับ</button>
                    </div>
                  </div>
        <?php
          }
        ?>
        <div class="row">
          <div class="col-md-2" style="text-align: left;">
            <div class="welcm-ht text-center">
              <ul class="list-inline ulockd-mrgn1215">
                <li> <a href="<?=$get_header->url_facebook; ?>"><i class="fa fa-facebook color-white"></i></a></li>
                <li> <a href="<?=$get_header->url_twitter; ?>"><i class="fa fa-twitter color-white"></i></a></li>
                <li> <a href="<?=$get_header->url_instagram; ?>"><i class="fa fa-instagram color-white"></i></a></li>
                <li> <a href="<?=$get_header->url_youtube; ?>"><i class="fa fa-youtube color-white"></i></a></li>
                <li> <div name="selfregisballoon" id="selfregisballoon" data-toggle="popover" title="<?=$this->lang->line('Self_regis_title');?>" data-content="<?=$this->lang->line('Self_regis_text');?>" data-placement="auto"><a href="https://selfservice.samitivejchonburi.com/"><i class="fa"><img src="<?=base_url();?>assets_f/images/icon_f/call-white.png" id="selfpopover" style="height:12px;width:12px;" alt="Self Register" ></i></a></div></li>            
              </ul>
            </div>
          </div>
          <div class="col-md-4 col-md-offset-6">
            <div class="welcm-ht text-left ulockd-mrgn1215" style="text-align: right;">
            <?php 
              if($this->session->userdata('ses_position') != '100'){
            ?>
            <ul class="list-inline">
              <li>
                <a class="color-white" href="#" data-toggle="modal" data-target=".bs-example-modal-md" data-whatever="@mdo"><i class="fa fa-user color-white"></i> <?=$this->lang->line('User_Login_Text');?></a>
                    <div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-md" role="document">
                          <div class="modal-content ">
                              <div class="modal-header head-modal">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title text-center color-white"><span class="flaticon-lock"></span> <?=$this->lang->line('User_Login_Text');?></h3>
                              </div>
                              <div class="modal-body bgc-darkgold1">
                                <div class="row">
                                  <div class="col-sm-12 col-md-8 col-md-offset-2 text-center">
                                    <form method="POST" action="<?=base_url(); ?>backend/Account/loginChk">
                                      <p><?=$this->lang->line('Login_Modal_Text');?></p>
                                        <div class="form-group">
                                        <input type="text" class="form-control border-r bgc" name="acc_username" placeholder="<?=$this->lang->line('User_Username_Text');?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control border-r bgc" name="acc_password" placeholder="<?=$this->lang->line('User_Password_Text');?>">
                                    </div>
                                        <button type="submit" class="btn btn-default ulockd-btn-thm2" name="submit" value="Login"><?=$this->lang->line('User_Login_Text');?></button>
                                    </form>
                                  </div>
                                </div>  
                              </div>
                              <!-- modal footer start here-->
                          </div>
                        </div>
                    </div>
              </li>
              <li>
                <a href="#" class="color-white"> | </a>
              </li>
              <li>
                <a class="color-white" href="#" data-toggle="modal" data-target=".bs-example-modal-lg" data-whatever="@mdo"><i class="fa fa-edit color-white"></i> <?=$this->lang->line('User_Register_Text');?></a>
                <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog">
                  <div class="modal-dialog modal-lg" role="document">
                          <div class="modal-content">
                              <div class="modal-header head-modal">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title text-center color-white"><span class="flaticon-lock"></span> <?=$this->lang->line('User_Register_Text');?></h3>
                              </div>
                              <div class="modal-body bgc-darkgold1">
                                <div class="row">
                                  <div class="col-sm-12 col-md-8 col-md-offset-2">
                                    <form class=" text-center row">
                                  <div class="col-md-6">
                                          <div class="form-group">
                                          <input type="text" class="form-control border-r bgc" id="exampleInputNamexa" placeholder="First Name">
                                      </div>
                                  </div>
                                  <div class="col-md-6">
                                          <div class="form-group">
                                          <input type="text" class="form-control border-r bgc" id="exampleInputNamexb" placeholder="Last Name">
                                      </div>
                                  </div>
                                  <div class="col-md-6">
                                          <div class="form-group">
                                          <input type="text" class="form-control border-r bgc" id="exampleInputEmailxu" placeholder="Username">
                                      </div>
                                  </div>
                                  <div class="col-md-6">
                                          <div class="form-group">
                                          <input type="email" class="form-control border-r bgc" id="exampleInputEmailx" placeholder="Email">
                                      </div>
                                  </div>
                                  <div class="col-md-12">
                                      <div class="form-group">
                                          <input type="password" class="form-control border-r bgc" placeholder="Password">
                                      </div>
                                  </div>
                                  <div class="col-md-12">
                                      <div class="form-group">
                                          <input type="password" class="form-control border-r bgc" placeholder="Repeat password">
                                      </div>
                                  </div>
                                  <div class="col-md-12">
                                      <div class="form-group text-center">
                                            <button type="submit" class="btn btn-default ulockd-btn-thm2"><?=$this->lang->line('User_Register_Text');?></button>
                                      </div>
                                  </div>
                                    </form>
                                  </div>
                                </div>  
                              </div>
                              <!-- modal footer start here-->
                          </div>
                  </div>
                    </div>
              </li>              
              <li>
                <div class="dropdown lang-button text-center">
                  <button class="dropbtn color-white"><i class="fa fa-globe color-white"></i></button>
                  <div class="dropdown-content">
                      <a style="padding-left: 5px;"align="left" href="<?=base_url(); ?>frontend/Home/change_language/<?php echo $lang='thai';?>"> <span><img src="<?php echo base_url(); ?>assets_f/images/resource/thai.jpg" alt=""> ภาษาไทย</span> </a>
                      <a style="padding-left: 5px;"align="left" href="<?=base_url(); ?>frontend/Home/change_language/<?php echo $lang='english';?>"> <span><img src="<?php echo base_url(); ?>assets_f/images/resource/english.jpg" alt=""> English</span> </a>
                  </div>
                </div>
              </li>
            </ul>
            <?php } 
            else{
            ?>                   
            <ul class="list-inline">
              <li>
                <a class="color-white"><i class="fa fa-user color-white"></i> <?=$this->session->userdata('ses_username');?></a>
              </li>
              <li>
                <a class="color-white" href="<?=base_url('backend/Dashboard');?>"> Backend</a>
              </li>
              <a href="#" class="color-white"> | </a>
              <li>
                <a class="color-white" href="<?=base_url('backend/Dashboard/Logout');?>"> Logout</a>
              </li>
              <li>                
                <div class="dropdown lang-button text-center">
                  <button class="dropbtn color-white"><i class="fa fa-globe color-white"></i></button>
                  <div class="dropdown-content">
                      <a style="padding-left: 5px;"align="left" href="<?=base_url(); ?>frontend/Home/change_language/<?php echo $lang='thai';?>"> <span><img src="<?php echo base_url(); ?>assets_f/images/resource/thai.jpg" alt=""> ภาษาไทย</span> </a>
                      <a style="padding-left: 5px;"align="left" href="<?=base_url(); ?>frontend/Home/change_language/<?php echo $lang='english';?>"> <span><img src="<?php echo base_url(); ?>assets_f/images/resource/english.jpg" alt=""> English</span> </a>
                  </div>
                </div>
              </li>
            </ul>              
            <?php
            }
            ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  
  <!-- Header Styles -->
  <header class="header-nav">
    <div class="main-header-nav navbar-scrolltofixed">
      <div class="container ulockd-pdng0">
          <nav class="navbar navbar-default bootsnav menu-style1">
          <!-- Start Top Search -->
              <div class="top-search search-bgc">
                  <div class="container">
                      <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-search"></i></span>
                          <input type="text" class="form-control" placeholder="Search">
                          <span class="input-group-addon close-search"><i class="fa fa-times color-white"></i></span>
                      </div>
                  </div>
              </div> 
              <!-- End Top Search -->

              <div class="container">
                  <!-- Start Atribute Navigation -->
                  <div class="attr-nav">
                      <ul>
                          <li class="search"><a href="#"><i class="fa"></i></a></li>
                      </ul>
                  </div>
                  <!-- 
                  <div class="attr-nav">
                      <ul>
                          <li class="search"><a href="#"><i class="fa fa-search"></i></a></li>
                      </ul>
                  </div>
                   -->
                  <!-- End Atribute Navigation -->

                  <!-- Start Header Navigation -->
                  <div class="navbar-header">
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                          <i class="fa fa-bars"></i>
                      </button>
                      <a class="navbar-brand ulockd-main-logo1" href="<?php echo base_url();?>frontend/Home" style="padding-top: 15px;">
                          <img src="<?php echo base_url(); ?>gallery/logo/small_imgs/<?=str_replace('.', '_thumb.', $get_header->header_logo_small_img); ?>" class="logo logo-scrolled" alt="">
                      </a>
                  </div>
                  <!-- End Header Navigation -->

                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="navbar-menu">
                      <ul class="nav navbar-nav navbar-leftabsolu" data-in="fadeIn">
                          <li class="dropdown">
                              <a href="<?php echo base_url();?>frontend/Home" class="active"><?=$this->lang->line('Home_Text');?></a>
                          </li>
                          <?php foreach($get_nav as $row_nav){ ?>
                          <li class="dropdown">
                              <?php
                                $i=0;
                                foreach($get_subnav as $row_subnav){
                                  if($row_subnav->parent_nav_id==$row_nav->id){
                                    $i++;
                                  }
                                }
                                if($i < 1){
                                  if($row_nav->nav_content != ''){
                                    echo '<a href="'.base_url().'frontend/Home/NavDetails/'.$row_nav->id.'" class="active">'.$row_nav->nav_title.'</a>';
                                  }
                                  else if(strpos(strtoupper($row_nav->nav_url), strtoupper('frontend')) !== false){
                                    echo '<a href="'.base_url().$row_nav->nav_url.'" class="active">'.$row_nav->nav_title.'</a>';         
                                  }
                                  else{
                                    echo '<a href="'.$row_nav->nav_url.'" class="active">'.$row_nav->nav_title.'</a>';                                    
                                  }
                                }
                                else{
                                  if($row_nav->nav_content){
                                    echo '<a href="'.base_url().'frontend/Home/NavDetails/'.$row_nav->id.'" class="dropdown-toggle" data-toggle="dropdown">'.$row_nav->nav_title.'</a><ul class="dropdown-menu">';         
                                  }
                                  else if(strpos(strtoupper($row_nav->nav_url), strtoupper('frontend')) !== false){
                                    echo '<a href="'.base_url().$row_nav->nav_url.'" class="dropdown-toggle" data-toggle="dropdown">'.$row_nav->nav_title.'</a><ul class="dropdown-menu">';         
                                  }
                                  else{
                                  echo '<a href="'.$row_nav->nav_url.'" class="dropdown-toggle" data-toggle="dropdown">'.$row_nav->nav_title.'</a><ul class="dropdown-menu">';                                   
                                  }
                              
                                }
                              ?>
                              <?php foreach($get_subnav as $row_subnav){
                                if($row_subnav->parent_nav_id==$row_nav->id){
                                  if($row_subnav->sub_nav_content != ''){
                              ?>
                                    <li><a href="<?php echo base_url();?>frontend/Home/SubNavDetails/<?php echo $row_subnav->subid;?>" class="tcolor-green"><?=$row_subnav->sub_nav_title; ?></a></li>
                                <?php 
                                  }
                                  elseif(strpos(strtoupper($row_subnav->sub_nav_url), strtoupper('frontend')) !== false || strpos(strtoupper($row_subnav->sub_nav_url), strtoupper('backend')) !== false){?>
                                    <li><a href="<?php echo base_url().$row_subnav->sub_nav_url;?>" class="tcolor-green"><?=$row_subnav->sub_nav_title; ?></a></li>
                                <?php
                                  }
                                  else{ ?>
                                    <li><a href="<?php echo $row_subnav->sub_nav_url;?>" class="tcolor-green"><?=$row_subnav->sub_nav_title; ?></a></li>
                              <?php }}} ?>
                              </ul>
                          </li>
                          <?php } ?>
                      </ul>
                  </div><!-- /.navbar-collapse -->
              </div>
          </nav>
      </div>
    </div>    
  </header>