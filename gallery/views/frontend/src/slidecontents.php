<!-- Home Design -->
  <div class="ulockd-home-slider">
    <div class="container-fluid ulockd-pdng0">
      <div class="row">
        <div id="sg-carousel" class="carousel slide carousel-fade" data-ride="carousel">
          <!-- Carousel items -->
          <div class="carousel-inner carousel-zoom">
            <?php foreach($get_carousel as $rows){ 
                if($rows->carousel_type==2){
            ?>
              <div class="item active">
                <video class="img-responsive poly" autoplay loop id="theid" preload="preload" poster="<?php echo base_url();?>gallery/carousel/video/small_imgs/<?=$rows->carousel_small_imgs; ?>" src="<?php echo base_url();?>gallery/carousel/video/<?=$rows->carousel_big_imgs; ?>" type="video/mp4" height='auto' width='auto'>
                </video>
              </div> 
            <?php
                }else if($rows->carousel_type==1){
            ?>
              <div class="item"><img class="img-responsive" src="<?=base_url(); ?>gallery/carousel/big_imgs/<?=$rows->carousel_big_imgs; ?>" alt="h1.jpg">
                <div class="carousel-caption">
                  <h1 data-animation="animated zoomInLeft" class="cap-txt color-white"><span class="ulockd-bgthm"><?=$rows->carousel_h1; ?></span></h1>
                  <h2 data-animation="animated bounceInDown"><span class="bgc-white text-thm2"><?=$rows->carousel_h2; ?></span></h2>
                  <a style="background-color: #FFFFFF00" href="<?=$rows->carousel_button1_link; ?>"><button data-animation="animated fadeInLeft" class="btn btn-lg ulockd-btn-thm2"><?=$rows->carousel_button1; ?></button></a>
                  <a style="background-color: #FFFFFF00" href="<?=$rows->carousel_button2_link; ?>"><button data-animation="animated fadeInRight" class="btn btn-lg ulockd-btn-styledark"><?=$rows->carousel_button2; ?></button></a>
                </div>
              </div>   
            <?php
                }
            ?>

              
          <?php } ?>
            <!-- Carousel nav -->
            <a class="carousel-control left color-black11" href="#sg-carousel" data-slide="prev">‹</a>
            <a class="carousel-control right color-black11" href="#sg-carousel" data-slide="next">›</a>
          </div> 
        </div>
      </div>
    </div>
  </div>

 