
<link rel="stylesheet" href="<?php echo base_url(); ?>resource/css/bootstrap.css?v=1001">


<style type="text/css">
	body{
		padding: 0px !important;
		margin: 0px !important;
	}
	.navbar-light .navbar-nav .nav-link {
		color: #000 !important;
	}

nav.navbar.bootsnav ul.nav > li > a{
	color:#000;
}

nav.navbar.bootsnav li.dropdown ul.dropdown-menu li a {{
	font-family: Prompt !important;
}
</style>


<div class="container-fluid bg-light">
	<div class="container">
		<div class="row">
			<div class="col-9">
				facebook 
			</div>
			<div class="col-3 float-right">
				<div style="width: 100%; height: 0px;">
					<ul style="list-style: none;">
						<li style="float: left; padding: 0 3px">เข้าสู่ระบบ
						</li>
						<li style="float: left; padding: 0 3px">สมัครสมาชิก
						</li>
					</ul>
				</div> 
			</div>
		</div>
	</div>
	

	<div class="container">
		<nav class="navbar navbar-expand-lg navbar-light ">
			<a class="navbar-brand" href="#"><img src="<?php echo base_url('assets_f/images/logo/SCH-logo-Png.png');?>" height="66px;"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
				<!-- <li class="nav-item active">
					<a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
				</li> -->

				<?php foreach($menu as $result):?>
					<li class="nav-item">
						<a class="nav-link" href="#"><?php echo $result->nav_title;?></a>
					</li>


				<?php endforeach;?>
				
				<!-- <li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Dropdown
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="#">Action</a>
						<a class="dropdown-item" href="#">Another action</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="#">Something else here</a>
					</div>
				</li> -->
				
			</ul>
			<ul>
				<?php foreach($language as $result):?>
					<li><a href="<?php echo base_url('frontend/Home/main/');?><?php echo $result->language_code;?>">
						<?php echo $result->language_name;?>
					</a>
				</li>
			<?php endforeach;?>

		</ul>
	</nav>
</div>
</div>
</div>







<link rel="stylesheet" href="<?php echo base_url('resource/OwlCarousel/docs/assets/css/docs.theme.min.css?v=1001');?>">

<!-- Owl Stylesheets -->
<link rel="stylesheet" href="<?php echo base_url('resource/OwlCarousel/docs');?>/assets/owlcarousel/assets/owl.carousel.min.css?v=1001">
<link rel="stylesheet" href="<?php echo base_url('resource/OwlCarousel/docs');?>/assets/owlcarousel/assets/owl.theme.default.min.css?v=1001">

<!-- HTML5 shim and Respond.js?v=1001 IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js?v=1001"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js?v=1001/1.3.0/respond.min.js?v=1001"></script>
  <![endif]-->

  <!-- Favicons -->


  <!-- Yeah i know js should not be in header. Its required for demos.-->

  <!-- javascript -->
  <script src="<?php echo base_url('resource/OwlCarousel/docs');?>/assets/vendors/jquery.min.js?v=1001"></script>
  <script src="<?php echo base_url('resource/OwlCarousel/docs');?>/assets/owlcarousel/owl.carousel.js?v=1001"></script>
</head>
<body>
	<!--  Demos -->
	<section id="demos">
		

		<div class="owl-carousel owl-theme">
			<div class="item">
				<img src="<?php echo base_url('gallery/carousel/big_imgs/Chonburi12.jpg');?>" class="img-fluid">
			</div>
			<div class="item">
				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" src="<?php echo base_url('gallery/carousel/video/clinic_children.mp4');?>" allowfullscreen></iframe>
				</div>
			</div>

		</div>
	</section>
	<div class="row">
		<div class="col-3">
			<div>
				<div>ค้นหาแพทย์</div>
				Which problem you faced on your health ?call or mail us and make an appointment with our expert who can solved your problem.
				<button class="btn btn-success">ค้นหาแพทย์</button>
			</div>
		</div>
		<div class="col-3"><div>นัดหมายแพทย์</div>
		Take a look on our clinic Doctor's timetable and make an appointment with our expert which department or service.

		<button class="btn btn-success">นัดหมายแพทย์</button>
	</div>
	<div class="col-3"><div>สายด่วน สมิติเวชชลบุรี</div>
	+(66) 33 038 912

	You can call us for emargency & make an appointment out of clinic.

	<button class="btn btn-success">ติดต่อสอบถาม</button>
</div>
<div class="col-3"> เวลาทำการ
	Mon - Fri 8:00 - 20:30
	Saturday 8:30 - 18:00
Sunday 9:00 - 14:30</div>
</div>




<div class="row">
	<div class="col-6 mx-auto">
		<h2>
			PACKAGE & PROMOTION
		</h2>
	</div>
</div>

<div class="row">
	<div class="col-4"></div>
	<div class="col-4"></div>
	<div class="col-4"></div>
</div>
<!-- footer -->

<script>
	$(document).ready(function() {
		var owl = $('.owl-carousel');
		owl.owlCarousel({
			items: 1,
			loop: true,
			margin: 10,
			autoplay: false,
			autoplayTimeout: 1000,
			autoplayHoverPause: true
		});
		$('.play').on('click', function() {
			owl.trigger('play.owl.autoplay', [1000])
		})
		$('.stop').on('click', function() {
			owl.trigger('stop.owl.autoplay')
		})
	})
</script>
<!-- vendors -->
<script src="<?php echo base_url('resource/OwlCarousel/docs');?>/assets/vendors/highlight.js?v=1001"></script>
<script src="<?php echo base_url('resource/OwlCarousel/docs');?>/assets/js/app.js?v=1001"></script>
</body>
</html>