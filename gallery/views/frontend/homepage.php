<!DOCTYPE html>
<html dir="ltr" lang="en">

<!-- Mirrored from unlockdesizn.com/html/health-and-beauty/be-medical/demo/index-multipage.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 16 Nov 2018 03:52:44 GMT -->
<head>
<meta http-equiv="Cache-Control" content="no-store" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php
	if(!isset($meta_author)){

	}else{
		echo '<meta name="meta_author" content="';
		echo $meta_author;
		echo '">';
	}
	if(!isset($meta_keywords)){

	}else{
		echo '<meta name="meta_keywords" content="';
		echo $meta_keywords;
		echo '">';
	}
	if(!isset($meta_description)){

	}else{
		echo '<meta name="meta_description" content="';
		echo $meta_description;
		echo '">';
	}
?>
<!-- css file -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets_f/css/bootstrap.css?v=1001">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets_f/css/style.css?v=1001">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets_f/style.css?v=1001">
<link rel="stylesheet" href="<?php echo base_url(); ?>resource/customCSS/customCSS.css?v=1001">
<!-- Responsive stylesheet -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets_f/css/responsive.css?v=1001">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets_f/css/css.daterange.css?v=1001">
<!--- Kanit Fonts --->
<link rel="stylesheet" href="<?php echo base_url(); ?>resource/fonts/kanit/stylesheet.css?v=1001">

<!-- Title -->
<!-- <title><?=$get_header->header_title; ?></title> -->
<title><?=$this->lang->line('title_name');?></title>
<!-- Favicon -->
<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" type="image/x-icon" />
<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" />

<!-- logo icon -->
    <link rel="icon" type="image/png" href="https://www.samitivejhospitals.com/th/wp-content/themes/svh/images/svh-favicons-standard.png" />
    <link rel="icon" type="image/x-icon" href="https://www.samitivejhospitals.com/th/wp-content/themes/svh/images/svh-favicons-standard-ico.ico" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="https://www.samitivejhospitals.com/th/wp-content/themes/svh/images/svh-homescreen-144px.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="https://www.samitivejhospitals.com/th/wp-content/themes/svh/images/svh-homescreen-72px.png" />
    <link rel="apple-touch-icon-precomposed" href="https://www.samitivejhospitals.com/th/wp-content/themes/svh/images/svh-homescreen-57px.png" />

    <link rel="stylesheet" id="colors" href="<?php echo base_url(); ?>assets_f/css/colors/default.css?v=1001" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets_f/css/color-switcher.css?v=1001" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets_f/fullcalendar/fullcalendar.min.css?v=1001" type="text/css">
</head>
<body>
<div class="wrapper">

<?php 
if( !isset($doctorDetail) ){
	if( !isset($previewContent) ){ ?>
   <div id="preloader" class="preloader">
    <div id="pre" class="preloader_container"><!-- <div class="preloader_disabler btn btn-default">Disable Preloader</div> --></div>
  </div>
<?php 
	} 
} ?>

	<?php $this->view($head_navbar); ?>


	<!-- Section: Slide Contents -->
	<?php
		if(!isset($slidecontents)){

		}else{
			$this->view($slidecontents);
		}
	?>
	<!-- Section: List-Boxes -->
	<?php
		if(!isset($listcontents)){

		}else{
			$this->view($listcontents);
		}
	?>

	<?php
		if(!isset($modal_selfregis)){

		}else{
			$this->view($modal_selfregis);
		}
	?>

	<!-- Section: Services Program -->
	<?php
		if(!isset($services)){

		}else{
			$this->view($services);
		}
	?>

	<!-- Section: Package -->
	<?php
		if(!isset($package)){

		}else{
			$this->view($package);
		}
	?>	

	<!-- Section: Combine Blog&News -->
	<?php
		if(!isset($bloghealthy) && !isset($blogcontents)){

		}else{
			$this->view('frontend/src/blognews');
		}
	?>

	<?php /*
	<!-- Section: Healthy Blog -->
	<?php
		if(!isset($bloghealthy)){

		}else{
			$this->view($bloghealthy);
		}
	?>
	*/ ?>

	<!-- Section: Doctors -->
	<?php 
		if(!isset($recommend_doctor)){
			 
		}else{
			$this->view($recommend_doctor);
		}
	?>

	<?php /*
	<!-- Section: Blog -->
	<?php 
		if(isset($blogcontents)){
			$this->view($blogcontents); 
		}
	?>	
	*/ ?>

	<!-- doctor_detail -->
	<?php
		if(!isset($doctorDetail)){

		}else{
			$this->view($doctorDetail);
		}
	?>

	<!-- All our_doctors -->
	<?php
		if(!isset($ourDoctors)){

		}else{
			$this->view($ourDoctors);
		}
	?>

	<!-- Doctor appointments -->
	<?php
		if(!isset($doctorAppointment)){

		}else{
			$this->view($doctorAppointment);
		}
	?>

	<!-- Clinical center adults -->
	<?php
		if(!isset($clinicAdults)){

		}else{
			$this->view($clinicAdults);
		}
	?>

	<!-- All news -->
	<?php
		if(!isset($news)){

		}else{
			$this->view($news);
		}
	?>
	
	<!-- News details -->
	<?php
		if(!isset($newsDetails)){

		}else{
			$this->view($newsDetails);
		}
	?>
	
	<!-- Healthy Blog -->
	<?php
		if(!isset($healthyBlog)){

		}else{
			$this->view($healthyBlog);
		}
	?>

	<!-- package details -->
	<?php
		if(!isset($packageDetails)){

		}else{
			$this->view($packageDetails);
		}
	?>

	<!-- Contents details -->
	<?php
		if(!isset($contentsDetails)){

		}else{
			$this->view($contentsDetails);
		}
	?>

	<!-- healthyblog details -->
	<?php
		if(!isset($healthyblogDetails)){

		}else{
			$this->view($healthyblogDetails);
		}
	?>

	<!-- All promotion -->
	<?php
		if(!isset($promotion)){

		}else{
			$this->view($promotion);
		}
	?>

	<!-- Pomotion Details -->
	<?php
		if(!isset($promotionDetails)){

		}else{
			$this->view($promotionDetails);
		}
	?>

	<!-- All service -->
	<?php
		if(!isset($service)){

		}else{
			$this->view($service);
		}
	?>

	<!-- All service -->
	<?php
		if(!isset($serviceDetails)){

		}else{
			$this->view($serviceDetails);
		}
	?>
	
	<!-- Contact US -->
	<?php
		if(!isset($contactUs)){

		}else{
			$this->view($contactUs);
		}
	?>

	<!-- treatment history -->
	<?php
		if(!isset($TreatmentHistory)){

		}else{
			$this->view($TreatmentHistory);
		}
	?>

	<!-- nav_detail -->
	<?php
		if(!isset($navDetails)){

		}else{
			$this->view($navDetails);
		}
	?>

	<!-- subnav_detail -->
	<?php
		if(!isset($subnavDetails)){

		}else{
			$this->view($subnavDetails);
		}
	?>

		<!-- Sidebar_detail -->
	<?php
		if(!isset($SidebarDetails)){

		}else{
			$this->view($SidebarDetails);
		}
	?>

		<!-- SubSidebar_detail -->
	<?php
		if(!isset($SubSidebarDetails)){

		}else{
			$this->view($SubSidebarDetails);
		}
	?>

		<!-- SubSubSidebar_detail -->
	<?php
		if(!isset($SubSubSidebarDetails)){

		}else{
			$this->view($SubSubSidebarDetails);
		}
	?>

	<!-- Footer -->
	<?php
		if(!isset($footerpages)){

		}else{
			$this->view($footerpages);
		}
	?>
	</div>
	<!-- end wrapper -->

<!-- Footer Scripts -->
	<div class="theme-panel1">
		<a href="javascript:;" data-click="theme-panel-expand1" class="theme-collapse-btn"><img src="https://www.samitivejhospitals.com/wp-content/themes/svh/css/../images/menu-cta/menu_hover_03.png"></a>
		<div class="theme-panel-content">
			<ul class="theme-list clearfix">
				<li><a href="<?php echo base_url('frontend/Home/doctorAppointment');?>" class="bg-red" data-theme="red" data-theme-file="../assets/css/e-commerce/theme/red.css?v=1001" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Red" data-original-title="" title=""><?=$this->lang->line('samitivej_Appointment');?></a></li>
			</ul>
		</div>
	</div>

	<div class="theme-panel2">
		<a href="javascript:;" data-click="theme-panel-expand2" class="theme-collapse-btn"><img src="https://www.samitivejhospitals.com/wp-content/themes/svh/css/../images/menu-cta/menu_hover_05.png"></a>
		<div class="theme-panel-content">
			<ul class="theme-list clearfix">
				<li><a href="<?php echo base_url('frontend/Home/OurDoctors');?>" class="bg-red" data-theme="red" data-theme-file="../assets/css/e-commerce/theme/red.css?v=1001" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Red" data-original-title="" title=""><?=$this->lang->line('samitivej_Find_a_Doctor');?></a></li>
			</ul>
		</div>
	</div>

	<div class="theme-panel3">
		<a href="javascript:;" data-click="theme-panel-expand3" class="theme-collapse-btn"><img src="https://www.samitivejhospitals.com/wp-content/themes/svh/css/../images/menu-cta/menu_hover_09.png"></a>
		<div class="theme-panel-content">
			<ul class="theme-list clearfix">
				<li><a href="<?php echo base_url('frontend/Home/ContactUs');?>" class="bg-red" data-theme="red" data-theme-file="../assets/css/e-commerce/theme/red.css?v=1001" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Red" data-original-title="" title=""><?=$this->lang->line('samitivej_Contact_Us');?></a></li>
			</ul>
		</div>
	</div>
<?php /*
	<div class="theme-panel4">
		<a href="javascript:;" data-click="theme-panel-expand3" class="theme-collapse-btn"><img src="https://www.samitivejhospitals.com/wp-content/themes/svh/css/../images/menu-cta/menu_hover_12.png"></a>
		<div class="theme-panel-content">
			<ul class="theme-list clearfix">
				<li><a href="javascript:;" class="bg-red" data-theme="red" data-theme-file="../assets/css/e-commerce/theme/red.css?v=1001" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Red" data-original-title="" title="">VITUAL HOSPITAL</a></li>
			</ul>
		</div>
	</div> */ ?>



<!-- external javascripts -->
<!-- Wrapper End -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/jquery-1.12.4.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/bootstrap.min.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/bootsnav.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/parallax.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/scrollto.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/jquery.counterup.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/gallery.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/wow.min.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/slider.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/video-player.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/jquery.barfiller.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/timepicker.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/tweetie.js?v=1001"></script>
<!-- Custom script for all pages --> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/script.js?v=1001"></script>
<script src="<?php echo base_url(); ?>assets_f/js/app.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/fullcalendar/lib/moment.min.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/fullcalendar/fullcalendar.min.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/fullcalendar/gcal.min.js?v=1001"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/videohover.js?v=1001"></script>
			
<script type="text/javascript">
	function freezeClicFn(e) {
		if(e.target.id != 'previewBackButton'){
	        e.stopPropagation();
	        e.preventDefault();
		}
	}
  $(document).ready(function() {
  	var checkPreview = '<?php if(isset($preview)&&$preview != ''){echo "preview";}else{echo '';}?>';
  	if(checkPreview != ''){
  		console.log("Preview Mode");
		document.addEventListener("click", freezeClicFn, true);	    
  	}
  	
		App.init();
  	/*console.log('Ready');*/
	    $('#myModal').modal('show');
	  	$('#myModal').on("hidden.bs.modal", function(){
		    /*console.log("close");*/
		    $('[data-toggle="popover"]').popover('show');
	        setTimeout(function () {
	        $('[data-toggle="popover"]').popover('hide');
	        }, 5000);
	  });
	$('#calendar').fullCalendar({
		eventSources: [
            {
                color: '#008051',   
                textColor: '#FFFFFF',
    			events: dowEvents
            }
        ],
	    dayClick: function(date, jsEvent, view) {
	        date_last_clicked = $(this);
	        //$(this).css('background-color', '#bed7f3');
	        if (IsDateHasEvent(date)) {
	        	$('#appointment_view').modal();
		        var dateString = date.format('Y/M/D');
		        $('#datetimepicker').val(dateString);
		        // $('#datetimepicker3').val(dateString);
		        var eventSources = $('#calendar').fullCalendar('getEventSources');
	    		console.log(eventSources);	        
	        }
	        
	    }

    });

    function IsDateHasEvent(date) {
        var allEvents = [];
        allEvents = $('#calendar').fullCalendar('clientEvents');
        var event = $.grep(allEvents, function (v) {
            return +v.start === +date;
        });
        return event.length > 0;
    }


    function dowEvents(start, end, tz, callback) {
	  var events = [];
	  var curr = start;
	  var all_doctor_workdays = document.getElementById("doctor_workdays").value;
	  var all_doctor_worktimes = document.getElementById("doctor_worktimes").value;
	  var doctor_workdays_arr = all_doctor_workdays.split(' ');
	  var doctor_worktimes_arr = all_doctor_worktimes.split('_');
	  while(curr <= end) {
	  	var i=0;
	  	while(i < doctor_workdays_arr.length){
		    if(curr.format('dddd') === doctor_workdays_arr[i]) {
		        events.push({
		        title: '✔',
		        start: moment(curr),
		      });
		    }
		    i++;
		}
		curr = curr.add(1, 'day');
	  }
	  callback(events);
	}



  });

</script>  

<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/js/jquery.daterange.js"></script>
</body>

</html>

