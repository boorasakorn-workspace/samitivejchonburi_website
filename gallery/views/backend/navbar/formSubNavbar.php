<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
		<li class="breadcrumb-item active">Sub Navigation</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header"><?=$headline; ?></h1>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">จัดการ Sub Menu</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<?=validation_errors("<p style='color:red;'>", "</p>") ?>
					<?php
						if(is_numeric($update_id)) {
					?>
                        <div class="form-group row fileupload-buttonbar">
                            <div class="col-md-12">
                                <span class="fileinput-button m-r-3">
                                	<!--
                                    <a href="<?=base_url(); ?>backend/src/Navbar/delete_menu_subnav/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-danger" id="delete_menu_subnav" name="delete_menu_subnav">Delete Sub Menu</button>
                                    </a>
									-->									

									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">Delete Sub Menu</button>

									<!-- Modal -->
									<div id="deleteModal" class="modal fade" role="dialog">
									  <div class="modal-dialog">
									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">Delete Sub Menu</h4>
									      </div>
									      <div class="modal-body">
									        <p>Sure to delete sub-menu?</p>
									      </div>
									      <div class="modal-footer">
		                                    <a href="<?=base_url(); ?>backend/src/Navbar/delete_menu_subnav/<?=$update_id; ?>">
		                                    	<button type="button" class="btn btn-danger" id="delete_menu_subnav" name="delete_menu_subnav">Delete Sub Menu</button>
		                                    </a>
									        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
									      </div>
									    </div>

									  </div>
									</div>

                                    <a href="<?=base_url(); ?>backend/src/Navbar/view_menu_details/<?=$update_id; ?>">
                                        <button type="button" class="btn btn-success" id="view_menu" name="view_menu"><i class="fas fa-eye"></i> View Sub Menu In Front</button>
                                    </a>
                                </span>
                            </div>
                        </div>
					<?php }
						$location_form = base_url()."backend/src/Navbar/create_subnav/".$update_id;
					?>
					<form class="form-horizontal" action="<?= $location_form; ?>" method="POST" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label" for="parent_nav_id">เลือกเมนูหลัก</label>
                                <div class="col-md-4">
                                <?php
                                $additional_dd_code = "id='selectMainMenu' class='form-control'";

                                    echo form_dropdown('parent_nav_id', $options, $parent_nav_id, $additional_dd_code);
                                ?>
                                </div>
                            </div>
						<div class="form-group">
                            <label for="sub_nav_title">ชื่อซับเมนู <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="sub_nav_title" name="sub_nav_title" value="<?=$sub_nav_title; ?>"  placeholder="Thai Submenu"/>
                            
                            <label for="sub_nav_title_en">English Title</label>
                            <input type="text" class="form-control" id="sub_nav_title_en" name="sub_nav_title_en" value="<?=$sub_nav_title_en; ?>" placeholder="English Submenu" />
                        </div>
						<div class="form-group">
							<?php 
								if(!isset($sub_nav_url)){
									$sub_nav_url="";
								} 
							?>
                            <label for="sub_nav_url">URL </label>
                            <input type="text" class="form-control" id="sub_nav_url" name="sub_nav_url" value="<?=$sub_nav_url; ?>"  placeholder="Submenu Link" />
                        </div>
						<div class="form-group row">
                            <label class="col-md-1 col-form-label" for="sub_nav_status">สถานะ</label>
                            <div class="col-md-4">
							<?php
							$additional_dd_code = "id='selectStatusCode' class='form-control'";
								$options = array(
												'' => 'โปรดเลือกการเผยแพร่',
												'1' => 'Published',
												'0' => 'Unpublished'
											);

								echo form_dropdown('sub_nav_status', $options, $sub_nav_status, $additional_dd_code);
							?>
							</div>
                        </div>
						<div class="form-group row">
                            <label class="col-md-1 col-form-label" for="sub_nav_url">Page Content</label>
                            <div class="col-md-4">                      
        	 				<select name="sub_nav_url_content" id="sub_nav_url_content" class="form-control bgc" tabindex="13">
        	 					<option value="">ไม่ใช้คอนเทนต์ที่มีอยู่</option>
        	 					<?php
        	 						foreach($content_page as $key => $selection ) :?>
        	 						<option value="<?=$selection['path'].$selection['id'];?>" <?php if(isset($sub_nav_url_content) && $sub_nav_url_content ==  $selection['path'].$selection['id']){echo 'selected';}?>>
        	 							<?php 
        	 								if($selection['id'] != ''){
        	 									echo " - ".$selection['title'];
        	 								}
        	 								else{
        	 									echo " ".$selection['title'];
        	 								}
        	 							?></option>
        	 					<?php endforeach;?>  

        	 				</select>
							</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-1 col-form-label" for="sub_nav_content">Content</label>
                            <div class="col-md-11">
								<textarea id="editor4" class="ckeditor" id="sub_nav_content" name="sub_nav_content"><?=$sub_nav_content;?></textarea>
							</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-1 col-form-label" for="order_priority">ลำดับเมนู</label>
                            <div class="col-md-4">
								<select class="form-control-sm" name="order_priority">
									<option <?php if($order_priority==$order_priority){ echo 'selected="selected"'; } ?> value="<?=$order_priority; ?>">
										<?=$order_priority; ?>
									</option>
									<?php for($i=1; $i<=50; $i++){ ?>
										<option value="<?=$i; ?>"><?=$i; ?></option>
									<?php } ?>
								</select>
							</div>
                        </div>
                    	<?php
							if(is_numeric($update_id)){
								echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">แก้ไขเมนูย่อย</button>';
							}else{
								echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">เพิ่มเมนูย่อย</button>';
                    		}
						?>
						<input type="hidden" id="parent" name="parent" value="<?=$parent_nav_id;?>">
                        <button type="submit" id="submit" name="submit" value="Cancel" class="btn btn-sm btn-warning m-r-5">ยกเลิก</button>
                    </form>
				</div>
				<!-- end panel-body -->
			</div>
			<!-- end panel -->
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
</div>
<!-- end #content -->
