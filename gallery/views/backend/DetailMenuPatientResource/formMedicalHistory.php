<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
		<li class="breadcrumb-item active">MedicalHistory</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header"><?=$headline; ?></h1>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">เพิ่มข้อมูลประวัติการรักษา</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<?=validation_errors("<p style='color:red;'>", "</p>") ?>
					<?php
						if(is_numeric($update_id)) {
					?>
                        <div class="form-group row fileupload-buttonbar">
                            <div class="col-md-12">
                                <span class="fileinput-button m-r-3">
									<?php if(!isset($pt_big_img)){ ?>
                                    <i class="fa fa-plus"></i>
                                    <a href="<?=base_url(); ?>backend/src/DetailMenuPatientResource/upload_image/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-primary" id="upload_img" name="upload_img">Upload MedicalHistory Image</button>
                                    </a>
                                	<?php }else{ ?>
                                    <i class="fa fa-times"></i>
									<a href="<?=base_url(); ?>backend/src/DetailMenuPatientResource/delete_image/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-danger" id="delete_img" name="delete_img">Delete MedicalHistory Image</button>
                                    </a>
                                	<?php } ?>
                                    <a href="<?=base_url(); ?>backend/src/DetailMenuPatientResource/delete_medicalhistory/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-danger" id="delete_medicalhistory" name="delete_medicalhistory">Delete MedicalHistory</button>
                                    </a>
                                    <a href="<?=base_url(); ?>backend/src/DetailMenuPatientResource/view_medicalhistory_details/<?=$update_id; ?>">
                                        <button type="button" class="btn btn-success" id="view_medicalhistory" name="view_medicalhistory">View MedicalHistory In Front</button>
                                    </a>
                                </span>
                            </div>
                        </div>
					<?php }
						$location_form = base_url()."backend/src/DetailMenuPatientResource/create/".$update_id;
					?>
					<form class="form-horizontal" action="<?= $location_form; ?>" method="POST" enctype="multipart/form-data">
						<div class="row row-space-10">
							<div class="col-md-12">
								<div class="form-group">
		                            <label for="pt_title">ชื่อ Title <span class="text-danger">*</span></label>
		                            <input type="text" class="form-control" id="pt_title" name="pt_title" value="<?=$pt_title; ?>" />
		                        </div>
		                    </div>
		                </div>
		                <div class="row row-space-10">
		                    <div class="col-md-12">
		                        <div class="form-group">
		                            <label for="pt_description">MedicalHistory Contents</label>
		                            <textarea id="editor1" class="ckeditor" name="pt_description"><?=$pt_description; ?></textarea>
		                        </div>
		                    </div>
		                </div>
						<div class="form-group row">
                            <div class="col-md-6">
								<div class="form-group">
	                                <label for="date_published">Date Published <span class="text-danger">*</span></label>
	                                <input type="text" class="form-control" id="datepicker-default" name="date_published" placeholder="Select Date" value="<?=$date_published; ?>" />
	                            </div>
	                        </div>
                            <div class="col-md-6">
                            <label for="pt_status">สถานะ</label>
							<?php
							$additional_dd_code = "id='selectStatusCode' class='form-control'";
								$options = array(
												'' => 'โปรดเลือกการเผยแพร่',
												'1' => 'Published',
												'0' => 'Unpublished'
											);

								echo form_dropdown('pt_status', $options, $pt_status, $additional_dd_code);
							?>
							</div>
                        </div>
						<div class="form-group row">
                            <div class="col-md-4">
								<div class="form-group">
		                            <label for="meta_author">Meta Author</label>
		                            <input type="text" class="form-control" id="meta_author" name="meta_author" value="<?=$meta_author; ?>" />
	                            </div>  
		                    </div>
                            <div class="col-md-4">
								<div class="form-group">
		                            <label for="meta_keyword">Meta Keyword</label>
		                            <input type="text" class="form-control" id="meta_keywords" name="meta_keywords" value="<?=$meta_keywords; ?>" />
	                            </div>  
		                    </div>
                            <div class="col-md-4">
								<div class="form-group">
		                            <label for="meta_desciption">Meta Description</label>
		                            <input type="text" class="form-control" id="meta_description" name="meta_description" value="<?=$meta_description; ?>" />
	                            </div>  
		                    </div>
                        </div>
						<?php if($big_imgs!=''){ //ต้องทำการส่งค่า blog_big_img มาจาก controller BlogHealthy->create ก่อน ?>
                        <div class="form-group row">
                            <label class="col-md-1 col-form-label">Big Image</label>
                            <div class="col-md-4">
								<img src="<?=base_url(); ?>gallery/MedicalHistory/big_imgs/<?=$big_imgs; ?>">
							</div>
                        </div>
                    	<?php }
							if(is_numeric($update_id)){
								echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">แก้ไขข้อมูล</button>';
							}else{
								echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">เพิ่มข้อมูล</button>';
                    		}
						?>
                        <button type="submit" id="submit" name="submit" value="Cancel" class="btn btn-sm btn-warning m-r-5">ยกเลิก</button>
                    </form>
				</div>
				<!-- end panel-body -->
			</div>
			<!-- end panel -->
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
</div>
<!-- end #content -->
