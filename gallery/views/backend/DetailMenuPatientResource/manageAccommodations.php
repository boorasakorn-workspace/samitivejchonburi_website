<!-- begin #content -->
<div id="content" class="content">		
	<h1>จัดการส่วนข้อมูลผู้ป่วย - ห้องพัก</h1>
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
		<li class="breadcrumb-item active">รายละเอียดคอนเทนต์ห้องพัก</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<?php
		$create_header_url = base_url()."backend/src/DetailMenuPatientResource/create/".$pages='Accommodations';
	?>
	<h1 class="page-header"><a href="<?=$create_header_url; ?>"><button type="button" class="btn btn-lime">Add New Content Accommodations</button></a></small></h1>
	
	<?php
		if(isset($flash)){
			echo $flash;
		}
	?>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-lg-12">
			<div class="panel panel-inverse">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">จัดการคอนเทนต์ห้องพัก</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<table id="data-table-buttons" class="table table-bordered">
						<thead>
							<tr>
								<th width="1%" class="text-nowrap">#</th>
								<th class="text-nowrap">Title Name</th>
								<th class="text-nowrap">สถานะ</th>
								<th class="text-nowrap">แก้ไข</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$view_accom_url = base_url()."backend/src/DetailMenuPatientResource/view_accom_details/".$accom->id;
								$edit_accom_url = base_url()."backend/src/DetailMenuPatientResource/create/".$pages='Accommodations'.'/'.$accom->id;
								$delete_accom_url = base_url()."backend/src/DetailMenuPatientResource/delete_PatientResource/".$accom->id;
								$status = $accom->pt_status;
								if($status == 1){
									$status_label = "green";
									$status_desc = "Published";
								}else{
									$status_label = "secondary";
									$status_desc = "Unpublished";
								}
							?>
							<tr class="odd gradeX">
								<td width="1%"><?=$accom->id; ?></td>
								<td><?=$accom->pt_title; ?></td>
								<td class="text-center">
									<span class="label label-<?=$status_label; ?>"><?=$status_desc; ?></span>
								</td>
								<td width="30%" class="text-center">
									<a class="btn btn-xs btn-success" href="<?=$view_accom_url; ?>">
										<i class="fas fa-eye"></i>
									</a>
									<a class="btn btn-xs btn-warning" href="<?=$edit_accom_url; ?>">
										<i class="fas fa-edit"></i>
									</a>
									<a class="btn btn-xs btn-danger" href="<?=$delete_accom_url; ?>">
										<i class="fas fa-trash"></i>
									</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- end panel-body -->
			</div>
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
</div>