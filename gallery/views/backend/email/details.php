<div id="content" class="content content-full-width inbox">
			<!-- begin vertical-box -->
			<div class="vertical-box with-grid">
				<!-- begin vertical-box-column -->
				<div class="vertical-box-column width-200 bg-silver hidden-xs">
					<!-- begin vertical-box -->
					<div class="vertical-box">
						<!-- begin wrapper -->
						<div class="wrapper bg-silver text-center">
							<a href="email_compose.html" class="btn btn-inverse p-l-40 p-r-40 btn-sm">
								Compose
							</a>
						</div>
						<!-- end wrapper -->
						<!-- begin vertical-box-row -->
						<div class="vertical-box-row">
							<!-- begin vertical-box-cell -->
							<div class="vertical-box-cell">
								<!-- begin vertical-box-inner-cell -->
								<div class="vertical-box-inner-cell">
									<!-- begin scrollbar -->
									<div data-scrollbar="true" data-height="100%">
										<!-- begin wrapper -->
										<div class="wrapper p-0">
											<div class="nav-title"><b>FOLDERS</b></div>
											<?php $this->view($sidelistemail); ?>
										</div>
										<!-- end wrapper -->
									</div>
									<!-- end scrollbar -->
								</div>
								<!-- end vertical-box-inner-cell -->
							</div>
							<!-- end vertical-box-cell -->
						</div>
						<!-- end vertical-box-row -->
					</div>
					<!-- end vertical-box -->
				</div>
				<!-- end vertical-box-column -->
				<!-- begin vertical-box-column -->
				<div class="vertical-box-column bg-white">
					<!-- begin vertical-box -->
					<div class="vertical-box">
						<!-- begin wrapper -->
						<!-- 
						<div class="wrapper bg-silver-lighter clearfix">
							<div class="pull-left">
								<div class="btn-group m-r-5">
									<a href="javascript:;" class="btn btn-white btn-sm"><i class="fa fa-reply f-s-14 m-r-3 m-r-xs-0 t-plus-1"></i> <span class="hidden-xs">Reply</span></a>
								</div>
								<div class="btn-group m-r-5">
									<a href="javascript:;" class="btn btn-white btn-sm"><i class="fa fa-trash f-s-14 m-r-3 m-r-xs-0 t-plus-1"></i> <span class="hidden-xs">Delete</span></a>
									<a href="javascript:;" class="btn btn-white btn-sm"><i class="fa fa-archive f-s-14 m-r-3 m-r-xs-0 t-plus-1"></i> <span class="hidden-xs">Archive</span></a>
								</div>
							</div>
							<div class="pull-right">
								<div class="btn-group">
									<a href="email_inbox.html" class="btn btn-white btn-sm disabled"><i class="fa fa-arrow-up f-s-14 t-plus-1"></i></a>
									<a href="email_inbox.html" class="btn btn-white btn-sm"><i class="fa fa-arrow-down f-s-14 t-plus-1"></i></a>
								</div>
								<div class="btn-group m-l-5">
									<a href="email_inbox.html" class="btn btn-white btn-sm"><i class="fa fa-times f-s-14 t-plus-1"></i></a>
								</div>
							</div>
						</div>
						-->
						<!-- end wrapper -->
						<!-- begin vertical-box-row -->
						<div class="vertical-box-row">
							<!-- begin vertical-box-cell -->
							<div class="vertical-box-cell">
								<!-- begin vertical-box-inner-cell -->
								<div class="vertical-box-inner-cell">
									<!-- begin scrollbar -->
									<div data-scrollbar="true" data-height="100%">
										<!-- begin wrapper -->
										<div class="wrapper">
											<h3 class="m-t-0 m-b-15 f-w-500"><?=$query->email_subject;?></h3>
											<ul class="media-list underline m-b-15 p-b-15">
												<li class="media media-sm clearfix">
													<a href="javascript:;" class="pull-left">
														<img class="media-object rounded-corner" alt="" src="../assets/img/user/user-12.jpg" />
													</a>
													<div class="media-body">
														<div class="email-from text-inverse f-s-14 f-w-600 m-b-3">
															from <?=$query->email_from;?>
														</div>
														<div class="m-b-3"><i class="fa fa-clock fa-fw"></i> Today, 8:30 AM</div>
														<div class="email-to">
															To: <?=$query->email_target;?>
														</div>
													</div>
												</li>
											</ul>
											<!-- 
											<ul class="attached-document clearfix">
												<li class="fa-file">
													<div class="document-file">
														<a href="javascript:;">
															<i class="fa fa-file-pdf"></i>
														</a>
													</div>
													<div class="document-name"><a href="javascript:;">flight_ticket.pdf</a></div>
												</li>
												<li class="fa-camera">
													<div class="document-file">
														<a href="javascript:;">
															<img src="../assets/img/gallery/gallery-11.jpg" alt="" />
														</a>
													</div>
													<div class="document-name"><a href="javascript:;">front_end_mockup.jpg</a></div>
												</li>
											</ul> -->

											<p class="f-s-12 text-inverse p-t-10">
												<?=$query->email_message;?>
											</p>
										</div>
										<!-- end wrapper -->
									</div>
									<!-- end scrollbar -->
								</div>
								<!-- end vertical-box-inner-cell -->
							</div>
							<!-- end vertical-box-cell -->
						</div>
						<!-- end vertical-box-row -->
						<!-- begin wrapper -->
						<!-- 
						<div class="wrapper bg-silver-lighter text-right clearfix">
							<div class="btn-group">
								<a href="email_inbox.html" class="btn btn-white btn-sm disabled"><i class="fa fa-arrow-up"></i></a>
								<a href="email_inbox.html" class="btn btn-white btn-sm"><i class="fa fa-arrow-down"></i></a>
							</div>
							<div class="btn-group m-l-5">
								<a href="email_inbox.html" class="btn btn-white btn-sm"><i class="fa fa-times"></i></a>
							</div>
						</div> 
						-->
						<!-- end wrapper -->
					</div>
					<!-- end vertical-box -->
				</div>
				<!-- end vertical-box-column -->
			</div>
			<!-- end vertical-box -->
		</div>