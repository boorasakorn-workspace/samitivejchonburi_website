<!-- begin #content -->
<div id="content" class="content">		
	<h1>จัดการส่วน Menu</h1>
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
		<li class="breadcrumb-item active">รายละเอียด Menu</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<?php
		$create_navbar_url = base_url()."backend/src/Sidebar/create";
		$create_subnavbar_url = base_url()."backend/src/Sidebar/create_subnav";
	?>
	<h1 class="page-header">
		<a href="<?=$create_navbar_url; ?>"><button type="button" class="btn btn-lime">Add New Menu</button></a>
		<!-- <a href="<?=$create_subnavbar_url; ?>"><button type="button" class="btn btn-lime">Add New Sub Menu</button></a></small> -->
	</h1>


	<?php
		if(isset($flash)){
			echo $flash;
		}
	?>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-lg-12">
			<div class="panel panel-inverse">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">จัดการ Menu</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<table id="data-table-buttons" class="table table-bordered">
						<thead>
							<tr>
								<th width="1%" class="text-nowrap">#</th>
								<th class="text-nowrap">ชื่อเมนู</th>
								<th class="text-nowrap">เมนูย่อยของเมนูหลัก</th>
								<th class="text-nowrap">ลำดับเมนู</th>
								<th class="text-nowrap">สถานะ</th>
								<th class="text-nowrap">แก้ไข</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach($query->result() as $rows){
								$num_sub_navigation = $subnav->_count_sub_nav();

								$view_menu_url = base_url()."backend/src/Sidebar/view_menu_details/".$rows->id;
								$edit_menu_url = base_url()."backend/src/Sidebar/create/".$rows->id;
								$delete_menu_url = base_url()."backend/src/Sidebar/delete_menu/".$rows->id;
								$status = $rows->sidemenu_status;
								if($status == 1){
									$status_label = "green";
									$status_desc = "Published";
								}else{
									$status_label = "secondary";
									$status_desc = "Unpublished";
								}
							?>
							<tr class="odd gradeX">
								<td width="1%" class="text-nowrap"><?=$rows->id; ?></td>
								<td><?=$rows->sidemenu_title; ?></td>
								<td>
									<?php
										if($num_sub_navigation < 1){
											echo "-";
										}else{
											if($num_sub_navigation == 1){
												$entity = "คลิกเพื่อดูเมนูย่อย";
											}else{
												$entity = "คลิกเพื่อดูเมนูย่อย";
											}

											$sub_sidemenu_url = base_url()."backend/src/Sidebar/manage_subnav/".$rows->id;

	echo '<a class="btn btn-block btn-purple" href="'.$sub_sidemenu_url.'"><i class="fas fa-eye"></i> '.$entity.'</a>';
										}
									?>
								</td>
								<td class="text-nowrap"><?=$rows->order_priority; ?></td>
								<td>
									<span class="label label-<?=$status_label; ?>"><?=$status_desc; ?></span>
								</td>
								<td width="30%" class="text-center">
									<a class="btn btn-xs btn-success" href="<?=$view_menu_url; ?>">
										<i class="fas fa-eye"></i>
									</a>
									<a class="btn btn-xs btn-warning" href="<?=$edit_menu_url; ?>">
										<i class="fas fa-edit"></i>
									</a>
									<a class="btn btn-xs btn-danger" href="<?=$delete_menu_url; ?>">
										<i class="fas fa-trash"></i>
									</a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
				<!-- end panel-body -->
			</div>
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
</div>