<script>
	$(document).ready(function() {
	    $("#Vidsubmit").click(function(e){
            e.preventDefault(); 
            var DoctorVidForm = document.getElementById('DoctorVidForm');
            var formData = new FormData(DoctorVidForm);

	        $.ajax({
			  url: "<?=base_url(); ?>backend/src/Doctor/modal_do_upload_vid/<?=$update_id; ?>",  
			  type: 'POST',
              data: formData,
             processData:false,
             contentType:false,
             cache:false,
             async:false,
			  success: function(result){
			  	console.log(JSON.parse(result));
			  	$('#VidPreviewSection').removeAttr('style');

			  	var addPreviewVidSection = '<div id="preview_video_section"></div>';
			  	$('#previd_section').html(addPreviewVidSection);

			  	var preview_vid_quote = '<video style="height:320px; width:260px;" src="<?=base_url(); ?>gallery/doctor/preview_vids/'+JSON.parse(result)+'" controls></video>';
			  	$('#doctor_video').val(result);
			  	$('#preview_video_section').html(preview_vid_quote);

	        	$('#uploadVidModal').modal('hide');
			  }
			});
	        
	    });

	    $("#deletePreviewVid").click(function(e){
            e.preventDefault(); 
	        $.ajax({
	        	url: "<?=base_url(); ?>backend/src/Doctor/delete_preview_video/<?=$update_id; ?>",
				processData:false,
				contentType:false,
				cache:false,
				async:false,
			  success: function(result){
			  	$("#VidPreviewSection").css({
			  		'opacity': '0' ,
			  		'height': '0px' ,
			  		'width': '0px'
			  	});

			  	$('#doctor_video').val("");

			  	var addPreviewVidSection = '';
			  	$('#previd_section').html(addPreviewVidSection);
			  }
			});
	        
	    });


	    $("#Imagesubmit").click(function(e){
            e.preventDefault(); 
            var DoctorImageForm = document.getElementById('DoctorImageForm');
            var formData = new FormData(DoctorImageForm);

	        $.ajax({
			  url: "<?=base_url(); ?>backend/src/Doctor/modal_do_upload/<?=$update_id; ?>",  
			  type: 'POST',
              data: formData,
             processData:false,
             contentType:false,
             cache:false,
             async:false,
			  success: function(result){
			  	$('#PreviewSection').removeAttr('style');

			  	var addPreviewSection = '<div id="preview_image_section"></div>';
			  	$('#preimage_section').html(addPreviewSection);

			  	var preview_quote = '<img class="img-responsive" style="height:320px; width:260px;" src="<?=base_url(); ?>gallery/doctor/preview_imgs/'+JSON.parse(result)+'" >';
			  	$('#preview_image_section').html(preview_quote);

	        	$('#uploadImgModal').modal('hide');
			  }
			});
	        
	    });

	    $("#deletePreview").click(function(e){
            e.preventDefault(); 
	        $.ajax({
	        	url: "<?=base_url(); ?>backend/src/Doctor/delete_preview/<?=$update_id; ?>",
				processData:false,
				contentType:false,
				cache:false,
				async:false,
			  success: function(result){
			  	$("#PreviewSection").css({
			  		'opacity': '0' ,
			  		'height': '0px' ,
			  		'width': '0px'
			  	});

			  	var addPreviewSection = '';
			  	$('#preimage_section').html(addPreviewSection);
			  }
			});
	        
	    });


	    $("#addParent").click(function(e){
            e.preventDefault(); 
            var addParentText = $("#parent option:selected").val();
            if($("#parent_medical_center_name").val() != '' && addParentText != ''){
            	addParentText = "|"+addParentText;
            }
            $("#parent_medical_center_name").val( $("#parent_medical_center_name").val() + addParentText);
	    });
	    
	    $("#addSpecialtyParent").click(function(e){
            e.preventDefault(); 
            var addSpecialtyParentText = $("#parent_specialty option:selected").val();
            if($("#parent_specialty_name").val() != '' && addSpecialtyParentText != ''){
            	addSpecialtyParentText = "|"+addSpecialtyParentText;
            }
            $("#parent_specialty_name").val( $("#parent_specialty_name").val() + addSpecialtyParentText);
	    });
	    
	    $("#addSubSpecialtyParent").click(function(e){
            e.preventDefault(); 
            var addSubSpecialtyParentText = $("#parent_sub_specialty option:selected").val();
            if($("#parent_sub_specialty_name").val() != '' && addSubSpecialtyParentText != ''){
            	addSubSpecialtyParentText = "|"+addSubSpecialtyParentText;
            }
            $("#parent_sub_specialty_name").val( $("#parent_sub_specialty_name").val() + addSubSpecialtyParentText);
	    });

	    $("#previewContent").click(function(e){
            e.preventDefault(); 
            var formContents = document.getElementById('formContents');
            var formData = new FormData(formContents);

	        $.ajax({
	        	url: "<?=base_url(); ?>backend/src/Doctor/preview_Contents",
				type: 'POST',
				data: formData,
				processData:false,
				contentType:false,
				cache:false,
				async:false,
			  success: function(result){
			  	var w = window.open('about:blank');
			    w.document.open();
			    w.document.write(result);
			  }
			});	        
	    });

	});
</script>	