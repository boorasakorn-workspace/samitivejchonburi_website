<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<title>Samitivej Chonburi</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />

<!-- Favicon -->
<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" type="image/x-icon" />
<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" />
	<!-- logo icon -->
    <link rel="icon" type="image/png" href="https://www.samitivejhospitals.com/th/wp-content/themes/svh/images/svh-favicons-standard.png" />
    <link rel="icon" type="image/x-icon" href="https://www.samitivejhospitals.com/th/wp-content/themes/svh/images/svh-favicons-standard-ico.ico" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="https://www.samitivejhospitals.com/th/wp-content/themes/svh/images/svh-homescreen-144px.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="https://www.samitivejhospitals.com/th/wp-content/themes/svh/images/svh-homescreen-72px.png" />
    <link rel="apple-touch-icon-precomposed" href="https://www.samitivejhospitals.com/th/wp-content/themes/svh/images/svh-homescreen-57px.png" />

	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets_b/plugins/jquery-ui/jquery-ui.min.css?v=1001" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets_b/plugins/bootstrap/4.1.3/css/bootstrap.min.css?v=1001" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets_b/plugins/font-awesome/5.3/css/all.min.css?v=1001" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets_b/plugins/animate/animate.min.css?v=1001" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets_b/css/default/style.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets_b/css/default/style-responsive.min.css?v=1001" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets_b/css/default/theme/default.css?v=1001" rel="stylesheet" id="theme" />
	<!-- ================== END BASE CSS STYLE ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL CSS STYLE ================== -->
	<link href="<?php echo base_url(); ?>assets_b/plugins/jquery-jvectormap/jquery-jvectormap.css?v=1001" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets_b/plugins/bootstrap-calendar/css/bootstrap_calendar.css?v=1001" rel="stylesheet" />
	<!-- <link href="<?php echo base_url(); ?>assets_b/plugins/gritter/css/jquery.gritter.css?v=1001" rel="stylesheet" /> -->
	<link href="<?php echo base_url(); ?>assets_b/plugins/nvd3/build/nv.d3.css?v=1001" rel="stylesheet" />
	<!-- ================== END PAGE LEVEL CSS STYLE ================== -->
	
	<link href="<?php echo base_url(); ?>assets_b/plugins/bootstrap-eonasdan-datetimepicker/build/css/bootstrap-datetimepicker.min.css?v=1001" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets_b/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css?v=1001" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets_b/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css?v=1001" rel="stylesheet" />
	
	<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
	<link href="<?php echo base_url(); ?>assets_b/plugins/DataTables/media/css/dataTables.bootstrap.min.css?v=1001" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets_b/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css?v=1001" rel="stylesheet" />
	<!-- ================== END PAGE LEVEL STYLE ================== -->

	
	<link href="<?php echo base_url(); ?>assets_b//plugins/jquery-tag-it/css/jquery.tagit.css?v=1001" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets_b//plugins/bootstrap-wysihtml5/src/bootstrap3-wysihtml5.css?v=1001" rel="stylesheet" />

	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url(); ?>assets_b/plugins/pace/pace.min.js?v=1001"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
	<link href="<?php echo base_url(); ?>assets_b/plugins/parsley/src/parsley.css?v=1001" rel="stylesheet" />
	<!-- ================== END PAGE LEVEL STYLE ================== -->

	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">

</head>