
	<script>
		CKEDITOR.replace( 'editor4', {
	        extraPlugins : 'image2,codesnippet,uploadimage,tableresize',

	        codeSnippet_theme: 'monokai_sublime',
	        height: 250,
	        enterMode: CKEDITOR.ENTER_BR,
	        filebrowserImageUploadUrl : '<?=base_url();?>backend/Ck_upload/upload_ck?type=image&path=work',
	        filebrowserUploadUrl : '<?=base_url();?>backend/Ck_upload/upload_ck?type=image&path=work',
	    });
	</script>