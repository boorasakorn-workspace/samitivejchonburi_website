
		<div id="sidebar" class="sidebar">
			<!-- begin sidebar scrollbar -->
			<div data-scrollbar="true" data-height="100%">
				<!-- begin sidebar user -->
				<ul class="nav">
					<li class="nav-profile">
						<a href="javascript:;" data-toggle="nav-profile">
							<div class="cover with-shadow"></div>
							<div class="image">
								<img src="<?php echo base_url(); ?>gallery/account/big_imgs/<?=$pic; ?>" alt="" />
							</div>
							<div class="info">
								<!-- <b class="caret pull-right"></b> -->
								<?=$username; ?>
								<small><?php if($position == '100'){ echo "Administrator System"; } ?></small>
							</div>
						</a>
					</li>
					<!-- <li>
						<ul class="nav nav-profile">
							<li><a href="javascript:;"><i class="fa fa-cog"></i> Settings</a></li>
							<li><a href="javascript:;"><i class="fa fa-pencil-alt"></i> Send Feedback</a></li>
							<li><a href="javascript:;"><i class="fa fa-question-circle"></i> Helps</a></li>
						</ul>
					</li> -->
				</ul>
				<!-- end sidebar user -->
				<!-- begin sidebar nav -->
				<ul class="nav">
					<li class="nav-header">Menu List</li>
					<li>
						<a href="<?=base_url(); ?>backend/Dashboard">
							<i class="fa fa-th-large"></i>
							<span>Dashboard</span>
						</a>
					</li>
					<li class="has-sub">
						<a href="javascript:;">
							<span class="badge pull-right"><?=$count;?></span>
							<i class="fa fa-hdd"></i> 
							<span>Email</span>
						</a>
						<ul class="sub-menu">
							<li><a href="<?=base_url(); ?>backend/src/Email/Inbox">Inbox</a></li>
							<li><a href="<?=base_url(); ?>backend/src/Email/Compose">Compose</a></li>
						</ul>
					</li> 
					<li class="has-sub">
						<a href="javascript:;">
							<b class="caret"></b>
							<i class="fa fa-gem"></i>
							<span>CMS</span> 
						</a>
						<ul class="sub-menu">
							<li>
								<a href="<?=base_url(); ?>backend/src/Header/manage">
									<i class="fa fa-th-large"></i>
									<span>จัดการ Header</span>
								</a>
							</li>
							<li>
								<a href="<?=base_url(); ?>backend/src/Navbar/manage">
									<i class="fa fa-th-large"></i>
									<span>จัดการเมนู</span>
								</a>
							</li>
							<li>
								<a href="<?=base_url(); ?>backend/src/sidebar/manage">
									<i class="fa fa-th-large"></i>
									<span>จัดการเมนูข้าง</span>
								</a>
							</li>
							<li>
								<a href="<?=base_url(); ?>backend/src/MainCarousel/manage">
									<i class="fa fa-th-large"></i>
									<span>จัดการบทความสไลด์</span>
								</a>
							</li>
							<li>
								<a href="<?=base_url(); ?>backend/src/BlogHealthy/manage">
									<i class="fa fa-th-large"></i>
									<span>จัดการบทความสุขภาพ</span>
								</a>
							</li>
							<li>
								<a href="<?=base_url(); ?>backend/src/Package/manage">
									<i class="fa fa-th-large"></i>
									<span>จัดการแพ็กเกจโปรโมชั่น</span>
								</a>
							</li>
							<li>
								<a href="<?=base_url(); ?>backend/src/News/manage">
									<i class="fa fa-th-large"></i>
									<span>จัดการข่าวสารและกิจกรรม</span>
								</a>
							</li>
							<!-- 
							<li class="has-sub">
								<a href="javascript:;">
									<b class="caret"></b>
									<i class="fa fa-gem"></i>
									<span>ข้อมูลสำหรับผู้ป่วย</span> 
								</a>
								<ul class="sub-menu">
									<li>
										<a href="<?=base_url(); ?>backend/src/DetailMenuPatientResource/manage/<?php echo $detail='MedicalHistory'; ?>">
											<i class="fa fa-th-large"></i>
											<span>ประวัติการรักษา</span>
										</a>
									</li>
									<li>
										<a href="<?=base_url(); ?>backend/src/DetailMenuPatientResource/manage/<?php echo $detail='Insurance'; ?>">
											<i class="fa fa-th-large"></i>
											<span>ข้อมูลประกันสุขภาพ</span>
										</a>
									</li>
									<li>
										<a href="<?=base_url(); ?>backend/src/DetailMenuPatientResource/manage/<?php echo $detail='DirectDisbursementProject'; ?>">
											<i class="fa fa-th-large"></i>
											<span>โครงการเบิกจ่ายตรง</span>
										</a>
									</li>
									<li>
										<a href="<?=base_url(); ?>backend/src/DetailMenuPatientResource/manage/<?php echo $detail='Accommodations'; ?>">
											<i class="fa fa-th-large"></i>
											<span>ห้องพัก</span>
										</a>
									</li>
									<li>
										<a href="<?=base_url(); ?>backend/src/DetailMenuPatientResource/manage/<?php echo $detail='RestaurantsShop'; ?>">
											<i class="fa fa-th-large"></i>
											<span>ร้านอาหาร และอื่นๆ</span>
										</a>
									</li>
									<li>
										<a href="<?=base_url(); ?>backend/src/DetailMenuPatientResource/manage/<?php echo $detail='MedicalRecordsReports'; ?>">
											<i class="fa fa-th-large"></i>
											<span>รายงานผลการรักษา</span>
										</a>
									</li>
									<li>
										<a href="<?=base_url(); ?>backend/src/DetailMenuPatientResource/manage/<?php echo $detail='FinancialBillingMatters'; ?>">
											<i class="fa fa-th-large"></i>
											<span>ข้อมูลทางการเงิน</span>
										</a>
									</li>
								</ul>
							</li>
							 -->
							<!-- 
							<li>
								<a href="<?=base_url(); ?>backend/src/Contents/manage">
									<i class="fa fa-th-large"></i>
									<span>จัดการคอนเทนต์</span>
								</a>
							</li> 
							-->
						</ul>
					</li>
					<li class="has-sub">
						<a href="javascript:;">
							<b class="caret"></b>
							<i class="fa fa-user-md"></i>
							<span>จัดการส่วนของแพทย์</span> 
						</a>
						<ul class="sub-menu">
							<li>
								<a href="<?=base_url(); ?>backend/src/Doctor/manage">
									<span>ตารางข้อมูลแพทย์</span>
								</a>
							</li>
							<li>
								<a href="<?=base_url(); ?>backend/src/DoctorWork/manage">
									<span>ตารางวันและเวลาปฏิบัติงาน</span>
								</a>
							</li>
						</ul>
					</li>
					<li class="has-sub">
						<a href="javascript:;">
							<b class="caret"></b>
							<i class="fas fa-hospital"></i>
							<span>จัดการศูนย์พยาบาล</span>
						</a>
						<ul class="sub-menu">
							<li>
								<a href="<?=base_url(); ?>backend/src/Medicalcenter/manage">
									<span>ตารางข้อมูลศูนย์พยาบาล</span>
								</a>
							</li>
							<li>
								<a href="<?=base_url(); ?>backend/src/Specialty/manage">
									<span>Specialty</span>
								</a>
							</li>
							<li>
								<a href="<?=base_url(); ?>backend/src/Specialty/manage_sub">
									<span>Sub Specialty</span>
								</a>
							</li>
						</ul>
					</li>
					<li class="has-sub">
						<a href="javascript:;">
							<b class="caret"></b>
							<i class="fas fa-user"></i>
							<span>Manage Account</span>
						</a>
						<ul class="sub-menu">
							<li>
								<a href="<?=base_url(); ?>backend/Account/manage">
									<span>ตารางแอคเคาท์</span>
								</a>
							</li>
						</ul>
					</li>
					<!-- begin sidebar minify button -->
					<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
					<!-- end sidebar minify button -->
				</ul>
				<!-- end sidebar nav -->
			</div>
			<!-- end sidebar scrollbar -->
		</div>
		<div class="sidebar-bg"></div>