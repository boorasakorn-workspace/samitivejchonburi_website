<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
		<li class="breadcrumb-item active">Specialty</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header"><?=$headline; ?></h1>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<h4 class="panel-title">Sub Specialty</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<?php if(isset($error_report)){
						echo "<p style='color:red;'>". $error_report ."</p>";
					}
					?>
					<?=validation_errors("<p style='color:red;'>", "</p>") ?>
					<?php
					if(is_numeric($update_id)) {
						?>
						<div class="form-group row fileupload-buttonbar">
							<div class="col-md-12">
								<span class="fileinput-button m-r-3">
									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">Delete Sub Specialty</button>

									<!-- Modal -->
									<div id="deleteModal" class="modal fade" role="dialog">
									  <div class="modal-dialog">
									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">Delete Sub Specialty</h4>
									      </div>
									      <div class="modal-body">
									        <p>Sure to delete sub specialty?</p>
									      </div>
									      <div class="modal-footer">
											<a href="<?=base_url(); ?>backend/src/Specialty/delete_sub_specialty/<?=$update_id; ?>">
												<button type="button" class="btn btn-danger" id="delete_sub_specialty" name="delete_sub_specialty">Delete</button>
											</a>
									        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
									      </div>
									    </div>

									  </div>
									</div>
								</span>
							</div>
						</div>
					<?php }
					$location_form = base_url()."backend/src/Specialty/create_sub/".$update_id;
					?>

					<form class="form-horizontal" action="<?= $location_form; ?>" method="POST" enctype="multipart/form-data">

						<div class="hljs-wrapper">
							<h4 class="text-lime"><span class="bold">Sub Specialty</span></h4>
						</div>
						<div class="row row-space-10">
							<div class="col-md-4">
								<div class="form-group">
									<label for="parent">Select Parent</label><label style="float: right;">  |  <button style="padding:1px;" class="btn btn-primary" type="button" id="addParent">ADD</button></label>
									<select class="form-control" name="parent" id="parent">
										<option value="">Select Parent</option>
										<?php
											foreach ($dropdown_parent as $dropdown):
										?>
											<option value="<?=$dropdown->medical_center_name;?>">
												<?=$dropdown->medical_center_name;?>
											</option>
										<?php
											endforeach
										?>									
									</select>
								</div>
							</div>
							<div class="col-md-8">
								<div class="form-group">
									<label for="parent_name">Parent</label>
									<input type="text" class="form-control" id="parent_name" name="parent_name" value="<?php if(isset($parent_name)){echo $parent_name;} ?>" />
								</div>
							</div>
						</div>
						<div class="row row-space-10">
							<div class="col-md-4">
								<div class="form-group">
									<label for="parent_specialty">Select Specialty</label><label style="float: right;">  |  <button style="padding:1px;" class="btn btn-primary" type="button" id="addSpecialtyParent">ADD</button></label>
									<select class="form-control" name="parent_specialty" id="parent_specialty">
										<option value="">Select Specialty Parent</option>
										<?php
											foreach ($dropdown_specialty_parent as $dropdown_specialty):
										?>
											<option value="<?=$dropdown_specialty->specialty_name_th;?>">
												<?=$dropdown_specialty->specialty_name_th;?>
											</option>
										<?php
											endforeach
										?>								
									</select>
								</div>
							</div>
							<div class="col-md-8">
								<div class="form-group">
									<label for="parent_specialty_name">Specialty Parent</label>
									<input type="text" class="form-control" id="parent_specialty_name" name="parent_specialty_name" value="<?php if(isset($parent_specialty_name)){echo $parent_specialty_name;} ?>" />
								</div>
							</div>
						</div>
						<div class="row row-space-10">
							<div class="col-md-4">
								<div class="form-group">
									<label for="sub_specialty_name_th">Sub Specialty TH<span class="text-danger">*</span></label>
									<input type="text" class="form-control" id="sub_specialty_name_th" name="sub_specialty_name_th" value="<?=$sub_specialty_name_th; ?>" />
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="sub_specialty_name_en">Sub Specialty EN<span class="text-danger">*</span></label>
									<input type="text" class="form-control" id="sub_specialty_name_en" name="sub_specialty_name_en" value="<?=$sub_specialty_name_en; ?>" />
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="status">Status</label>
									<select class="form-control" name="status">
										<option value="1">Active</option>
										<option value="0">Inactive</option>									
									</select>
								</div>
							</div>
						</div>
						<?php 
						if(is_numeric($update_id)){
							echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">Edit SubSpecialty</button>';
						}else{
							echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">Add SubSpecialty</button>';
						}
						?>
						<button type="submit" id="submit" name="submit" value="Cancel" class="btn btn-sm btn-warning m-r-5">ยกเลิก</button>
					</form>
				</div>
				<!-- end panel-body -->
			</div>
			<!-- end panel -->
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
</div>
<!-- end #content -->
