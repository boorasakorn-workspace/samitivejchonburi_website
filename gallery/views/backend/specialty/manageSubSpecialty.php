<!-- begin #content -->
<div id="content" class="content">		
	<h1>Sub Specialty Manage</h1>
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
		<li class="breadcrumb-item active">Specialty</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<?php
		$create_specialty_url = base_url()."backend/src/Specialty/create";
		$create_subspecialty_url = base_url()."backend/src/Specialty/create_sub";
	?>
	<h1 class="page-header">
		<!-- <a href="<?=$create_specialty_url; ?>"><button type="button" class="btn btn-lime">Add Specialty</button></a> -->
		<a href="<?=$create_subspecialty_url; ?>"><button type="button" class="btn btn-lime">Add Sub Specialty</button></a></small>
	</h1>


	<?php
		if(isset($flash)){
			echo $flash;
		}
	?>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-lg-12">
			<div class="panel panel-inverse">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<h4 class="panel-title">Sub Specialty Manage</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<table id="data-table-buttons" class="table table-bordered">
						<thead>
							<tr>
								<th width="1%" class="text-nowrap">#</th>
								<th class="text-nowrap">SubSpecialty</th>
								<th class="text-nowrap">Status</th>
								<th class="text-nowrap">Manage</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach($query->result() as $rows){
								$edit_menu_url = base_url()."backend/src/Specialty/create_sub/".$rows->id;
								$delete_menu_url = base_url()."backend/src/Specialty/delete_sub_specialty/".$rows->id;
								$status = $rows->status;
								if($status == 1){
									$status_label = "green";
									$status_desc = "Active";
								}else{
									$status_label = "secondary";
									$status_desc = "Inactive";
								}
							?>
							<tr class="odd gradeX">
								<td width="1%" class="text-nowrap"><?=$rows->id; ?></td>
								<td><?=$rows->sub_specialty_name_th; ?></td>
								<td>
									<span class="label label-<?=$status_label; ?>"><?=$status_desc; ?></span>
								</td>
								<td width="30%" class="text-center">
									<a class="btn btn-xs btn-warning" href="<?=$edit_menu_url; ?>">
										<i class="fas fa-edit"></i>
									</a>
									<a class="btn btn-xs btn-danger" href="<?=$delete_menu_url; ?>">
										<i class="fas fa-trash"></i>
									</a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
				<!-- end panel-body -->
			</div>
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
</div>