<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
		<li class="breadcrumb-item active">Header</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header"><?=$headline; ?></h1>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">จัดการแถบ Header / Logo / Social</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<?=validation_errors("<p style='color:red;'>", "</p>") ?>
					<?php
						if(is_numeric($update_id)) {
					?>
                        <div class="form-group row fileupload-buttonbar">
                            <div class="col-md-12">
                                <span class="fileinput-button m-r-3">
									<?php if($header_logo_big_img==''){ ?>
                                    <i class="fa fa-plus"></i>
                                    <!--
                                    <a href="<?=base_url(); ?>backend/src/Header/upload_image/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-primary" id="upload_img" name="upload_img">Upload Logo Image</button>
                                    </a>
	                                -->

	                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#uploadImgModal">Upload Header Image</button>

									<!-- Modal -->
									<div id="uploadImgModal" class="modal fade" role="dialog">
									  <div class="modal-dialog">
									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">Upload Header Image</h4>
									      </div>
									      <div class="modal-body">
						        			<form id="HeaderImageForm" name="HeaderImageForm" enctype="multipart/form-data" method="post" >
											<label class="control-label">Please Choose a file from your Computer </label>
											<input type="file" name="file" size="20">
									      </div>
									      <div class="modal-footer">
											<button type="submit" id="Imagesubmit" name="submit" value="Submit" class="btn btn-block btn-success">Upload</button>
									        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											</form>
									      </div>
									    </div>

									  </div>
									</div>

                                	<?php }else{ ?>
                                    <i class="fa fa-times"></i>
                                    <!--
									<a href="<?=base_url(); ?>backend/src/Header/delete_image/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-danger" id="delete_img" name="delete_img">Delete Logo Image</button>
                                    </a>
	                                -->

									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteImgModal">Delete Logo Image</button>

									<!-- Modal -->
									<div id="deleteImgModal" class="modal fade" role="dialog">
									  <div class="modal-dialog">
									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">Delete Logo Image</h4>
									      </div>
									      <div class="modal-body">
									        <p>Sure to delete logo image?</p>
									      </div>
									      <div class="modal-footer">
									      	<a href="<?=base_url(); ?>backend/src/Header/delete_image/<?=$update_id; ?>">
		                                    	<button type="button" class="btn btn-danger" id="delete_img" name="delete_img">Delete Logo Image</button>
		                                    </a>
									        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
									      </div>
									    </div>

									  </div>
									</div>

                                	<?php } ?>
                                	<!--
                                    <a href="<?=base_url(); ?>backend/src/Header/delete_header/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-danger" id="delete_header" name="delete_header">Delete Header</button>
                                    </a>
	                                -->

									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">Delete Header</button>

									<!-- Modal -->
									<div id="deleteModal" class="modal fade" role="dialog">
									  <div class="modal-dialog">
									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">Delete Header</h4>
									      </div>
									      <div class="modal-body">
									        <p>Sure to delete header?</p>
									      </div>
									      <div class="modal-footer">
		                                    <a href="<?=base_url(); ?>backend/src/Header/delete_header/<?=$update_id; ?>">
		                                    	<button type="button" class="btn btn-danger" id="delete_header" name="delete_header">Delete Header</button>
		                                    </a>
									        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
									      </div>
									    </div>

									  </div>
									</div>

                                    <a href="<?=base_url(); ?>backend/src/Header/view_header_details/<?=$update_id; ?>">
                                        <button type="button" class="btn btn-success" id="view_header" name="view_header"><i class="fas fa-eye"></i> View Header In Front</button>
                                    </a>
                                </span>
                            </div>
                        </div>
					<?php }
						$location_form = base_url()."backend/src/Header/create/".$update_id;
					?>
					<form class="form-horizontal" action="<?= $location_form; ?>" method="POST" enctype="multipart/form-data">
						<div class="row row-space-10">
							<div class="col-md-3">
								<div class="form-group">
		                            <label for="header_title">ชื่อ Title <span class="text-danger">*</span></label>
		                            <input type="text" class="form-control" id="header_title" name="header_title" value="<?=$header_title; ?>" placeholder="Title" />
		                        </div>
		                    </div>
		                </div>
		                <div class="row row-space-10">
		                    <div class="col-md-3">
		                        <div class="form-group">
		                            <label for="url_facebook">Link Facebook</label>
		                            <input type="text" class="form-control" id="url_facebook" name="url_facebook" value="<?=$url_facebook; ?>" placeholder="Facebook" />
		                        </div>
		                    </div>
		                    <div class="col-md-3">
		                        <div class="form-group">
		                            <label for="url_twitter">Link Twitter</label>
		                            <input type="text" class="form-control" id="url_twitter" name="url_twitter" value="<?=$url_twitter; ?>" placeholder="Twitter" />
		                        </div>
		                    </div>
		                    <div class="col-md-3">
		                        <div class="form-group">
		                            <label for="url_instagram">Link Instagram</label>
		                            <input type="text" class="form-control" id="url_instagram" name="url_instagram" value="<?=$url_instagram; ?>" placeholder="Instagram" />
		                        </div>
		                    </div>
		                    <div class="col-md-3">
		                        <div class="form-group">
		                            <label for="url_youtube">Link Youtube</label>
		                            <input type="text" class="form-control" id="url_youtube" name="url_youtube" value="<?=$url_youtube; ?>" placeholder="Youtube" />
		                        </div>
		                    </div>
		                </div>
						<div class="form-group row">
                            <label class="col-md-1 col-form-label" for="header_status">สถานะ</label>
                            <div class="col-md-4">
							<?php
							$additional_dd_code = "id='selectStatusCode' class='form-control'";
								$options = array(
												'' => 'โปรดเลือกการเผยแพร่',
												'1' => 'Published',
												'0' => 'Unpublished'
											);

								echo form_dropdown('header_status', $options, $header_status, $additional_dd_code);
							?>
							</div>
                        </div>

		                <div class="row row-space-10" style="margin-bottom: 40px;">
		                    <div class="col-md-4" id="PreviewSection" style="width: 0px;height: 0px;opacity: 0;">
		                    	<label for="package_preview_img" >Preview Upload Image |  </label>
		                    	<button type="button" class="btn btn-danger" id="deletePreview" name="deletePreview">Delete</button>
		                        <div class="form-group" id="preimage_section"></div>
		                    </div>		                        
		                </div>

						<?php if($header_logo_big_img!=''){ //ต้องทำการส่งค่า item_big_img มาจาก controller store_items->create ก่อน ?>
                        <div class="form-group row">
                            <label class="col-md-1 col-form-label">Big Image</label>
                            <div class="col-md-4">
								<img src="<?=base_url(); ?>gallery/logo/big_imgs/<?=$header_logo_big_img; ?>">
							</div>
                        </div>
                    	<?php }
							if(is_numeric($update_id)){
								echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">แก้ไขข้อมูล</button>';
							}else{
								echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">เพิ่มข้อมูล</button>';
                    		}
						?>
                        <button type="submit" id="submit" name="submit" value="Cancel" class="btn btn-sm btn-warning m-r-5">ยกเลิก</button>
                    </form>
				</div>
				<!-- end panel-body -->
			</div>
			<!-- end panel -->
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
</div>
<!-- end #content -->
