<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
		<li class="breadcrumb-item active">Contents</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header"><?=$headline; ?></h1>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">เพิ่มข้อมูลแพ็กเกจ</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<?=validation_errors("<p style='color:red;'>", "</p>") ?>
					<?php
						if(isset(explode('_',$update_id)[1]) && explode('_',$update_id)[1] == '2'){
							$language = "EN";
						}
						else{
							$language = "TH";
						}
						$update_id = explode('_',$update_id)[0];

						if(is_numeric($update_id)) {							
					?>
                        <div class="form-group row fileupload-buttonbar">
                            <div class="col-md-12">
                                <span class="fileinput-button m-r-3">
									<?php if($contents_big_img==''){ ?>
                                    <i class="fa fa-plus"></i>
                                    <a href="<?=base_url(); ?>backend/src/contents/upload_image/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-primary" id="upload_img" name="upload_img">Upload Contents Image</button>
                                    </a>
                                	<?php }else{ ?>
                                    <i class="fa fa-times"></i>
									<a href="<?=base_url(); ?>backend/src/Contents/delete_image/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-danger" id="delete_img" name="delete_img">Delete Contents Image</button>
                                    </a>
                                	<?php } ?>
                                    <a href="<?=base_url(); ?>backend/src/Contents/delete_contents/<?=$update_id; ?>">
                                    	<button type="button" class="btn btn-danger" id="delete_contents" name="delete_contents">Delete Contents</button>
                                    </a>
                                    <a href="<?=base_url(); ?>backend/src/Contents/view_contents_details/<?=$update_id; ?>">
                                        <button type="button" class="btn btn-success" id="view_contents" name="view_contents">View Contents In Front</button>
                                    </a>
                                    <?php
										if($language == "EN"){
											echo '<a href="'.base_url().'backend/src/Contents/create/'.explode('_',$update_id)[0].'">';
										}
										else{
											echo '<a href="'.base_url().'backend/src/Contents/create/'.explode('_',$update_id)[0]."_2".'">';
										}
									/*
                                    <a href="<?=base_url(); ?>backend/src/Contents/create/<?=$update_id.'_2'; ?>">
                                    */
                                    ?>
                                        <button type="button" class="btn btn-success" id="switch_language" name="switch_language">Switch Language</button>
                                    </a>
                                </span>
                            </div>
                        </div>
					<?php }
						$location_form = base_url()."backend/src/Contents/create/".$update_id;
					?>
					<form class="form-horizontal" action="<?= $location_form; ?>" method="POST" enctype="multipart/form-data">
						<H4><?=$language;?></H4>
						<div class="row row-space-10">
							<div class="col-md-4">
								<div class="form-group">
		                            <label for="contents_title">ชื่อ Title <span class="text-danger">*</span></label>
		                            <?php
		                            	if($language == 'EN'){
		                            		echo '<input type="hidden" class="form-control" id="contents_title" name="contents_title" value="'.$contents_title.'" />
				                            <input type="hidden" id="contents_name" name="contents_name" value="'.$contents_title.'" />';
		                            		echo '<input type="text" class="form-control" id="contents_title_en" name="contents_title_en" placeholder="ENG Title" value="'.$contents_title_en.'" />
				                            <input type="hidden" id="contents_name_en" name="contents_name_en" value="'.$contents_title_en.'" />';
				                        }
				                        else{
		                            		echo '<input type="text" class="form-control" id="contents_title" name="contents_title" placeholder="TH Title" value="'.$contents_title.'" />
				                            <input type="hidden" id="contents_name" name="contents_name" value="'.$contents_title.'" />';
		                            		echo '<input type="hidden" class="form-control" id="contents_title_en" name="contents_title_en" value="'.$contents_title_en.'" />
				                            <input type="hidden" id="contents_name_en" name="contents_name_en" value="'.$contents_title_en.'" />';
				                        }
		                            ?>		                            
		                        </div>
		                    </div>
							<div class="col-md-8">
		                        <div class="form-group">
		                            <label for="contents_subdesc">Contents Sub Description</label>
		                            <?php
		                            	if($language == 'EN'){
		                            		echo '<textarea class="form-control" style="display:none;" name="contents_subdesc">'.$contents_subdesc.'</textarea>';
		                            		echo '<textarea class="form-control" name="contents_subdesc_en" placeholder="ENG Subdescription">'.$contents_subdesc_en.'</textarea>';
				                        }
				                        else{
		                            		echo '<textarea class="form-control" name="contents_subdesc" placeholder="TH Subdescription" >'.$contents_subdesc.'</textarea>';
		                            		echo '<textarea class="form-control" style="display:none;" name="contents_subdesc_en">'.$contents_subdesc_en.'</textarea>';
				                        }
		                            ?>
		                        </div>
		                    </div>
		                </div>
		                <div class="row row-space-10">
		                    <div class="col-md-12">
		                        <div class="form-group">
		                            <label for="contents_description">Contents Description</label>
		                            <?php
		                            	if($language == 'EN'){
		                            		echo '<textarea style="display:none;" name="contents_description">'.$contents_description.'</textarea>';
		                            		echo '<textarea id="editor1" class="ckeditor" name="contents_description_en" >'.$contents_description_en.'</textarea>';
				                        }
				                        else{
		                            		echo '<textarea id="editor1" class="ckeditor" name="contents_description" >'.$contents_description.'</textarea>';
		                            		echo '<textarea style="display:none;" name="contents_description_en">'.$contents_description_en.'</textarea>';
				                        }
		                            ?>
		                            
		                        </div>
		                    </div>
		                </div>
						<div class="form-group row">
                            <div class="col-md-4">
								<div class="form-group">
		                            <label for="meta_author">Meta Author</label>
		                            <input type="text" class="form-control" id="meta_author" name="meta_author" value="<?=$meta_author; ?>" />
	                            </div>  
		                    </div>
                            <div class="col-md-4">
								<div class="form-group">
		                            <label for="meta_keyword">Meta Keyword</label>
		                            <input type="text" class="form-control" id="meta_keywords" name="meta_keywords" value="<?=$meta_keywords; ?>" />
	                            </div>  
		                    </div>
                            <div class="col-md-4">
								<div class="form-group">
		                            <label for="meta_desciption">Meta Description</label>
		                            <input type="text" class="form-control" id="meta_description" name="meta_description" value="<?=$meta_description; ?>" />
	                            </div>  
		                    </div>
                        </div>
                        <div class="form-group row">                        
                            <div class="col-md-4">
								<div class="form-group">
		                            <label for="contents_status">สถานะ</label>
									<?php
									$additional_dd_code = "id='selectStatusCode' class='form-control'";
										$options = array(
														'' => 'โปรดเลือกการเผยแพร่',
														'1' => 'Published',
														'0' => 'Unpublished'
													);

										echo form_dropdown('contents_status', $options, $contents_status, $additional_dd_code);
									?>
								</div>
							</div>
						</div>
                        </div>
						<?php if($contents_big_img!=''){ //ต้องทำการส่งค่า contents_big_img มาจาก controller Contents->create ก่อน ?>
                        <div class="form-group row">
                            <label class="col-md-1 col-form-label">Big Image</label>
                            <div class="col-md-4">
								<img src="<?=base_url(); ?>gallery/pagecontents/big_imgs/<?=$contents_big_img; ?>">
							</div>
                        </div>
                    	<?php }	
							if(is_numeric($update_id)){	
								echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">แก้ไขข้อมูล</button>';
							}else{
								echo '<button type="submit" id="submit" name="submit" value="Submit" class="btn btn-sm btn-primary m-r-5">เพิ่มข้อมูล</button>';
                    		}
						?>						
                        <button type="submit" id="submit" name="submit" value="Cancel" class="btn btn-sm btn-warning m-r-5">ยกเลิก</button>
                    </form>
				</div>
				<!-- end panel-body -->
			</div>
			<!-- end panel -->
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
</div>
<!-- end #content -->
