<!-- begin #content -->
<div id="content" class="content">		
	<h1>จัดการส่วน Account</h1>
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
		<li class="breadcrumb-item active">รายละเอียด Account</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<?php
		$create_account_url = base_url()."backend/Account/Register";
	?>
	<h1 class="page-header"><a href="<?=$create_account_url; ?>"><button type="button" class="btn btn-lime">Add New Account</button></a></small></h1>
	
	<?php
		if(isset($flash)){
			echo $flash;
		}
	?>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-lg-12">
			<div class="panel panel-inverse">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">จัดการ Account</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<table id="data-table-buttons" class="table table-bordered">
						<thead>
							<tr>
								<th width="1%" class="text-nowrap">#</th>
								<th class="text-nowrap">Username</th>
								<th class="text-nowrap">Firstname/Lastname</th>
								<th class="text-nowrap">สถานะ</th>
								<th class="text-nowrap">แก้ไข</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach($query->result() as $rows){
								/*$view_doctor_url = base_url()."backend/Account/view_doctor_details/".$rows->acc_id;*/
								$edit_acc_url = base_url()."backend/Account/Register/".$rows->acc_id;
								$delete_acc_url = base_url()."backend/Account/delete_account/".$rows->acc_id;
								$status = $rows->acc_status;
								if($status == 1){
									$status_label = "green";
									$status_desc = "Active";
								}else{
									$status_label = "secondary";
									$status_desc = "Inactive";
								}
							?>
							<tr class="odd gradeX">
								<td width="1%" ><?=$rows->acc_id; ?></td>
								<td><?=$rows->acc_username; ?></td>
								<td><?=$rows->acc_firstname.' '.$rows->acc_lastname; ?></td>
								<td class="text-center">
									<span class="label label-<?=$status_label; ?>"><?=$status_desc; ?></span>
								</td>
								<td width="30%" class="text-center">
									<!-- <a class="btn btn-xs btn-success" href="<?//=$view_doctor_url; ?>">
										<i class="fas fa-eye"></i>
									</a> -->
									<a class="btn btn-xs btn-warning" href="<?=$edit_acc_url; ?>">
										<i class="fas fa-edit"></i>
									</a>
									<a class="btn btn-xs btn-danger" href="<?=$delete_acc_url; ?>">
										<i class="fas fa-trash"></i>
									</a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
				<!-- end panel-body -->
			</div>
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
</div>