<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Back_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}

	public function initGeneral(){
		$this->load->model('Mdl_formemail');			
		$data['count'] = $this->Mdl_formemail->count_array(array('email_status' => 1, 'email_open' => 0));
		// SESSION
        $data['username'] = $this->session->userdata('ses_username');
      	$data['fullname'] = $this->session->userdata('ses_full_name');
      	$data['position'] = $this->session->userdata('ses_position');
      	$data['pic'] = $this->session->userdata('ses_pic');
      	// END SESSION

		$data['flash'] = $this->session->flashdata('item');

		$data['header'] = 'backend/src/header';
		$data['sidebar'] = 'backend/src/sidebar';
		$data['script'] = 'backend/src/script';

		return $data;
	}

	public function General($data = null){
		$this->load->model('Mdl_formemail');			
		$data['count'] = $this->Mdl_formemail->count_array(array('email_status' => 1, 'email_open' => 0));

        $data['username'] = $this->session->userdata('ses_username');
      	$data['fullname'] = $this->session->userdata('ses_full_name');
      	$data['position'] = $this->session->userdata('ses_position');
      	$data['pic'] = $this->session->userdata('ses_pic');

		$data['header'] = 'backend/src/header';
		$data['sidebar'] = 'backend/src/sidebar';
		$data['script'] = 'backend/src/script';

		return $data;
	}

	function _custom_query($mysql_query)
	{
		$query = $this->db->query($mysql_query);
		return $query;
	}
}
