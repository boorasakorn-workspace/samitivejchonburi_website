<?php
	class Mdl_frontend extends CI_Model
	{
		function __construct()
		{
			parent::__construct();
		}


		function get_tb_subnavigation()
		{
			$tbsubnav = "tb_subnavigation";
			return $tbsubnav;
		}

		function get_tb_navigation()
		{
			$tbnav = "tb_navigation";
			return $tbnav;
		}

		function get_subnav($order_by)
		{
			$table = $this->get_tb_subnavigation();
			$this->db->order_by($order_by);
			$query = $this->db->get($table);
			return $query;
		}

		function get_nav($order_by)
		{
			$table = $this->get_tb_navigation();
			$this->db->order_by($order_by);
			$query = $this->db->get($table);
			return $query;
		}

		function get_with_limit($limit, $offset, $order_by)
		{
			$table = $this->get_tb_navigation();
			$this->db->limit($limit, $offset);
			$this->db->order_by($order_by);
			$query = $this->db->get($table);
			return $query;
		}

		function get_where_subnav($id)
		{
			$table = $this->get_tb_subnavigation();
			$this->db->where('id', $id);
			$query = $this->db->get($table);
			return $query;
		}

		function get_where_nav($id)
		{
			$table = $this->get_tb_navigation();
			$this->db->where('id', $id);
			$query = $this->db->get($table);
			return $query;
		}

		function get_where_custom_subnav($col, $value)
		{
			$table = $this->get_tb_subnavigation();
			$this->db->where($col, $value);
			$query = $this->db->get($table);
			return $query;
		}

		function get_where_custom_nav($col, $value)
		{
			$table = $this->get_tb_navigation();
			$this->db->where($col, $value);
			$query = $this->db->get($table);
			return $query;
		}

		function _insert_subnav($data)
		{
			$table = $this->get_tb_subnavigation();
			$this->db->insert($table, $data);
		}

		function _insert_nav($data)
		{
			$table = $this->get_tb_navigation();
			$this->db->insert($table, $data);
		}

		function _update_subnav($id, $data)
		{
			$table = $this->get_tb_subnavigation();
			$this->db->where('id', $id);
			$this->db->update($table, $data);
		}

		function _update_nav($id, $data)
		{
			$table = $this->get_tb_navigation();
			$this->db->where('id', $id);
			$this->db->update($table, $data);
		}

		function _delete_subnav($id)
		{
			$table = $this->get_tb_subnavigation();
			$this->db->where('id', $id);
			$this->db->delete($table);
		}

		function _delete_nav($id)
		{
			$table = $this->get_tb_navigation();
			$this->db->where('id', $id);
			$this->db->delete($table);
		}

		function count_where($column, $value)
		{
			$table = $this->get_tb_navigation();
			$this->db->where($column, $value);
			$query = $this->db->get($table);
			$num_rows = $query->num_rows();
			return $num_rows;
		}

		function count_all()
		{
			$table = $this->get_tb_navigation();
			$query = $this->db->get($table);
			$num_rows = $query->num_rows();
			return $num_rows;
		}

		function get_max_subnav()
		{
			$table = $this->get_tb_subnavigation();
			$this->db->select_max('id');
			$query = $this->db->get($table);
			$row = $query->row();
			$id = $row->id;
			return $id;
		}

		function get_max_nav()
		{
			$table = $this->get_tb_navigation();
			$this->db->select_max('id');
			$query = $this->db->get($table);
			$row = $query->row();
			$id = $row->id;
			return $id;
		}

		function _custom_query($mysql_query)
		{
			$query = $this->db->query($mysql_query);
			return $query;
		}
	}