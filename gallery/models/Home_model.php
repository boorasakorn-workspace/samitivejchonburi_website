<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	function _insert_email($data)
	{
		$table = "tb_email";
		$this->db->insert($table, $data);
	}

	public function get_nav_menu(){
		$query=$this->db->get('language');
		return $query->result();
	}
	
	public function menu($language){
		$this->db->where('language',$language);
		$query=$this->db->get('tb_navigation');
		return $query->result();
	}

	public function package(){
		// $this->db->where('package',$language);
		$this->db->limit(3);
		//$query=$this->db->get('tb_package');
		$ql = $this->session->userdata('q_l');
		$query = $this->db->query("select package_title".$ql." as package_title , package_name".$ql." as package_name , package_description".$ql." as package_description , package_subdesc".$ql." as package_subdesc ,package_url,package_status,package_unitprice,package_big_img,package_small_img,package_startdate,package_enddate,id,c_user,m_user,created_at,updated_at,meta_author,meta_description,meta_keywords from tb_package where package_status='1' and package_big_img != '' and package_startdate <= '" . date('Y-m-d') . "' and package_enddate >= '" . date('Y-m-d') . "' order by id asc limit 4");

		return $query->result();
	}

	public function speciality(){
		// $this->db->where('package',$language);

		//$query=$this->db->get('speciality');
		$ql = $this->session->userdata('q_l');
		$query = $this->db->query("select speciality_id,speciality_name".$ql. " as speciality_name,speciality_code,cwhen,mwhen from speciality");
		return $query->result();
	}
	public function s_speciality(){
		// $this->db->where('package',$language);

		$query=$this->db->get('speciality');
		return $query->result();
	}


public function medicial_center(){
		// $this->db->where('package',$language);

		$this->db->limit(6);
		//$query=$this->db->get('tb_medical_center');
		$ql = $this->session->userdata('q_l');
		$query = $this->db->query("select medical_center_name".$ql." as medical_center_name , id , medical_master_id, medical_center_imgs, medical_center_thumb_imgs, medical_center_icon, medical_center_url, medical_center_details, medical_center_contents, medical_center_location, medical_center_office_daystxt, medical_center_office_hours, medical_center_phone, medical_center_email, medical_center_meta_keywords, medical_center_meta_description, c_user, m_user, order_priority, medical_center_is, created_at,updated_at from tb_medical_center where status='1' limit 6");
		return $query->result();
	}

public function lang_all_medicial_center(){
		// $this->db->where('package',$language);

		//$query=$this->db->get('tb_medical_center');
		$ql = $this->session->userdata('q_l');
		$query = $this->db->query("SELECT * , medical_center_name".$ql." as medical_center_name FROM tb_medical_center where status='1' ");
		return $query->result();
	}
public function lang_all_specialty(){
		$ql = $this->session->userdata('q_l');
		if($ql == ' '){
			$ql = '_th';
		}
		$query = $this->db->query("SELECT * , specialty_name".$ql." as specialty_name FROM tb_specialty WHERE status='1' ");
		return $query->result();
	}
public function lang_all_sub_specialty(){
		$ql = $this->session->userdata('q_l');
		if($ql == ' '){
			$ql = '_th';
		}
		$query = $this->db->query("SELECT * , sub_specialty_name".$ql." as sub_specialty_name FROM tb_sub_specialty WHERE status='1' ");
		return $query->result();
	}

public function all_specialty(){
		$ql = $this->session->userdata('q_l');
		if($ql == ' '){
			$ql = '_th';
		}
		$query = $this->db->query("SELECT * , specialty_name".$ql." as specialty_name FROM tb_specialty WHERE status='1' ");
		return $query->result();
	}

public function all_sub_specialty(){
		$query = $this->db->query("SELECT * FROM tb_sub_specialty WHERE status='1' ");
		return $query->result();
	}

public function all_medicial_center(){
		// $this->db->where('package',$language);

		//$query=$this->db->get('tb_medical_center');
		$ql = $this->session->userdata('q_l');
		$query = $this->db->query("SELECT * FROM tb_medical_center where status='1' ");
		return $query->result();
	}
	public function alldoctor(){

		$ql = $this->session->userdata('q_l');
		if($ql == ' '){
			$ql = '_th';
		}
		//$query=$this->db->get('tb_doctor');
		$query = $this->db->query("select id, doctor_url, doctor_imgs, pname" .$ql. " as pname_th, firstname" .$ql. " as firstname_th, lastname" .$ql. " as lastname_th, medical_id, medical_master_id, education" .$ql. " as education_th, experience" .$ql. " as experience_th, phone, email, address, type" .$ql. " as type_th, speciality_id, speciality" .$ql. " as speciality_th, doctor_department, doctor_department_child, language" .$ql. " as language_th, gender, at_hospital, status, come_when, out_when, created_at, updated_at, doctor_code, location_code, location_name  , parent_medical_center_id , parent_specialty_id , parent_sub_specialty_id from tb_doctor where status='1'");
		return $query->result();
	}
	public function doctor(){
		
		$this->db->limit(4);
		//$query=$this->db->get('tb_doctor');
		$ql = $this->session->userdata('q_l');
		if($ql == ' '){
			$ql = '_th';
		}
		//$query=$this->db->get('tb_doctor');
		$query = $this->db->query("select id, doctor_url, doctor_imgs, pname" .$ql. " as pname_th, firstname" .$ql. " as firstname_th, lastname" .$ql. " as lastname_th, medical_id, medical_master_id, education" .$ql. " as education_th, experience" .$ql. " as experience_th, phone, email, address, type" .$ql. " as type_th, speciality_id, speciality" .$ql. " as speciality_th, doctor_department, doctor_department_child, language" .$ql. " as language_th, doctor_description" .$ql. " as doctor_description, gender, at_hospital, status, come_when, out_when, created_at, updated_at, doctor_code, location_code, location_name , parent_medical_center_id , parent_specialty_id , parent_sub_specialty_id from tb_doctor where status='1' limit 4");
		return $query->result();
	}

	public function where_doctor($doc_id){
		
		//$query=$this->db->get('tb_doctor');
		$ql = $this->session->userdata('q_l');
		if($ql == ' '){
			$ql = '_th';
		}
		//$query=$this->db->get('tb_doctor');
		$query = $this->db->query("select id, doctor_url, doctor_imgs, pname" .$ql. " as pname_th, firstname" .$ql. " as firstname_th, lastname" .$ql. " as lastname_th, medical_id, medical_master_id, education" .$ql. " as education_th, experience" .$ql. " as experience_th, phone, email, address, type" .$ql. " as type_th, speciality_id, speciality" .$ql. " as speciality_th, doctor_department, doctor_department_child, language" .$ql. " as language_th, doctor_description" .$ql. " as doctor_description, gender, at_hospital, status, come_when, out_when, created_at, updated_at, doctor_code, location_code, location_name , parent_medical_center_id , parent_specialty_id , parent_sub_specialty_id from tb_doctor where status='1' and id = '".$doc_id."'");
		return $query->row();
	}

	public function new_activity(){
		// $this->db->where('package',$language);
		$this->db->limit(5);
		//$query=$this->db->get('new_activity');
		//$query = $this->db->query('SELECT * FROM new_activity LEFT JOIN tb_account on tb_account.acc_id = new_activity.cuser');
		$ql = $this->session->userdata('q_l');
		$query = $this->db->query("select new_activity_name".$ql." as new_activity_name , description".$ql." as description , new_activity_big_img,new_activity_small_img,new_activity_id,cuser,cwhen,mwhen,meta_author,meta_description,meta_keywords , tb_account.* FROM new_activity LEFT JOIN tb_account on tb_account.acc_id = new_activity.cuser WHERE new_activity_status = 1 limit 5");

		return $query->result();
	}


	public function bloghealthy(){
		// $this->db->where('package',$language);
		$this->db->limit(6);
		//$query=$this->db->get('tb_bloghealthy');
		$ql = $this->session->userdata('q_l');

		$ql_doc = $this->session->userdata('q_l');
		if($ql_doc == ' '){
			$ql_doc = '_th';
		}

		//$query = $this->db->query("select blog_title".$ql." as blog_title , blog_description".$ql." as blog_description , blog_contents".$ql." as blog_contents ,id,blog_url,blog_keywords,blog_status,date_published,c_user,m_user,created_at,updated_at,meta_author,meta_description,meta_keywords,blog_big_img,blog_small_img,promotion_link_1,promotion_link_2 from tb_bloghealthy where blog_status='1' and blog_big_img != '' order by id desc limit 6");
		$query = $this->db->query("select tb_bloghealthy.blog_title".$ql." as blog_title , tb_bloghealthy.blog_description".$ql." as blog_description , tb_bloghealthy.blog_contents".$ql." as blog_contents ,tb_bloghealthy.id,tb_bloghealthy.blog_url,tb_bloghealthy.blog_keywords,tb_bloghealthy.blog_status,tb_bloghealthy.date_published,tb_bloghealthy.c_user,tb_bloghealthy.m_user,tb_bloghealthy.created_at,tb_bloghealthy.updated_at,tb_bloghealthy.meta_author,tb_bloghealthy.meta_description,tb_bloghealthy.meta_keywords,tb_bloghealthy.blog_big_img,tb_bloghealthy.blog_small_img,tb_bloghealthy.promotion_link_1,tb_bloghealthy.promotion_link_2,CONCAT(tb_doctor.pname".$ql_doc.",tb_doctor.firstname".$ql_doc.",' ',tb_doctor.lastname".$ql_doc.") as doctor_name from tb_bloghealthy JOIN tb_doctor ON tb_doctor.id = tb_bloghealthy.doc_id where blog_status='1' and blog_big_img != '' order by id desc limit 6");
		return $query->result();
	}

	public function bloghealthyofdoc($doc_id){
		// $this->db->where('package',$language);
		//$query=$this->db->get('tb_bloghealthy');
		$ql = $this->session->userdata('q_l');
		$query = $this->db->query("select blog_title".$ql." as blog_title , blog_description".$ql." as blog_description , blog_contents".$ql." as blog_contents ,id,blog_url,blog_keywords,blog_status,date_published,c_user,m_user,created_at,updated_at,meta_author,meta_description,meta_keywords,blog_big_img,blog_small_img,promotion_link_1,promotion_link_2,doc_id from tb_bloghealthy where blog_status='1' and blog_big_img != '' and doc_id = '". $doc_id ."' order by date_published desc");
		return $query->result();
	}

	public function where_related_doctor($doc_id){
		
		//$query=$this->db->get('tb_doctor');
		$ql = $this->session->userdata('q_l');
		if($ql == ' '){
			$ql = '_th';
		}
		//$query=$this->db->get('tb_doctor');
		$query = $this->db->query("select id, doctor_url, doctor_imgs, pname" .$ql. " as pname_th, firstname" .$ql. " as firstname_th, lastname" .$ql. " as lastname_th, medical_id, medical_master_id, education" .$ql. " as education_th, experience" .$ql. " as experience_th, phone, email, address, type" .$ql. " as type_th, speciality_id, speciality" .$ql. " as speciality_th, doctor_department, doctor_department_child, language" .$ql. " as language_th, doctor_description" .$ql. " as doctor_description, gender, at_hospital, status, come_when, out_when, created_at, updated_at, doctor_code, location_code, location_name from tb_doctor where status='1' and (parent_medical_center_id = '".$doc_id."' OR parent_medical_center_id LIKE '%|".$doc_id."%' )");
		return $query->result();
	}

	public function id_medicial_center($update_id){
		// $this->db->where('package',$language);

		//$query=$this->db->get('tb_medical_center');
		$ql = $this->session->userdata('q_l');
		$query = $this->db->query("select medical_center_name".$ql." as medical_center_name , id , medical_master_id, medical_center_imgs, medical_center_thumb_imgs, medical_center_icon, medical_center_url, medical_center_details, medical_center_contents, medical_center_location, medical_center_office_daystxt, medical_center_office_hours, medical_center_phone, medical_center_email, medical_center_meta_keywords, medical_center_meta_description, c_user, m_user, order_priority, medical_center_is, created_at,updated_at from tb_medical_center where status='1' and id='".$update_id."'");
		return $query->row();
	}

}
