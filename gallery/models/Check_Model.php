<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Check_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function Available($tableCheck,$idCheck,$idCustom = 'id',$statusCustom = 'status'){
		$select_query = "SELECT * ";
		$from_query = "FROM ".$tableCheck." ";
		$where_query = "WHERE ".$idCustom." = '".$idCheck."' AND ".$statusCustom." = 1 ";
		$after_query = " ";

		$mysql_query = $select_query.$from_query.$where_query.$after_query;
		$rowsCount = $this->_custom_query($mysql_query)->num_rows();
		if($rowsCount > 0){
			return true;
		}else{
			return false;
		}
	}

	function _custom_query($mysql_query)
	{
		$query = $this->db->query($mysql_query);
		return $query;
	}
}
