<?php
  class Mdl_account extends CI_Model
  {
  
    function __construct()
    {
        parent::__construct();
    }

    function get_table()
    {
        $table = "tb_account";
        return $table;
    }


    // Member
    public function logged_security_id(){
      $this->load->library('session');
      $ses_id = $this->session->userdata('session_id');
      return $ses_id;
    }


    // ADMIN
    /*public function logged_admin_name(){
      $this->load->library('session');
      $sesAdmin = array(
                    'ses_id' => $this->session->userdata('ses_id'),
                    'ses_username' => $this->session->userdata('ses_username'),
                    'ses_full_name' => $this->session->userdata('ses_full_name'),
                    'ses_position' => $this->session->userdata('ses_position'),
                    'ses_status' => $this->session->userdata('ses_status')
                  );
      return $sesAdmin;
    }*/

    public function create_user($data) {
      $data['acc_password'] = $this->hash_password($data['acc_password']);
      return $this->db->insert('tb_account', $data);
    }

    private function hash_password($password) {
      return password_hash($password, PASSWORD_BCRYPT);
    }

    private function verify_password_hash($password, $hash) {
      return password_verify($password, $hash);
    }

  function get($order_by)
  {
   $table = $this->get_table();
   $this->db->order_by($order_by);
   $query = $this->db->get($table);
   return $query;
  }

  function get_with_limit($limit, $offset, $order_by)
  {
   $table = $this->get_table();
   $this->db->limit($limit, $offset);
   $this->db->order_by($order_by);
   $query = $this->db->get($table);
   return $query;
  }

  function get_where($id)
  {
   $table = $this->get_table();
   $this->db->where('acc_id', $id);
   $query = $this->db->get($table);
   return $query;
  }

  function get_where_custom($col, $value)
  {
   $table = $this->get_table();
   $this->db->where($col, $value);
   $query = $this->db->get($table);
   return $query;
  }

  function _insert($data)
  {
   $table = $this->get_table();
   $this->db->insert($table, $data);
  }

  function _update($id, $data)
  {
   $table = $this->get_table();
   $this->db->where('acc_id', $id);
   $this->db->update($table, $data);
  }

  function _delete($id)
  {
   $table = $this->get_table();
   $this->db->where('acc_id', $id);
   $this->db->delete($table);
  }

  function count_where($column, $value)
  {
   $table = $this->get_table();
   $this->db->where($column, $value);
   $query = $this->db->get($table);
   $num_rows = $query->num_rows();
   return $num_rows;
  }

  function count_all()
  {
   $table = $this->get_table();
   $query = $this->db->get($table);
   $num_rows = $query->num_rows();
   return $num_rows;
  }

  function get_max()
  {
   $table = $this->get_table();
   $this->db->select_max('acc_id');
   $query = $this->db->get($table);
   $row = $query->row();
   $id = $row->acc_id;
   return $id;
  }

  function _custom_query($mysql_query)
  {
   $query = $this->db->query($mysql_query);
   return $query;
  }
 }