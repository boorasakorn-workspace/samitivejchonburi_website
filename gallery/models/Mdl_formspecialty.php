<?php
	class Mdl_formspecialty extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}

		function get_parent_table(){			
			$parent_table = "tb_medical_center";
			return $parent_table;
		}

		function get_table()
		{
			$parent_table = "tb_specialty";
			$table = "tb_specialty";
			return $table;
		}

		function check_name_duplicate($namecheck)
		{
			$table = $this->get_table();
			$this->db->where('specialty_name_th', $namecheck);
			$query = $this->db->get($table);
			return $query->num_rows();
		}

		function get_id_parent($parentname)
		{
			$table = $this->get_parent_table();
			$this->db->select('id');
			$this->db->where('medical_center_name', $parentname);
			$query = $this->db->get($table);
			return $query;
		}

		function get($order_by)
		{
			$table = $this->get_table();
			$this->db->order_by($order_by);
			$query = $this->db->get($table);
			return $query;
		}

		function get_with_limit($limit, $offset, $order_by)
		{
			$table = $this->get_table();
			$this->db->limit($limit, $offset);
			$this->db->order_by($order_by);
			$query = $this->db->get($table);
			return $query;
		}

		function get_where($id)
		{
			$table = $this->get_table();
			$this->db->where('id', $id);
			$query = $this->db->get($table);
			return $query;
		}

		function get_parent_sub($id)
		{
			$table = $this->get_table();
			$this->db->where('parent_id', $id);
			$query = $this->db->get($table);
			return $query;
		}


		function get_where_custom($col, $value)
		{
			$table = $this->get_table();
			$this->db->where($col, $value);
			$query = $this->db->get($table);
			return $query;
		}

		function _insert($data)
		{
			$table = $this->get_table();
			$this->db->insert($table, $data);
		}

		function _update($id, $data)
		{
			$table = $this->get_table();
			$this->db->where('id', $id);
			$this->db->update($table, $data);
		}

		function _delete($id)
		{
			$table = $this->get_table();
			$this->db->where('id', $id);
			$this->db->delete($table);
		}

		function count_where($column, $value)
		{
			$table = $this->get_table();
			$this->db->where($column, $value);
			$query = $this->db->get($table);
			$num_rows = $query->num_rows();
			return $num_rows;
		}

		function count_all()
		{
			$table = $this->get_table();
			$query = $this->db->get($table);
			$num_rows = $query->num_rows();
			return $num_rows;
		}

		function get_max()
		{
			$table = $this->get_table();
			$this->db->select_max('id');
			$query = $this->db->get($table);
			$row = $query->row();
			$id = $row->id;
			return $id;
		}

		function _custom_query($mysql_query)
		{
			$query = $this->db->query($mysql_query);
			return $query;
		}		
	}