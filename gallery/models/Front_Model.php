<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function Check_Preview(){
		$this->load->library('session');
		$data['preview'] = $this->session->flashdata('preview');
		return $data;
	}

	public function General(){
        $data = $this->Check_Preview();

		$data['head_navbar'] = 'frontend/src/head_navbar';
		$data['get_header'] = $this->fetch_header();
		$data['get_nav'] = $this->fetch_nav()->result();
		$data['get_subnav'] = $this->fetch_subnav()->result();

		$data['footerpages'] = 'frontend/src/footerpages';

		return $data;
	}

	public function Detailpage(){
		$data = $this->General();

		$data['get_sidemenu'] = $this->fetch_sidemenu()->result();
		$data['get_subsidemenu'] = $this->fetch_subsidemenu()->result();
		$data['get_subofsubmenu'] = $this->fetch_subofsubmenu()->result();

		return $data;
	}

	// GENERAL
	function fetch_header(){		

		$select_query = "SELECT * ";
		$from_query = "FROM tb_header ";
		$where_query = "WHERE header_status = 1 ";
		$after_query = " ";

		$mysql_query = $select_query.$from_query.$where_query.$after_query;
		$query = $this->_custom_query($mysql_query)->row();
		return $query;
	}
	function fetch_nav(){
		$ql = $this->session->userdata('q_l');

		$select_query = 
		"SELECT nav_title".$ql." AS nav_title, 
			nav_url, 
			nav_content, 
			id ";
		$from_query = "FROM tb_navigation ";
		$where_query = "WHERE nav_status = 1 ";
		$after_query = "ORDER BY order_priority ASC ";

		$mysql_query = $select_query.$from_query.$where_query.$after_query;
		$query = $this->_custom_query($mysql_query);

		return $query;
	}
	function fetch_subnav(){
		$ql = $this->session->userdata('q_l');

		$select_query = 
		"SELECT tb_subnavigation.id AS subid, 
			tb_subnavigation.sub_nav_title".$ql." AS sub_nav_title, 
			tb_subnavigation.sub_nav_url,
			tb_subnavigation.sub_nav_content,
			tb_subnavigation.parent_nav_id,
			tb_navigation.id ";
		$from_query = "FROM tb_subnavigation ";
		$join_query = "INNER JOIN tb_navigation ";
		$join_on_query = "ON tb_subnavigation.parent_nav_id = tb_navigation.id ";
		$where_query = 
		"WHERE tb_subnavigation.sub_nav_status = 1
		 AND tb_subnavigation.parent_nav_id = tb_navigation.id ";
		$after_query = "ORDER BY tb_subnavigation.order_priority ASC ";

		$mysql_query = $select_query.$from_query.$join_query.$join_on_query.$where_query.$after_query;
		$query = $this->_custom_query($mysql_query);

		return $query;
	}

	//Detailpage	
	function fetch_sidemenu(){
		$ql = $this->session->userdata('q_l');

		$select_query = 
		"SELECT sidemenu_title".$ql." AS sidemenu_title, 
			sidemenu_url, 
			sidemenu_content, 
			id ";
		$from_query = "FROM tb_sidemenu ";
		$where_query = "WHERE sidemenu_status = 1 ";
		$after_query = "ORDER BY order_priority ASC ";

		$mysql_query = $select_query.$from_query.$where_query.$after_query;
		$query = $this->_custom_query($mysql_query);

		return $query;
	}
	function fetch_subsidemenu(){
		$ql = $this->session->userdata('q_l');

		$select_query = 
		"SELECT parent_sidemenu_id, 
			sub_sidemenu_title".$ql." AS sub_sidemenu_title,
			sub_sidemenu_url, 
			sub_sidemenu_content, 
			id ";
		$from_query = "FROM tb_subofsidemenu ";
		$where_query = "WHERE sub_sidemenu_status = 1 ";
		$after_query = "ORDER BY order_priority ASC ";

		$mysql_query = $select_query.$from_query.$where_query.$after_query;
		$query = $this->_custom_query($mysql_query);

		return $query;
	}
	function fetch_subofsubmenu(){
		$ql = $this->session->userdata('q_l');

		$select_query = 
		"SELECT parent_submenu_id, 
			subofsubmenu_title".$ql." AS subofsubmenu_title,
			subofsubmenu_url, 
			subofsubmenu_content, 
			id ";
		$from_query = "FROM tb_subofsubmenu ";
		$where_query = "WHERE subofsubmenu_status = 1 ";
		$after_query = "ORDER BY order_priority ASC ";

		$mysql_query = $select_query.$from_query.$where_query.$after_query;
		$query = $this->_custom_query($mysql_query);

		return $query;
	}

	function _custom_query($mysql_query)
	{
		$query = $this->db->query($mysql_query);
		return $query;
	}
}
