<?php
	class Mdl_site_security extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}

		/*function test()
		{
			$name = "Sorakrit";
			$hashed_name = $this->_hash_string($name);
			echo "You are $name<br/>";
			echo $hashed_name;

			echo "<hr>";

			$submitted_name = "Sorakrit";
			$result = $this->_verify_hash($submitted_name, $hashed_name);

			if($result==TRUE){
				echo "Well Done";
			}else{
				echo "Failed";
			}
		}*/

		function check_login(){
	      $username = $this->input->post('acc_username');
	      $password = $this->input->post('acc_password');

	      $this->db->select('*');
	      $this->db->from('tb_account');
	      $this->db->where('acc_username', $username);
	      $query = $this->db->get();
	      if ($query->num_rows() == 1) {
	        $hash = $query->row('acc_password');
	          /*echo "<pre>"; print_r($hash->acc_password); die();*/
	        if(!$this->_verify_hash($password, $hash)){
		        echo "Password is invalid";
	        }
	        else{
	        	return $query->result();
	        }	        
	      } else {
	        echo "Username is invalid";
	      }
	    }

		function generate_randStr($length)
		{
			$char = '23456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$randStr = '';

			for($i=0; $i<$length; $i++){
				$randStr .= $char[rand(0, strlen($char) -1)];
			}

			return $randStr;
		}

		// เข้ารหัส ด้วย hashing จาก module store_accounts->update_password()
		function _hash_string($str)
		{
			$hashed_string = password_hash($str, PASSWORD_BCRYPT, array(
				'cost' => 11
			));
			return $hashed_string;
		}

		// เช็ครหัสซ้ำ
		function _verify_hash($plain_text_str, $hashed_string)
		{
			$result = password_verify($plain_text_str, $hashed_string);
			return $result; // TRUE or FALSE
		}

		function _make_sure_is_admin()
		{
			/*$is_admin = TRUE;

			if($is_admin != TRUE){
				redirect('site_security/not_allowed');
			}*/
			if($this->session->userdata('ses_position') != '100'){
				redirect(base_url('frontend/Home'));
			}
		}

		function not_allowed()
		{
			echo "You are not allowed to be here.";
		}

	}