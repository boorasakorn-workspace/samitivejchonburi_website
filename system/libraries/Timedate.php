<?php
	class CI_Timedate
	{

		public function get_date($timestamp, $format)
		{
			switch ($format){
				case 'fulldatetime':
				// FULL Date Time Ex. -> Friday 18th of October 2018 at 10:00:00 AM
				$the_date = date('l jS \of F Y \a\t h:i:s A', $timestamp);
				break;

				case 'fulldate':
				// FULL Date Ex. -> Friday 18th of October 2011
				$the_date = date('l jS \of F Y', $timestamp);
				break;

				case 'fulldate_en':
				// FULL Date Ex. -> 18 October 2011
				$the_date = date('d F Y', $timestamp);
				break;

				case 'shorterdate':
				// Shorter Date Ex. -> 18th of October 2011
				$the_date = date('jS \of F Y', $timestamp);
				break;

				case 'minidate':
				// Mini Date Ex. -> 18th Oct 2011
				$the_date = date('jS M Y', $timestamp);
				break;

				case 'shortestdate':
				// Shortest Date Ex. -> 18/2/11
				$the_date = date('j\/n\/y', $timestamp);
				break;

				case 'datepicker':
				// Mini Date Ex. -> 18-2-11
				$the_date = date('d\-m\-Y', $timestamp);
				break;

				case 'datepicker_us':
				// Mini Date Ex. -> 2/18/11
				$the_date = date('m\-d\-Y', $timestamp);
				break;

				case 'monyear':
				// Mon Year Ex. -> 18th Oct 2011
				$the_date = date('F Y', $timestamp);
				break;
			}

			return $the_date;
		}

		public function make_timestamp_from_datepicker_us($datepicker)
		{
			$hours = 7;
			$minutes = 0;
			$seconds = 0;

			$month = substr($datepicker, 0, 2);
			$day = substr($datepicker, 3, 2);
			$year = substr($datepicker, 6, 4);

			$timestamp = mktime($hours, $minutes, $seconds, $month, $day, $year);
			return $timestamp;
		}

		public function make_timestamp_from_datepicker($datepicker)
		{
			$hours = 7;
			$minutes = 0;
			$seconds = 0;

			$day = substr($datepicker, 0, 2);
			$month = substr($datepicker, 3, 2);
			$year = substr($datepicker, 6, 4);

			$timestamp = mktime($hours, $minutes, $seconds, $month, $day, $year);
			return $timestamp;
		}

		public function make_timestamp($day, $month, $year)
		{
			$hours = 7;
			$minutes = 0;
			$seconds = 0;

			$timestamp = mktime($hours, $minutes, $seconds, $month, $day, $year);
			return $timestamp;
		}

	}