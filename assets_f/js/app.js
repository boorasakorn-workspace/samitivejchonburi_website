
/* 05. Handle Theme Panel Expand
------------------------------------------------ */
var handleThemePanelExpand1 = function() {
    $(document).on('click', '[data-click="theme-panel-expand1"]', function() {
        var targetContainer = '.theme-panel1';
        var targetClass = 'active';
        if ($(targetContainer).hasClass(targetClass)) {
            $(targetContainer).removeClass(targetClass);
        } else {
            $(targetContainer).addClass(targetClass);
        }
    });
};


/* 05. Handle Theme Panel Expand
------------------------------------------------ */
var handleThemePanelExpand2 = function() {
    $(document).on('click', '[data-click="theme-panel-expand2"]', function() {
        var targetContainer = '.theme-panel2';
        var targetClass = 'active';
        if ($(targetContainer).hasClass(targetClass)) {
            $(targetContainer).removeClass(targetClass);
        } else {
            $(targetContainer).addClass(targetClass);
        }
    });
};

/* 05. Handle Theme Panel Expand
------------------------------------------------ */
var handleThemePanelExpand3 = function() {
    $(document).on('click', '[data-click="theme-panel-expand3"]', function() {
        var targetContainer = '.theme-panel3';
        var targetClass = 'active';
        if ($(targetContainer).hasClass(targetClass)) {
            $(targetContainer).removeClass(targetClass);
        } else {
            $(targetContainer).addClass(targetClass);
        }
    });
};

/* 05. Handle Theme Panel Expand
------------------------------------------------ */
var handleThemePanelExpand4 = function() {
    $(document).on('click', '[data-click="theme-panel-expand4"]', function() {
        var targetContainer = '.theme-panel4';
        var targetClass = 'active';
        if ($(targetContainer).hasClass(targetClass)) {
            $(targetContainer).removeClass(targetClass);
        } else {
            $(targetContainer).addClass(targetClass);
        }
    });
};

/* 06. Handle Theme Page Control
------------------------------------------------ */
var handleThemePageControl = function() {
	if (typeof Cookies !== 'undefined') {
		if (Cookies && Cookies.get('theme')) {
			if ($('.theme-list').length !== 0) {
				$('.theme-list [data-theme]').closest('li').removeClass('active');
				$('.theme-list [data-theme="'+ Cookies.get('theme') +'"]').closest('li').addClass('active');
			}
			var cssFileSrc = $('[data-theme="'+ Cookies.get('theme') +'"]').attr('data-theme-file');
			$('#theme').attr('href', cssFileSrc, { expires: 365 });
		}
	
		$(document).on('click', '.theme-list [data-theme]', function() {
			var cssFileSrc = $(this).attr('data-theme-file');
			$('#theme').attr('href', cssFileSrc);
			$('.theme-list [data-theme]').not(this).closest('li').removeClass('active');
			$(this).closest('li').addClass('active');
			Cookies.set('theme', $(this).attr('data-theme'));
		});
	}
};


/* Application Controller
------------------------------------------------ */
var App = function () {
	"use strict";
	
	return {
		//main function
		init: function () {
            handleThemePanelExpand1();
            handleThemePanelExpand2();
            handleThemePanelExpand3();
            handleThemePanelExpand4();
            handleThemePageControl();
		}
  };
}();